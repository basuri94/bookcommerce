<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Refer extends Model
{
   protected $table="refers";
   protected $fillable=['referby_amount','getrefer_amount'];
}
