<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Emailtemplate;
use App\Models\GeneralSetting;
class ReferMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $array;
    public $app_url;
    public $userInfo;
    public function __construct($array)
    {
        $app_url=env('APP_URL');
        $this->array = $array;
        $this->app_url =  $app_url;
        $this->userInfo=GeneralSetting::first();
       
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->array['referby']==""){
          
            $getData=Emailtemplate::where('slug','referred')->first();
            $result = strtr($getData->mail_body,array(
                'app_url'=> $this->app_url,
                'user_name'=> $this->array['name'],
                'amount_bonus'=> $this->array['getrefer_amount'],
                'contact_email' => $this->userInfo->email,
                'referrer_name' => $this->array['referbyname'],
            ));
           }
           else if($this->array['referby']=="1"){
          
            $getData=Emailtemplate::where('slug','refer_by')->first();
            $result = strtr($getData->mail_body,array(
                'app_url'=> $this->app_url,
                'user_name'=> $this->array['referbyname'],
                'amount_bonus'=> $this->array['referby_amount'],
                'joiner_name'=> $this->array['name'],
                'contact_email' => $this->userInfo->email,
              
              
            ));
           }
         
          
    
            return $this
            ->with(['content' => $result])->view('emails.emailverification')
          //  ->from($this->array['from'])
            ->subject($getData->subject);
    }
}
