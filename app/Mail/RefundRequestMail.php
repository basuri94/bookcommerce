<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Emailtemplate;
use App\Models\GeneralSetting;
class RefundRequestMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $array;
    public $app_url;
    public $userInfo;
    public function __construct($array)
    {
        $app_url=env('APP_URL');
        $this->array = $array;
        $this->app_url =  $app_url;
        $this->userInfo=GeneralSetting::first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      
       if( $this->array['type_select']=="Return"){
        $getData=Emailtemplate::where('slug','refund_request')->first();
        $result = strtr($getData->mail_body,array(
            'app_url'=> $this->app_url,
            'user_name'=> $this->array['name'],
            'order_id'=> $this->array['order_id'],
            'product_name'=> $this->array['product_name'],
            'product_amount'=> $this->array['product_amount'],
            'contact_email' => $this->userInfo->email,
          
          
        ));
       }
       else if($this->array['type_select']=="Replacement"){
        $getData=Emailtemplate::where('slug','replacement_request')->first();
        $result = strtr($getData->mail_body,array(
            'app_url'=> $this->app_url,
            'user_name'=> $this->array['name'],
            'order_id'=> $this->array['order_id'],
            'product_name'=> $this->array['product_name'],
            'product_amount'=> $this->array['product_amount'],
            'contact_email' => $this->userInfo->email,
          
          
        ));
       }
      
      if($this->array['sellerrefundapprove']==1){
        $getData=Emailtemplate::where('slug','sellerrefundapprove')->first();
        $result = strtr($getData->mail_body,array(
            'app_url'=> $this->app_url,
            'user_name'=> $this->array['name'],
            'order_id'=> $this->array['order_id'],
            'product_name'=> $this->array['product_name'],
            'product_amount'=> $this->array['product_amount'],
            'contact_email' => $this->userInfo->email,
          
          
        ));
      }
      if($this->array['sellerrefundreject']==1){
        $getData=Emailtemplate::where('slug','sellerrefundreject')->first();
        $result = strtr($getData->mail_body,array(
            'app_url'=> $this->app_url,
            'user_name'=> $this->array['name'],
            'order_id'=> $this->array['order_id'],
            'product_name'=> $this->array['product_name'],
            'product_amount'=> $this->array['product_amount'],
            'contact_email' => $this->userInfo->email,
          
          
        ));
      }
      if($this->array['type_select_from_seller']=="Return"){
        $getData=Emailtemplate::where('slug','return_from_seller')->first();
        $result = strtr($getData->mail_body,array(
            'app_url'=> $this->app_url,
            'user_name'=> $this->array['name'],
            'order_id'=> $this->array['order_id'],
            'product_name'=> $this->array['product_name'],
            'product_amount'=> $this->array['product_amount'],
            'contact_email' => $this->userInfo->email,
          
          
        ));
      }
      else if($this->array['type_select_from_seller']=="Replacement"){
        $getData=Emailtemplate::where('slug','replacement_from_seller')->first();
        $result = strtr($getData->mail_body,array(
            'app_url'=> $this->app_url,
            'user_name'=> $this->array['name'],
            'order_id'=> $this->array['order_id'],
            'product_name'=> $this->array['product_name'],
            'product_amount'=> $this->array['product_amount'],
            'contact_email' => $this->userInfo->email,
          
          
        ));
      }
        return $this
        ->with(['content' => $result])->view('emails.emailverification')
      //  ->from($this->array['from'])
        ->subject($getData->subject);
    }
}
