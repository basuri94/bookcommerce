<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Emailtemplate;
use App\Models\GeneralSetting;
class WalletMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $array;
    public $app_url;
    public $userInfo;
    public function __construct($array)
    {
        $app_url=env('APP_URL');
        $this->array = $array;
        $this->app_url =  $app_url;
        $this->userInfo=GeneralSetting::first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->array['amount_credit']!=""){
            $getData=Emailtemplate::where('slug','add_money_wallet')->first();
            $result = strtr($getData->mail_body,array(
                'app_url'=> $this->app_url,
                'user_name'=> $this->array['name'],
                'amount_credit'=>$this->array['amount_credit'],
                'contact_email' => $this->userInfo->email,
              
              
            ));
        }
         if($this->array['amount_debit']!=""){
            $getData=Emailtemplate::where('slug','debit_money_wallet')->first();
            $result = strtr($getData->mail_body,array(
                'app_url'=> $this->app_url,
                'user_name'=> $this->array['name'],
                'amount_debit'=>$this->array['amount_debit'],
                'contact_email' => $this->userInfo->email,
              
              
            ));
         }

           return $this
           ->with(['content' => $result])->view('emails.emailverification')
         //  ->from($this->array['from'])
           ->subject($getData->subject);
    }
}
