<?php

namespace App\Mail;
use App\Emailtemplate;
use App\Models\GeneralSetting;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SellerVerificationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $array;
    public $app_url;
    public $userInfo;
    public function __construct($array)
    {
        $app_url=env('APP_URL');
        $this->array = $array;
        $this->app_url =  $app_url;
        $this->userInfo=GeneralSetting::first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $getData=Emailtemplate::where('slug','seller_email_verification')->first();
       
        $result = strtr($getData->mail_body,array(
            'app_url'=> $this->app_url,
            'user_name'=> $this->array['name'],
            
            'password_confirmation'=> $this->array['password_confirmation'],
            'user_email'=> $this->array['user_email'],
           
            'contact_email' => $this->userInfo->email,
          
        ));
      

        return $this
        ->with(['content' => $result])->view('emails.emailverification')
      //  ->from($this->array['from'])
        ->subject($getData->subject);
    }
}
