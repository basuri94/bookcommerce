<?php

namespace App\Mail;
use App\Emailtemplate;
use App\Models\GeneralSetting;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CustomerVerificationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $array;
    public $app_url;
    public $userInfo;
    public function __construct($array)
    {
        $app_url=env('APP_URL');
        $this->array = $array;
        $this->app_url =  $app_url;
        $this->userInfo=GeneralSetting::first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       
       if($this->array['id']!=""){
          
        $getData=Emailtemplate::where('slug','customer_email_verification')->first();
        $result = strtr($getData->mail_body,array(
            'app_url'=> $this->app_url,
            'user_name'=> $this->array['name'],
            'id'=> $this->array['id'],
            'contact_email' => $this->userInfo->email,
          
        ));
       }
       else{
      
        $getData=Emailtemplate::where('slug','customer_signup')->first();
        $result = strtr($getData->mail_body,array(
            'app_url'=> $this->app_url,
            'user_name'=> $this->array['name'],
          
            'contact_email' => $this->userInfo->email,
          
        ));
       }
       
      

        return $this
        ->with(['content' => $result])->view('emails.emailverification')
      //  ->from($this->array['from'])
        ->subject($getData->subject);
    }
}
