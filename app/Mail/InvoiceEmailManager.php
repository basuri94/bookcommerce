<?php

namespace App\Mail;

use App\Emailtemplate;
use App\Models\GeneralSetting;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvoiceEmailManager extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $array;
    public $app_url;
    public $userInfo;
    public function __construct($array)
    {
        $app_url=env('APP_URL');
        $this->array = $array;
        $this->app_url =  $app_url;
        $this->userInfo=GeneralSetting::first();
    }
    /**
     * Build the message.
     *
     * @return $this
     */
     public function build()
     { 
//print_r($this->array);die;
        $getData=Emailtemplate::where('slug','customer_order_invoice')->first();
       
        $result = strtr($getData->mail_body,array(
            'app_url'=> $this->app_url,
            'customer_name'=> $this->array['name'],
            'contact_email' => $this->userInfo->email,
            'order_code'=>$this->array['order_code'],
        ));
         return $this
        ->with(['content' => $result])->view('emails.emailverification')
      //  ->from($this->array['from'])
        ->subject($getData->subject)
        ->attach($this->array['file'],[
            'as' => $this->array['file_name'],
            'mime' => 'application/pdf'
        ]);

        // return $this
        // ->with(['content' => $result])->view($this->array['view'])
        // ->from($this->array['from'])
        // ->subject($getData->subject)
        // ->attach($this->array['file'],[
        //     'as' => $this->array['file_name'],
        //     'mime' => 'application/pdf'
        // ]);
        
     }
 }
