<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Emailtemplate;
use App\Models\GeneralSetting;
class OrderMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $array;
    public $app_url;
    public $userInfo;
    public function __construct($array)
    {
        $app_url=env('APP_URL');
        $this->array = $array;
        $this->app_url =  $app_url;
        $this->userInfo=GeneralSetting::first();
       
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->array['delivery_status']=="on_review"){
          
            $getData=Emailtemplate::where('slug','order_onreview')->first();
            $result = strtr($getData->mail_body,array(
                'app_url'=> $this->app_url,
                'user_name'=> $this->array['name'],
                'order_code'=> $this->array['code'],
                'contact_email' => $this->userInfo->email,
              
            ));
           }
           else if($this->array['delivery_status']=="on_delivery"){
          
            $getData=Emailtemplate::where('slug','order_delivery')->first();
            $result = strtr($getData->mail_body,array(
                'app_url'=> $this->app_url,
                'user_name'=> $this->array['name'],
                'order_code'=> $this->array['code'],
                'contact_email' => $this->userInfo->email,
              
            ));
           }
           else if($this->array['delivery_status']=="delivered"){
            $getData=Emailtemplate::where('slug','order_delivered')->first();
            $result = strtr($getData->mail_body,array(
                'app_url'=> $this->app_url,
                'user_name'=> $this->array['name'],
                'order_code'=> $this->array['code'],
                'contact_email' => $this->userInfo->email,
              
            ));
            
           }
          
    
            return $this
            ->with(['content' => $result])->view('emails.emailverification')
          //  ->from($this->array['from'])
            ->subject($getData->subject);
    }
}
