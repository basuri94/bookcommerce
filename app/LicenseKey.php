<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LicenseKey extends Model
{
    protected $table='licensekeys';
}
