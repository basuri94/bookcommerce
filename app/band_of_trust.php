<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class band_of_trust extends Model
{
    protected $table='band_of_trust';
    protected $primaryKey = 'id';
}
