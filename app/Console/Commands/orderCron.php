<?php

namespace App\Console\Commands;

use App\BusinessSetting;
use App\CuriorRefund;
use App\Order;
use App\OrderDetail;
use App\RefundRequest;
use App\Seller;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Wallet;
class orderCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update admin to pay after return period over';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sellers = Seller::with('user')->get();
        $data = array();
        $refund_time=BusinessSetting::where('type','refund_request_time')->value('value');
        foreach ($sellers as $key => $val) {
            $final_admin_to_pay="";
            $totalCommissionPercentage = 0;
            $totalGateWayCharge = 0;
            $totalCourierRefundOrReplaceAmount = 0;
            $totalCourierCharge = 0;
            $walletCharge = 0;
            $tcs=0;
            $tds=0;
            $totalfinalWalletAmount=0;
            $price = OrderDetail::where('payment_status', 'paid')
                ->where('seller_id', $val->user_id)->sum('price');
            $tax = OrderDetail::where('payment_status', 'paid')
                ->where('seller_id', $val->user_id)->sum('tax');
            $shipping_cost = OrderDetail::where('payment_status', 'paid')
                ->where('seller_id', $val->user_id)->sum('shipping_cost');

            $total_price_includegst = $price + $tax;

           

            $courierCharge = DB::table('curior_orders')->join('order_details', 'order_details.curior_order_id', '=', 'curior_orders.id')
                ->where('curior_orders.seller_id', $val->user_id)->sum('curior_charge');

           
            $OrderDetail = OrderDetail::where('seller_id', $val->user_id)->where('payment_status', 'paid')
           // ->where(DB::raw('DATEDIFF( now(), STR_TO_DATE(order_details.created_at,"%d-%m-%Y") )'), '=', $refund_time);
            ->whereRaw('DATE(order_details.created_at) = DATE_SUB(CURDATE(), INTERVAL '.$refund_time. ' DAY)');
            
            $OrderDetail=$OrderDetail->get();
       
            foreach ($OrderDetail as $key => $OrderDetailVal) {
                $finalWalletAmount=0;
                $calculateCommission=0;
                $totalFee=0;
                $gatewayCharge=0;
                $walletAmount=0;
                if(!empty($OrderDetailVal->product)){
                    $OrderCharge = Order::where('id', $OrderDetailVal->order_id)->first();
                    if(!empty($OrderCharge->payment_details)){
                        $gatewayCharge = ((json_decode($OrderCharge->payment_details)->fee) / 100) / $OrderCharge->grand_total *    ($OrderDetailVal->tax + $OrderDetailVal->price + $OrderDetailVal->shipping_cost);
                    }
                   
    
                    $calculateCommission = (($OrderDetailVal->product->subcategory->commision_rate) * $OrderDetailVal->price) / 100;
                    $refunddata = RefundRequest::where('status', 'Approved')->where('order_id', $OrderDetailVal->id)->first();
                    if (!empty($refunddata)) {
                        if ($refunddata->type == "Refund") {
                            $refundAmount = $refunddata->amount;
                            $courierRefundAmount = CuriorRefund::where('refund_request_id', $refunddata->id)->where('status', 1)->value('curior_charge');
                            $totalCourierRefundOrReplaceAmount = $totalCourierRefundOrReplaceAmount + $refundAmount + $courierRefundAmount;
                        } else if ($refunddata->type == "Replacement") {
                            $totalReplaceAmount = CuriorRefund::where('refund_request_id', $refunddata->id)->where('status', 1)->sum('curior_charge');
                            $totalCourierRefundOrReplaceAmount = $totalCourierRefundOrReplaceAmount + $totalReplaceAmount;
                        }
                    }
                    $courierCharge = DB::table('curior_orders')->where('curior_orders.id', $OrderDetailVal->curior_order_id)->first();
                    if (!empty($courierCharge->curior_charge)) {
    
                        $courierChargeValue = ($courierCharge->curior_charge) / $OrderCharge->grand_total *    ($OrderDetailVal->tax + $OrderDetailVal->price + $OrderDetailVal->shipping_cost);
                        $totalCourierCharge = $totalCourierCharge + $courierChargeValue;
                    }
                   
                  if($OrderCharge->wallet_amount!="0.00"){
                 
                    $walletData=Wallet::where('payment_method','razorpay')->where('user_id',$OrderCharge->user_id)->get();
                    foreach( $walletData as $walletVal){
                   
                        if (!empty(json_decode($walletVal->payment_details)->fee)) {
                            $totalFee +=(json_decode($walletVal->payment_details)->fee);
                          }
                         $walletAmount +=$walletVal->amount;
                       
                        
                    }
                    if($totalFee!=0){
                        $convertTotalFee= $totalFee/100;
                        $WalletAmountCalculate=($convertTotalFee / $walletAmount) *$OrderCharge->wallet_amount;
                        $finalWalletAmount =  $WalletAmountCalculate / $OrderCharge->grand_total *     ($OrderDetailVal->tax + $OrderDetailVal->price + $OrderDetailVal->shipping_cost);;
                      
                    }
                   
                }
              
                    $totalGateWayCharge = $totalGateWayCharge + $gatewayCharge;
                    $totalCommissionPercentage = $calculateCommission + $totalCommissionPercentage;
                    $totalfinalWalletAmount +=$finalWalletAmount;
                }
               
            }
       
            $actual_gatewaycharge = number_format((float)$totalGateWayCharge, 2, '.', '');

            $gst_in_commisssion = (($totalCommissionPercentage * 18) / 100) + $totalCommissionPercentage;
            $tcs= ($price)*0.01;
            $tds= ($price)*0.01;
            $vendor_charges = $totalCourierCharge + $actual_gatewaycharge + $gst_in_commisssion;
          
           $admin_to_pay = $total_price_includegst - $vendor_charges - $totalCourierRefundOrReplaceAmount-$totalfinalWalletAmount-$tcs-$tds;

           $final_admin_to_pay = number_format((float)$admin_to_pay, 2, '.', '');
       
          $update_data=Seller::find($val->id);
          $update_data->admin_to_pay += $final_admin_to_pay;
          $update_data->save();
        }
    }
}
