<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && (Auth::user()->user_type == 'customer' || Auth::user()->user_type == 'seller')) {
            return $next($request);
        }
        else{
            session(['link' => url()->current()]);
          $subDomain = request()->getHttpHost();

            if ($subDomain == env('APP_SUBDOMAIN')) {
                return redirect()->route('login');
            }else{
                return redirect()->route('user.login');
            }
        }
    }
}
