<?php

namespace App\Http\Middleware;

use App\Models\Seller;
use Closure;
use Illuminate\Support\Facades\Auth;

class IsSeller
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    { 
        if (Auth::check() ) {
            if(Auth::user()->user_type == 'seller'){
                $verification_status=Seller::where('user_id',Auth::user()->id)->value('verification_status');
                if($verification_status==1){
                    $next($request);
                }
                else{
                  //  echo 3;die;
                    return redirect(route('shops.index'));
                }
            }
        else{
            abort(404);
        }
           
        }
        else{
            $subDomain = request()->getHttpHost();
     
           
                return redirect(route('login'));

           
           
        }

          
        $response = $next($request);
        return $response->header('Cache-Control','nocache, no-store, max-age=0, must-revalidate')
            ->header('Pragma','no-cache')
            ->header('Expires','Sun, 02 Jan 1990 00:00:00 GMT');
    }
}
