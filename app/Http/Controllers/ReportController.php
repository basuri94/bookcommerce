<?php

namespace App\Http\Controllers;

use App\CuriorRefund;
use App\Exports\CouponReport;
use App\Exports\RefundReport;
use App\Exports\SalesExport;
use App\Exports\VendorExport;
use App\Exports\ReferralReport;
use App\Exports\CustomerList;
use App\Exports\SellerList;
use App\Exports\SalesCommision;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Payment;
use Illuminate\Http\Request;
use App\Product;
use App\Refers;
use App\RefundRequest;
use App\Seller;
use App\SubCategoryHsn;
use App\User;
use App\Wallet;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;


class ReportController extends Controller
{
    public function stock_report(Request $request)
    {
        if ($request->has('category_id')) {
            $products = Product::where('category_id', $request->category_id)->get();
        } else {
            $products = Product::all();
        }
        return view('reports.stock_report', compact('products'));
    }

    public function in_house_sale_report(Request $request)
    {
        if ($request->has('category_id')) {
            $products = Product::where('category_id', $request->category_id)->orderBy('num_of_sale', 'desc')->get();
        } else {
            $products = Product::orderBy('num_of_sale', 'desc')->get();
        }
        return view('reports.in_house_sale_report', compact('products'));
    }

    public function seller_report(Request $request)
    {
        if ($request->has('verification_status')) {
            $sellers = Seller::where('verification_status', $request->verification_status)->get();
        } else {
            $sellers = Seller::all();
        }
        return view('reports.seller_report', compact('sellers'));
    }

    public function seller_sale_report(Request $request)
    {
        if ($request->has('verification_status')) {
            $sellers = Seller::where('verification_status', $request->verification_status)->get();
        } else {
            $sellers = Seller::all();
        }
        return view('reports.seller_sale_report', compact('sellers'));
    }

    public function wish_report(Request $request)
    {
        if ($request->has('category_id')) {
            $products = Product::where('category_id', $request->category_id)->get();
        } else {
            $products = Product::all();
        }
        return view('reports.wish_report', compact('products'));
    }
    public function sales_budget(Request $request)
    {


        $orderDetails = OrderDetail::with('order', 'product')->where('payment_status', 'paid');



        if (!empty($request->to_date)) {
            $to_date = date('Y-m-d', strtotime(trim(str_replace('/', '-', $request->to_date))));
            $orderDetails = $orderDetails->where(DB::raw("DATE(order_details.created_at)"), '<=', DB::raw("DATE('" . $to_date . "')"));
        }
        if (!empty($request->from_date)) {
            $from_date = date('Y-m-d', strtotime(trim(str_replace('/', '-', $request->from_date))));
            $orderDetails = $orderDetails->where(DB::raw("DATE(order_details.created_at)"), '>=', DB::raw("DATE('" . $from_date . "')"));
        }

        $orderDetails = $orderDetails->orderBy('order_details.id','desc')->get();
        $data = array();
        
        foreach ($orderDetails as $key => $val) {
            $tcs=0;
            $tds=0;
            $totalFee=0;
            $gatewayCharge=0;
            $walletAmount=0;
            $finalWalletAmount=0;
            if(isset($val->product->user)){
            $courierChargeValue = 0;

            $totalCourierRefundOrReplaceAmount = 0;

            $nested['created_date'] = date('d/m/Y', $val->order->date);
            $nested['invoice'] = $val->order->code;
            $nested['vendor_name'] = $val->product->user->name;
            $nested['gstno'] = $val->product->user->seller->gst;
            $total_price_includegst = $val->price + $val->tax;
            $courierCharge = DB::table('curior_orders')->where('curior_orders.id', $val->curior_order_id)->first();

            $nested['sale_without_tax'] = $val->price +  $val->shipping_cost;

            $nested['sale_with_tax'] = $total_price_includegst +  $val->shipping_cost;

            $hsnid = $val->product->hsn_id;

            $nested['gst'] =  $val->tax;

            $commission_percentage = $val->product->subcategory->commision_rate;
            $nested['commission'] = number_format((float)$commission_percentage * $val->price / 100, 2, '.', '');
            $seller = $val->product->user->seller;

            $OrderCharge = Order::where('id', $val->order_id)->first();
            if(  $OrderCharge->payment_details!=null){
            if(!empty($OrderCharge->payment_details)){
                $gatewayCharge = ((json_decode($OrderCharge->payment_details)->fee) / 100) / $OrderCharge->grand_total *    ($val->tax + $val->price + $val->shipping_cost);
            }
            }
           
            $actual_gatewaycharge = number_format((float)$gatewayCharge, 2, '.', '');

            $refunddata = RefundRequest::where('status', 'Approved')->where('order_id', $val->id)->first();
            if (!empty($refunddata)) {
                if ($refunddata->type == "Return") {
                    $refundAmount = $refunddata->amount;
                    $courierRefundAmount = CuriorRefund::where('refund_request_id', $refunddata->id)->where('status', 1)->value('curior_charge');
                    $totalCourierRefundOrReplaceAmount = $refundAmount + $courierRefundAmount;
                } else if ($refunddata->type == "Replacement") {
                    $totalReplaceAmount = CuriorRefund::where('refund_request_id', $refunddata->id)->where('status', 1)->sum('curior_charge');
                    $totalCourierRefundOrReplaceAmount = $totalReplaceAmount;
                }
            }


            $gst_in_commisssion = ((($val->price * $commission_percentage) / 100) * 18 / 100) + (($val->price * $commission_percentage) / 100);

            if (!empty($courierCharge->curior_charge)) {

                $courierChargeValue = ($courierCharge->curior_charge) / $OrderCharge->grand_total *    ($val->tax + $val->price + $val->shipping_cost);
                $courierChargeValue = number_format((float)$courierChargeValue, 2, '.', '');
            }
            if($OrderCharge->wallet_amount!="0.00"){
             
                $walletData=Wallet::where('payment_method','razorpay')->where('user_id',$OrderCharge->user_id)->get();
                foreach( $walletData as $walletVal){
               
                    if (!empty(json_decode($walletVal->payment_details)->fee)) {
                        $totalFee +=(json_decode($walletVal->payment_details)->fee);
                      }
                     $walletAmount +=$walletVal->amount;
                   
                    
                }
                if($totalFee!=0){
                    $convertTotalFee= $totalFee/100;
                    $WalletAmountCalculate=($convertTotalFee / $walletAmount) *$OrderCharge->wallet_amount;
                    $finalWalletAmount =  $WalletAmountCalculate / $OrderCharge->grand_total *     ($val->tax + $val->price + $val->shipping_cost);;
                  
                }
                
            }
          
           $tcs= ($val->price)*0.01;
           $tds= ($val->price)*0.01;
            $vendor_charges = $courierChargeValue + $actual_gatewaycharge + $gst_in_commisssion+$finalWalletAmount;

            $admin_to_pay = $total_price_includegst - $vendor_charges - $totalCourierRefundOrReplaceAmount- $tcs-$tds;
          //  $nested['brake_amount']='Total Amount:- '.$total_price_includegst.'Vendor Charges '.$vendor_charges.', Refund/Replace Amount:- '.$totalCourierRefundOrReplaceAmount.', Wallet Charges:- ,'. number_format((float)$finalWalletAmount, 2, '.', '');
           // $nested['vendor_charges']='Courier Charge:- '.$courierChargeValue.'Gateway Charge:- '.$actual_gatewaycharge.'Commission:- '.$gst_in_commisssion;
            $nested['amount_pay'] = number_format((float)$admin_to_pay, 2, '.', '');
            $data[] = $nested;
            }
        }

        return view('reports.sales_budget', compact('data'));
    }

    public function sales_budget_export(Request $request)
    {
        $file = 'Sales_Budget' . date('d/m/Y H:i:s') . 'xlsx';
        return Excel::download(new SalesExport($request->from_date, $request->to_date), 'sales_budget.xlsx');

       
    }


    public function sales_vendor()
    {
        $sellers = Seller::with('user')->get();
        $data = array();
        foreach ($sellers as $key => $val) {
            $totalCommissionPercentage = 0;
            $totalGateWayCharge = 0;
            $totalCourierRefundOrReplaceAmount = 0;
            $totalCourierCharge = 0;
            $walletCharge = 0;
             $tcs=0;
             $tds=0;
            $totalfinalWalletAmount=0;
            $price = OrderDetail::where('payment_status', 'paid')
                ->where('seller_id', $val->user_id)->sum('price');
            $tax = OrderDetail::where('payment_status', 'paid')
                ->where('seller_id', $val->user_id)->sum('tax');
            $shipping_cost = OrderDetail::where('payment_status', 'paid')
                ->where('seller_id', $val->user_id)->sum('shipping_cost');

            $total_price_includegst = $price + $tax;

            $nested['user_id'] = $val->user_id;
            $nested['vendor_name'] = $val->user->name;
            $nested['gstno'] = $val->gst;

            $courierCharge = DB::table('curior_orders')->join('order_details', 'order_details.curior_order_id', '=', 'curior_orders.id')
                ->where('curior_orders.seller_id', $val->user_id)->sum('curior_charge');

            $nested['sale_without_tax'] = $price +  $shipping_cost;

            $nested['sale_with_tax'] = $price + $tax + $shipping_cost;
            $nested['tax'] =  $tax;
            $OrderDetail = OrderDetail::where('seller_id', $val->user_id)->where('payment_status', 'paid')->get();
          
            foreach ($OrderDetail as $key => $OrderDetailVal) {
              
                $finalWalletAmount=0;
                $calculateCommission=0;
                $totalFee=0;
                $gatewayCharge=0;
                $walletAmount=0;
                if(!empty($OrderDetailVal->product)){
                    $OrderCharge = Order::where('id', $OrderDetailVal->order_id)->first();
                    if(!empty($OrderCharge->payment_details)){
                        $gatewayCharge = ((json_decode($OrderCharge->payment_details)->fee) / 100) / $OrderCharge->grand_total *    ($OrderDetailVal->tax + $OrderDetailVal->price + $OrderDetailVal->shipping_cost);
                    }
                   
    
                    $calculateCommission = (($OrderDetailVal->product->subcategory->commision_rate) * $OrderDetailVal->price) / 100;
                    $refunddata = RefundRequest::where('status', 'Approved')->where('order_id', $OrderDetailVal->id)->first();
                    if (!empty($refunddata)) {
                        if ($refunddata->type == "Return") {
                            $refundAmount = $refunddata->amount;
                            $courierRefundAmount = CuriorRefund::where('refund_request_id', $refunddata->id)->where('status', 1)->value('curior_charge');
                            $totalCourierRefundOrReplaceAmount = $totalCourierRefundOrReplaceAmount + $refundAmount + $courierRefundAmount;
                        } else if ($refunddata->type == "Replacement") {
                            $totalReplaceAmount = CuriorRefund::where('refund_request_id', $refunddata->id)->where('status', 1)->sum('curior_charge');
                            $totalCourierRefundOrReplaceAmount = $totalCourierRefundOrReplaceAmount + $totalReplaceAmount;
                        }
                    }
                    $courierCharge = DB::table('curior_orders')->where('curior_orders.id', $OrderDetailVal->curior_order_id)->first();
                    if (!empty($courierCharge->curior_charge)) {
    
                        $courierChargeValue = ($courierCharge->curior_charge) / $OrderCharge->grand_total *    ($OrderDetailVal->tax + $OrderDetailVal->price + $OrderDetailVal->shipping_cost);
                        $totalCourierCharge = $totalCourierCharge + $courierChargeValue;
                    }
                   
                  if($OrderCharge->wallet_amount!="0.00"){
                 
                    $walletData=Wallet::where('payment_method','razorpay')->where('user_id',$OrderCharge->user_id)->get();
                    foreach( $walletData as $walletVal){
                   
                        if (!empty(json_decode($walletVal->payment_details)->fee)) {
                            $totalFee +=(json_decode($walletVal->payment_details)->fee);
                          }
                         $walletAmount +=$walletVal->amount;
                       
                        
                    }
                    if($totalFee!=0){
                        $convertTotalFee= $totalFee/100;
                        $WalletAmountCalculate=($convertTotalFee / $walletAmount) *$OrderCharge->wallet_amount;
                        $finalWalletAmount =  $WalletAmountCalculate / $OrderCharge->grand_total *     ($OrderDetailVal->tax + $OrderDetailVal->price + $OrderDetailVal->shipping_cost);;
                      
                    }
                   
                }
              
                    $totalGateWayCharge = $totalGateWayCharge + $gatewayCharge;
                    $totalCommissionPercentage = $calculateCommission + $totalCommissionPercentage;
                    $totalfinalWalletAmount +=$finalWalletAmount;
                }
              
               
            }
       
            $actual_gatewaycharge = number_format((float)$totalGateWayCharge, 2, '.', '');

            $gst_in_commisssion = (($totalCommissionPercentage * 18) / 100) + $totalCommissionPercentage;
            $tcs= ($price)*0.01;
            $tds=($price)*0.01;
            $vendor_charges = $totalCourierCharge + $actual_gatewaycharge + $gst_in_commisssion;
           // $nested['brake_amount']='Total Amount:- '.$total_price_includegst.'Vendor Charges '.$vendor_charges.', Refund/Replace Amount:- '.$totalCourierRefundOrReplaceAmount.', Wallet Charges:- ,'. number_format((float)$totalfinalWalletAmount, 2, '.', '');
          // $nested['vendor_charges']='Courier Charge:- '.$totalCourierCharge.'Gateway Charge:- '.$actual_gatewaycharge.'Commission:- '.$gst_in_commisssion;
           $admin_to_pay = $total_price_includegst - $vendor_charges - $totalCourierRefundOrReplaceAmount-$totalfinalWalletAmount-$tcs-$tds;




            $nested['commission_charges'] = number_format((float)$totalCommissionPercentage, 2, '.', '');
            $payment = Payment::where('seller_id', $val->id)->sum('amount');
            $nested['amount_paid'] = $payment;

            $nested['amount_due'] = number_format((float)$admin_to_pay, 2, '.', '');
            $data[] = $nested;
        }

        return view('reports.vendor_report', compact('data'));
    }

    public function sales_vandor_export()
    {
        $file = 'Sales_Budget' . date('d/m/Y H:i:s') . 'xlsx';
        return Excel::download(new VendorExport, 'invoices.xlsx');
    }

    public function sales_vendor_record($id)
    {
        $decrypt_sellerid = decrypt($id);
        $orderDetails = OrderDetail::with('order', 'product')->where('payment_status', 'paid')->where('seller_id', $decrypt_sellerid);

        $orderDetails = $orderDetails->orderBy('order_details.id','desc')->get();
        $data = array();
        foreach ($orderDetails as $key => $val) {
            $courierChargeValue = 0;
            $actual_gatewaycharge=0;
            $totalCourierRefundOrReplaceAmount = 0;
            $totalFee=0;
            $gatewayCharge=0;
            $walletAmount=0;
            $finalWalletAmount=0;
$tcs=0;
$tds=0;
            $nested['created_date'] = date('d/m/Y', $val->order->date);
            $nested['invoice'] = $val->order->code;
            $nested['vendor_name'] = $val->product->user->name;
            $nested['gstno'] = $val->product->user->seller->gst;
            $total_price_includegst = $val->price + $val->tax;
            $courierCharge = DB::table('curior_orders')->where('curior_orders.id', $val->curior_order_id)->first();

            $nested['sale_without_tax'] = $val->price +  $val->shipping_cost;

            $nested['sale_with_tax'] = $total_price_includegst +  $val->shipping_cost;

            $hsnid = $val->product->hsn_id;

            $nested['gst'] =  $val->tax;

            $commission_percentage = $val->product->subcategory->commision_rate;
            $nested['commission'] = number_format((float)$commission_percentage * $val->price / 100, 2, '.', '');
            $seller = $val->product->user->seller;

            $OrderCharge = Order::where('id', $val->order_id)->first();

            if(  $OrderCharge->payment_details!=null){
                if(!empty($OrderCharge->payment_details)){
                $gatewayCharge = ((json_decode($OrderCharge->payment_details)->fee) / 100) / $OrderCharge->grand_total *    ($val->tax + $val->price + $val->shipping_cost);
                $actual_gatewaycharge = number_format((float)$gatewayCharge, 2, '.', '');
    }
    }

            $refunddata = RefundRequest::where('status', 'Approved')->where('order_id', $val->id)->first();
            if (!empty($refunddata)) {
                if ($refunddata->type == "Return") {
                    $refundAmount = $refunddata->amount;
                    $courierRefundAmount = CuriorRefund::where('refund_request_id', $refunddata->id)->where('status', 1)->value('curior_charge');
                    $totalCourierRefundOrReplaceAmount = $refundAmount + $courierRefundAmount;
                } else if ($refunddata->type == "Replacement") {
                    $totalReplaceAmount = CuriorRefund::where('refund_request_id', $refunddata->id)->where('status', 1)->sum('curior_charge');
                    $totalCourierRefundOrReplaceAmount = $totalReplaceAmount;
                }
            }


            $gst_in_commisssion = ((($val->price * $commission_percentage) / 100) * 18 / 100) + (($val->price * $commission_percentage) / 100);

            if (!empty($courierCharge->curior_charge)) {

                $courierChargeValue = ($courierCharge->curior_charge) / $OrderCharge->grand_total *    ($val->tax + $val->price + $val->shipping_cost);
                $courierChargeValue = number_format((float)$courierChargeValue, 2, '.', '');
            }
            if($OrderCharge->wallet_amount!="0.00"){
             
                $walletData=Wallet::where('payment_method','razorpay')->where('user_id',$OrderCharge->user_id)->get();
                foreach( $walletData as $walletVal){
               
                    if (!empty(json_decode($walletVal->payment_details)->fee)) {
                        $totalFee +=(json_decode($walletVal->payment_details)->fee);
                      }
                     $walletAmount +=$walletVal->amount;
                   
                    
                }
                if($totalFee!=0){
                    $convertTotalFee= $totalFee/100;
                    $WalletAmountCalculate=($convertTotalFee / $walletAmount) *$OrderCharge->wallet_amount;
                    $finalWalletAmount =  $WalletAmountCalculate / $OrderCharge->grand_total *     ($val->tax + $val->price + $val->shipping_cost);;
                  
                }
                
            }

            $vendor_charges = $courierChargeValue + $actual_gatewaycharge + $gst_in_commisssion;
            $tcs= ($val->price)*0.01;
            $tds= ($val->price)*0.01;
            $admin_to_pay = $total_price_includegst - $vendor_charges - $totalCourierRefundOrReplaceAmount-$tcs-$tds;
            $nested['amount_pay'] = number_format((float)$admin_to_pay, 2, '.', '');
            $data[] = $nested;
        
        }
        return view('reports.sales_vendor_record', compact('data'));
    }
    public function sales_vendor_commision(Request $request)
    {
        $decrypt_sellerid = Auth::user()->id;
        $orderDetails = OrderDetail::with('order','product')->where('payment_status', 'paid')->where('seller_id', $decrypt_sellerid);
        if (!empty($request->to_date)) {
            $to_date = date('Y-m-d', strtotime(trim(str_replace('/', '-', $request->to_date))));
            $orderDetails = $orderDetails->where(DB::raw("DATE(order_details.created_at)"), '<=', DB::raw("DATE('" . $to_date . "')"));
        }
        if (!empty($request->from_date)) {
            $from_date = date('Y-m-d', strtotime(trim(str_replace('/', '-', $request->from_date))));
            $orderDetails = $orderDetails->where(DB::raw("DATE(order_details.created_at)"), '>=', DB::raw("DATE('" . $from_date . "')"));
        }
        $orderDetails = $orderDetails->orderBy('order_details.id','desc')->get();
        $data = array();
        //echo json_encode($orderDetails);die;
        foreach ($orderDetails as $key => $val) {
            $courierChargeValue = 0;
            $actual_gatewaycharge=0;
            $totalCourierRefundOrReplaceAmount = 0;
            $refundAmount=0;
            $courierRefundAmount=0;
            $totalReplaceAmount=0;
            $totalFee=0;
            $gatewayCharge=0;
            $walletAmount=0;
            $finalWalletAmount=0;
$tcs=0;
$tds=0;
            $nested['created_date'] = date('d/m/Y', $val->order->date);
            $nested['invoice'] = $val->order->code;
            $nested['vendor_name'] = $val->product->user->name;
            $nested['gstno'] = $val->product->user->seller->gst;
            $total_price_includegst = $val->price + $val->tax;
            $courierCharge = DB::table('curior_orders')->where('curior_orders.id', $val->curior_order_id)->first();

            $nested['sale_without_tax'] = $val->price +  $val->shipping_cost;

            $nested['sale_with_tax'] = $total_price_includegst +  $val->shipping_cost;

            $hsnid = $val->product->hsn_id;

            $nested['gst'] =  $val->tax;

            $commission_percentage = $val->product->subcategory->commision_rate;
            $nested['commission'] = number_format((float)$commission_percentage * $val->price / 100, 2, '.', '');
            $seller = $val->product->user->seller;

            $OrderCharge = Order::where('id', $val->order_id)->first();

            if(  $OrderCharge->payment_details!=null){
                if(!empty($OrderCharge->payment_details)){
                $gatewayCharge = ((json_decode($OrderCharge->payment_details)->fee) / 100) / $OrderCharge->grand_total *    ($val->tax + $val->price + $val->shipping_cost);
                $actual_gatewaycharge = number_format((float)$gatewayCharge, 2, '.', '');
    }
    }

            $refunddata = RefundRequest::where('status', 'Approved')->where('order_id', $val->id)->first();
            if (!empty($refunddata)) {
                if ($refunddata->type == "Return") {
                    $refundAmount = $refunddata->amount;
                    $courierRefundAmount = CuriorRefund::where('refund_request_id', $refunddata->id)->where('status', 1)->value('curior_charge');
                    $totalCourierRefundOrReplaceAmount = $refundAmount + $courierRefundAmount;
                } else if ($refunddata->type == "Replacement") {
                    $totalReplaceAmount = CuriorRefund::where('refund_request_id', $refunddata->id)->where('status', 1)->sum('curior_charge');
                    $totalCourierRefundOrReplaceAmount = $totalReplaceAmount;
                }
            }


            $gst_in_commisssion = ((($val->price * $commission_percentage) / 100) * 18 / 100) + (($val->price * $commission_percentage) / 100);

            if (!empty($courierCharge->curior_charge)) {

                $courierChargeValue = ($courierCharge->curior_charge) / $OrderCharge->grand_total *    ($val->tax + $val->price + $val->shipping_cost);
                $courierChargeValue = number_format((float)$courierChargeValue, 2, '.', '');
            }

            if($OrderCharge->wallet_amount!="0.00"){
             
                $walletData=Wallet::where('payment_method','razorpay')->where('user_id',$OrderCharge->user_id)->get();
                foreach( $walletData as $walletVal){
               
                    if (!empty(json_decode($walletVal->payment_details)->fee)) {
                        $totalFee +=(json_decode($walletVal->payment_details)->fee);
                      }
                     $walletAmount +=$walletVal->amount;
                   
                    
                }
                if($totalFee!=0){
                    $convertTotalFee= $totalFee/100;
                    $WalletAmountCalculate=($convertTotalFee / $walletAmount) *$OrderCharge->wallet_amount;
                    $finalWalletAmount =  $WalletAmountCalculate / $OrderCharge->grand_total *     ($val->tax + $val->price + $val->shipping_cost);;
                  
                }
                
            }

            $vendor_charges = $courierChargeValue + $actual_gatewaycharge + $gst_in_commisssion;
            $tcs= ($val->price)*0.01;
            $tds= ($val->price)*0.01;
            $nested['return_amount'] = number_format((float)$refundAmount, 2, '.', '');
            $curior=$courierChargeValue+$courierRefundAmount+$totalReplaceAmount;
            $nested['curior_charge'] = number_format((float)$curior, 2, '.', '');
            $nested['tcs'] = number_format((float)$tcs, 2, '.', '');
            $nested['tds'] =number_format((float)$tds, 2, '.', ''); 
            $prcessing_fee=$actual_gatewaycharge+$finalWalletAmount;
            $nested['payment_handling'] = number_format((float)$prcessing_fee, 2, '.', '');
            $nested['payment_commision'] =number_format((float)$gst_in_commisssion, 2, '.', ''); 
            $admin_to_pay = $total_price_includegst - $vendor_charges - $totalCourierRefundOrReplaceAmount-$tcs-$tds;
            $nested['amount_pay'] = number_format((float)$admin_to_pay, 2, '.', '');
            $data[] = $nested;
        
        }
        return view('frontend.seller.sales_vendor_commision', compact('data'));
    }
    public function sales_comision_export(Request $request)
    {
        $file = 'Sales_commision' . date('d/m/Y H:i:s') . 'xlsx';
        return Excel::download(new SalesCommision($request->from_date, $request->to_date), 'Sales_commision.xlsx');

       
    }
    public function pay_to_seller()
    {


        $orderDetails = OrderDetail::with('order', 'product')->where('payment_status', 'paid')->get();
        $data = array();
        foreach ($orderDetails as $key => $val) {
            $courierChargeValue = 0;
            
            $nested['created_date'] = date('d/m/Y', $val->order->date);

            $nested['invoice'] = $val->order->code;
            $nested['vendor_name'] = $val->product->user->name;
            $nested['gstno'] = $val->product->user->seller->gst;
            $courierCharge = DB::table('curior_orders')->join('order_details', 'order_details.curior_order_id', '=', 'curior_orders.id')->where('curior_orders.id', $val->curior_order_id)->first();

            if (!empty($courierCharge->curior_charge)) {
                $courierChargeValue = $courierCharge->curior_charge;
            }
            $nested['sale_without_tax'] = $val->price +   $courierChargeValue;

            $nested['sale_with_tax'] = $val->price + $val->tax +   $courierChargeValue;

            $hsnid = $val->product->hsn_id;
            $gst_amount = SubCategoryHsn::where('id', $hsnid)->value('gst_amount');
            $nested['gst'] = $gst_amount;

            $commission_percentage = $val->product->subcategory->commision_rate;
            $nested['commission'] = $commission_percentage;
            $seller = $val->product->user->seller;
            $OrderCharge = Order::where('id', $val->order_id)->first();
            $total_ammount = ($val->price * (100 - $commission_percentage)) / 100  + $val->tax + $val->shipping_cost;
            $gatewayCharge = (json_decode($OrderCharge->payment_details)->fee) / 100;
            $order_amount = (json_decode($OrderCharge->payment_details)->amount) / 100;
            $gatewayCharge = $gatewayCharge * $total_ammount / $order_amount;
            $minus_amount = ($val->price * (1) / 100) + $gatewayCharge;
            //echo  $minus_amount;die;
            $admin_to_pay =  ($val->price * (100 - $commission_percentage)) / 100  + $val->tax + $val->shipping_cost - $minus_amount;
            $seller = $OrderCharge->product->user->seller;
            $seller->admin_to_pay = $seller->admin_to_pay + $admin_to_pay;
            $seller->save();
            // $nested['amount_pay'] = number_format((float)$admin_to_pay, 2, '.', '');;
            // $data[] = $nested;
        }
        // return view('reports.sales_budget', compact('data'));



        // return Excel::create('General_Student_Details', function($excel) use ($data) {
        //     $excel->sheet(' ', function($sheet) use ($data) {
        //         $sheet->fromArray($data);
        //     });
        // })->download('xls');
    }

    public function sales_coupon(Request $request){
        $data=array();
        $order_coupon=Order::where('coupon_discount','!=', '0.00');
        
        if (!empty($request->to_date)) {
            $to_date = date('Y-m-d', strtotime(trim(str_replace('/', '-', $request->to_date))));
            $order_coupon = $order_coupon->where(DB::raw("DATE(orders.created_at)"), '<=', DB::raw("DATE('" . $to_date . "')"));
        }
        if (!empty($request->from_date)) {
            $from_date = date('Y-m-d', strtotime(trim(str_replace('/', '-', $request->from_date))));
            $order_coupon = $order_coupon->where(DB::raw("DATE(orders.created_at)"), '>=', DB::raw("DATE('" . $from_date . "')"));
        }
        $order_coupon=  $order_coupon->get();
        foreach ($order_coupon as $key => $val) {
          $nested['created_date']=date('d/m/Y', strtotime(trim(str_replace('-', '/', $val->created_at))));
          $nested['user_name']= $val->user->name;
          $nested['invoice']= $val->code;
          $nested['coupon_discount']= $val->coupon_discount;
          $data[]=$nested;
        }
        return view('reports.sales_coupon', compact('data'));
    }

    public function sales_coupon_export(Request $request)
    {
        $file = 'Sales_Budget' . date('d/m/Y H:i:s') . 'xlsx';
        return Excel::download(new CouponReport($request->from_date, $request->to_date), 'sales_coupon.xlsx');
    }

    public function saler_refund(Request $request){
        $allrefund = RefundRequest::orderBy('refund_request.id', 'desc')
        ->leftjoin('order_details', 'order_details.id', '=', 'refund_request.order_id')
        ->leftjoin('orders', 'orders.id', '=', 'order_details.order_id')
        ->leftjoin('users as seller_user', 'seller_user.id', '=', 'order_details.seller_id')
        ->leftjoin('users as customer_user', 'customer_user.id', '=', 'orders.user_id')
            ->leftjoin('products', 'products.id', '=', 'refund_request.product_id')
            ->where('products.user_id','!=', \Auth::user()->id)
            ->select('refund_request.*', 'products.name', 'orders.code','seller_user.name as seller_name','customer_user.name as customer_name');

            if (!empty($request->to_date)) {
                $to_date = date('Y-m-d', strtotime(trim(str_replace('/', '-', $request->to_date))));
                $allrefund = $allrefund->where(DB::raw("DATE(refund_request.created_at)"), '<=', DB::raw("DATE('" . $to_date . "')"));
            }
            if (!empty($request->from_date)) {
                $from_date = date('Y-m-d', strtotime(trim(str_replace('/', '-', $request->from_date))));
                $order_coupon = $allrefund->where(DB::raw("DATE(refund_request.created_at)"), '>=', DB::raw("DATE('" . $from_date . "')"));
            }
            $allrefund=  $allrefund->get();
            $data=array();
            foreach ($allrefund as $key => $val) {
              $nested['created_date']=date('d/m/Y', strtotime(trim(str_replace('-', '/', $val->created_at))));
              $nested['seller_name']= $val->seller_name;
              $nested['customer_name']= $val->customer_name;
              $nested['invoice']= $val->code;
              $nested['amount']= $val->amount;
              $nested['product_name']= $val->name;
              $nested['status']= $val->status;
              $data[]=$nested;
            }
            //return $data;
             return view('reports.saler_refund', compact('data'));
    }
    public function sellerrefund_export(Request $request){
     
            $file = 'seler_refund_report' . date('d/m/Y H:i:s') . 'xlsx';
            return Excel::download(new RefundReport($request->from_date, $request->to_date), 'refund_report.xlsx');
        }


        public function referral_report(Request $request){
            $data=array();
            $refer_wallet=Wallet::where('payment_method','=', 'Refer');
            
            if (!empty($request->to_date)) {
                $to_date = date('Y-m-d', strtotime(trim(str_replace('/', '-', $request->to_date))));
                $refer_wallet = $refer_wallet->where(DB::raw("DATE(wallets.created_at)"), '<=', DB::raw("DATE('" . $to_date . "')"));
            }
            if (!empty($request->from_date)) {
                $from_date = date('Y-m-d', strtotime(trim(str_replace('/', '-', $request->from_date))));
                $refer_wallet = $refer_wallet->where(DB::raw("DATE(wallets.created_at)"), '>=', DB::raw("DATE('" . $from_date . "')"));
            }
            $refer_wallet=  $refer_wallet->get();
            foreach ($refer_wallet as $key => $val) {
              $nested['created_date']=date('d/m/Y', strtotime(trim(str_replace('-', '/', $val->created_at))));
              $nested['customer_name']= $val->user->name;
               $nested['referred_by']= User::where('id',$val->user->referred_by)->value('name');
              $nested['refer_code']= User::where('id',$val->user->referred_by)->value('referral_code');
              $nested['amount']= $val->amount;
              $data[]=$nested;
            }
            return view('reports.referral', compact('data'));
        }

        public function referral_report_export(Request $request){
            $file = 'referral_report' . date('d/m/Y H:i:s') . 'xlsx';
            return Excel::download(new ReferralReport($request->from_date, $request->to_date), 'referral_report.xlsx');
        }
        public function customer_list_export(Request $request){
            $file = 'customer_list' . date('d/m/Y H:i:s') . 'xlsx';
            return Excel::download(new CustomerList($request->from_date, $request->to_date), 'customer_list.xlsx');
        }
        public function seller_list_export(Request $request){
            $file = 'seller_list' . date('d/m/Y H:i:s') . 'xlsx';
            return Excel::download(new SellerList($request->from_date, $request->to_date), 'seller_list.xlsx');
        }
}
