<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Category;
use App\Http\Controllers\PaypalController;
use App\Http\Controllers\InstamojoController;
use App\Http\Controllers\ClubPointController;
use App\Http\Controllers\StripePaymentController;
use App\Http\Controllers\PublicSslCommerzPaymentController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\AffiliateController;
use App\Http\Controllers\PaytmController;
use App\Order;
use App\BusinessSetting;
use App\Product;
use App\Coupon;
use App\CouponUsage;
use App\User;
use App\Address;
use App\Http\Controllers\Auth\RegisterController;
use App\Mail\WalletMail;

use App\Utility\PayhereUtility;
use Illuminate\Support\Facades\Mail;

use DB;
use PDF;
use App\Mail\InvoiceEmailManager;
use Illuminate\Support\Facades\Session;

class CheckoutController extends Controller
{

    public function __construct()
    {
        //
    }

    //check the selected payment gateway and redirect to that controller accordingly
    public function checkout(Request $request)
    {
        if ($request->payment_option != null) {

            $orderController = new OrderController;
            $orderController->store($request);

            $request->session()->put('payment_type', 'cart_payment');

            if($request->session()->get('order_id') != null){
                if($request->payment_option == 'paypal'){
                    $paypal = new PaypalController;
                    return $paypal->getCheckout();
                }
                elseif ($request->payment_option == 'stripe') {
                    $stripe = new StripePaymentController;
                    return $stripe->stripe();
                }
                elseif ($request->payment_option == 'sslcommerz') {
                    $sslcommerz = new PublicSslCommerzPaymentController;
                    return $sslcommerz->index($request);
                }
                elseif ($request->payment_option == 'instamojo') {
                    $instamojo = new InstamojoController;
                    return $instamojo->pay($request);
                }
                elseif ($request->payment_option == 'razorpay') {
                   $razorpay = new RazorpayController;
                    return $razorpay->payWithRazorpay($request);
                }
                elseif ($request->payment_option == 'paystack') {
                    $paystack = new PaystackController;
                    return $paystack->redirectToGateway($request);
                }
                elseif ($request->payment_option == 'voguepay') {
                    $voguePay = new VoguePayController;
                    return $voguePay->customer_showForm();
                }
                elseif ($request->payment_option == 'twocheckout') {
                    $twocheckout = new TwoCheckoutController;
                    return $twocheckout->index($request);
                }
                elseif ($request->payment_option == 'payhere') {
                    $order = Order::findOrFail($request->session()->get('order_id'));

                    $order_id = $order->id;
                    $amount = $order->grand_total;
                    $first_name = json_decode($order->shipping_address)->name;
                    $last_name = 'X';
                    $phone = json_decode($order->shipping_address)->phone;
                    $email = json_decode($order->shipping_address)->email;
                    $address = json_decode($order->shipping_address)->address;
                    $city = json_decode($order->shipping_address)->city;

                    return PayhereUtility::create_checkout_form($order_id, $amount, $first_name, $last_name, $phone, $email, $address, $city);
                }
                elseif ($request->payment_option == 'paytm') {
                    $paytm = new PaytmController;
                    return $paytm->index();
                }
                elseif ($request->payment_option == 'cash_on_delivery') {
                    $request->session()->put('cart', collect([]));
                    // $request->session()->forget('order_id');
                    $request->session()->forget('delivery_info');
                    $request->session()->forget('coupon_id');
                    $request->session()->forget('coupon_discount');
               $this->checkout_done($request,$request->session()->get('order_id'), null);
                    flash("Your order has been placed successfully")->success();
                	return redirect()->route('order_confirmed');
                }
                elseif ($request->payment_option == 'wallet') {
                   
                    $user = Auth::user();
                    $user->balance -= Order::findOrFail($request->session()->get('order_id'))->grand_total;
                    $user->save();
                    
                    $array['from'] = env('MAIL_USERNAME');
                    $array['name'] = $user->name;
                    $array['amount_credit'] = '';
                    $array['amount_debit'] = Order::findOrFail($request->session()->get('order_id'))->grand_total;
                    
                    Mail::to($user->email)->queue(new WalletMail($array));
                    $registerController= new RegisterController();
                    $data_base[0]['body'] = env("WALLET_DEBIT"); // Tests
            
                    $vars = array(
                        '{$name}'       => Auth::user()->name,
                        '{$amountdebit}'       => Order::findOrFail($request->session()->get('order_id'))->grand_total,
                       
                    );
                    $registerController->sms_function( $data_base[0]['body'],$vars, $user->phone_no);
                  

                    return $this->checkout_done($request,$request->session()->get('order_id'), null);
                }
                else{
                    $order = Order::findOrFail($request->session()->get('order_id'));
                    $order->manual_payment = 1;
                    $order->save();

                    $request->session()->put('cart', collect([]));
                    // $request->session()->forget('order_id');
                    $request->session()->forget('delivery_info');
                    $request->session()->forget('coupon_id');
                    $request->session()->forget('coupon_discount');

                    flash(__('Your order has been placed successfully. Please submit payment information from purchase history'))->success();
                	return redirect()->route('order_confirmed');
                }
            }
        }else {
            flash(__('Select Payment Option.'))->warning();
            return back();
        }
    }

    //redirects to this method after a successfull checkout
    public function checkout_done($request,$order_id, $payment)
    { 
        $order = Order::findOrFail($order_id);
        if(!empty($request->payment_option)){
            if($request->payment_option!= 'cash_on_delivery'){
            $order->payment_status = 'paid';
        }
    }
        $order->payment_details = $payment;
        $order->save();
        $admin_products=array();
        $seller_products = array();
         foreach (Session::get('cart') as $key => $cartItem) {
                $product = Product::find($cartItem['id']);

                if ($product->added_by == 'admin') {
                    array_push($admin_products, $cartItem['id']);
                } else {
                    $product_ids = array();
                    if (array_key_exists($product->user_id, $seller_products)) {
                        $product_ids = $seller_products[$product->user_id];
                    }
                    array_push($product_ids, $cartItem['id']);
                    $seller_products[$product->user_id] = $product_ids;
                }
}
 // stores the pdf for invoice
            $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true,
                'logOutputFile' => storage_path('logs/log.htm'),
                'tempDir' => storage_path('logs/')
            ])->loadView('invoices.customer_invoice', compact('order'));
           $output = $pdf->output();
            //file_put_contents('public/invoices/'.'Order#'.$order->code.'.pdf', $output);
            $path = public_path('invoices');
            $fileName = 'Order#' . $order->code . '.pdf';
              $fileName =str_replace("/","_",$fileName);
        $pdf->save($path . '/' . $fileName);

            $array['view'] = 'emails.invoice';
            $array['subject'] = 'Order Placed - ' . $order->code;
            $array['from'] = env('MAIL_USERNAME');
            $array['content'] = 'Hi.  A new order has been placed. Please check the attached invoice.';
            $array['file'] = public_path('invoices/' . $fileName);
            $array['file_name'] = $fileName;
          
           $array['order_code'] = $order->code;
              foreach ($seller_products as $key => $seller_product) {
                $array['name'] = \App\User::find($key)->name;
                try {
                    Mail::to(\App\User::find($key)->email)->queue(new InvoiceEmailManager($array));
                } catch (\Exception $e) {
                }
            }

     

            //sends email to customer with the invoice pdf attached
            if (env('MAIL_USERNAME') != null) {
                try {
                    $array['name'] = Auth::user()->name;
                    Mail::to(Session::get('shipping_info')['email'])->queue(new InvoiceEmailManager($array));
                    $array['name'] = User::where('user_type', 'admin')->first()->name;
                  
                    Mail::to(User::where('user_type', 'admin')->first()->email)->queue(new InvoiceEmailManager($array));
                } catch (\Exception $e) {
                }
            }

            if (file_exists($array['file'])) {
                unlink($array['file']);
            }
            $registerController= new RegisterController();
            $data_base[0]['body'] = env("ORDER_PLACED"); // Tests
                        
                                $vars = array(
                                    '{$ordercode}'       => $order->code,
                                   
                                   
                                );
                                $registerController->sms_function( $data_base[0]['body'],$vars, Auth::user()->phone_no);
        if (\App\Addon::where('unique_identifier', 'affiliate_system')->first() != null && \App\Addon::where('unique_identifier', 'affiliate_system')->first()->activated) {
            $affiliateController = new AffiliateController;
            $affiliateController->processAffiliatePoints($order);
        }

        if (\App\Addon::where('unique_identifier', 'club_point')->first() != null && \App\Addon::where('unique_identifier', 'club_point')->first()->activated) {
            $clubpointController = new ClubPointController;
            $clubpointController->processClubPoints($order);
        }

        if(\App\Addon::where('unique_identifier', 'seller_subscription')->first() == null || !\App\Addon::where('unique_identifier', 'seller_subscription')->first()->activated){
            // if (BusinessSetting::where('type', 'category_wise_commission')->first()->value != 1) {
            //     $commission_percentage = BusinessSetting::where('type', 'vendor_commission')->first()->value;
            //     foreach ($order->orderDetails as $key => $orderDetail) {
            //         $orderDetail->payment_status = 'paid';
            //         $orderDetail->save();
            //         if($orderDetail->product->user->user_type == 'seller'){
            //             $seller = $orderDetail->product->user->seller;
            //             $seller->admin_to_pay = $seller->admin_to_pay + ($orderDetail->price*(100-$commission_percentage))/100 + $orderDetail->tax + $orderDetail->shipping_cost;
            //             $seller->save();
            //         }
            //     }
            // }
          //  else{
                // foreach ($order->orderDetails as $key => $orderDetail) {
                //     $orderDetail->payment_status = 'paid';
                //     $orderDetail->save();
                //     if($orderDetail->product->user->user_type == 'seller'){
                //         $commission_percentage = $orderDetail->product->subcategory->commision_rate;
                //         $seller = $orderDetail->product->user->seller;
                //         $OrderCharge=Order::where('id',$order_id)->first();
                //         $gatewayCharge=json_decode($OrderCharge->payment_details)->fee;

                //         $minus_amount=($orderDetail->price*(1)/100)+$gatewayCharge;
                //         $seller->admin_to_pay = $seller->admin_to_pay + (($orderDetail->price*(100-$commission_percentage))/100  + $orderDetail->tax + $orderDetail->shipping_cost)-$minus_amount;
                //         $seller->save();
                //     }
                // }
          //  }
        }

        // $order->commission_calculated = 1;
        // $order->save();
        foreach ($order->orderDetails as $key => $orderDetail) {
            if(!empty($request->payment_option)){
            if($request->payment_option!= 'cash_on_delivery'){
             $orderDetail->payment_status = 'paid';
               }
            }
                
                $orderDetail->save();
             
            }
        Session::put('cart', collect([]));
        // Session::forget('order_id');
        Session::forget('payment_type');
        Session::forget('delivery_info');
        Session::forget('coupon_id');
        Session::forget('coupon_discount');
      Session::forget('wallet_amount_for_order');


        flash(__('Payment completed'))->success();
        return view('frontend.order_confirmed', compact('order'));
    }

    public function get_shipping_info(Request $request)
    {
        if(Session::has('cart') && count(Session::get('cart')) > 0){
            $categories = Category::all();
            return view('frontend.shipping_info', compact('categories'));
        }
        flash(__('Your cart is empty'))->success();
        return back();
    }

    public function store_shipping_info(Request $request)
    {
        if (Auth::check()) {
            if($request->address_id == null){
                flash("Please add shipping address")->warning();
                return back();
            }
            $address = Address::findOrFail($request->address_id);
            $data['name'] = Auth::user()->name;
            $data['email'] = Auth::user()->email;
            $data['address'] = $address->address;
            $data['country'] = $address->country;
            $data['city'] = $address->city;
            $data['postal_code'] = $address->postal_code;
            $data['phone'] = $address->phone;
            $data['checkout_type'] = $request->checkout_type;
        }
        else {
            $data['name'] = $request->name;
            $data['email'] = $request->email;
            $data['address'] = $request->address;
            $data['country'] = $request->country;
            $data['city'] = $request->city;
            $data['postal_code'] = $request->postal_code;
            $data['phone'] = $request->phone;
            $data['checkout_type'] = $request->checkout_type;
        }

        $shipping_info = $data;
        $request->session()->put('shipping_info', $shipping_info);

        $subtotal = 0;
        $tax = 0;
        $shipping = 0;
        foreach (Session::get('cart') as $key => $cartItem){
            $subtotal += $cartItem['price']*$cartItem['quantity'];
            $tax += $cartItem['tax']*$cartItem['quantity'];
            if (is_numeric($cartItem['shipping']) ) {
                $shipping += $cartItem['shipping']*$cartItem['quantity'];
                        } else {
                        // do some error handling...
                        }
           
        }

        $total = $subtotal + $tax + $shipping;

        if(Session::has('coupon_discount')){
                $total -= Session::get('coupon_discount');
        }
if (Session::has('cart') && count(Session::get('cart')) > 0) {
            $cart = $request->session()->get('cart', collect([]));
            $cart = $cart->map(function ($object, $key) use ($request) {
               $object['shipping_type'] = 'home_delivery';
                   
                return $object;
            });

            $request->session()->put('cart', $cart);
        }
        //return view('frontend.delivery_info');
         return view('frontend.payment_select', compact('total'));
    }

    public function store_delivery_info(Request $request)
    {
        if(Session::has('cart') && count(Session::get('cart')) > 0){
           
        $subtotal = 0;
        $tax = 0;
        $shipping = 0;
        foreach (Session::get('cart') as $key => $cartItem){
            $subtotal += $cartItem['price']*$cartItem['quantity'];
            $tax += $cartItem['tax']*$cartItem['quantity'];
            if (is_numeric($cartItem['shipping']) ) {
                $shipping += $cartItem['shipping']*$cartItem['quantity'];
                        } else {
                        // do some error handling...
                        }
           
        }

        $total = $subtotal + $tax + $shipping;

        if(Session::has('coupon_discount')){
                $total -= Session::get('coupon_discount');
        }
if (Session::has('cart') && count(Session::get('cart')) > 0) {
            $cart = $request->session()->get('cart', collect([]));
            $cart = $cart->map(function ($object, $key) use ($request) {
               $object['shipping_type'] = 'home_delivery';
                   
                return $object;
            });

            $request->session()->put('cart', $cart);
        }
        //return view('frontend.delivery_info');
         return view('frontend.payment_select', compact('total'));
        }
        else {
            flash('Your Cart was empty')->warning();
            return redirect()->route('home');
        }
    }

    public function get_payment_info(Request $request)
    {
        $subtotal = 0;
        $tax = 0;
        $shipping = 0;
        foreach (Session::get('cart') as $key => $cartItem){
            $subtotal += $cartItem['price']*$cartItem['quantity'];
            $tax += $cartItem['tax']*$cartItem['quantity'];
            $shipping += $cartItem['shipping']*$cartItem['quantity'];
        }

        $total = $subtotal + $tax + $shipping;

        if(Session::has('coupon_discount')){
                $total -= Session::get('coupon_discount');
        }

        return view('frontend.payment_select', compact('total'));
    }

    public function add_wallet_amount_to_order(Request $request){
        //dd($request->all());
              if($request->amount){
                            
                            $request->session()->put('wallet_amount_for_order', $request->amount + \Session::get('wallet_amount_for_order'));
                            flash('Wallet Amount has been applied to Order')->success();
                        }else{
                            flash('Please Choose Valid Amount!')->warning();
                        }
            //  return back();              
           // exit;      
       return redirect()->route('checkout.store_delivery_info');
    }
    public function remove_wallet_amount_to_order(Request $request){
        $request->session()->forget('wallet_amount_for_order');
        return back();
    }
    public function apply_coupon_code(Request $request){
        //dd($request->all());
        $coupon = Coupon::where('code', $request->code)->first();
//return (date('d-m-Y')) .'/'.$coupon->start_date;
        if($coupon != null){
            if(strtotime(date('d-m-Y')) >= $coupon->start_date && strtotime(date('d-m-Y')) <= $coupon->end_date){
                if(CouponUsage::where('user_id', Auth::user()->id)->where('coupon_id', $coupon->id)->first() == null){
                    $coupon_details = json_decode($coupon->details);

                    if ($coupon->type == 'cart_base')
                    {
                        $flag1=0;
                        $subtotal = 0;
                        $tax = 0;
                        $shipping = 0;
                        foreach (Session::get('cart') as $key => $cartItem)
                        {
                            $subtotal += $cartItem['price']*$cartItem['quantity'];
                            $tax += $cartItem['tax']*$cartItem['quantity'];
                            if (is_numeric($cartItem['shipping']) ) {
                                $shipping += $cartItem['shipping']*$cartItem['quantity'];
                                        } else {
                                        // do some error handling...
                                        }
                          
                        }
                        $sum = $subtotal+$tax+$shipping;

                        if ($sum >= $coupon_details->min_buy) {
                            $flag1=1;
                            if ($coupon->discount_type == 'percent') {
                                $coupon_discount =  ($sum * $coupon->discount)/100;
                                if ($coupon_discount > $coupon_details->max_discount) {
                                    $coupon_discount = $coupon_details->max_discount;
                                }
                            }
                            elseif ($coupon->discount_type == 'amount') {
                                $coupon_discount = $coupon->discount;
                            }
                            
                        }
                        if($flag1==1){
                            $request->session()->put('coupon_id', $coupon->id);
                            $request->session()->put('coupon_discount', $coupon_discount);
                            flash('Coupon has been applied')->success();
                        }else{
                            flash('Invalid coupon!')->warning();
                        }   

                    }
                    elseif ($coupon->type == 'product_base')
                    {
                        $coupon_discount = 0;
                        $flag=0;
                        foreach (Session::get('cart') as $key => $cartItem){
                            foreach ($coupon_details as $key => $coupon_detail) {
                                if($coupon_detail->product_id == $cartItem['id']){
                                     $subtotal = $cartItem['price'];
                            $tax = $cartItem['tax'];
                             if (is_numeric($cartItem['shipping']) ) {
                                $shipping = $cartItem['shipping'];
                                 $sum = $subtotal+$tax+$shipping;
                                        }else{
                                            $sum = $subtotal+$tax; 
                                        }
                                       
                                    $flag=1;
                                    if ($coupon->discount_type == 'percent') {
                                        $coupon_discount += $sum*$coupon->discount/100;
                                    }
                                    elseif ($coupon->discount_type == 'amount') {
                                        $coupon_discount += $coupon->discount;
                                    }
                                }
                            }
                        }
                        if($flag==1){
                            $request->session()->put('coupon_id', $coupon->id);
                            $request->session()->put('coupon_discount', $coupon_discount);
                            flash('Coupon has been applied')->success();
                        }else{
                            flash('Invalid coupon!')->warning();
                        }
                       
                    }
                }
                else{
                    flash('You already used this coupon!')->warning();
                }
            }
            else{
                flash('Coupon expired!')->warning();
            }
        }
        else {
            flash('Invalid coupon!')->warning();
        }
        return back();
    }

    public function remove_coupon_code(Request $request){
        $request->session()->forget('coupon_id');
        $request->session()->forget('coupon_discount');
        return back();
    }

    public function order_confirmed(){
        $order = Order::findOrFail(Session::get('order_id'));
        return view('frontend.order_confirmed', compact('order'));
    }
}
