<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Refers;
use CoreComponentRepository;

class RefersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //CoreComponentRepository::instantiateShopRepository();
        $refers = Refers::all();
        return view('refers.index', compact('refers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('refers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Refers = new refers;
        $Refers->referby_amount = $request->referby_amount;
        $Refers->getrefer_amount = $request->getrefer_amount;
        if($Refers->save()){
            flash(__('Refers has been inserted successfully'))->success();
            return redirect()->route('Refers.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $refers = Refers::findOrFail(decrypt($id));
        return view('refers.edit', compact('refers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Refers = Refers::findOrFail($id);
        $Refers->referby_amount = $request->referby_amount;
        $Refers->getrefer_amount = $request->getrefer_amount;
        if($Refers->save()){
            flash(__('Refers has been updated successfully'))->success();
            return redirect()->route('Refers.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { //echo $id;die;
        $Refers = Refers::findOrFail($id);
        if(Refers::destroy($id)){ //echo $id;die;
            flash(__('Refers has been deleted successfully'))->success();
            return redirect()->route('Refers.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }
}

