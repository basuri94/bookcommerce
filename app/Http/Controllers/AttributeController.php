<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attribute;
use App\Models\Category;
use App\Models\SubCategory;
use App\TblAttributes;
use CoreComponentRepository;

class AttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //CoreComponentRepository::instantiateShopRepository();
        $attributes = TblAttributes::join('attributes','attributes.id','=','tbl_attributes.attributeid')
        ->join('sub_categories','sub_categories.id','=','tbl_attributes.sub_categoryid')
        ->select('attributes.name','tbl_attributes.id','sub_categories.name as subname','attributes.mandatory')->get();
        return view('attribute.index', compact('attributes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Category=Category::all();
         return view('attribute.create',compact('Category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { //dd($request->all());
        $check=Attribute::where('name', 'like', '%'.$request->name.'%')->first();
        if(empty($check)){
            $attribute = new Attribute;
            $attribute->name = $request->name;
            if ($request->mandatory != null) {
                $attribute->mandatory = 1;
            }
            else {
                $attribute->mandatory = 0;
            }
            $attribute->save();
            $lastid=$attribute->id;
        }
        else{
            $lastid= $check->id;
        }
      $check2=TblAttributes::join('attributes','attributes.id','=','tbl_attributes.attributeid')
      ->where('sub_categoryid',$request->sub_categoryid)
      ->where('name', 'like', '%'.$request->name.'%')->count();
      if( $check2==0){
        $tbl_attribute=new TblAttributes();

        $tbl_attribute->sub_categoryid=$request->sub_categoryid;
        $tbl_attribute->categoryid=$request->categoryid;
        $tbl_attribute->attributeid= $lastid;
      
       
        
        if( $tbl_attribute->save()){
            flash(__('Attribute has been inserted successfully'))->success();
            return redirect()->route('attributes.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
      }
      else{
        flash(__('Duplicate entry not allow'))->error();
        return back();
      }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Category=Category::all();
        $attribute = Attribute::join('tbl_attributes','tbl_attributes.attributeid','=','attributes.id')
        ->join('sub_categories','sub_categories.id','=','tbl_attributes.sub_categoryid')
        ->where('tbl_attributes.id','=',decrypt($id))
        ->select('attributes.name','sub_categories.name as subname','tbl_attributes.*','attributes.mandatory')->first();
      
        return view('attribute.edit', compact('attribute','Category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attribute = Attribute::findOrFail($id);
        $attribute->name = $request->name;

        if ($request->mandatory != null) {
            $attribute->mandatory = 1;
        }
        else {
            $attribute->mandatory = 0;
        }
        $attribute->save();
        TblAttributes::where('attributeid',$id)->update([
            'sub_categoryid'=>$request->sub_categoryid,
            'categoryid'=>$request->categoryid,
        ]);

      

       
      
        if($attribute->save()){
            flash(__('Attribute has been updated successfully'))->success();
            return redirect()->route('attributes.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $attribute = TblAttributes::findOrFail($id);
        $attributeid= $attribute->attributeid;
    $checkCOunt=TblAttributes::where('attributeid', $attributeid)->count();
   
    if($checkCOunt==1){
        if(TblAttributes::destroy($id)){
            Attribute::destroy($attributeid);
            flash(__('Attribute has been deleted successfully'))->success();
            return redirect()->route('attributes.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }
    else{
        if(TblAttributes::destroy($id)){
           
            flash(__('Attribute has been deleted successfully'))->success();
            return redirect()->route('attributes.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }
        
    }

    public function mandatoryupdate(Request $request){
        $attribute = Attribute::findOrFail($request->id);
        $attribute->mandatory = $request->status;
        if($attribute->save()){
            
            return 1;
        }
        return 0;
    }
}
