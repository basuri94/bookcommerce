<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Hash;
use Illuminate\Support\Facades\Session;

class ForgetPasswordController extends Controller
{
    public function admingetforgetPassword(){
        Cookie::queue(Cookie::forget('OTP'));
        Cookie::queue(Cookie::forget('TIMEOUT'));
        return view('admin-forget-password');
    }

    public function adminpostforgetPassword(Request $request){
      
        $this->validate($request, [
            'mobile_no' => 'required',
        ], [
            'mobile_no.required' => 'Mobile no is required',
          

        ]);
        $phone = $request->mobile_no;
        $length = 5;
        $userData = User::where('phone_no', $phone)->where('user_type','admin')->first();
        if (!empty($userData)) {
            $otp = substr(str_shuffle(str_repeat($x = '0123456789', ceil($length / strlen($x)))), 2, $length);
            //$otp='654321';
            Cookie::queue('OTP', $otp, 43200);
            Cookie::queue('TIMEOUT', time() + (60 * 5));
            $data_base[0]['body'] =env("FORGET_PASSWORD_OTP"); 

             $vars = array(
               '{$otp}'       => $otp
             );
             
            $msg=strtr($data_base[0]['body'], $vars); 
            $apiKey = urlencode("PBnYEfajueU-ZLgRhC24TA3poA2CQ1TSzFMndmR29d");
            $sender = urlencode("LOVOCL");
            $message = rawurlencode($msg);
            $data = array("apikey" => $apiKey, "numbers" => $phone, "sender" => $sender, "message" => $message);
            $url = "https://api.textlocal.in/send/";

             $ch = curl_init();
             curl_setopt($ch, CURLOPT_URL, $url);
             curl_setopt($ch, CURLOPT_POST, true);
             curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
             $buffer = curl_exec($ch);
             curl_close($ch);
            
             if (empty($buffer)) {
                Session::flash('failure_message', 'Oops.. Something wrong. Try again!');
                Session::flash('alert-class', 'alert alert-danger');
               return redirect()->route('admingetforgetPassword');

             } else {
               
               
              return redirect()->route('admingetOtpPage',encrypt($phone));
             }
        } else {
            Session::flash('failure_message', 'Invalid Mobile No!');
            Session::flash('alert-class', 'alert alert-danger');
            return redirect()->route('admingetforgetPassword');
        }
    }

    public function admingetOtpPage($phone){

        return view('adminoptcheck-page',compact('phone'));
    }

    public function adminpostOtpVerify(Request $request){
        //dd($request->all());
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        $this->validate($request, [
            'new_passowrd' => 'required',
            'confirm_password' => 'required|same:new_passowrd',
        ], [
            'new_passowrd.required' => 'Password is required',
            'confirm_password.required' => 'Confirmation Password is required',
            'confirm_password.same' => 'Password and confirmation password are not same',
        ]);
        try{   
        if (Cookie::get('TIMEOUT') >= time()) {
            $otp = $request->otp;
         
            $phone=$request->phone;
            if ($otp == Cookie::get('OTP')) {
               
                $password = Hash::make($request->confirm_password);
                $update=User::where('phone_no',$phone)->where('user_type','admin')->update([
                    'password'=>$password
                ]);
               
                if($update>0){
                    $response = array(
                        'status' => 1,
                        'message' => "Password Changed Successfully Please Login.",
                    );
                }
                else{
                    $response = array(
                        'status' => 2,
                        'message' => "Oops. Something wrong. Please try again",
                    );
                }
               
                Cookie::queue(Cookie::forget('OTP'));
                Cookie::queue(Cookie::forget('TIMEOUT'));
            } else {
                $response = array(
                    'status' => 0,
                    'message' => "OTP Not valid, Try Again.",
                );
            }
        } else {
            $response = array(
                'status' => 0,
                'message' => "OTP Expired! Please Generate A New One!",
            );
        }
    } catch (\Exception $e) {
        $response = array(
            'exception' => true,
            'exception_message' => $e->getMessage(),
        );
        $statusCode = 400;
    } finally {
        return response()->json($response, $statusCode);
    }
    }




/******************** Seller ****************************/

    public function getforgetPassword(){
        Cookie::queue(Cookie::forget('OTP'));
        Cookie::queue(Cookie::forget('TIMEOUT'));
        return view('forget-password');
    }
    public function postforgetPassword(Request $request){
      
        $this->validate($request, [
            'mobile_no' => 'required',
        ], [
            'mobile_no.required' => 'Mobile no is required',
          

        ]);
        $phone = $request->mobile_no;
        $length = 5;
        $userData = User::where('phone_no', $phone)->where('user_type','seller')->first();
        if (!empty($userData)) {
          
            $otp = substr(str_shuffle(str_repeat($x = '0123456789', ceil($length / strlen($x)))), 2, $length);
            // $otp1 = hash('sha256', ("L2v" . $otp));
              //$otp='12345';
                //$otp='12345';
               // Session::put('OTP', $otp);
               // Session::put('TIMEOUT', time() + (60 * 5));
                Cookie::queue('OTP', $otp, 43200);
                Cookie::queue('TIMEOUT', time() + (60 * 5));
            //  session(['OTP' => $otp]);
            //  session(['TIMEOUT' => time() + (60 * 5)]);
             $data_base[0]['body'] =env("FORGET_PASSWORD_OTP"); // Tests

             $vars = array(
               '{$otp}'       => $otp
             );
             
              $msg=strtr($data_base[0]['body'], $vars); 
             //$msg = "Your One Time Password $otp for registering in Local2Vocal is .";

       
             $apiKey = urlencode("PBnYEfajueU-ZLgRhC24TA3poA2CQ1TSzFMndmR29d");
            

             $sender = urlencode("LOVOCL");
             $message = rawurlencode($msg);
            
            
             $data = array("apikey" => $apiKey, "numbers" => $phone, "sender" => $sender, "message" => $message);
           
             $url = "https://api.textlocal.in/send/";

             $ch = curl_init();
             curl_setopt($ch, CURLOPT_URL, $url);
             curl_setopt($ch, CURLOPT_POST, true);
             curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
             $buffer = curl_exec($ch);
             curl_close($ch);
            
             if (empty($buffer)) {
                Session::flash('failure_message', 'Oops.. Something wrong. Try again!');
                Session::flash('alert-class', 'alert alert-danger');
               return redirect()->route('getforgetPassword');

             } else {
               
               
              return redirect()->route('getOtpPage',encrypt($phone));
             }
        } else {
            Session::flash('failure_message', 'Invalid Mobile No!');
            Session::flash('alert-class', 'alert alert-danger');
            return redirect()->route('getforgetPassword');
           // return redirect()->route('getOtpPage',encrypt($phone));
        }
    }

    public function getOtpPage($phone){

return view('optcheck-page',compact('phone'));
    }
    public function postOtpVerify(Request $request){
        //dd($request->all());
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        $this->validate($request, [
           
            'new_passowrd' => 'required',
            'confirm_password' => 'required|same:new_passowrd',
           
        ], [

         
            'new_passowrd.required' => 'Password is required',
            'confirm_password.required' => 'Confirmation Password is required',
            'confirm_password.same' => 'Password and confirmation password are not same',
           
        ]);
        try{   
        if (Cookie::get('TIMEOUT') >= time()) {
            $otp = $request->otp;
         
            $phone=$request->phone;
            if ($otp == Cookie::get('OTP')) {
               
                $password = Hash::make($request->confirm_password);
                $update=User::where('phone_no',$phone)->where('user_type','seller')->update([
                    'password'=>$password
                ]);
               
                if($update>0){
                    $response = array(
                        'status' => 1,
                        'message' => "Password Changed Successfully Please Login.",
                    );
                }
                else{
                    $response = array(
                        'status' => 2,
                        'message' => "Oops. Something wrong. Please try again",
                    );
                }
               
                Cookie::queue(Cookie::forget('OTP'));
                Cookie::queue(Cookie::forget('TIMEOUT'));
            } else {
                $response = array(
                    'status' => 0,
                    'message' => "OTP Not valid, Try Again.",
                );
            }
        } else {
            $response = array(
                'status' => 0,
                'message' => "OTP Expired! Please Generate A New One!",
            );
        }
    } catch (\Exception $e) {
        $response = array(
            'exception' => true,
            'exception_message' => $e->getMessage(),
        );
        $statusCode = 400;
    } finally {
        return response()->json($response, $statusCode);
    }
    }
}
