<?php

namespace App\Http\Controllers;

use App\Models\Seller;
use Illuminate\Http\Request;

class BankDetailsController extends Controller
{
    public function index(Request $request)
    {
        $sort_search =null;
        $sellers = Seller::join('users','users.id','=','sellers.user_id')->orderBy('users.created_at', 'desc')
        ->select('users.name','users.phone_no','sellers.id');
        if ($request->has('search')){
            $sort_search = $request->search;
            $sellers = $sellers->where('name', 'like', '%'.$sort_search.'%');
        }
        $sellers = $sellers->paginate(15);
        return view('admin.bank_details.index', compact('sellers', 'sort_search'));
    }

    public function edit($id)
    {
        $seller = Seller::with('user')->findOrFail(decrypt($id));
      
        return view('admin.bank_details.edit', compact('seller'));
    }

    public function update(Request $request, $id)
    {
        $seller = Seller::findOrFail($id);
        $seller->benificiary_name = $request->benificiary_name;
        $seller->bank_acc_no = $request->bank_acc_no;
        $seller->ifsc_code = $request->ifsc_code;
        $seller->bname = $request->bname;
        
       
        if($seller->save()){
            flash(__('Seller bank details has been updated successfully'))->success();
            return redirect()->route('bank_details.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

}
