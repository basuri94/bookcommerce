<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PromotionalBanner;
use App\Emailtemplate;
use Illuminate\Support\Str;

class PromotionalBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sort_search =null;
        $Busines = PromotionalBanner::orderBy('created_at', 'desc');
        //$Busines =  $Busines->where('type','promotional_banner');
        /*if ($request->has('search')){
            $sort_search = $request->search;
            $e_template = $e_template->where('subject', 'like', '%'.$sort_search.'%');
        }*/
        $Busines = $Busines->paginate(15);
        return view('promotional_banner.index', compact('Busines', 'sort_search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('promotional_banner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Busines = new PromotionalBanner;

        // if($request->file('thumbnail_img')!=''){
        //     list($width, $height, $type, $attr) = getimagesize($request->file('thumbnail_img')); 
        //     if($width!=850 && $height!=315){
        //         flash(__('Please Thumbnail Image image  width=850 and height=315 '))->error();
        //         return back();
        //     }
        // }
        if($request->hasFile('thumbnail_img')){
            $Busines->banners = $request->thumbnail_img->store('uploads/banners');
        }
       
        if($Busines->save()){
            flash(__('Promotional Banner has been inserted successfully'))->success();
            return redirect()->route('promotional_banner.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Busines = PromotionalBanner::findOrFail(decrypt($id));
        return view('promotional_banner.edit', compact('Busines'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $e_template = Emailtemplate::findOrFail($id);
        
        // if ($request->slug != null) {
        //     $e_template->slug = str_replace(' ', '-', $request->slug);
        // }
        // else {
        //     $e_template->slug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->subject)).'-'.Str::random(5);
        // }
        /*$e_template->title = $request->title;
        $e_template->subject = $request->subject;
        $e_template->mail_body = $request->mail_body;

        if($e_template->save()){
            flash(__('Email Template Details has been updated successfully'))->success();
            return redirect()->route('promotional_banner.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { 
        $data = PromotionalBanner::where('id', $id)->select('banners')->first();
        unlink(public_path($data->banners));
         
        $PromotionalBanner = PromotionalBanner::findOrFail($id);
        if(PromotionalBanner::destroy($id)){
           
            flash(__('Request has been deleted successfully'))->success();
            return back();
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }
}
