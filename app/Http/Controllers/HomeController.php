<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

use Hash;
use App\Category;
use App\FlashDeal;
use App\Brand;
use App\SubCategory;
use App\SubSubCategory;
use App\Product;
use App\PickupPoint;
use App\CustomerPackage;
use App\CustomerProduct;
use App\User;
use App\Seller;
use App\Shop;
use App\Color;
use App\Order;
use App\Contact;
use App\BusinessSetting;
use App\SellerOnboard;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\OrderController;
use App\Mail\ChangePhoneNumber;
use App\SellerRequest;
use ImageOptimizer;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{

    public function customergetforgetPassword()
    {
        Cookie::queue(Cookie::forget('OTP'));
        Cookie::queue(Cookie::forget('TIMEOUT'));
        return view('frontend.customer-forget-password');
    }

    public function customerpostforgetPassword(Request $request)
    {

        $this->validate($request, [
            'mobile_no' => 'required',
        ], [
            'mobile_no.required' => 'Mobile no is required',


        ]);
        $phone = $request->mobile_no;
        $length = 5;
        $userData = User::where('phone_no', $phone)->where('user_type', 'customer')->first();
        if (!empty($userData)) {

            $otp = '321654';
            Cookie::queue('OTP', $otp, 43200);
            Cookie::queue('TIMEOUT', time() + (60 * 5));
            $data_base[0]['body'] = env("FORGET_PASSWORD_OTP");

            $vars = array(
                '{$otp}'       => $otp
            );

            $msg = strtr($data_base[0]['body'], $vars);
            $apiKey = urlencode("PBnYEfajueU-ZLgRhC24TA3poA2CQ1TSzFMndmR29d");
            $sender = urlencode("LOVOCL");
            $message = rawurlencode($msg);
            $data = array("apikey" => $apiKey, "numbers" => $phone, "sender" => $sender, "message" => $message);
            $url = "https://api.textlocal.in/send/";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $buffer = curl_exec($ch);
            curl_close($ch);

            if (empty($buffer)) {
                Session::flash('failure_message', 'Oops.. Something wrong. Try again!');
                Session::flash('alert-class', 'alert alert-danger');
                return redirect()->route('customergetforgetPassword');
            } else {


                return redirect()->route('customergetOtpPage', encrypt($phone));
            }
        } else {
            Session::flash('failure_message', 'Invalid Mobile No!');
            Session::flash('alert-class', 'alert alert-danger');
            return redirect()->route('customergetforgetPassword');
        }
    }

    public function customergetOtpPage($phone)
    {

        return view('frontend.customeroptcheck-page', compact('phone'));
    }

    public function customerpostOtpVerify(Request $request)
    {
        //dd($request->all());
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        $this->validate($request, [
            'new_passowrd' => 'required',
            'confirm_password' => 'required|same:new_passowrd',
        ], [
            'new_passowrd.required' => 'Password is required',
            'confirm_password.required' => 'Confirmation Password is required',
            'confirm_password.same' => 'Password and confirmation password are not same',
        ]);
        try {
            if (Cookie::get('TIMEOUT') >= time()) {
                $otp = $request->otp;

                $phone = $request->phone;
                if ($otp == Cookie::get('OTP')) {

                    $password = Hash::make($request->confirm_password);
                    $update = User::where('phone_no', $phone)->where('user_type', 'customer')->update([
                        'password' => $password
                    ]);

                    if ($update > 0) {
                        $response = array(
                            'status' => 1,
                            'message' => "OTP Changed Successfully Please Login.",
                        );
                    } else {
                        $response = array(
                            'status' => 2,
                            'message' => "Oops. Something wrong. Please try again",
                        );
                    }

                    Cookie::queue(Cookie::forget('OTP'));
                    Cookie::queue(Cookie::forget('TIMEOUT'));
                } else {
                    $response = array(
                        'status' => 0,
                        'message' => "OTP Not valid, Try Again.",
                    );
                }
            } else {
                $response = array(
                    'status' => 0,
                    'message' => "OTP Expired! Please Generate A New One!",
                );
            }
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }






    public function login()
    {
        if (Auth::check()) {
            return redirect()->route('home');
        }
        return view('frontend.user_login');
    }

    public function registration(Request $request)
    {
        Cookie::queue(Cookie::forget('referral_code'));
        if (Auth::check()) {
            return redirect()->route('home');
        }
        if ($request->has('referral_code')) {
            Cookie::queue('referral_code', $request->referral_code, 43200);
        }

        return view('frontend.user_registration');
    }

    // public function user_login(Request $request)
    // {
    //     $user = User::whereIn('user_type', ['customer', 'seller'])->where('email', $request->email)->first();
    //     if($user != null){
    //         if(Hash::check($request->password, $user->password)){
    //             if($request->has('remember')){
    //                 auth()->login($user, true);
    //             }
    //             else{
    //                 auth()->login($user, false);
    //             }
    //             return redirect()->route('dashboard');
    //         }
    //     }
    //     return back();
    // }

    public function cart_login(Request $request)
    {
        $user = User::whereIn('user_type', ['customer', 'seller'])->where('email', $request->email)->first();
        if ($user != null) {
            updateCartSetup();
            if (Hash::check($request->password, $user->password)) {
                if ($request->has('remember')) {
                    auth()->login($user, true);
                } else {
                    auth()->login($user, false);
                }
            }
        }
        return back();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_dashboard()
    {
        return view('dashboard');
    }

    /**
     * Show the customer/seller dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        //echo json_encode(Auth::user()->seller);die;
        if (Auth::user()->user_type == 'seller' && Auth::user()->seller->verification_status == 1) {
            return view('frontend.seller.dashboard');
        } elseif (Auth::user()->user_type == 'customer') {
            return view('frontend.customer.dashboard');
        } elseif (Auth::user()->user_type == 'seller' && Auth::user()->seller->verification_status != 1) {
            return redirect(route('approvalStatus'));
        } else {
            abort(404);
        }
    }

    public function profile(Request $request)
    {
        if (Auth::user()->user_type == 'customer') {
            return view('frontend.customer.profile');
        } elseif (Auth::user()->user_type == 'seller') {
            return view('frontend.seller.profile');
        }
    }

    public function customer_update_profile(Request $request)
    {

        $this->validate($request, [

            'new_password' => 'nullable',
            'confirm_password' => 'nullable|same:new_password',
            'name' => 'required'
        ], [

            'phone.required' => 'Mobile No is required',
            'email.required' => 'Email is required',
            'phone.regex' => 'Invalid Mobile No',
            'new_password.required' => 'Password is required',
            'confirm_password.required' => 'Confirmation Password is required',
            'confirm_password.same' => ' Password and Confirm password does not match',
            'email.required' => 'Email is required',
            'name.required' => 'Name is required',
        ]);
        $user = Auth::user();
        $user->name = $request->name;
        $user->address = $request->address;
        $user->country = $request->country;
        $user->city = $request->city;
        $user->postal_code = $request->postal_code;
        $user->phone_no = $request->phone;
        if ($request->new_password != $request->confirm_password) {
            flash(__(' Password and Confirm password does not match'))->success();
            return back();
        }
        if ($request->new_password != null && ($request->new_password == $request->confirm_password)) {
            $user->password = Hash::make($request->new_password);
        }

        if ($request->hasFile('photo')) {
            $user->avatar_original = $request->photo->store('uploads/users');
        }

        if ($user->save()) {
            flash(__('Your Profile has been updated successfully!'))->success();
            return back();
        }

        flash(__('Sorry! Something went wrong.'))->error();
        return back();
    }


    public function seller_update_profile(Request $request)
    {

        $this->validate($request, [

            'new_password' => 'nullable|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/',
            'confirm_password' => 'nullable|same:new_password',


        ], [
            'new_password.regex' => 'Pasword Must Contain at least 1 lowercase,1 uppercase,1 numeric,one special character and greater than 8 charcter',

            'confirm_password.same' => 'Password and confirmation password are not same',

        ]);

        $user = Auth::user();



        $user->name = $request->fname . " " . $request->lname;
        if ($request->new_password == $request->confirm_password) {
            if ($request->new_password != null && ($request->new_password == $request->confirm_password)) {
                $user->password = Hash::make($request->new_password);
            }
        } else {
            flash(__('Sorry! password and confirm password not maching'))->error();
            return back();
        }


        if ($request->hasFile('photo')) {
            $user->avatar_original = $request->photo->store('uploads');
        }

        $seller = $user->seller;
        //$seller->cash_on_delivery_status = $request->cash_on_delivery_status;
        //$seller->bank_payment_status = $request->bank_payment_status;
        $seller->benificiary_name = $request->benificiary_name;
        $seller->ifsc_code = $request->ifsc_code;
        $seller->bname = $request->bname;
        $seller->bank_acc_no = $request->bank_acc_no;


        if ($user->save() && $seller->save()) {
            flash(__('Your Profile has been updated successfully!'))->success();
            return back();
        }

        flash(__('Sorry! Something went wrong.'))->error();
        return back();
    }

    /**
     * Show the application frontend home.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  

        $subDomain = request()->getHttpHost();

        if ($subDomain == env('APP_SUBDOMAIN')) {
            if (Auth::check()) {
                return redirect(route('dashboard'));
            } else {
                return redirect(route('login'));
                //  return view('frontend.seller_regis_log');
            }
        }
        return view('frontend.index');
    }

    public function flash_deal_details($slug)
    {
        $flash_deal = FlashDeal::where('slug', $slug)->first();
        if ($flash_deal != null)
            return view('frontend.flash_deal_details', compact('flash_deal'));
        else {
            abort(404);
        }
    }

    public function load_featured_section()
    {
        return view('frontend.partials.featured_products_section');
    }

    public function load_best_selling_section()
    {
        return view('frontend.partials.best_selling_section');
    }

    public function load_home_categories_section()
    {
        return view('frontend.partials.home_categories_section');
    }

    public function load_best_sellers_section()
    {
        return view('frontend.partials.best_sellers_section');
    }

    public function trackOrder(Request $request)
    {
        if ($request->has('order_code')) {
            $order = Order::where('code', $request->order_code)->first();
            //     $tracking_dt=[];
            //     $tracking=$order->curiorOrderDetails;
            //     // echo json_encode($tracking);die;
            //     // echo $tracking[0]->awbNo;die;
            // if($tracking->count()>0){
            // foreach($tracking as  $tracking_data){

            //     if ($tracking_data->awbNo!=null) {
            //         $OrderController = new OrderController;
            //         array_push($tracking_dt,$OrderController->track($tracking_data->awbNo));
            //     }
            // }

            // }
            if ($order != null) {
                return view('frontend.track_order', compact('order', 'tracking_dt'));
            }
        }
        return view('frontend.track_order');
    }

    public function product(Request $request, $slug)
    { 
        $detailedProduct  = Product::where('slug', $slug)->first();
        if ($detailedProduct != null && $detailedProduct->published) {
            updateCartSetup();
            if ($request->has('product_referral_code')) {
                Cookie::queue('product_referral_code', $request->product_referral_code, 43200);
                Cookie::queue('referred_product_id', $detailedProduct->id, 43200);
            }
            if ($detailedProduct->digital == 1) {
                return view('frontend.digital_product_details', compact('detailedProduct'));
            } else {
                return view('frontend.product_details', compact('detailedProduct'));
            }
            // return view('frontend.single_product', compact('detailedProduct'));
        }
        abort(404);
    }

    public function shop($slug)
    {
        $shop  = Shop::where('slug', $slug)->first();
        if ($shop != null) {
            $seller = Seller::where('user_id', $shop->user_id)->first();
            if ($seller->verification_status != 0) {
                return view('frontend.seller_shop', compact('shop'));
            } else {
                return view('frontend.seller_shop_without_verification', compact('shop', 'seller'));
            }
        }
        abort(404);
    }

    public function filter_shop($slug, $type)
    {
        $shop  = Shop::where('slug', $slug)->first();
        if ($shop != null && $type != null) {
            return view('frontend.seller_shop', compact('shop', 'type'));
        }
        abort(404);
    }

    public function listing(Request $request)
    {
        // $products = filter_products(Product::orderBy('created_at', 'desc'))->paginate(12);
        // return view('frontend.product_listing', compact('products'));
        return $this->search($request);
    }

    public function all_categories(Request $request)
    {
        $categories = Category::all();
        return view('frontend.all_category', compact('categories'));
    }
    public function all_brands(Request $request)
    {
        $categories = Category::all();
        return view('frontend.all_brand', compact('categories'));
    }

    public function show_product_upload_form(Request $request)
    {
        if (\App\Addon::where('unique_identifier', 'seller_subscription')->first() != null && \App\Addon::where('unique_identifier', 'seller_subscription')->first()->activated) {
            if (Auth::user()->seller->remaining_uploads > 0) {
                $categories = Category::all();
                return view('frontend.seller.product_upload', compact('categories'));
            } else {
                flash('Upload limit has been reached. Please upgrade your package.')->warning();
                return back();
            }
        }
        $categories = Category::where('digital', 0)->get();
        return view('frontend.seller.product_upload', compact('categories'));
    }

    public function show_product_edit_form(Request $request, $id)
    {
        $categories = Category::where('digital', 0)->get();
        $product = Product::find(decrypt($id));
        return view('frontend.seller.product_edit', compact('categories', 'product'));
    }

    public function seller_product_list(Request $request)
    { //echo 'hi';die;
        $search  = null;
        $products = Product::where('digital', 0)->where('user_id', Auth::user()->id)->orderBy('created_at', 'desc');
        if ($request->has('search')) {
            $search = $request->search;
            $products = $products->where('name', 'like', '%' . $search . '%');
        }
        $products = $products->paginate(10);
        return view('frontend.seller.products', compact('products', 'search'));
    }

    public function ajax_search(Request $request)
    {
        $keywords = array();
        $products = Product::where('is_approve', 1)->where('published', 1)->where('tags', 'like', '%' . $request->search . '%')->get();
        foreach ($products as $key => $product) {
            foreach (explode(',', $product->tags) as $key => $tag) {
                if (stripos($tag, $request->search) !== false) {
                    if (sizeof($keywords) > 5) {
                        break;
                    } else {
                        if (!in_array(strtolower($tag), $keywords)) {
                            array_push($keywords, strtolower($tag));
                        }
                    }
                }
            }
        }

        $products = filter_products(Product::where('is_approve', 1)->where('published', 1)->where('name', 'like', '%' . $request->search . '%'))->get()->take(3);

        $subsubcategories = SubSubCategory::where('name', 'like', '%' . $request->search . '%')->get()->take(3);

        $shops = Shop::whereIn('user_id', verified_sellers_id())->where('name', 'like', '%' . $request->search . '%')->get()->take(3);

        if (sizeof($keywords) > 0 || sizeof($subsubcategories) > 0 || sizeof($products) > 0 || sizeof($shops) > 0) {
            return view('frontend.partials.search_content', compact('products', 'subsubcategories', 'keywords', 'shops'));
        }
        return '0';
    }

    public function search(Request $request)
    {
        $query = $request->q;
        $brand_arr = $request->brand;
        $seller_arr=$request->seller_id;
        if (empty($brand_arr)) {
            $brand_arr = array();
        }
        if (empty($seller_arr)) {
            $seller_arr = array();
        }
        // if($request->brand!=''){
          
        //     $brand_id = (Brand::where('slug', $request->brand)->first() != null) ? Brand::where('slug', $request->brand)->first()->id : null;
        // }else{
        //     $brand_id =null; 
        // }
        
        $sort_by = $request->sort_by;
        $category_id = (Category::where('slug', $request->category)->first() != null) ? Category::where('slug', $request->category)->first()->id : null;
        $subcategory_id = (SubCategory::where('slug', $request->subcategory)->first() != null) ? SubCategory::where('slug', $request->subcategory)->first()->id : null;
        $subsubcategory_id = (SubSubCategory::where('slug', $request->subsubcategory)->first() != null) ? SubSubCategory::where('slug', $request->subsubcategory)->first()->id : null;
        $min_price = $request->min_price;
        $max_price = $request->max_price;
       // $seller_id = $request->seller_id;
        $extra = $request->extra;
//echo $category_id;die;
        $conditions = ['published' => 1];
        // $conditions = ['is_approve' => 1];

        // if ($brand_arr != null) {
            
        //     $conditions = array_merge($conditions, ['brand_id' => $brand_id]);
        // }
        if ($category_id != null) {
            $category=Category::find($category_id);
            $conditions = array_merge($conditions, ['category_id' => $category_id]);
        }
        if ($subcategory_id != null) {
            $conditions = array_merge($conditions, ['subcategory_id' => $subcategory_id]);
        }
        if ($subsubcategory_id != null) {
            $conditions = array_merge($conditions, ['subsubcategory_id' => $subsubcategory_id]);
        }
        // if ($seller_id != null) {
        //     $conditions = array_merge($conditions, ['user_id' => Seller::findOrFail($seller_id)->user->id]);
        // }

        $products = Product::where($conditions);
        if (!empty($brand_arr)) {
            $products = $products->whereHas('brand', function ($q) use ($brand_arr) {
                $q->whereIn('brands.slug', $brand_arr);
            });
            //$conditions = array_merge($conditions, ['brand_id' => $brand_id]);
        }
        if (!empty($seller_arr)) {
            $products = $products->whereIn('products.user_id', $seller_arr);
           
            
        }
        
        if ($min_price != null && $max_price != null) {
            $products = $products->where('unit_price', '>=', $min_price)->where('unit_price', '<=', $max_price);
        }

        if ($query != null) {
            $searchController = new SearchController;
            $searchController->store($request);
            $products = $products->where('name', 'like', '%' . $query . '%')->orWhere('tags', 'like', '%' . $query . '%');
        }

        if ($sort_by != null) {
            switch ($sort_by) {
                case '1':
                    $products->orderBy('created_at', 'desc');
                    break;
                case '2':
                    $products->orderBy('created_at', 'asc');
                    break;
                case '3':
                    $products->orderBy('unit_price', 'asc');
                    break;
                case '4':
                    $products->orderBy('unit_price', 'desc');
                    break;
                default:
                    // code...
                    break;
            }
        }


        $non_paginate_products = filter_products($products)->get();

        //Attribute Filter

        $attributes = array();
        foreach ($non_paginate_products as $key => $product) {
            if ($product->attributes != null && is_array(json_decode($product->attributes))) {
                foreach (json_decode($product->attributes) as $key => $value) {
                    $flag = false;
                    $pos = 0;
                    foreach ($attributes as $key => $attribute) {
                        if ($attribute['id'] == $value) {
                            $flag = true;
                            $pos = $key;
                            break;
                        }
                    }
                    if (!$flag) {
                        $item['id'] = $value;
                        $item['values'] = array();
                        foreach (json_decode($product->choice_options) as $key => $choice_option) {
                            if ($choice_option->attribute_id == $value) {
                                $item['values'] = $choice_option->values;
                                break;
                            }
                        }
                        array_push($attributes, $item);
                    } else {
                        foreach (json_decode($product->choice_options) as $key => $choice_option) {
                            if ($choice_option->attribute_id == $value) {
                                foreach ($choice_option->values as $key => $value) {
                                    if (!in_array($value, $attributes[$pos]['values'])) {
                                        array_push($attributes[$pos]['values'], $value);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $selected_attributes = array();

        foreach ($attributes as $key => $attribute) {
            if ($request->has('attribute_' . $attribute['id'])) {
                foreach ($request['attribute_' . $attribute['id']] as $key => $value) {
                    $str = '"' . $value . '"';
                    $products = $products->where('choice_options', 'like', '%' . $str . '%');
                }

                $item['id'] = $attribute['id'];
                $item['values'] = $request['attribute_' . $attribute['id']];
                array_push($selected_attributes, $item);
            }
        }


        //Color Filter
        $all_colors = array();

        foreach ($non_paginate_products as $key => $product) {
            if ($product->colors != null) {
                foreach (json_decode($product->colors) as $key => $color) {
                    if (!in_array($color, $all_colors)) {
                        array_push($all_colors, $color);
                    }
                }
            }
        }

        $selected_color = null;

        if ($request->has('color')) {
            $str = '"' . $request->color . '"';
            $products = $products->where('colors', 'like', '%' . $str . '%');
            $selected_color = $request->color;
        }
        if ($extra=="daily_deals") {
            $products = $products->where('is_approve', 1)->where('published', 1)->where('todays_deal', '1');
           
        }
        if ($extra=="digital_product") {
            $products = $products->where('is_approve', 1)->where('published', 1)->where('digital', '1');
           
        }
        if ($extra=="new_arrival") {
            $date = \Carbon\Carbon::today()->subDays(10);
            $products = $products->where('is_approve', 1)->where('published', 1)->where('created_at', '>=', $date);
           
        }
        if ($extra=="trending") {
            $date = \Carbon\Carbon::today()->subDays(10);
            $products = $products->where('featured', 1)->where('is_approve', 1)->where('published', 1);
           
        }

        $products = filter_products($products)->paginate(12)->appends(request()->query());
     
      
        return view('frontend.product_listing', compact('category','products', 'query', 'category_id', 'subcategory_id', 'subsubcategory_id', 'brand_id', 'sort_by', 'seller_id', 'min_price', 'max_price', 'attributes', 'selected_attributes', 'all_colors', 'selected_color','extra',
    'brand_arr','seller_arr' 
    ));
    }

    public function product_content(Request $request)
    {
        $connector  = $request->connector;
        $selector   = $request->selector;
        $select     = $request->select;
        $type       = $request->type;
        productDescCache($connector, $selector, $select, $type);
    }

    public function home_settings(Request $request)
    {
        return view('home_settings.index');
    }

    public function top_10_settings(Request $request)
    {
        foreach (Category::all() as $key => $category) {
            if (in_array($category->id, $request->top_categories)) {
                $category->top = 1;
                $category->save();
            } else {
                $category->top = 0;
                $category->save();
            }
        }

        foreach (Brand::all() as $key => $brand) {
            if (in_array($brand->id, $request->top_brands)) {
                $brand->top = 1;
                $brand->save();
            } else {
                $brand->top = 0;
                $brand->save();
            }
        }

        flash(__('Top 9 categories and brands have been updated successfully'))->success();
        return redirect()->route('home_settings.index');
    }

    public function variant_price(Request $request)
    {
        $product = Product::find($request->id);
        $str = '';
        $quantity = 0;

        if ($request->has('color')) {
            $data['color'] = $request['color'];
            $str = Color::where('code', $request['color'])->first()->name;
        }

        if (json_decode(Product::find($request->id)->choice_options) != null) {
            foreach (json_decode(Product::find($request->id)->choice_options) as $key => $choice) {
                if ($str != null) {
                    $str .= '-' . str_replace(' ', '', $request['attribute_id_' . $choice->attribute_id]);
                } else {
                    $str .= str_replace(' ', '', $request['attribute_id_' . $choice->attribute_id]);
                }
            }
        }



        if ($str != null && $product->variant_product) {
            $product_stock = $product->stocks->where('variant', $str)->first();
            $price = $product_stock->price;
            $quantity = $product_stock->qty;
        } else {
            $price = $product->unit_price;
            $quantity = $product->current_stock;
        }

        //discount calculation
        $flash_deals = \App\FlashDeal::where('status', 1)->get();
        $inFlashDeal = false;
        foreach ($flash_deals as $key => $flash_deal) {
            if ($flash_deal != null && $flash_deal->status == 1 && strtotime(date('d-m-Y')) >= $flash_deal->start_date && strtotime(date('d-m-Y')) <= $flash_deal->end_date && \App\FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $product->id)->first() != null) {
                $flash_deal_product = \App\FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $product->id)->first();
                if ($flash_deal_product->discount_type == 'percent') {
                    $price -= ($price * $flash_deal_product->discount) / 100;
                } elseif ($flash_deal_product->discount_type == 'amount') {
                    $price -= $flash_deal_product->discount;
                }
                $inFlashDeal = true;
                break;
            }
        }
        if (!$inFlashDeal) {
            if ($product->discount_type == 'percent') {
                $price -= ($price * $product->discount) / 100;
            } elseif ($product->discount_type == 'amount') {
                $price -= $product->discount;
            }
        }

        // if ($product->tax_type == 'percent') {
        //     $price += ($price * $product->tax) / 100;
        // } elseif ($product->tax_type == 'amount') {
        //     $price += $product->tax;
        // }
        return array('price' => single_price($price * $request->quantity), 'quantity' => $quantity, 'digital' => $product->digital);
    }

    public function sellerpolicy()
    {
        return view("frontend.policies.sellerpolicy");
    }

    public function returnpolicy()
    {
        return view("frontend.policies.returnpolicy");
    }

    public function supportpolicy()
    {
        return view("frontend.policies.supportpolicy");
    }

    public function terms()
    {
        return view("frontend.policies.terms");
    }

    public function privacypolicy()
    {
        return view("frontend.policies.privacypolicy");
    }

    public function sellerfaq()
    {
        return view("frontend.faq.sellerfaq");
    }

    public function customerfaq()
    {
        return view("frontend.faq.customerfaq");
    }

    public function store_contact(Request $request)
    {
        $contact = new Contact;
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->subject = $request->subject;
        $contact->message = $request->message;


        if ($contact->save()) {

            flash(__('Thank you for contacting us. We will get back to you soon!'))->success();
            return redirect()->route('customer_contact');
        } else {
            flash(__('Sorry! Something went wrong.'))->error();
            return back();
        }
    }

    public function ckeditor_upload(Request $request)
    {

        if ($request->hasFile('upload')) {
            //   $image = $request->file('upload');
            //   $folder = 'uploads/CMS/';
            //   $ext = $image->getClientOriginalExtension();
            //   $name = $this->uploadOne($folder, $image, $ext, '');

            //   if (!empty($request->file('upload'))) {
            $product_licence = $request->file('upload');
            $file_ext = $product_licence->getClientOriginalExtension();
            $name = date("dmYhms") . "up" . rand(101, 99999) . "." . $file_ext;
            $destination_path_product_licence = "uploads/ckupload";
            $product_licence->move($destination_path_product_licence, $name);
            // }

            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = \URL::to('/uploads/ckupload/' . $name);
            // $url = asset('images/'.$fileName); 
            $msg = 'Image uploaded successfully';
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

            @header('Content-type: text/html; charset=utf-8');
            echo $response;
        }
    }

    public function get_pick_ip_points(Request $request)
    {
        $pick_up_points = PickupPoint::all();
        return view('frontend.partials.pick_up_points', compact('pick_up_points'));
    }

    public function get_category_items(Request $request)
    {
        $category = Category::findOrFail($request->id);
        return view('frontend.partials.category_elements', compact('category'));
    }

    public function premium_package_index()
    {
        $customer_packages = CustomerPackage::all();
        return view('frontend.customer_packages_lists', compact('customer_packages'));
    }

    public function seller_digital_product_list(Request $request)
    {
        $products = Product::where('user_id', Auth::user()->id)->where('digital', 1)->orderBy('created_at', 'desc')->paginate(10);
        return view('frontend.seller.digitalproducts.products', compact('products'));
    }
    public function show_digital_product_upload_form(Request $request)
    {
        if (\App\Addon::where('unique_identifier', 'seller_subscription')->first() != null && \App\Addon::where('unique_identifier', 'seller_subscription')->first()->activated) {
            if (Auth::user()->seller->remaining_digital_uploads > 0) {
                $business_settings = BusinessSetting::where('type', 'digital_product_upload')->first();
                $categories = Category::where('digital', 1)->get();
                return view('frontend.seller.digitalproducts.product_upload', compact('categories'));
            } else {
                flash('Upload limit has been reached. Please upgrade your package.')->warning();
                return back();
            }
        }

        $business_settings = BusinessSetting::where('type', 'digital_product_upload')->first();
        $categories = Category::where('digital', 1)->get();
        return view('frontend.seller.digitalproducts.product_upload', compact('categories'));
    }

    public function show_digital_product_edit_form(Request $request, $id)
    {
        $categories = Category::where('digital', 1)->get();
        $product = Product::find(decrypt($id));
        return view('frontend.seller.digitalproducts.product_edit', compact('categories', 'product'));
    }
    public function daily_deals(Request $request)
    {
        return view('frontend.daily_deals_product_listing');
    }
    public function best_sellers_profile(Request $request)
    {
        return view('frontend.best_sellers_profile');
    }
    public function services(Request $request)
    {
        return view('frontend.services');
    }
    public function new_arrival(Request $request)
    {
        return view('frontend.new_arrival');
    }
    public function trending(Request $request)
    {
        return view('frontend.trending');
    }


    /*****************Change Mobile Number******************* */

    public function userChangePhoneNumber(Request $request)
    { //session()->forget(['OTP', 'TIMEOUT']);
        // dd($request->all());
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }

        try {
            $length = 5;
            $phone = Auth::user()->phone_no;




            $otp = substr(str_shuffle(str_repeat($x = '0123456789', ceil($length / strlen($x)))), 2, $length);


            $array['from'] = env('MAIL_USERNAME');
            $array['name'] = Auth::user()->name;
            $array['otp'] = $otp;
            Mail::to(Auth::user()->email)->queue(new ChangePhoneNumber($array));

           


            session(['OTP' => $otp]);
            session(['TIMEOUT' => time() + (60 * 5)]);

                            $data_base[0]['body'] =env("CHANGE_PHONE_OTP"); // Tests
         
                            $vars = array(
                              '{$otp}'       => $otp
                            );

            $msg=strtr($data_base[0]['body'], $vars); 
             //$msg = "Your One Time Password $otp for registering in Local2Vocal is .";
            $apiKey = urlencode("PBnYEfajueU-ZLgRhC24TA3poA2CQ1TSzFMndmR29d");
            // Message details

            $sender = urlencode("LOVOCL");
            $message = rawurlencode($msg);


            // Prepare data for POST request
            $data = array("apikey" => $apiKey, "numbers" => $phone, "sender" => $sender, "message" => $message);
            // Send the POST request with cURL
            $url = "https://api.textlocal.in/send/";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $buffer = curl_exec($ch);
            curl_close($ch);
            //return $buffer;
            if (empty($buffer)) {
                $response = array(
                    'status' => 0,
                    'message' => "Sorry.. Some Error Occurred! Try Again.",
                );
            } else {

                $response = array(
                    'status' => 1
                );
            }
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }


    public function userChangePhoneOtpcheck(Request $request)
    { 
        //dd($request->all());
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        $this->validate($request, [

            'otp' => 'required|regex:/^[0-9]+$/',
            'new_phone' => 'required|regex:/^\\+[0-9]+$/',
        ], [

            'otp.required' => 'OTP is required',
            'otp.regex' => 'OTP  contains only integer value',
            'new_phone.required' => 'Phone No is required',

            'new_phone.regex' => 'Invalid Phone No',
        ]);
      
        try {

            if (session()->get('TIMEOUT') >= time()) {
                
                $otp = $request->otp;
                $phone = $request->new_phone;
                $type=$request->type;
                if ($otp === session()->get('OTP')) {
                    $check1 = DB::table("users")

                        ->where(function ($query) use ($phone) {
                            $query->Where('phone_no', '=', $phone);
                        })->where('user_type', $type)->get();

                    if ($check1->count() > 0) {
                        $response = array(
                            'status' => 0,
                            'message' => "Sorry!  Phone No  Already Exist!",
                        );
                    } else {
                        $update = User::where('id', Auth::user()->id)->update([
                            'phone_no' => $request->new_phone
                        ]);
                        session()->forget(['OTP', 'TIMEOUT']);
                        $response = array(
                            'status' => 1,
                            'message' => "Phone Number Changed Successfully.",
                        );
                    }
                } else {

                    $response = array(
                        'status' => 0,
                        'message' => "OTP Not valid, Try Again.",
                    );
                }
            } else {
                $response = array(
                    'status' => 2,
                    'message' => "OTP Expired! Please Generate A New One!",
                );
            }
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    /*********Request For New Category/Subcategory/Subsubcategory/HSN******************/
    public function request_category(Request $request)
    { 
        // echo "hi";die;
        $sellerRequest = SellerRequest::where('user_id', \Auth::user()->id)->orderBy('created_at', 'desc')->paginate(15);
        return view('frontend.seller.request_category', compact('sellerRequest'));
    }
    public function new_request(Request $request)
    {
        // echo "hi";die;
        $sellerRequest = new SellerRequest;
        $sellerRequest->user_id = \Auth::user()->id;
        $sellerRequest->category = $request->category;
        $sellerRequest->subcategory = $request->subcategory;
        $sellerRequest->subsubcategory = $request->subsubcategory;
        $sellerRequest->hsn = $request->hsn;
        $sellerRequest->gst = $request->gst;


        if ($sellerRequest->save()) {

            flash(__('Your Request Sent Successfully. Please Wait For Admin Review'))->success();
            return back();
        } else {
            flash(__('Sorry! Something went wrong.'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function request_destroy(Request $request)
    {
        $id = $request->id;
        $SellerRequest = SellerRequest::findOrFail($id);
        if (SellerRequest::destroy($id)) {

            flash(__('Request has been deleted successfully'))->success();
            return back();
        } else {
            flash(__('Something went wrong'))->error();
            return back();
        }
    }
    public function seller_request(Request $request)
    { 
        // echo "hi";die;
        $col_name = null;
        $query = null;
        $seller_id = null;
        $sort_search = null;
        $sellerRequest = SellerRequest::where('status', '<', '3');
        if ($request->has('user_id') && $request->user_id != null) {
            $sellerRequest = $sellerRequest->where('user_id', $request->user_id);
            $seller_id = $request->user_id;
        }
        if ($request->approved_status != null) {
            $approved = $request->approved_status;
            $sellerRequest = $sellerRequest->where('status', $approved);
        }
        $sellerRequest = $sellerRequest->orderBy('created_at', 'desc')->paginate(15);
        return view('sellers.seller_request', compact('sellerRequest', 'seller_id', 'approved'));
    }


    public function request_approval(Request $request)
    {
        $product = SellerRequest::findOrFail($request->id);
        $product->status = $request->status;
        if ($product->save()) {
            return 1;
        }
        return 0;
    }


    public function price_calculator_view()
    {
        return view('frontend.seller.price_calculator');
    }

    public function seller_onboard()
    {
        return view('seller_onboard');
    }

    public function seller_onboard_save(Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $phone = $request->phone;
        $shopname = $request->shopname;
        $category = $request->category;
        $state = $request->state;
        $gst = $request->gst;

        $SellerOnboard = new SellerOnboard();
        $SellerOnboard->name = $name;
        $SellerOnboard->email = $email;
        $SellerOnboard->phone = $phone;
        $SellerOnboard->shopname = $shopname;
        $SellerOnboard->category = $category;
        $SellerOnboard->state = $state;
        $SellerOnboard->gst = $gst;
        $SellerOnboard->save();

        $msg = "Name:" . $name . ", Email ID:" . $email . ", Phone No.:" . $phone . ", Shop Name:" . $shopname . ", Category:" . $category . ", State:" . $state . ", GST:" . $gst;
        Mail::send([], [], function ($message) use ($msg) {
             $message->to("query@thelocal2vocal.com")
            //$message->to("dass.suman126@gmail.com")
                ->subject("Seller Onboarding")
                ->setBody($msg, 'text/html');
        });
    }
}
