<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_faq;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FAQController extends Controller
{
    public function index()
    { 
        return view('Faq.index');
    }

    public function store(Request $request)
    { 
      $statusCode = 200;
     
      if (!$request->ajax()) {
          $statusCode = 400;
          $response = array('error' => 'Error occured in form submit.');
          return response()->json($response, $statusCode);
      }
     
      /******Validation*******/
      $this->validate($request, [
          'type' => 'required',
          'question' => 'required',
          'answer' => 'required',
              ], [
          'type.required' => 'Please select type',
          'question.required' => 'Please enter a question',
          'answer.required' => 'Please  enter an answer',
      ]);
      try {
        $type=$request->type;
        $question=$request->question;
        $answer=$request->answer;
         
    
        $faq = new tbl_faq;
        $faq->type = $type;
        $faq->question = $question;
        $faq->answer = $answer;
        $faq->save();
          $response = array(
              'options' =>$faq, 'status' => 1 
          );
      } catch (\Exception $e) {
          $response = array(
              'exception' => true,
              'exception_message' => $e->getMessage(),
          );
          $statusCode = 400;
      } finally {
          return response()->json($response, $statusCode);
      }
   

   
        
       
    }

    public function view_faq(Request $request)
    { $statusCode = 200;
      $faq = tbl_faq::where('type',$request->type)->select('*')->get();
      return response()->json($faq, $statusCode);
    }
    public function update(Request $request)
    
    {  $statusCode = 200;
     
      if (!$request->ajax()) {
          $statusCode = 400;
          $response = array('error' => 'Error occured in form submit.');
          return response()->json($response, $statusCode);
      }
     
      /******Validation*******/
      $this->validate($request, [
          'type' => 'required',
          'question' => 'required',
          'answer' => 'required',
              ], [
          'type.required' => 'Please select type',
          'question.required' => 'Please enter a question',
          'answer.required' => 'Please  enter an answer',
      ]);
      try {
        $type=$request->type;
        $question=$request->question;
        $answer=$request->answer;
         
    
        $faq =tbl_faq::find($request->editid);
        $faq->type = $type;
        $faq->question = $question;
        $faq->answer = $answer;
        $faq->save();
          $response = array(
              'options' =>$faq, 'status' => 1 
          );
      } catch (\Exception $e) {
          $response = array(
              'exception' => true,
              'exception_message' => $e->getMessage(),
          );
          $statusCode = 400;
      } finally {
          return response()->json($response, $statusCode);
      }
    }
    public function delete(Request $request){
   
    $statusCode = 200;
    

    try {
        $tbl_faq = tbl_faq::where('id', '=', $request->editid); //Should be changed #27

        if (!empty($tbl_faq)) {
            $tbl_faq = $tbl_faq->delete();
        }
        $response = array(
            'tbl_faq' => $tbl_faq //Should be changed #32
        );
    } catch (\Exception $e) {
        $response = array(
            'exception' => true,
            'exception_message' => $e->getMessage(),
            'exception_code'=>$e->getCode()
        );
        $statusCode = 400;
    } finally {
        return response()->json($response, $statusCode);
    }
    }
}
