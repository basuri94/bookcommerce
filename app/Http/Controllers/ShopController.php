<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop;
use App\User;
use App\Seller;
use App\BusinessSetting;
use App\Mail\SellerSignupMail;
use App\Models\Category;

use App\Mail\SellerVerificationMail;
use Hash;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
class ShopController extends Controller
{

    public function __construct()
    {
        $this->middleware('user', ['only' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shop = Auth::user()->shop;
        return view('frontend.seller.shop', compact('shop'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check() && Auth::user()->user_type == 'admin') {
            flash(__('Admin can not be a seller'))->error();
            return back();
        } else {
            if (Auth::check() && Auth::user()->user_type == 'customer') {
                Auth::logout();
            }
            $category = Category::select('name', 'id')->get();
            return view('frontend.seller_form', compact('category'));
        }
    }
    /**
     * Send otp.
     *@param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function vendor_send_otp(Request $request)
    { //dd($request->all());
        if (Auth::check() && Auth::user()->user_type == 'admin') {
            flash(__('Admin can not be a seller'))->error();
            return back();
        } else {
            $statusCode = 200;
            if (!$request->ajax()) {
                $statusCode = 400;
                $response = array('error' => 'Error occured in Ajax Call.');
                return response()->json($response, $statusCode);
            }
            $this->validate($request, [

                'user_phone' => 'required|regex:/^\\+[0-9]+$/|min:13',

            ], [

                'user_phone.required' => 'Mobile No is required',
                
                'user_phone.regex' => 'Invalid Mobile No',
                'user_phone.min' => 'Mobile No is not valid',
            ]);
            try {
                $length = 5;
                $phone = $request->user_phone;
                $email = $request->user_email;
                $check1= \DB::table("users")->where(function ($query) use ($email, $phone) {
                    $query->where('email', '=', $email)
                        ->orWhere('phone_no', '=', $phone);
                })->where('user_type', "seller")->get();
                //  $check1_l2v ="<strong>Sorry!</strong> Phone No or Email Already Exist!"; 
                if ($check1->count() > 0) {
                    $response = array(
                        'status' => 0,
                        'message' => "<strong>Sorry!</strong> Phone No or Email Already Exist!",
                    );
                } else {
                    $otp = substr(str_shuffle(str_repeat($x = '0123456789', ceil($length / strlen($x)))), 2, $length);
                   // $otp1 = hash('sha256', ("L2v" . $otp));
                     //$otp='12345';
                    //   $otp1='12345';
                    session(['OTP' => $otp]);
                    session(['TIMEOUT' => time() + (60 * 5)]);
                    $data_base[0]['body'] =env("SELLER_REGISTRATION_OTP"); // Tests

                    $vars = array(
                      '{$otp}'       => $otp
                    );
                    
                     $msg=strtr($data_base[0]['body'], $vars); 
                    //$msg = "Your One Time Password $otp for registering in Local2Vocal is .";

                    // Account details
                    $apiKey = urlencode("PBnYEfajueU-ZLgRhC24TA3poA2CQ1TSzFMndmR29d");
                    // Message details

                    $sender = urlencode("LOVOCL");
                    $message = rawurlencode($msg);


                    // Prepare data for POST request
                    $data = array("apikey" => $apiKey, "numbers" => $phone, "sender" => $sender, "message" => $message);
                    // Send the POST request with cURL
                    $url = "https://api.textlocal.in/send/";

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $buffer = curl_exec($ch);
                    curl_close($ch);
                    //return $buffer;
                    if (empty($buffer)) {
                        $response = array(
                            'status' => 0,
                            'message' => "<strong>Sorry!</strong> Some Error Occurred! Try Again.",
                        );
                    } else {
                        // session(['OTPSTATUS' => "TRUE"]);
                        $response = array(
                            'status' => 1,
                            // 'otp' => $otp
                        );
                    }
                }
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function check_otp_for_signup(Request $request)
    {
        if (Auth::check() && Auth::user()->user_type == 'admin') {
            flash(__('Admin can not be a seller'))->error();
            return back();
        } else {

            $statusCode = 200;
            if (!$request->ajax()) {
                $statusCode = 400;
                $response = array('error' => 'Error occured in Ajax Call.');
                return response()->json($response, $statusCode);
            }
            $this->validate($request, [

                'otp' => 'required|regex:/^[a-zA-Z0-9]+$/',
            ], [

                'otp.required' => 'OTP is required',
                'otp.regex' => 'OTP Must contain Alphanumeric value',
            ]);
            try {
                if (session()->get('TIMEOUT') >= time()) {
                    $otp = $request->otp;
                    $user_email=$request->user_email;
                   
                    $password_confirmation=$request->password_confirmation;
                    //$otp=$request->otp;
                    if ($otp == session()->get('OTP')) {
                        $response = array(
                            'status' => 1,
                            'message' => "OTP verified successfully Please check your mail for login detais.",
                        );
                        
                        session()->forget(['OTP', 'TIMEOUT']);
                        $array['from'] = env('MAIL_USERNAME');
                        $array['name'] = $request->fname . " " . $request->lname;
                        $array['password_confirmation'] =  $password_confirmation;
                        $array['user_email'] = $user_email;
                        Mail::to($request->user_email)->queue(new SellerSignupMail($array));

                      
                       
                    } else {
                        $response = array(
                            'status' => 0,
                            'message' => "OTP Not valid, Try Again.",
                        );
                    }
                } else {
                    $response = array(
                        'status' => 0,
                        'message' => "OTP Expired! Please Generate A New One!",
                    );
                }
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { //dd($request->all());
        $this->validate($request, [
            'email' => 'required',
            'phone_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/',
            'password' => 'required',
            'password_confirmation' => 'required|same:password',
            'name' => 'required|unique:shops,name',
            'gst' => 'required|unique:sellers,gst',
        ], [

            'phone_no.required' => 'Mobile No is required',
            'email.required' => 'Email is required',
            'phone.regex' => 'Mobile No Must Be Digit',
            'password.required' => 'Password is required',
            'password_confirmation.required' => 'Confirmation Password is required',
            'password_confirmation.same' => 'Password and confirmation password are not same',
            'name.required' => 'Business name  is required',
            'name.unique' => 'This business name is already registered within our system',
            'gst.required' => 'GST is required',
            'gst.unique' => 'This GST no is already registered within our system'
        ]);

        // DB::beginTransaction();

        // try {
            $user = null;
        $phone = $request->phone_no;
                $email = $request->email;
        if (!Auth::check()) {
            $check1= \DB::table("users")->where(function ($query) use ($email, $phone) {
                $query->where('email', '=', $email)
                    ->orWhere('phone_no', '=', $phone);
            })->where('user_type', "seller")->get();
            if ($check1->count() > 0) {
                flash(__('Phone No Or Email already exists!'))->error();
                return back();
            }
            if ($request->password == $request->password_confirmation) {
                $user = new User;
                $user->name = $request->fname . " " . $request->lname;
                $user->email = $request->email;
                $user->phone_no = $request->phone_no;
                $user->user_type = "seller";
                // $user->address = $request->address_business;
                // $user->city = $request->city;
                // $user->postal_code = $request->postal_code;
                $user->password = \Hash::make($request->password);
                $user->save();
            } else {
                flash(__('Sorry! Password did not match.'))->error();
                return back();
            }
        } else {
            $user = Auth::user();
            if ($user->customer != null) {
                $user->customer->delete();
            }
            $user->user_type = "seller";
            $user->save();
        }

        if (BusinessSetting::where('type', 'email_verification')->first()->value != 1) {
            $user->email_verified_at = date('Y-m-d H:m:s');
            $user->save();
        }

        $seller = new Seller;
        $seller->user_id = $user->id;


        // $data = array();
        // $i = 0;
        // foreach (json_decode(BusinessSetting::where('type', 'verification_form')->first()->value) as $key => $element) {
        //     $item = array();
        //     if ($element->type == 'text') {
        //         $item['type'] = 'text';
        //         $item['label'] = $element->label;
        //         $item['value'] = $request['element_'.$i];
        //     }
        //     elseif ($element->type == 'select' || $element->type == 'radio') {
        //         $item['type'] = 'select';
        //         $item['label'] = $element->label;
        //         $item['value'] = $request['element_'.$i];
        //     }
        //     elseif ($element->type == 'multi_select') {
        //         $item['type'] = 'multi_select';
        //         $item['label'] = $element->label;
        //         $item['value'] = json_encode($request['element_'.$i]);
        //     }
        //     elseif ($element->type == 'file') {
        //         $item['type'] = 'file';
        //         $item['label'] = $element->label;
        //         $item['value'] = $request['element_'.$i]->store('uploads/verification_form');
        //     }
        //     array_push($data, $item);
        //     $i++;
        // }

        // $seller->verification_info = json_encode($data);
        $seller->business_nature = $request->business_nature;
        $seller->hsn_code = $request->hsn_code;
        $seller->product_category = $request->product_category;
     //   $seller->address_business = $request->address_business;
        $seller->pan_card = $request->pan_card;
      //  $seller->pincode = $request->pincode;



        if (!empty($request->file('file_pan'))) {
            $file_pan = $request->file('file_pan');
            $file_ext = $file_pan->getClientOriginalExtension();
            $filename_upload = date("dmYhms") . "PAN" . rand(101, 99999) . "." . $file_ext;
            $destination_path_pan = "uploads/verification_form";
            $file_pan->move($destination_path_pan, $filename_upload);
            $seller->file_pan = $filename_upload;
        }

        if (!empty($request->file('file_gst'))) {
            $file_gst = $request->file('file_gst');
            $file_ext = $file_gst->getClientOriginalExtension();
            $filegst_upload = date("dmYhms") . "GST" . rand(101, 99999) . "." . $file_ext;
            $destination_path_gst = "uploads/verification_form";
            $file_gst->move($destination_path_gst, $filegst_upload);
            $seller->file_gst = $filegst_upload;
        }


        if (!empty($request->file('cancelled_cheque'))) {
            $cancelled_cheque = $request->file('cancelled_cheque');
            $file_ext = $cancelled_cheque->getClientOriginalExtension();
            $filecheque_upload = date("dmYhms") . "CHEQUE" . rand(101, 99999) . "." . $file_ext;
            $destination_path_cheque = "uploads/verification_form";
            $cancelled_cheque->move($destination_path_cheque, $filecheque_upload);
            $seller->cancelled_cheque = $filecheque_upload;
        }


        if (!empty($request->file('trademark_icon'))) {
            $trademark_icon = $request->file('trademark_icon');
            $file_ext = $trademark_icon->getClientOriginalExtension();
            $filetrademark_upload = date("dmYhms") . "ICON" . rand(101, 99999) . "." . $file_ext;
            $destination_path_trade = "uploads/verification_form";
            $trademark_icon->move($destination_path_trade, $filetrademark_upload);
            $seller->trademark_icon = $filetrademark_upload;
        }
        if (!empty($request->file('product_licence'))) {
            $product_licence = $request->file('product_licence');
            $file_ext = $product_licence->getClientOriginalExtension();
            $fileproduct_licence = date("dmYhms") . "INV" . rand(101, 99999) . "." . $file_ext;
            $destination_path_product_licence = "uploads/verification_form";
            $product_licence->move($destination_path_product_licence, $fileproduct_licence);
            $seller->product_licence = $fileproduct_licence;
        }
        // if (!empty($request->file('product_invoice'))) {
        //     $product_invoice = $request->file('product_invoice');
        //     $file_ext = $product_invoice->getClientOriginalExtension();
        //     $fileproduct_invoice = date("dmYhms") . "PL" . rand(101, 99999) . "." . $file_ext;
        //     $destination_path_product_invoice = "uploads/verification_form";
        //     $product_invoice->move($destination_path_product_invoice, $fileproduct_invoice);
        //     $seller->product_invoice = $fileproduct_invoice;
        // }

        $seller->gst = $request->gst;
        $seller->benificiary_name = $request->benificiary_name;
        $seller->bank_acc_no = $request->bank_acc_no;
        $seller->ifsc_code = $request->ifsc_code;
        $seller->bname = $request->bname;
        if(!empty($request->other_category)){
            $seller->other_category = $request->other_category;
        }
       



        $seller->save();


        if (Shop::where('user_id', $user->id)->first() == null) {
            $shop = new Shop;
            $shop->user_id = $user->id;
            $shop->name = $request->name;
            $shop->address = $request->address_business;
            $shop->city = $request->city;
            $shop->postal_code = $request->postal_code;
            $shop->slug = preg_replace('/\s+/', '-', $request->name) . '-' . $shop->id;

            if ($shop->save()) {
                auth()->login($user, false);
        
                flash(__('Your Shop has been created successfully!'))->success();
                return redirect()->route('shops.index');
            } else {
                $seller->delete();
                $user->user_type == 'customer';
                $user->save();
            }
        }

        flash(__('Sorry! Something went wrong.'))->error();
        return back();
        
            // DB::commit();
            // all good
    //     } catch (\Exception $e) {
    //         DB::rollback();
    //         //return $e->getMessage();
    //   flash(__('Sorry! Something went wrong.'))->error();
    //      return back();
    //     }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

//dd($request->all());
        $shop = Shop::find($id);

        if ($request->has('name') && $request->has('address_business')) {
            $shop->name = $request->name;
            if ($request->has('shipping_cost')) {
                $shop->shipping_cost = $request->shipping_cost;
            }
            $shop->address = $request->address_business;
            $shop->city = $request->city;
            $shop->postal_code = $request->postal_code;
            $shop->slug = preg_replace('/\s+/', '-', $request->name) . '-' . $shop->id;

            $shop->meta_title = $request->meta_title;
            $shop->meta_description = $request->meta_description;

            if ($request->hasFile('logo')) {
                $shop->logo = $request->logo->store('uploads/shop/logo');
            }

            if ($request->has('pick_up_point_id')) {
                $shop->pick_up_point_id = json_encode($request->pick_up_point_id);
            } else {
                $shop->pick_up_point_id = json_encode(array());
            }
        } elseif ($request->has('facebook') || $request->has('google') || $request->has('twitter') || $request->has('youtube') || $request->has('instagram')) {
            $shop->facebook = $request->facebook;
            $shop->google = $request->google;
            $shop->twitter = $request->twitter;
            $shop->youtube = $request->youtube;
        } else {
            if ($request->has('previous_sliders')) {
                $sliders = $request->previous_sliders;
            } else {
                $sliders = array();
            }

            if ($request->hasFile('sliders')) {
                foreach ($request->sliders as $key => $slider) {
                    array_push($sliders, $slider->store('uploads/shop/sliders'));
                }
            }

            $shop->sliders = json_encode($sliders);
        }

        if ($shop->save()) {
            flash(__('Your Shop has been updated successfully!'))->success();
            return back();
        }

        flash(__('Sorry! Something went wrong.'))->error();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verify_form(Request $request)
    {
        if (Auth::user()->seller->verification_info == null) {
            $shop = Auth::user()->shop;
            return view('frontend.seller.verify_form', compact('shop'));
        } else {
            flash(__('Sorry! You have sent verification request already.'))->error();
            return back();
        }
    }

    public function verify_form_store(Request $request)
    {
        $data = array();
        $i = 0;
        foreach (json_decode(BusinessSetting::where('type', 'verification_form')->first()->value) as $key => $element) {
            $item = array();
            if ($element->type == 'text') {
                $item['type'] = 'text';
                $item['label'] = $element->label;
                $item['value'] = $request['element_' . $i];
            } elseif ($element->type == 'select' || $element->type == 'radio') {
                $item['type'] = 'select';
                $item['label'] = $element->label;
                $item['value'] = $request['element_' . $i];
            } elseif ($element->type == 'multi_select') {
                $item['type'] = 'multi_select';
                $item['label'] = $element->label;
                $item['value'] = json_encode($request['element_' . $i]);
            } elseif ($element->type == 'file') {
                $item['type'] = 'file';
                $item['label'] = $element->label;
                $item['value'] = $request['element_' . $i]->store('uploads/verification_form');
            }
            array_push($data, $item);
            $i++;
        }
        $seller = Auth::user()->seller;
        $seller->verification_info = json_encode($data);
        if ($seller->save()) {
            flash(__('Your shop verification request has been submitted successfully!'))->success();
            return redirect()->route('dashboard');
        }

        flash(__('Sorry! Something went wrong.'))->error();
        return back();
    }

    public function approvalStatus()
    {
        $category = Category::select('name', 'id')->get();
        $sellerdata = Seller::where('user_id', Auth::user()->id)->first();

        if ($sellerdata->verification_status == 1) {
            return redirect()->route('dashboard');
        }
        return view('frontend.seller.seller_approval', compact('category', 'userdata'));
    }

    public function sellerUpdate($id, Request $request)
    {
        //  dd($request->all());
        $user = User::find(Auth::user()->id);
        $user->name = $request->fname . " " . $request->lname;
        $user->save();
        $updatedata=([
            'address'=>$request->address_business,
            'city'=>$request->city,
            'postal_code'=>$request->postal_code

        ]);
           
       
        
        Shop::where('user_id',Auth::user()->id)->update($updatedata);

        $seller = Seller::find($id);

        $seller->verification_status = 0;


        $seller->business_nature = $request->business_nature;
        $seller->hsn_code = $request->hsn_code;
        $seller->product_category = $request->product_category;
       
        $seller->pan_card = $request->pan_card;
      


        if (!empty($request->file('file_pan'))) {
            $file_pan = $request->file('file_pan');
            $file_ext = $file_pan->getClientOriginalExtension();
            $filename_upload = date("dmYhms") . "PAN" . rand(101, 99999) . "." . $file_ext;
            $destination_path_pan = "uploads/verification_form";
            $file_pan->move($destination_path_pan, $filename_upload);
            $seller->file_pan = $filename_upload;
        }

        if (!empty($request->file('file_gst'))) {
            $file_gst = $request->file('file_gst');
            $file_ext = $file_gst->getClientOriginalExtension();
            $filegst_upload = date("dmYhms") . "GST" . rand(101, 99999) . "." . $file_ext;
            $destination_path_gst = "uploads/verification_form";
            $file_gst->move($destination_path_gst, $filegst_upload);
            $seller->file_gst = $filegst_upload;
        }



        if (!empty($request->file('cancelled_cheque'))) {
            $cancelled_cheque = $request->file('cancelled_cheque');
            $file_ext = $cancelled_cheque->getClientOriginalExtension();
            $filecheque_upload = date("dmYhms") . "CHEQUE" . rand(101, 99999) . "." . $file_ext;
            $destination_path_cheque = "uploads/verification_form";
            $cancelled_cheque->move($destination_path_cheque, $filecheque_upload);
            $seller->cancelled_cheque = $filecheque_upload;
        }


        if (!empty($request->file('trademark_icon'))) {
            $trademark_icon = $request->file('trademark_icon');
            $file_ext = $trademark_icon->getClientOriginalExtension();
            $filetrademark_upload = date("dmYhms") . "ICON" . rand(101, 99999) . "." . $file_ext;
            $destination_path_trade = "uploads/verification_form";
            $trademark_icon->move($destination_path_trade, $filetrademark_upload);
            $seller->trademark_icon = $filetrademark_upload;
        }

        if (!empty($request->file('product_licence'))) {
            $product_licence = $request->file('product_licence');
            $file_ext = $product_licence->getClientOriginalExtension();
            $fileproduct_licence = date("dmYhms") . "INV" . rand(101, 99999) . "." . $file_ext;
            $destination_path_product_licence = "uploads/verification_form";
            $product_licence->move($destination_path_product_licence, $fileproduct_licence);
            $seller->product_licence = $fileproduct_licence;
        }
        // if (!empty($request->file('product_invoice'))) {
        //     $product_invoice = $request->file('product_invoice');
        //     $file_ext = $product_invoice->getClientOriginalExtension();
        //     $fileproduct_invoice = date("dmYhms") . "PL" . rand(101, 99999) . "." . $file_ext;
        //     $destination_path_product_invoice = "uploads/verification_form";
        //     $product_invoice->move($destination_path_product_invoice, $fileproduct_invoice);
        //     $seller->product_invoice = $fileproduct_invoice;
        // }
        $seller->gst = $request->gst;
        $seller->bank_acc_no = $request->bank_acc_no;
        $seller->ifsc_code = $request->ifsc_code;
        $seller->bname = $request->bname;
        if(!empty($request->other_category)){
            $seller->other_category = $request->other_category;
        }



        $seller->save();
        flash(__('Your profile updated successfully!'))->success();
        return back();
    }


    public function checkgst_shop_for_signup(Request $request)
    {
        $statusCode = 200;
        $this->validate($request, [

            'name' => 'required|unique:shops,name',
            'gst' => 'required|unique:sellers,gst',
        ], [


            'name.required' => 'Business name  is required',
            'name.unique' => 'This business name is already registered within our system',
            'gst.required' => 'GST is required',
            'gst.unique' => 'This GST no is already registered within our system'
        ]);

        try {
            $response = array('status' => 1);
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }
}
