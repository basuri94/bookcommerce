<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\SubSubCategory;
use App\SubCategoryHsn;
use App\Brand;
use App\Product;
use App\Language;
use Illuminate\Support\Str;

class SubCategoryHsnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sort_search =null;
        $subcategoryhsns = SubCategoryHsn::orderBy('created_at', 'desc');
        if ($request->has('search')){
            $sort_search = $request->search;
            $subcategoryhsns = $subcategoryhsns->where('name', 'like', '%'.$sort_search.'%');
        }
        $subcategoryhsns = $subcategoryhsns->paginate(10000000000);
        return view('subcategoryhsns.index', compact('subcategoryhsns', 'sort_search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $brands = Brand::all();
        return view('subcategoryhsns.create', compact('categories', 'brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subsubcategory = new SubCategoryHsn;
        $subsubcategory->hns_no = $request->hns_no;
        $subsubcategory->sub_category_id = $request->sub_category_id;
        //$subsubcategory->attributes = json_encode($request->choice_attributes);
        //$subsubcategory->brands = json_encode($request->brands);
        $subsubcategory->gst_amount = $request->gst_amount;

        // $data = openJSONFile('en');
        // $data[$subsubcategory->hns_no] = $subsubcategory->hns_no;
        // saveJSONFile('en', $data);

        if($subsubcategory->save()){
            flash(__('SubCategory Wise HSN has been inserted successfully'))->success();
            return redirect()->route('subcategoryhsns.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subsubcategory = SubCategoryHsn::findOrFail(decrypt($id));
        $categories = Category::all();
        $brands = Brand::all();
        return view('subcategoryhsns.edit', compact('subsubcategory', 'categories', 'brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subsubcategory = SubCategoryHsn::findOrFail($id);

        foreach (Language::all() as $key => $language) {
            $data = openJSONFile($language->code);
            unset($data[$subsubcategory->name]);
            $data[$request->name] = "";
            saveJSONFile($language->code, $data);
        }

        $subsubcategory->hns_no = $request->hns_no;
        $subsubcategory->sub_category_id = $request->sub_category_id;
        //$subsubcategory->attributes = json_encode($request->choice_attributes);
        //$subsubcategory->brands = json_encode($request->brands);
        $subsubcategory->gst_amount = $request->gst_amount;

        if($subsubcategory->save()){
            flash(__('SubCategory Wise HSN has been updated successfully'))->success();
            return redirect()->route('subcategoryhsns.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subsubcategory = SubCategoryHsn::findOrFail($id);
        if(SubCategoryHsn::destroy($id)){
            // foreach (Language::all() as $key => $language) {
            //     $data = openJSONFile($language->code);
            //     unset($data[$subsubcategory->hns_no]);
            //     saveJSONFile($language->code, $data);
            // }
            flash(__('SubCategory Wise HSN has been deleted successfully'))->success();
            return redirect()->route('subcategoryhsns.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    public function get_subcategoryhsns_by_subcategory(Request $request)
    {
        $subcategoryhsns = SubCategoryHsn::where('sub_category_id', $request->subcategory_id)->get();
        return $subcategoryhsns;
    }

    // public function get_brands_by_subsubcategory(Request $request)
    // {
    //     $brand_ids = json_decode(SubSubCategory::findOrFail($request->subsubcategory_id)->brands);
    //     $brands = array();
    //     foreach ($brand_ids as $key => $brand_id) {
    //         array_push($brands, Brand::findOrFail($brand_id));
    //     }
    //     return $brands;
    // }

    // public function get_attributes_by_subsubcategory(Request $request)
    // {
    //     $attribute_ids = json_decode(SubSubCategory::findOrFail($request->subsubcategory_id)->attributes);
    //     $attributes = array();
    //     foreach ($attribute_ids as $key => $attribute_id) {
    //         if(\App\Attribute::find($attribute_id) != null){
    //             array_push($attributes, \App\Attribute::findOrFail($attribute_id));
    //         }
    //     }
    //     return $attributes;
    // }
}
