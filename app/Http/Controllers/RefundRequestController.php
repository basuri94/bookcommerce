<?php

namespace App\Http\Controllers;
use App\Http\Controllers\OrderController;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Wallet;
use App\RefundRequest;
use App\CuriorRefund;
use App\User;
use App\Shop;
use App\Product;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Razorpay\Api\Api;
use App\Mail\RefundRequestMail;
use App\Http\Controllers\Auth\RegisterController;
use Session;
use App\Mail\WalletMail;




class RefundRequestController extends Controller
{
    public function refund()
    {
        $refund = DB::table('refund_request')
            ->orderBy('code', 'desc')
            ->leftjoin('order_details', 'order_details.id', '=', 'refund_request.order_id')
            ->leftjoin('orders', 'orders.id', '=', 'order_details.order_id')
            ->leftjoin('products', 'products.id', '=', 'refund_request.product_id')
            
            ->where('refund_request.userid', Auth::user()->id)
           // ->where('type','Refund')

            ->select('refund_request.*', 'products.name', 'orders.code')
            ->paginate(10);
            $return = DB::table('refund_request')
            ->orderBy('code', 'desc')
            ->leftjoin('order_details', 'order_details.id', '=', 'refund_request.order_id')
            ->leftjoin('orders', 'orders.id', '=', 'order_details.order_id')
            ->leftjoin('products', 'products.id', '=', 'refund_request.product_id')
            ->where('refund_request.userid', Auth::user()->id)
//->where('type','Return')

            ->select('refund_request.*', 'products.name', 'orders.code')
            ->paginate(10);
        return view('frontend.refund.index', compact('refund','return'));
    }


    public function addrefund($id)
    {
        $decryptid = decrypt($id);
   
    
        $getOrderDet = Order::join('order_details', 'order_details.order_id', '=', 'orders.id')

            ->join('products', 'products.id', '=', 'order_details.product_id')
            ->where('order_details.id', $decryptid)
            ->select('order_details.id', 'orders.payment_type', 'orders.code', 'products.name', 'order_details.price', 'order_details.tax', 'order_details.shipping_cost', 'products.id as product_id')->first();
        return view('frontend.refund.addrefund', compact('getOrderDet'));
    }


    public function store(Request $request)
    {
        
      
        $checking = RefundRequest::where('order_id', $request->order_id)->where('status','Pending')->where('userid', Auth::user()->id)->count();

        if ($checking > 0) {
            flash(__('Your Already Sent A Refund Request For This Order'))->error();
            return redirect()->route('refund.index');
        }

        $checking1 = RefundRequest::where('order_id', $request->order_id)->where('status','Approved')->where('userid', Auth::user()->id)->count();

        if ($checking1 > 0) {
            flash(__('Your Refund Request Approved Please Check your Account Balance.'))->error();
            return redirect()->route('refund.index');
        }
        
        $save = new RefundRequest();
        $save->order_id = $request->order_id;
        $save->product_id = $request->product_id;
        $save->type_refund = $request->type_refund;
        $save->reason = $request->reason;
        $save->amount = $request->amount;
        $save->status = "Pending";
        $save->type=$request->type_select;
        if($request->hasFile('photos')){
            $save->photos = $request->photos->store('uploads/refund');
        }
      
        $save->userid = Auth::user()->id;
     
        if ($save->save()) {
            $orderid=Orderdetail::where('order_details.id',$request->order_id)->value('order_id');
        $product_det=Product::where('id',$request->product_id)->first();
        $array['from'] = env('MAIL_USERNAME');
        $array['name'] = $product_det->user->name;
        $array['order_id'] = Order::where('id',$orderid)->value('code');
        $array['product_name'] = $product_det->name;
        $array['product_amount'] = $request->amount;
        $array['type_select'] = $request->type_select;
        $array['type_select_from_seller'] = '';
        $array['sellerrefundreject'] = 0;
        $array['sellerrefundapprove'] = 0;
        Mail::to($product_det->user->email)->queue(new RefundRequestMail($array));
            flash(__('Return/Refund Request Sent successfully'))->success();
            // if($request->type_select=="Return"){
               
            // }
            // else{
            //     flash(__('Refund Request Sent successfully'))->success();
            // }
         
            return redirect()->route('refund.index');
        } else {
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    public function refer_show(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }

        try {
            $userid = Auth::user()->id;
            $getrefercode = User::where('id', $userid)->value('referral_code');
         //   $referlink = env('APP_URL') . '/users/registration?referral_code=' . $getrefercode;
            $referlink=url('/users/registration?referral_code=' .  $getrefercode);
            $html = "";
            $html .= '<div class="input-group input-group--style-1" style="padding:18px;">';
            $html .= ' <span class="log_addon input-group-addon"><i class="text-md fa fa-link"></i>';
            $html .= '</span><input type="text" readonly value="' . $referlink . '" id="myInput" class="log_input form-control">';
            $html .= '&nbsp;<button class="btn btn-primary"  onclick="myFunction();">Copy Link</button>';
            $html .= '</div>';
            // $html .='<input type="text" class="form-control" readonly value="'.$referlink.'" id="myInput">';



            $response = array(
                'status' => 1,
                'html' => $html,
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }







    /****************Seller************************* */


    public function sellerefundlist(Request $request)
    {
        $sort_search = null;
        $datarefund = RefundRequest::orderBy('refund_request.id', 'desc')
        ->leftjoin('order_details', 'order_details.id', '=', 'refund_request.order_id')
        ->leftjoin('orders', 'orders.id', '=', 'order_details.order_id')
            ->leftjoin('products', 'products.id', '=', 'refund_request.product_id')
            ->where('products.user_id', Auth::user()->id)
           // ->where('type','Refund')
            ->select('refund_request.*', 'products.name', 'orders.code');

            $datareturn = RefundRequest::orderBy('refund_request.id', 'desc')
            ->leftjoin('order_details', 'order_details.id', '=', 'refund_request.order_id')
            ->leftjoin('orders', 'orders.id', '=', 'order_details.order_id')
            ->leftjoin('products', 'products.id', '=', 'refund_request.product_id')
            ->where('products.user_id', Auth::user()->id)
         //   ->where('type','Return')
            ->select('refund_request.*', 'products.name', 'orders.code');
        // if ($request->has('search')) {
        //     $sort_search = $request->search;
        //     // $user_ids = User::where('user_type', 'customer')->where(function($user) use ($sort_search){
        //     //     $user->where('name', 'like', '%'.$sort_search.'%')->orWhere('email', 'like', '%'.$sort_search.'%');
        //     // })->pluck('id')->toArray();
        //     // $customers = $customers->where(function($customer) use ($user_ids){
        //     //     $customer->whereIn('user_id', $user_ids);
        //     // });
        // }
        $datarefund = $datarefund->paginate(15);
        $datareturn = $datareturn->paginate(15);
        return view('frontend.seller.refund.index', compact('datarefund', 'sort_search','datareturn'));
    }

    public function sellerrefundview(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }

        try {

            $refundid = $request->refundid;
           // $type=$request->type;
            $getOrderDet = RefundRequest::leftjoin('order_details', 'order_details.id', '=', 'refund_request.order_id')
            ->leftjoin('orders', 'orders.id', '=', 'order_details.order_id')
                ->leftjoin('products', 'products.id', '=', 'refund_request.product_id')
                ->select('refund_request.*', 'products.name', 'orders.code','order_details.id as orderdetails_id')->where('refund_request.id', $refundid)->first();

            $html = "";
            $footer = "";
            $html .= ' <input type="hidden" class="form-control mb-3" value="' . $getOrderDet->id . '" id="refundid" name="product">';
            $html .= ' <input type="hidden" class="form-control mb-3" value="' . $getOrderDet->orderdetails_id . '" id="orderdetails_id" name="orderdetails_id">';
            
            $html .= '<div class="row">';
            $html .= '<div class="col-md-2">';
            $html .= '<label>' . translate('Product Name') . ' <span class=""></span></label>';
            $html .= '</div>';
            $html .= '<div class="col-md-10">';
            $html .= ' <input type="text" class="form-control mb-3" disabled value="' . $getOrderDet->name . '" name="product" placeholder="' . translate('Product Name') . '" required>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="row">';
            $html .= ' <div class="col-md-2">';
            $html .= '<label>' . translate('Order Code') . ' <span class=""></span></label>';
            $html .= '</div>';
            $html .= '<div class="col-md-10">';
            $html .= '<input type="text" class="form-control mb-3" name="order" disabled value="' . $getOrderDet->code . '" placeholder="' . translate('Order Code') . '" required>';
            $html .= '</div>';
            $html .= '</div>';

            $html .= '<div class="row">';
            $html .= '<div class="col-md-2">';
            $html .= '<label>' . translate('Amount') . ' <span class=""></span></label>';
            $html .= '</div>';
            $html .= '<div class="col-md-10">';
            $html .= '<input type="text" class="form-control mb-3" disabled value="' . $getOrderDet->amount . '" name="amount" placeholder="' . translate('Amount') . '" required>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="row">';
            $html .= '<div class="col-md-2">';
            $html .= '<label>' . translate('Reason') . ' <span class=""></span></label>';
            $html .= '</div>';
            $html .= '<div class="col-md-10">';
            $html .= '<textarea type="text" class="form-control mb-3" name="reason" disabled required>' . $getOrderDet->reason . '</textarea>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="row">';
            $html .= '<div class="col-md-2">';
            $html .= '<label>' . translate('Uploaded Document') . ' <span class=""></span></label>';
            $html .= '</div>';
            $html .= '<div class="col-md-10">';
            $html .= '<a href="' .\URL::to("/").'/'. $getOrderDet->photos . '" target="_blank"><i class="fa fa-eye"></></a>';
            $html .= '</div>';
            $html .= '</div>';
          
            
           
         $tracking=CuriorRefund::where('refund_request_id', $getOrderDet->id)->first();
       

            $footer .= '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>';
            if($tracking!=null){
                if($tracking->status==1){
                    $footer .= '<button type="button" onclick="sellerrefundreturn_cancel($tracking->tracking_id)" class="btn btn-primary">Cancel courier</button>';
                }
                // else{
                //     $footer .= '<button type="button" onclick="sellerrefundreturn()" class="btn btn-primary">Return Via curiour</button>';
                // }
            }else{
                if ($getOrderDet->status == "Pending") {
                    $footer .= '<button type="button" onclick="sellerrefundreturn()" class="btn btn-primary">Return Via courier</button>';
                }
            }
            if ($getOrderDet->type == "Replacement") {
                $footer .= '<button type="button" onclick="sellerrefundreturn_replace()" class="btn btn-primary">Send Via courier</button>';
            }

            if ($getOrderDet->status == "Pending" && $getOrderDet->type == "Return") {
                $footer .= '<button type="button" onclick="sellerrefundapprove()" class="btn btn-primary">Refund Money</button>';
            }
            if ($getOrderDet->status == "Pending") {
                $footer .= '<button type="button" onclick="sellerrefundreject()" class="btn btn-primary">Reject</button>';
            }



            $response = array(
                'status' => 1,
                'html' => $html,
                'footer' => $footer
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    
    public function sellerrefundreturn(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }

        try {
            $msg="";
            $userid = Auth::user()->id;
            $refundid = $request->refundid;
            $orderdetails_id = $request->orderdetails_id;
            $refunddata = RefundRequest::where('id', $refundid)->first();

            

          

            $CuriorOrder = CuriorRefund::where('refund_request_id',$refundid)->where('order_type','Prepaid')->first();
            if($CuriorOrder!=null){
$response=array(
    'status'=>1,
    'tracking_id'=>$CuriorOrder->tracking_id
);
            }else{
                $timestamp = time();
                $appID = 4613;
                $key = "jIZz/Be60J8=";
                $secret = "j2dkS8en9l59rhwYrKcW4tQbmaCzYtBeKAUyvKUfrRKxDNvNSh72fvrkYUObVBgDO33mezg4Pg1QJl10IVYxkw==";
                $seller_id = "36422";
                $order_details=OrderDetail::findOrFail($orderdetails_id );
                if($order_details->order_id!=""){
                    $order = Order::findOrFail($order_details->order_id);
        
                    $det = json_decode($order->shipping_address);
                    $seller_address= Shop::where('user_id',$order_details->seller_id)->first();
                    $source_pin =$seller_address->postal_code;
                    $destination_pin = $det->postal_code;
                    //$order_det = $order->orderDetails->where('seller_id', Auth::user()->id);
                //echo json_encode($order_det);die;
                $amount=0;
                // foreach ($order_det as $order_details) {
                   
                    $amount=$amount+$order_details->price+$order_details->tax+$order_details->shipping_cost;
                // }
                    // if ($order->payment_type == "cash_on_delivery") {
                    //     $orderType = "2";
                    //     $invoice_value = $amount;
                    // } else {
                        $orderType = "1";
                        $invoice_value = "";
                    // }
                }
                
                $length = $order_details->length;
                $height = $order_details->height;
                $width = $order_details->width;
                $weight = $order_details->weight;
                
                $sign = "key:" . $key . "id:" . $appID . ":timestamp:" . $timestamp;
                $authtoken = rawurlencode(base64_encode(hash_hmac('sha256', $sign, $secret, true)));
                $ch = curl_init();
        
                $data = array(
                    "sourcePin" => $source_pin,
                    "destinationPin" => $destination_pin,
                    "orderType" => $orderType,
                    "length" => $length,
                    "width" => $width,
                    "height" => $height,
                    "weight" => $weight,
                    "invoiceValue" => $invoice_value
                );
        // echo json_encode($data);die;
                $data_json = json_encode($data);
        
                $header = array(
                    "x-appid: $appID",
                    "x-sellerid:$seller_id",
                    "x-timestamp: $timestamp",
                    "x-version:3", // for auth version 3.0 only
                    "Authorization: $authtoken",
                    "Content-Type: application/json",
                    "Content-Length: " . strlen($data_json)
                );
        
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://api.shyplite.com/pricecalculator');
                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $response  = curl_exec($ch);
        
                curl_close($ch);
                $response=array(
                    'status'=>2,
                    'response_data'=>$response
                );

                $orderid=Orderdetail::where('order_details.id',$refunddata->order_id)->value('order_id');
            $product_det=Product::where('id',$refunddata->product_id)->first();
            $order_det= Order::where('id',$orderid)->first();
            $array['from'] = env('MAIL_USERNAME');
            $array['name'] = $product_det->user->name;
            $array['order_id'] = $order_det->code;
            $array['product_name'] = $product_det->name;
            $array['product_amount'] = $refunddata->amount;
            $array['type_select_from_seller'] = $refunddata->type;
            $array['type_select'] = '';
            $array['sellerrefundapprove'] = 0;
            $array['sellerrefundreject'] = 0;
            
            Mail::to($order_det->user->email)->queue(new RefundRequestMail($array));
            }
        


            
            //return $response;
            
            // $msg='Return/Refund Approved Successfully';
            
            // $response = array(
            //     'status' => 1, 'msg'=>$msg

            // );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function set_order_return(Request $request)
    {
        $refundid = $request->refundid;
        $type_of_order=$request->type;
        $order_det_id=$request->orderdetails_id;
        $getorderid=OrderDetail::where('id', $order_det_id)->value('order_id');
           // $order_details=Order::find($getorderid);
        $order = Order::findOrFail($getorderid);
        $order_det = $order->orderDetails->where('id',$order_det_id);
        //echo json_encode($order_det);die;
        $skulist = array();
        $amount=0;
        $orderId = date('Ymd-His') . rand(10, 99);
        $length_total = $order_det[0]->length;
        $height_total = $order_det[0]->height;
        $width_total = $order_det[0]->width;
        $weight_total = $order_det[0]->weight;
        if ($type_of_order == "replacement") {
            $orderType = "Prepaid";
        } else {
            $orderType = "Reverse";
        }
        $CuriorOrder=new CuriorRefund();
        $CuriorOrder->refund_request_id=$refundid;
        $CuriorOrder->order_type=$orderType;
        $CuriorOrder->tracking_id=$orderId;
        $CuriorOrder->curior_mode=$request->mode_type;
        $CuriorOrder->curior_charge=$request->dv_cost;
        $CuriorOrder->save();
        foreach ($order_det as $order_details) {
            $product = array();
            $variation = $order_details->variation;
            $stock = $order_details->product->stocks;
            foreach ($stock as $stock_det) {
                if ($variation == $stock_det->variant) {
                    $stockdet = $stock_det->sku;
                }
            }
            
            $amount=$amount+$order_details->price+$order_details->tax+$order_details->shipping_cost;
           
            $product['sku'] = $stockdet;
            $product['itemName'] = $order_details->product->name;
            $product['quantity'] = $order_details->quantity;
            $product['price'] = $order_details->price;
            $product['itemLength'] = $order_details->length;
            $product['itemWidth'] =  $order_details->width;
            $product['itemHeight'] =  $order_details->height;
            $product['itemWeight'] =  $order_details->weight;
            array_push($skulist, $product);
        }

        $det = json_decode($order->shipping_address);
        
        $mode_type = ucfirst($request->mode_type);
        $invoice_value = $amount;
        
        
        $customerName = $det->name;
        $customerAddress = $det->address;
        $customerCity = $det->city;
        $customerPinCode = $det->postal_code;
        $customerContact = $det->phone;
        $sellerAddressId = Auth::user()->address_id;
        $timestamp = time();
        $orderDate = date('yy-m-d');
        $appID = 4613;
        $key = "jIZz/Be60J8=";
        $secret = "j2dkS8en9l59rhwYrKcW4tQbmaCzYtBeKAUyvKUfrRKxDNvNSh72fvrkYUObVBgDO33mezg4Pg1QJl10IVYxkw==";
        $seller_id = "36422";
        $sign = "key:" . $key . "id:" . $appID . ":timestamp:" . $timestamp;
        $authtoken = rawurlencode(base64_encode(hash_hmac('sha256', $sign, $secret, true)));
        $ch = curl_init();

        $data = array(
            "orders" => array(
                "0" => array(
                    "orderId" => $orderId,
                    "customerName" => $customerName,
                    "customerAddress" => $customerAddress,
                    "customerCity" => $customerCity,
                    "customerPinCode" => $customerPinCode,
                    "customerContact" => $customerContact,
                    "orderType" => $orderType,
                    "modeType" => $mode_type,
                    "orderDate" => $orderDate,
                    "package" => array(
                        "itemLength" => $length_total,
                        "itemWidth" => $width_total,
                        "itemHeight" => $height_total,
                        "itemWeight" => $weight_total
                    ),
                    "skuList" => $skulist,
                    "totalValue" => $invoice_value,
                    "sellerAddressId" => $sellerAddressId
                )
            )
        );
        
 //echo json_encode($data);die;
        $data_json = json_encode($data);

        $header = array(
            "x-appid: $appID",
            "x-sellerid:$seller_id",
            "x-timestamp: $timestamp",
            "x-version:3", // for auth version 3.0 only
            "Authorization: $authtoken",
            "Content-Type: application/json",
            "Content-Length: " . strlen($data_json)
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.shyplite.com/order?method=sku');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        //   var_dump($response);
        //   exit;
        // curl_close($ch);
        // $order = Order::findOrFail($request->order_id);
        // $order->curior_order = 1;
        // $order->save();
       // flash('Order has been Added For Shipment')->success();
       $CuriorOrder=CuriorRefund::find($CuriorOrder->id);
        $CuriorOrder->status=1;
        $CuriorOrder->save();
        
        if ($type_of_order == "replacement") {
            $save=RefundRequest::find($refundid);
            $save->status="Product sent via courier";
            $save->save();
            $msg='Order has been Added For Shipment';
        } else {
            $save=RefundRequest::find($refundid);
        $save->status="Wait For Product Receive";
        $save->save();
        $msg='Order has been Added For reverse Shipment';
        }
        
       
            
            $response = array(
                'status' => 1, 'message'=>$msg,'tracking_id'=>$orderId

            );
return $response;
    }
    public function replace_generate_silp(Request $request)
    {
        
        $orderId = $request->order_id;
        $timestamp = time();
        $appID = 4613;
        $key = "jIZz/Be60J8=";
        $secret = "j2dkS8en9l59rhwYrKcW4tQbmaCzYtBeKAUyvKUfrRKxDNvNSh72fvrkYUObVBgDO33mezg4Pg1QJl10IVYxkw==";
        $seller_id = "36422";
        $sign = "key:" . $key . "id:" . $appID . ":timestamp:" . $timestamp;
        $authtoken = rawurlencode(base64_encode(hash_hmac('sha256', $sign, $secret, true)));
        $ch = curl_init();

        $header = array(
            "x-appid: $appID",
            "x-timestamp: $timestamp",
            "x-sellerid:$seller_id",
            "Authorization: $authtoken",
            "Content-Type: application/json",
            "x-version:3"
        );

        curl_setopt($ch, CURLOPT_URL, 'https://api.shyplite.com/getSlip?orderID=' . urlencode($orderId));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
       $response=json_decode($server_output);

       $carrierName=$response->carrierName;
       $awbNo=$response->awbNo;
       $manifestID=$response->manifestID;
       $slip_file=$response->fileName;
       $filepath=$response->s3Path[0];
     

        curl_close($ch);
        $CuriorOrder = CuriorRefund::where('tracking_id',$orderId)->first();
        $order = CuriorRefund::findOrFail($CuriorOrder->id);
        $order->carrierName =$carrierName;
        $order->awbNo =$awbNo;
        $order->manifestID =$manifestID;
        $order->s3Path_slip = $filepath;
        $order->fileName_slip =$slip_file;
        $order->save();
        return  $filepath;
        
    }

    public function replace_generate_manifest(Request $request)
    {
        $orderId=$request->order_id;
        $CuriorOrder = CuriorRefund::where('tracking_id',$orderId)->first();
        $order = CuriorRefund::findOrFail($CuriorOrder->id);
        $timestamp = time();
        $manifestID = $order->manifestID;
        $appID = 4613;
        $key = "jIZz/Be60J8=";
        $secret = "j2dkS8en9l59rhwYrKcW4tQbmaCzYtBeKAUyvKUfrRKxDNvNSh72fvrkYUObVBgDO33mezg4Pg1QJl10IVYxkw==";
        $seller_id = "36422";
        $sign = "key:" . $key . "id:" . $appID . ":timestamp:" . $timestamp;
        $authtoken = rawurlencode(base64_encode(hash_hmac('sha256', $sign, $secret, true)));
        $ch = curl_init();
        $header = array(
            "x-appid: $appID",
            "x-timestamp: $timestamp",
            "x-sellerid:$seller_id",
            "x-version:3", // for auth version 3.0 only
            "Authorization: $authtoken"
        );

        curl_setopt($ch, CURLOPT_URL, "https://api.shyplite.com/getManifestPDF/$manifestID");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        $response=json_decode($server_output);
        
        curl_close($ch);
        $save=RefundRequest::find($order->refund_request_id);
        $save->status="Approved";
        $save->save();
        $order->s3Path_manifest = $response->s3Path;
        $order->name_manifest =$response->fileName;
        $order->save();
        return $response->s3Path;
    }
    public function sellerrefundreturn_cancel_order(Request $request)
    {
        $orderId=$request->order_id;
        
        $timestamp = time();
        $appID = 4613;
        $key = "jIZz/Be60J8=";
        $secret = "j2dkS8en9l59rhwYrKcW4tQbmaCzYtBeKAUyvKUfrRKxDNvNSh72fvrkYUObVBgDO33mezg4Pg1QJl10IVYxkw==";
        $seller_id = "36422";
        $sign = "key:" . $key . "id:" . $appID . ":timestamp:" . $timestamp;
        $authtoken = rawurlencode(base64_encode(hash_hmac('sha256', $sign, $secret, true)));
        $ch = curl_init();

        $data = array(
            "orders" => [$orderId]
        );

        $data_json = json_encode($data);

        $header = array(
            "x-appid: $appID",
            "x-sellerid:$seller_id",
            "x-timestamp: $timestamp",
            "x-version:3", // for auth version 3.0 only
            "Authorization: $authtoken",
            "Content-Type: application/json",
            "Content-Length: " . strlen($data_json)
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.shyplite.com/ordercancel');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        
        curl_close($ch);
        $CuriorOrder = CuriorRefund::where('tracking_id',$orderId)->first();
        $CuriorOrder=CuriorRefund::find($CuriorOrder->id);
        $CuriorOrder->status=2;
        $CuriorOrder->save();
        return true;
    }
    public function sellerrefundapprove(Request $request)
    {
        
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }

        try {
            $msg="";
            $userid = Auth::user()->id;
            $refundid = $request->refundid;
            $refunddata = RefundRequest::where('id', $refundid)->first();

           $getorderid=OrderDetail::where('id', $refunddata->order_id)->value('order_id');
            $order_details=Order::find($getorderid);
            if($order_details->payment_type=="online"){
                if($refunddata->type_refund=="Bank"){
                    $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
                    $order_res=json_decode($order_details->payment_details);
                    $order_res_payment_id=$order_res->id;
                    //echo $order_res_payment_id;die;
                    $payment = $api->payment->fetch($order_res_payment_id);
                    $refund = $payment->refund(array('amount' => $refunddata->amount));
                    $update = RefundRequest::where('id', $refundid)->update([
                        'status' => 'Approved',
                        'refund_response'=>$refund
                    ]);
                }else{
                    $update = RefundRequest::where('id', $refundid)->update([
                        'status' => 'Approved',
                        'refund_response'=>""
                    ]);
                    $insertWallet = Wallet::insert([
                        'user_id' => $refunddata->userid,
                        'amount' => $refunddata->amount,
                        'payment_method' => 'Refund',
        
                    ]);
                    $prevbal = User::where('id', $refunddata->userid)->value('balance');
                    $totalbal = $prevbal + $refunddata->amount;
                    $updatebal = User::where('id', $refunddata->userid)->update([
                        'balance' => $totalbal
                    ]);  
                    $user=User::find($refunddata->userid);
                    $array['from'] = env('MAIL_USERNAME');
                $array['name'] = $user->name;
                $array['amount_credit'] = '';
                $array['amount_debit'] = $refunddata->amount;
                
                Mail::to($user->email)->queue(new WalletMail($array));
                $registerController= new RegisterController();
                $data_base[0]['body'] = env("WALLET_DEBIT"); // Tests
        
                $vars = array(
                    '{$name}'       => $user->name,
                    '{$amountdebit}'       => $refunddata->amount,
                   
                );
                $registerController->sms_function( $data_base[0]['body'],$vars, $user->phone_no);

                }
               
            }else{
                $update = RefundRequest::where('id', $refundid)->update([
                    'status' => 'Approved',
                    'refund_response'=>""
                ]);
                $insertWallet = Wallet::insert([
                    'user_id' => $refunddata->userid,
                    'amount' => $refunddata->amount,
                    'payment_method' => 'Refund',
    
                ]);
                $prevbal = User::where('id', $refunddata->userid)->value('balance');
                $totalbal = $prevbal + $refunddata->amount;
                $updatebal = User::where('id', $refunddata->userid)->update([
                    'balance' => $totalbal
                ]);
                $user=User::find($refunddata->userid);
                    $array['from'] = env('MAIL_USERNAME');
                $array['name'] = $user->name;
                $array['amount_credit'] = '';
                $array['amount_debit'] = $refunddata->amount;
                
                Mail::to($user->email)->queue(new WalletMail($array));
                $registerController= new RegisterController();
                $data_base[0]['body'] = env("WALLET_DEBIT"); // Tests
        
                $vars = array(
                    '{$name}'       => $user->name,
                    '{$amountdebit}'       => $refunddata->amount,
                   
                );
                $registerController->sms_function( $data_base[0]['body'],$vars, $user->phone_no);
            }

            $orderid=Orderdetail::where('order_details.id',$refunddata->order_id)->value('order_id');
            $product_det=Product::where('id',$refunddata->product_id)->first();
            $order_det= Order::where('id',$orderid)->first();
            $array['from'] = env('MAIL_USERNAME');
            $array['name'] = $product_det->user->name;
            $array['order_id'] = $order_det->code;
            $array['product_name'] = $product_det->name;
            $array['product_amount'] = $refunddata->amount;
            $array['type_select'] = '';
            $array['type_select_from_seller'] = '';
            $array['sellerrefundreject'] = 0;
            $array['sellerrefundapprove'] = 1;
            Mail::to($order_det->user->email)->queue(new RefundRequestMail($array));
            


            $msg='Return/Refund Approved Successfully';
            // if( $refunddata->type="Return"){
              
            // }
            // else{
            //     $msg='Refund Approved Successfully';
            // }
      

//$refund = $payment->refund(array('amount' => 500100)); for partial refund

            
            $response = array(
                'status' => 1, 'msg'=>$msg

            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }
    public function sellerrefundreject(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }

        try {
            $userid = Auth::user()->id;
            $refundid = $request->refundid;
            $refunddata = RefundRequest::where('id', $refundid)->first();
            $orderid=Orderdetail::where('order_details.id',$refunddata->order_id)->value('order_id');
            $product_det=Product::where('id',$refunddata->product_id)->first();
            $order_det= Order::where('id',$orderid)->first();
            $array['from'] = env('MAIL_USERNAME');
            $array['name'] = $product_det->user->name;
            $array['order_id'] = $order_det->code;
            $array['product_name'] = $product_det->name;
            $array['product_amount'] = $refunddata->amount;
            $array['type_select'] = '';
            $array['type_select_from_seller'] = '';
            $array['sellerrefundreject'] = 1;
            $array['sellerrefundapprove'] = 0;
            Mail::to($order_det->user->email)->queue(new RefundRequestMail($array));

        
            $update = RefundRequest::where('id', $refundid)->update([
                'status' => 'Rejected'
            ]);
          
            $response = array(
                'status' => 1,

            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }


    /*******************Admin************* */

    public function adminfundlist(Request $request){
       
        $allrefund = RefundRequest::orderBy('refund_request.id', 'desc')
        ->leftjoin('order_details', 'order_details.id', '=', 'refund_request.order_id')
        ->leftjoin('orders', 'orders.id', '=', 'order_details.order_id')
            ->leftjoin('products', 'products.id', '=', 'refund_request.product_id')
            ->where('products.user_id','!=', Auth::user()->id)
            ->select('refund_request.*', 'products.name', 'orders.code');

            $adminfund = RefundRequest::orderBy('refund_request.id', 'desc')
            ->leftjoin('order_details', 'order_details.id', '=', 'refund_request.order_id')
        ->leftjoin('orders', 'orders.id', '=', 'order_details.order_id')
            ->join('products', 'products.id', '=', 'refund_request.product_id')
            ->where('products.user_id', Auth::user()->id)
            ->select('refund_request.*', 'products.name', 'orders.code');
        $allrefund = $allrefund->paginate(15);
        $adminfund = $adminfund->paginate(15);
        return view('admin.refund.index', compact('allrefund','adminfund'));
    }

    public function adminrefundview(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }

        try {

            $refundid = $request->refundid;
            $getOrderDet = RefundRequest::leftjoin('order_details', 'order_details.id', '=', 'refund_request.order_id')
            ->leftjoin('orders', 'orders.id', '=', 'order_details.order_id')
                ->join('products', 'products.id', '=', 'refund_request.product_id')
                ->select('refund_request.*', 'products.name', 'orders.code','products.user_id as productuserid')->where('refund_request.id', $refundid)->first();

            $html = "";
            $footer = "";
            $html .= ' <input type="hidden" class="form-control mb-3" value="' . $getOrderDet->id . '" id="refundid" name="product">';
            $html .= '<div class="row">';
            $html .= '<div class="col-md-2">';
            $html .= '<label>' . translate('Product Name') . ' <span class=""></span></label>';
            $html .= '</div>';
            $html .= '<div class="col-md-10">';
            $html .= ' <input type="text" class="form-control mb-3" disabled value="' . $getOrderDet->name . '" name="product" placeholder="' . translate('Product Name') . '" required>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="row">';
            $html .= ' <div class="col-md-2">';
            $html .= '<label>' . translate('Order Code') . ' <span class=""></span></label>';
            $html .= '</div>';
            $html .= '<div class="col-md-10">';
            $html .= '<input type="text" class="form-control mb-3" name="order" disabled value="' . $getOrderDet->code . '" placeholder="' . translate('Order Code') . '" required>';
            $html .= '</div>';
            $html .= '</div>';

            $html .= '<div class="row">';
            $html .= '<div class="col-md-2">';
            $html .= '<label>' . translate('Amount') . ' <span class=""></span></label>';
            $html .= '</div>';
            $html .= '<div class="col-md-10">';
            $html .= '<input type="text" class="form-control mb-3" disabled value="' . $getOrderDet->amount . '" name="amount" placeholder="' . translate('Amount') . '" required>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="row">';
            $html .= '<div class="col-md-2">';
            $html .= '<label>' . translate('Reason') . ' <span class=""></span></label>';
            $html .= '</div>';
            $html .= '<div class="col-md-10">';
            $html .= '<textarea type="text" class="form-control mb-3" name="reason" disabled required>' . $getOrderDet->reason . '</textarea>';
            $html .= '</div>';
            $html .= '</div>';

            $footer .= '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>';
            if ($getOrderDet->status == "Pending" && $getOrderDet->productuserid==Auth::user()->id) {
                $footer .= '<button type="button" onclick="adminfundapprove()" class="btn btn-primary">Approve</button>';
            }



            $response = array(
                'status' => 1,
                'html' => $html,
                'footer' => $footer
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function adminfundapprove(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }

        try {
            $userid = Auth::user()->id;
            $refundid = $request->refundid;
            $refunddata = RefundRequest::where('id', $refundid)->first();
            $update = RefundRequest::where('id', $refundid)->update([
                'status' => 'Approved'
            ]);
            $insertWallet = Wallet::insert([
                'user_id' => $refunddata->userid,
                'amount' => $refunddata->amount,
                'payment_method' => 'Refund',

            ]);
            $prevbal = User::where('id', $refunddata->userid)->value('balance');
            $totalbal = $prevbal + $refunddata->amount;
            $updatebal = User::where('id', $refunddata->userid)->update([
                'balance' => $totalbal
            ]);
            $response = array(
                'status' => 1,

            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }
}
