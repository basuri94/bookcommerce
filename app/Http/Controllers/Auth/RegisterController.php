<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Customer;
use App\BusinessSetting;
use App\OtpConfiguration;
use App\Http\Controllers\Controller;
use App\Http\Controllers\OTPVerificationController;

use App\Mail\CustomerVerificationMail;
use App\Mail\ReferMail;
use App\Models\Wallet;
use App\Refer;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Mail;
use Nexmo;
use Twilio\Rest\Client;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if (filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
            ]);

            $customer = new Customer;
            $customer->user_id = $user->id;
            $customer->save();

            if (BusinessSetting::where('type', 'email_verification')->first()->value != 1) {
                $user->email_verified_at = date('Y-m-d H:m:s');
                $user->save();
                flash(__('Registration successfull.'))->success();
            } else {
                $user->sendEmailVerificationNotification();
                flash(__('Registration successfull. Please verify your email.'))->success();
            }
        } else {
            if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated) {
                $user = User::create([
                    'name' => $data['name'],
                    'phone_no' => '+' . $data['country_code'] . $data['phone'],
                    'password' => Hash::make($data['password']),
                    'verification_code' => rand(100000, 999999)
                ]);

                $customer = new Customer;
                $customer->user_id = $user->id;
                $customer->save();

                if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated) {
                    $otpController = new OTPVerificationController;
                    $otpController->send_code($user);
                }
            }
        }

        if (Cookie::has('referral_code')) {
            $referral_code = Cookie::get('referral_code');
            $referred_by_user = User::where('referral_code', $referral_code)->first();
            if ($referred_by_user != null) {
                $user->referred_by = $referred_by_user->id;
                $user->save();
            }
        }

        return $user;
    }

    public function register(Request $request)
    {
        //echo session()->get('OTP');die;
        // session()->forget(['OTP', 'TIMEOUT']);
        //Cookie::queue(Cookie::forget('referral_code'));
      



        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        $this->validate($request, [
            'email' => 'required',
            'phone' => 'required|regex:/^\\+[0-9]+$/|min:13',
            'password' => 'required',
            'password_confirmation' => 'required|same:password',
        ], [

            'phone.required' => 'Mobile No is required',
            'email.required' => 'Email is required',
            'phone.regex' => 'Invalid Mobile No',
            'phone.min' => 'Phone number is not valid',
            'password.required' => 'Password is required',
            'password_confirmation.required' => 'Confirmation Password is required',
            'password_confirmation.same' => 'Password and Confirm Password does not match',
            'email.required' => 'Email is required'
        ]);
        try {
            $length = 5;
            $phone = $request->phone;
            $email = $request->email;
            $check1 = \DB::table("users")

                ->where(function ($query) use ($email, $phone) {
                    $query->where('email', '=', $email)
                        ->orWhere('phone_no', '=', $phone);
                })->where('user_type', "customer")->get();

            if ($check1->count() > 0) {
                $response = array(
                    'status' => 0,
                    'message' => "Phone No or Email Already Exist!",
                );
            } else {
                $otp = substr(str_shuffle(str_repeat($x = '0123456789', ceil($length / strlen($x)))), 2, $length);
                $otp1 = hash('sha256', ("L2v" . $otp));
                // $otp1 = '12345';
                //  $otp = '12345';
                session(['OTP' => $otp]);
                session(['TIMEOUT' => time() + (60 * 5)]);

                $data_base[0]['body'] = env("CUSTOMER_REGISTRATION_OTP"); // Tests

                $vars = array(
                    '{$otp}'       => $otp
                );

                $msg = strtr($data_base[0]['body'], $vars);
                //    env("CUSTOMER_REGISTRATION_OTP");
                //   return  $msg=str_replace($otp,'$otp',$msg);
                //$msg = "Your One Time Password $otp for registering in Local2Vocal is .";

                // Account details
                $apiKey = urlencode("PBnYEfajueU-ZLgRhC24TA3poA2CQ1TSzFMndmR29d");
                // Message details

                $sender = urlencode("LOVOCL");
                $message = rawurlencode($msg);


                // Prepare data for POST request
                $data = array("apikey" => $apiKey, "numbers" => $phone, "sender" => $sender, "message" => $message);
                // Send the POST request with cURL
                $url = "https://api.textlocal.in/send/";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $buffer = curl_exec($ch);
                curl_close($ch);
                //return $buffer;
                if (empty($buffer)) {
                    $response = array(
                        'status' => 0,
                        'message' => "Some Error Occurred! Try Again.",
                    );
                } else {

                    $response = array(
                        'status' => 1
                    );
                }
            }
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }


    public function customer_otp_check(Request $request)
    { //dd($request->all());
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        $this->validate($request, [

            'otp' => 'required|regex:/^[a-zA-Z0-9]+$/',
        ], [

            'otp.required' => 'OTP is required',
            'otp.regex' => 'OTP Must contain Alphanumeric value',
        ]);
        try {
          
            // echo (session()->get('TIMEOUT')); echo "<br>";
            //echo time();die;
            if (session()->get('TIMEOUT') >= time()) {
                //$otp = hash('sha256', ("L2V" . $request->otp));
                $otp = $request->otp;
                $referid = NULL;
                if ($otp === session()->get('OTP')) {
                    if (Cookie::has('referral_code')) {
                        $referral_code = Cookie::get('referral_code');
                        $referred_by_user = User::where('referral_code', $referral_code)->first();
                        if (!empty($referred_by_user)) {
                            $referid = $referred_by_user->id;
                        }
                    }

                    $ref_code = substr($request->name, 0, 3) . rand(100000, 999999);
                    $array['from'] = env('MAIL_USERNAME');
                    $array['name'] = $request['name'];


                    $user = User::create([
                        'name' => $request['name'],
                        'email' => $request['email'],
                        'phone_no' => $request['phone'],
                        'password' => Hash::make($request['password']),
                        'referral_code' => $ref_code,
                        'referred_by' => $referid,
                    ]);
                    if (Cookie::has('referral_code')) {
                        $referral_code = Cookie::get('referral_code');
                        $referred_by_user = User::where('referral_code', $referral_code)->first();
                        if (!empty($referred_by_user)) {
                            $refer = Refer::first();
                            $walletSave = Wallet::insert([
                                'user_id' => $user->id,
                                'amount' => $refer->getrefer_amount,
                                'payment_method' => 'Refer',
                                'payment_details' => 'Referred By ' . $referred_by_user->name
                            ]);
                            $array['referby']='';
                            $array['getrefer_amount'] =$refer->getrefer_amount;
                            $array['referbyname'] =$referred_by_user->name;
                            $data_base[0]['body'] = env("REFERRED_USER"); // Tests

                            $vars = array(
                                '{$getrefer_amount}'       => $refer->getrefer_amount,
                                '{$referred_by_username}'       => $referred_by_user->name
                            );
                            $this->sms_function( $data_base[0]['body'],$vars,$referred_by_user->phone_no);
                            Mail::to($request['email'])->queue(new ReferMail($array));



                          
                            $array['referby'] =1;
                            $array['referby_amount'] =$refer->referby_amount;
                         
                            $walletSave = Wallet::insert([
                                'user_id' => $referred_by_user->id,
                                'amount' => $refer->referby_amount,
                                'payment_method' => 'Referral Bonus',
                                'payment_details' => 'Referred To ' . $request['name']
                            ]);
                            Mail::to($referred_by_user->email)->queue(new ReferMail($array));
                            $data_base[0]['body'] = env("REFER_BY_USER"); // Tests

                            $vars = array(
                                '{$referby_amount}'       => $refer->referby_amount,
                                '{$name}'       => $request['name']
                            );
                            $this->sms_function( $data_base[0]['body'],$vars,$request['phone']);
                            $walletSumOfReferByUser = Wallet::where('user_id', $referred_by_user->id)->sum('amount');
                            $walletSumOfNewUser = Wallet::where('user_id', $user->id)->sum('amount');
                            $updateReferByuser = User::where('id', $referred_by_user->id)->update(['balance' => $walletSumOfReferByUser]);
                            $updateNewuser = User::where('id', $user->id)->update(['balance' => $walletSumOfNewUser]);

                           
                        }
                    }
                    Cookie::queue(Cookie::forget('referral_code'));
                    $customer = new Customer;
                    $customer->user_id = $user->id;
                    $customer->save();

                   
                   
                  
                   
                    if (BusinessSetting::where('type', 'email_verification')->first()->value != 1) {
                        $user->email_verified_at = date('Y-m-d H:m:s');
                        $user->save();
                        $status = 1;
                        $msg = "You have been registered successfully.";
                        $array['id']="";
                        Mail::to($request['email'])->queue(new CustomerVerificationMail($array));
                        //  flash(__('Registration successfull.'))->success();
                    } else {
                        //  $user->sendEmailVerificationNotification();
                       
                        $array['id'] =  encrypt($user->id);
                        Mail::to($request['email'])->queue(new CustomerVerificationMail($array));
                        $status = 2;
                        $msg = "Registration successfull.";
                      
                        //flash(__('Registration successfull. Please verify your email.'))->success();
                    }

                    $this->guard()->login($user);

                    // return $this->registered($request, $user)
                    //     ?: redirect($this->redirectPath());
                    $response = array(
                        'status' => $status,
                        'message' => $msg,
                    );
                    session()->forget(['OTP', 'TIMEOUT']);
                } else {

                    $response = array(
                        'status' => 0,
                        'message' => "Invalid OTP, Try Again.",
                    );
                }
            } else {
                $response = array(
                    'status' => 0,
                    'message' => "OTP Expired! Please Generate A New One!",
                );
            }
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }
    protected function registered(Request $request, $user)
    {
        if ($user->email == null) {
            return redirect()->route('verification');
        } else {
            return redirect()->route('home');
        }
    }

    public function emailverified($id)
    {
        $decryptId = decrypt($id);
        if ($decryptId) {
            $user = User::where('id', $decryptId)->update([
                'email_verified_at' => date('Y-m-d H:m:s')

            ]);
            if ($user) {
                flash('Email Verified successfully')->success();
                return redirect(route('user.login'));
            }
        } else {
            abort(404);
        }
    }

    public function sms_function($env_var, $vars,$phone){
        $data_base[0]['body'] = $env_var; // Tests

               

                $msg = strtr($data_base[0]['body'], $vars);
            
                //    env("CUSTOMER_REGISTRATION_OTP");
                //   return  $msg=str_replace($otp,'$otp',$msg);
                //$msg = "Your One Time Password $otp for registering in Local2Vocal is .";

                // Account details
                $apiKey = urlencode("PBnYEfajueU-ZLgRhC24TA3poA2CQ1TSzFMndmR29d");
                // Message details

                $sender = urlencode("LOVOCL");
                $message = rawurlencode($msg);


                // Prepare data for POST request
                $data = array("apikey" => $apiKey, "numbers" => $phone, "sender" => $sender, "message" => $message);
                // Send the POST request with cURL
                $url = "https://api.textlocal.in/send/";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $buffer = curl_exec($ch);
                curl_close($ch);
                
    }
}
