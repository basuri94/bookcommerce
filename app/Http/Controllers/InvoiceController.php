<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Shop;
use App\User;
use App\Seller;
use App\BusinessSetting;
use App\CuriorRefund;
use App\OrderDetail;
use App\RefundRequest;
use Illuminate\Support\Facades\DB;
use App\Wallet;
use App\SellerInvoice;
use PDF;
use Auth;

class InvoiceController extends Controller
{
    //downloads customer invoice
    public function customer_invoice_download($id)
    {
        $order = Order::findOrFail($id);
        $pdf = PDF::setOptions([
                        'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true,
                        'logOutputFile' => storage_path('logs/log.htm'),
                        'tempDir' => storage_path('logs/')
                    ])->loadView('invoices.customer_invoice', compact('order'));
                //    return view('invoices.customer_invoice', compact('order'));
                $fileName =str_replace("/","_",$order->code);
        return $pdf->download('order-'.$fileName.'.pdf');
    }


    public static function oder_wise_shop($seller_id){
        $Shop=Shop::where('user_id',$seller_id)->first();
        return $Shop;
    }

    public static function oder_wise_user($seller_id){
        $Shop=User::where('id',$seller_id)->first();
        return $Shop;
    }
    public static function oder_wise_seller($seller_id){
        $Shop=Seller::where('user_id',$seller_id)->first();
        return $Shop;
    }
    
    
    //downloads seller invoice
    public function seller_invoice_download($id)
    {
        $order = Order::findOrFail($id);
        // $pdf = PDF::setOptions([
        //                 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true,
        //                 'logOutputFile' => storage_path('logs/log.htm'),
        //                 'tempDir' => storage_path('logs/')
        //             ])->loadView('invoices.seller_invoice', compact('order'));
        return view('invoices.seller_invoice', compact('order'));
        // return $pdf->download('order-'.$order->code.'.pdf');
    }

    //downloads admin invoice
    public function admin_invoice_download($id)
    {
        $order = Order::findOrFail($id);
        $pdf = PDF::setOptions([
                        'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true,
                        'logOutputFile' => storage_path('logs/log.htm'),
                        'tempDir' => storage_path('logs/')
                    ])->loadView('invoices.admin_invoice', compact('order'));
        return $pdf->download('order-'.$order->code.'.pdf');
    }
    // public function seller_invoice(Request $request){
    //     return view('report.seller_invoice_report');
    // }
    public function seller_report_invoice(Request $request){
    
        $from_date=$request->from_date;
        $refund_time=BusinessSetting::where('type','refund_request_time')->value('value');
        $sellers = Seller::with('user')->join('order_details','order_details.seller_id','=','sellers.user_id')->where('order_details.payment_status', 'paid')
        // ->where(DB::raw('DATEDIFF( now(), STR_TO_DATE(order_details.created_at,"%d-%m-%Y") )'), '=', $refund_time);
        ->whereRaw('DATE(order_details.created_at) <= DATE_SUB(CURDATE(), INTERVAL '.$refund_time. ' DAY)')
        ->groupBy('order_details.seller_id');
      
        if(!empty($from_date)){
         $date_ex=explode('-',$from_date);
         $year=$date_ex[1];
         $month=$date_ex[0];
       
         $sellers=$sellers->whereYear('order_details.created_at', '=', $year)
         ->whereMonth('order_details.created_at', '=', $month);
 
         
        }
        
        
        $sellers= $sellers->get();
        $data = array();
       if(!empty($from_date)){
        foreach ($sellers as $key => $val) {
         
            $nested['vendor_name']= $val->user->name;
            $nested['id']= $val->user->id;
            $data[]=$nested;
           
}
}
        return view('reports.seller_invoice_report')->with('data',$data);
        // return view('invoices.seller_report_invoice', compact('order'));
    }
    public function seller_report_invoice_download(Request $request){
        $seller_id=$request->seller_id;
        $from_date=$request->from_date;
        $type=$request->type;
            $final_admin_to_pay="";
            $totalCommissionPercentage = 0;
            $totalGateWayCharge = 0;
            $totalCourierRefundOrReplaceAmount = 0;
            $totalCourierCharge = 0;
            $walletCharge = 0;
            $prcessing_fee=0;
            $tcs=0;
            $tds=0;
            $totalfinalWalletAmount=0;
            $finalWalletAmount=0;
            $refund_time=BusinessSetting::where('type','refund_request_time')->value('value');
            $price = OrderDetail::where('payment_status', 'paid')
                ->where('seller_id', $seller_id)->sum('price');
            $tax = OrderDetail::where('payment_status', 'paid')
                ->where('seller_id', $seller_id)->sum('tax');
            $shipping_cost = OrderDetail::where('payment_status', 'paid')
                ->where('seller_id', $seller_id)->sum('shipping_cost');

            $total_price_includegst = $price + $tax;
           
         
           

            $courierCharge = DB::table('curior_orders')->join('order_details', 'order_details.curior_order_id', '=', 'curior_orders.id')
                ->where('curior_orders.seller_id', $seller_id)->sum('curior_charge');

           
            $OrderDetail = OrderDetail::where('seller_id', $seller_id)->where('payment_status', 'paid')
           // ->where(DB::raw('DATEDIFF( now(), STR_TO_DATE(order_details.created_at,"%d-%m-%Y") )'), '=', $refund_time);
           ->whereRaw('DATE(order_details.created_at) <= DATE_SUB(CURDATE(), INTERVAL '.$refund_time. ' DAY)');
           if(!empty($from_date)){
            $date_ex=explode('-',$from_date);
            $year=$date_ex[1];
            $month=$date_ex[0];
          
            $OrderDetail=$OrderDetail->whereYear('order_details.created_at', '=', $year)
            ->whereMonth('order_details.created_at', '=', $month);
    
            
           }
            $OrderDetail=$OrderDetail->get();
            
            $courierRefundAmount_curior=0;
            foreach ($OrderDetail as $key => $OrderDetailVal) {
                $finalWalletAmount=0;
                $calculateCommission=0;
                $totalFee=0;
                $gatewayCharge=0;
                $walletAmount=0;
                $courierRefundAmount=0;
                
                if(!empty($OrderDetailVal->product)){
                    $OrderCharge = Order::where('id', $OrderDetailVal->order_id)->first();
                    if(!empty($OrderCharge->payment_details)){
                        $gatewayCharge = ((json_decode($OrderCharge->payment_details)->fee) / 100) / $OrderCharge->grand_total *    ($OrderDetailVal->tax + $OrderDetailVal->price + $OrderDetailVal->shipping_cost);
                    }
                   
    
                    $calculateCommission = (($OrderDetailVal->product->subcategory->commision_rate) * $OrderDetailVal->price) / 100;
                    $refunddata = RefundRequest::where('status', 'Approved')->where('order_id', $OrderDetailVal->id)->first();
                    if (!empty($refunddata)) {
                        if ($refunddata->type == "Refund") {
                            $refundAmount = $refunddata->amount;
                            $courierRefundAmount = CuriorRefund::where('refund_request_id', $refunddata->id)->where('status', 1)->value('curior_charge');
                            $courierRefundAmount_curior= $courierRefundAmount_curior + $courierRefundAmount ; 
                            $totalCourierRefundOrReplaceAmount = $totalCourierRefundOrReplaceAmount + $refundAmount + $courierRefundAmount;
                        } else if ($refunddata->type == "Replacement") {
                            $totalReplaceAmount = CuriorRefund::where('refund_request_id', $refunddata->id)->where('status', 1)->sum('curior_charge');
                            $courierRefundAmount_curior= $courierRefundAmount_curior + $totalReplaceAmount ; 
                            $totalCourierRefundOrReplaceAmount = $totalCourierRefundOrReplaceAmount + $totalReplaceAmount;
                        }
                    }
                    $courierCharge = DB::table('curior_orders')->where('curior_orders.id', $OrderDetailVal->curior_order_id)->first();
                    if (!empty($courierCharge->curior_charge)) {
    
                        $courierChargeValue = ($courierCharge->curior_charge) / $OrderCharge->grand_total *    ($OrderDetailVal->tax + $OrderDetailVal->price + $OrderDetailVal->shipping_cost);
                        $totalCourierCharge = $totalCourierCharge + $courierChargeValue;
                    }
                   
                  if($OrderCharge->wallet_amount!="0.00"){
                 
                    $walletData=Wallet::where('payment_method','razorpay')->where('user_id',$OrderCharge->user_id)->get();
                    foreach( $walletData as $walletVal){
                   
                        if (!empty(json_decode($walletVal->payment_details)->fee)) {
                            $totalFee +=(json_decode($walletVal->payment_details)->fee);
                          }
                         $walletAmount +=$walletVal->amount;
                       
                        
                    }
                    if($totalFee!=0){
                        $convertTotalFee= $totalFee/100;
                        $WalletAmountCalculate=($convertTotalFee / $walletAmount) *$OrderCharge->wallet_amount;
                        $finalWalletAmount =  $WalletAmountCalculate / $OrderCharge->grand_total *     ($OrderDetailVal->tax + $OrderDetailVal->price + $OrderDetailVal->shipping_cost);;
                      
                    }
                   
                }
              
                    $totalGateWayCharge = $totalGateWayCharge + $gatewayCharge;
                    $totalCommissionPercentage = $calculateCommission + $totalCommissionPercentage;
                    $totalfinalWalletAmount +=$finalWalletAmount;
                }
               
            }
       
            $actual_gatewaycharge = number_format((float)$totalGateWayCharge, 2, '.', '');

            $gst_in_commisssion = (($totalCommissionPercentage * 18) / 100) + $totalCommissionPercentage;
            $commision_gst=($totalCommissionPercentage * 18) / 100;
            
            $tcs= ($price)*0.01;
            $tds= ($price)*0.01;
            $vendor_charges = $totalCourierCharge + $actual_gatewaycharge + $gst_in_commisssion;
            $gst_in_commisssion = number_format((float)$gst_in_commisssion, 2, '.', '');  
            $curior=(($totalCourierCharge+$courierRefundAmount_curior)*100)/118;
            $curior_gst=(($curior)*18)/100;
            $curior_charge = number_format((float)$curior, 2, '.', '');           
            $prcessing_fee=(($actual_gatewaycharge+$finalWalletAmount)*100)/118;
            $prcessing_fee_gst=(($prcessing_fee)*18)/100;
            $payment_handling = number_format((float)$prcessing_fee, 2, '.', '');
           $admin_to_pay = $total_price_includegst - $vendor_charges - $totalCourierRefundOrReplaceAmount-$totalfinalWalletAmount-$tcs-$tds;
           $final_admin_to_pay = number_format((float)$admin_to_pay, 2, '.', '');
           $gst_total=$commision_gst+$curior_gst+$prcessing_fee_gst;
           $gst_total_amount = number_format((float)$gst_total, 2, '.', '');

           $year1 = (( date('m') < 4 ) ? date('Y') - 1 : date('Y'))."-04-01"; 
        $year2 = (( date('m') > 3) ? date('Y') + 1 : date('Y'))."-03-31";
        $year3 = substr((( date('m') < 4 ) ? date('Y') - 1 : date('Y')), -2);
        $year4 = substr((( date('m') > 3 ) ? date('Y') + 1 : date('Y')), -2);
        $order_id_count=SellerInvoice::whereRaw('DATE_FORMAT(created_at, "%Y-%m-%d") BETWEEN ? AND ?', [ $year1, $year2])->count();
        $order_code=sprintf("%02d",$order_id_count + 1) ;
        $inv_no="L2V/".$year3."-".$year4."/".$order_code;
       $data=array(
           'commision'=>$totalCommissionPercentage,
           'payment_handling'=>$payment_handling,
           'curior_charge'=>$curior_charge,
           'seller_id'=>$seller_id,
           'gst'=>$gst_total_amount,
           'inv_no'=>$inv_no,
           'inv_date'=>date('d/m/Y'),
           'total_amount'=>$totalCommissionPercentage+$payment_handling+$curior_charge+$gst_total_amount,
       );
       if($type=="download") {
        $pdf = PDF::setOptions([
            'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true,
            'logOutputFile' => storage_path('logs/log.htm'),
            'tempDir' => storage_path('logs/')
        ])->loadView('invoices.seller_report_invoice', compact('data'));
    return $pdf->download('.seller_report_-'.$seller_id.'.pdf');
       }else{
        
        $order=new SellerInvoice();
        $order->seller_id = $seller_id;
        $order->commision = $totalCommissionPercentage;
        $order->courier_charge = $curior_charge;
        $order->payment_handling = $payment_handling;
        $order->gst = $gst_total_amount;
        $order->total = $totalCommissionPercentage+$payment_handling+$curior_charge+$gst_total_amount;
        $order->for_month = $from_date;
        $order->invoice_no =$inv_no;
        $pdf = PDF::setOptions([
            'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true,
            'logOutputFile' => storage_path('logs/log.htm'),
            'tempDir' => storage_path('logs/')
        ])->loadView('invoices.seller_report_invoice', compact('data'));
       $output = $pdf->output();
        //file_put_contents('public/invoices/'.'Order#'.$order->code.'.pdf', $output);
        $path = public_path('invoices');
        $fileName = 'Order#' . $inv_no . '.pdf';
        $fileName =str_replace("/","_",$fileName);
    $pdf->save($path . '/' . $fileName);
    $order->invoice_path = $path . '/' . $fileName;
    $order->save();
        flash('Invoice Sent Succcessfully')->success();
        return back();
       }
     

        // return view('invoices.seller_report_invoice',compact('data'));
    }

    public function seller_invoice_list(Request $request){
        $seller_id=\Auth::user()->id;
        $seller_data=SellerInvoice::where('seller_id',$seller_id)->get();
        return view('frontend.seller.sales_vendor_invoice',compact('seller_data'));

    }
    public function seller_report_invoice_download_from_seller(Request $request){
        $inv_id=$request->inv_id;
        $seller_data=SellerInvoice::find($inv_id);
        $timestamp = strtotime($seller_data->created_at);
 
// Creating new date format from that timestamp
$new_date = date("d/m/Y", $timestamp);
        $data=array(
            'commision'=>$seller_data->commision,
            'payment_handling'=>$seller_data->payment_handling,
            'curior_charge'=>$seller_data->courier_charge,
            'seller_id'=>$seller_data->seller_id,
            'gst'=>$seller_data->gst,
            'inv_no'=>$seller_data->invoice_no,
            'inv_date'=>$new_date,
            'total_amount'=>$seller_data->total,
        );
      
         $pdf = PDF::setOptions([
             'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true,
             'logOutputFile' => storage_path('logs/log.htm'),
             'tempDir' => storage_path('logs/')
         ])->loadView('invoices.seller_report_invoice', compact('data'));
     return $pdf->download('.seller_invoice_-'.$seller_data->for_month.'.pdf');
    }
}
