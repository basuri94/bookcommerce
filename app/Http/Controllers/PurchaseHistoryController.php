<?php

namespace App\Http\Controllers;

use App\Mail\OrderCancelMail;
use App\Models\OrderDetail as ModelsOrderDetail;
use App\Models\Product;
use Illuminate\Http\Request;
use Razorpay\Api\Api;
use App\Order;
use App\OrderDetail;
use DB;
use App\Http\Controllers\Auth\RegisterController;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

class PurchaseHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::where('user_id', Auth::user()->id)->orderBy('code', 'desc')->paginate(9);
        return view('frontend.purchase_history', compact('orders'));
    }

    public function digital_index()
    {
        $orders = DB::table('orders')
                        ->orderBy('code', 'desc')
                        ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                        ->join('products', 'order_details.product_id', '=', 'products.id')
                        ->where('orders.user_id', Auth::user()->id)
                        ->where('products.digital', '1')
                        ->where('order_details.payment_status', 'paid')
                        ->select('order_details.id')
                        ->paginate(10);
        return view('frontend.digital_purchase_history', compact('orders'));
    }

    public function purchase_history_details(Request $request)
    {
        $order = Order::findOrFail($request->order_id);
        $order->delivery_viewed = 1;
        $order->payment_status_viewed = 1;
        $order->save();
        return view('frontend.partials.order_details_customer', compact('order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
public function showlicense(Request $request){
    $statusCode = 200;
    if (!$request->ajax()) {
        $statusCode = 400;
        $response = array('error' => 'Error occered in Json call.');
        return response()->json($response, $statusCode);
    }

    $this->validate($request, [


        'orderdetailsid' => 'required',
    ], [
        'orderdetailsid.required' => 'Code is required.',

    ]);
    try {
      
        $orderdetailsid = $request->orderdetailsid;
        OrderDetail::where('id',$orderdetailsid)->update([
            'is_viewed'=>1
        ]);
       
       $licensevalue= OrderDetail::where('id',$orderdetailsid)->value('license_key');
                $decryptlicensevalue=decrypt($licensevalue);
        
                $html = "";
             
                $html .= '<div class="input-group input-group--style-1" style="padding:18px;">';
                $html .= ' <span class="log_addon input-group-addon"><i class="text-md fa fa-link"></i>';
                $html .= '</span><input type="password" readonly value="' . $decryptlicensevalue . '" id="licenseInput" class="log_input form-control">';
               // $html .= '&nbsp;<input type="checkbox" onclick="viewlicense()">';
                $html .= '<button class="btn btn-primary" type="button"  id="btnView" onclick="viewlicense();">View</button>';
                $html .= '</div>';
               

        $response = array('status' => 1, 'html' => $html);
    } catch (\Exception $e) {
        $response = array(
            'exception' => true,
            'exception_message' => $e->getMessage(),
        );
        $statusCode = 400;
    } finally {
        return response()->json($response, $statusCode);
    }
}

public function digitalproduct_conv(Request $request){
    $statusCode = 200;

    $this->validate($request, [


        'productid' => 'required',
    ], [
        'productid.required' => 'Code is required.',

    ]);
    try {
      
        $productid = $request->productid;
        $product=Product::where('id',$productid)->first();
       if(!empty($product)){
        $response = array('status' => 1, 'product' => $product);
       }
       else{
        $response = array('status' => 2);
       }
     
               

       
    } catch (\Exception $e) {
        $response = array(
            'exception' => true,
            'exception_message' => $e->getMessage(),
        );
        $statusCode = 400;
    } finally {
        return response()->json($response, $statusCode);
    }
}
    public function ordercancelled(Request $request){
        try {
            $statusCode = 200;
            $orderid = $request->orderid;
            $orderDet=Order::where('id',$orderid)->first();
            if($orderDet->payment_type=='online'){
            $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
            $order_res=json_decode($orderDet->payment_details);
            $order_res_payment_id=$order_res->id;
            //echo $order_res_payment_id;die;
            $payment = $api->payment->fetch($order_res_payment_id);
            $refund = $payment->refund();
            }
            Order::where('id',$orderid)->update([
                'payment_status'=>'cancelled'
            ]);
            ModelsOrderDetail::where('order_id',$orderid)->update([
                'delivery_status'=>'cancelled'
            ]);
$user=Auth::user();
            $array['name']=Auth::user()->name;
            $array['code']=$orderDet->code;
          
            Mail::to(Auth::user()->email)->queue(new OrderCancelMail($array));
          
        $registerController= new RegisterController();
        $data_base[0]['body'] = env("ORDER_CANCELLED"); // Tests
        $product_data=$orderDet->orderDetails;
        $name_of_product="";
        foreach($product_data as $item){
            if($name_of_product==""){
                $name_of_product=substr($item->product->name, 0, 5)."..";
            }else{
                $name_of_product= $name_of_product.",".substr($item->product->name, 0, 5)."..";
            }
          
        }
        $vars = array(
            '{$name}'       => Auth::user()->name,
            '{$ordercode}'       => $orderDet->code,
            '{$itemsname}'       => $name_of_product
           
        );
        $registerController->sms_function( $data_base[0]['body'],$vars, $user->phone_no);
    
            $response = array('status' => 1, 'orderid' => $orderid);
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }
}
