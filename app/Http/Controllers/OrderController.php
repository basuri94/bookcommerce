<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\OTPVerificationController;
use App\Http\Controllers\ClubPointController;
use App\Http\Controllers\AffiliateController;
use App\Order;
use App\Product;
use App\Color;
use App\OrderDetail;
use App\CouponUsage;
use App\OtpConfiguration;
use App\User;
use App\BusinessSetting;
use App\LicenseKey;
use App\Shop;
use App\CuriorOrder;
use App\Http\Controllers\Auth\RegisterController;
use Session;
use DB;
use PDF;
use App\Mail\InvoiceEmailManager;
use App\Mail\OrderMail;
use App\Mail\WalletMail;
use Config;
use CoreComponentRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource to seller.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $payment_status = null;
        $delivery_status = null;
        $sort_search = null;
        $orders = DB::table('orders')
            ->orderBy('code', 'desc')
            ->join('order_details', 'orders.id', '=', 'order_details.order_id')
            ->where('order_details.seller_id', Auth::user()->id)
            ->select('orders.id')
            ->distinct();

            if ($request->product_type != null) {
                $orders = $orders->leftjoin('products','products.id','=','order_details.product_id')
                ->leftjoin('categories','categories.id','=','products.category_id')
                ->where('categories.digital', $request->product_type);
                $product_type = $request->product_type;
            }
        if ($request->payment_status != null) {
            $orders = $orders->where('order_details.payment_status', $request->payment_status);
            $payment_status = $request->payment_status;
        }
        if ($request->delivery_status != null) {
            $orders = $orders->where('order_details.delivery_status', $request->delivery_status);
            $delivery_status = $request->delivery_status;
        }
        if ($request->has('search')) {
            $sort_search = $request->search;
            $orders = $orders->where('code', 'like', '%' . $sort_search . '%');
        }

        $orders = $orders->paginate(15);

        foreach ($orders as $key => $value) {
            $order = \App\Order::find($value->id);
            $order->viewed = 1;
            $order->save();
        }

        return view('frontend.seller.orders', compact('orders', 'payment_status', 'delivery_status', 'sort_search'));
    }

    /**
     * Display a listing of the resource to admin.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_orders(Request $request)
    {


        $payment_status = null;
        $delivery_status = null;
        $sort_search = null;
        $admin_user_id = User::where('user_type', 'admin')->first()->id;
        $orders = DB::table('orders')
            ->orderBy('code', 'desc')
            ->join('order_details', 'orders.id', '=', 'order_details.order_id')
            ->where('order_details.seller_id', $admin_user_id)
            ->select('orders.id')
            ->distinct();

        if ($request->payment_type != null) {
            $orders = $orders->where('order_details.payment_status', $request->payment_type);
            $payment_status = $request->payment_type;
        }
        if ($request->delivery_status != null) {
            $orders = $orders->where('order_details.delivery_status', $request->delivery_status);
            $delivery_status = $request->delivery_status;
        }
        if ($request->has('search')) {
            $sort_search = $request->search;
            $orders = $orders->where('code', 'like', '%' . $sort_search . '%');
        }
        $orders = $orders->paginate(15);
        return view('orders.index', compact('orders', 'payment_status', 'delivery_status', 'sort_search', 'admin_user_id'));
    }

    /**
     * Display a listing of the sales to admin.
     *
     * @return \Illuminate\Http\Response
     */
    public function sales(Request $request)
    {


        $sort_search = null;
        $orders = Order::orderBy('code', 'desc');
        if ($request->has('search')) {
            $sort_search = $request->search;
            $orders = $orders->where('code', 'like', '%' . $sort_search . '%');
        }
        $orders = $orders->paginate(15);
        return view('sales.index', compact('orders', 'sort_search'));
    }


    public function order_index(Request $request)
    {
        if (Auth::user()->user_type == 'staff' && Auth::user()->staff->pick_up_point != null) {
            //$orders = Order::where('pickup_point_id', Auth::user()->staff->pick_up_point->id)->get();
            $orders = DB::table('orders')
                ->orderBy('code', 'desc')
                ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                ->where('order_details.pickup_point_id', Auth::user()->staff->pick_up_point->id)
                ->select('orders.id')
                ->distinct()
                ->paginate(15);

            return view('pickup_point.orders.index', compact('orders'));
        } else {
            //$orders = Order::where('shipping_type', 'Pick-up Point')->get();
            $orders = DB::table('orders')
                ->orderBy('code', 'desc')
                ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                ->where('order_details.shipping_type', 'pickup_point')
                ->select('orders.id')
                ->distinct()
                ->paginate(15);

            return view('pickup_point.orders.index', compact('orders'));
        }
    }

    public function pickup_point_order_sales_show($id)
    {
        if (Auth::user()->user_type == 'staff') {
            $order = Order::findOrFail(decrypt($id));
            return view('pickup_point.orders.show', compact('order'));
        } else {
            $order = Order::findOrFail(decrypt($id));
            return view('pickup_point.orders.show', compact('order'));
        }
    }

    /**
     * Display a single sale to admin.
     *
     * @return \Illuminate\Http\Response
     */
    public function sales_show($id)
    {
        $order = Order::findOrFail(decrypt($id));
        return view('sales.show', compact('order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 

//         $array['view'] = 'emails.invoice';
//         $array['subject'] = 'Order Placed - 20190207-120759';
//         $array['from'] = env('MAIL_USERNAME');
//         $array['content'] = 'Hi. A new order has been placed. Please check the attached invoice.';
//         $array['file'] = public_path('invoices/Order#20190207-120759.pdf');
//         $array['file_name'] = 'Order#20190207-120759.pdf';
//         $array['name'] = Auth::user()->name;
//         $array['order_code'] = '20190207-120759';
      
//         try { 

//           Mail::to(\App\User::find(46)->email)->queue(new InvoiceEmailManager($array));
//         } catch (\Exception $e) {
//           echo $e->getMessage();die;
//         }
// echo 3;die;

        $order = new Order;
        if (Auth::check()) {
            $order->user_id = Auth::user()->id;
        } else {
            $order->guest_id = mt_rand(100000, 999999);
        }

        $order->shipping_address = json_encode($request->session()->get('shipping_info'));
        $paymentType="";
if($request->payment_option=="razorpay"){
    $paymentType="online";
}else{
    $paymentType="cash_on_delivery";
}
        $year1 = (( date('m') < 4 ) ? date('Y') - 1 : date('Y'))."-04-01"; 
        $year2 = (( date('m') > 3) ? date('Y') + 1 : date('Y'))."-03-31";
        $year3 = substr((( date('m') < 4 ) ? date('Y') - 1 : date('Y')), -2);
        $year4 = substr((( date('m') > 3 ) ? date('Y') + 1 : date('Y')), -2);
        $order_id_count=Order::whereRaw('DATE_FORMAT(created_at, "%Y-%m-%d") BETWEEN ? AND ?', [ $year1, $year2])->count();
        $order_code=sprintf("%02d",$order_id_count + 1) ;
        
        $order->payment_type = $paymentType;
        $order->delivery_viewed = '0';
        $order->payment_status_viewed = '0';
        $order->code ="TL2V/".$year3."-".$year4."/".$order_code;
        $order->date = strtotime('now');

        if ($order->save()) {
            $subtotal = 0;
            $tax = 0;
            $shipping = 0;

            //calculate shipping is to get shipping costs of different types
            $admin_products = array();
            $seller_products = array();

            //Order Details Storing
            foreach (Session::get('cart') as $key => $cartItem) {
                $product = Product::find($cartItem['id']);

                if ($product->added_by == 'admin') {
                    array_push($admin_products, $cartItem['id']);
                } else {
                    $product_ids = array();
                    if (array_key_exists($product->user_id, $seller_products)) {
                        $product_ids = $seller_products[$product->user_id];
                    }
                    array_push($product_ids, $cartItem['id']);
                    $seller_products[$product->user_id] = $product_ids;
                }

                $subtotal += $cartItem['price'] * $cartItem['quantity'];
                $tax += $cartItem['tax'] * $cartItem['quantity'];

                $product_variation = $cartItem['variant'];

                if ($product_variation != null) {
                    $product_stock = $product->stocks->where('variant', $product_variation)->first();
                    $product_stock->qty -= $cartItem['quantity'];
                    $product_stock->save();
                } else {
                    $product->current_stock -= $cartItem['quantity'];
                    $product->save();
                }

                $order_detail = new OrderDetail;
                $order_detail->order_id  = $order->id;
                $order_detail->seller_id = $product->user_id;
                $order_detail->product_id = $product->id;
                $order_detail->variation = $product_variation;
                $order_detail->price = $cartItem['price'] * $cartItem['quantity'];
                $order_detail->tax = $cartItem['tax'] * $cartItem['quantity'];
                $order_detail->shipping_type = $cartItem['shipping_type'];
                $order_detail->product_referral_code = $cartItem['product_referral_code'];

                //Dividing Shipping Costs
                if ($cartItem['shipping_type'] == 'home_delivery') {
                    $order_detail->shipping_cost = getShippingCost($key);
                    $shipping = $order_detail->shipping_cost;
                } else {
                    $order_detail->shipping_cost = 0;
                    $order_detail->pickup_point_id = $cartItem['pickup_point'];
                }
                //End of storing shipping cost

                $order_detail->quantity = $cartItem['quantity'];
                $order_detail->save();

                $product->num_of_sale++;
                $product->save();
            }

            $order->grand_total = $subtotal + $tax + $shipping;

            if (Session::has('coupon_discount')) {
                $order->grand_total -= Session::get('coupon_discount');
                $order->coupon_discount = Session::get('coupon_discount');

                $coupon_usage = new CouponUsage;
                $coupon_usage->user_id = Auth::user()->id;
                $coupon_usage->coupon_id = Session::get('coupon_id');
                $coupon_usage->save();
            }

            if (Session::has('wallet_amount_for_order')) {
                $order->grand_total -= Session::get('wallet_amount_for_order');
                $order->Wallet_amount = Session::get('wallet_amount_for_order');
                $user = Auth::user();
                $user->balance -= Session::get('wallet_amount_for_order');
                $user->save();
                
                $array['from'] = env('MAIL_USERNAME');
                $array['name'] = $user->name;
                $array['amount_credit'] = '';
                $array['amount_debit'] = Session::get('wallet_amount_for_order');
                
                Mail::to($user->email)->queue(new WalletMail($array));
                $registerController= new RegisterController();
                $data_base[0]['body'] = env("WALLET_DEBIT"); // Tests
        
                $vars = array(
                    '{$name}'       => Auth::user()->name,
                    '{$amountdebit}'       => Session::get('wallet_amount_for_order'),
                   
                );
                $registerController->sms_function( $data_base[0]['body'],$vars, $user->phone_no);
              

               
                
                
            }

            $order->save();

            // stores the pdf for invoice
        //     $pdf = PDF::setOptions([
        //         'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true,
        //         'logOutputFile' => storage_path('logs/log.htm'),
        //         'tempDir' => storage_path('logs/')
        //     ])->loadView('invoices.customer_invoice', compact('order'));
        //     $output = $pdf->output();
        //     //file_put_contents('public/invoices/'.'Order#'.$order->code.'.pdf', $output);
        //     $path = public_path('invoices');
        //     $fileName = 'Order#' . $order->code . '.pdf';
        //     $pdf->save($path . '/' . $fileName);

        //     $array['view'] = 'emails.invoice';
        //     $array['subject'] = 'Order Placed - ' . $order->code;
        //     $array['from'] = env('MAIL_USERNAME');
        //     $array['content'] = 'Hi.  A new order has been placed. Please check the attached invoice.';
        //     $array['file'] = public_path('invoices/Order#' . $order->code . '.pdf');
        //     $array['file_name'] = 'Order#' . $order->code . '.pdf';
          
        //   $array['order_code'] = $order->code;
        //       foreach ($seller_products as $key => $seller_product) {
        //         $array['name'] = \App\User::find($key)->name;
        //         try {
        //             Mail::to(\App\User::find($key)->email)->queue(new InvoiceEmailManager($array));
        //         } catch (\Exception $e) {
        //         }
        //     }

        //     if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated && \App\OtpConfiguration::where('type', 'otp_for_order')->first()->value) {
        //         try {
        //             $otpController = new OTPVerificationController;
        //             $otpController->send_order_code($order);
        //         } catch (\Exception $e) {
        //         }
        //     }

        //     //sends email to customer with the invoice pdf attached
        //     if (env('MAIL_USERNAME') != null) {
        //         try {
        //             $array['name'] = Auth::user()->name;
        //             Mail::to($request->session()->get('shipping_info')['email'])->queue(new InvoiceEmailManager($array));
        //             $array['name'] = User::where('user_type', 'admin')->first()->name;
                  
        //             Mail::to(User::where('user_type', 'admin')->first()->email)->queue(new InvoiceEmailManager($array));
        //         } catch (\Exception $e) {
        //         }
        //     }

        //     if (file_exists($array['file'])) {
        //         unlink($array['file']);
        //     }
        //     $registerController= new RegisterController();
        //     $data_base[0]['body'] = env("ORDER_PLACED"); // Tests
                        
        //                         $vars = array(
        //                             '{$ordercode}'       => $order->code,
                                   
                                   
        //                         );
        //                         $registerController->sms_function( $data_base[0]['body'],$vars, Auth::user()->phone_no);

            $request->session()->put('order_id', $order->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::findOrFail(decrypt($id));
        $order->viewed = 1;
        $order->save();
        return view('orders.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::findOrFail($id);
        if ($order != null) {
            foreach ($order->orderDetails as $key => $orderDetail) {
                $orderDetail->delete();
            }
            $order->delete();
            flash('Order has been deleted successfully')->success();
        } else {
            flash('Something went wrong')->error();
        }
        return back();
    }

    public function order_details(Request $request)
    {
        $order = Order::findOrFail($request->order_id);
        //$order->viewed = 1;
        $order->save();
        $tracking_dt=[];
        $tracking=$order->curiorOrderDetails->where('seller_id', Auth::user()->id)->first();
        if($tracking!=null){
            if ($tracking->awbNo!=null) {
                $tracking_dt=$this->track($tracking->awbNo);
            }
        }
        //$service = $this->getserviceability($request->order_id);
        return view('frontend.partials.order_details_seller', compact('order', 'tracking_dt'));
    }

    public function update_delivery_status(Request $request)
    { 
        $order = Order::findOrFail($request->order_id);

        $array['from'] = env('MAIL_USERNAME');
        $array['name'] =$order->user->name;
        $array['code'] =$order->code;
        $array['delivery_status'] =$request->status;
        Mail::to($order->user->email)->queue(new OrderMail($array));
     
        $order->delivery_viewed = '0';
        $order->save();
        if (Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'seller') {


            foreach ($order->orderDetails->where('seller_id', Auth::user()->id) as $key => $orderDetail) {
                $orderDetail->delivery_status = $request->status;
                
                if($request->status=="delivered"){
                    $orderDetail->payment_status = "paid";
                   
                }
                $orderDetail->save();
            }
            
        } else {
            foreach ($order->orderDetails->where('seller_id', \App\User::where('user_type', 'admin')->first()->id) as $key => $orderDetail) {
                $orderDetail->delivery_status = $request->status;
                $orderDetail->save();
            }
        }
        if($request->status=="delivered"){
                
        $order->payment_status = 'paid';
        $order->save();  
                }
        if($request->status=="on_review"){
            $data_base[0]['body'] = env("ORDER_PROCESSING"); 
        }else if($request->status=="on_delivery"){
            $data_base[0]['body'] = env("ORDER_DISPATCHED");
        }
   
   if($request->status=="on_review" ||  $request->status=="on_delivery"){
    $product_data=$order->orderDetails;
    $name_of_product="";
    foreach($product_data as $item){
        if($name_of_product==""){
            $name_of_product=substr($item->product->name, 0, 5)."..";
        }else{
            $name_of_product= $name_of_product.",".substr($item->product->name, 0, 5)."..";
        }
      
    }
    
      

     $vars = array(
            '{$name}'       => $order->user->name,
            '{$ordercode}'       => $order->code,
            '{$itemsname}'       => $name_of_product
        );

        $msg = strtr($data_base[0]['body'], $vars);
      
        $apiKey = urlencode("PBnYEfajueU-ZLgRhC24TA3poA2CQ1TSzFMndmR29d");
     

        $sender = urlencode("LOVOCL");
        $message = rawurlencode($msg);


     
        $data = array("apikey" => $apiKey, "numbers" => $order->user->phone_no, "sender" => $sender, "message" => $message);
      
        $url = "https://api.textlocal.in/send/";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $buffer = curl_exec($ch);
        curl_close($ch);
}
        if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated && \App\OtpConfiguration::where('type', 'otp_for_delivery_status')->first()->value) {
            try {
                $otpController = new OTPVerificationController;
                $otpController->send_delivery_status($order);
            } catch (\Exception $e) {
            }
        }

        return 1;
    }
    public function price_calculator(Request $request)
    {
        $timestamp = time();
        $appID = 4613;
        $key = "jIZz/Be60J8=";
        $secret = "j2dkS8en9l59rhwYrKcW4tQbmaCzYtBeKAUyvKUfrRKxDNvNSh72fvrkYUObVBgDO33mezg4Pg1QJl10IVYxkw==";
        $seller_id = "36422";
        if($request->order_id!=""){
            $order = Order::findOrFail($request->order_id);

            $det = json_decode($order->shipping_address);
            $seller_address= Shop::where('user_id',\Auth::user()->id)->first();
            $source_pin =$seller_address->postal_code;
            $destination_pin = $det->postal_code;
            $order_det = $order->orderDetails->where('seller_id', Auth::user()->id);
        //echo json_encode($order_det);die;
        $amount=0;
        foreach ($order_det as $order_details) {
           
            $amount=$amount+$order_details->price+$order_details->tax+$order_details->shipping_cost;
        }
            if ($order->payment_type == "cash_on_delivery") {
                $orderType = "2";
                $invoice_value = $amount;
            } else {
                $orderType = "1";
                $invoice_value = "";
            }
        }else{
            $source_pin = $request->source_pin;
            $destination_pin = $request->destination_pin;
            $orderType = $request->orderType;
            $invoice_value = $request->invoice_value;
        }
        
        $length = $request->length;
        $height = $request->height;
        $width = $request->width;
        $weight = $request->weight;
        $mode_type = $request->mode_type;
        
        $sign = "key:" . $key . "id:" . $appID . ":timestamp:" . $timestamp;
        $authtoken = rawurlencode(base64_encode(hash_hmac('sha256', $sign, $secret, true)));
        $ch = curl_init();

        $data = array(
            "sourcePin" => $source_pin,
            "destinationPin" => $destination_pin,
            "orderType" => $orderType,
            "length" => $length,
            "width" => $width,
            "height" => $height,
            "weight" => $weight,
            "invoiceValue" => $invoice_value
        );
// echo json_encode($data);die;
        $data_json = json_encode($data);

        $header = array(
            "x-appid: $appID",
            "x-sellerid:$seller_id",
            "x-timestamp: $timestamp",
            "x-version:3", // for auth version 3.0 only
            "Authorization: $authtoken",
            "Content-Type: application/json",
            "Content-Length: " . strlen($data_json)
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.shyplite.com/pricecalculator');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);

        curl_close($ch);
        return $response;
    }
    public function getserviceability($order_id)
    {
        $timestamp = time();
        $appID = 4613;
        $key = "jIZz/Be60J8=";
        $secret = "j2dkS8en9l59rhwYrKcW4tQbmaCzYtBeKAUyvKUfrRKxDNvNSh72fvrkYUObVBgDO33mezg4Pg1QJl10IVYxkw==";
        $seller_id = "36422";
        $order = Order::findOrFail($order_id);
        $det = json_decode($order->shipping_address);
        $source_pin = \Auth::user()->postal_code;
        $destination_pin = $det->postal_code;

        $sign = "key:" . $key . "id:" . $appID . ":timestamp:" . $timestamp;
        $authtoken = rawurlencode(base64_encode(hash_hmac('sha256', $sign, $secret, true)));
        $ch = curl_init();

        $header = array(
            "x-appid: $appID",
            "x-timestamp: $timestamp",
            "x-sellerid:$seller_id",
            "x-version:3", // for auth version 3.0 only
            "Authorization: $authtoken"
        );

        curl_setopt($ch, CURLOPT_URL, "https://api.shyplite.com/getserviceability/$source_pin/$destination_pin");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        // var_dump($server_output);
        // exit;
        curl_close($ch);
        return $server_output;
    }
    public function set_order(Request $request)
    {
        $order = Order::findOrFail($request->order_id);
        $order_det = $order->orderDetails->where('seller_id', Auth::user()->id);
        //echo json_encode($order_det);die;
        $skulist = array();
        $amount=0;
        $orderId = date('Ymd-His') . rand(10, 99);
        $length_total = $request->length;
        $height_total = $request->height;
        $width_total = $request->width;
        $weight_total = $request->weight;
        $CuriorOrder=new CuriorOrder();
        $CuriorOrder->order_id=$request->order_id;
        $CuriorOrder->seller_id=\Auth::user()->id;
        $CuriorOrder->tracking_id=$orderId;
        $CuriorOrder->length=$length_total;
        $CuriorOrder->width=$width_total;
        $CuriorOrder->height=$height_total;
        $CuriorOrder->weight=$weight_total;
        $CuriorOrder->curior_mode=$request->mode_type;
        $CuriorOrder->curior_charge=$request->dv_cost;
        $CuriorOrder->save();
        foreach ($order_det as $order_details) {
            $product = array();
            $stockdet="";
            $variation = $order_details->variation;
            if($variation!="" && $variation!=null){
                $stock = $order_details->product->stocks;
                foreach ($stock as $stock_det) {
                    if ($variation == $stock_det->variant) {
                        $stockdet = $stock_det->sku;
                    }
                }
            }else{
                $stockdet = $order_details->product->name;
            }
            
            
            $amount=$amount+$order_details->price+$order_details->tax+$order_details->shipping_cost;
            $length="length".$order_details->product->id;
            $width="width".$order_details->product->id;
            $height="height".$order_details->product->id;
            $weight="weight".$order_details->product->id;
            

            $product['sku'] = $stockdet;
            $product['itemName'] = $order_details->product->name;
            $product['quantity'] = $order_details->quantity;
            $product['price'] = $order_details->price;
            $product['itemLength'] = $request->$length;
            $product['itemWidth'] =  $request->$width;
            $product['itemHeight'] =  $request->$height;
            $product['itemWeight'] =  $request->$weight;
            array_push($skulist, $product);
            $order_details->length=$request->$length;
            $order_details->width=$request->$width;
            $order_details->height=$request->$height;
            $order_details->weight=$request->$weight;
            $order_details->curior_order_id=$CuriorOrder->id;
            
            $order_details->save();
        }

        $det = json_decode($order->shipping_address);
        
        $mode_type = ucfirst($request->mode_type);
        $invoice_value = $amount;
        
        if ($order->payment_type == "cash_on_delivery") {
            $orderType = "COD";
        } else {
            $orderType = "Prepaid";
        }
        $customerName = $det->name;
        $customerAddress = $det->address;
        $customerCity = $det->city;
        $customerPinCode = $det->postal_code;
        $customerContact = $det->phone;
        $sellerAddressId = Auth::user()->address_id;
        $timestamp = time();
        $orderDate = date('yy-m-d');
        $appID = 4613;
        $key = "jIZz/Be60J8=";
        $secret = "j2dkS8en9l59rhwYrKcW4tQbmaCzYtBeKAUyvKUfrRKxDNvNSh72fvrkYUObVBgDO33mezg4Pg1QJl10IVYxkw==";
        $seller_id = "36422";
        $sign = "key:" . $key . "id:" . $appID . ":timestamp:" . $timestamp;
        $authtoken = rawurlencode(base64_encode(hash_hmac('sha256', $sign, $secret, true)));
        $ch = curl_init();

        $data = array(
            "orders" => array(
                "0" => array(
                    "orderId" => $orderId,
                    "customerName" => $customerName,
                    "customerAddress" => $customerAddress,
                    "customerCity" => $customerCity,
                    "customerPinCode" => $customerPinCode,
                    "customerContact" => $customerContact,
                    "orderType" => $orderType,
                    "modeType" => $mode_type,
                    "orderDate" => $orderDate,
                    "package" => array(
                        "itemLength" => $length_total,
                        "itemWidth" => $width_total,
                        "itemHeight" => $height_total,
                        "itemWeight" => $weight_total
                    ),
                    "skuList" => $skulist,
                    "totalValue" => $invoice_value,
                    "sellerAddressId" => $sellerAddressId
                )
            )
        );
        
        // $data = Array(
        //     "orders" => Array(
        //         "0" => Array(
        //             "orderId" => "TSTAPI038sss",
        //             "customerName" => "Pushpendra Kumar",
        //             "customerAddress" => "Address Line1, Address Line2, Address Line3",
        //             "customerCity" => "New Delhi",
        //             "customerPinCode" => "110016",
        //             "customerContact" => "9876543210",
        //             "orderType" => "Prepaid",
        //             "modeType" => "Air",
        //             "orderDate" => "2019-11-26",
        //             "package" => Array( 
        //                 "itemLength" => "12",
        //                 "itemWidth" => "15",
        //                 "itemHeight"=> "20",
        //                 "itemWeight" => "1.5"
        //             ),
        //             "skuList" => Array(
        //                 "0" => Array(
        //                     "sku" => "Test",
        //                     "itemName" => "Item1",
        //                     "quantity" => 1,
        //                     "price" => 45.00,
        //                     "itemLength" => "", //optional
        //                     "itemWidth" => "", //optional
        //                     "itemHeight"=> "", //optional
        //                     "itemWeight" => "" //optional
        //                 ),
        //                 "1" => Array(
        //                     "sku" => "Test1",
        //                     "itemName" => "Item2",
        //                     "quantity" => 1,
        //                     "price" => 45.00,
        //                     "itemLength" => "", //optional
        //                     "itemWidth" => "", //optional
        //                     "itemHeight"=> "", //optional
        //                     "itemWeight" => "" //optional
        //                 ),
        //                 "2" => Array(
        //                     "sku" => "Test1",
        //                     "itemName" => "Item3",
        //                     "quantity" => 1,
        //                     "price" => 45.00,
        //                     "itemLength" => "", //optional
        //                     "itemWidth" => "", //optional
        //                     "itemHeight"=> "", //optional
        //                     "itemWeight" => "" //optional
        //                 )
        //             ),
        //             "totalValue" => 1320,
        //             "sellerAddressId" =>$sellerAddressId
        //         )
        //     )
        // );
 //echo json_encode($data);die;
        $data_json = json_encode($data);

        $header = array(
            "x-appid: $appID",
            "x-sellerid:$seller_id",
            "x-timestamp: $timestamp",
            "x-version:3", // for auth version 3.0 only
            "Authorization: $authtoken",
            "Content-Type: application/json",
            "Content-Length: " . strlen($data_json)
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.shyplite.com/order?method=sku');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        //  var_dump($response);
        //  exit;
        // curl_close($ch);
        // $order = Order::findOrFail($request->order_id);
        // $order->curior_order = 1;
        // $order->save();
        flash('Order has been Added For Shipment')->success();
    return back();

    }
    public function generate_silp(Request $request)
    {
        
        $orderId = $request->order_id;
        $timestamp = time();
        $appID = 4613;
        $key = "jIZz/Be60J8=";
        $secret = "j2dkS8en9l59rhwYrKcW4tQbmaCzYtBeKAUyvKUfrRKxDNvNSh72fvrkYUObVBgDO33mezg4Pg1QJl10IVYxkw==";
        $seller_id = "36422";
        $sign = "key:" . $key . "id:" . $appID . ":timestamp:" . $timestamp;
        $authtoken = rawurlencode(base64_encode(hash_hmac('sha256', $sign, $secret, true)));
        $ch = curl_init();

        $header = array(
            "x-appid: $appID",
            "x-timestamp: $timestamp",
            "x-sellerid:$seller_id",
            "Authorization: $authtoken",
            "Content-Type: application/json",
            "x-version:3"
        );

        curl_setopt($ch, CURLOPT_URL, 'https://api.shyplite.com/getSlip?orderID=' . urlencode($orderId));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
       $response=json_decode($server_output);

       $carrierName=$response->carrierName;
       $awbNo=$response->awbNo;
       $manifestID=$response->manifestID;
       $slip_file=$response->fileName;
       $filepath=$response->s3Path[0];
     

        curl_close($ch);
        $CuriorOrder = CuriorOrder::where('tracking_id',$orderId)->first();
        $order = CuriorOrder::findOrFail($CuriorOrder->id);
        $order->carrierName =$carrierName;
        $order->awbNo =$awbNo;
        $order->manifestID =$manifestID;
        $order->s3Path_slip = $filepath;
        $order->fileName_slip =$slip_file;
        $order->save();
        return  $filepath;
        
    }

    public function generate_manifest(Request $request)
    {
        $orderId=$request->order_id;
        $CuriorOrder = CuriorOrder::where('tracking_id',$orderId)->first();
        $order = CuriorOrder::findOrFail($CuriorOrder->id);
        $timestamp = time();
        $manifestID = $order->manifestID;
        $appID = 4613;
        $key = "jIZz/Be60J8=";
        $secret = "j2dkS8en9l59rhwYrKcW4tQbmaCzYtBeKAUyvKUfrRKxDNvNSh72fvrkYUObVBgDO33mezg4Pg1QJl10IVYxkw==";
        $seller_id = "36422";
        $sign = "key:" . $key . "id:" . $appID . ":timestamp:" . $timestamp;
        $authtoken = rawurlencode(base64_encode(hash_hmac('sha256', $sign, $secret, true)));
        $ch = curl_init();
        $header = array(
            "x-appid: $appID",
            "x-timestamp: $timestamp",
            "x-sellerid:$seller_id",
            "x-version:3", // for auth version 3.0 only
            "Authorization: $authtoken"
        );

        curl_setopt($ch, CURLOPT_URL, "https://api.shyplite.com/getManifestPDF/$manifestID");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        $response=json_decode($server_output);
        
        curl_close($ch);
        
        $order->s3Path_manifest = $response->s3Path;
        $order->name_manifest =$response->fileName;
        $order->save();
        return $response->s3Path;
    }
    public function cancel_order(Request $request)
    {
        $orderId=$request->order_id;
        
        $timestamp = time();
        $appID = 4613;
        $key = "jIZz/Be60J8=";
        $secret = "j2dkS8en9l59rhwYrKcW4tQbmaCzYtBeKAUyvKUfrRKxDNvNSh72fvrkYUObVBgDO33mezg4Pg1QJl10IVYxkw==";
        $seller_id = "36422";
        $sign = "key:" . $key . "id:" . $appID . ":timestamp:" . $timestamp;
        $authtoken = rawurlencode(base64_encode(hash_hmac('sha256', $sign, $secret, true)));
        $ch = curl_init();

        $data = array(
            "orders" => [$orderId]
        );

        $data_json = json_encode($data);

        $header = array(
            "x-appid: $appID",
            "x-sellerid:$seller_id",
            "x-timestamp: $timestamp",
            "x-version:3", // for auth version 3.0 only
            "Authorization: $authtoken",
            "Content-Type: application/json",
            "Content-Length: " . strlen($data_json)
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.shyplite.com/ordercancel');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        
        curl_close($ch);
        $CuriorOrder = CuriorOrder::where('tracking_id',$orderId)->first();
        $order = CuriorOrder::findOrFail($CuriorOrder->id);
        $order->curior_status=2;
        $order->save();
        return true;
    }

    public function track($tracking_id)
    {
        //$order = Order::findOrFail($request->order_id);
        $orderId = $tracking_id;
       // echo $orderId;die;
        $timestamp = time();
        $appID = 4613;
        $key = "jIZz/Be60J8=";
        $secret = "j2dkS8en9l59rhwYrKcW4tQbmaCzYtBeKAUyvKUfrRKxDNvNSh72fvrkYUObVBgDO33mezg4Pg1QJl10IVYxkw==";
        $seller_id = "36422";
        $sign = "key:" . $key . "id:" . $appID . ":timestamp:" . $timestamp;
        $authtoken = rawurlencode(base64_encode(hash_hmac('sha256', $sign, $secret, true)));
        $ch = curl_init();
        $header = array(
            "x-appid: $appID",
            "x-timestamp: $timestamp",
            "x-sellerid:$seller_id",
            "x-version:3", // for auth version 3.0 only
            "Authorization: $authtoken"
        );

        curl_setopt($ch, CURLOPT_URL, "https://api.shyplite.com/track/$orderId");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        // var_dump($server_output);
        // exit;
        curl_close($ch);
        return json_decode($server_output);
       
    }

    public function update_payment_status(Request $request)
    {
        $order = Order::findOrFail($request->order_id);
        $order->payment_status_viewed = '0';
        $order->save();

        if (Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'seller') {
            foreach ($order->orderDetails->where('seller_id', Auth::user()->id) as $key => $orderDetail) {
                $orderDetail->payment_status = $request->status;
                $orderDetail->save();
            }
        } else {
            foreach ($order->orderDetails->where('seller_id', \App\User::where('user_type', 'admin')->first()->id) as $key => $orderDetail) {
                $orderDetail->payment_status = $request->status;
                $orderDetail->save();
            }
        }

        $status = 'paid';
        foreach ($order->orderDetails as $key => $orderDetail) {
            if ($orderDetail->payment_status != 'paid') {
                $status = 'unpaid';
            }
        }
        $order->payment_status = $status;
        $order->save();


        if ($order->payment_status == 'paid' && $order->commission_calculated == 0) {
            if (\App\Addon::where('unique_identifier', 'seller_subscription')->first() == null || !\App\Addon::where('unique_identifier', 'seller_subscription')->first()->activated) {
                if ($order->payment_type == 'cash_on_delivery') {
                    if (BusinessSetting::where('type', 'category_wise_commission')->first()->value != 1) {
                        $commission_percentage = BusinessSetting::where('type', 'vendor_commission')->first()->value;
                        foreach ($order->orderDetails as $key => $orderDetail) {
                            $orderDetail->payment_status = 'paid';
                            $orderDetail->save();
                            if ($orderDetail->product->user->user_type == 'seller') {
                                $seller = $orderDetail->product->user->seller;
                                $seller->admin_to_pay = $seller->admin_to_pay - ($orderDetail->price * $commission_percentage) / 100;
                                $seller->save();
                            }
                        }
                    } else {
                        foreach ($order->orderDetails as $key => $orderDetail) {
                            $orderDetail->payment_status = 'paid';
                            $orderDetail->save();
                            if ($orderDetail->product->user->user_type == 'seller') {
                                $commission_percentage = $orderDetail->product->subcategory->commision_rate;
                                $seller = $orderDetail->product->user->seller;
                                $seller->admin_to_pay = $seller->admin_to_pay - ($orderDetail->price * $commission_percentage) / 100;
                                $seller->save();
                            }
                        }
                    }
                } elseif ($order->manual_payment) {
                    if (BusinessSetting::where('type', 'category_wise_commission')->first()->value != 1) {
                        $commission_percentage = BusinessSetting::where('type', 'vendor_commission')->first()->value;
                        foreach ($order->orderDetails as $key => $orderDetail) {
                            $orderDetail->payment_status = 'paid';
                            $orderDetail->save();
                            if ($orderDetail->product->user->user_type == 'seller') {
                                $seller = $orderDetail->product->user->seller;
                                $seller->admin_to_pay = $seller->admin_to_pay + ($orderDetail->price * (100 - $commission_percentage)) / 100;
                                $seller->save();
                            }
                        }
                    } else {
                        foreach ($order->orderDetails as $key => $orderDetail) {
                            $orderDetail->payment_status = 'paid';
                            $orderDetail->save();
                            if ($orderDetail->product->user->user_type == 'seller') {
                                $commission_percentage = $orderDetail->product->subcategory->commision_rate;
                                $seller = $orderDetail->product->user->seller;
                                $seller->admin_to_pay = $seller->admin_to_pay + ($orderDetail->price * (100 - $commission_percentage)) / 100;
                                $seller->save();
                            }
                        }
                    }
                }
            }

            if (\App\Addon::where('unique_identifier', 'affiliate_system')->first() != null && \App\Addon::where('unique_identifier', 'affiliate_system')->first()->activated) {
                $affiliateController = new AffiliateController;
                $affiliateController->processAffiliatePoints($order);
            }

            if (\App\Addon::where('unique_identifier', 'club_point')->first() != null && \App\Addon::where('unique_identifier', 'club_point')->first()->activated) {
                $clubpointController = new ClubPointController;
                $clubpointController->processClubPoints($order);
            }

            $order->commission_calculated = 1;
            $order->save();
        }

        if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated && \App\OtpConfiguration::where('type', 'otp_for_paid_status')->first()->value) {
            try {
                $otpController = new OTPVerificationController;
                $otpController->send_payment_status($order);
            } catch (\Exception $e) {
            }
        }
        return 1;
    }


    public function licenseUpload(Request $request)
    {

        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occered in Json call.');
            return response()->json($response, $statusCode);
        }

        $this->validate($request, [


            'license_key' => 'required',
        ], [
            'license_key.required' => 'Please enter license key.',

        ]);
        try {
            $order_id = $request->order_id;
            $orderdetailsid = $request->orderdetailsid;
            $licensekey=encrypt($request['license_key']);
            $msg = "License key uploaded successfully.";
            $input = [
                'license_key' => $licensekey,
                'is_viewed'=>0
                    ];
            LicenseKey::insert([
                'seller_id' => Auth::user()->id,
                'order_detailsid' => $orderdetailsid,
                'licensekey' => $licensekey,
               
            ]);

            OrderDetail::where('id', $orderdetailsid)
                ->update($input);

            $response = array('status' => 1, 'msg' => $msg);
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function listlicense(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occered in Json call.');
            return response()->json($response, $statusCode);
        }

        $this->validate($request, [


            'orderdetailsid' => 'required',
        ], [
            'orderdetailsid.required' => 'Code is required.',

        ]);
        try {
            $html = "";
            $count = 0;
            $orderdetailsid = $request->orderdetailsid;
            $data = LicenseKey::where('order_detailsid', $orderdetailsid)->get();
            $dataCount=count($data);
            $html .=   '<h3>Previously added license key</h3>';
            $html .=   '<table class="table table-sm table-hover table-responsive-md">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .=  '<th>' . translate("Sl#") . '</th>';
            $html .=  '<th>' . translate('License Key') . '</th>';


            $html .= '</tr>';
            $html .= ' </thead>';
            $html .= '<tbody>';
            foreach ($data as $val) {
                $count++;
                $html .= '<tr>';
                $html .= '<td>' . $count . '</td>';
                $html .= '<td>' . decrypt($val->licensekey) . '</td>';
                $html .= '</tr>';
            }
            $html .= '</tbody>';
            $html .= '</table>';
            $response = array('dataCount' => $dataCount, 'html' => $html);
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }
    
    
    public function test_order(Request $request){
        $order_id=Order::where('id', \DB::raw("(select max(`id`) from orders)"))->get();
        if(count($order_id)>0){
         $orde_rand_id=sprintf("%02d", $order_id[0]->id) + 1;   
        }else{
         $orde_rand_id=01;     
        }
        return  $orde_rand_id;
    }
}
