<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\band_of_trust;
use CoreComponentRepository;

class BandTrustController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //CoreComponentRepository::instantiateShopRepository();
        $data = band_of_trust::all();
        return view('Band_Trust.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('Band_Trust.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->file('logo_img')!=''){
            list($width, $height, $type, $attr) = getimagesize($request->file('logo_img')); 
            if($width!=512 && $height!=512){
                flash(__('Please  Image  width=512 and height=512 '))->error();
                return back();
            }
        }

        $Band = new band_of_trust;
        $Band->title = $request->title;
        if($request->hasFile('logo_img')){
            $Band->image_name = $request->logo_img->store('uploads/Band_Trust');
        }

        if($Band->save()){
            flash(__('Band Of Trust has been inserted successfully'))->success();
            return redirect()->route('Band_Trust.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = band_of_trust::findOrFail(decrypt($id));
        return view('Band_Trust.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->file('logo_img')!=''){
            unlink($request->old_img);
            list($width, $height, $type, $attr) = getimagesize($request->file('logo_img')); 
            if($width!=512 && $height!=512){
                flash(__('Please  Image  width=512 and height=512 '))->error();
                return back();
            }
        }

        $Band = band_of_trust::findOrFail($id);
        $Band->title = $request->title;

        if($request->hasFile('logo_img')){
            $Band->image_name = $request->logo_img->store('uploads/Band_Trust');
        }

        if($Band->save()){
            flash(__('Band Of Trust has been updated successfully'))->success();
            return redirect()->route('Band_Trust.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { 
        $Band = band_of_trust::findOrFail($id);
        unlink($Band->image_name);
        if(band_of_trust::destroy($id)){ 
            flash(__('Band Of Trust has been deleted successfully'))->success();
            return redirect()->route('Band_Trust.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }
}

