<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Emailtemplate;
use Illuminate\Support\Str;

class EmailTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sort_search =null;
        $e_template = Emailtemplate::orderBy('created_at', 'desc');
        if ($request->has('search')){
            $sort_search = $request->search;
            $e_template = $e_template->where('subject', 'like', '%'.$sort_search.'%');
        }
        $e_template = $e_template->paginate(15);
        return view('email_template.index', compact('e_template', 'sort_search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('email_template.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $e_template = new Emailtemplate;

        // if ($request->slug != null) {
        //     $e_template->slug = str_replace(' ', '-', $request->slug);
        // }
        // else {
        //     $e_template->slug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->subject)).'-'.Str::random(5);
        // }
       
        $e_template->subject = $request->subject;
        $e_template->mail_body = $request->mail_body;
        $e_template->title = $request->title;
        
        if($e_template->save()){
            flash(__('Email Template Details has been inserted successfully'))->success();
            return redirect()->route('email_template.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $e_template = Emailtemplate::findOrFail(decrypt($id));
        return view('email_template.edit', compact('e_template'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $e_template = Emailtemplate::findOrFail($id);
        
        // if ($request->slug != null) {
        //     $e_template->slug = str_replace(' ', '-', $request->slug);
        // }
        // else {
        //     $e_template->slug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->subject)).'-'.Str::random(5);
        // }
        $e_template->title = $request->title;
        $e_template->subject = $request->subject;
        $e_template->mail_body = $request->mail_body;

        if($e_template->save()){
            flash(__('Email Template Details has been updated successfully'))->success();
            return redirect()->route('email_template.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //$e_template = Emailtemplate;
        if(Emailtemplate::destroy($id)){
            
            flash(__('Email Template Details has been deleted successfully'))->success();
            return redirect()->route('email_template.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }
}
