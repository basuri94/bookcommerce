<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductStock;
use App\Category;
use App\Brand;
use App\Color;
use App\Language;
use App\Ticket;
use App\TicketReply;
use Auth;
use App\SubSubCategory;
use Session;
use ImageOptimizer;
use DB;
use CoreComponentRepository;
use Illuminate\Support\Str;
use App\Conversation;

class ProductController extends Controller
{



    public function product_approve(Request $request)
    {
      $id = $request->product_id;
        $products = Product::findOrFail($id);
        $products->is_approve =1;
        $products->published =1;
        if($products->save()){
            return 1;
        }
        else{
            return 0;
        }
    }
    
    public function product_reject(Request $request)
    {
        $id = $request->product_id;
        $seller_id = $request->seller_id;
        $added_by = $request->added_by;
        $remark = $request->remark;
        $name = $request->name;

        $products = Product::findOrFail($id);
        $products->is_approve =0;
        $products->published =0;
        $products->save();
        
        $ticket = new Ticket;
        $ticket->code = max(100000, (Ticket::latest()->first() != null ? Ticket::latest()->first()->id + 1 : 0)).date('s');
        $ticket->user_id = $seller_id;
        $ticket->subject = 'Product Approved';
        $ticket->details = $name;
        $ticket->status = 'pending';
        $ticket->save();
       $last_id=Ticket::latest()->first()->id;
      
        $ticket_reply = new TicketReply;
        $ticket_reply->ticket_id = $last_id;
        $ticket_reply->user_id = Auth::user()->id;
        $ticket_reply->reply = $remark;

        if($ticket_reply->save()){
            return 1;
        }
        else{
            return 0;
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_products(Request $request)
    { 
        //CoreComponentRepository::instantiateShopRepository();

        $type = 'Book details';
        $col_name = null;
        $query = null;
        $sort_search = null;

        $products = Product::where('added_by', 'admin');

        if ($request->type != null){
            $var = explode(",", $request->type);
            $col_name = $var[0];
            $query = $var[1];
            $products = $products->orderBy($col_name, $query);
            $sort_type = $request->type;
        }
        if ($request->search != null){
            $products = $products
                        ->where('name', 'like', '%'.$request->search.'%');
            $sort_search = $request->search;
        }

        $products = $products->where('digital', 0)->orderBy('created_at', 'desc')->paginate(1000000);

        return view('products.index', compact('products','type', 'col_name', 'query', 'sort_search'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function seller_products(Request $request)
    { 
        $col_name = null;
        $query = null;
        $seller_id = null;
        $sort_search = null;
        $products = Product::where('added_by', 'seller');
        if ($request->has('user_id') && $request->user_id != null) {
            $products = $products->where('user_id', $request->user_id);
            $seller_id = $request->user_id;
        }
        $products = $products->where('is_approve', 1);
        if ($request->search != null){
            $products = $products
                        ->where('name', 'like', '%'.$request->search.'%');
            $sort_search = $request->search;
        }
        if ($request->type != null){
            $var = explode(",", $request->type);
            $col_name = $var[0];
            $query = $var[1];
            $products = $products->orderBy($col_name, $query);
            $sort_type = $request->type;
        }

        $products = $products->orderBy('created_at', 'desc')->paginate(1000000);
        $type = 'Seller';

        return view('products.index', compact('products','type', 'col_name', 'query', 'seller_id', 'sort_search'));
    }

    public function seller_products_pending(Request $request)
    { 
        $col_name = null;
        $query = null;
        $seller_id = null;
        $sort_search = null;
        $products = Product::where('added_by', 'seller');
        if ($request->has('user_id') && $request->user_id != null) {
            $products = $products->where('user_id', $request->user_id);
            $seller_id = $request->user_id;
        }
        $products = $products->where('is_approve', 0);
        if ($request->search != null){
            $products = $products
                        ->where('name', 'like', '%'.$request->search.'%');
            $sort_search = $request->search;
        }
        if ($request->type != null){
            $var = explode(",", $request->type);
            $col_name = $var[0];
            $query = $var[1];
            $products = $products->orderBy($col_name, $query);
            $sort_type = $request->type;
        }

        $products = $products->orderBy('created_at', 'desc')->paginate(1000000);
        $type = 'Seller';

        return view('products.index_pending', compact('products','type', 'col_name', 'query', 'seller_id', 'sort_search'));
    }


    public function seller_products_strike(Request $request)
    { 
        $col_name = null;
        $query = null;
        $seller_id = null;
        $sort_search = null;
         $conversation = Conversation::get()->toArray();
         $conversation_arr = array_column($conversation, 'product_id');
        
        $products = Product::whereIn('id',$conversation_arr)->where('added_by', 'seller');
        if ($request->has('user_id') && $request->user_id != null) {
            $products = $products->where('user_id', $request->user_id);
            $seller_id = $request->user_id;
        }
        $products = $products->where('is_approve', 0);
        if ($request->search != null){
            $products = $products
                        ->where('name', 'like', '%'.$request->search.'%');
            $sort_search = $request->search;
        }
        if ($request->type != null){
            $var = explode(",", $request->type);
            $col_name = $var[0];
            $query = $var[1];
            $products = $products->orderBy($col_name, $query);
            $sort_type = $request->type;
        }

        $products = $products->orderBy('products.created_at', 'desc')->paginate(10000000000);
        $type = 'Seller';

        return view('products.index_strike', compact('products','type', 'col_name', 'query', 'seller_id', 'sort_search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { //dd($request->all());
       
        $refund_request_addon = \App\Addon::where('unique_identifier', 'refund_request')->first();

        $product = new Product;
        $product->name =ucwords($request->name);
        $product->added_by = $request->added_by;
        if(Auth::user()->user_type == 'seller'){
            $product->user_id = Auth::user()->id;
           
        }
        else{
            $product->user_id = \App\User::where('user_type', 'admin')->first()->id;
            $product->is_approve = 1;
        }
        $product->category_id = $request->category_id;
        $product->subcategory_id = $request->subcategory_id;
        $product->subsubcategory_id = $request->subsubcategory_id;
      
        $product->current_stock = $request->current_stock;
        $product->barcode = $request->barcode;
        $product->hsn_id = $request->SubCategoryHsn_id;
        $product->unit_name = $request->unit_name;
        $product->country_of_origin = $request->country_of_origin;
        if(!empty($request->add_brand)){
            $brand=Brand::create([
                'name'=>$request->add_brand
            ]);
        }
        if(!empty($request->add_brand)){
            $product->brand_id = $brand->id;
        }
        else{
            $product->brand_id = $request->brand_id;
        }
       
            if ($request->refundable != null) {
                $product->refundable = 1;
            }
            else {
                $product->refundable = 0;
            }
            if ($request->no_delivery != null) {
                $product->no_delivery = 1;
            }
            else {
                $product->no_delivery = 0;
            }
            if ($request->days_return != null) {
                $product->days_return = 1;
            }
            else {
                $product->days_return = 0;
            }
            if ($request->b_warrenty != null) {
                $product->b_warrenty = 1;
            }
            else {
                $product->b_warrenty = 0;
            }
            if ($request->cod != null) {
                $product->cod = 1;
            }
            else {
                $product->cod = 0;
            }
            if ($request->online_payment != null) {
                $product->online_payment = 1;
            }
            else {
                $product->online_payment = 0;
            }
            /*if($request->file('photos')!=''){
                list($width, $height, $type, $attr) = getimagesize($request->file('photos')); 
                if($width!=190 && $height!=70){
                    flash(__('Please logo image  width=190 and height=70 '))->error();
                    return back();
                }
            }*/
            if($request->file('thumbnail_img')!=''){
                list($width, $height, $type, $attr) = getimagesize($request->file('thumbnail_img')); 
                if($width!=200 && $height!=280){
                    flash(__('Please Thumbnail Image image  width=200 and height=280 '))->error();
                    return back();
                }
            }
            if($request->file('featured_img')!=''){
                list($width, $height, $type, $attr) = getimagesize($request->file('featured_img')); 
                if($width!=200 && $height!=280){
                    flash(__('Please Featured image  width=200 and height=280 '))->error();
                    return back();
                }
            }
            if($request->file('flash_deal_img')!=''){
                list($width, $height, $type, $attr) = getimagesize($request->file('flash_deal_img')); 
                if($width!=200 && $height!=280){
                    flash(__('Please Flash Deal image  width=200 and height=280 '))->error();
                    return back();
                }
            }
           /* if($request->file('meta_img')!=''){
                list($width, $height, $type, $attr) = getimagesize($request->file('meta_img')); 
                if($width!=200 && $height!=280){
                    flash(__('Please Flash Deal image  width=200 and height=280 '))->error();
                    return back();
                }
            }*/
        $photos = array();

        

        if($request->hasFile('thumbnail_img')){
            $product->thumbnail_img = $request->thumbnail_img->store('uploads/products/thumbnail');
            //ImageOptimizer::optimize(base_path('public/').$product->thumbnail_img);
        }

        if($request->hasFile('featured_img')){
            $product->featured_img = $request->featured_img->store('uploads/products/featured');
            //ImageOptimizer::optimize(base_path('public/').$product->featured_img);
        }

        if($request->hasFile('flash_deal_img')){
            $product->flash_deal_img = $request->flash_deal_img->store('uploads/products/flash_deal');
            //ImageOptimizer::optimize(base_path('public/').$product->flash_deal_img);
        }

        $product->unit = $request->unit;
        //$product->tags = implode('|',$request->tags);
        $product->tags = $request->tags;
        $product->description = $request->description_product_editor;
        $product->specification = $request->specification_product;
        $product->video_provider = $request->video_provider;
        $product->video_link = $request->video_link;
        $product->unit_price = $request->unit_price;
        //$product->purchase_price = $request->purchase_price;
        $product->tax = $request->tax;
        $product->tax_type = 'percent';
        $product->discount = $request->discount;
        $product->discount_type = $request->discount_type;
        $product->shipping_type = $request->shipping_type;
        if ($request->has('shipping_type')) {
            if($request->shipping_type == 'free'){
                $product->shipping_cost = 0;
            }
            elseif ($request->shipping_type == 'flat_rate') {
                $product->shipping_cost = $request->flat_shipping_cost;
            }
        }
        
        $product->meta_title = $request->meta_title;
        $product->meta_description = $request->meta_description;

        if($request->hasFile('meta_img')){
            $product->meta_img = $request->meta_img->store('uploads/products/meta');
            //ImageOptimizer::optimize(base_path('public/').$product->meta_img);
        }

        if($request->hasFile('pdf')){
            $product->pdf = $request->pdf->store('uploads/products/pdf');
        }

        $product->slug = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->name)).'-'.Str::random(5));
        $product->colors = $request->colors;
        
        /*if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $product->colors = json_encode($request->colors);
            $arr_final=[];
            foreach($request->colors as $color_dt){
                $arr_val=[];
                $options=Color::where('code', $color_dt)->first();
                $photos='photos'.$options->id;
                if($request->hasFile($photos)){
                    $photos_arr=[];
                    foreach ($request->$photos as $key => $photo) {
                        $path = $photo->store('uploads/products/photos');
                        array_push($photos_arr, $path);
                    }
                    $arr_val['id']=$options->id;
                    $arr_val['products']=json_encode($photos_arr);
                    array_push($arr_final,$arr_val);
                }
            }
            $product->photos = json_encode($arr_final);
        }
        else {
       
        if($request->hasFile('photos')){
            foreach ($request->photos as $key => $photo) {
                $path = $photo->store('uploads/products/photos');
                array_push($photos, $path);
            }
            $product->photos = json_encode($photos);
        }
            $colors = array();
            $product->colors = json_encode($colors);
        }*/

       
        //echo json_encode($arr_final);die;
       
        $choice_options = array();

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $str = 'choice_options_'.$no;

                $item['attribute_id'] = $no;
                $item['values'] = explode(',', implode('|', $request[$str]));

                array_push($choice_options, $item);
            }
        }

        if (!empty($request->choice_no)) {
            $product->attributes = json_encode($request->choice_no);
        }
        else {
            $product->attributes = json_encode(array());
        }

        $product->choice_options = json_encode($choice_options);
        
        //$variations = array();

        $product->save();
        //echo 'hello';die;
        //combinations start
        /*$options = array();
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $colors_active = 1;
            array_push($options, $request->colors);
        }

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;
                $my_str = implode('|',$request[$name]);
                array_push($options, explode(',', $my_str));
            }
        }*/

        //Generates the combinations of customer choice options
       /* $combinations = combinations($options);
        if(count($combinations[0]) > 0){
            $product->variant_product = 1;
            foreach ($combinations as $key => $combination){
                $str = '';
                foreach ($combination as $key => $item){
                    if($key > 0 ){
                        $str .= '-'.str_replace(' ', '', $item);
                    }
                    else{
                        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
                            $color_name = \App\Color::where('code', $item)->first()->name;
                            $str .= $color_name;
                        }
                        else{
                            $str .= str_replace(' ', '', $item);
                        }
                    }
                }
               

                $product_stock = ProductStock::where('product_id', $product->id)->where('variant', $str)->first();
                if($product_stock == null){
                    $product_stock = new ProductStock;
                    $product_stock->product_id = $product->id;
                }

                $product_stock->variant = $str;
                $product_stock->price = $request['price_'.str_replace('.', '_', $str)];
                $product_stock->sku = $request['sku_'.str_replace('.', '_', $str)];
                $product_stock->qty = $request['qty_'.str_replace('.', '_', $str)];
                $product_stock->save();
            }
        }*/

        foreach (Language::all() as $key => $language) {
            $data = openJSONFile($language->code);
            $data[$product->name] = $product->name;
            saveJSONFile($language->code, $data);
        }

	    $product->save();

        flash(__('Book has been inserted successfully'))->success();
        if(Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'staff'){
            return redirect()->route('products.admin');
        }
        else{
            if(\App\Addon::where('unique_identifier', 'seller_subscription')->first() != null && \App\Addon::where('unique_identifier', 'seller_subscription')->first()->activated){
                $seller = Auth::user()->seller;
                $seller->remaining_uploads -= 1;
                $seller->save();
            }
            return redirect()->route('seller.products');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function admin_product_edit($id)
    {
        $product = Product::findOrFail(decrypt($id));
        $tags = json_decode($product->tags);
        $categories = Category::all();
        return view('products.edit', compact('product', 'categories', 'tags'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function seller_product_edit($id)
    {
        $product = Product::findOrFail(decrypt($id));
        $tags = json_decode($product->tags);
        $categories = Category::all();
        return view('products.edit', compact('product', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { //dd($request->all());
       
        $refund_request_addon = \App\Addon::where('unique_identifier', 'refund_request')->first();
        $product = Product::findOrFail($id);
        $checkApprove=$product->is_approve;
     //   echo $checkApprove;die;
     
        if( $checkApprove==1 && Auth::user()->user_type=="seller"){
            $product->unit_price = $request->unit_price;
            $product->discount = $request->discount;
            $product->save();
        }
        else{

      
        
        $product->name = ucwords($request->name);
        $product->category_id = $request->category_id;
        $product->subcategory_id = $request->subcategory_id;
        $product->subsubcategory_id = $request->subsubcategory_id;
        $product->brand_id = $request->brand_id;
        $product->current_stock = $request->current_stock;
        $product->barcode = $request->barcode;
        $product->hsn_id = $request->SubCategoryHsn_id;
        $product->unit_name = $request->unit_name;
        $product->country_of_origin = $request->country_of_origin;
     //   if ($refund_request_addon != null && $refund_request_addon->activated == 1) {
            if ($request->refundable != null) {
                $product->refundable = 1;
            }
            else {
                $product->refundable = 0;
            }

            if ($request->no_delivery != null) {
                $product->no_delivery = 1;
            }
            else {
                $product->no_delivery = 0;
            }
            if ($request->days_return != null) {
                $product->days_return = 1;
            }
            else {
                $product->days_return = 0;
            }
            if ($request->b_warrenty != null) {
                $product->b_warrenty = 1;
            }
            else {
                $product->b_warrenty = 0;
            }
            if ($request->cod != null) {
                $product->cod = 1;
            }
            else {
                $product->cod = 0;
            }
            if ($request->online_payment != null) {
                $product->online_payment = 1;
            }
            else {
                $product->online_payment = 0;
            }
      //  }

        
/*if($request->file('photos')!=''){
                list($width, $height, $type, $attr) = getimagesize($request->file('photos')); 
                if($width!=190 && $height!=70){
                    flash(__('Please logo image  width=190 and height=70 '))->error();
                    return back();
                }
            }*/
            if($request->file('thumbnail_img')!=''){
                list($width, $height, $type, $attr) = getimagesize($request->file('thumbnail_img')); 
                if($width!=200 && $height!=280){
                    flash(__('Please Thumbnail Image image  width=200 and height=280 '))->error();
                    return back();
                }
            }
            if($request->file('featured_img')!=''){
                list($width, $height, $type, $attr) = getimagesize($request->file('featured_img')); 
                if($width!=200 && $height!=280){
                    flash(__('Please Featured image  width=200 and height=280 '))->error();
                    return back();
                }
            }
            if($request->file('flash_deal_img')!=''){
                list($width, $height, $type, $attr) = getimagesize($request->file('flash_deal_img')); 
                if($width!=200 && $height!=280){
                    flash(__('Please Flash Deal image  width=200 and height=280 '))->error();
                    return back();
                }
            }
           /* if($request->file('meta_img')!=''){
                list($width, $height, $type, $attr) = getimagesize($request->file('meta_img')); 
                if($width!=200 && $height!=280){
                    flash(__('Please Flash Deal image  width=200 and height=280 '))->error();
                    return back();
                }
            }*/
           

        $product->thumbnail_img = $request->previous_thumbnail_img;
        if($request->hasFile('thumbnail_img')){
            $product->thumbnail_img = $request->thumbnail_img->store('uploads/products/thumbnail');
            //ImageOptimizer::optimize(base_path('public/').$product->thumbnail_img);
        }

        $product->featured_img = $request->previous_featured_img;
        if($request->hasFile('featured_img')){
            $product->featured_img = $request->featured_img->store('uploads/products/featured');
            //ImageOptimizer::optimize(base_path('public/').$product->featured_img);
        }

        $product->flash_deal_img = $request->previous_flash_deal_img;
        if($request->hasFile('flash_deal_img')){
            $product->flash_deal_img = $request->flash_deal_img->store('uploads/products/flash_deal');
            //ImageOptimizer::optimize(base_path('public/').$product->flash_deal_img);
        }

        $product->unit = $request->unit;
        $product->tags = implode('|',$request->tags);
        $product->description = $request->description_product_editor;
        $product->specification = $request->specification_product;
        $product->video_provider = $request->video_provider;
        $product->video_link = $request->video_link;
        $product->unit_price = $request->unit_price;
        //$product->purchase_price = $request->purchase_price;
        $product->tax = $request->tax;
        $product->tax_type = "percent";
        $product->discount = $request->discount;
        $product->shipping_type = $request->shipping_type;
        if ($request->has('shipping_type')) {
            if($request->shipping_type == 'free'){
                $product->shipping_cost = 0;
            }
            elseif ($request->shipping_type == 'flat_rate') {
                $product->shipping_cost = $request->flat_shipping_cost;
            }
        }
        $product->discount_type = $request->discount_type;
        $product->meta_title = $request->meta_title;
        $product->meta_description = $request->meta_description;

        $product->meta_img = $request->previous_meta_img;
        if($request->hasFile('meta_img')){
            $product->meta_img = $request->meta_img->store('uploads/products/meta');
            //ImageOptimizer::optimize(base_path('public/').$product->meta_img);
        }

        if($request->hasFile('pdf')){
            $product->pdf = $request->pdf->store('uploads/products/pdf');
        }

        $product->slug = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->name)).'-'.substr($product->slug, -5));

        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $product->colors = json_encode($request->colors);
            $arr_final=[];
            //echo json_encode($request->colors);die;
            foreach($request->colors as $color_dt){
                $arr_val=[];
                $options=Color::where('code', $color_dt)->first();
                $photos='photos'.$options->id;
                $previous_photos='previous_photos'.$options->id;
                if($request->has($previous_photos)){
                    $photos_arr1=[];

                    foreach ($request->$previous_photos as $key => $photo1) {
                        
                        array_push($photos_arr1, $photo1);
                        //ImageOptimizer::optimize(base_path('public/').$path);
                    }
                    $arr_val1['id']=$options->id;
                    $arr_val1['products']=json_encode($photos_arr1);
                    array_push($arr_final,$arr_val1);
                }
                if($request->hasFile($photos)){
                    $photos_arr=[];
                    foreach ($request->$photos as $key => $photo) {
                        $path = $photo->store('uploads/products/photos');
                        // echo $path;die;
                        array_push($photos_arr, $path);
                        //ImageOptimizer::optimize(base_path('public/').$path);
                    }
                    $arr_val['id']=$options->id;
                    $arr_val['products']=json_encode($photos_arr);
                    array_push($arr_final,$arr_val);
                }
            }
            //echo json_encode($arr_final);die;
            $product->photos = json_encode($arr_final);
        }
        else {
            if($request->has('previous_photos')){
                $photos = $request->previous_photos;
            }
            else{
                $photos = array();
            }
        if($request->hasFile('photos')){
            foreach ($request->photos as $key => $photo) {
                $path = $photo->store('uploads/products/photos');
                array_push($photos, $path);
                //ImageOptimizer::optimize(base_path('public/').$path);
            }
        }
        $product->photos = json_encode($photos);
            $colors = array();
            $product->colors = json_encode($colors);
        }

        $choice_options = array();

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $str = 'choice_options_'.$no;

                $item['attribute_id'] = $no;
                $item['values'] = explode(',', implode('|', $request[$str]));

                array_push($choice_options, $item);
            }
        }

        if($product->attributes != json_encode($request->choice_attributes)){
            foreach ($product->stocks as $key => $stock) {
                $stock->delete();
            }
        }

        if (!empty($request->choice_no)) {
            $product->attributes = json_encode($request->choice_no);
        }
        else {
            $product->attributes = json_encode(array());
        }

        $product->choice_options = json_encode($choice_options);

        foreach (Language::all() as $key => $language) {
            $data = openJSONFile($language->code);
            unset($data[$product->name]);
            $data[$request->name] = "";
            saveJSONFile($language->code, $data);
        }

        //combinations start
        $options = array();
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $colors_active = 1;
            array_push($options, $request->colors);
        }

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;
                $my_str = implode('|',$request[$name]);
                array_push($options, explode(',', $my_str));
            }
        }

        $combinations = combinations($options);
        if(count($combinations[0]) > 0){
            $product->variant_product = 1;
            foreach ($combinations as $key => $combination){
                $str = '';
                foreach ($combination as $key => $item){
                    if($key > 0 ){
                        $str .= '-'.str_replace(' ', '', $item);
                    }
                    else{
                        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
                            $color_name = \App\Color::where('code', $item)->first()->name;
                            $str .= $color_name;
                        }
                        else{
                            $str .= str_replace(' ', '', $item);
                        }
                    }
                }

                $product_stock = ProductStock::where('product_id', $product->id)->where('variant', $str)->first();
                if($product_stock == null){
                    $product_stock = new ProductStock;
                    $product_stock->product_id = $product->id;
                }

                $product_stock->variant = $str;
                $product_stock->price = $request['price_'.str_replace('.', '_', $str)];
                $product_stock->sku = $request['sku_'.str_replace('.', '_', $str)];
                $product_stock->qty = $request['qty_'.str_replace('.', '_', $str)];

                $product_stock->save();
            }
        }

        $product->save();
        
    }
   
        flash(__('Book has been updated successfully'))->success();
        if(Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'staff'){
          
            return redirect()->route('products.admin');
        }
        else{
            
            return redirect()->route('seller.products');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        if(Product::destroy($id)){
            foreach (Language::all() as $key => $language) {
                $data = openJSONFile($language->code);
                unset($data[$product->name]);
                saveJSONFile($language->code, $data);
            }
            flash(__('Book has been deleted successfully'))->success();
            if(Auth::user()->user_type == 'admin'){
                return redirect()->route('products.admin');
            }
            else{
                return redirect()->route('seller.products');
            }
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Duplicates the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function duplicate($id)
    {
        $product = Product::find($id);
        
        $product_new = $product->replicate();
        $product_new->slug = strtolower(substr($product_new->slug, 0, -5).Str::random(5));

        if($product_new->save()){
            $product_stock = ProductStock::where('product_id',$id)->get();
            foreach($product_stock as $product_stock_dt){
                $product_stock= ProductStock::find($product_stock_dt->id);
                $product_stock_new=$product_stock->replicate();
                $product_stock_new->product_id=$product_new->id;
                $product_stock_new->save();
            }
        
            flash(__('Book has been duplicated successfully'))->success();
            if(Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'staff'){
                return redirect()->route('products.admin');
            }
            else{
                return redirect()->route('seller.products');
            }
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    public function get_products_by_subsubcategory(Request $request)
    {
        $products = Product::where('subsubcategory_id', $request->subsubcategory_id)->get();
        return $products;
    }

    public function get_products_by_subcategory(Request $request)
    {
        $products = Product::where('subcategory_id', $request->subcategory_id)->get();
        return $products;
    }

    public function get_products_by_brand(Request $request)
    {
        $products = Product::where('brand_id', $request->brand_id)->get();
        return view('partials.product_select', compact('products'));
    }

    public function updateTodaysDeal(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $product->todays_deal = $request->status;
        if($product->save()){
            return 1;
        }
        return 0;
    }

    public function updatePublished(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $product->published = $request->status;

        if($product->added_by == 'seller' && \App\Addon::where('unique_identifier', 'seller_subscription')->first() != null && \App\Addon::where('unique_identifier', 'seller_subscription')->first()->activated){
            $seller = $product->user->seller;
            if($seller->invalid_at != null && Carbon::now()->diffInDays(Carbon::parse($seller->invalid_at), false) <= 0){
                return 0;
            }
        }

        $product->save();
        return 1;
    }
    public function updateApproved(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $product->is_approve = $request->status;
if($request->status==0){
    $product->published=0;
}
        if($product->added_by == 'seller' && \App\Addon::where('unique_identifier', 'seller_subscription')->first() != null && \App\Addon::where('unique_identifier', 'seller_subscription')->first()->activated){
            $seller = $product->user->seller;
            if($seller->invalid_at != null && Carbon::now()->diffInDays(Carbon::parse($seller->invalid_at), false) <= 0){
                return 0;
            }
        }

        $product->save();
        if($product->is_approve==0){
            return 1;
        }
        else{
            return 2;
        }
        
    }
    
    public function updateFeatured(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $product->featured = $request->status;
        if($product->save()){
            return 1;
        }
        return 0;
    }

    public function sku_combination(Request $request)
    { //echo 'hi';die;
        $options = array();
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $colors_active = 1;
            array_push($options, $request->colors);
        }
        else {
            $colors_active = 0;
        }

        $unit_price = $request->unit_price;
        $product_name = $request->name;

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;
                $my_str = implode('', $request[$name]);
                array_push($options, explode(',', $my_str));
            }
        }

        $combinations = combinations($options);
        return view('partials.sku_combinations', compact('combinations', 'unit_price', 'colors_active', 'product_name'));
    }

    

    public function sku_combination_edit(Request $request)
    {
        $product = Product::findOrFail($request->id);

        $options = array();
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $colors_active = 1;
            array_push($options, $request->colors);
        }
        else {
            $colors_active = 0;
        }

        $product_name = $request->name;
        $unit_price = $request->unit_price;
        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;
                $my_str = implode('', $request[$name]);
                array_push($options, explode(',', $my_str));
            }
        }
        

        $combinations = combinations($options);
        return view('partials.sku_combinations_edit', compact('combinations', 'unit_price', 'colors_active', 'product_name', 'product'));
    }
    public function sku_combination_view(Request $request)
    {
        $product = Product::findOrFail($request->product_id);

        $options = array();
        if(count(json_decode($product->colors))>0){
            $colors_active = 1;
            array_push($options, json_decode($product->colors));
        }
        else {
            $colors_active = 0;
        }

        $product_name = $product->name;
        $unit_price = $product->unit_price;

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;
                $my_str = implode('|', $request[$name]);
                array_push($options, explode(',', $my_str));
            }
        }

        $combinations = combinations($options);
        return view('partials.sku_combinations_edit', compact('combinations', 'unit_price', 'colors_active', 'product_name', 'product'));
    }

    public function color_wise_image(Request $request)
    { //echo 'hi';die;
        $options = array();
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $options=Color::whereIn('code', $request->colors)->get();
            $colors_active = 1;
        }else{
            $colors_active = 0;
        }
       return view('partials.color_wise_image', compact('options','colors_active'));
    }
    public function color_wise_image_edit(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $options = array();
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $options=Color::whereIn('code', $request->colors)->get();
            $colors_active = 1;
        }else{
            $colors_active = 0;
        }
        $active_add=0;
        return view('partials.color_wise_image_edit', compact('options','colors_active','product','active_add'));
    }
    public function color_wise_image_view(Request $request)
    {
        $product = Product::findOrFail($request->product_id);
        $options = array();
        if(count(json_decode($product->colors))>0){
            $options=Color::whereIn('code', json_decode($product->colors))->get();
            $colors_active = 1;

        }else{
            $colors_active = 0;
        }
        $active_add=1;
        return view('partials.color_wise_image_edit', compact('options','colors_active','product','active_add'));
    }

    public function productView($id){
        $product = Product::findOrFail(decrypt($id));
        $tags = json_decode($product->tags);
        $categories = Category::all();
        return view('products.view', compact('product', 'categories', 'tags'));
    }

    public function show_images(Request $request){
      $colors=$request->colors;
      $product_id=$request->product_id;
      $product_data=Product::findOrFail($product_id);
      if($colors=="no_color"){
          if(count(json_decode($product_data->colors))>0){
            $photos=json_decode(json_decode($product_data->photos)[0]->products);
          }else{
            $photos=json_decode($product_data->photos);
          }
      
      }else{
        $options=Color::where('code', $colors)->first();
        foreach(json_decode($product_data->photos) as $img){
            if($img->id==$options->id) {
                $photos=json_decode($img->products);  
            } 
        }
      }
      
      return view('partials.view_color_wise_image', compact('photos'));
    }

}
