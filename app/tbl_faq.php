<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_faq extends Model
{
    protected $table='tbl_faq';
    protected $fillable=['id','type','question','answer','created_at','updated_at'];
}
