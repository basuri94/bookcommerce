<?php

namespace App;

use App\Models\OrderDetail;
use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
  public function user(){
  	return $this->belongsTo(User::class);
  }

  public function payments(){
  	return $this->hasMany(Payment::class);
  }
  public function orderDetails(){
  	return $this->hasMany('App\OrderDetail','seller_id');
  }
}
