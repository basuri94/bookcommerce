<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Emailtemplate extends Model
{
    protected $table='email_template';
    protected $fillable=['id','slug','subject','mail_body','created_at','updated_at'];
}
