<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblAttributes extends Model
{
    protected $table="tbl_attributes";

    public function getSubCategory(){
        return $this->belongsTo('App\Models\SubCategory','sub_categoryid');
    }
}
