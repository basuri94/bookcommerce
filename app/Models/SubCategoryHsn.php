<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;


/**
 * App\Models\SubSubCategory
 *
 * @property int $id
 * @property int $sub_category_id
 * @property string $name
 * @property string $brands
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubCategoryHsn newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubCategoryHsn newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubCategoryHsn query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubCategoryHsn whereBrands($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubCategoryHsn whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubCategoryHsn whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubCategoryHsn whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubCategoryHsn whereSubCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubCategoryHsn whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SubCategoryHsn extends Model
{
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('alphabetical', function (Builder $builder) {
            $builder->orderBy('name', 'asc');
        });
    }

    public function subCategoryHSN()
    {
        return $this->belongsTo(SubCategory::class);
    }
}
