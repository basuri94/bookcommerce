<?php

namespace App\Exports;

use App\Models\Order;
use App\RefundRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
class RefundReport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    protected $request;
    public function __construct($from_date,$to_date)
    {
        $this->from_date = $from_date;
         $this->to_date = $to_date;
       
    }
    public function collection()
    {
        $data=array();
        $allrefund = RefundRequest::orderBy('refund_request.id', 'desc')
        ->leftjoin('order_details', 'order_details.id', '=', 'refund_request.order_id')
        ->leftjoin('orders', 'orders.id', '=', 'order_details.order_id')
        ->leftjoin('users as seller_user', 'seller_user.id', '=', 'order_details.seller_id')
        ->leftjoin('users as customer_user', 'customer_user.id', '=', 'orders.user_id')
            ->leftjoin('products', 'products.id', '=', 'refund_request.product_id')
            ->where('products.user_id','!=', Auth::user()->id)
            ->select('refund_request.*', 'products.name', 'orders.code','seller_user.name as seller_name','customer_user.name as customer_name');

            if (!empty($this->to_date)) {
                $to_date = date('Y-m-d', strtotime(trim(str_replace('/', '-', $this->to_date))));
                $allrefund = $allrefund->where(DB::raw("DATE(refund_request.created_at)"), '<=', DB::raw("DATE('" . $to_date . "')"));
            }
            if (!empty($this->from_date)) {
                $from_date = date('Y-m-d', strtotime(trim(str_replace('/', '-', $this->from_date))));
                $order_coupon = $allrefund->where(DB::raw("DATE(refund_request.created_at)"), '>=', DB::raw("DATE('" . $from_date . "')"));
            }
            $allrefund=  $allrefund->get();
            foreach ($allrefund as $key => $val) {
              $nested['created_date']=date('d/m/Y', strtotime(trim(str_replace('-', '/', $val->created_at))));
              $nested['invoice']= $val->code;
              $nested['seller_name']= $val->seller_name;
              $nested['customer_name']= $val->customer_name;
              
              $nested['amount']= $val->amount;
              $nested['product_name']= $val->name;
              $nested['status']= $val->status;
              $data[]=$nested;
            }
          return collect($data);
    }
    public function headings(): array
    {
        return [
            'Date',
            'Invoice No',
            'Vendor name',
            'Customer name',
            'Amount',
            'Product name',
            'Status',
            

        ];
    }
}
