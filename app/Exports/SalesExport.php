<?php

namespace App\Exports;

use App\CuriorRefund;
use App\Models\Order;
use App\Models\OrderDetail;
use App\RefundRequest;
use App\SubCategoryHsn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Wallet;

class SalesExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    use Exportable;
    protected $request;
    public function __construct($from_date,$to_date)
    {
        $this->from_date = $from_date;
         $this->to_date = $to_date;
       
    }
    public function collection()
    {
       
        $orderDetails = OrderDetail::with('order', 'product')->where('payment_status', 'paid');



        if (!empty($this->to_date)) {
            $to_date = date('Y-m-d', strtotime(trim(str_replace('/', '-', $this->to_date))));
            $orderDetails = $orderDetails->where(DB::raw("DATE(order_details.created_at)"), '<=', DB::raw("DATE('" . $to_date . "')"));
        }
        if (!empty($this->from_date)) {
            $from_date = date('Y-m-d', strtotime(trim(str_replace('/', '-', $this->from_date))));
            $orderDetails = $orderDetails->where(DB::raw("DATE(order_details.created_at)"), '>=', DB::raw("DATE('" . $from_date . "')"));
        }
$orderDetails = $orderDetails->orderBy('order_details.id','desc')->get();
        $data = array();
        
        foreach ($orderDetails as $key => $val) {
            $totalFee=0;
            $gatewayCharge=0;
            $walletAmount=0;
            $finalWalletAmount=0;
            $tcs=0;
            $tds=0;
            if(isset($val->product->user)){
            $courierChargeValue = 0;

            $totalCourierRefundOrReplaceAmount = 0;

            $nested['created_date'] = date('d/m/Y', $val->order->date);
            $nested['invoice'] = $val->order->code;
            $nested['vendor_name'] = $val->product->user->name;
            $nested['gstno'] = $val->product->user->seller->gst;
            $total_price_includegst = $val->price + $val->tax;
            $courierCharge = DB::table('curior_orders')->where('curior_orders.id', $val->curior_order_id)->first();

            $nested['sale_without_tax'] = $val->price +  $val->shipping_cost;

            $nested['sale_with_tax'] = $total_price_includegst +  $val->shipping_cost;

            $hsnid = $val->product->hsn_id;

            $nested['gst'] =  $val->tax;

            $commission_percentage = $val->product->subcategory->commision_rate;
            $nested['commission'] = number_format((float)$commission_percentage * $val->price / 100, 2, '.', '');
            $seller = $val->product->user->seller;

            $OrderCharge = Order::where('id', $val->order_id)->first();
            if(  $OrderCharge->payment_details!=null){
            if(!empty($OrderCharge->payment_details)){
                $gatewayCharge = ((json_decode($OrderCharge->payment_details)->fee) / 100) / $OrderCharge->grand_total *    ($val->tax + $val->price + $val->shipping_cost);
            }
            }
           
            $actual_gatewaycharge = number_format((float)$gatewayCharge, 2, '.', '');

            $refunddata = RefundRequest::where('status', 'Approved')->where('order_id', $val->id)->first();
            if (!empty($refunddata)) {
                if ($refunddata->type == "Refund") {
                    $refundAmount = $refunddata->amount;
                    $courierRefundAmount = CuriorRefund::where('refund_request_id', $refunddata->id)->where('status', 1)->value('curior_charge');
                    $totalCourierRefundOrReplaceAmount = $refundAmount + $courierRefundAmount;
                } else if ($refunddata->type == "Replacement") {
                    $totalReplaceAmount = CuriorRefund::where('refund_request_id', $refunddata->id)->where('status', 1)->sum('curior_charge');
                    $totalCourierRefundOrReplaceAmount = $totalReplaceAmount;
                }
            }


            $gst_in_commisssion = ((($val->price * $commission_percentage) / 100) * 18 / 100) + (($val->price * $commission_percentage) / 100);

            if (!empty($courierCharge->curior_charge)) {

                $courierChargeValue = ($courierCharge->curior_charge) / $OrderCharge->grand_total *    ($val->tax + $val->price + $val->shipping_cost);
                $courierChargeValue = number_format((float)$courierChargeValue, 2, '.', '');
            }
            if($OrderCharge->wallet_amount!="0.00"){
             
                $walletData=Wallet::where('payment_method','razorpay')->where('user_id',$OrderCharge->user_id)->get();
                foreach( $walletData as $walletVal){
               
                    if (!empty(json_decode($walletVal->payment_details)->fee)) {
                        $totalFee +=(json_decode($walletVal->payment_details)->fee);
                      }
                     $walletAmount +=$walletVal->amount;
                   
                    
                }
                if($totalFee!=0){
                    $convertTotalFee= $totalFee/100;
                    $WalletAmountCalculate=($convertTotalFee / $walletAmount) *$OrderCharge->wallet_amount;
                    $finalWalletAmount =  $WalletAmountCalculate / $OrderCharge->grand_total *     ($val->tax + $val->price + $val->shipping_cost);;
                  
                }
                
            }
          
            $tcs= ($val->price)*0.01;
            $tds= ($val->price)*0.01;
            $vendor_charges = $courierChargeValue + $actual_gatewaycharge + $gst_in_commisssion+$finalWalletAmount;

            $admin_to_pay = $total_price_includegst - $vendor_charges - $totalCourierRefundOrReplaceAmount-$tcs-$tds;
          //  $nested['brake_amount']='Total Amount:- '.$total_price_includegst.'Vendor Charges '.$vendor_charges.', Refund/Replace Amount:- '.$totalCourierRefundOrReplaceAmount.', Wallet Charges:- ,'. number_format((float)$finalWalletAmount, 2, '.', '');
           // $nested['vendor_charges']='Courier Charge:- '.$courierChargeValue.'Gateway Charge:- '.$actual_gatewaycharge.'Commission:- '.$gst_in_commisssion;
            $nested['amount_pay'] = number_format((float)$admin_to_pay, 2, '.', '');
            $data[] = $nested;
            }
        }
        return collect($data);
       
        // return ;
    }
    public function headings(): array
    {
        return [
            'Created Date',
            'invoice',
            'Vendor Name',
            'Gst No',
            'Sale Without Tax',
            'Sale With Tax',
            'Gst',
            'Commission',
            'Amout Pay',

        ];
    }
}
