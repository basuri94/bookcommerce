<?php

namespace App\Exports;

use App\Models\Order;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
class CouponReport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    protected $request;
    public function __construct($from_date,$to_date)
    {
        $this->from_date = $from_date;
         $this->to_date = $to_date;
       
    }
    public function collection()
    {
        $data=array();
        $order_coupon=Order::where('coupon_discount','!=', '0.00');
        if (!empty($this->to_date)) {
            $to_date = date('Y-m-d', strtotime(trim(str_replace('/', '-', $this->to_date))));
            $order_coupon = $order_coupon->where(DB::raw("DATE(orders.created_at)"), '<=', DB::raw("DATE('" . $to_date . "')"));
        }
        if (!empty($this->from_date)) {
            $from_date = date('Y-m-d', strtotime(trim(str_replace('/', '-', $this->from_date))));
            $order_coupon = $order_coupon->where(DB::raw("DATE(orders.created_at)"), '>=', DB::raw("DATE('" . $from_date . "')"));
        }
        $order_coupon=  $order_coupon->get();
        foreach ($order_coupon as $key => $val) {
            $nested['created_date']=date('d/m/Y', strtotime(trim(str_replace('-', '/', $val->created_at))));
            $nested['user_name']= $val->user->name;
            $nested['invoice']= $val->code;
            $nested['coupon_discount']= $val->coupon_discount;
            $data[]=$nested;
          }
          return collect($data);
    }
    public function headings(): array
    {
        return [
            'Date',
            'User name',
            'Invoice No',
           'Coupon Discount',
            

        ];
    }
}
