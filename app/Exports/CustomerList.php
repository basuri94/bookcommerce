<?php

namespace App\Exports;
use App\Models\Order;
use App\Customer;
use App\Wallet;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;

class CustomerList implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    protected $request;
    public function __construct($from_date,$to_date)
    {
        $this->from_date = $from_date;
         $this->to_date = $to_date;
       
    }
    public function collection()
    {
        $data=array();
        $customers = Customer::orderBy('created_at', 'desc')->get();
        
            foreach ($customers as $val) {
                if($val->user != null){
                    $nested['name']=$val->user->name;
                    $nested['email']= $val->user->email;
                     $nested['phone_no']=$val->user->phone_no;
                    $nested['balance']= $val->user->balance;
                    $data[]=$nested;
                }
              
            }
          return collect($data);
    }
    public function headings(): array
    {
        return [
            'Name',
            'Email Address',
            'Phone',
           'Wallet Balance'
            

        ];
    }
}
