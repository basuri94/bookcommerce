<?php

namespace App\Exports;
use App\Models\Order;
use App\Seller;
use App\Wallet;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;

class SellerList implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    protected $request;
    public function __construct($from_date,$to_date)
    {
        $this->from_date = $from_date;
         $this->to_date = $to_date;
       
    }
    public function collection()
    {
        $data=array();
        $sellers = Seller::all();
            
            
            foreach ($sellers as $key => $val) {
                $nested['name']=$val->user->name;
                $nested['email']= $val->user->email;
                 $nested['phone_no']=$val->user->phone_no;
                 $nested['product_count']=\App\Product::where('user_id', $val->user->id)->count();
                $nested['admin_to_pay']= $val->user->admin_to_pay;
              $data[]=$nested;
            }
          return collect($data);
    }
    public function headings(): array
    {
        return [
            'Name',
            'Email Address',
            'Phone',
           'No of product',
           'Due Amount'
        ];
    }
}
