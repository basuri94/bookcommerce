<?php

namespace App\Exports;
use App\Models\Order;
use App\User;
use App\Wallet;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;

class ReferralReport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    protected $request;
    public function __construct($from_date,$to_date)
    {
        $this->from_date = $from_date;
         $this->to_date = $to_date;
       
    }
    public function collection()
    {
        $data=array();
            $refer_wallet=Wallet::where('payment_method','=', 'Refer');
            
            if (!empty($this->to_date)) {
                $to_date = date('Y-m-d', strtotime(trim(str_replace('/', '-', $this->to_date))));
                $refer_wallet = $refer_wallet->where(DB::raw("DATE(wallets.created_at)"), '<=', DB::raw("DATE('" . $to_date . "')"));
            }
            if (!empty($this->from_date)) {
                $from_date = date('Y-m-d', strtotime(trim(str_replace('/', '-', $this->from_date))));
                $refer_wallet = $refer_wallet->where(DB::raw("DATE(wallets.created_at)"), '>=', DB::raw("DATE('" . $from_date . "')"));
            }
            $refer_wallet=  $refer_wallet->get();
            foreach ($refer_wallet as $key => $val) {
              $nested['created_date']=date('d/m/Y', strtotime(trim(str_replace('-', '/', $val->created_at))));
              $nested['customer_name']= $val->user->name;
               $nested['referred_by']= User::where('id',$val->user->referred_by)->value('name');
              $nested['refer_code']= User::where('id',$val->user->referred_by)->value('referral_code');
              $nested['amount']= $val->amount;
              $data[]=$nested;
            }
          return collect($data);
    }
    public function headings(): array
    {
        return [
            'Date',
            'Customer name',
            'Referred By',
          
           'Referral Code',
           'Amount'
            

        ];
    }
}
