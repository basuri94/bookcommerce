<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CuriorOrder extends Model
{
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
    public function curiorOrderOrderDetails()
    {
        return $this->hasMany(OrderDetail::class);
    }
   
}
