<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Refers extends Model
{
    protected $table='refers';
    protected $primaryKey = 'id';
}
