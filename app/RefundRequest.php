<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefundRequest extends Model
{
   protected $table='refund_request';
   protected $fillable=['order_id','product_id','reason','amount','userid','status'];

}
