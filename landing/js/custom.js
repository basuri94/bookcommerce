(function($){
    
    "use strict";

    $(window).load(function() {

        /* ==============================================
        Preloader
        =============================================== */

        var preloader = $("body").hasClass('no-preloader') ? false : true;

        var minimumTime = 500,
            preloaderDelay = 500,
            preloaderFadeOutTime = 800;

        function hidePreloader() {
            var preloader = $('.preloader'),
                loadingAnimation = $(preloader).find('.preloader-logo');
               
            setTimeout(function() {
                loadingAnimation.delay().fadeOut();
                preloader.delay(preloaderDelay).fadeOut(preloaderFadeOutTime);
            }, minimumTime);
        }

        if(preloader) {
            hidePreloader();
        }

        /* ==============================================
        Contact Form
        =============================================== */

        
        /* ==============================================
        Click Scroll
        =============================================== */

        $('a.cta').bind('click.smoothscroll',function (event) {
            event.preventDefault();

            var target = this.hash;

            $('html, body').stop().animate({
                'scrollTop': $(target).offset().top
            }, 500, 'easeInOutQuad');

        });


    });

    $(document).ready(function(){

        /* ==============================================
        Parallax
        =============================================== */

        $('.intro').parallax("50%", 0.2);

    });

})(jQuery);