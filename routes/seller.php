<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//demo

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

// Route::group(['prefix' =>'seller', 'middleware' => ['seller', 'verified']], function(){
    Route::group(['prefix' =>'seller', 'middleware' => ['seller', 'verified']], function(){
	Route::get('/products', 'HomeController@seller_product_list')->name('seller.products');
	Route::get('/product/upload', 'HomeController@show_product_upload_form')->name('seller.products.upload');
	Route::get('/product/{id}/edit', 'HomeController@show_product_edit_form')->name('seller.products.edit');
	Route::resource('payments','PaymentController');

	
	
	Route::get('/shop/apply_for_verification', 'ShopController@verify_form')->name('shop.verify');
	Route::post('/shop/apply_for_verification', 'ShopController@verify_form_store')->name('shop.verify.store');

	Route::get('/reviews', 'ReviewController@seller_reviews')->name('reviews.seller');

	//digital Product
	Route::get('/digitalproducts', 'HomeController@seller_digital_product_list')->name('seller.digitalproducts');
	Route::get('/digitalproducts/upload', 'HomeController@show_digital_product_upload_form')->name('seller.digitalproducts.upload');
	Route::get('/digitalproducts/{id}/edit', 'HomeController@show_digital_product_edit_form')->name('seller.digitalproducts.edit');
	Route::get('/profile', 'HomeController@profile')->name('profile');
	Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
	Route::get('/', 'HomeController@index')->name('home');
	Route::post('/subcategories.get_attributes', 'SubCategoryController@get_attributes_bysubcategory')->name('subcategories.get_attributes');
	
	
	
	Route::post('/request_destroy', 'HomeController@request_destroy')->name('request_destroy');
});
	
Route::resource('shops', 'ShopController');
Route::get('/', 'HomeController@index')->name('home');
Route::post('/subcategories.get_attributes', 'SubCategoryController@get_attributes_bysubcategory')->name('subcategories.get_attributes');
Route::post('/request_destroy', 'HomeController@request_destroy')->name('request_destroy');

?>