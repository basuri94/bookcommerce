<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/clear-cache', function() {
    $exitCode = \Artisan::call('cache:clear');
    return "Cache is cleared";
});
Route::get('/dashboard_seller', 'HomeController@dashboard')->name('dashboard_seller');
Route::get('/admin', 'HomeController@admin_dashboard')->name('admin.dashboard')->middleware(['auth', 'admin']);
Route::group(['prefix' =>'admin', 'middleware' => ['auth', 'admin']], function(){
	Route::post('/refer', 'BusinessSettingsController@updateStatus')->name('countries.status');

	Route::resource('categories','CategoryController');
	Route::get('/categories/destroy/{id}', 'CategoryController@destroy')->name('categories.destroy');
	Route::post('/categories/featured', 'CategoryController@updateFeatured')->name('categories.featured');

	Route::resource('subcategories','SubCategoryController');
	Route::get('/subcategories/destroy/{id}', 'SubCategoryController@destroy')->name('subcategories.destroy');

	Route::resource('subsubcategories','SubSubCategoryController');
	Route::get('/subsubcategories/destroy/{id}', 'SubSubCategoryController@destroy')->name('subsubcategories.destroy');
	

	Route::resource('subcategoryhsns','SubCategoryHsnController');
	Route::get('/subcategoryhsns/destroy/{id}', 'SubCategoryHsnController@destroy')->name('subcategoryhsns.destroy');

	Route::post('/subcategories.get_subcategories_by_SubCategoryHsn', 'SubCategoryController@get_subcategories_by_SubCategoryHsn')->name('subcategories.get_subcategories_by_SubCategoryHsn');
	Route::post('/subcategories.get_gst_by_hsn_id', 'SubCategoryController@get_gst_by_hsn_id')->name('subcategories.get_gst_by_hsn_id');
	Route::resource('brands','BrandController');
	Route::get('/brands/destroy/{id}', 'BrandController@destroy')->name('brands.destroy');

	Route::get('/products/admin','ProductController@admin_products')->name('products.admin');
	Route::get('/products/seller','ProductController@seller_products')->name('products.seller');
	Route::get('/products/view/{id}','ProductController@productView')->name('products.view');
	Route::post('/products/color_wise_image_view', 'ProductController@color_wise_image_view')->name('color_wise_image_view');
	Route::post('/products/sku_combination_view', 'ProductController@sku_combination_view')->name('products.sku_combination_view');

	Route::get('/products/seller_pending','ProductController@seller_products_pending')->name('products.seller_pending');
	Route::get('/products/create','ProductController@create')->name('products.create');
	Route::get('/products/admin/{id}/edit','ProductController@admin_product_edit')->name('products.admin.edit');
	Route::get('/products/seller/{id}/edit','ProductController@seller_product_edit')->name('products.seller.edit');
	Route::post('/products/todays_deal', 'ProductController@updateTodaysDeal')->name('products.todays_deal');
	Route::post('/products/get_products_by_subsubcategory', 'ProductController@get_products_by_subsubcategory')->name('products.get_products_by_subsubcategory');
	Route::post('/products/get_products_by_subcategory', 'ProductController@get_products_by_subcategory')->name('products.get_products_by_subcategory');
	Route::get('/products/view/{id}','ProductController@productView')->name('products.view');
	Route::post('/products/approve_button', 'ProductController@product_approve')->name('products.approve_button');
	Route::post('/products/reject_button', 'ProductController@product_reject')->name('products.reject_button');

	Route::get('/products/seller_strike','ProductController@seller_products_strike')->name('products.seller_strike');



	Route::resource('sellers','SellerController');
	Route::get('/sellers/destroy/{id}', 'SellerController@destroy')->name('sellers.destroy');
	Route::get('/sellers/login_test/{id}', 'SellerController@login_test')->name('sellers.login_test');
	
	Route::get('/sellers/view/{id}/verification', 'SellerController@show_verification_request')->name('sellers.show_verification_request');
	Route::get('/sellers/approve/{id}', 'SellerController@approve_seller')->name('sellers.approve');
	Route::get('/sellers/reject/{id}', 'SellerController@reject_seller')->name('sellers.reject');
	Route::post('/sellers/payment_modal', 'SellerController@payment_modal')->name('sellers.payment_modal');
	Route::get('/seller/payments', 'PaymentController@payment_histories')->name('sellers.payment_histories');
	Route::get('/seller/payments/show/{id}', 'PaymentController@show')->name('sellers.payment_history');

	Route::resource('customers','CustomerController');
	Route::get('/customers/destroy/{id}', 'CustomerController@destroy')->name('customers.destroy');

	Route::get('/newsletter', 'NewsletterController@index')->name('newsletters.index');
	Route::post('/newsletter/send', 'NewsletterController@send')->name('newsletters.send');

	Route::resource('profile','ProfileController');

	Route::get('/refer', 'BusinessSettingsController@getreferindex')->name('getreferindex');

	Route::post('/refer-store/{id}', 'BusinessSettingsController@referstore')->name('referstore');
Route::get('/getsettelamount', 'BusinessSettingsController@getsettelAmount')->name('getsettelAmount');
	Route::post('/settelment-store/{id}', 'BusinessSettingsController@settelmentstore')->name('settelmentstore');

	Route::post('/business-settings/update', 'BusinessSettingsController@update')->name('business_settings.update');
	Route::post('/business-settings/update/activation', 'BusinessSettingsController@updateActivationSettings')->name('business_settings.update.activation');
	Route::get('/activation', 'BusinessSettingsController@activation')->name('activation.index');
	Route::get('/payment-method', 'BusinessSettingsController@payment_method')->name('payment_method.index');
	Route::get('/social-login', 'BusinessSettingsController@social_login')->name('social_login.index');
	Route::get('/smtp-settings', 'BusinessSettingsController@smtp_settings')->name('smtp_settings.index');
	Route::get('/google-analytics', 'BusinessSettingsController@google_analytics')->name('google_analytics.index');
	Route::get('/facebook-chat', 'BusinessSettingsController@facebook_chat')->name('facebook_chat.index');
	Route::post('/env_key_update', 'BusinessSettingsController@env_key_update')->name('env_key_update.update');
	Route::post('/payment_method_update', 'BusinessSettingsController@payment_method_update')->name('payment_method.update');

	Route::get('/SMS_Settings', 'BusinessSettingsController@SMS_Settings')->name('SMS_Settings.index');
	Route::post('/SMS_Settings_update', 'BusinessSettingsController@SMS_Settings_update')->name('SMS_Settings.update');

	Route::post('/google_analytics', 'BusinessSettingsController@google_analytics_update')->name('google_analytics.update');
	Route::post('/facebook_chat', 'BusinessSettingsController@facebook_chat_update')->name('facebook_chat.update');
	Route::post('/facebook_pixel', 'BusinessSettingsController@facebook_pixel_update')->name('facebook_pixel.update');
	Route::get('/currency', 'CurrencyController@currency')->name('currency.index');
    Route::post('/currency/update', 'CurrencyController@updateCurrency')->name('currency.update');
    Route::post('/your-currency/update', 'CurrencyController@updateYourCurrency')->name('your_currency.update');
	Route::get('/currency/create', 'CurrencyController@create')->name('currency.create');
	Route::post('/currency/store', 'CurrencyController@store')->name('currency.store');
	Route::post('/currency/currency_edit', 'CurrencyController@edit')->name('currency.edit');
	Route::post('/currency/update_status', 'CurrencyController@update_status')->name('currency.update_status');
	Route::get('/verification/form', 'BusinessSettingsController@seller_verification_form')->name('seller_verification_form.index');
	Route::post('/verification/form', 'BusinessSettingsController@seller_verification_form_update')->name('seller_verification_form.update');
	Route::get('/vendor_commission', 'BusinessSettingsController@vendor_commission')->name('business_settings.vendor_commission');
	Route::post('/vendor_commission_update', 'BusinessSettingsController@vendor_commission_update')->name('business_settings.vendor_commission.update');

	Route::resource('/languages', 'LanguageController');
	Route::post('/languages/update_rtl_status', 'LanguageController@update_rtl_status')->name('languages.update_rtl_status');
	Route::get('/languages/destroy/{id}', 'LanguageController@destroy')->name('languages.destroy');
	Route::get('/languages/{id}/edit', 'LanguageController@edit')->name('languages.edit');
	Route::post('/languages/{id}/update', 'LanguageController@update')->name('languages.update');
	Route::post('/languages/key_value_store', 'LanguageController@key_value_store')->name('languages.key_value_store');

	Route::get('/frontend_settings/home', 'HomeController@home_settings')->name('home_settings.index');
	Route::post('/frontend_settings/home/top_10', 'HomeController@top_10_settings')->name('top_10_settings.store');
	Route::get('/sellerpolicy/{type}', 'PolicyController@index')->name('sellerpolicy.index');
	Route::get('/returnpolicy/{type}', 'PolicyController@index')->name('returnpolicy.index');
	Route::get('/supportpolicy/{type}', 'PolicyController@index')->name('supportpolicy.index');
	Route::get('/terms/{type}', 'PolicyController@index')->name('terms.index');
	Route::get('/privacypolicy/{type}', 'PolicyController@index')->name('privacypolicy.index');

	//Policy Controller
	Route::post('/policies/store', 'PolicyController@store')->name('policies.store');
	Route::get('/sellers/addressid/{id}', 'SellerController@updateaddressid')->name('sellers.addressid');
	Route::group(['prefix' => 'frontend_settings'], function(){
		Route::resource('sliders','SliderController');
	    Route::get('/sliders/destroy/{id}', 'SliderController@destroy')->name('sliders.destroy');

		Route::resource('home_banners','BannerController');
		Route::get('/home_banners/create/{position}', 'BannerController@create')->name('home_banners.create');
		Route::post('/home_banners/update_status', 'BannerController@update_status')->name('home_banners.update_status');
	    Route::get('/home_banners/destroy/{id}', 'BannerController@destroy')->name('home_banners.destroy');

		Route::resource('home_categories','HomeCategoryController');
	    Route::get('/home_categories/destroy/{id}', 'HomeCategoryController@destroy')->name('home_categories.destroy');
		Route::post('/home_categories/update_status', 'HomeCategoryController@update_status')->name('home_categories.update_status');
		Route::post('/home_categories/get_subsubcategories_by_category', 'HomeCategoryController@getSubSubCategories')->name('home_categories.get_subsubcategories_by_category');
	});

	Route::resource('roles','RoleController');
    Route::get('/roles/destroy/{id}', 'RoleController@destroy')->name('roles.destroy');

    Route::resource('staffs','StaffController');
    Route::get('/staffs/destroy/{id}', 'StaffController@destroy')->name('staffs.destroy');

	Route::resource('flash_deals','FlashDealController');
    Route::get('/flash_deals/destroy/{id}', 'FlashDealController@destroy')->name('flash_deals.destroy');
	Route::post('/flash_deals/update_status', 'FlashDealController@update_status')->name('flash_deals.update_status');
	Route::post('/flash_deals/update_featured', 'FlashDealController@update_featured')->name('flash_deals.update_featured');
	Route::post('/flash_deals/product_discount', 'FlashDealController@product_discount')->name('flash_deals.product_discount');
	Route::post('/flash_deals/product_discount_edit', 'FlashDealController@product_discount_edit')->name('flash_deals.product_discount_edit');


	Route::resource('bank_details','BankDetailsController');


	Route::get('/orders', 'OrderController@admin_orders')->name('orders.index.admin');
	Route::get('/orders/{id}/show', 'OrderController@show')->name('orders.show');
	Route::get('/sales/{id}/show', 'OrderController@sales_show')->name('sales.show');
	Route::get('/orders/destroy/{id}', 'OrderController@destroy')->name('orders.destroy');
	Route::get('/sales', 'OrderController@sales')->name('sales.index');

	Route::resource('links','LinkController');
	Route::get('/links/destroy/{id}', 'LinkController@destroy')->name('links.destroy');

	Route::resource('generalsettings','GeneralSettingController');
	Route::get('/logo','GeneralSettingController@logo')->name('generalsettings.logo');
	Route::post('/logo','GeneralSettingController@storeLogo')->name('generalsettings.logo.store');
	Route::get('/color','GeneralSettingController@color')->name('generalsettings.color');
	Route::post('/color','GeneralSettingController@storeColor')->name('generalsettings.color.store');

	Route::resource('seosetting','SEOController');

	Route::post('/pay_to_seller', 'CommissionController@pay_to_seller')->name('commissions.pay_to_seller');

	//Reports
	Route::get('/stock_report', 'ReportController@stock_report')->name('stock_report.index');
	Route::get('/in_house_sale_report', 'ReportController@in_house_sale_report')->name('in_house_sale_report.index');
	Route::get('/seller_report', 'ReportController@seller_report')->name('seller_report.index');
	Route::get('/seller_sale_report', 'ReportController@seller_sale_report')->name('seller_sale_report.index');
	Route::get('/wish_report', 'ReportController@wish_report')->name('wish_report.index');
	Route::get('/sales_budget', 'ReportController@sales_budget')->name('sales.budget');
	Route::post('/sales_budget_export', 'ReportController@sales_budget_export')->name('sales.export');
	Route::get('/sales_vendor', 'ReportController@sales_vendor')->name('sales.vendor');
	Route::post('/sales_vendor_export', 'ReportController@sales_vandor_export')->name('sales.vendor.export');

	Route::get('/sales_vendor/{id}', 'ReportController@sales_vendor_record')->name('sales.vendor.record');
	Route::get('/sales_coupon', 'ReportController@sales_coupon')->name('sales.coupon');
	Route::post('/sales_coupon_export', 'ReportController@sales_coupon_export')->name('sales.coupon.export');

	Route::get('/saler_refund', 'ReportController@saler_refund')->name('saler_refund');
	Route::post('/sellerrefund_export', 'ReportController@sellerrefund_export')->name('sales.sellerrefund.export');
	
	Route::get('/referral_report', 'ReportController@referral_report')->name('referral_report');
	Route::post('/referral_report_export', 'ReportController@referral_report_export')->name('referral_report.export');
	Route::post('/customer_list_export', 'ReportController@customer_list_export')->name('customer_list.export');
	Route::post('/seller_list_export', 'ReportController@seller_list_export')->name('seller_list.export');
	
	
	//Coupons
	Route::resource('coupon','CouponController');
	Route::post('/coupon/get_form', 'CouponController@get_coupon_form')->name('coupon.get_coupon_form');
	Route::post('/coupon/get_form_edit', 'CouponController@get_coupon_form_edit')->name('coupon.get_coupon_form_edit');
	Route::get('/coupon/destroy/{id}', 'CouponController@destroy')->name('coupon.destroy');

	//Reviews
	Route::get('/reviews', 'ReviewController@index')->name('reviews.index');
	Route::post('/reviews/published', 'ReviewController@updatePublished')->name('reviews.published');

	//Support_Ticket
	Route::get('support_ticket/','SupportTicketController@admin_customer_index')->name('support_ticket.admin_index');
	Route::get('admin_index_seller/','SupportTicketController@admin_seller_index')->name('support_ticket.admin_index_seller');
	Route::get('support_ticket/{id}/show','SupportTicketController@admin_show')->name('support_ticket.admin_show');
	Route::post('support_ticket/reply','SupportTicketController@admin_store')->name('support_ticket.admin_store');

	//Pickup_Points
	Route::resource('pick_up_points','PickupPointController');
	Route::get('/pick_up_points/destroy/{id}', 'PickupPointController@destroy')->name('pick_up_points.destroy');


	Route::get('orders_by_pickup_point','OrderController@order_index')->name('pick_up_point.order_index');
	Route::get('/orders_by_pickup_point/{id}/show', 'OrderController@pickup_point_order_sales_show')->name('pick_up_point.order_show');

	Route::get('invoice/admin/{order_id}', 'InvoiceController@admin_invoice_download')->name('admin.invoice.download');

	//conversation of seller customer
	Route::get('conversations','ConversationController@admin_index')->name('conversations.admin_index');
	Route::get('conversations/{id}/show','ConversationController@admin_show')->name('conversations.admin_show');
	Route::get('/conversations/destroy/{id}', 'ConversationController@destroy')->name('conversations.destroy');
	Route::post('/strike_product', 'ConversationController@strike_product')->name('strike_product');

    Route::post('/sellers/profile_modal', 'SellerController@profile_modal')->name('sellers.profile_modal');
	Route::post('/sellers/approved', 'SellerController@updateApproved')->name('sellers.approved');
	
	

	Route::resource('attributes','AttributeController');
	Route::get('/attributes/destroy/{id}', 'AttributeController@destroy')->name('attributes.destroy');

	Route::resource('Refers','RefersController');
	Route::get('/Refers/destroy/{id}', 'RefersController@destroy')->name('Refers.destroy');

	Route::resource('Band_Trust','BandTrustController');
	Route::get('/Band_Trust/destroy/{id}', 'BandTrustController@destroy')->name('Band_Trust.destroy');

	Route::resource('addons','AddonController');
	Route::post('/addons/activation', 'AddonController@activation')->name('addons.activation');

	Route::get('/customer-bulk-upload/index', 'CustomerBulkUploadController@index')->name('customer_bulk_upload.index');
	Route::post('/bulk-user-upload', 'CustomerBulkUploadController@user_bulk_upload')->name('bulk_user_upload');
	Route::post('/bulk-customer-upload', 'CustomerBulkUploadController@customer_bulk_file')->name('bulk_customer_upload');
	Route::get('/user', 'CustomerBulkUploadController@pdf_download_user')->name('pdf.download_user');
	//Customer Package
	Route::resource('customer_packages','CustomerPackageController');
	Route::get('/customer_packages/destroy/{id}', 'CustomerPackageController@destroy')->name('customer_packages.destroy');
	//Classified Products
	Route::get('/classified_products', 'CustomerProductController@customer_product_index')->name('classified_products');
	Route::post('/classified_products/published', 'CustomerProductController@updatePublished')->name('classified_products.published');

	//Shipping Configuration
	Route::get('/shipping_configuration', 'BusinessSettingsController@shipping_configuration')->name('shipping_configuration.index');
	Route::post('/shipping_configuration/update', 'BusinessSettingsController@shipping_configuration_update')->name('shipping_configuration.update');

	Route::resource('pages', 'PageController');
	Route::get('/pages/destroy/{id}', 'PageController@destroy')->name('pages.destroy');

	Route::resource('countries','CountryController');
	Route::post('/countries/status', 'CountryController@updateStatus')->name('countries.status');


	/*******************Refund************** */
	Route::get('/admin-refund-list', 'RefundRequestController@adminfundlist')->name('adminfundlist');
	Route::post('/admin-refund-view', 'RefundRequestController@adminrefundview')->name('adminrefundview');
	Route::post('/admin-refund-approve', 'RefundRequestController@adminfundapprove')->name('adminfundapprove');

	 /********************** Email Template ******************/
	Route::resource('email_template','EmailTemplateController');
	//Route::get('/email_template/{id}/edit', 'EmailTemplateController@edit')->name('email_template.edit');
	Route::get('/email_template/edit/{id}', 'EmailTemplateController@edit')->name('email_template.edit');
	Route::get('/email_template/destroy/{id}', 'EmailTemplateController@destroy')->name('email_template.destroy');

	/*********************** Promotonal Banner ******************************/
	Route::resource('promotional_banner','PromotionalBannerController');
	Route::get('/promotional_banner/edit/{id}', 'PromotionalBannerController@edit')->name('promotional_banner.edit');
	Route::get('/promotional_banner/destroy/{id}', 'PromotionalBannerController@destroy')->name('promotional_banner.destroy');

	/*******************Refund************** */
	//Route::resource('faq.index','FAQController');
	Route::get('/faq.index', 'FAQController@index')->name('faq.index');
	Route::post('/faq.store', 'FAQController@store')->name('faq.store');
	Route::post('/faq.view_faq', 'FAQController@view_faq')->name('faq.view_faq');
	Route::post('/faq.update', 'FAQController@update')->name('faq.update');
	Route::post('/faq.delete', 'FAQController@delete')->name('faq.delete');
	//Route::post('/admin-refund-view', 'RefundRequestController@adminrefundview')->name('adminrefundview');
	//Route::post('/admin-refund-approve', 'RefundRequestController@adminfundapprove')->name('adminfundapprove');
});
Route::get('/forget-password', 'ForgetPasswordController@getforgetPassword')->name('getforgetPassword');
Route::post('/forget-password', 'ForgetPasswordController@postforgetPassword')->name('postforgetPassword');
Route::get('/otp-verify/{phone}', 'ForgetPasswordController@getOtpPage')->name('getOtpPage');
Route::post('/otp-verify', 'ForgetPasswordController@postOtpVerify')->name('postOtpVerify');

Route::get('/admin-forget-password', 'ForgetPasswordController@admingetforgetPassword')->name('admingetforgetPassword');
Route::post('/admin-forget-password', 'ForgetPasswordController@adminpostforgetPassword')->name('adminpostforgetPassword');
Route::get('/admin-otp-verify/{phone}', 'ForgetPasswordController@admingetOtpPage')->name('admingetOtpPage');
Route::post('/admin-otp-verify', 'ForgetPasswordController@adminpostOtpVerify')->name('adminpostOtpVerify');


Route::post('/attributes-mandatory/mandatory', 'AttributeController@mandatoryupdate')->name('attributes.mandatory');

Route::get('/request_category', 'HomeController@request_category')->name('request_category');
Route::post('/new_request', 'HomeController@new_request')->name('new_request');
Route::get('/request_destroy', 'HomeController@request_destroy')->name('request_destroy');



Route::get('/seller/seller_request', 'HomeController@seller_request')->name('sellers.seller_request');
Route::post('/seller/request_approval', 'HomeController@request_approval')->name('request.approval');

Route::get('/seller/price_calculator_view', 'HomeController@price_calculator_view')->name('price_calculator_view');
Route::get('/seller_onboard', 'HomeController@seller_onboard')->name('seller_onboard');
Route::post('/seller_onboard_save', 'HomeController@seller_onboard_save')->name('seller_onboard_save');
Route::post('/subcategories.get_attributes', 'SubCategoryController@get_attributes_bysubcategory')->name('subcategories.get_attributes');


Route::get('/seller_report_invoice', 'InvoiceController@seller_report_invoice')->name('seller_report_invoice');
Route::post('/seller_report_invoice_download', 'InvoiceController@seller_report_invoice_download')->name('seller_report_invoice_download');
