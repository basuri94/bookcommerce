<!doctype html>
<html lang="en">
  <head>

   

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="index, follow">
<title>TheLocal2Vocal</title>
<meta name="description" content="E-commerce CMS Multi vendor system is such a platform to build a border less marketplace both for physical and digital goods." />
<meta name="keywords" content="bootstrap,responsive,template,developer">
<meta name="author" content="Local2Vocal">
<meta name="sitemap_link" content="Local2Vocal">


<!-- Favicon -->
<link type="image/x-icon" href="https://www.staging.thelocal2vocal.com/uploads/favicon/bWxAJ2vFPU9ZaapFGmNjGjNsdbkIbL1xdYaDlO7W.png" rel="shortcut icon" />

	<?php include"includes/login-head-tag.php";?>

  </head>
  <body>


	<section class="main-section-elementor login-form-main-box-ineer-bg margin-box">
		
		<div class="container">
			
			<div class="row">
				<div class="col-sm-5" style="margin: auto;">

					<div class="login-content-containe" style=" transform: translate(1px, 22%);">
					
						<div class="login-content">

							<div class="up-logo-img">
								
								<img src="img/logo-cosmatics.jpg">

							</div>
							
							<div class="login-img-text-section">


								
								<div class="login-header">
									<img src="img/agent.png">

									<p>We are open for</p>

								    <h1>Seller Registration</h1>
								</div>

								<div class="login-box-button">

									<ul>

									<li>

											<div class="quote-button login-buttonone ">
												
							                  <a class=" btn btn-2 " type="button" value="button"  href="https://www.seller.thelocal2vocal.com/shops/create">
							                    <div class="eff-1"></div>
							                    <span>Seller Registration</span></a>
							                </div>

							            </li>

							            <li>

											<div class="quote-button login-buttontwo ">
												
							                  <a class=" btn btn-2 " type="button" value="button"  href="https://www.seller.thelocal2vocal.com/login">
							                    <div class="eff-1"></div>
							                    <span>Seller Login</span></a>
							                </div>

							            </li>



						            </ul>    
					            </div> 

					            <div class="login-icon">
					            	
					            	<ul class="">
						               			
				               		
				               			<li>
				               				
				               				<a href="https://www.facebook.com/Thelocal2vocal/" target="_blank" class="icons-sm fb-ic">
				               					<i class="fab fa-facebook-square"> </i>
				               				</a>
				               				
				               				<a href="https://twitter.com/local2the" target="_blank" class="icons-sm tw-ic">
				               					<i class="fab fa-twitter-square"> </i>
				               				</a>
				               				
				               				<a href="https://instagram.com/thelocal2vocal?igshid=13d6r2j70gcuo" target="_blank" class="icons-sm instagram-ic">
				               					<i class="fab fa-instagram-square"> </i>
				               				</a>

				               			</li>



				               		</ul>

				               			<h5>- Social Links -</h5>

					            </div>  


							</div>

						</div>

					</div>	

				</div>
				
				<div class="col-sm-2" style="margin: auto;">
				</div>
                <div class="col-sm-5" style="margin: auto;">
                    	<div class="login-content-containe" style=" transform: translate(1px, 22%);">
                    	    <div class="login-content">
                    	        <h2>About Us </h2>
                    	        <p style="text-align: justify;">Thelocal2vocal is a designated place designed to transform India into a global maker. We follow the concept of #AatmaNirbhar India by helping homegrown brands to prosper,  making India a global manufacturing leader. We believe when a country invests in itself, it empowers its communities and citizen in every aspect. We are not just a platform but a voice of our nation. Our mission is to foster Indian brands and entrepreneurs of all categories through our platform and lead #Aatmanirbhar Bharat.</p>
                    	        <hr>
                    	        <div style="text-align:left;">
                    	           <strong>Address: </strong>A-140, Sector 63 , Noida -201301<br>
                    	           <strong>Email: </strong>query@thelocal2vocal.com<br>
                    	           <strong>Phone Number: </strong>+911204205675
                    	        </div>
                    	    </div>
                    	</div>
                </div>
			</div>

		</div>

	</section>




<!--------footer-------->

<?php include"includes/login-footer.php";?>
<!------footer.end---------->


	</body>
</html>