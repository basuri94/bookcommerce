@extends('admin.layout.admin_template')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body">
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{translate('Saler Invoice Generate')}}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">
                    {{-- <div class="row">
                            <div class="col-sm-12">
                            <a href="{{ route('staffs.create')}}" class="btn btn-rounded btn-info pull-right">{{translate('Add New Staff')}}</a>
                            </div>
                        </div> --}}
                        <div class="pull-right clearfix" style="width: 100%;">
                            <form class="" id="sort_sellers" action="{{route('seller_report_invoice')}}" method="GET">
                                @csrf
                                <div class="box-inline pad-rgt pull-left">
                                    <div class="box-inline pad-rgt pull-left">
                                        <div class="" style="min-width: 200px;">
                                        
                                            <input type="text" autocomplete="off"  value="<?php echo isset($_GET['from_date'])?$_GET['from_date']:'' ?>" class="form-control" id="from_date" name="from_date"
                                         
                                              
                                            
                                            
                                            placeholder="{{ translate('For Month') }}">
                                           
                                        </div>
                                    </div>
                                   

                                <div class="box-inline pad-rgt pull-left">
                                    <div class="" style="min-width: 200px;margin-left:10px;">
                                        <button class="btn btn-primary mr-1 mb-1" type="submit">{{ translate('Generate') }}</button>
                                    <button class="btn btn-primary mr-1 mb-1" type="button"  onclick="window.location.href='{{route('sales.budget')}}'">{{ translate('Reset') }}</button>
                                </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                        <th>{{ translate('Sl#') }}</th>
                                        <th>{{ translate('Vendor Name') }}</th>
                                         <th>{{ translate('Download') }}</th>
                                         <th>{{ translate('Send To Seller') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($data))
                                    @foreach ($data as $key => $sales_invoice)
                                   
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{ $sales_invoice['vendor_name'] }}</td>
                                           
                                            <td> <a onclick="download_invoice('{{route('seller_report_invoice_download')}}','{{$sales_invoice['id']}}','download');"><i class="fa fa-download" aria-hidden="true"></i></a></td>
                                            <td> <a onclick="download_invoice('{{route('seller_report_invoice_download')}}','{{$sales_invoice['id']}}','send');"><i class="fa fa-paper-plane" aria-hidden="true"></i></a></td>
                                        </tr>
                                
                                    @endforeach
                                    @endif
                                </tbody>
                              <tfoot>
                                    <tr>
                                    <th>{{ translate('Sl#') }}</th>
                                        <th>{{ translate('Vendor Name') }}</th>
                                         <th>{{ translate('Download') }}</th>
                                         <th>{{ translate('Send To Seller') }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
</div>
</div>

@endsection
@section('script')
<script>
  $('#from_date').datepicker({
               // startDate: '-0d',
                // todayBtn: "linked",
                // autoclose: true,
                // todayHighlight: true,
                // format: 'mm/yyyy'
                format: "mm-yyyy",
    viewMode: "months", 
    minViewMode: "months"
        	});
          
            

            function download_invoice(url,seller_id,type){
                var from_date=$('#from_date').val();
                var token = $("input[name='_token']").val();
        // var fd = new FormData();
        // fd.append('_token', token);
        // fd.append('from_date', from_date);
        // fd.append('to_date', to_date);
        var data={from_date:from_date,_token:token,seller_id:seller_id,type:type};
        redirectPost(url,data); 

               
            }

            var redirectPost = function (url, data = null, method = 'post') {
                var form = document.createElement('form');
                form.method = method;
                form.action = url;
                for (var name in data) {
                    var input = document.createElement('input');
                    input.type = 'hidden';
                    input.name = name;
                    input.value = data[name];
                    form.appendChild(input);
                }
                $('body').append(form);
                form.submit();
            };
</script>
@endsection
