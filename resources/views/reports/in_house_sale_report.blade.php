@extends('admin.layout.admin_template')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body">
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{ translate('Product wise sale report') }}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">


                    <div class="pull-left clearfix">
                    <form class="" action="{{ route('in_house_sale_report.index') }}" method="GET">
                    <div class="box-inline pad-rgt pull-left">
                        <div class="select" style="min-width: 300px;">
                        <span> {{ translate('Sort by Category') }}:</span>
                                    <div class="select" style=" margin-top:5px;">
                                        <select id="demo-ease" class="form-control" name="category_id" required>
                                            @foreach (\App\Category::all() as $key => $category)
                                                <option value="{{ $category->id }}">{{ __($category->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                        </div>
                    </div>
                    <div class="box-inline pad-rgt pull-left">
                        <div class="" style="min-width: 200px; margin-top:25px;margin-left:10px;">
                        <button class="btn btn-primary mr-1 mb-1" type="submit">{{ translate('Filter') }}</button>
                        </div>
                    </div>
                    

                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                       <th>{{ translate('Product Name') }}</th>
                                       <th>{{ translate('Num of Sale') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   @foreach ($products as $key => $product)
                                        <tr>
                                            <td>{{ __($product->name) }}</td>
                                            <td>{{ $product->num_of_sale }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                              <tfoot>
                                    <tr>
                                       <th>{{ translate('Product Name') }}</th>
                                       <th>{{ translate('Num of Sale') }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
</div>
</div>
@endsection
