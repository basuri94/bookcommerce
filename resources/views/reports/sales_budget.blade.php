@extends('admin.layout.admin_template')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body">
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{translate('Sales Ledger')}}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">
                    {{-- <div class="row">
                            <div class="col-sm-12">
                            <a href="{{ route('staffs.create')}}" class="btn btn-rounded btn-info pull-right">{{translate('Add New Staff')}}</a>
                            </div>
                        </div> --}}
                        <div class="pull-right clearfix">
                            <form class="" id="sort_sellers" action="{{route('sales.budget')}}" method="GET">
                                @csrf
                                <div class="box-inline pad-rgt pull-left">
                                    <div class="box-inline pad-rgt pull-left">
                                        <div class="" style="min-width: 200px;">
                                            <input type="text" autocomplete="off"  value="<?php echo isset($_GET['from_date'])?$_GET['from_date']:'' ?>" class="form-control" id="from_date" name="from_date"
                                         
                                              
                                            
                                            
                                            placeholder="{{ translate('From Date') }}">
                                           
                                        </div>
                                    </div>
                                    <div class="box-inline pad-rgt pull-left">
                                        <div class="" style="min-width: 200px;">
                                            <input type="text" autocomplete="off" class="form-control" id="to_date" name="to_date"
                                            value="<?php echo isset($_GET['to_date'])?$_GET['to_date']:'' ?>"
                                            
                                            placeholder="{{ translate('To Date') }}">
                                            
                                        </div>
                                    </div>
                                </div>
                             

                                <div class="box-inline pad-rgt pull-left">
                                    <div class="" style="min-width: 200px;margin-left:10px;">
                                        <button class="btn btn-primary mr-1 mb-1" type="submit">{{ translate('Filter') }}</button>
                                    <button class="btn btn-primary mr-1 mb-1" type="button" onclick="excelReport();">{{ translate('Export Excel') }}</button>
                                    <button class="btn btn-primary mr-1 mb-1" type="button"  onclick="window.location.href='{{route('sales.budget')}}'">{{ translate('Reset') }}</button>
                                </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                        <th>{{ translate('Sl#') }}</th>
                                        <th>{{ translate('Date') }}</th>
                                        <th>{{ translate('Invoice No') }}</th>
                                        <th>{{ translate('Vendor Name') }}</th>
                                        <th>{{ translate('GST No') }}</th>
                                        <th>{{ translate('Sale  (Wthout Tax)') }}</th>
                                         <th>{{ translate('Sale  (Wth Tax)') }}</th>
                                          <th>{{ translate('Commission Charges') }}</th>
                                         <th>{{ translate('GST') }}</th>
                                         <th>{{ translate('Amount Payable') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($data))
                                    @foreach ($data as $key => $sales_budget)
                                   
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{ $sales_budget['created_date'] }}</td>
                                            <td>{{ $sales_budget['invoice'] }}</td>
                                            <td>{{ $sales_budget['vendor_name'] }}</td>
                                            <td>{{ $sales_budget['gstno'] }}</td>
                                            <td>{{ $sales_budget['sale_without_tax'] }}</td>
                                            <td>{{ $sales_budget['sale_with_tax'] }}</td>
                                            <td>{{ $sales_budget['commission'] }}</td>
                                            <td>{{ $sales_budget['gst'] }}</td>
                                            <td>{{ $sales_budget['amount_pay'] }} </td>
                                           
                                        </tr>
                                
                                    @endforeach
                                    @endif
                                </tbody>
                              <tfoot>
                                    <tr>
                                        <th>{{ translate('Sl#') }}</th>
                                        <th>{{ translate('Date') }}</th>
                                        <th>{{ translate('Invoice No') }}</th>
                                        <th>{{ translate('Vendor Name') }}</th>
                                        <th>{{ translate('GST No') }}</th>
                                        <th>{{ translate('Sale  (Wthout Tax)') }}</th>
                                         <th>{{ translate('Sale  (Wth Tax)') }}</th>
                                          <th>{{ translate('Commission Charges') }}</th>
                                         <th>{{ translate('GST') }}</th>
                                         <th>{{ translate('Amount Payable') }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
</div>
</div>

@endsection
@section('script')
<script>
  $('#from_date').datepicker({
               // startDate: '-0d',
                todayBtn: "linked",
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy'
        	});
            $('#to_date').datepicker({
              //  startDate: '-0d',
                todayBtn: "linked",
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy'
        	});
            

            function excelReport(){
                var from_date=$('#from_date').val();
                var to_date=$('#to_date').val();
                var token = $("input[name='_token']").val();
        // var fd = new FormData();
        // fd.append('_token', token);
        // fd.append('from_date', from_date);
        // fd.append('to_date', to_date);
        var data={from_date:from_date,to_date:to_date,_token:token};
        redirectPost("{{route('sales.export')}}",data); 

               
            }

            var redirectPost = function (url, data = null, method = 'post') {
                var form = document.createElement('form');
                form.method = method;
                form.action = url;
                for (var name in data) {
                    var input = document.createElement('input');
                    input.type = 'hidden';
                    input.name = name;
                    input.value = data[name];
                    form.appendChild(input);
                }
                $('body').append(form);
                form.submit();
            };
</script>
@endsection
