@extends('admin.layout.admin_template')
@section('content')

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">{{translate('Vendor Report')}}</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    {{-- <div class="row">
                            <div class="col-sm-12">
                            <a href="{{ route('staffs.create')}}" class="btn btn-rounded btn-info
                                    pull-right">{{translate('Add New Staff')}}</a>
                                </div>
                            </div> --}}
                            <div class="pull-right clearfix">
                                <form class="" id="sort_sellers" action="{{route('sales.vendor.export')}}" method="POST">
                                    @csrf
                                    {{-- <div class="box-inline pad-rgt pull-left">
                                    <div class="select" style="min-width: 300px;">
                                        <select class="form-control demo-select2" name="approved_status" id="approved_status" onchange="sort_sellers()">
                                            <option value="">{{translate('Filter by Approval')}}</option>
                                    <option value="1" @isset($approved) @if($approved=='paid' ) selected @endif
                                        @endisset>{{translate('Approved')}}</option>
                                    <option value="0" @isset($approved) @if($approved=='unpaid' ) selected @endif
                                        @endisset>{{translate('Non-Approved')}}</option>
                                    </select>
                            </div>
                        </div>
                        <div class="box-inline pad-rgt pull-left">
                            <div class="" style="min-width: 200px;">
                                <input type="text" class="form-control" id="search" name="search" @isset($sort_search)
                                    value="{{ $sort_search }}" @endisset
                                    placeholder="{{ translate('Type name or email & Enter') }}">
                            </div>
                        </div> --}}

                        <div class="box-inline pad-rgt pull-left">
                            <div class="" style="min-width: 200px;margin-left:10px;">
                                <button class="btn btn-primary mr-1 mb-1"
                                    type="submit">{{ translate('Export Excel') }}</button>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table class="table zero-configuration">
                            <thead>
                                <tr>
                                    <th>{{ translate('Sl#') }}</th>

                                    <th>{{ translate('Vendor Name') }}</th>
                                    <th>{{ translate('GST No') }}</th>
                                    <th>{{ translate('Total Sale (Wthout Tax)') }}</th>
                                    <th>{{ translate('Tax') }}</th>
                                    <th>{{ translate('Sale  (Wth Tax)') }}</th>
                                    <th>{{ translate('Commission Charges') }}</th>

                                    <th>{{ translate('Amount Paid') }}</th>
                                    <th>{{ translate('Due') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($data))
                                @foreach ($data as $key => $sales_budget)

                                <tr>
                                    <td>{{$key+1}}</td>

                                    <td><a href="{{route('sales.vendor.record',encrypt($sales_budget['user_id']))}}">{{ $sales_budget['vendor_name'] }}</a></td>
                                    <td>{{ $sales_budget['gstno'] }}</td>
                                    <td>{{ $sales_budget['sale_without_tax'] }}</td>
                                    <td>{{ $sales_budget['tax'] }}</td>
                                    <td>{{ $sales_budget['sale_with_tax'] }}</td>
                                    <td>{{ $sales_budget['commission_charges'] }}</td>
                                    <td>{{ $sales_budget['amount_paid'] }}</td>
                                    <td>{{ $sales_budget['amount_due'] }} </td>

                                </tr>

                                @endforeach
                                @endif
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>{{ translate('Sl#') }}</th>

                                    <th>{{ translate('Vendor Name') }}</th>
                                    <th>{{ translate('GST No') }}</th>
                                    <th>{{ translate('Total Sale (Wthout Tax)') }}</th>
                                    <th>{{ translate('Tax') }}</th>
                                    <th>{{ translate('Sale  (Wth Tax)') }}</th>
                                    <th>{{ translate('Commission Charges') }}</th>

                                    <th>{{ translate('Amount Paid') }}</th>
                                    <th>{{ translate('Due') }}</th>
                                </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>
        </div>
    </div>
</div>
</div>
</section>
</div>
</div>
</div>

@endsection