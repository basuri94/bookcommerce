@extends('admin.layout.admin_template')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body">
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{ translate('Seller Based Selling Report') }}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">
                    <div class="pull-left clearfix">
                    <form class="" action="{{ route('seller_sale_report.index') }}" method="GET">
                                <div class="box-inline pad-rgt pull-left">
                                    <div class="select" style="min-width: 300px;">
                                    <span>{{ translate('Sort by verificarion status') }}:</span>
                                    <div class="select" style=" margin-top:5px;">
                                        <select class="form-control" name="verification_status" required>
                                            <option value="1">{{ translate('Approved') }}</option>
                                            <option value="0">{{ translate('Non Approved') }}</option>
                                        </select>
                                    </div>
                                    </div>
                                </div>
                                <div class="box-inline pad-rgt pull-left">
                                    <div class="" style="min-width: 200px; margin-top:25px;margin-left:10px;">
                                    <button class="btn btn-primary mr-1 mb-1" type="submit">{{ translate('Filter') }}</button>
                                    </div>
                                </div>

                    
                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                        <th>{{ translate('Seller Name') }}</th>
                                        <th>{{ translate('Shop Name') }}</th>
                                        <th>{{ translate('Number of Product Sale') }}</th>
                                        <th>{{ translate('Order Amount') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($sellers as $key => $seller)
                                @if($seller->user != null)
                                    <tr>
                                        <td>{{ $seller->user->name }}</td>
                                        <td>{{ $seller->user->shop->name }}</td>
                                        <td>
                                            @php
                                                $num_of_sale = 0;
                                                foreach ($seller->user->products as $key => $product) {
                                                    $num_of_sale += $product->num_of_sale;
                                                }
                                            @endphp
                                            {{ $num_of_sale }}
                                        </td>
                                        <td>
                                            {{ single_price(\App\OrderDetail::where('seller_id', $seller->user->id)->sum('price')) }}
                                        </td>
                                    </tr>
                                @endif
                                @endforeach
                                </tbody>
                              <tfoot>
                                    <tr>
                                        <th>{{ translate('Seller Name') }}</th>
                                        <th>{{ translate('Shop Name') }}</th>
                                        <th>{{ translate('Number of Product Sale') }}</th>
                                        <th>{{ translate('Order Amount') }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
</div>
</div>
@endsection