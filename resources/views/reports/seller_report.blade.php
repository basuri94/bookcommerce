@extends('admin.layout.admin_template')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body">
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{ translate('Seller report') }}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">
                    <div class="pull-left clearfix">
                    <form class="" action="{{ route('seller_report.index') }}" method="GET">
                                <div class="box-inline pad-rgt pull-left">
                                    <div class="select" style="min-width: 300px;">
                                    <span>{{ translate('Sort by verificarion status') }}:</span>
                                    <div class="select" style=" margin-top:5px;">
                                        <select class="form-control" name="verification_status" required>
                                            <option value="1">{{ translate('Approved') }}</option>
                                            <option value="0">{{ translate('Non Approved') }}</option>
                                        </select>
                                    </div>
                                    </div>
                                </div>
                                <div class="box-inline pad-rgt pull-left">
                                    <div class="" style="min-width: 200px; margin-top:25px;margin-left:10px;">
                                    <button class="btn btn-primary mr-1 mb-1" type="submit">{{ translate('Filter') }}</button>
                                    </div>
                                </div>


                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                           <th>{{ translate('Seller Name') }}</th>
                                            <th>{{ translate('Email') }}</th>
                                            <th>{{ translate('Shop Name') }}</th>
                                            <th>{{ translate('Status') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($sellers as $key => $seller)
                                @if($seller->user != null)
                                    <tr>
                                        <td>{{ $seller->user->name }}</td>
                                        <td>{{ $seller->user->email }}</td>
                                        <td>{{ $seller->user->shop->name }}</td>
                                        <td>
                                            @if ($seller->verification_status == 1)
                                                <div class="label label-table label-success">
                                                    {{translate('Verified')}}
                                                </div>
                                            @elseif ($seller->verification_info != null)
                                                <a href="{{ route('sellers.show_verification_request', $seller->id) }}">
                                                    <div class="label label-table label-info">
                                                        {{translate('Requested')}}
                                                    </div>
                                                </a>
                                            @else
                                                <div class="label label-table label-danger">
                                                    {{translate('Not Verified')}}
                                                </div>
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                                @endforeach
                                </tbody>
                              <tfoot>
                                    <tr>
                                            <th>{{ translate('Seller Name') }}</th>
                                            <th>{{ translate('Email') }}</th>
                                            <th>{{ translate('Shop Name') }}</th>
                                            <th>{{ translate('Status') }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
</div>
</div>

@endsection
