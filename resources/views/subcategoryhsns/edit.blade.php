
     
@extends('admin.layout.admin_template')
@section('content')
 
 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
     
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
  <div class="row match-height">
      <div class="col-md-12 col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">{{translate('Subcategory Wise HSN Information')}}</h3>
              </div>
              <div class="card-content">
                  <div class="card-body">
  <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('subcategoryhsns.update', $subsubcategory->id) }}" method="POST" enctype="multipart/form-data">
		<input name="_method" type="hidden" value="PATCH">
            @csrf
            <div class="form-body">
			<div class="row">
					
			        <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Category')}}</span></div>
					       <div class="col-md-8">
                           <select name="category_id" id="category_id" class="form-control demo-select2" required>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{__($category->name)}}</option>
                            @endforeach
                        </select>
					       </div>
				        </div>
			        </div>
			
			        <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Subcategory')}}</span></div>
					       <div class="col-md-8">
                           <select name="sub_category_id" id="sub_category_id" class="form-control demo-select2" required>

                          </select>
					       </div>
				        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('HSN')}}</span></div>
					       <div class="col-md-8">
						   <input type="text" placeholder="{{translate('HSN')}}" id="hns_no" name="hns_no" class="form-control" required value="{{$subsubcategory->hns_no}}">
                           </div>
				        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('GST (%)')}}</span></div>
					       <div class="col-md-8">
                           <input type="number" class="form-control" name="gst_amount" value="{{ $subsubcategory->gst_amount }}" required placeholder="{{translate('Gst Percentage')}}">
                         </div>
				        </div>
			        </div>
				
				 <div class="col-md-8 offset-md-4">
                                      <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                      <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                      <a href="{{route('subcategoryhsns.index')}}" class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>
                                  </div>
								  
            </div>
             </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->


                  </div>
              </div>
          </div>
      </div>
      
  </div>
</section>
<!-- // Basic Horizontal form layout section end -->


<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->

  @endsection


@section('script')

<script type="text/javascript">

    function get_subcategories_by_category(){
        var category_id = $('#category_id').val();
        $.post('{{ route('subcategories.get_subcategories_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){
            $('#sub_category_id').html(null);
            for (var i = 0; i < data.length; i++) {
                $('#sub_category_id').append($('<option>', {
                    value: data[i].id,
                    text: data[i].name
                }));
                $('.demo-select2').select2();
            }
        });
    }

    $('.demo-select2').select2();

    $(document).ready(function(){

        $("#category_id > option").each(function() {
            if(this.value == '{{$subsubcategory->subcategory->category_id}}'){
                $("#category_id").val(this.value).change();
            }
        });

        get_subcategories_by_category();
    });

    $('#category_id').on('change', function() {
        get_subcategories_by_category();
    });

</script>

@endsection
