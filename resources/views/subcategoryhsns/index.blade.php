@extends('admin.layout.admin_template')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body">
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Sub-category wise HNS</h4>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">
                    <div class="row">
                            <div class="col-sm-12">
                            <a href="{{ route('subcategoryhsns.create')}}" class="btn btn-rounded btn-info pull-right">{{translate('Add New Subcategory Wise HSN')}}</a>
                           </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{translate('Category')}}</th>
                                        <th>{{translate('Subcategory')}}</th>
                                        <th>{{translate('HSN')}}</th>
                                        <th>{{translate('GST')}}</th>
                                        {{-- <th>{{translate('Attributes')}}</th> --}}
                                        <th width="10%">{{translate('Options')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php //echo json_encode($subcategoryhsns); ?>
                            
                                @foreach($subcategoryhsns as $key => $subsubcategory)
                                @if ($subsubcategory->subcategory != null && $subsubcategory->subcategory->category != null)
                                    <tr>
                                        <td>{{ ($key+1) + ($subcategoryhsns->currentPage() - 1)*$subcategoryhsns->perPage() }}</td>
                                        <td>{{$subsubcategory->subcategory->name}}</td>
                                        <td>{{$subsubcategory->subcategory->name}}</td>
                                        <td>{{$subsubcategory->hns_no}}</td>
                                        <td>{{$subsubcategory->gst_amount}} %</td>
                                        <td>
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    {{translate('Actions')}} <i class="dropdown-caret"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="{{route('subcategoryhsns.edit', encrypt($subsubcategory->id))}}">{{translate('Edit')}}</a></li>
                                                    <li><a onclick="confirm_modal('{{route('subcategoryhsns.destroy', $subsubcategory->id)}}');">{{translate('Delete')}}</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                                @endforeach
                                </tbody>
                              <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>{{translate('Category')}}</th>
                                        <th>{{translate('Subcategory')}}</th>
                                        <th>{{translate('HSN')}}</th>
                                        <th>{{translate('GST')}}</th>
                                        {{-- <th>{{translate('Attributes')}}</th> --}}
                                        <th width="10%">{{translate('Options')}}</th>
                                    </tr>
                                </tfoot>
                            </table>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
</div>
</div>

@endsection
