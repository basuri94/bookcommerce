@extends('admin.layout.admin_template')
@section('content')
 
 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
     
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
  <div class="row match-height">
      <div class="col-md-12 col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">{{translate('Edit Page')}}</h4>
              </div>
              <div class="card-content">
                  <div class="card-body">
  <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('pages.update', $page->id) }}" method="POST" enctype="multipart/form-data">
            	@csrf
                <input type="hidden" name="_method" value="PATCH">
            <div class="form-body">
			<div class="row">
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Title')}}</span></div>
					       <div class="col-md-8">
                           <input type="text" placeholder="{{translate('Title')}}" id="title" name="title" value="{{ $page->title }}" class="form-control" required>
                        </div>
				        </div>
			        </div>
			        <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Slug')}}</span></div>
					       <div class="col-md-8">
                           <input type="text" placeholder="{{translate('Slug')}}" id="slug" name="slug" value="{{ $page->slug }}" class="form-control" required>
                            <small><code>http://domain.com/your-slug</code> Only a-z, numbers, hypen allowed</small>
                        </div>
				        </div>
			        </div>
                    <!-- <div class="row">
                        <div class="col-sm-12">
                          <div id="full-wrapper">
                            <div id="full-container">
                              <div class="editor"  name="content">
                                <h1 class="ql-align-center">{{translate('Content')}}</h1>
                               
                              </div>
                            </div>
                          </div>
                        </div>
                      </div> -->
                      <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Content')}}</span></div>
					       <div class="col-md-8">
						   <textarea class="editor" name="content" required>
                                @php
                                    echo $page->content
                                @endphp
                            </textarea>
                        </div>
				        </div>
			        </div>
                <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Meta Title')}}</span></div>
					       <div class="col-md-8">
                           <input type="text" placeholder="{{translate('Meta Title')}}" id="meta_title" name="meta_title" value="{{ $page->meta_title }}" class="form-control">
                        </div>
				        </div>
			        </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Meta Description')}}</span></div>
					       <div class="col-md-8">
                           <input type="text" placeholder="{{translate('Meta Description')}}" id="meta_description" name="meta_description" value="{{ $page->meta_description }}" class="form-control">
                        </div>
				        </div>
			        </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Keywords')}}</span></div>
					       <div class="col-md-8">
                           <input type="text" placeholder="{{translate('Keywords')}}" id="keywords" name="keywords" value="{{ $page->keywords }}" class="form-control">
                        </div>
				        </div>
			        </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Meta Image')}}</span> <small>(200x300)</small></div>
					       <div class="col-md-8">
                           <input type="file" id="meta_image" name="meta_image" class="form-control">
                        </div>
				        </div>
			        </div>
				
				 <div class="col-md-8 offset-md-4">
                                      <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Update')}}</button>
                                      <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                      <a href="{{route('pages.index')}}" class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>
                                  </div>
								  
            </div>
             </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->


                  </div>
              </div>
          </div>
      </div>
      
  </div>
</section>
<!-- // Basic Horizontal form layout section end -->


<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->

@endsection
@section('script')
  <script>
var editor1 = CKEDITOR.replace('content', {
      extraAllowedContent: 'div(*);h3(*);img(*);h1(*);button(*);h2(*);h4(*);ul(*);a(*);section(*);li(*);span(*);p(*)',
      height: 400,
      filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
      filebrowserUploadMethod: 'form'
    });
    editor1.on('instanceReady', function() {
      // Output self-closing tags the HTML4 way, like <br>.
      this.dataProcessor.writer.selfClosingEnd = '>';

      // Use line breaks for block elements, tables, and lists.
      var dtd = CKEDITOR.dtd;
      for (var e in CKEDITOR.tools.extend({}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent)) {
        this.dataProcessor.writer.setRules(e, {
          indent: true,
          breakBeforeOpen: true,
          breakAfterOpen: true,
          breakBeforeClose: true,
          breakAfterClose: true
        });
      }
      // Start in source mode.
      //this.setMode('source');
    });


  </script>
  @stop
