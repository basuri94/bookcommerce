     
@extends('admin.layout.admin_template')
@section('content')
 
 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
     
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
  <div class="row match-height">
      <div class="col-md-12 col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">{{translate('SEO Settings')}}</h4>
              </div>
              <div class="card-content">
                  <div class="card-body">
  <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('seosetting.update',$seosetting->id ) }}" method="POST" enctype="multipart/form-data">
            	@csrf
                <input type="hidden" name="_method" value="PATCH">
            <div class="form-body">
			<div class="row">
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Keyword')}}</span></div>
					       <div class="col-md-8">
						   <input type="text" class="form-control" name="tags[]" value="{{ $seosetting->keyword }}" placeholder="{{translate('Type and Hit Enter')}}" data-role="tagsinput">
                        </div>
				        </div>
			        </div>
			        <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Author')}}</span></div>
					       <div class="col-md-8">
                           <input type="text" id="author" name="author" value="{{ $seosetting->author }}" class="form-control" required>
                        </div>
				        </div>
			        </div>
			
			        <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Revisit After')}}</span></div>
					       <div class="col-md-6">
						   <input type="number" min="0" step="1" value="{{ $seosetting->revisit }}" placeholder="{{translate('Revisit After')}}" name="revisit" class="form-control" required>
                        </div>
                        <div class="col-md-2" for="days">{{translate('Days')}}</div>
				        </div>
			        </div>
                <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Sitemap Link')}}</span></div>
					       <div class="col-md-8">
                           <input type="text" id="sitemap" name="sitemap" value="{{ $seosetting->sitemap_link }}" class="form-control" required>
                        </div>
				        </div>
			        </div>
               
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Description')}}</span></div>
					       <div class="col-md-8">
                           <textarea class="form-control" rows="5" name="description">{{ $seosetting->description }}</textarea>
                        </div>
				        </div>
			        </div>
				
				 <div class="col-md-8 offset-md-4">
                                      <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                      <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                  </div>
								  
            </div>
             </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->


                  </div>
              </div>
          </div>
      </div>
      
  </div>
</section>
<!-- // Basic Horizontal form layout section end -->


<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->

  @endsection

