    
@extends('admin.layout.admin_template')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body"><!-- Dashboard Ecommerce Starts -->
<!-- Zero configuration table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{translate('Customers')}}</h4>
                    <div class="box-inline pad-rgt pull-left">
                        @csrf
                                    <div class="" style="min-width: 200px;margin-left:10px;">
                                       <button class="btn btn-primary mr-1 mb-1" type="button" onclick="excelReport();">{{ translate('Export Excel') }}</button>
                                    </div>
                                </div>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">
                        
                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{translate('Name')}}</th>
                                        <th>{{translate('Email Address')}}</th>
                                        <th>{{translate('Phone')}}</th>
                                       
                                        <th>{{translate('Wallet Balance')}}</th>
                                        <th width="10%">{{translate('Options')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($customers as $key => $customer)
                                    @if ($customer->user != null)
                                        <tr>
                                            <td>{{ ($key+1) + ($customers->currentPage() - 1)*$customers->perPage() }}</td>
                                            <td>{{$customer->user->name}}</td>
                                            <td>{{$customer->user->email}}</td>
                                            <td>{{$customer->user->phone_no}}</td>
                                          
                                            <td>{{single_price($customer->user->balance)}}</td>
                                            <td>
                                                <div class="btn-group dropdown">
                                                    <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                        {{translate('Actions')}} <i class="dropdown-caret"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a onclick="confirm_modal('{{route('customers.destroy', $customer->id)}}');">{{translate('Delete')}}</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>{{translate('Name')}}</th>
                                        <th>{{translate('Email Address')}}</th>
                                        <th>{{translate('Phone')}}</th>
                                        
                                        <th>{{translate('Wallet Balance')}}</th>
                                        <th width="10%">{{translate('Options')}}</th>
                                    </tr>
                                </tfoot>
                            </table>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
</div>
</div>
<!-- END: Content-->




@endsection
@section('script')
    <script type="text/javascript">
        function sort_customers(el){
            $('#sort_customers').submit();
        }
        function excelReport(){
                var from_date=$('#from_date').val();
                var to_date=$('#to_date').val();
                var token = $("input[name='_token']").val();
        // var fd = new FormData();
        // fd.append('_token', token);
        // fd.append('from_date', from_date);
        // fd.append('to_date', to_date);
        var data={from_date:from_date,to_date:to_date,_token:token};
        redirectPost("{{route('customer_list.export')}}",data); 

               
            }
            var redirectPost = function (url, data = null, method = 'post') {
                var form = document.createElement('form');
                form.method = method;
                form.action = url;
                for (var name in data) {
                    var input = document.createElement('input');
                    input.type = 'hidden';
                    input.name = name;
                    input.value = data[name];
                    form.appendChild(input);
                }
                $('body').append(form);
                form.submit();
            };
    </script>
@endsection