@extends('admin.layout.admin_template')
@section('content')


<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- Dashboard Ecommerce Starts -->
            <!-- Zero configuration table -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">{{translate('Seller Withdraw Request')}}</h4>
                               
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                  
                                    <div class="table-responsive">
                                        <table class="table zero-configuration">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>{{translate('Date')}}</th>

                                                    <th>{{translate('Seller')}}</th>

                                                    <th>{{translate('Total Amount to Pay')}}</th>

                                                    <th>{{translate('Requested Amount')}}</th>

                                                    <th>{{ translate('Message') }}</th>

                                                    <th>{{ translate('Status') }}</th>

                                                    <th width="10%">{{translate('Options')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($seller_withdraw_requests as $key => $seller_withdraw_request)

                                                @if (\App\Seller::find($seller_withdraw_request->user_id) != null &&
                                                \App\Seller::find($seller_withdraw_request->user_id)->user != null)

                                                <tr>

                                                    <td>{{ ($key+1) + ($seller_withdraw_requests->currentPage() - 1)*$seller_withdraw_requests->perPage() }}
                                                    </td>

                                                    <td>{{ $seller_withdraw_request->created_at }}</td>

                                                    <td>

                                                        @if (\App\Seller::find($seller_withdraw_request->user_id) !=
                                                        null)

                                                        {{ \App\Seller::find($seller_withdraw_request->user_id)->user->name }}
                                                        ({{ \App\Seller::find($seller_withdraw_request->user_id)->user->shop->name }})

                                                        @endif

                                                    </td>

                                                    <td>{{ single_price(\App\Seller::find($seller_withdraw_request->user_id)->admin_to_pay) }}
                                                    </td>

                                                    <td>{{ single_price($seller_withdraw_request->amount) }}</td>

                                                    <td>

                                                        {{ $seller_withdraw_request->message }}

                                                    </td>

                                                    <td>

                                                        @if ($seller_withdraw_request->status == 1)

                                                        <span class="ml-2"
                                                            style="color:green"><strong>{{translate('Paid')}}</strong></span>

                                                        @else

                                                        <span class="ml-2"
                                                            style="color:red"><strong>{{translate('Pending')}}</strong></span>

                                                        @endif

                                                    </td>

                                                    <td>

                                                        <div class="btn-group dropdown">

                                                            <button
                                                                class="btn btn-primary dropdown-toggle dropdown-toggle-icon"
                                                                data-toggle="dropdown" type="button">

                                                                {{translate('Actions')}} <i class="dropdown-caret"></i>

                                                            </button>

                                                            <ul class="dropdown-menu dropdown-menu-right">

                                                                <li><a
                                                                        onclick="show_seller_payment_modal('{{$seller_withdraw_request->user_id}}','{{ $seller_withdraw_request->id }}');">{{translate('Pay Now')}}</a>
                                                                </li>

                                                                <li><a
                                                                        href="{{route('sellers.payment_history', encrypt($seller_withdraw_request->user_id))}}">{{translate('Payment History')}}</a>
                                                                </li>

                                                            </ul>

                                                        </div>

                                                    </td>

                                                </tr>

                                                @endif

                                                @endforeach
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>#</th>
                                                    <th>{{translate('Date')}}</th>

                                                    <th>{{translate('Seller')}}</th>

                                                    <th>{{translate('Total Amount to Pay')}}</th>

                                                    <th>{{translate('Requested Amount')}}</th>

                                                    <th>{{ translate('Message') }}</th>

                                                    <th>{{ translate('Status') }}</th>

                                                    <th width="10%">{{translate('Options')}}</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <div class="clearfix">
                                            <div class="pull-right">
                                                {{ $seller_withdraw_requests->appends(request()->input())->links() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="payment_modal" tabindex="-1" role="dialog"
aria-labelledby="myModalLabel17" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
  <div class="modal-content" id="modal-content">
  
   
  </div>
</div>
</div>

<div class="modal fade text-left" id="message_modal" tabindex="-1" role="dialog"
aria-labelledby="myModalLabel17" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
  <div class="modal-content" id="modal-content">
  
   
  </div>
</div>
</div>
@endsection



@section('script')

<script type="text/javascript">
    function show_seller_payment_modal(id, seller_withdraw_request_id){



            $.post('{{ route('withdraw_request.payment_modal') }}',{_token:'{{ @csrf_token() }}', id:id, seller_withdraw_request_id:seller_withdraw_request_id}, function(data){

                $('#modal-content').html(data);

                $('#payment_modal').modal('show', {backdrop: 'static'});

                $('.demo-select2-placeholder').select2();

            });

        }



        function show_message_modal(id){

            $.post('{{ route('withdraw_request.message_modal') }}',{_token:'{{ @csrf_token() }}', id:id}, function(data){

                $('#message_modal .modal-content').html(data);

                $('#message_modal').modal('show', {backdrop: 'static'});

            });

        }

</script>

@endsection