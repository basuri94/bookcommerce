@if(count($combinations[0]) > 0)
<!-- Striped rows start -->
<div class="row" id="table-striped">
	
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title"></h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                   
                </div>
                <div class="table-responsive">
                    <table class="table table-striped mb-0">
                        <thead>
                            <tr>
                                <th scope="col">{{translate('Variant')}}</th>
                                <th scope="col">{{translate('Variant Price')}}</th>
                                <th scope="col">{{translate('SKU')}}</th>
                                <th scope="col">{{translate('Quantity')}}</th>
                            </tr>
                      </thead>
                      <tbody>
						@foreach ($combinations as $key => $combination)
						@php
							$sku = '';
							foreach (explode(' ', $product_name) as $key => $value) {
								$sku .= substr($value, 0, 1);
							}
					
							$str = '';
							foreach ($combination as $key => $item){
								if($key > 0 ){
									$str .= '-'.str_replace(' ', '', $item);
									$sku .='-'.str_replace(' ', '', $item);
								}
								else{
									if($colors_active == 1){
										$color_name = \App\Color::where('code', $item)->first()->name;
										$str .= $color_name;
										$sku .='-'.$color_name;
									}
									else{
										$str .= str_replace(' ', '', $item);
										$sku .='-'.str_replace(' ', '', $item);
									}
								}
							}
						@endphp
						@if(strlen($str) > 0)
                        <tr>
                          <th>{{ $str }}</th>
                          <td>	<input type="number" name="price_{{ $str }}" value="{{ $unit_price }}" min="0" step="0.01" class="form-control" required></td>
                          <td><input type="text" name="sku_{{ $str }}" value="{{ $sku }}" class="form-control" required></td>
                          <td><input type="number" name="qty_{{ $str }}" value="10" min="0" step="1" class="form-control" required></td>
                        </tr>
                    
						@endif
						@endforeach
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endif


	
