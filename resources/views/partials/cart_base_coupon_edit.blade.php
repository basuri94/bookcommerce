@php
    $coupon_det = json_decode($coupon->details);
@endphp
<div class="panel-heading">
    <h3 class="panel-title">{{translate('Add Your Cart Base Coupon')}}</h3>
</div>
<div class="col-12">
    <div class="form-group row">
        <div class="col-md-4"><span>{{translate('Coupon code')}}</span></div>
        <div class="col-md-8">
            <input type="text" value="{{$coupon->code}}" id="coupon_code" name="coupon_code" class="form-control" required>
         </div>
    </div>
</div>
<div class="col-12">
    <div class="form-group row">
        <div class="col-md-4"><span>{{translate('Minimum Shopping')}}</span></div>
        <div class="col-md-8">
            <input type="number" min="0" step="0.01" name="min_buy" class="form-control" value="{{ $coupon_det->min_buy }}" required>
      </div>
    </div>
</div>
<div class="col-12">
    <div class="form-group row">
        <div class="col-md-4"><span>{{translate('Discount')}}</span></div>
        <div class="col-md-7">
               <input type="number" min="0" step="0.01" placeholder="{{translate('Discount')}}" name="discount" class="form-control" value="{{ $coupon->discount }}" required>
         </div>
        <div class="col-md-1">
         <select class="demo-select2" name="discount_type">
            <option value="amount" @if ($coupon->discount_type == 'amount') selected  @endif >₹</option>
            <option value="percent" @if ($coupon->discount_type == 'percent') selected  @endif>%</option>
         </select>
        </div>
    </div>
</div>
<div class="col-12">
    <div class="form-group row max_discount" @if ($coupon->discount_type == 'amount') style="display: none;"  @endif>
        <div class="col-md-4"><span>{{translate('Maximum Discount Amount')}}</span></div>
        <div class="col-md-8">
            <input type="number" min="0" step="0.01" placeholder="{{translate('Maximum Discount Amount')}}" name="max_discount" class="form-control" value="{{ $coupon_det->max_discount }}" >
      </div>
    </div>
</div>
<div class="col-12">
    <div class="form-group row">
        <div class="col-md-4"><span>{{translate('Date')}}</span></div>
        <div class="col-md-8">
           <div class="input-daterange input-group" id="datepicker">
               <input type="text" class="form-control" autocomplete="off" id="start_date" name="start_date" value="{{ date('m/d/Y', $coupon->start_date) }}">
               <span class="input-group-addon">{{translate('to')}}</span>
               <input type="text" autocomplete="off" class="form-control" id="end_date" name="end_date" value="{{ date('m/d/Y', $coupon->end_date) }}">
           </div>
        </div>
    </div>
</div>


<script type="text/javascript">

   $(document).ready(function(){
       $('.demo-select2').select2();
       $("#min_buy").keyup(function(){
            var min_buy=$("#min_buy").val();
            var discount=$("#discount").val();
            var discount_type=$("#discount_type").val();
            if(discount_type=="amount" && parseInt(discount) >= parseInt(min_buy)){
                $('.label-important').remove(); 
                $(this).after("<span class='label label-important' style='color: #e63b05;font-weight: 600;font-size: 12px;'>Minimum shoping amount must be greater than discount amount.</span>");
                $("#btnSubmit").attr("disabled", true);
          return false;  
            }else{
                $("#btnSubmit").attr("disabled", false);
                $('.label-important').remove(); 
            }
        });
        $("#discount").keyup(function(){
            var min_buy=$("#min_buy").val();
            var discount=$("#discount").val();
            var discount_type=$("#discount_type").val();
            if(discount_type=="amount" && parseInt(discount) >= parseInt(min_buy)){
                $('.label-important').remove(); 
                $(this).after("<span class='label label-important' style='color: #e63b05;font-weight: 600;font-size: 12px;'>Discount amount must be less than minimum shoping amount.</span>");
                $("#btnSubmit").attr("disabled", true);
          return false;  
            }else{
                $("#btnSubmit").attr("disabled", false);
                $('.label-important').remove(); 
            }
            if(discount_type=="percent" && discount>100){
                $('.label-important').remove(); 
                $(this).after("<span class='label label-important' style='color: #e63b05;font-weight: 600;font-size: 12px;'>Discount amount Must be less than 100.</span>");
                $("#btnSubmit").attr("disabled", true);
          return false;  
            }else{
                $("#btnSubmit").attr("disabled", false);
                $('.label-important').remove(); 
            }
        });
        $("#discount_type").change(function(){
            $("#discount").val('');
            var discount_type=$("#discount_type").val();
            if(discount_type=="percent"){
            $(".max_discount").show();   
            }else{
                $(".max_discount").hide();   
            }
        });
   });

</script>
