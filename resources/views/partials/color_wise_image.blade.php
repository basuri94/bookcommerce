@php 
if($colors_active==1){
if($options->count()>0){
foreach($options as $options_dt){
@endphp 
<div id="product-images" class="productmain{{$options_dt->id}}">
    <input type="hidden" id="imagefileCount{{$options_dt->id}}" value="0" />
    <div class="row">
        <div class="col-md-2">
            <label>{{ translate('Main Images For color ')}} {{$options_dt->name}} <span class="required-star">*</span></label>
        </div>
        <div class="col-md-10">
            <input type="file" name="photos{{$options_dt->id}}[]" id="photos-1-{{$options_dt->id}}" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" accept="image/*" />
            <label for="photos-1-{{$options_dt->id}}" class="mw-100 mb-3">
                <span></span>
                <strong>
                    <i class="fa fa-upload"></i>
                    {{ translate('Choose image')}}
                </strong>
            </label>
        </div>
    </div>
</div>
<div class="text-right">
    <button type="button" class="btn btn-info mb-3" onclick="add_more_slider_image({{$options_dt->id}})">{{  translate('Add More') }}</button>
</div>
@php
}    
}
}else{
    @endphp 
    <div id="product-images">
        <input type="hidden" id="imagefileCount" value="0" />
        <div class="row">
            <div class="col-md-2">
                <label>{{ translate('Main Images')}} <span class="required-star">*</span></label>
            </div>
            <div class="col-md-10">
                <input type="file" name="photos[]" id="photos-1" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" accept="image/*" />
                <label for="photos-1" class="mw-100 mb-3">
                    <span></span>
                    <strong>
                        <i class="fa fa-upload"></i>
                        {{ translate('Choose image')}}
                    </strong>
                </label>
            </div>
        </div>
    </div>
    <div class="text-right">
        <button type="button" class="btn btn-info mb-3" onclick="add_more_slider_image1()">{{  translate('Add More') }}</button>
    </div>
    @php
}
@endphp
