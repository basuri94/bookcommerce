@php 
if($colors_active==1){
if($options->count()>0){
foreach($options as $options_dt){
@endphp 
<div id="product-images" class="productmain{{$options_dt->id}}">
    <input type="hidden" id="imagefileCount{{$options_dt->id}}" value="0" />
    <div class="row">
        <div class="col-md-2">
            <label>{{ translate('Main Images For color ')}} {{$options_dt->name}} <span class="required-star">*</span></label>
        </div>

        <div class="col-md-10">
        <div class="row productsub{{$options_dt->id}}">
            @if ($product->photos != null  && count(json_decode($product->colors))!=0)
                @foreach (json_decode($product->photos) as $key1 => $photo1)
                    @if($photo1->id==$options_dt->id)
                    @foreach (json_decode($photo1->products) as $key => $photo)
                    <div class="col-md-3">
                        <div class="img-upload-preview">
                            <img loading="lazy"  src="{{ asset($photo) }}" alt="" class="img-responsive">
                            <input type="hidden" name="previous_photos{{$options_dt->id}}[]" value="{{ $photo }}">
                            @if($active_add==0)
                            <button type="button" class="btn btn-danger close-btn remove-files"><i class="fa fa-times"></i></button>
                        @endif
                        </div>
                    </div>
                    @endforeach
                    @endif
                @endforeach
            @endif
        </div>
        @if($active_add==0)
            <input type="file" name="photos{{$options_dt->id}}[]" id="photos-1-{{$options_dt->id}}" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" accept="image/*" />
            <label for="photos-1-{{$options_dt->id}}" class="mw-100 mb-3">
                <span></span>
                <strong>
                    <i class="fa fa-upload"></i>
                    {{ translate('Choose image')}}
                </strong>
            </label>
            @endif
        </div>
    </div>
</div>
@if($active_add==0)
<div class="text-right">
    <button type="button" class="btn btn-info mb-3" onclick="add_more_slider_image({{$options_dt->id}})">{{  translate('Add More') }}</button>
</div>
@endif
@php
}    
}
}else{
    @endphp 
    <div id="product-images">
        <input type="hidden" id="imagefileCount" value="{{count(json_decode($product->photos))}}" />
        <div class="row">
            <div class="col-md-2">
                <label>{{ translate('Main Images')}} <span class="required-star">*</span></label>
            </div>
            <div class="col-md-10">
                <div class="row">
                    @if ($product->photos != null && count(json_decode($product->colors))==0)
                        @foreach (json_decode($product->photos) as $key => $photo)
                            <div class="col-md-3">
                                <div class="img-upload-preview">
                                    <img loading="lazy"  src="{{ asset($photo) }}" alt="" class="img-responsive">
                                    <input type="hidden" name="previous_photos[]" value="{{ $photo }}">
                                    @if($active_add==0)
                                    <button type="button" class="btn btn-danger close-btn remove-files"><i class="fa fa-times"></i></button>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
                @if($active_add==0)
                <input type="file" name="photos[]" id="photos-1" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" accept="image/*" />
                <label for="photos-1" class="mw-100 mb-3">
                    <span></span>
                    <strong>
                        <i class="fa fa-upload"></i>
                        {{ translate('Choose image')}}
                    </strong>
                </label>
                @endif
            </div>
        </div>
    </div>
    @if($active_add==0)
    <div class="text-right">
        <button type="button" class="btn btn-info mb-3" onclick="add_more_slider_image1()">{{  translate('Add More') }}</button>
    </div>
    @endif
    @php
}
@endphp
