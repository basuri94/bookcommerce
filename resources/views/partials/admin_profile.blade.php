

     
@extends('admin.layout.admin_template')
@section('content')
 
 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
     
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
  <div class="row match-height">
      <div class="col-md-12 col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">{{translate('Profile')}}</h4>
              </div>
              <div class="card-content">
                  <div class="card-body">
  <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('profile.update', Auth::user()->id) }}" method="POST" enctype="multipart/form-data">
                <input name="_method" type="hidden" value="PATCH">
        	@csrf
            <div class="form-body">
			<div class="row">
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Name')}}</span></div>
					       <div class="col-md-8">
						   <input type="text" class="form-control" placeholder="{{translate('Name')}}" name="name" value="{{ Auth::user()->name }}" required>
                        </div>
				        </div>
			        </div>
			        <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Email')}}</span></div>
					       <div class="col-md-8">
                           <input type="email" class="form-control" placeholder="{{translate('Email')}}" name="email" value="{{ Auth::user()->email }}">
                        </div>
				        </div>
			        </div>
			
			        <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('New Password')}}</span></div>
					       <div class="col-md-8">
                           <input type="password" class="form-control" placeholder="{{translate('New Password')}}" name="new_password">
                        </div>
				        </div>
			        </div>
                <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Confirm Password')}}</span></div>
					       <div class="col-md-8">
                           <input type="password" class="form-control" placeholder="{{translate('Confirm Password')}}" name="confirm_password">
                        </div>
				        </div>
			        </div>
               
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Avatar')}}</span>&nbsp;<small>( 120x80 )</small></div>
					       <div class="col-md-8">
                           <input type="file" id="avatar" name="avatar" class="form-control">
                        </div>
				        </div>
			        </div>
				
				 <div class="col-md-8 offset-md-4">
                                      <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                      <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                  </div>
								  
            </div>
             </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->


                  </div>
              </div>
          </div>
      </div>
      
  </div>
</section>
<!-- // Basic Horizontal form layout section end -->


<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->

  @endsection