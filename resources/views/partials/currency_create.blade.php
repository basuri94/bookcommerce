<form class="form-horizontal" action="{{ route('currency.store') }}" method="POST" enctype="multipart/form-data">
<div class="modal-header">
    <h4 class="modal-title" id="myModalLabel17">{{translate('Add New Currency')}}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <section id="basic-horizontal-layouts">
        <div class="row match-height">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{translate('Add New Currency')}}</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                           
                                @csrf
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>{{translate('Name')}}</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" placeholder="{{translate('Name')}}" id="name" name="name" class="form-control" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>{{translate('Symbol')}}</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" placeholder="{{translate('Symbol')}}" id="symbol" name="symbol" class="form-control" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>{{translate('Code')}}</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" placeholder="{{translate('Code')}}" id="code" name="code" class="form-control" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>{{translate('Exchange Rate')}}</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="number" step="0.01" min="0" placeholder="{{translate('Exchange Rate')}}" id="exchange_rate" name="exchange_rate" class="form-control" required>
                                                </div>
                                            </div>
                                        </div>
                                      
                                       
                                    </div>
                                </div>
                           
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

</div>
<div class="modal-footer">
    <button type="submit" class="btn btn-primary ">{{translate('Save')}}</button>
    <button type="button" class="btn btn-primary" data-dismiss="modal">{{translate('Close')}}</button>
</div>

</form>