<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $order->code }}</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="UTF-8">
	<style media="all">
		@font-face {
            font-family: 'Roboto';
            src: url("{{ asset('fonts/Roboto-Regular.ttf') }}") format("truetype");
            font-weight: normal;
            font-style: normal;
        }
        *{
            margin: 0;
            padding: 0;
            line-height: 1.3;
            font-family: 'Roboto';
            color: #333542;
        }
		.footer {
			position: relative;
    margin-top: 20%;
    left: 0;
    bottom: 0;
    width: 100%;
   
    color: white;
    text-align: center;
    padding: 10px 0px 10px 0px;
}
		body{
			font-size: .875rem;
		}
		.gry-color *,
		.gry-color{
			color:#373737;
		}
		table{
			width: 100%;
		}
		table th{
			font-weight: normal;
		}
		table.padding th{
			padding: .5rem .7rem;
		}
		table.padding td{
			padding: .7rem;
		}
		table.sm-padding td{
			padding: .2rem .7rem;
		}
		.border-bottom td,
		.border-bottom th{
			border-bottom:1px solid #eceff4;
		}
		.text-left{
			text-align:left;
		}
		.text-right{
			text-align:right;
		}
		.small{
			font-size: .85rem;
		}
		.currency{

		}
	</style>
	<?php 
	use \App\Http\Controllers\InvoiceController;
	 ?>
</head>
<body>
	<div>

		@php
			$generalsetting = \App\GeneralSetting::first();
		@endphp
		@php
					$shipping_address = json_decode($order->shipping_address);
				@endphp
		<div style="background: #ffffff;padding: 1.5rem;">
			<table>
				<tr>
					<td>
						@if($generalsetting->logo != null)
							<img loading="lazy"  src="{{ asset($generalsetting->logo) }}" height="40" style="display:inline-block;">
						@else
							<img loading="lazy"  src="{{ asset('frontend/images/logo/logo1.png') }}" height="40" style="display:inline-block;">
						@endif
					</td>
					<td style="font-size: 1.3rem;" class="text-right strong">Tax Invoice/Bill of Supply/Cash Memo<br>(Original for Recipient)</td>
				</tr>
			</table>
			

		</div>
		<div style="margin-top: 2%;padding: 1.5rem;">
		<style>
		.bill th, .bill td {
  border: 1px solid black;
  border-collapse: collapse !important;
}
</style>
		<table>
			     <tr>
					<td>
					<table>
					@php 
		$seller=0;
		
		@endphp
		@foreach ($order->orderDetails as $key => $orderDetail)
		@php 
		
		$seller_id= $orderDetail->seller_id;
		if($seller!=$seller_id){
		$shop_details=InvoiceController::oder_wise_shop($seller_id);
		$seller_details=InvoiceController::oder_wise_user($seller_id);
		$seller_details_reg=InvoiceController::oder_wise_seller($seller_id);
		@endphp
			     <tr>
					<td style="font-size: 1.2rem;" class="strong">Sold By</td>
					</tr>
				<tr>
					<td style="font-size: 1.2rem;" class="strong">{{ $shop_details->name }}</td>
					</tr>
				<tr>
					<td class="gry-color small">{{ $shop_details->address }} ,{{ $shop_details->city }}:{{ $shop_details->postal_code }}</td>
					</tr>
				<!-- <tr>
					<td class="gry-color small">{{  translate('Email') }}: {{ $seller_details->email }}</td>
					</tr>
				<tr>
					<td class="gry-color small">{{  translate('Phone') }}: {{ $seller_details->phone_no }}</td>

					</tr> -->
					<tr style="font-weight: 700;">
					<td class="gry-color small">{{  translate('GST Registration No') }}: {{ $seller_details_reg->gst }}</td>
					
					</tr>
					@php
		}
		@endphp
		@endforeach
			</table>
					</td>
					<td>
					<table>
			     <tr>
					
					<td class="text-right strong small gry-color"  style="font-size: 1.2rem;">Billing Address:</td>
				</tr>
				<tr>
					
					<td class="text-right strong">{{ $shipping_address->name }}</td>
				</tr>
				<tr>
					
					<td class="text-right gry-color small">{{ $shipping_address->address }}, {{ $shipping_address->city }}, {{ $shipping_address->country }}</td>
				</tr>
				<tr>

					<td class="text-right gry-color small">{{ translate('Email') }}: {{ $shipping_address->email }}</td>
					
				</tr>
				<tr>
					
					<td class="text-right gry-color small">Phone: {{ $shipping_address->phone }}</td>
					
				</tr>
			</table>
					</td>
				</tr>
		</table>

	
		
		
		
			<table style="margin-top: 2%;">
			     <tr>
					
					<td class="text-right strong small gry-color"  style="font-size: 1.2rem;">Shiping Address:</td>
				</tr>
				<tr>
					
					<td class="text-right strong">{{ $shipping_address->name }}</td>
				</tr>
				<tr>
					
					<td class="text-right gry-color small">{{ $shipping_address->address }}, {{ $shipping_address->city }}, {{ $shipping_address->country }}</td>
				</tr>
				<tr>
					
					<td class="text-right gry-color small">{{ translate('Email') }}: {{ $shipping_address->email }}</td>
					
				</tr>
				<tr>
					
					<td class="text-right gry-color small">Phone: {{ $shipping_address->phone }}</td>
					
				</tr>
			</table>
		</div>

		<div style="padding: 1.5rem;padding-bottom: 0">
			<table>
				
				<tr><td class="text-right small"><span class="gry-color small">{{  translate('Order ID') }}:</span> <span class="strong">{{ $order->code }}</span></td></tr>
				<tr><td class="text-right small"><span class="gry-color small">{{  translate('Order Date') }}:</span> <span class=" strong">{{ date('d/m/Y', $order->date) }}</span></td></tr>
				
			</table>
		</div>

	    <div style="padding: 1.5rem;">
			<table class="padding text-left small border-bottom bill"  cellspacing="0" cellpadding="0">
				<thead>
	                <tr class="gry-color" style="background: #eceff4;">
	                    <th width="35%">{{ translate('Product Name') }}</th>
	                    <th width="10%">{{ translate('Qty') }}</th>
						<th width="15%">{{ translate('Unit Price') }}</th>
						<th width="15%">{{ translate('Shipping Cost') }}</th>
						<th width="10%">{{ translate('Tax Type') }}</th>
						<th width="10%">{{ translate('Tax Percent') }}</th>
	                    <th width="10%">{{ translate('Tax') }}</th>
	                    <th width="15%" class="text-right">{{ translate('Total Amount') }}</th>
	                </tr>
				</thead>
				<tbody class="strong">
	                @foreach ($order->orderDetails as $key => $orderDetail)
		                @if ($orderDetail->product != null)
							<tr class="">
								<td>{{ $orderDetail->product->name }} ({{ $orderDetail->variation }})<br>HSN: {{$orderDetail->product->subcategoryhsn->hns_no}}</td>
								
								<td class="gry-color">{{ $orderDetail->quantity }}</td>
								<td class="gry-color currency">{{ single_price($orderDetail->price/$orderDetail->quantity) }}</td>
								<td class="gry-color currency">{{ single_price($orderDetail->shipping_cost) }}</td>
								<td class="gry-color currency">IGST</td>
								<td class="gry-color currency">{{ $orderDetail->product->tax }}%</td>
								<td class="gry-color currency">{{ single_price($orderDetail->tax/$orderDetail->quantity) }}</td>

			                    <td class="text-right currency">{{ single_price($orderDetail->price+$orderDetail->tax) }}</td>
							</tr>
		                @endif
					@endforeach
					 <tr>
					<td width="15%">{{ translate('Total Amount') }}</td>
					<td width="15%">&nbsp;</td>
					<td width="15%">&nbsp;</td>
			            <td class="gry-color currency">{{ single_price($order->orderDetails->sum('shipping_cost')) }}</td>
			      
					<td width="15%">&nbsp;</td>
					<td width="15%">&nbsp;</td>
			            <td class="gry-color currency">{{ single_price($order->orderDetails->sum('tax')) }}</td>

			            
			            <td class="currency">{{ single_price($order->grand_total+$order->coupon_discount+$order->wallet_amount) }}</td>
					</tr>
					@if($order->coupon_discount!="0.00" && $order->coupon_discount !="")
	            <tr>
					<td width="15%">{{ translate('Coupon Discount') }}</td>
					<td width="15%">&nbsp;</td>
					<td width="15%">&nbsp;</td>
			        <td class="gry-color currency">&nbsp;</td>
			        <td width="15%">&nbsp;</td>
					<td width="15%">&nbsp;</td>
			        <td class="gry-color currency">&nbsp;</td>
                    <td class="currency"> {{ single_price($order->coupon_discount) }}</td>
					</tr>
					@endif
				@if($order->wallet_amount!="0.00" && $order->wallet_amount !="")
	            <tr>
					<td width="15%">{{ translate('Wallet Amount') }}</td>
					<td width="15%">&nbsp;</td>
					<td width="15%">&nbsp;</td>
			        <td class="gry-color currency">&nbsp;</td>
			        <td width="15%">&nbsp;</td>
					<td width="15%">&nbsp;</td>
			        <td class="gry-color currency">&nbsp;</td>
                    <td class="currency"> {{ single_price($order->wallet_amount) }}</td>
					</tr>
					@endif
			        <tr>
					<td width="15%">{{ translate('Amount To Pay') }}</td>
					<td width="15%">&nbsp;</td>
					<td width="15%">&nbsp;</td>
			            <td class="gry-color currency">&nbsp;</td>
			      
					<td width="15%">&nbsp;</td>
					<td width="15%">&nbsp;</td>
			            <td class="gry-color currency">&nbsp;</td>

			            
			            <td class="currency">{{ single_price($order->grand_total) }}</td>
					</tr>
					<tr style="font-weight: 700;">
					<td width="15%">{{ translate('Amount in Words') }}</td>
					<?php
 $get_amount= AmountInWords($order->grand_total);
//  echo $get_amount;
 ?>
			            <td class="gry-color currency" colspan="7" style="text-align: left;">{{ $get_amount }}</td>
					</tr>
					<tr>
						<td colspan="8" style="text-align: left;">
						Whether tax is payable under reverse charge - No

						</td>
					</tr>
		        </tbody>
		    </table>
		</div>
		<?php
// Create a function for converting the amount in words
function AmountInWords(float $amount)
{
   $amount_after_decimal = round($amount - ($num = floor($amount)), 2) * 100;
   // Check if there is any number after decimal
   $amt_hundred = null;
   $count_length = strlen($num);
   $x = 0;
   $string = array();
   $change_words = array(0 => '', 1 => 'One', 2 => 'Two',
     3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six',
     7 => 'Seven', 8 => 'Eight', 9 => 'Nine',
     10 => 'Ten', 11 => 'Eleven', 12 => 'Twelve',
     13 => 'Thirteen', 14 => 'Fourteen', 15 => 'Fifteen',
     16 => 'Sixteen', 17 => 'Seventeen', 18 => 'Eighteen',
     19 => 'Nineteen', 20 => 'Twenty', 30 => 'Thirty',
     40 => 'Forty', 50 => 'Fifty', 60 => 'Sixty',
     70 => 'Seventy', 80 => 'Eighty', 90 => 'Ninety');
    $here_digits = array('', 'Hundred','Thousand','Lakh', 'Crore');
    while( $x < $count_length ) {
      $get_divider = ($x == 2) ? 10 : 100;
      $amount = floor($num % $get_divider);
      $num = floor($num / $get_divider);
      $x += $get_divider == 10 ? 1 : 2;
      if ($amount) {
       $add_plural = (($counter = count($string)) && $amount > 9) ? 's' : null;
       $amt_hundred = ($counter == 1 && $string[0]) ? ' and ' : null;
       $string [] = ($amount < 21) ? $change_words[$amount].' '. $here_digits[$counter]. $add_plural.' 
       '.$amt_hundred:$change_words[floor($amount / 10) * 10].' '.$change_words[$amount % 10]. ' 
       '.$here_digits[$counter].$add_plural.' '.$amt_hundred;
        }
   else $string[] = null;
   }
   $implode_to_Rupees = implode('', array_reverse($string));
   $get_paise = ($amount_after_decimal > 0) ? "And " . ($change_words[$amount_after_decimal / 10] . " 
   " . $change_words[$amount_after_decimal % 10]) . ' Paise' : '';
   return ($implode_to_Rupees ? $implode_to_Rupees . 'Rupees ' : '') . $get_paise;
}
?>

<!-- call the function here -->
 
	</div>
</body>
</html>
