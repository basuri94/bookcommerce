<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Seller Invoice</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="UTF-8">
	<style media="all">
		@font-face {
            font-family: 'Roboto';
            src: url("{{ asset('fonts/Roboto-Regular.ttf') }}") format("truetype");
            font-weight: normal;
            font-style: normal;
        }
        *{
            margin: 0;
            padding: 0;
            line-height: 1.3;
            font-family: 'Roboto';
            color: #333542;
        }
		.footer {
			position: relative;
    margin-top: 20%;
    left: 0;
    bottom: 0;
    width: 100%;
   
    color: white;
    text-align: center;
    padding: 10px 0px 10px 0px;
}
		body{
			font-size: .875rem;
		}
		.gry-color *,
		.gry-color{
			color:#373737;
		}
		table{
			width: 100%;
		}
		table th{
			font-weight: normal;
		}
		table.padding th{
			padding: .5rem .7rem;
		}
		table.padding td{
			padding: .7rem;
		}
		table.sm-padding td{
			padding: .2rem .7rem;
		}
		.border-bottom td,
		.border-bottom th{
			border-bottom:1px solid #eceff4;
		}
		.text-left{
			text-align:left;
		}
		.text-right{
			text-align:right;
		}
		.small{
			font-size: .85rem;
		}
		.currency{

		}
        .text-center{
            margin-left: auto;
  margin-right: auto;
        }
 
	</style>
	<?php 
	use App\Http\Controllers\InvoiceController;
	 ?>
</head>
<body>
	<div>
	@php
						
							$user_id = $data['seller_id'];
						
					@endphp
		@php
			$generalsetting = \App\GeneralSetting::first();
		@endphp
		
		<div style="background: #ffffff;padding: 1.5rem;">
			<table>
				<tr>
					<td>
						@if($generalsetting->logo != null)
							<img loading="lazy"  src="{{ asset($generalsetting->logo) }}" height="40" style="display:inline-block;">
						@else
							<img loading="lazy"  src="{{ asset('frontend/images/logo/logo1.png') }}" height="40" style="display:inline-block;">
						@endif
					</td>
					<td style="font-size: 1.3rem;" class="text-right strong">Tax Invoice<br>(Original for Recipient)</td>
				</tr>
			</table>
			

		</div>
		<div style="margin-top: 2%;padding: 1.5rem;">
		<style>
		.bill th, .bill td {
  border: 1px solid black;
  border-collapse: collapse !important;
}
.pad_l_10{
    padding-left:10%;
}
</style>
		<table>
			     <tr>
					<td>
					<table>
					@php 
		$seller=0;
		
		@endphp
		
		@php 
		
		$seller_id= $user_id;
		
		
		$seller_details=InvoiceController::oder_wise_user($seller_id);
        $seller_details_reg=InvoiceController::oder_wise_seller($seller_id);
        $oder_wise_shop=InvoiceController::oder_wise_shop($seller_id);
        $admin_details=\App\User::where('user_type','admin')->first();
		@endphp
			   
				<tr>
					<td style="font-size: 1.2rem;" class="strong">L2V E Market Private Limited</td>
					</tr>
				
				<tr>
					<td class="gry-color small">{{ $generalsetting->address }} </td>
					</tr>
				 <tr>
					<td class="gry-color small">{{  translate('Email') }}: {{ $generalsetting->email }}</td>
					</tr>
				<tr>
					<td class="gry-color small">{{  translate('Phone') }}: {{ $generalsetting->phone }}</td>

					</tr>
					<tr>
					<td class="gry-color small"> Website : www.thelocal2vocal.com</td>

					</tr>
					<tr>
					<td class="gry-color small">Pan : AAECL2684A</td>

					</tr>
					<tr>
					<td class="gry-color small"> GST : 09AAECL2684A1ZN</td>

					</tr>
					<tr>
					<td class="gry-color small"> CIN : U74999UP2020PTC131268</td>

					</tr>
					
					<!--<tr>-->
					<!--<td class="gry-color small">{{  translate('Phone') }}: {{ $generalsetting->phone }}</td>-->

					<!--</tr>-->
					{{-- <tr style="font-weight: 700;">
					<td class="gry-color small">{{  translate('GST Registration No') }}: {{ $seller_details_reg->gst }}</td>
					
					</tr> --}}
		
			</table>
					</td>
					<td>
					<table>
			     <tr>
					
                 <td class="text-right strong small gry-color"  style="font-size: 1.2rem;">Invoice Date:{{$data['inv_date']}}</td>
                </tr>
                <tr>
					
					<td class="text-right strong small gry-color"  style="font-size: 1.2rem;">Invoice No:{{$data['inv_no']}}</td>
				</tr>
			
			</table>
					</td>
				</tr>
		</table>

	
		
		
		
			<table style="margin-top: 0%;">
			     <tr>
					
					<td class="text-right strong small gry-color"  style="font-size: 1.2rem;"></td>
				</tr>
				<tr>
					
					<td class="text-right strong"></td>
				</tr>
				<tr>
					
					<td class="text-right gry-color small"></td>
				</tr>
				<tr>
					
					<td class="text-right gry-color small"> </td>
					
				</tr>
				<tr>
					
					<td class="text-right gry-color small"></td>
					
				</tr>
			</table>
		</div>
    <hr>
	<div style="margin-top: 0%;padding: 1.5rem;">
<table>
    <tr>
        <td>
        <table>
    <tr>
        <td style="font-size: 1.2rem;" class="strong">Bill To</td>
        </tr>
    <tr>
        <td style="font-size: 1.2rem;" class="strong">Name: {{ $seller_details->name }}</td>
        </tr>
    <tr>
        <td class="gry-color small">{{ $seller_details_reg->business_nature }} </td>
        </tr>
     <tr>
        <td class="gry-color small">{{  translate('Email') }}: {{ $seller_details->email }}</td>
        </tr>
    <tr>
        <td class="gry-color small">{{  translate('Phone') }}: {{ $seller_details->phone_no }}</td>

        </tr>
        <tr>
        <td class="gry-color small">{{  translate('GST') }}: {{ $seller_details_reg->gst }}</td>

        </tr> 
        <tr>
        <td class="gry-color small">{{  translate('Address') }}: {{ $oder_wise_shop->address }}</td>

        </tr>
        @if($seller_details_reg->city !=null && $seller_details_reg->postal_code !=null)
         <tr>
        <td class="gry-color small">{{ $seller_details_reg->city }} : {{ $oder_wise_shop->postal_code }}</td>

        </tr>
        @endif
        
    </table>
    </td>
</tr>
   
   


    </table>

</div>
		<div >
			<table  width="60%" cellpadding="0" cellspacing="0" border="1" class="text-center">
			    
			    <tr>
                    <th style="text-align: center;"> S. No</th>
                    <th class="pad_l_10">Description of Services</th>
                    <th>Amount</th>
                </tr>
				<tr>
                    <td style="text-align: center;">1</td>
                    <td class="pad_l_10">Commision</td>
                    <td style="text-align: center;">{{single_price($data['commision'])}}</td>
                </tr>
                <tr>
                    <td style="text-align: center;">2</td>
                    <td class="pad_l_10">Courier Charge</td>
                    <td style="text-align: center;">{{single_price($data['curior_charge'])}}</td>
                </tr>
                <tr>
                    <td style="text-align: center;">3</td>
                    <td class="pad_l_10">Payment Handling Charge</td>
                    <td style="text-align: center;">{{single_price($data['payment_handling'])}}</td>
                </tr>
                <tr>
                    <td style="text-align: center;">4</td>
                    <td class="pad_l_10">GST</td>
                    <td style="text-align: center;">{{single_price($data['gst'])}}</td>
                </tr>
                
				<tr>
                    <td>&nbsp;</td>
                    <td class="pad_l_10">Total Amount</td>
                    <td style="text-align: center;">{{single_price($data['total_amount'])}}</td>
                </tr>
				
			</table>
		</div>

	  
		<?php
// Create a function for converting the amount in words
function AmountInWords(float $amount)
{
   $amount_after_decimal = round($amount - ($num = floor($amount)), 2) * 100;
   // Check if there is any number after decimal
   $amt_hundred = null;
   $count_length = strlen($num);
   $x = 0;
   $string = array();
   $change_words = array(0 => '', 1 => 'One', 2 => 'Two',
     3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six',
     7 => 'Seven', 8 => 'Eight', 9 => 'Nine',
     10 => 'Ten', 11 => 'Eleven', 12 => 'Twelve',
     13 => 'Thirteen', 14 => 'Fourteen', 15 => 'Fifteen',
     16 => 'Sixteen', 17 => 'Seventeen', 18 => 'Eighteen',
     19 => 'Nineteen', 20 => 'Twenty', 30 => 'Thirty',
     40 => 'Forty', 50 => 'Fifty', 60 => 'Sixty',
     70 => 'Seventy', 80 => 'Eighty', 90 => 'Ninety');
    $here_digits = array('', 'Hundred','Thousand','Lakh', 'Crore');
    while( $x < $count_length ) {
      $get_divider = ($x == 2) ? 10 : 100;
      $amount = floor($num % $get_divider);
      $num = floor($num / $get_divider);
      $x += $get_divider == 10 ? 1 : 2;
      if ($amount) {
       $add_plural = (($counter = count($string)) && $amount > 9) ? 's' : null;
       $amt_hundred = ($counter == 1 && $string[0]) ? ' and ' : null;
       $string [] = ($amount < 21) ? $change_words[$amount].' '. $here_digits[$counter]. $add_plural.' 
       '.$amt_hundred:$change_words[floor($amount / 10) * 10].' '.$change_words[$amount % 10]. ' 
       '.$here_digits[$counter].$add_plural.' '.$amt_hundred;
        }
   else $string[] = null;
   }
   $implode_to_Rupees = implode('', array_reverse($string));
   $get_paise = ($amount_after_decimal > 0) ? "And " . ($change_words[$amount_after_decimal / 10] . " 
   " . $change_words[$amount_after_decimal % 10]) . ' Paise' : '';
   return ($implode_to_Rupees ? $implode_to_Rupees . 'Rupees ' : '') . $get_paise;
}
?>

<!-- call the function here -->
 <div class="footer">

 </div>
	</div>
</body>
</html>
