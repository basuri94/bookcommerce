    
@extends('admin.layout.admin_template')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body"><!-- Dashboard Ecommerce Starts -->
<!-- Zero configuration table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{translate('Classified Packages')}}</h4>
                    <div class="row">
                        <div class="col-sm-12">
                            <a href="{{ route('customer_packages.create')}}" class="btn btn-rounded btn-info pull-right">{{translate('Add New Package')}}</a>
                        </div>
                    </div>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">
                        
                        <div class="row">
                            @foreach ($customer_packages as $key => $customer_package)
                                <div class="col-lg-3">
                                    <div class="panel">
                                        <div class="panel-body text-center">
                                            <img alt="Package Logo" class="img-lg img-circle mar-btm" src="{{ asset($customer_package->logo) }}">
                                            <p class="text-lg text-semibold mar-no text-main">{{$customer_package->name}}</p>
                                            <p class="text-3x">{{single_price($customer_package->amount)}}</p>
                                            <p class="text-sm text-overflow pad-top">
                                                 {{translate('Product Upload') }}:
                                                <span class="text-bold">{{$customer_package->product_upload}}</span>
                                            </p>
                                            <div class="mar-top">
                                                <a href="{{route('customer_packages.edit', encrypt($customer_package->id))}}" class="btn btn-mint">{{translate('Edit')}}</a>
                                                <a onclick="confirm_modal('{{route('customer_packages.destroy', $customer_package->id)}}');" class="btn btn-danger">{{translate('Delete')}}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
</div>
</div>
<!-- END: Content-->




@endsection
@section('script')
    <script type="text/javascript">
        function sort_customers(el){
            $('#sort_customers').submit();
        }
    </script>
@endsection