@extends('admin.layout.admin_template')
@section('content')
   

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h2 class="content-header-title float-left mb-0">{{translate('Create New Package')}}</h2>
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">{{translate('Home')}}</a>
                  </li>
                  <li class="breadcrumb-item"><a href="{{route('customer_packages.index')}}">{{translate('Classfied Packages')}}</a>
                  </li>
                  <li class="breadcrumb-item active"><a href="#">{{translate('Create New Package')}}</a>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--<div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
          <div class="form-group breadcrum-right">
            <div class="dropdown">
              <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>
              <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a></div>
            </div>
          </div>
        </div>-->
      </div>
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
  <div class="row match-height">
      <div class="col-md-12 col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title"></h4>
              </div>
              <div class="card-content">
                  <div class="card-body">
                    <form class="form-horizontal" action="{{ route('customer_packages.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-body">
                              <div class="row">
                                  <div class="col-12">
                                      <div class="form-group row">
                                          <div class="col-md-4">
                      <span>{{translate('Package Name')}}</span>
                    </div>
                                          <div class="col-md-8">
                                            <input type="text" placeholder="{{translate('Package Name')}}" id="name" name="name" class="form-control" required>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-12">
                                      <div class="form-group row">
                                          <div class="col-md-4">
                      <span>{{translate('Amount')}}</span>
                    </div>
                                          <div class="col-md-8">
                                            <input type="number" min="0" step="0.01" placeholder="{{translate('Amount')}}" id="amount" name="amount" class="form-control" required>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-12">
                                      <div class="form-group row">
                                          <div class="col-md-4">
                      <span>{{translate('Product Upload')}}</span>
                    </div>
                                          <div class="col-md-8">
                                            <input type="number" min="0" step="1" placeholder="{{translate('Product Upload')}}" id="product_upload" name="product_upload" class="form-control" required>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                    <span>{{translate('Package Logo')}}</span>
                  </div>
                                        <div class="col-md-8">
                                            <input type="file" id="logo" name="logo" class="form-control">
                                        </div>
                                    </div>
                                </div>
                    
                <div class="col-md-8 offset-md-4">
                                      <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                      <button type="reset" onclick="document.location.reload()" class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                  </div>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
      
  </div>
</section>
<!-- // Basic Horizontal form layout section end -->


<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->

@endsection
