@extends('admin.layout.admin_template')
@section('content')
 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
     
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
  <div class="row match-height">
      <div class="col-md-12 col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">{{translate('Role Information')}}</h4>
              </div>
              <div class="card-content">
                  <div class="card-body" >
   <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('roles.store') }}" method="POST"  enctype="multipart/form-data">
        	@csrf
            <div class="panel-body" >
                <div class="form-group row">
                    <label class="col-sm-3 control-label required" for="name">{{translate('Name')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{translate('Name')}}" id="name" name="name" class="form-control" required>
                    </div>
                </div>
                <div class="panel-heading">
                    <h3 class="panel-title">{{ translate('Permissions') }}</h3>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 control-label" for="banner"></label>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-10">
                                <label class="control-label">{{ translate('Products') }}</label>
                            </div>
                            <div class="col-sm-2">
                                <label class="switch">
                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="1">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div><hr>
                        <div class="row">
                            <div class="col-sm-10">
                                <label class="control-label">{{ translate('Flash Deal') }}</label>
                            </div>
                            <div class="col-sm-2">
                                <label class="switch">
                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="2">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div><hr>
                        <div class="row">
                            <div class="col-sm-10">
                                <label class="control-label">{{ translate('Orders') }}</label>
                            </div>
                            <div class="col-sm-2">
                                <label class="switch">
                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="3">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div><hr>
                        <div class="row">
                            <div class="col-sm-10">
                                <label class="control-label">{{ translate('Sales') }}</label>
                            </div>
                            <div class="col-sm-2">
                                <label class="switch">
                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="4">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div><hr>
                        <div class="row">
                            <div class="col-sm-10">
                                <label class="control-label">{{ translate('Sellers') }}</label>
                            </div>
                            <div class="col-sm-2">
                                <label class="switch">
                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="5">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div><hr>
                        <div class="row">
                            <div class="col-sm-10">
                                <label class="control-label">{{ translate('Customers') }}</label>
                            </div>
                            <div class="col-sm-2">
                                <label class="switch">
                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="6">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div><hr>
                        <!-- <div class="row">
                            <div class="col-sm-10">
                                <label class="control-label">{{ translate('Messaging') }}</label>
                            </div>
                            <div class="col-sm-2">
                                <label class="switch">
                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="7">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div><hr> -->
                        <div class="row">
                            <div class="col-sm-10">
                                <label class="control-label">{{ translate('Business Settings') }}</label>
                            </div>
                            <div class="col-sm-2">
                                <label class="switch">
                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="8">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div><hr>
                        <div class="row">
                            <div class="col-sm-10">
                                <label class="control-label">{{ translate('Frontend Settings') }}</label>
                            </div>
                            <div class="col-sm-2">
                                <label class="switch">
                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="9">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div><hr>
                        <div class="row">
                            <div class="col-sm-10">
                                <label class="control-label">{{ translate('Staffs') }}</label>
                            </div>
                            <div class="col-sm-2">
                                <label class="switch">
                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="10">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div><hr>
                        <div class="row">
                            <div class="col-sm-10">
                                <label class="control-label">{{ translate('SEO Setting') }}</label>
                            </div>
                            <div class="col-sm-2">
                                <label class="switch">
                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="11">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div><hr>
                        <div class="row">
                            <div class="col-sm-10">
                                <label class="control-label">{{ translate('E-commerce Setup') }}</label>
                            </div>
                            <div class="col-sm-2">
                                <label class="switch">
                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="12">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div><hr>
                        <div class="row">
                            <div class="col-sm-10">
                                <label class="control-label">{{ translate('Support Ticket') }}</label>
                            </div>
                            <div class="col-sm-2">
                                <label class="switch">
                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="13">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div><hr>
                        <!-- <div class="row">
                            <div class="col-sm-10">
                                <label class="control-label">{{ translate('Pickup Point Order') }}</label>
                            </div>
                            <div class="col-sm-2">
                                <label class="switch">
                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="14">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div><hr> -->
                        <!-- <div class="row">
                            <div class="col-sm-10">
                                <label class="control-label">{{ translate('Addon Manager') }}</label>
                            </div>
                            <div class="col-sm-2">
                                <label class="switch">
                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="15">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div><hr> -->
                    </div>
                </div>
            </div>
            <div class="panel-footer text-left" style="margin-left:25%;">
                   <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                    <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                    <a href="{{route('roles.index')}}" class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>

            </div>
        </form>


                  </div>
              </div>
          </div>
      </div>
      
  </div>
</section>
<!-- // Basic Horizontal form layout section end -->


<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->

  @endsection

