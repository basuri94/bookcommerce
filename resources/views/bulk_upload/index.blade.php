
    
@extends('admin.layout.admin_template')
@section('content')
 
 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
     
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
  <div class="row match-height">
      <div class="col-md-12 col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">{{translate('Product Bulk Upload')}}</h4>
              </div>
              <div class="card-content">
                  <div class="card-body">
  <!--Horizontal Form-->
        <!--===================================================-->
        
            <div class="form-body">
            <div class="row">

                    <div class="alert" style="color: #004085;background-color: #cce5ff;border-color: #b8daff;margin-bottom:0;margin-top:10px;">
                        <strong>{{ translate('Step 1')}}:</strong>
                        <p>1. {{translate('Download the skeleton file and fill it with proper data')}}.</p>
                        <p>2. {{translate('You can download the example file to understand how the data must be filled')}}.</p>
                        <p>3. {{translate('Once you have downloaded and filled the skeleton file, upload it in the form below and submit')}}.</p>
                        <p>4. {{translate('After uploading products you need to edit them and set product\'s images and choices')}}.</p>
                    </div>
                    <br><br><br>
                    <div class="" style="margin-bottom:0;margin-top:10px;margin-left:10px;">
                        <a href="{{ asset('download/product_bulk_demo.xlsx') }}" download><button class="btn btn-primary">{{ translate('Download CSV')}}</button></a>
                    </div>
                    <div class="alert" style="color: #004085;background-color: #cce5ff;border-color: #b8daff;margin-bottom:0;margin-top:10px;">
                        <strong>{{translate('Step 2')}}:</strong>
                        <p>1. {{translate('Category,Sub category,Sub Sub category and Brand should be in numerical ids')}}.</p>
                        <p>2. {{translate('You can download the pdf to get Category,Sub category,Sub Sub category and Brand id')}}.</p>
                    </div>
                    <br><br><br>
                    <div class="" style="margin-bottom:0;margin-top:20px;">
                        <a href="{{ route('pdf.download_category') }}"><button class="btn btn-primary">{{translate('Download Category')}}</button></a>
                        <a href="{{ route('pdf.download_sub_category') }}"><button class="btn btn-primary">{{translate('Download Sub category')}}</button></a>
                        <a href="{{ route('pdf.download_sub_sub_category') }}"><button class="btn btn-primary">{{translate('Download Sub Sub category')}}</button></a>
                        <a href="{{ route('pdf.download_brand') }}"><button class="btn btn-primary">{{translate('Download Brand')}}</button></a>
                    </div>
                    <br><br><br>
            </div>
			<div class="row" style="margin-bottom:0;margin-top:10px;">
            <form class="form-horizontal" action="{{ route('bulk_product_upload') }}" method="POST" enctype="multipart/form-data">
        	@csrf
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('File')}}</span></div>
					       <div class="col-md-8">
                           <input type="file" class="form-control" name="bulk_file" required>
					       </div>
				        </div>
			        </div>
			       
               
				
				 <div class="col-md-8 offset-md-4">
                                      <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Upload CSV')}}</button>
                                      
                                  </div>
                                  </form>				  
            </div>
             </div>
        
        <!--===================================================-->
        <!--End Horizontal Form-->


                  </div>
              </div>
          </div>
      </div>
      
  </div>
</section>
<!-- // Basic Horizontal form layout section end -->


<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->

  @endsection

