     
@extends('admin.layout.admin_template')
@section('content')
 
 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
     
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
  <div class="row match-height">
      <div class="col-md-12 col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">{{translate('Staff Information')}}</h4>
              </div>
              <div class="card-content">
                  <div class="card-body">
  <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('staffs.update', $staff->id) }}" method="POST" enctype="multipart/form-data">
            <input name="_method" type="hidden" value="PATCH">
        	@csrf
            <div class="form-body">
			<div class="row">
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Name')}}</span></div>
					       <div class="col-md-8">
						   <input type="text" placeholder="{{translate('Name')}}" id="name" name="name" value="{{ $staff->user->name }}" class="form-control" required>
					       </div>
				        </div>
			        </div>
			        <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Email')}}</span></div>
					       <div class="col-md-8">
                           <input type="text" placeholder="{{translate('Email')}}" id="email" name="email"  value="{{ $staff->user->email }}" class="form-control" required>
					       </div>
				        </div>
			        </div>
			
			        <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Phone')}}</span></div>
					       <div class="col-md-8">
						   <input type="text" placeholder="{{translate('Phone')}}" id="mobile" name="mobile" value="{{ $staff->user->phone }}" class="form-control" required>
					       </div>
				        </div>
			        </div>
                <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Password')}}</span></div>
					       <div class="col-md-8">
						   <input type="password" placeholder="{{translate('Password')}}" id="password" name="password" class="form-control">
                    </div>
				        </div>
			        </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Role')}}</span></div>
					       <div class="col-md-8">
						   <select name="role_id" required class="form-control demo-select2-placeholder">
                            @foreach($roles as $role)
                                <option value="{{$role->id}}" @php if($staff->role_id == $role->id) echo "selected"; @endphp >{{$role->name}}</option>
                            @endforeach
                        </select>
					       </div>
				        </div>
			        </div>
               
				 <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                    <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                    <a href="{{ route('staffs.index') }}" class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>
                </div>
								  
            </div>
             </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->
                  </div>
              </div>
          </div>
      </div>
      
  </div>
</section>
<!-- // Basic Horizontal form layout section end -->
<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->

@endsection
