
<!DOCTYPE html>

<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
 <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link name="favicon" type="image/x-icon" href="{{ asset(\App\GeneralSetting::first()->favicon) }}" rel="shortcut icon" />

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('front/app-assets/vendors/css/vendors.min.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('front/app-assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front/app-assets/css/bootstrap-extended.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front/app-assets/css/colors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front/app-assets/css/components.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front/app-assets/css/themes/dark-layout.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front/app-assets/css/themes/semi-dark-layout.min.css')}}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('front/app-assets/css/core/menu/menu-types/vertical-menu.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front/app-assets/css/core/colors/palette-gradient.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front/app-assets/css/pages/app-chat.min.css')}}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('front/assets/css/style.css')}}">
    <!-- END: Custom CSS-->

<style>

.bubbleWrapper {
	padding: 10px 10px;
	display: flex;
	justify-content: flex-end;
	flex-direction: column;
	align-self: flex-end;
  color: #fff;
}
.inlineContainer {
  display: inline-flex;
}
.inlineContainer.own {
  flex-direction: row-reverse;
}
.inlineIcon {
  width:20px;
  object-fit: contain;
}
.ownBubble {
	min-width: 60px;
	max-width: 700px;
	padding: 14px 18px;
  margin: 6px 8px;
	background-color: #5b5377;
	border-radius: 16px 16px 0 16px;
	border: 1px solid #443f56;
 
}
.otherBubble {
	min-width: 60px;
	max-width: 700px;
	padding: 14px 18px;
  margin: 6px 8px;
	background-color: #6C8EA4;
	border-radius: 16px 16px 16px 0;
	border: 1px solid #54788e;
  
}
.own {
	align-self: flex-end;
}
.other {
	align-self: flex-start;
}
span.own,
span.other{
  font-size: 14px;
  color: grey;
}
div.scroll {
  background-color: lightblue;
  width: 1000px;
  height: 180px;
  overflow: scroll;
}
</style>

  
  </head>
  <!-- END: Head-->

  <!-- BEGIN: Body-->
  <body class="vertical-layout vertical-menu-modern content-left-sidebar chat-application navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="content-left-sidebar">

    @include('admin.includes.header')
    
    <!-- END: Header-->

    @include('admin.includes.sidebar')


   
    <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      <div class="content-area-wrapper">

          <div class="content-right">
              <div class="content-wrapper">
                  <div class="content-header row">
                  </div>
                  <div class="content-body">



                      <section class="">

                          <div >
                              <div class="card no-border p-3">
                                    @php 
                                  $product_dta=\App\Product::find($conversation->product_id);
                                  @endphp
                                  
                                    <h2 class="heading heading-6 text-capitalize strong-600 mb-0 d-inline-block">
                                     @if($product_dta!="" && $product_dta!=null)
                                   <a href="{{ route('product', $product_dta->slug) }}" target="_blank">{{ $conversation->title }} </a>   
                                     
                                  
                                  @endif
                                       
                                    </h2>
                                  <br>
                                  Between @if($conversation->sender != null) 
                                  {{ $conversation->sender->name }} @endif and @if($conversation->receiver != null) 
                                  {{ $conversation->receiver->name }} @endif
                              </div>

                          </div>
         
                          <div id="messages" class="scroll">
          @foreach($conversation->messages as $message)
          <div class="bubbleWrapper">
            <div class="inlineContainer">
            
                <img class="inlineIcon" @if($message->user->avatar_original != null)src="{{ asset($message->user->avatar_original) }}" @else src="/frontend/images/user.png" @endif alt="Avatar">
               
               
                <div class="otherBubble other">
                  {{ $message->message }}
                </div>
              </div><span class="other">{{ date('h:i:m d-m-Y', strtotime($message->created_at)) }}</span>
            </div>
        
       
          @endforeach
          </div>
    @if (Auth::user()->id == $conversation->receiver_id)
    <div class="chat-app-form">
      <form class="chat-app-input d-flex" action="{{ route('messages.store') }}" method="POST">
        @csrf
        <input type="hidden" name="conversation_id" value="{{ $conversation->id }}">
        <input type="text" name="message" class="form-control message mr-1 ml-50" id="iconLeft4-1" placeholder="Type your message">
        <button type="submit" class="btn btn-primary send" ><i class="fa fa-paper-plane-o d-lg-none"></i> <span class="d-none d-lg-block">{{translate('Send')}}</span></button>
      </form>
    </div>
    @endif
    <div class="chat-app-form">
      <form class="chat-app-input d-flex" action="{{ route('strike_product') }}" method="POST">
        @csrf
        <input type="hidden" name="product_id" value="{{ $conversation->product_id }}">
        <button type="submit" class="btn btn-primary send" ><i class="fa fa-paper-plane-o d-lg-none"></i> <span class="d-none d-lg-block">{{translate('Strike Product')}}</span></button>
      </form>
    </div>
</section>

<!--/ User Chat profile right area -->

            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END: Content-->


    @include('admin.includes.customizer')
    @include('admin.includes.footer') 
    <script>
$(function(){
        
        var objDiv = document.getElementById("messages");
objDiv.scrollTop = objDiv.scrollHeight;

    });
</script>
</body>
<!-- END: Body-->

<!-- Mirrored from pixinvent.com/demo/vuexy-html-bootstrap-admin-template/html/ltr/vertical-menu-template/dashboard-ecommerce.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Aug 2020 19:44:14 GMT -->
</html>