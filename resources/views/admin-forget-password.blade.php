<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link name="favicon" type="image/x-icon" href="{{asset('img/favicon.png')}}" rel="shortcut icon" />

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="apple-touch-icon" href="{{ asset('front/app-assets/images/ico/apple-icon-120.html')}}">
    <link type="image/x-icon" href="{{ asset(\App\GeneralSetting::first()->favicon) }}" rel="shortcut icon" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/vendors/css/vendors.min.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/bootstrap-extended.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/colors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/components.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/themes/dark-layout.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/themes/semi-dark-layout.min.css')}}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/core/menu/menu-types/vertical-menu.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/core/colors/palette-gradient.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/pages/authentication.css')}}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('front/assets/css/style.css')}}">
    <!-- END: Custom CSS-->

  </head>
  <!-- END: Head-->
  @php
        $generalsetting = \App\GeneralSetting::first();
    @endphp
  <!-- BEGIN: Body-->
  <body class="vertical-layout vertical-menu-modern 1-column  navbar-floating footer-static bg-full-screen-image  blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
    <!-- BEGIN: Content-->
    <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
<section class="row flexbox-container">
    <div class="col-xl-12 col-11 d-flex " style="margin-left: 70%;">
        <div class="card bg-authentication rounded-0 mb-0" >
            <div class="row m-0">
          
                <div class="col-lg-12 col-12 p-0">
                    <div class="card rounded-0 mb-0 px-2">
                        <div class="card-header pb-1">
                            <div class="card-title">
                                <h4 class="mb-0">Forget Password</h4>
                            </div>
                        </div>
                        <p class="px-2">Please enter your registered mobile no.</p>
                        <div class="card-content">
                            <div class="card-body pt-1">
                                @if (count($errors) > 0)
                                <div class="alert alert-danger successErrorMessage">
                                  <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                  </ul>
                                </div>
                                @endif
                                @if(Session::has('success_message'))
                                <div class="alert alert-danger successErrorMessage">
                                <p {{ Session::get('alert-class', 'alert-info') }}>{{ Session::get('success_message') }}</p>
                            </div>
                                @endif
                  
                            @if(Session::has('failure_message'))
                            <p class="alert {{ Session::get('alert-class', 'alert-info') }} successErrorMessage">{{ Session::get('failure_message') }}</p>
                            @endif
                                <form method="POST" action="{{ route('adminpostforgetPassword') }}">
                                    @csrf
                                    <fieldset class="form-label-group form-group position-relative has-icon-left">
                                        <input id="mobile_no" type="integer" class="form-control @error('mobile_no') is-invalid @enderror" name="mobile_no"
                                            value="{{ old('mobile_no') }}" required autocomplete="off" autofocus>
                                
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                        <div class="form-control-position">
                                            <i class="fa fa-mobile"></i>
                                        </div>
                                        <label for="user-name">{{ __('Mobile No') }}</label>
                                    </fieldset>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Send') }}
                                    </button>
                                    <button type="button" onclick="window.location.href='{{route('login')}}'" class="btn btn-primary">
                                        {{ __('Back to login') }}
                                    </button>
                                </form>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

        </div>
      </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('front/app-assets/vendors/js/vendors.min.js')}}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('front/app-assets/js/core/app-menu.min.js')}}"></script>
    <script src="{{ asset('front/app-assets/js/core/app.min.js')}}"></script>
    <script src="{{ asset('front/app-assets/js/scripts/components.min.js')}}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <!-- END: Page JS-->
   <script>
       $(function(){
        $('.successErrorMessage').delay(8000).slideUp(300);
        $("#mobile_no").focus(function(){
  $("#mobile_no").val("+91");
});

  $("#mobile_no").keyup(function(){
    // var prefix = "+91"

    // if(this.value.indexOf(prefix) !== 0 ){
    //     this.value = prefix + this.value;
    // }
    if($(this).val().indexOf('+91') == 0) {
        $(this).val($(this).val());
    }else{
      $phone_val=$(this).val();
      if($phone_val!=""){
        $(this).val("");
      }
      $(this).val("+91" + $(this).val());
    }
});
       });



   </script>
  </body>
  <!-- END: Body-->

<!-- Mirrored from pixinvent.com/demo/vuexy-html-bootstrap-admin-template/html/ltr/vertical-menu-template/auth-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Aug 2020 19:44:13 GMT -->
</html>