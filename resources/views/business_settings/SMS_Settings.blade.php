     
@extends('admin.layout.admin_template')
@section('content')
 
 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h2 class="content-header-title float-left mb-0">SMS Template Settings</h2>
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="#">Business Settings</a>
                  </li>
                  <li class="breadcrumb-item active"><a href="#">SMS Template Settings</a>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        
      </div>
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
        <section id="basic-horizontal-layouts">
            <div class="row match-height">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{translate('SMS Template Settings')}}</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form-horizontal" action="{{ route('SMS_Settings.update') }}" method="POST">
                                    <input type="hidden" name="SMS_Settings" value="SMS">
                                    @csrf
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="CUSTOMER_REGISTRATION_OTP">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('Registration Otp')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="CUSTOMER_REGISTRATION_OTP" value="{{  env('CUSTOMER_REGISTRATION_OTP') }}" placeholder="{{ translate('Customer Registration Otp') }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="SELLER_REGISTRATION_OTP">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('Seller Registration Otp')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="SELLER_REGISTRATION_OTP" value="{{  env('SELLER_REGISTRATION_OTP') }}" placeholder="{{ translate('Seller Registration Otp') }}" required>
                                                    </div>
                                                </div>
                                            </div> --}}
                                           
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="FORGET_PASSWORD_OTP">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('Forget Password Otp')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="FORGET_PASSWORD_OTP" value="{{  env('FORGET_PASSWORD_OTP') }}" placeholder="{{ translate('Forget Password Otp') }}" required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="ORDER_PLACED">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('Order Placed')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="ORDER_PLACED" value="{{  env('ORDER_PLACED') }}" placeholder="{{ translate('Order Placed') }}" required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="ORDER_PROCESSING">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('Order Processing')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="ORDER_PROCESSING" value="{{  env('ORDER_PROCESSING') }}" placeholder="{{ translate('Order Processing') }}" required>
                                                    </div>
                                                </div>
                                            </div>


                                            {{-- <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="ORDER_CONFIRMED">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('Order Confirmed')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="ORDER_CONFIRMED" value="{{  env('ORDER_CONFIRMED') }}" placeholder="{{ translate('Order Confirmed') }}" required>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="ORDER_CANCELLED">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('Order Cancelled')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="ORDER_CANCELLED" value="{{  env('ORDER_CANCELLED') }}" placeholder="{{ translate('Order Cancelled') }}" required>
                                                    </div>
                                                </div>
                                            </div> --}}

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="ORDER_DISPATCHED">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('Order dispatched')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="ORDER_DISPATCHED" value="{{  env('ORDER_DISPATCHED') }}" placeholder="{{ translate('Order dispatched') }}" required>
                                                    </div>
                                                </div>
                                            </div>

                                         

                                         

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="CHANGE_PHONE_OTP">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('Change phone number otp')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="CHANGE_PHONE_OTP" value="{{  env('CHANGE_PHONE_OTP') }}" placeholder="{{ translate('Change phone number otp') }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="WALLET_CREDIT">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('Wallet Credit')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="WALLET_CREDIT" value="{{  env('WALLET_CREDIT') }}" placeholder="{{ translate('Wallet Credit') }}" required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="WALLET_DEBIT">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('Wallet Debit')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="WALLET_DEBIT" value="{{  env('WALLET_DEBIT') }}" placeholder="{{ translate('Wallet Debit') }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="REFER_BY_USER">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('Refer By user')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="REFER_BY_USER" value="{{  env('REFER_BY_USER') }}" placeholder="{{ translate('Refer By user') }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                           
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="REFERRED_USER">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('Referred  User')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="REFERRED_USER" value="{{  env('REFERRED_USER') }}" placeholder="{{ translate('Referred  User') }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                         
                          <div class="col-md-8 offset-md-4">
                                                <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                                <button type="reset" onclick="document.location.reload()" class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
               
                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<!-- // Basic Horizontal form layout section end -->


<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->

  @endsection