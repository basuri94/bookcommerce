@extends('admin.layout.admin_template')
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- Dashboard Ecommerce Starts -->
            <!-- Zero configuration table -->
            <section id="basic-horizontal-layouts">
                <div class="row match-height">
                    <div class="col-md-6 col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">{{translate('System Default Currency')}}</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <form class="form-horizontal" action="{{ route('business_settings.update') }}"
                                        method="POST">
                                        @csrf
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group row">

                                                        <div class="col-md-4">
                                                            <span>{{translate('System Default Currency')}}</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <select class="form-control demo-select2-placeholder"
                                                                name="system_default_currency">
                                                                @foreach ($active_currencies as $key => $currency)
                                                                <option value="{{ $currency->id }}"
                                                                    <?php if(\App\BusinessSetting::where('type', 'system_default_currency')->first()->value == $currency->id) echo 'selected'?>>
                                                                    {{ $currency->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <input type="hidden" name="types[]"
                                                            value="system_default_currency">
                                                    </div>
                                                </div>



                                                <div class="col-md-8 offset-md-4">
                                                    <button type="submit"
                                                        class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                                    <button type="reset" onclick="document.location.reload()"
                                                        class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">{{translate('Set Currency Formats')}}</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <form class="form-horizontal" action="{{ route('business_settings.update') }}"
                                        method="POST">
                                        @csrf
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <input type="hidden" name="types[]" value="symbol_format">
                                                        <div class="col-md-4">
                                                            <span>{{translate('Symbol Format')}}</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <select class="form-control demo-select2-placeholder"
                                                                name="symbol_format">
                                                                <option value="1"
                                                                    @if(\App\BusinessSetting::where('type', 'symbol_format'
                                                                    )->first()->value == 1) selected @endif>[Symbol]
                                                                    [Amount]</option>
                                                                <option value="2"
                                                                    @if(\App\BusinessSetting::where('type', 'symbol_format'
                                                                    )->first()->value == 2) selected @endif>[Amount]
                                                                    [Symbol]</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <input type="hidden" name="types[]" value="no_of_decimals">
                                                        <div class="col-md-4">
                                                            <span>{{ translate('No of decimals') }}</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <select class="form-control demo-select2-placeholder"
                                                                name="no_of_decimals">
                                                                <option value="0"
                                                                    @if(\App\BusinessSetting::where('type', 'no_of_decimals'
                                                                    )->first()->value == 0) selected @endif>12345
                                                                </option>
                                                                <option value="1"
                                                                    @if(\App\BusinessSetting::where('type', 'no_of_decimals'
                                                                    )->first()->value == 1) selected @endif>1234.5
                                                                </option>
                                                                <option value="2"
                                                                    @if(\App\BusinessSetting::where('type', 'no_of_decimals'
                                                                    )->first()->value == 2) selected @endif>123.45
                                                                </option>
                                                                <option value="3"
                                                                    @if(\App\BusinessSetting::where('type', 'no_of_decimals'
                                                                    )->first()->value == 3) selected @endif>12.345
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-md-8 offset-md-4">
                                                    <button type="submit"
                                                        class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                                    <button type="reset" onclick="document.location.reload()"
                                                        class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-body">
                        <div class="row">
                            <div class="col-12">

                                <a onclick="currency_modal()"
                                class="btn btn-rounded btn-info pull-right">{{translate('Add New Currency')}}</a>

                            </div>
                        </div>
                    </div>
                  
                    <br>

                    <!-- social links end -->
                </div>
            </section>

            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">{{translate('All Currency')}}</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                   
                                    <div class="table-responsive">
                                        <table class="table zero-configuration">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>{{translate('Currency name')}}</th>
                                                    <th>{{translate('Currency symbol')}}</th>
                                                    <th>{{translate('Currency code')}}</th>
                                                    <th>{{translate('Exchange rate')}}(1 USD = ?)</th>
                                                    <th>{{translate('Status')}}</th>
                                                    <th width="10%">{{translate('Options')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($currencies as $key => $currency)
                                                <tr>
                                                    <td>{{$key+1}}</td>
                                                    <td>{{$currency->name}}</td>
                                                    <td>{{$currency->symbol}}</td>
                                                    <td>{{$currency->code}}</td>
                                                    <td>{{$currency->exchange_rate}}</td>
                                                    <td>
                                                        <label class="switch">
                                                            <input onchange="update_currency_status(this)" value="{{ $currency->id }}" type="checkbox" <?php if($currency->status == 1) echo "checked";?> >
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <div class="btn-group dropdown">
                                                            <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                                {{translate('Actions')}} <i class="dropdown-caret"></i>
                                                            </button>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a onclick="edit_currency_modal('{{$currency->id}}');">{{translate('Edit')}}</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>#</th>
                                                    <th>{{translate('Currency name')}}</th>
                                                    <th>{{translate('Currency symbol')}}</th>
                                                    <th>{{translate('Currency code')}}</th>
                                                    <th>{{translate('Exchange rate')}}(1 USD = ?)</th>
                                                    <th>{{translate('Status')}}</th>
                                                    <th width="10%">{{translate('Options')}}</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </section>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="add_currency_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content" id="modal-content">


        </div>
    </div>
</div>
<div class="modal fade text-left" id="currency_modal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content" id="modal-content">


        </div>
    </div>
</div>




<!-- END: Content-->

@endsection


@section('script')
<script type="text/javascript">
    

        function currency_modal(){
            $.get('{{ route('currency.create') }}',function(data){
                $('#modal-content').html(data);
                $('#add_currency_modal').modal('show');
            });
        }

        function update_currency_status(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('currency.update_status') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    showAlert('success', 'Currency Status updated successfully');
                }
                else{
                    showAlert('danger', 'Something went wrong');
                }
            });
        }

        function edit_currency_modal(id){
            $.post('{{ route('currency.edit') }}',{_token:'{{ @csrf_token() }}', id:id}, function(data){
                $('#currency_modal_edit .modal-content').html(data);
                $('#currency_modal_edit').modal('show', {backdrop: 'static'});
            });
        }
</script>
@endsection