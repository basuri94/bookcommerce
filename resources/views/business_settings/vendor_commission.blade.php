    
@extends('admin.layout.admin_template')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body"><!-- Dashboard Ecommerce Starts -->
<!-- Zero configuration table -->
<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-6 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Vendor Commission Form</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <form class="form-horizontal" action="{{ route('business_settings.vendor_commission.update') }}" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="type" value="{{ $business_settings->type }}">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                        <span>{{ translate('Seller Commission') }}</span>
                      </div>
                                            <div class="col-md-8">
                                                <input type="number" min="0" step="0.01" value="{{ $business_settings->value }}" placeholder="{{translate('Seller Commission')}}" name="value" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                              
                             
                                
                 
                  <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                        <button type="reset" onclick="document.location.reload()" class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-12 ">
            <div class="card">
              <div class="card-header">
                <div class="card-title mb-2">Note</div>
              </div>
              <div class="card-body">
                <ul class="list-group">
                    <li class="list-group-item">
                        1. {{ $business_settings->value }}% {{translate('of seller product price will be deducted from seller earnings') }}.
                    </li>
                    <li class="list-group-item">
                        2. {{translate('This commission only works when Category Based Commission is turned off from Business Settings') }}.
                    </li>
                    <li class="list-group-item">
                        3. {{translate('Commission doesn\'t work if seller package system add-on is activated') }}.
                    </li>
                </ul>
              </div>
            </div>
          </div>
          <!-- social links end -->
    </div>
</section>
</div>
</div>
</div>
 





<!-- END: Content-->

@endsection
