@extends('admin.layout.admin_template')
@section('content')
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
        
        </div>
        <!--<div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
          <div class="form-group breadcrum-right">
            <div class="dropdown">
              <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>
              <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a></div>
            </div>
          </div>
        </div>-->
      </div>
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
  <div class="row match-height">
      <div class="col-md-12 col-12">
          <div class="card">
              
              <div class="card-content">
                  <div class="card-body">
                    <form class="form-horizontal" action="{{ route('languages.key_value_store') }}" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="{{ $language->id }}">
                          <div class="form-body">
                              <div class="row">
                                <section id="basic-datatable">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h4 class="card-title">{{ $language->name }}</h4>
                                                </div>
                                                <div class="card-content">
                                                    <div class="card-body card-dashboard">
                                                       
                                                        <div class="table-responsive">
                                                            <table class="table zero-configuration">
                                                                <thead>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>{{translate('Key')}}</th>
                                                                        <th>{{translate('Value')}}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @php
                                $i = 1;
                            @endphp
                            @foreach (openJSONFile('en') as $key => $value)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td class="key">{{ $key }}</td>
                                    <td>
                                        <input type="text" class="form-control value" style="width:100%" name="key[{{ $key }}]" @isset(openJSONFile($language->code)[$key])
                                            value="{{ openJSONFile($language->code)[$key] }}"
                                        @endisset>
                                    </td>
                                </tr>
                                @php
                                    $i++;
                                @endphp
                            @endforeach
                                                                </tbody>
                                                                <tfoot>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>{{translate('Key')}}</th>
                                                                        <th>{{translate('Value')}}</th>
                                                                    </tr>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  </section>
                                
                              
                              
                         
                <div class="col-md-8 offset-md-4">
                                      <button type="submit" onclick="copyTranslation()" class="btn btn-primary mr-1 mb-1">{{ translate('Copy Translations') }}</button>
                                      <button type="submit" class="btn btn-outline-warning mr-1 mb-1">{{ translate('Save') }}</button>
                                      <a href="{{route('languages.index')}}" class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>
                                  </div>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
      
  </div>
</section>
<!-- // Basic Horizontal form layout section end -->


<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>

@endsection

@section('script')
    <script type="text/javascript">
        //translate in one click
        function copyTranslation() {
            $('#tranlation-table > tbody  > tr').each(function (index, tr) {
                $(tr).find('.value').val($(tr).find('.key').text());
            });
        }
    </script>
@endsection

