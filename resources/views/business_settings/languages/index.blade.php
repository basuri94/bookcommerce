@extends('admin.layout.admin_template')
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- Dashboard Ecommerce Starts -->
            <!-- Zero configuration table -->
            <section id="basic-horizontal-layouts">
                <div class="row match-height">
                    <div class="col-md-6 col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">{{translate('Default Language')}}</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <form class="form-horizontal" action="{{ route('env_key_update.update') }}" method="POST">
                                        @csrf
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group row">

                                                        <div class="col-md-4">
                                                            <span>{{translate('Default Language')}}</span>
                                                        </div>
                                                        <input type="hidden" name="types[]" value="DEFAULT_LANGUAGE">
                                                        <div class="col-md-8">
                                                            <select class="form-control demo-select2-placeholder" name="{{ translate('DEFAULT_LANGUAGE') }}">
                                                                @foreach (\App\Language::all() as $key => $language)
                                                                    <option value="{{ $language->code }}" <?php if(env('DEFAULT_LANGUAGE') == $language->code) echo 'selected'?> >{{ $language->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        
                                                    </div>
                                                </div>



                                                <div class="col-md-8 offset-md-4">
                                                    <button type="submit"
                                                        class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                                    <button type="reset" onclick="document.location.reload()"
                                                        class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    

                    <div class="form-body">
                        <div class="row">
                            <div class="col-12">

                                <a href="{{ route('languages.create')}}" class="btn btn-rounded btn-info pull-right">{{translate('Add New Language')}}</a>

                            </div>
                        </div>
                    </div>
                  
                    <br>

                    <!-- social links end -->
                </div>
            </section>

            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">{{translate('Language')}}</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                   
                                    <div class="table-responsive">
                                        <table class="table zero-configuration">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>{{translate('Name')}}</th>
                                                    <th>{{translate('Code')}}</th>
                                                    <th>{{translate('RTL')}}</th>
                                                    <th width="10%">{{translate('Options')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                $i = 1;
                                            @endphp
                                            @foreach ($languages as $key => $language)
                                                <tr>
                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ $language->name }}</td>
                                                    <td>{{ $language->code }}</td>
                                                    <td><label class="switch">
                                                        <input onchange="update_rtl_status(this)" value="{{ $language->id }}" type="checkbox" <?php if($language->rtl == 1) echo "checked";?> >
                                                        <span class="slider round"></span></label>
                                                    </td>
                                                    <td>
                                                        <div class="btn-group dropdown">
                                                            <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                                {{translate('Actions')}} <i class="dropdown-caret"></i>
                                                            </button>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="{{route('languages.show', encrypt($language->id))}}">{{translate('Translation')}}</a></li>
                                                                <li><a href="{{route('languages.edit', encrypt($language->id))}}">{{translate('Edit')}}</a></li>
                                                                @if($language->code != 'en')
                                                                    <li><a onclick="confirm_modal('{{route('languages.destroy', $language->id)}}');">{{translate('Delete')}}</a></li>
                                                                @endif
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @php
                                                    $i++;
                                                @endphp
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>#</th>
                                                    <th>{{translate('Name')}}</th>
                                                    <th>{{translate('Code')}}</th>
                                                    <th>{{translate('RTL')}}</th>
                                                    <th width="10%">{{translate('Options')}}</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </section>
        </div>
    </div>
</div>





<!-- END: Content-->

@endsection


@section('script')
    <script type="text/javascript">
        function update_rtl_status(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('languages.update_rtl_status') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    location.reload();
                }
                else{
                    showAlert('danger', 'Something went wrong');
                }
            });
        }
    </script>
@endsection