@extends('admin.layout.admin_template')
@section('content')
   
 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h2 class="content-header-title float-left mb-0">{{translate('Seller Verification Form')}}</h2>
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">{{translate('Home')}}</a>
                  </li>
                  <li class="breadcrumb-item"><a href="{{route('sellers.index')}}">{{translate('Sellers')}}</a>
                  </li>
                  <li class="breadcrumb-item active"><a href="#">{{translate('Seller Verification Form')}}</a>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--<div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
          <div class="form-group breadcrum-right">
            <div class="dropdown">
              <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>
              <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a></div>
            </div>
          </div>
        </div>-->
      </div>
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
  <div class="row match-height">
      <div class="col-md-12 col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title"></h4>
              </div>
              <div class="card-content">
                  <div class="card-body">
					<form action="{{ route('seller_verification_form.update') }}" method="post">
						@csrf
                          <div class="form-body">
                            <div class="row">
								<div class="col-lg-8 form-horizontal" id="form">
									@foreach (json_decode(\App\BusinessSetting::where('type', 'verification_form')->first()->value) as $key => $element)
										@if ($element->type == 'text' || $element->type == 'file')
											<div class="form-group" style="background:rgba(0,0,0,0.1);padding:10px 0;">
												<input type="hidden" name="type[]" value="{{ $element->type }}">
												<div class="col-lg-3">
													<label class="control-label">{{ ucfirst($element->type) }}</label>
												</div>
												<div class="col-lg-7">
													<input class="form-control" type="text" name="label[]" value="{{ $element->label }}" placeholder="{{ translate('Label') }}">
												</div>
												<div class="col-lg-2"><span class="btn btn-icon btn-circle icon-lg fa fa-times" onclick="delete_choice_clearfix(this)"></span></div>
											</div>
										@elseif ($element->type == 'select' || $element->type == 'multi_select' || $element->type == 'radio')
											<div class="form-group" style="background:rgba(0,0,0,0.1);padding:10px 0;">
												<input type="hidden" name="type[]" value="{{ $element->type }}">
												<input type="hidden" name="option[]" class="option" value="{{ $key }}">
												<div class="col-lg-3">
													<label class="control-label">{{ ucfirst(str_replace('_', ' ', $element->type)) }}</label>
												</div>
												<div class="col-lg-7">
													<input class="form-control" type="text" name="label[]" value="{{ $element->label }}" placeholder="{{ translate('Select Label') }}" style="margin-bottom:10px">
													<div class="customer_choice_options_types_wrap_child">
														@if (is_array(json_decode($element->options)))
															@foreach (json_decode($element->options) as $value)
																<div class="form-group">
																	<div class="col-sm-6 col-sm-offset-4">
																		<input class="form-control" type="text" name="options_{{ $key }}[]" value="{{ $value }}" required="">
																	</div>
																	<div class="col-sm-2"> <span class="btn btn-icon btn-circle icon-lg fa fa-times" onclick="delete_choice_clearfix(this)"></span></div>
																</div>
															@endforeach
														@endif
													</div>
													<button class="btn btn-success pull-right" type="button" onclick="add_customer_choice_options(this)"><i class="glyphicon glyphicon-plus"></i> Add option</button>
												</div>
												<div class="col-lg-2"><span class="btn btn-icon btn-circle icon-lg fa fa-times" onclick="delete_choice_clearfix(this)"></span></div>
											</div>
										@endif
									@endforeach
								</div>
								<div class="col-lg-4">
		
									<ul class="list-group">
										<li class="list-group-item btn" style="text-align: left;" onclick="appenddToForm('text')">{{translate('Text Input')}}</li>
										<li class="list-group-item btn" style="text-align: left;" onclick="appenddToForm('select')">{{translate('Select')}}</li>
										<li class="list-group-item btn" style="text-align: left;" onclick="appenddToForm('multi-select')">{{translate('Multiple Select')}}</li>
										<li class="list-group-item btn" style="text-align: left;" onclick="appenddToForm('radio')">{{translate('Radio')}}</li>
										<li class="list-group-item btn" style="text-align: left;" onclick="appenddToForm('file')">{{translate('File')}}</li>
									</ul>
		
								</div>
							</div>
									
								
                                     
                <div class="col-md-8 offset-md-4">
					<button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
					<button type="reset" onclick="document.location.reload()" class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                  </div>
                              
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
      
  </div>
</section>
<!-- // Basic Horizontal form layout section end -->


<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->

  @endsection

  @section('script')
	<script type="text/javascript">

		var i = 0;

		function add_customer_choice_options(em){
			var j = $(em).closest('.form-group').find('.option').val();
			var str = '<div class="form-group">'
							+'<div class="col-sm-6 col-sm-offset-4">'
								+'<input class="form-control" type="text" name="options_'+j+'[]" value="" required>'
							+'</div>'
							+'<div class="col-sm-2"> <span class="btn btn-icon btn-circle icon-lg fa fa-times" onclick="delete_choice_clearfix(this)"></span>'
							+'</div>'
						+'</div>'
			$(em).parent().find('.customer_choice_options_types_wrap_child').append(str);
		}
		function delete_choice_clearfix(em){
			$(em).parent().parent().remove();
		}
		function appenddToForm(type){
			//$('#form').removeClass('seller_form_border');
			if(type == 'text'){
				var str = '<div class="form-group" style="background:rgba(0,0,0,0.1);padding:10px 0;">'
								+'<input type="hidden" name="type[]" value="text">'
								+'<div class="col-lg-3">'
									+'<label class="control-label">Text</label>'
								+'</div>'
								+'<div class="col-lg-7">'
									+'<input class="form-control" type="text" name="label[]" placeholder="{{ translate('Label') }}">'
								+'</div>'
								+'<div class="col-lg-2">'
									+'<span class="btn btn-icon btn-circle icon-lg fa fa-times" onclick="delete_choice_clearfix(this)"></span>'
								+'</div>'
							+'</div>';
				$('#form').append(str);
			}
			else if (type == 'select') {
				i++;
				var str = '<div class="form-group" style="background:rgba(0,0,0,0.1);padding:10px 0;">'
								+'<input type="hidden" name="type[]" value="select"><input type="hidden" name="option[]" class="option" value="'+i+'">'
								+'<div class="col-lg-3">'
									+'<label class="control-label">Select</label>'
								+'</div>'
								+'<div class="col-lg-7">'
									+'<input class="form-control" type="text" name="label[]" placeholder="{{ translate('Select Label') }}" style="margin-bottom:10px">'
									+'<div class="customer_choice_options_types_wrap_child">'

									+'</div>'
									+'<button class="btn btn-success pull-right" type="button" onclick="add_customer_choice_options(this)"><i class="glyphicon glyphicon-plus"></i> Add option</button>'
								+'</div>'
								+'<div class="col-lg-2">'
									+'<span class="btn btn-icon btn-circle icon-lg fa fa-times" onclick="delete_choice_clearfix(this)"></span>'
								+'</div>'
							+'</div>';
				$('#form').append(str);
			}
			else if (type == 'multi-select') {
				i++;
				var str = '<div class="form-group" style="background:rgba(0,0,0,0.1);padding:10px 0;">'
								+'<input type="hidden" name="type[]" value="multi_select"><input type="hidden" name="option[]" class="option" value="'+i+'">'
								+'<div class="col-lg-3">'
									+'<label class="control-label">Multiple select</label>'
								+'</div>'
								+'<div class="col-lg-7">'
									+'<input class="form-control" type="text" name="label[]" placeholder="{{ translate('Multiple Select Label') }}" style="margin-bottom:10px">'
									+'<div class="customer_choice_options_types_wrap_child">'

									+'</div>'
									+'<button class="btn btn-success pull-right" type="button" onclick="add_customer_choice_options(this)"><i class="glyphicon glyphicon-plus"></i> Add option</button>'
								+'</div>'
								+'<div class="col-lg-2">'
									+'<span class="btn btn-icon btn-circle icon-lg fa fa-times" onclick="delete_choice_clearfix(this)"></span>'
								+'</div>'
							+'</div>';
				$('#form').append(str);
			}
			else if (type == 'radio') {
				i++;
				var str = '<div class="form-group" style="background:rgba(0,0,0,0.1);padding:10px 0;">'
								+'<input type="hidden" name="type[]" value="radio"><input type="hidden" name="option[]" class="option" value="'+i+'">'
								+'<div class="col-lg-3">'
									+'<label class="control-label">Radio</label>'
								+'</div>'
								+'<div class="col-lg-7">'
									+'<div class="customer_choice_options_types_wrap_child">'
									+'<input class="form-control" type="text" name="label[]" placeholder="{{ translate('Radio Label') }}" style="margin-bottom:10px">'

									+'</div>'
									+'<button class="btn btn-success pull-right" type="button" onclick="add_customer_choice_options(this)"><i class="glyphicon glyphicon-plus"></i> Add option</button>'
								+'</div>'
								+'<div class="col-lg-2">'
									+'<span class="btn btn-icon btn-circle icon-lg fa fa-times" onclick="delete_choice_clearfix(this)"></span>'
								+'</div>'
							+'</div>';
				$('#form').append(str);
			}
			else if (type == 'file') {
				var str = '<div class="form-group" style="background:rgba(0,0,0,0.1);padding:10px 0;">'
								+'<input type="hidden" name="type[]" value="file">'
								+'<div class="col-lg-3">'
									+'<label class="control-label">File</label>'
								+'</div>'
								+'<div class="col-lg-7">'
									+'<input class="form-control" type="text" name="label[]" placeholder="{{ translate('Label') }}">'
								+'</div>'
								+'<div class="col-lg-2">'
									+'<span class="btn btn-icon btn-circle icon-lg fa fa-times" onclick="delete_choice_clearfix(this)"></span>'
								+'</div>'
							+'</div>';
				$('#form').append(str);
			}
		}
	</script>
@stop
