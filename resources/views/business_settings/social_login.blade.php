     
@extends('admin.layout.admin_template')
@section('content')
 
 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h2 class="content-header-title float-left mb-0">Social Media login</h2>
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="#">Business Settings</a>
                  </li>
                  <li class="breadcrumb-item active"><a href="#">Social Media login</a>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--<div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
          <div class="form-group breadcrum-right">
            <div class="dropdown">
              <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>
              <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a></div>
            </div>
          </div>
        </div>-->
      </div>
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
        <section id="basic-horizontal-layouts">
            <div class="row match-height">
                <div class="col-md-6 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{translate('Google Login Credential')}}</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form-horizontal" action="{{ route('env_key_update.update') }}" method="POST">
                                    @csrf
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="GOOGLE_CLIENT_ID">
                                                    <div class="col-md-4">
                                <span  class="required">{{translate('Client ID')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="GOOGLE_CLIENT_ID" value="{{  env('GOOGLE_CLIENT_ID') }}" placeholder="{{ translate('Google Client ID') }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="GOOGLE_CLIENT_SECRET">
                                                    <div class="col-md-4">
                                <span  class="required">{{translate('Client Secret')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="GOOGLE_CLIENT_SECRET" value="{{  env('GOOGLE_CLIENT_SECRET') }}" placeholder="{{ translate('Google Client Secret') }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                          
                                      
                          <div class="col-md-8 offset-md-4">
                                                <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                                <button type="reset" onclick="document.location.reload()" class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{translate('Facebook Login Credential')}}</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form-horizontal" action="{{ route('env_key_update.update') }}" method="POST">
                                    @csrf
                                 
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="FACEBOOK_CLIENT_ID">
                                                    <div class="col-md-4">
                                <span  class="required">{{translate('App ID')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <div class="position-relative has-icon-left">
                                                            <input type="text" class="form-control" name="FACEBOOK_CLIENT_ID" value="{{ env('FACEBOOK_CLIENT_ID') }}" placeholder="{{ translate('Facebook Client ID') }}" required>
                                                            <div class="form-control-position">
                                       <i class="feather icon-user"></i>
                                  </div>
                                </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="FACEBOOK_CLIENT_SECRET">
                                                    <div class="col-md-4">
                                <span  class="required">{{translate('App Secret')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <div class="position-relative has-icon-left">
                                                            <input type="text" class="form-control" name="FACEBOOK_CLIENT_SECRET" value="{{ env('FACEBOOK_CLIENT_SECRET') }}" placeholder="{{ translate('Facebook Client Secret') }}" required>
                                                            <div class="form-control-position">
                                      <i class="feather icon-mail"></i>
                                  </div>
                                </div>
                                                    </div>
                                                </div>
                                            </div>
                                          
                                          
                                         
                          <div class="col-md-8 offset-md-4">
                                                <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                                <button onclick="document.location.reload()" type="reset" class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{translate('Twitter Login Credential')}}</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form-horizontal" action="{{ route('env_key_update.update') }}" method="POST">
                                    @csrf
               
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="TWITTER_CLIENT_ID">
                                                    <div class="col-md-4">
                                <span  class="required">{{translate('Client ID')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="STRIPE_KEY" value="{{  env('STRIPE_KEY') }}" placeholder="{{ translate('STRIPE KEY') }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="TWITTER_CLIENT_SECRET">
                                                    <div class="col-md-4">
                                <span  class="required">{{translate('Client Secret')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="TWITTER_CLIENT_SECRET" value="{{ env('TWITTER_CLIENT_SECRET') }}" placeholder="{{ translate('Twitter Client Secret') }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                           
                                      
                          <div class="col-md-8 offset-md-4">
                                                <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                                <button type="reset" onclick="document.location.reload()" class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

               


                
            </div>
        </section>
<!-- // Basic Horizontal form layout section end -->


<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->

  @endsection