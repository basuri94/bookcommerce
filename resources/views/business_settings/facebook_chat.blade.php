    
@extends('admin.layout.admin_template')
@section('content')
 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body"><!-- Dashboard Ecommerce Starts -->
<!-- Zero configuration table -->
<section id="basic-horizontal-layouts">
<div class="row match-height">
    <div class="col-md-6 col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{translate('Facebook Chat Setting')}}</h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <form class="form-horizontal" action="{{ route('facebook_chat.update') }}" method="POST">
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group row">
                                       
                                        <div class="col-md-4">
                    <span>{{translate('Facebook Chat')}}</span>
                  </div>
                                        <div class="col-md-8">
                                        <label class="switch">
                                    <input value="1" name="facebook_chat" type="checkbox" @if (\App\BusinessSetting::where('type', 'facebook_chat')->first()->value == 1) checked @endif>
                                    <span class="slider round"></span>
                                </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group row">
                                        <input type="hidden" name="types[]" value="FACEBOOK_PAGE_ID">
                                        <div class="col-md-4">
                    <span class="required">{{translate('Facebook Page ID')}}</span>
                  </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="FACEBOOK_PAGE_ID" value="{{  env('FACEBOOK_PAGE_ID') }}" placeholder="{{ translate('Facebook Page ID') }}" required>
                                        </div>
                                    </div>
                                </div>
                              
                          
              <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                    <button type="reset" onclick="document.location.reload()" class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{translate('Facebook Pixel Setting')}}</h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <form class="form-horizontal" action="{{ route('facebook_pixel.update') }}" method="POST">
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group row">
                                       
                                        <div class="col-md-4">
                    <span >{{translate('Facebook Pixel')}}</span>
                  </div>
                                        <div class="col-md-8">
                                        <label class="switch">
                                    <input value="1" name="facebook_pixel" type="checkbox" @if (\App\BusinessSetting::where('type', 'facebook_pixel')->first()->value == 1) checked @endif>
                                    <span class="slider round"></span>
                                </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group row">
                                        <input type="hidden" name="types[]" value="FACEBOOK_PIXEL_ID">
                                        <div class="col-md-4">
                    <span class="required">{{ translate('Facebook Pixel ID') }}</span>
                  </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="FACEBOOK_PIXEL_ID" value="{{  env('FACEBOOK_PIXEL_ID') }}" placeholder="{{ translate('Facebook Pixel ID') }}" required>
                                        </div>
                                    </div>
                                </div>
                              
                          
              <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                    <button type="reset" onclick="document.location.reload()" class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-12 ">
        <div class="card">
          <div class="card-header">
            <div class="card-title mb-2">Note</div>
          </div>
          <div class="card-body">
            <h4>{{translate('Instruction')}}</h4>
            <p class="text-danger">{{ translate('Please be carefull when you are configuring Facebook chat. For incorrect configuration you will not get messenger icon on your user-end site.') }}</p>
                <hr>
               
                <ul class="list-group mar-no">
                    <li class="list-group-item text-dark">1. {{ translate('Login into your facebook page') }}</li>
                    <li class="list-group-item text-dark">2. {{ translate('Find the About option of your facebook page') }}.</li>
                    <li class="list-group-item text-dark">3. {{ translate('At the very bottom, you can find the \“Facebook Page ID\”') }}.</li>
                    <li class="list-group-item text-dark">4. {{ translate('Go to Settings of your page and find the option of \"Advance Messaging\"') }}.</li>
                    <li class="list-group-item text-dark">5. {{ translate('Scroll down that page and you will get \"white listed domain\"') }}.</li>
                    <li class="list-group-item text-dark">6. {{ translate('Set your website domain name') }}.</li>
                </ul>
               
          </div>
        </div>
      </div>

      <div class="col-md-6 col-12 ">
        <div class="card">
          <div class="card-header">
            <div class="card-title mb-2">Note</div>
          </div>
          <div class="card-body">
            <h4>{{translate('Instruction')}}</h4>
            <p class="text-danger">{{ translate('Please be carefull when you are configuring Facebook pixel.') }}</p>
                <hr>
               
                <ul class="list-group mar-no">
                    <li class="list-group-item text-dark">1. {{ translate('Log in to Facebook and go to your Ads Manager account') }}.</li>
                    <li class="list-group-item text-dark">2. {{ translate('Open the Navigation Bar and select Events Manager') }}.</li>
                    <li class="list-group-item text-dark">3. {{ translate('Copy your Pixel ID from underneath your Site Name and paste the number into Facebook Pixel ID field') }}.</li>
                </ul>
               
          </div>
        </div>
      </div>
      <!-- social links end -->
</div>
</section>
</div>
</div>
</div>






<!-- END: Content-->

@endsection


