     
@extends('admin.layout.admin_template')
@section('content')
 
 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h2 class="content-header-title float-left mb-0">Payment Method</h2>
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="#">Business Settings</a>
                  </li>
                  <li class="breadcrumb-item active"><a href="#">Payment Method</a>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        
      </div>
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
        <section id="basic-horizontal-layouts">
            <div class="row match-height">
                <div class="col-md-6 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{translate('Paypal Credential')}}</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form-horizontal" action="{{ route('payment_method.update') }}" method="POST">
                                    <input type="hidden" name="payment_method" value="paypal">
                                    @csrf
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="PAYPAL_CLIENT_ID">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('Paypal Client Id')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="PAYPAL_CLIENT_ID" value="{{  env('PAYPAL_CLIENT_ID') }}" placeholder="{{ translate('Paypal Client ID') }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="PAYPAL_CLIENT_SECRET">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('Paypal Client Secret')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="PAYPAL_CLIENT_SECRET" value="{{  env('PAYPAL_CLIENT_SECRET') }}" placeholder="{{ translate('Paypal Client Secret') }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('Paypal Sandbox Mode')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                    <label class="switch">
                                <input value="1" name="paypal_sandbox" type="checkbox" @if (\App\BusinessSetting::where('type', 'paypal_sandbox')->first()->value == 1) checked @endif>
                                <span class="slider round"></span>
                            </label>
                                                    </div>
                                                </div>
                                            </div>
                                      
                          <div class="col-md-8 offset-md-4">
                                                <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                                <button type="reset" onclick="document.location.reload()" class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{translate('Sslcommerz Credential')}}</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form-horizontal" action="{{ route('payment_method.update') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="payment_method" value="sslcommerz">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="SSLCZ_STORE_ID">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('Sslcz Store Id')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <div class="position-relative has-icon-left">
                                                            <input type="text" class="form-control" name="SSLCZ_STORE_ID" value="{{  env('SSLCZ_STORE_ID') }}" placeholder="{{translate('Sslcz Store Id')}}" required>
                                                            <div class="form-control-position">
                                       <i class="feather icon-user"></i>
                                  </div>
                                </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="SSLCZ_STORE_PASSWD">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('Sslcz store password')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <div class="position-relative has-icon-left">
                                                            <input type="text" class="form-control" name="SSLCZ_STORE_PASSWD" value="{{  env('SSLCZ_STORE_PASSWD') }}" placeholder="{{translate('Sslcz store password')}}" required>
                                                            <div class="form-control-position">
                                      <i class="feather icon-mail"></i>
                                  </div>
                                </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('Sslcommerz Sandbox Mode')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                    <label class="switch">
                                <input value="1" name="sslcommerz_sandbox" type="checkbox" @if (\App\BusinessSetting::where('type', 'sslcommerz_sandbox')->first()->value == 1) checked  @endif>
                                <span class="slider round"></span>
                            </label>
                                                    </div>
                                                </div>
                                            </div>
                                          
                                         
                          <div class="col-md-8 offset-md-4">
                                                <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                                <button onclick="document.location.reload()" type="reset" class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{translate('Stripe Credential')}}</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form-horizontal" action="{{ route('payment_method.update') }}" method="POST">
                                    @csrf
                    <input type="hidden" name="payment_method" value="stripe">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="STRIPE_KEY">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('Stripe Key')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="STRIPE_KEY" value="{{  env('STRIPE_KEY') }}" placeholder="{{ translate('STRIPE KEY') }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="STRIPE_SECRET">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('Stripe Secret')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="STRIPE_SECRET" value="{{  env('STRIPE_SECRET') }}" placeholder="{{ translate('STRIPE SECRET') }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                           
                                      
                          <div class="col-md-8 offset-md-4">
                                                <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                                <button type="reset" onclick="document.location.reload()" class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{translate('RazorPay Credential')}}</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form-horizontal" action="{{ route('payment_method.update') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="payment_method" value="razorpay">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="RAZOR_KEY">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('RAZOR KEY')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="RAZOR_KEY" value="{{  env('RAZOR_KEY') }}" placeholder="{{ translate('RAZOR KEY') }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="RAZOR_SECRET">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('RAZOR SECRET')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="RAZOR_SECRET" value="{{  env('RAZOR_SECRET') }}" placeholder="{{ translate('RAZOR SECRET') }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                          
                                      
                          <div class="col-md-8 offset-md-4">
                                                <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                                <button type="reset" onclick="document.location.reload()" class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{translate('Instamojo Credential')}}</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form-horizontal" action="{{ route('payment_method.update') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="payment_method" value="instamojo">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="IM_API_KEY">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('API KEY')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="IM_API_KEY" value="{{  env('IM_API_KEY') }}" placeholder="{{ translate('IM API KEY') }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <input type="hidden" name="types[]" value="IM_AUTH_TOKEN">
                                                    <div class="col-md-4">
                                <span class="required">{{translate('AUTH TOKEN')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="IM_AUTH_TOKEN" value="{{  env('IM_AUTH_TOKEN') }}" placeholder="{{ translate('IM AUTH TOKEN') }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                 
                                                    <div class="col-md-4">
                                <span class="required">{{translate('Instamojo Sandbox Mode')}}</span>
                              </div>
                                                    <div class="col-md-8">
                                                    <label class="switch">
                                <input value="1" name="instamojo_sandbox" type="checkbox" @if (\App\BusinessSetting::where('type', 'instamojo_sandbox')->first()->value == 1)
                                    checked
                                @endif>
                                <span class="slider round"></span>
                            </label>
                                                    </div>
                                                </div>
                                            </div>
                                      
                          <div class="col-md-8 offset-md-4">
                                                <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                                <button type="reset" onclick="document.location.reload()" class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-6 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{translate('PayStack Credential')}}</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                            <form class="form-horizontal" action="{{ route('payment_method.update') }}" method="POST">
                    @csrf
                    <input type="hidden" name="payment_method" value="paystack">
                    <div class="form-group row">
                        <input type="hidden" name="types[]" value="PAYSTACK_PUBLIC_KEY">
                        <div class="col-lg-4">
                            <label class="control-label required">{{translate('PUBLIC KEY')}}</label>
                        </div>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="PAYSTACK_PUBLIC_KEY" value="{{  env('PAYSTACK_PUBLIC_KEY') }}" placeholder="{{ translate('PUBLIC KEY') }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <input type="hidden" name="types[]" value="PAYSTACK_SECRET_KEY">
                        <div class="col-lg-4">
                            <label class="control-label required">{{translate('SECRET KEY')}}</label>
                        </div>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="PAYSTACK_SECRET_KEY" value="{{  env('PAYSTACK_SECRET_KEY') }}" placeholder="{{ translate('SECRET KEY') }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <input type="hidden" name="types[]" value="MERCHANT_EMAIL">
                        <div class="col-lg-4">
                            <label class="control-label required">{{translate('MERCHANT EMAIL')}}</label>
                        </div>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="MERCHANT_EMAIL" value="{{  env('MERCHANT_EMAIL') }}" placeholder="{{ translate('MERCHANT EMAIL') }}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12 text-right">
                            <button class="btn btn-primary mr-1 mb-1" type="submit">{{translate('Save')}}</button>
                        
                            <button type="reset" onclick="document.location.reload()" class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button></div>
                    </div>
                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{translate('VoguePay Credential')}}</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                            <form class="form-horizontal" action="{{ route('payment_method.update') }}" method="POST">
                    @csrf
                    <input type="hidden" name="payment_method" value="voguepay">
                    <div class="form-group row">
                        <input type="hidden" name="types[]" value="VOGUE_MERCHANT_ID">
                        <div class="col-lg-4">
                            <label class="control-label required">{{translate('MERCHANT ID')}}</label>
                        </div>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="VOGUE_MERCHANT_ID" value="{{  env('VOGUE_MERCHANT_ID') }}" placeholder="{{ translate('MERCHANT ID') }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label class="control-label required">{{translate('Sandbox Mode')}}</label>
                        </div>
                        <div class="col-lg-8">
                            <label class="switch">
                                <input value="1" name="voguepay_sandbox" type="checkbox" @if (\App\BusinessSetting::where('type', 'voguepay_sandbox')->first()->value == 1)
                                    checked
                                @endif>
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12 text-right">
                            <button class="btn btn-primary mr-1 mb-1" type="submit">{{translate('Save')}}</button>
                            <button type="reset" onclick="document.location.reload()" class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                        </div>
                    </div>
                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{translate('Payhere Credential')}}</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                            <form class="form-horizontal" action="{{ route('payment_method.update') }}" method="POST">
                    @csrf
                    <input type="hidden" name="payment_method" value="payhere">
                    <div class="form-group row">
                        <input type="hidden" name="types[]" value="PAYHERE_MERCHANT_ID">
                        <div class="col-lg-4">
                            <label class="control-label required">{{translate('PAYHERE MERCHANT ID')}}</label>
                        </div>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="PAYHERE_MERCHANT_ID" value="{{  env('PAYHERE_MERCHANT_ID') }}" placeholder="{{ translate('PAYHERE MERCHANT ID') }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <input type="hidden" name="types[]" value="PAYHERE_SECRET">
                        <div class="col-lg-4">
                            <label class="control-label required">{{translate('PAYHERE SECRET')}}</label>
                        </div>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="PAYHERE_SECRET" value="{{  env('PAYHERE_SECRET') }}" placeholder="{{ translate('PAYHERE SECRET') }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <input type="hidden" name="types[]" value="PAYHERE_CURRENCY">
                        <div class="col-lg-4">
                            <label class="control-label required">{{translate('PAYHERE CURRENCY')}}</label>
                        </div>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="PAYHERE_CURRENCY" value="{{  env('PAYHERE_CURRENCY') }}" placeholder="{{ translate('PAYHERE CURRENCY') }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label class="control-label required">{{translate('Payhere Sandbox Mode')}}</label>
                        </div>
                        <div class="col-lg-8">
                            <label class="switch">
                                <input value="1" name="payhere_sandbox" type="checkbox" @if (\App\BusinessSetting::where('type', 'payhere_sandbox')->first()->value == 1)
                                    checked
                                @endif>
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12 text-right">
                            <button class="btn btn-primary mr-1 mb-1" type="submit">{{translate('Save')}}</button>
                            <button type="reset" onclick="document.location.reload()" class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                        </div>
                    </div>
                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<!-- // Basic Horizontal form layout section end -->


<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->

  @endsection