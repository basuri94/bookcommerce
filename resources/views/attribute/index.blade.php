@extends('admin.layout.admin_template')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body">
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{ translate('Attributes')}}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">
                    <div class="row">
                            <div class="col-sm-12">
                            <a href="{{ route('attributes.create')}}" class="btn btn-rounded btn-info pull-right">{{ translate('Add New Attribute')}}</a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                        <th>SL#</th>
                                        <th>{{ translate('Name')}}</th>
                                        <th>{{ translate('Sub Category')}}</th>
                                        <th>{{ translate('Mandatory/Not Mandatory')}}</th>
                                        <th width="10%">{{ translate('Options')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                 
                                 @endphp
                                    @foreach($attributes as $key => $attribute)
                              
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$attribute->name}}</td>
                                            <td>{{$attribute->subname}}</td>
                                          
                                            <td><label class="switch">
                                                <input onchange="update_mandatory(this)" value="{{ $attribute->id }}" type="checkbox" <?php if($attribute->mandatory == 1) echo "checked";?> >
                                                <span class="slider round"></span></label>
                                            </td>
                                            <td>
                                                <div class="btn-group dropdown">
                                                    <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                        {{ translate('Actions')}} <i class="dropdown-caret"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="{{route('attributes.edit', encrypt($attribute->id))}}">{{ translate('Edit')}}</a></li>
                                                        <li><a onclick="confirm_modal('{{route('attributes.destroy', $attribute->id)}}');">{{ translate('Delete')}}</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                              <tfoot>
                                    <tr>
                                        <th>SL#</th>
                                        <th>{{ translate('Name')}}</th>
                                        <th width="10%">{{ translate('Options')}}</th>
                                    
                                    </tr>
                                </tfoot>
                            </table>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
</div>
</div>

@endsection

@section('script')
    <script type="text/javascript">
function update_mandatory (el){
    if(el.checked){
        var status = 1;
    }
    else{
        var status = 0;
    }
    $.post('{{ route('attributes.mandatory') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
        if(data == 1){
            Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Attribute Mandatory Status Changed successfully',
            showConfirmButton: false,
            timer: 1500
            });
        }
        else{
            Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'Something went wrong',
            showConfirmButton: false,
            timer: 1500
            });
        }
    });
}
</script>
@endsection