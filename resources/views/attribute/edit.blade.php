@extends('admin.layout.admin_template')
@section('content')

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">

        <div class="content-body">
            <!-- Basic Horizontal form layout section start -->
            <section id="basic-horizontal-layouts">
                <div class="row match-height">
                    <div class="col-md-12 col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">{{ translate('Attribute Information')}}</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <!--Horizontal Form-->
                                    <!--===================================================-->
                                    <form class="form-horizontal"
                                        action="{{ route('attributes.update', $attribute->id) }}" method="POST"
                                        enctype="multipart/form-data">
                                        <input name="_method" type="hidden" value="PATCH">
                                        @csrf
                                        <div class="form-body">
                                            <div class="row">

                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4"><span class="required">{{translate('Category')}}</span></div>
                                                       <div class="col-md-8">
                                                       <select  placeholder="{{ translate('Category')}}" id="categoryid" name="categoryid" class="form-control"
                                                       onchange="get_subcategories_by_category(this.value)" required>
                                                        <option value="">--Select Category--</option>
                                                        @foreach($Category as $key=> $catval)
                                                       <option value="{{ $catval->id}}"  @if($attribute->categoryid==$catval->id) selected @endif>{{ $catval->name}}</option>
                                                        @endforeach
                                
                                                       </select>
                                                </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4"><span
                                                            class="required">{{translate('Sub Category')}}</span></div>
                                                    <div class="col-md-8">
                                                        <select placeholder="{{ translate('Sub Category')}}"
                                                            id="sub_categoryid" name="sub_categoryid"
                                                            class="form-control" required>
                                                            <option value="">--Select Sub-Category--</option>
                                                          
                                                            {{-- @foreach($subcatgory as $key=> $subval)
                                                            <option value="{{ $subval->id}}" @if($attribute->sub_categoryid==$subval->id) selected @endif>{{ $subval->name}}</option>
                                                            @endforeach --}}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4"><span
                                                                class="required">{{translate('Name')}}</span></div>
                                                        <div class="col-md-8">
                                                            <input type="text" placeholder="{{ translate('Name')}}"
                                                                id="name" name="name" class="form-control" required
                                                                value="{{ $attribute->name }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <label class="col-md-2">{{ translate('Mandatory')}}</label>
                                                <div class="col-md-10">
                                                    <label class="switch" style="margin-top:5px;">
                                                        <input type="checkbox" name="mandatory" @if ($attribute->mandatory == 1) checked @endif>
                                                        <span class="slider round"></span></label>
                                                    </label>
                                                </div>


                                                <div class="col-md-8 offset-md-4">
                                                    <button type="submit"
                                                        class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                                    <button type="reset"
                                                        class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                                    <a href="{{route('attributes.index')}}"
                                                        class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>
                                                </div>

                                            </div>
                                        </div>
                                    </form>
                                    <!--===================================================-->
                                    <!--End Horizontal Form-->


                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <!-- // Basic Horizontal form layout section end -->


            <!-- // Basic Floating Label Form section end -->

        </div>
    </div>
</div>
<!-- END: Content-->

@endsection

@section('script')
<script>
    $(function(){
        get_subcategories_by_category({{$attribute->categoryid}});
    })
 function get_subcategories_by_category(cat_id){
           
            category_id = cat_id;
           
            $('#sub_categoryid').html('');
          
            $.post('{{ route('subcategories.get_subcategories_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){
                $('#sub_categoryid').append('<option value="">--Select Sub-Category--</option>');
                for (var i = 0; i < data.length; i++) {
                    $('#sub_categoryid').append('<option value="'+data[i].id+'">'+data[i].name+'</option>');
                }
               $('#sub_categoryid').val({{$attribute->sub_categoryid}}).trigger('change')
            });
        }

</script>


  @endsection
