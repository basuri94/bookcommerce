 <!-- BEGIN: Main Menu-->
 <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
  <div class="navbar-header">
    <ul class="nav navbar-nav flex-row">
      <li class="nav-item mr-auto"><a class="navbar-brand" href="#">
          <!-- <div class="brand-logo"></div> -->
          <img src="{{asset('assets/images/logo/logo-cosmatics.png')}}" style="width: 100%;">
          <!-- <h2 class="brand-text mb-0">Local2Vocal</h2> -->
        </a></li>
      <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="icon-disc"></i></a></li>
    </ul>
  </div>
  <div class="shadow-bottom"></div>
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

    <li class="nav-item {{ areActiveRoutesHome(['admin.dashboard'])}}">
                    <a href="{{ route('admin.dashboard') }}" class="{{ areActiveRoutesHome(['admin.dashboard'])}}">
                        <i class="fa fa-dashboard"></i>
                        <span class="category-name">
                            {{ translate('Dashboard')}}
                        </span>
                    </a>
                </li>
     
      
      @if(Auth::user()->user_type == 'admin' || in_array('1', json_decode(Auth::user()->staff->role->permissions)))
      <li class=" nav-item"><a href="#"><i class="fa fa-shopping-cart"></i><span class="menu-title" data-i18n="Ecommerce">{{translate('Products')}}</span></a>

        <ul class="menu-content ">
         
          <li class=" {{ areActiveRoutes(['brands.index', 'brands.create', 'brands.edit'])}}">
            <a href="{{route('brands.index')}}"><i class="feather icon-circle"></i>{{translate('Publishers')}}</a>
          </li>
          <li class="{{ areActiveRoutes(['categories.index', 'categories.create', 'categories.edit'])}}">
          <a href="{{route('categories.index')}}"><i class="feather icon-circle"></i>{{translate('Category')}}</a>
          </li>
          <li class="{{ areActiveRoutes(['subcategories.index', 'subcategories.create', 'subcategories.edit'])}}">
          <a href="{{route('subcategories.index')}}"><i class="feather icon-circle"></i>{{translate('Subcategory')}}</a>
          </li>

          <li class="{{ areActiveRoutes(['subsubcategories.index', 'subsubcategories.create', 'subsubcategories.edit'])}}">
          <a href="{{route('subsubcategories.index')}}"><i class="feather icon-circle"></i>{{translate('Sub Subcategory')}}</a>
          </li>
          <li class="{{ areActiveRoutes(['subcategoryhsns.index', 'subcategoryhsns.create', 'subcategoryhsns.edit'])}}">
          <a href="{{route('subcategoryhsns.index')}}"><i class="feather icon-circle"></i>HSN & GST</a>
          </li>
          <li class="{{ areActiveRoutes(['products.admin', 'products.create', 'products.admin.edit'])}}">
                                        <a class="nav-link" href="{{route('products.admin')}}"><i class="feather icon-circle"></i>{{translate('Book details')}}</a>
                                    </li>
         
          @if(\App\BusinessSetting::where('type', 'classified_product')->first()->value == 1)
          {{--<li class="{{ areActiveRoutes(['classified_products'])}}">
          <a href="{{route('classified_products')}}"><i class="feather icon-circle"></i>{{translate('Classified Products')}}</a>
          </li>--}}
          @endif
          <!-- <li class="{{ areActiveRoutes(['digitalproducts.index', 'digitalproducts.create', 'digitalproducts.edit'])}}">
          <a href="{{route('digitalproducts.index')}}"><i class="feather icon-circle"></i>{{translate('Digital Products')}}</a>
          </li> -->
          <li class="{{ areActiveRoutes(['product_bulk_upload.index'])}}">
          <a href="{{route('product_bulk_upload.index')}}"><i class="feather icon-circle"></i>{{translate('Bulk Import')}}</a>
          </li>
          <li class="{{ areActiveRoutes(['product_bulk_export.export'])}}">
          <a href="{{route('product_bulk_export.index')}}" onclick="return confirm('Are you sure to bulk export ?')"><i class="feather icon-circle"></i>{{translate('Bulk Export')}}</a>
          </li>
          @php
          $review_count = DB::table('reviews')
                      ->orderBy('code', 'desc')
                      ->join('products', 'products.id', '=', 'reviews.product_id')
                      ->where('products.user_id', Auth::user()->id)
                      ->where('reviews.viewed', 0)
                      ->select('reviews.id')
                      ->distinct()
                      ->count();

          @endphp
          <li class="{{ areActiveRoutes(['reviews.index'])}}">
          <a href="{{route('reviews.index')}}"><i class="feather icon-circle"></i>{{translate('Product Reviews')}}@if($review_count > 0)<span class="pull-right badge badge-primary">{{ $review_count }}</span>@endif</a>
          </li>

        </ul>
      </li>
      @endif



      @if(Auth::user()->user_type == 'admin' || in_array('2', json_decode(Auth::user()->staff->role->permissions)))
      <li class="nav-item {{ areActiveRoutes(['flash_deals.index', 'flash_deals.create', 'flash_deals.edit'])}}">
        <a href="{{ route('flash_deals.index') }}"><i class="fa fa-bolt"></i><span class="menu-title" data-i18n="Flash Deal">{{translate('Flash Deal')}}</span></a>
      </li>
      @endif
      @if(Auth::user()->user_type == 'admin' || in_array('2', json_decode(Auth::user()->staff->role->permissions)))
      <li class="nav-item {{ areActiveRoutes(['bank_details.index', 'bank_details.create', 'bank_details.edit'])}}">
        <a href="{{ route('bank_details.index') }}"><i class="fa fa-money"></i><span class="menu-title" data-i18n="Seller Bank Details">{{translate('Seller Bank Details')}}</span></a>
      </li>
      @endif
      @if(Auth::user()->user_type == 'admin' || in_array('3', json_decode(Auth::user()->staff->role->permissions)))
      @php
          $orders = DB::table('orders')
                      ->orderBy('code', 'desc')
                      ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                      ->where('order_details.seller_id', \App\User::where('user_type', 'admin')->first()->id)
                      ->where('orders.viewed', 0)
                      ->select('orders.id')
                      ->distinct()
                      ->count();
      @endphp
<li class="nav-item {{  areActiveRoutes(['orders.index.admin', 'orders.show'])}}">
  <a href="{{ route('orders.index.admin') }}">  <i class="fa fa-shopping-basket"></i>
    <span class="menu-title" data-i18n="Inhouse orders">{{translate('Inhouse orders')}}@if($orders > 0)<span class="pull-right badge badge-primary">{{ $orders }}</span>@endif</span>
   
  </a>
</li>
@endif

      @if(Auth::user()->user_type == 'admin' || in_array('4', json_decode(Auth::user()->staff->role->permissions)))
      @php 
      $totalorders = DB::table('orders')
                      ->orderBy('code', 'desc')
                      ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                      ->where('orders.viewed', 0)
                      ->select('orders.id')
                      ->distinct()
                      ->count();
      @endphp
      <li class="nav-item {{ areActiveRoutes(['sales.index', 'sales.show'])}}"><a href="{{ route('sales.index') }}"><i class="fa fa-money"></i><span class="menu-title" data-i18n="Total sales">{{translate('Total sales')}}@if($totalorders > 0)<span class="pull-right badge badge-primary">{{ $totalorders }}</span>@endif</span></a>
      </li>
      @endif


      @if((Auth::user()->user_type == 'admin' || in_array('5', json_decode(Auth::user()->staff->role->permissions))) && \App\BusinessSetting::where('type', 'vendor_system_activation')->first()->value == 1)
      <li class=" nav-item"><a href="#"><i class="fa fa-user-plus"></i><span class="menu-title" data-i18n="Sellers">{{translate('Sellers')}}</span></a>
        <ul class="menu-content">
        
          @php
          $sellers = \App\Seller::where('verification_status', 0)->count();
          //$withdraw_req = \App\SellerWithdrawRequest::where('viewed', '0')->get();
      @endphp
          <li class=" {{ areActiveRoutes(['sellers.index', 'sellers.create', 'sellers.edit', 'sellers.payment_history','sellers.approved','sellers.profile_modal'])}}">
            <a href="{{route('sellers.index')}}"><i class="feather icon-circle"></i>{{translate('Seller List')}}@if($sellers > 0)<span class="badge badge-info" data-i18n="Seller List">{{ $sellers }}</span>@endif</a>
          </li>
          @php
          $sellers_request = \App\SellerRequest::where('status', 0)->count();
      @endphp
<li class="nav-item {{areActiveRoutes(['sellers.seller_request'])}}">
  <a href="{{ route('sellers.seller_request') }}"><i class="fa fa-shopping-basket"></i>
    <span class="menu-title" data-i18n="Seller Request">{{translate('Category Request')}}@if($sellers_request > 0)<span class="pull-right badge badge-primary">{{ $sellers_request }}</span>@endif</span> 
  </a>
</li>
          <!-- <li class="{{ areActiveRoutes(['withdraw_requests_all'])}}"><a href="{{ route('withdraw_requests_all') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Seller Withdraw Requests">{{translate('Seller Withdraw Requests')}}</span></a>
          </li> -->
          <li class="{{ areActiveRoutes(['sellers.payment_histories'])}}"><a href="{{ route('sellers.payment_histories') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Seller Payments">{{translate('Seller Payments')}}</span></a>
          </li>
          <!-- <li class="{{ areActiveRoutes(['business_settings.vendor_commission'])}}"><a href="{{ route('business_settings.vendor_commission') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Seller Commission">{{translate('Seller Commission')}}</span></a>
          </li> -->
          <!-- <li class="{{ areActiveRoutes(['seller_verification_form.index'])}}"><a href="{{ route('seller_verification_form.index') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Seller Verification Form">{{translate('Seller Verification Form')}}</span></a>
          </li> -->
          @if (\App\Addon::where('unique_identifier', 'seller_subscription')->first() != null && \App\Addon::where('unique_identifier', 'seller_subscription')->first()->activated)
          <li class="{{ areActiveRoutes(['seller_packages.index', 'seller_packages.create', 'seller_packages.edit'])}}">
            <a href="{{ route('seller_packages.index') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Seller Packages">{{translate('Seller Packages')}}</span></a>
          </li>
          @endif
          @if(\App\BusinessSetting::where('type', 'vendor_system_activation')->first()->value == 1)
           <li class="{{ areActiveRoutes(['products.seller'])}}">
          <a href="{{route('products.seller')}}"><i class="feather icon-circle"></i>{{translate('Approved Products')}}</a>
          </li>
          @php
          $PendingSP_count = DB::table('products')
                      ->where('is_approve', 0)
                      ->select('id')
                      ->distinct()
                      ->count();
                      @endphp
          <li class="{{ areActiveRoutes(['products.seller_pending'])}}">
          <a href="{{route('products.seller_pending')}}"><i class="feather icon-circle"></i>{{translate('Pending Products')}} @if($PendingSP_count > 0)<span class="pull-right badge badge-primary">{{ $PendingSP_count }}</span>@endif</a>
          </li>

          @php
          $seller_strike_count = DB::table('products')->join('conversations','conversations.product_id','=','products.id')
                      ->where('is_approve', 0)
                      ->select('id')
                      ->distinct()
                      ->count();
                      @endphp
          <li class="{{ areActiveRoutes(['products.seller_strike'])}}">
          <a href="{{route('products.seller_strike')}}"><i class="feather icon-circle"></i>{{translate('Strike Products')}} @if($seller_strike_count > 0)<span class="pull-right badge badge-primary">{{ $seller_strike_count }}</span>@endif</a>
          </li>
          @endif
        </ul>
      </li>
      @endif
      @if(Auth::user()->user_type == 'admin' || in_array('6', json_decode(Auth::user()->staff->role->permissions)))
      <li class=" nav-item "><a href="#"><i class="fa fa-user-plus"></i>
        <span class="menu-title" data-i18n="Customers">{{translate('Customers')}}</span></a>
        <ul class="menu-content">
          <li class="{{ areActiveRoutes(['customers.index'])}}"><a href="{{ route('customers.index') }}">
            <i class="feather icon-circle"></i><span class="menu-item" data-i18n="Customer list">{{translate('Customer list')}}</span></a>
          </li>
   
        
        </ul>
      </li>
      @endif

      @php
      $conversation = \App\Conversation::where('receiver_viewed', '1')->count();
  @endphp
  @if(Auth::user()->user_type == 'admin')
  <li class="{{ areActiveRoutes(['conversations.admin_index', 'conversations.admin_show'])}}  nav-item">
      <a class="nav-link" href="{{ route('conversations.admin_index') }}">
          <i class="fa fa-comment"></i>
          <span class="menu-item" data-i18n="Conversations">{{translate('Conversations')}}</span>
          @if ($conversation > 0)
              <span class="pull-right badge badge-primary">{{ $conversation }}</span>
          @endif
      </a>
  </li>
@endif
      @if(Auth::user()->user_type == 'admin' || in_array('8', json_decode(Auth::user()->staff->role->permissions)))


      <li class=" nav-item "><a href="#"><i class="fa fa-briefcase"></i>
        <span class="menu-title" data-i18n="Business Settings">{{translate('Business Settings')}}</span></a>
        <ul class="menu-content">
          <li class="{{ areActiveRoutes(['activation.index'])}}"><a href="{{route('activation.index')}}">
            <i class="feather icon-circle"></i><span class="menu-item" data-i18n="Activation">{{translate('Activation')}}</span></a>
          </li>
          <li  class="{{ areActiveRoutes(['payment_method.index'])}}">
            <a href="{{ route('payment_method.index') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Payment method">
              {{translate('Payment method')}}</span></a>
          </li>
          <li  class="{{ areActiveRoutes(['SMS_Settings.index'])}}">
            <a href="{{ route('SMS_Settings.index') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="SMS Settings">
              {{translate('SMS Template Settings')}}</span></a>
          </li>
          <li  class="{{ areActiveRoutes(['smtp_settings.index'])}}">
            <a href="{{ route('smtp_settings.index') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="SMTP Settings">
              {{translate('SMTP Settings')}}</span></a>
          </li>
          <li  class="{{ areActiveRoutes(['google_analytics.index'])}}">
            <a href="{{ route('google_analytics.index') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Google Analytics">
              {{translate('Google Analytics')}}</span></a>
          </li>
          <li  class="{{ areActiveRoutes(['facebook_chat.index'])}}">
            <a href="{{ route('facebook_chat.index') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Facebook Chat & Pixel">
              {{translate('Facebook Chat & Pixel')}}</span></a>
          </li>
          <li  class="{{ areActiveRoutes(['social_login.index'])}}">
            <a href="{{ route('social_login.index') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Social Media Login">
              {{translate('Social Media Login')}}</span></a>
          </li>
          <!-- <li  class="{{ areActiveRoutes(['currency.index'])}}">
            <a href="{{ route('currency.index') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Currency">
              {{translate('Currency')}}</span></a>
          </li> -->
          <!-- <li  class="{{ areActiveRoutes(['languages.index', 'languages.create', 'languages.store', 'languages.show', 'languages.edit'])}}">
            <a href="{{ route('languages.index') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Languages">
              {{translate('Languages')}}</span></a>
          </li> -->
          <li  class="{{ areActiveRoutes(['getreferindex'])}}">
            <a href="{{ route('getreferindex') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Refund">
              {{translate('Refund')}}</span></a>
          </li>
          <li  class="{{ areActiveRoutes(['getsettelAmount'])}}">
            <a href="{{ route('getsettelAmount') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Refund">
              {{translate('Settlement Amount')}}</span></a>
          </li>
          <li class=" {{ areActiveRoutes(['email_template.index', 'email_template.create', 'email_template.edit'])}}">
            <a href="{{route('email_template.index')}}"><i class="feather icon-circle"></i>{{translate('Email Template')}}</a>
          </li>
          <li class=" {{ areActiveRoutes(['promotional_banner.index', 'promotional_banner.create', 'promotional_banner.edit'])}}">
            <a href="{{route('promotional_banner.index')}}"><i class="feather icon-circle"></i>{{translate('Promotional Banner')}}</a>
          </li>
        </ul>
      </li>

      @endif
      @if(Auth::user()->user_type == 'admin' || in_array('12', json_decode(Auth::user()->staff->role->permissions)))
                        <li>
                            <a href="#">
                                <i class="fa fa-gear"></i>
                                <span class="menu-title">{{translate('E-commerce Setup')}}</span>
                                <i class="arrow"></i>
                            </a>

                            <!--Submenu-->
                            <ul class="menu-content">
                                <li class="{{ areActiveRoutes(['attributes.index','attributes.create','attributes.edit'])}}">
                                    <a class="nav-link" href="{{route('attributes.index')}}">{{translate('Attribute')}}</a>
                                </li>
                               
                                <li class="{{ areActiveRoutes(['coupon.index','coupon.create','coupon.edit'])}}">
                                    <a class="nav-link" href="{{route('coupon.index')}}">{{translate('Coupon')}}</a>
                                </li>
                               
                                <li>
                                    <li class="{{ areActiveRoutes(['shipping_configuration.index','shipping_configuration.edit','shipping_configuration.update'])}}">
                                        <a class="nav-link" href="{{route('shipping_configuration.index')}}">{{translate('Shipping Configuration')}}</a>
                                    </li>
                                </li>

                                <li class="{{ areActiveRoutes(['Refers.index','Refers.create','Refers.edit'])}}">
                                    <a class="nav-link" href="{{route('Refers.index')}}">{{translate('Referral')}}</a>
                                </li>

                                <li class="{{ areActiveRoutes(['Band_Trust.index','Band_Trust.create','Band_Trust.edit'])}}">
                                    <a class="nav-link" href="{{route('Band_Trust.index')}}">{{translate('TL2V Assurance')}}</a>
                                </li>
                                
                            </ul>
                        </li>
                        @endif
      @if(Auth::user()->user_type == 'admin' || in_array('9', json_decode(Auth::user()->staff->role->permissions)))
      <li class=" nav-item "><a href="#"><i class="fa fa-desktop"></i>
        <span class="menu-title" data-i18n="Business Settings">{{translate('Frontend Settings')}}</span></a>
        <ul class="menu-content">          
      
                                <li class="{{ areActiveRoutes(['home_settings.index', 'home_banners.index', 'sliders.index', 'home_categories.index', 'home_banners.create', 'home_categories.create', 'home_categories.edit', 'sliders.create'])}}">
                                    <a class="nav-link" href="{{route('home_settings.index')}}"><i class="feather icon-circle"></i>{{translate('Home')}}</a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="menu-title"><i class="feather icon-circle"></i>{{translate('Policy Pages')}}</span>
                                        <i class="arrow"></i>
                                    </a>

                                    <!--Submenu-->
                                    <ul class="collapse">

                                        <li class="{{ areActiveRoutes(['sellerpolicy.index'])}}">
                                            <a class="nav-link" href="{{route('sellerpolicy.index', 'seller_policy')}}"><i class="feather icon-circle"></i>{{translate('Seller Policy')}}</a>
                                        </li>
                                        <li class="{{ areActiveRoutes(['returnpolicy.index'])}}">
                                            <a class="nav-link" href="{{route('returnpolicy.index', 'return_policy')}}"><i class="feather icon-circle"></i>{{translate('Return Policy')}}</a>
                                        </li>
                                        <li class="{{ areActiveRoutes(['supportpolicy.index'])}}">
                                            <a class="nav-link" href="{{route('supportpolicy.index', 'support_policy')}}"><i class="feather icon-circle"></i>{{translate('Support Policy')}}</a>
                                        </li>
                                        <li class="{{ areActiveRoutes(['terms.index'])}}">
                                            <a class="nav-link" href="{{route('terms.index', 'terms')}}"><i class="feather icon-circle"></i>{{translate('Terms & Conditions')}}</a>
                                        </li>
                                        <li class="{{ areActiveRoutes(['privacypolicy.index'])}}">
                                            <a class="nav-link" href="{{route('privacypolicy.index', 'privacy_policy')}}"><i class="feather icon-circle"></i>{{translate('Privacy Policy')}}</a>
                                        </li>
                                    </ul>

                                </li>
                                <li class="{{ areActiveRoutes(['pages.index', 'pages.create', 'pages.edit'])}}">
                                    <a class="nav-link" href="{{route('pages.index')}}"><i class="feather icon-circle"></i>{{translate('Custom Pages')}}</a>
                                </li>
                                <li class=" {{ areActiveRoutes(['faq.index', 'faq.store'])}}">
            <a href="{{route('faq.index')}}"><i class="feather icon-circle"></i>{{translate('FAQ Pages')}}</a>
          </li>
                                <li class="{{ areActiveRoutes(['generalsettings.index'])}}">
                                    <a class="nav-link" href="{{route('generalsettings.index')}}"><i class="feather icon-circle"></i>{{translate('General Settings')}}</a>
                                </li>
                                <li class="{{ areActiveRoutes(['generalsettings.logo'])}}">
                                    <a class="nav-link" href="{{route('generalsettings.logo')}}"><i class="feather icon-circle"></i>{{translate('Logo Settings')}}</a>
                                </li>
                                <!-- <li class="{{ areActiveRoutes(['generalsettings.color'])}}">
                                    <a class="nav-link" href="{{route('generalsettings.color')}}"><i class="feather icon-circle"></i>{{translate('Color Settings')}}</a>
                                </li> -->
                            </ul>
                        </li>
                        @endif
                        @if(Auth::user()->user_type == 'admin' || in_array('13', json_decode(Auth::user()->staff->role->permissions)))

                            @php
                                $support_ticket1 = DB::table('tickets')
                                ->join('users', 'users.id', '=', 'tickets.user_id')
                                            ->where('tickets.viewed', 0)
                                            ->where('users.user_type', 'customer')
                                            ->select('id')
                                            ->count();

                                $support_ticket2 = DB::table('tickets')
                                ->join('users', 'users.id', '=', 'tickets.user_id')
                                            ->where('tickets.viewed', 0)
                                            ->where('users.user_type', 'seller')
                                            ->select('id')
                                            ->count();
                                            $support_ticket3=$support_ticket1+$support_ticket2;        
                            @endphp
                        <li class="{{ areActiveRoutes(['support_ticket.admin_index', 'support_ticket.admin_show','support_ticket.admin_index_seller'])}}">
                            <a class="nav-link" href="javascript:">
                                <i class="fa fa-support"></i>
                                <span class="menu-title">{{translate('Support Ticket')}} @if($support_ticket3 > 0)<span class="pull-right badge badge-primary" style="margin-right:2%">{{ $support_ticket3 }}</span>@endif</span>
                            </a>
                            <ul class="menu-content">
                                <li class="{{ areActiveRoutes(['support_ticket.admin_index'])}}">
                                    <a class="nav-link" href="{{ route('support_ticket.admin_index') }}">{{translate('Customer')}} @if($support_ticket1 > 0)<span class="pull-right badge badge-primary" style="margin-right:2%">{{ $support_ticket1 }}</span>@endif</span></a>
                                </li>
                                <li class="{{ areActiveRoutes(['support_ticket.admin_index_seller'])}}">
                                    <a class="nav-link" href="{{ route('support_ticket.admin_index_seller') }}">{{translate('Seller')}}  @if($support_ticket2 > 0)<span class="pull-right badge badge-primary" style="margin-right:2%">{{ $support_ticket2 }}</span>@endif</span></a>
                                </li>
                            </ul>

                        </li>

                        



                        @endif


                        @if(Auth::user()->user_type == 'admin' || in_array('11', json_decode(Auth::user()->staff->role->permissions)))
                        <li class="{{ areActiveRoutes(['seosetting.index'])}}">
                            <a class="nav-link" href="{{ route('seosetting.index') }}">
                                <i class="fa fa-search"></i>
                                <span class="menu-title">{{translate('SEO Setting')}}</span>
                            </a>
                        </li>
                        @endif

                        @if(Auth::user()->user_type == 'admin' || in_array('10', json_decode(Auth::user()->staff->role->permissions)))
                        <li>
                            <a href="#">
                                <i class="fa fa-user"></i>
                                <span class="menu-title">{{translate('Staffs')}}</span>
                                <i class="arrow"></i>
                            </a>

                            <!--Submenu-->
                            <ul class="menu-content">
                                <li class="{{ areActiveRoutes(['staffs.index', 'staffs.create', 'staffs.edit'])}}">
                                    <a class="nav-link" href="{{ route('staffs.index') }}">{{translate('All staffs')}}</a>
                                </li>
                                <li class="{{ areActiveRoutes(['roles.index', 'roles.create', 'roles.edit'])}}">
                                    <a class="nav-link" href="{{route('roles.index')}}">{{translate('Staff permissions')}}</a>
                                </li>
                            </ul>
                        </li>
                        @endif
                        @if(Auth::user()->user_type == 'admin')
                        <li class="{{ areActiveRoutes(['adminfundlist'])}}  nav-item">
                            <a class="nav-link" href="{{ route('adminfundlist') }}">
                                <i class="fa fa-undo"></i>
                                <span class="menu-item" data-i18n="Refund/Return Request List">{{translate('Refund/Return Request List')}}</span>
                              
                            </a>
                        </li>
                      @endif
                      @if(Auth::user()->user_type == 'admin')
                      <li class="{{ areActiveRoutes(['newsletters.index'])}}  nav-item">
                          <a class="nav-link" href="{{ route('newsletters.index') }}">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                              <span class="menu-item" data-i18n="Newsletter">{{translate('Newsletter')}}</span>
                            
                          </a>
                      </li>
                    @endif
                        @if(Auth::user()->user_type == 'admin')
                        <li>
                            <a href="#">
                                <i class="fa fa-file"></i>
                                <span class="menu-title">{{translate('Reports')}}</span>
                                <i class="arrow"></i>
                            </a>

                            <!--Submenu-->
                            <ul class="menu-content">
                              <li class="{{ areActiveRoutes(['sales.budget'])}}">
                                <a class="nav-link" href="{{ route('sales.budget') }}">{{translate('Sales Ledger')}}</a>
                            </li>
                            <li class="{{ areActiveRoutes(['sales.vendor'])}}">
                              <a class="nav-link" href="{{ route('sales.vendor') }}">{{translate('Vendor Report')}}</a>
                          </li>
                           </li>
                            <li class="{{ areActiveRoutes(['sales.coupon'])}}">
                              <a class="nav-link" href="{{ route('sales.coupon') }}">{{translate('Coupon Report')}}</a>
                          </li>
                          <li class="{{ areActiveRoutes(['saler_refund'])}}">
                              <a class="nav-link" href="{{ route('saler_refund') }}">{{translate('Refund Report')}}</a>
                          </li>

                          <li class="{{ areActiveRoutes(['referral_report'])}}">
                            <a class="nav-link" href="{{ route('referral_report') }}">{{translate('Referral Report')}}</a>
                        </li>
                                <li class="{{ areActiveRoutes(['stock_report.index'])}}">
                                    <a class="nav-link" href="{{ route('stock_report.index') }}">{{translate('Stock Report')}}</a>
                                </li>
                                <li class="{{ areActiveRoutes(['in_house_sale_report.index'])}}">
                                    <a class="nav-link" href="{{ route('in_house_sale_report.index') }}">{{translate('In House Sale Report')}}</a>
                                </li>
                                <li class="{{ areActiveRoutes(['seller_report.index'])}}">
                                    <a class="nav-link" href="{{ route('seller_report.index') }}">{{translate('Seller Report')}}</a>
                                </li>
                                <li class="{{ areActiveRoutes(['seller_sale_report.index'])}}">
                                    <a class="nav-link" href="{{ route('seller_sale_report.index') }}">{{translate('Seller Based Selling Report')}}</a>
                                </li>
                                <li class="{{ areActiveRoutes(['wish_report.index'])}}">
                                    <a class="nav-link" href="{{ route('wish_report.index') }}">{{translate('Product Wish Report')}}</a>
                                </li>
                                 <li class="{{ areActiveRoutes(['seller_report_invoice'])}}">
                                    <a class="nav-link" href="{{ route('seller_report_invoice') }}">{{translate('Seller Invoice Generate')}}</a>
                                </li>
                            </ul>
                        </li>
     @endif
    
     
     
     
    </ul>
  </div>
</div>
<!-- END: Main Menu-->