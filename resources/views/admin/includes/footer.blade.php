<!-- Buynow Button-->
{{-- <div class="buy-now"><a href="https://1.envato.market/vuexy_admin" target="_blank" class="btn btn-danger">Buy Now</a>

</div> --}}
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
<footer class="footer footer-static footer-light">
<p class="clearfix blue-grey lighten-2 mb-0"><span class="float-md-left d-block d-md-inline-block mt-25">Copyright © TheLocal2Vocal</span><span class="float-md-right d-none d-md-block">Hand-crafted & Made with<i class="feather icon-heart pink"></i> by Social Planet</span>
    <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="feather icon-arrow-up"></i></button>
  </p>
</footer>
<!-- END: Footer-->


<!-- BEGIN: Vendor JS-->
<script src="{{ asset('front/app-assets/vendors/js/vendors.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>
<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

  <!--Select2 [ OPTIONAL ]-->
  <script src="{{ asset('front/app-assets/vendors/js/select2.min.js')}}"></script>
   <!--Bootstrap Tags Input [ OPTIONAL ]-->
   <script src="{{asset('front/app-assets/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="{{ asset('front/app-assets/vendors/js/charts/apexcharts.min.js')}}"></script>
<!-- BEGIN: Page Vendor JS-->
<script src="{{ asset('front/app-assets/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>
<script src="{{ asset('front/app-assets/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>
<script src="{{ asset('front/app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{ asset('front/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{ asset('front/app-assets/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
<script src="{{ asset('front/app-assets/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>
<script src="{{ asset('front/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js')}}"></script>
<script src="{{ asset('front/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('front/app-assets/js/scripts/pages/app-chat.min.js')}}"></script>
<!-- END: Page Vendor JS-->


<script src="{{ asset('front/app-assets/js/core/app-menu.min.js')}}"></script>

<script src="{{ asset('front/app-assets/js/scripts/datatables/datatable.min.js')}}"></script>
<script src="{{ asset('front/app-assets/js/scripts/modal/components-modal.min.js')}}"></script>
<script src="{{ asset('front/app-assets/vendors/js/extensions/sweetalert2.all.min.js')}}"></script>
<script src="{{ asset('front/app-assets/js/core/app.min.js')}}"></script>
<script src="{{ asset('front/app-assets/js/scripts/components.min.js')}}"></script>
<script src="{{ asset('front/app-assets/js/scripts/customizer.min.js')}}"></script>
<script src="{{ asset('front/app-assets/js/scripts/footer.min.js')}}"></script>
<script src="{{ asset('front/app-assets/js/scripts/pages/dashboard-ecommerce.min.js')}}"></script>
<script src="{{ asset('front/app-assets/js/scripts/pages/app-ecommerce-shop.min.js')}}"></script>
<script src="{{ asset('front/app-assets/js/spartan-multi-image-picker-min.js')}}"></script>






<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>

<!-- END: Page JS-->
@foreach (session('flash_notification', collect())->toArray() as $message)
<script type="text/javascript">
       $(function() {
    Swal.fire({
position: 'center',
icon: 'success',
title: "{{ $message['message'] }}",
showConfirmButton: false,
timer: 1500
});

       });     
</script>
@endforeach
