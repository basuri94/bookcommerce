<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
  <div class="navbar-wrapper">
    <div class="navbar-container content">
      <div class="navbar-collapse" id="navbar-mobile">
        <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
          <ul class="nav navbar-nav">
            <li class="nav-item mobile-menu d-xl-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ficon feather icon-menu"></i></a></li>
          </ul>
          <ul class="nav navbar-nav bookmark-icons">
            <!-- li.nav-item.mobile-menu.d-xl-none.mr-auto-->
            <!--   a.nav-link.nav-menu-main.menu-toggle.hidden-xs(href='#')-->
            <!--     i.ficon.feather.icon-menu-->
            </ul>
          <ul class="nav navbar-nav">
            <li class="nav-item d-none d-lg-block"></a>
              
              <!-- select.bookmark-select-->
              <!--   option Chat-->
              <!--   option email-->
              <!--   option todo-->
              <!--   option Calendar-->
            </li>
          </ul>
        </div>
        <ul class="nav navbar-nav float-right">
          @php
          $orders = DB::table('orders')
                      ->orderBy('code', 'desc')
                      ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                      ->where('order_details.seller_id', \App\User::where('user_type', 'admin')->first()->id)
                      ->where('orders.viewed', 0)
                      ->select('orders.id')
                      ->distinct()
                      ->count();
                      $totalorders = DB::table('orders')
                      ->orderBy('code', 'desc')
                      ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                      ->where('orders.viewed', 0)
                      ->select('orders.id')
                      ->distinct()
                      ->count();
          $sellers = \App\Seller::where('verification_status', 0)->count();
          $sellers_request = \App\SellerRequest::where('status', 0)->count();
          $PSP_count = DB::table('products')
                      ->where('is_approve', 0)
                      ->select('id')
                      ->distinct()
                      ->count();

        $conversation_new = \App\Conversation::where('receiver_viewed', '1')->count();
      @endphp
          <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i class="ficon feather icon-maximize"></i></a></li>
          
          <li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="javascript:" data-toggle="dropdown"><i class="ficon feather icon-bell"></i><span class="badge badge-pill badge-primary badge-up">{{ $orders + $totalorders + $sellers + $sellers_request + $PSP_count +  $conversation_new }}</span></a>
            <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
            
            @if($orders==0 && $sellers==0)
            <li class="dropdown-menu-footer"><a class="dropdown-item p-1 text-center" style="color:red;" href="javascript:">No Record Found.</a></li>

            @else
            @if($orders==0 )
            <!-- <li class="dropdown-menu-footer"><a class="dropdown-item p-1 text-center" style="color:red;" href="javascript:">In Orders No Record.</a></li> -->

            @else 
              @if($orders > 0)
              <li class="dropdown-menu-header">
                <div class="dropdown-header m-0">
                  <a href ="{{ route('orders.index.admin') }}" ><h6 class="white">{{$orders}} New</h6><span class="notification-title"> {{translate(' new order(s)')}}</span></a>
                </div>
              </li>
              @endif
              @endif

             @if($totalorders>0)
              <li class="dropdown-menu-header">
                <div class="dropdown-header m-0">
                  <a href ="{{ route('sales.index') }}" ><h6 class="white">{{$totalorders}} New</h6><span class="notification-title"> {{translate(' new order(s)')}}</span></a>
                </div>
              </li>
             

@endif

              @if($sellers==0 )
            <li class="dropdown-menu-footer"><a class="dropdown-item p-1 text-center" style="color:red;" href="javascript:">In Sellers No Record.</a></li>

            @else 
              @if($sellers > 0)
              <li class="dropdown-menu-header">
                <div class="dropdown-header m-0">
                  <a href ="{{ route('sellers.index') }}" ><h6 class="white">{{$sellers}} New</h6><span class="notification-title"> {{translate('New verification request(s)')}}</span></a>
                </div>
              </li>
              @endif
              @endif
              @endif
              
              @if($conversation_new > 0)
              <li class="dropdown-menu-header">
                <div class="dropdown-header m-0">
                  <a href ="{{ route('conversations.admin_index') }}" ><h6 class="white">{{$conversation_new}} New</h6><span class="notification-title"> {{translate('New Conversations')}}</span></a>
                </div>
              </li>
              @endif
              @if($PSP_count > 0)
              <li class="dropdown-menu-header">
                <div class="dropdown-header m-0">
                  <a href ="{{ route('products.seller_pending') }}" ><h6 class="white">{{$PSP_count}} New</h6><span class="notification-title"> {{translate('New Pending Seller Products')}}</span></a>
                </div>
              </li>
              @endif
              @php
          $seller_strike_count = DB::table('products')->join('conversations','conversations.product_id','=','products.id')
                      ->where('is_approve', 0)
                      ->select('id')
                      ->distinct()
                      ->count();
                      @endphp
                      @if($seller_strike_count > 0)
              <li class="dropdown-menu-header">
                <div class="dropdown-header m-0">
                  <a href ="{{ route('products.seller_strike') }}" ><h6 class="white">{{$seller_strike_count}} New</h6><span class="notification-title"> {{translate('New Pending Strike Products')}}</span></a>
                </div>
              </li>
              @endif
              @if($sellers_request > 0)
              <li class="dropdown-menu-header">
                <div class="dropdown-header m-0">
                  <a href ="{{ route('sellers.seller_request') }}" ><h6 class="white">{{$sellers_request}} New</h6><span class="notification-title"> {{translate('New Category Request')}}</span></a>
                </div>
              </li>
              @endif
              {{-- <li class="dropdown-menu-footer"><a class="dropdown-item p-1 text-center" href="javascript:void(0)">Read all notifications</a></li> --}}
            </ul>
          </li>
          <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
              <div class="user-nav d-sm-flex d-none"><span class="user-name text-bold-600">{{Auth::user()->name}}</span><span class="user-status"></span></div>
              <span>
              @if (file_exists(Auth::user()->avatar_original)) 
              <img class="round" src="{{ asset(Auth::user()->avatar_original)}}" alt="avatar" height="40" width="40">
              @endif
              </span></a>
            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="{{ url('/admin/profile') }}"><i class="feather icon-user"></i> Edit Profile</a>
              <div class="dropdown-divider"></div><a class="dropdown-item" href="/logout"><i class="feather icon-power"></i> Logout</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</nav>

<!-- END: Header-->