<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>{{ config('app.name') }}</title>
    <link rel="apple-touch-icon" href="{{ asset('front/app-assets/images/ico/apple-icon-120.html')}}">
    <link type="image/x-icon" href="{{ asset(\App\GeneralSetting::first()->favicon) }}" rel="shortcut icon" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/vendors/css/vendors.min.css')}}">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />
 
 
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/vendors/css/charts/apexcharts.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/bootstrap-extended.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/colors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/components.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/themes/dark-layout.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/themes/semi-dark-layout.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/vendors/css/extensions/sweetalert2.min.css')}}">

       <!--Select2 [ OPTIONAL ]-->
       <link href="{{asset('front/app-assets/css/select2.min.css')}}" rel="stylesheet">
        
<!--Bootstrap Tags Input [ OPTIONAL ]-->
<link href="{{asset('front/app-assets/bootstrap-tagsinput/bootstrap-tagsinput.min.css')}}" rel="stylesheet">
<!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/core/menu/menu-types/vertical-menu.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/core/colors/palette-gradient.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/pages/dashboard-ecommerce.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/pages/app-ecommerce-shop.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/pages/card-analytics.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/pages/app-chat.min.css')}}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('front/assets/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/custom.css')}}">
    <!-- END: Custom CSS-->











    
    <style>
   .dropdown-menu  > li{
    padding: 5px 0px 10px 10px !important;
    font-size: 15px !important;
    border-bottom: 1px solid #e4dae4 !important;
   
   }  
   .dropdown-menu {
    box-shadow: rgba(0, 0, 0, 0.47) -1px 5px 9px 4px !important;
   } 
   /*.dropdown-menu  > li> a:hover{ 
       background: #7367F0;
    color: #FFF!important;
   }
   .dropdown-menu  > li:hover{ 
       background: #7367F0;
    color: #FFF!important;
   }*/
   </style>
   <style>
      #navcontainer ul
      {
      margin: 0;
      padding: 0;
      list-style-type: none;
      text-align: center;
      }

      #navcontainer ul li { display: inline; }

      #navcontainer ul li a
      {
      text-decoration: none;
      padding: .2em 1em;
      color: #fff;
      background-color: #036;
      }

      #navcontainer ul li a:hover
      {
      color: #fff;
      background-color: #369;
      }
   </style>
<script type="text/javascript">
function showAlert(icon,msg){ 
          Swal.fire({
            position: 'center',
            icon: icon,
            title: msg,
            showConfirmButton: false,
            timer: 1500
          });
       }

       </script>