     
@extends('admin.layout.admin_template')
@section('content')
 
 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
     
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
  <div class="row match-height">
      <div class="col-md-12 col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">{{translate('Bank Details Information')}}</h4>
              </div>
              <div class="card-content">
                  <div class="card-body">
  <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('bank_details.update', $seller->id) }}" id="myform" method="POST" enctype="multipart/form-data">
        <input name="_method" type="hidden" value="PATCH">
            @csrf
            <div class="form-body">
                <div class="row">
                    <div class="col-md-2">
                        <label>{{ translate('Seller Name')}}</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" class="form-control mb-3 seller_name"
                            placeholder="{{ translate('Bank Account Number')}}"
                            value="{{ $seller->user->name }}" disabled name="seller_name"
                             required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label>{{ translate('Benificiary Name')}}</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" class="form-control mb-3"
                            placeholder="{{ translate('Bank Account Number')}}"
                            value="{{ $seller->benificiary_name }}" name="benificiary_name"
                             required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label>{{ translate('Bank Account Number')}}</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" class="form-control mb-3"
                            placeholder="{{ translate('Bank Account Number')}}"
                            value="{{ $seller->bank_acc_no }}"  name="bank_acc_no"
                            required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label>{{ translate('IFSC Code')}}</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" class="form-control mb-3" placeholder="{{ translate('IFSC Code')}}"
                            value="{{ $seller->ifsc_code }}"  name="ifsc_code" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label>{{ translate('Branch Name')}}</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" class="form-control mb-3" placeholder="{{ translate('Branch Name')}}"
                            value="{{ $seller->bname }}"  name="bname" required>
                    </div>
                </div>

                   
				 <div class="col-md-8 offset-md-4">
                                      <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Update')}}</button>
                                      <button type="button" id="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                      <a href="{{route('bank_details.index')}}" class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>
                                  </div>
								  
           
             </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->


                  </div>
              </div>
          </div>
      </div>
      
  </div>
</section>
<!-- // Basic Horizontal form layout section end -->


<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->

  @endsection
  @section('script')
  <script type="text/javascript">
      $(document).ready(function() {
    
        $('#reset').click(function() {
        $(':input','#myform')
            .not(':button,.seller_name, :submit, :reset, :hidden')
            .val('')
            .removeAttr('checked')
            .removeAttr('selected');
    });
  
      });
   
  </script>
  
  
    @endsection

