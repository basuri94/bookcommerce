@extends('admin.layout.admin_template')
@section('content')

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- Nav Filled Starts -->
<section id="nav-filled">
    <div class="row">
      <div class="col-sm-12">
        <div class="card overflow-hidden">
          <div class="card-header">
            <h4 class="card-title">{{translate('Return/Refund Request')}}</h4>
          </div>
          <div class="card-content">
            <div class="card-body">
              @csrf
              <!-- Nav tabs -->
              <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="home-tab-fill" data-toggle="tab" href="#home-fill" role="tab"
                    aria-controls="home-fill" aria-selected="true">{{translate('Your Return/Refund Request')}}</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="profile-tab-fill" data-toggle="tab" href="#profile-fill" role="tab"
                    aria-controls="profile-fill" aria-selected="false">{{translate('Seller Return/Refund Request')}}</a>
                </li>
               
              </ul>
  
              <!-- Tab panes -->
              <div class="tab-content pt-1">
                <div class="tab-pane active" id="home-fill" role="tabpanel" aria-labelledby="home-tab-fill">
                    <div class="table-responsive">
                        <table class="table zero-configuration">
                            <thead>
                                <tr>
                                    <th>{{ translate('#')}}</th>
                                    {{-- <th>{{ translate('Type')}}</th> --}}
                                    <th>{{ translate('Date')}}</th>
                                    <th>{{ translate('Order Id')}}</th>
                                    <th>{{ translate('Product')}}</th>
                                    <th>{{ translate('Amount')}}</th>

                                    <th>{{ translate('Status')}}</th>
                                    <th width="10%">{{translate('Options')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($adminfund as $key => $adminfund_list)

                                <tr>
                                    <td>{{ ($key+1) + ($adminfund->currentPage() - 1)*$adminfund->perPage() }}
                                    </td>
                                    {{-- <td>{{$adminfund_list->type}}</td> --}}
                                    <td>
                                        {{ date('d-m-Y', strtotime($adminfund_list->created_at))}}
                                    </td>
                                    <td>{{$adminfund_list->code}}</td>
                                    <td>{{$adminfund_list->name}}</td>
                                    <td>{{$adminfund_list->amount}}</td>
                                    <td>


                                        @if ($adminfund_list->status === "Pending")
                                        <span style="color:red">{{ translate('Pending')}}</span>
                                        @else
                                        <span style="color:green">
                                            {{ translate('Approved')}}</span>
                                        @endif

                                    </td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn" type="button"
                                                id="dropdownMenuButton-{{ $key }}"
                                                data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </button>

                                            <div class="dropdown-menu dropdown-menu-right"
                                                aria-labelledby="dropdownMenuButton-{{ $key }}">
                                                <button
                                                    onclick="adminrefundview({{$adminfund_list->id}});"
                                                    class="dropdown-item">{{ translate('View')}}</button>


                                            </div>
                                        </div>
                                    </td>

                                </tr>

                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>{{ translate('#')}}</th>
                                    {{-- <th>{{ translate('Type')}}</th> --}}
                                    <th>{{ translate('Date')}}</th>
                                    <th>{{ translate('Order Id')}}</th>
                                    <th>{{ translate('Product')}}</th>
                                    <th>{{ translate('Amount')}}</th>

                                    <th>{{ translate('Status')}}</th>
                                    <th width="10%">{{translate('Options')}}</th>
                                </tr>
                            </tfoot>
                        </table>
                        <div class="clearfix">
                            <div class="pull-right">
                                {{ $adminfund->appends(request()->input())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="profile-fill" role="tabpanel" aria-labelledby="profile-tab-fill">
                    <div class="table-responsive">
                        <table class="table zero-configuration">
                            <thead>
                                <tr>
                                    <th>{{ translate('#')}}</th>
                                    {{-- <th>{{ translate('Type')}}</th> --}}
                                    <th>{{ translate('Date')}}</th>
                                    <th>{{ translate('Order Id')}}</th>
                                    <th>{{ translate('Product')}}</th>
                                    <th>{{ translate('Amount')}}</th>

                                    <th>{{ translate('Status')}}</th>
                                    <th width="10%">{{translate('Options')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($allrefund as $key => $allrefund_list)

                                <tr>
                                    <td>{{ ($key+1) + ($allrefund->currentPage() - 1)*$allrefund->perPage() }}
                                    </td>
                                    {{-- <td>{{$allrefund_list->type}}</td> --}}
                                    <td>
                                        {{ date('d-m-Y', strtotime($allrefund_list->created_at))}}
                                    </td>
                                    <td>{{$allrefund_list->code}}</td>
                                    <td>{{$allrefund_list->name}}</td>
                                    <td>{{$allrefund_list->amount}}</td>
                                    <td>


                                        @if ($allrefund_list->status === "Pending")
                                        <span style="color:red">{{ translate('Pending')}}</span>
                                        @else
                                        <span style="color:green">
                                            {{ translate('Approved')}}</span>
                                        @endif

                                    </td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn" type="button"
                                                id="dropdownMenuButton-{{ $key }}"
                                                data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                                <i class="fa fa-ellipsis-v"></i>
                                            </button>

                                            <div class="dropdown-menu dropdown-menu-right"
                                                aria-labelledby="dropdownMenuButton-{{ $key }}">
                                                <button
                                                    onclick="adminrefundview({{$allrefund_list->id}});"
                                                    class="dropdown-item">{{ translate('View')}}</button>


                                            </div>
                                        </div>
                                    </td>

                                </tr>

                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>{{ translate('#')}}</th>
                                    {{-- <th>{{ translate('Type')}}</th> --}}
                                    <th>{{ translate('Date')}}</th>
                                    <th>{{ translate('Order Id')}}</th>
                                    <th>{{ translate('Product')}}</th>
                                    <th>{{ translate('Amount')}}</th>

                                    <th>{{ translate('Status')}}</th>
                                    <th width="10%">{{translate('Options')}}</th>
                                </tr>
                            </tfoot>
                        </table>
                        <div class="clearfix">
                            <div class="pull-right">
                                {{ $allrefund->appends(request()->input())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Nav Filled Ends -->
           

    </div>
</div>
</div>
<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="refundviewmodal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Refund View</h5>
                {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"> --}}
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="refund_body">
                ...
            </div>
            <div class="modal-footer" id="footer_id">

            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{asset('front/app-assets/js/scripts/navs/navs.min.js')}}"></script>
<script type="text/javascript">
    function adminrefundview(refundid){
            $.ajax({
                    type: 'post',
                    url: '{{route("adminrefundview")}}',
                    data: {'refundid': refundid, '_token': $('input[name="_token"]').val()},
                    dataType: 'json',
                    success: function (response) {
                        $("#refundviewmodal").modal('show');
                        $('#refund_body').html(response.html)
                        $('#footer_id').html(response.footer)
                        

                    },
                 
                });
        }

        function adminfundapprove(){
            var refundid=$('#refundid').val();
            
            Swal.fire({
            title: "Are you sure you want to approve refund?",
            type: "",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        }).then((result) => {
          if (result.value) {
            $.ajax({
                    type: 'post',
                    url: '{{route("adminfundapprove")}}',
                    data: {'refundid': refundid, '_token': $('input[name="_token"]').val()},
                    dataType: 'json',
                    success: function (response) {
                         
                        Swal.fire({
                       // position: 'top-end',
                        icon: 'success',
                        title: 'Refund Approved Successfully',
                        showConfirmButton: true,
                        
                        }).then(function (e) {
                        console.log(e)
                        if (e.value === true) {
                        location.reload();

                        }

                    },)
                     


                    },
                 
                });
          }
         
        });
           
        }
</script>
@endsection