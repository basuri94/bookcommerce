<!DOCTYPE html>

<html class="loading" lang="en" data-textdirection="ltr">
<head>
    @include('admin.includes.head')
  
    

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 2-columns  navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
    @include('admin.includes.header')
    <style>
      .required:after {
        content:" *";
        color: red;
      }
   

/* Style the tab */
.tab {
    float: left;
    border: 1px solid #ccc;
    background-color: #f5efef;
    width: 30%;
    height: auto;
}

/* Style the buttons inside the tab */
.tab a {
	display: block;
    background-color: inherit;
    color: #565656;
    padding: 10px 16px;
    width: 100%;
    border: none;
    outline: none;
    text-align: left;
    cursor: pointer;
    transition: 0.3s;
    font-size: 17px;
    border-bottom: 1px solid #c5d2de;
}

/* Change background color of buttons on hover */
.tab a:hover {
	background: linear-gradient(45deg, #766af0, #b4afe6);
    color: white !important;
}

/* Create an active/current "tab button" class */
.tab a.active {
	background: linear-gradient(45deg, #766af0, #b4afe6);
    color: white;
}

/* Style the tab content */
.tabcontent {
	float: left;
    padding: 23px 12px;
    border: 1px solid #8b82f3;
    width: 70%;
    border-left: none;
    height: auto;
}
.img-md{
  width: 75px !important;
}
.view_pdf_up{
  padding: 5px;
    background-color: #766cd6;
    margin-top: 10px;
    position: relative;
    border-radius: 5px;
    box-shadow: 2px 2px 3px 0px #000000b3;
    border: 1px solid #766cd6;
    
}
.view_pdf_up a{
  color: white;
}
.bootstrap-tagsinput .tag {
    border-radius: 2px;
    display: inline-block;
    font-size: 13px;
    font-weight: normal;
    margin: 0 2px 5px 0;
    padding: 8px;
    background-color: #766cd6;
    color: white;
}
.img-upload-preview img{
  width: 63px;
}
.pad-rgt{
  padding-right: 15px;
}
.pull-right.clearfix{
  float: left !important;
}
.badge-info{
  float: right;
    border-radius: 13%;
    padding: 5px;
    margin-right: 7px;
}
.badge.badge-info {
    background-color: #5a3d67;
}
.label-info {
    background-color: #038ece;
    color:white;
}
.label-table {
    display: inline-block;
    width: 80%;
    min-width: 8ex;
    font-size: 1em;
    max-width: 100px;
    padding: 5px;
    text-overflow: ellipsis;
    overflow: hidden;
    vertical-align: top;
    border-radius: 3px;
    font-weight: 600;
    text-align: center;
}
.btn-info {
    border-color: #00A1B5!important;
    background-color: #8b82f3!important;
    color: #FFF;
    padding: 6px;
    border-radius: 5px;
}
.select{
  margin: 2px;
}
.panel {
  border: 0;
    border-radius: 12px;
    box-shadow: none !important;
    margin-bottom: 20px;
    background: #e5e4e8;
}
.panel .panel-heading, .panel>:first-child {
    border-top-left-radius: 2px;
    border-top-right-radius: 2px;
}
.panel-heading {
    position: relative;
    height: 42px;
    padding: 0;
    color: #4d627b;
}
.panel-title {
    font-weight: normal;
    padding: 0 20px 0 20px;
    font-size: 1.15em;
    line-height: 42px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
}
.panel .panel-footer, .panel>:last-child {
    border-bottom-left-radius: 2px;
    border-bottom-right-radius: 2px;
}

.panel-body {
    padding: 15px 20px 25px;
}
.media-body{
  padding-left: 11px;
}

</style>
    @include('admin.includes.sidebar')

    @yield('content')
    @include('admin.includes.customizer')
    @include('admin.includes.footer') 
    
    @include('partials.modal')

    @yield('script')


</body>
</html>