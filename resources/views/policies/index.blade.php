     
@extends('admin.layout.admin_template')
@section('content')

 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h2 class="content-header-title float-left mb-0">{{ ucfirst(str_replace('_', ' ',$policy->name))}}</h2>
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="javascript:">{{translate('Home')}}</a>
                  </li>
                  <li class="breadcrumb-item"><a href="#">{{translate('Privacy pages')}}</a>
                  </li>
                  <li class="breadcrumb-item active">{{ ucfirst(str_replace('_', ' ',$policy->name))}}
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--<div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
          <div class="form-group breadcrum-right">
            <div class="dropdown">
              <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>
              <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a></div>
            </div>
          </div>
        </div>-->
      </div>
      <div class="content-body">




<!-- full Editor start -->
<section class="full-editor">
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
      <h4 class="card-title">{{ ucfirst(str_replace('_', ' ',$policy->name))}}</h4>
      </div>
      <div class="card-content collapse show">
        <div class="card-body">
            <form class="form-horizontal" action="{{ route('policies.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="name" value="{{ $policy->name }}">
          <div class="row">
            <div class="col-12">
              <div class="form-group row">
                  <div class="col-md-2">
<span>{{translate('Content')}} :</span>
</div>
                  <div class="col-md-10">
                    <textarea name="content" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$policy->content}}</textarea>
                  </div>
              </div>
          </div>
         
          </div>
          <div class="row">
            <div class="col-sm-12">
          <div class="col-md-8 offset-md-4">
            <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
            <button type="reset" onclick="document.location.reload()" class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
        </div>
    </div>
</div>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<!-- full Editor end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->

  @endsection
@section('script')
  <script>
var editor1 = CKEDITOR.replace('content', {
      extraAllowedContent: 'div(*);h3(*);img(*);h1(*);button(*);h2(*);h4(*);ul(*);a(*);section(*);li(*);span(*);p(*)',
      height: 400,
      filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
      filebrowserUploadMethod: 'form'
    });
    editor1.on('instanceReady', function() {
      // Output self-closing tags the HTML4 way, like <br>.
      this.dataProcessor.writer.selfClosingEnd = '>';

      // Use line breaks for block elements, tables, and lists.
      var dtd = CKEDITOR.dtd;
      for (var e in CKEDITOR.tools.extend({}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent)) {
        this.dataProcessor.writer.setRules(e, {
          indent: true,
          breakBeforeOpen: true,
          breakAfterOpen: true,
          breakBeforeClose: true,
          breakAfterClose: true
        });
      }
      // Start in source mode.
      //this.setMode('source');
    });


  </script>
  @stop

