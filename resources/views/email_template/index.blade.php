@extends('admin.layout.admin_template')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body">
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{translate('Email Template')}}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">
                    {{-- <div class="row">
                            <div class="col-sm-12">
                                <a href="{{ route('email_template.create')}}" class="btn btn-rounded btn-info pull-right">{{translate('Add New Email Template')}}</a>
                            </div>
                        </div> --}}
                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{translate('Title')}}</th>
                                        <th>{{translate('Subject')}}</th>
                                        <th width="10%">{{translate('Options')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($e_template as $key => $email_temp)
                                        <tr>
                                            <td>{{ ($key+1) }}</td>
                                            <td>{{$email_temp->title}}</td>
                                            <td>{{$email_temp->subject}}</td>
                                            <td>
                                                <div class="btn-group dropdown">
                                                    <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                        {{translate('Actions')}} <i class="dropdown-caret"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="{{route('email_template.edit', encrypt($email_temp->id))}}">{{translate('Edit')}}</a></li> 
                                                        <li><a onclick="confirm_modal('{{route('email_template.destroy', $email_temp->id)}}');">{{translate('Delete')}}</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                              <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>{{translate('Slug')}}</th>
                                        <th>{{translate('Subject')}}</th>
                                        <th width="10%">{{translate('Options')}}</th>
                                    </tr>
                                </tfoot>
                            </table>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
</div>
</div>

@endsection
@section('script')
    <script type="text/javascript">
        function sort_email_temps(el){
            $('#sort_email_temps').submit();
        }
    </script>
@endsection