@extends('admin.layout.admin_template')
@section('content')

<!-- BEGIN: Content-->
<div class="app-content content">
  <div class="content-overlay"></div>
  <div class="header-navbar-shadow"></div>
  <div class="content-wrapper">

    <div class="content-body">
      <!-- Basic Horizontal form layout section start -->
      <section id="basic-horizontal-layouts">
        <div class="row match-height">
          <div class="col-md-12 col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">{{translate('Email Template Details')}}</h4>
              </div>
              <div class="card-content">
                <div class="card-body">
                  <!--Horizontal Form-->
                  <!--===================================================-->
                  <form class="form-horizontal" action="{{ route('email_template.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">
                      <div class="row">
                      
                        <div class="col-12">
                          <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Slug')}}</span></div>
                            <div class="col-md-8">
                              <input type="text" placeholder="{{translate('Slug')}}" id="slug" name="slug" class="form-control" required>
                              
                            </div>
                          </div>
                        </div>
                       
                        <div class="col-12">
                          <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Subject')}}</span></div>
                            <div class="col-md-8">
                              <input type="text" class="form-control" name="subject" id="subject" placeholder="{{translate('Subject')}}" required> 
                            </div>
                          </div>
                        </div>
                        <div class="col-12">
                          <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Mail Body')}}</span></div>
                            <div class="col-md-8">
                            <textarea class="editor" name="mail_body" id="mail_body" required></textarea>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-8 offset-md-4">
                          <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                          <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                          <a href="{{route('email_template.index')}}"
                            class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>
                        </div>

                      </div>
                    </div>
                  </form>
                  <!--===================================================-->
                  <!--End Horizontal Form-->
                </div>
              </div>
            </div>
          </div>

        </div>
      </section>
     
    </div>
  </div>
</div>
<!-- END: Content-->

@endsection
@section('script')
<script>
var editor1 = CKEDITOR.replace('mail_body', {
      extraAllowedContent: 'div(*);h3(*);img(*);h1(*);button(*);h2(*);h4(*);ul(*);a(*);section(*);li(*);span(*);p(*)',
      height: 400,
      filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
      filebrowserUploadMethod: 'form'
    
    });
    editor1.on('instanceReady', function() {
      // Output self-closing tags the HTML4 way, like <br>.
      this.dataProcessor.writer.selfClosingEnd = '>';

      // Use line breaks for block elements, tables, and lists.
      var dtd = CKEDITOR.dtd;
      for (var e in CKEDITOR.tools.extend({}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent)) {
        this.dataProcessor.writer.setRules(e, {
          indent: true,
          breakBeforeOpen: true,
          breakAfterOpen: true,
          breakBeforeClose: true,
          breakAfterClose: true
        });
      }
      // Start in source mode.
      //this.setMode('source');
    });


  </script>
  @stop
