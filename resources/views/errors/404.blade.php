@extends('layouts.blank_404')

@section('content')
<!-- <img src="{{ asset('frontend/images/Eyes.png')}}" alt=""
                                style="width:100%;height: -webkit-fill-available;" /> -->
<div class="text-center"  >

    <h1 class="error-code text-danger"><img src="{{ asset('frontend/images/Eyes.png')}}" alt="" style="width:30%;" /> </h1>
    <p class="h4 text-uppercase text-bold text_404">{{translate('404')}}</p>
    <div class="pad-btm desc_404">
    {{translate("ahhhhh....")}}{{translate("Got lost")}}?{{translate(" How")}}? {{translate("Why")}}?
    </div>
    <?php 
                                $subDomain = request()->getHttpHost();
     
                                if ($subDomain == env('APP_SUBDOMAIN')) {
                                    ?>
    <div class="pad-top"><a class="btn btn-primary btn_404"  onclick="history.go(-1)">{{translate('Back To Previous')}}</a></div>
                                <?php }else{ ?>

                                    <div class="pad-top"><a class="btn btn-primary btn_404" href="{{env('APP_URL')}}">{{translate('Take Me Home')}}</a></div>
     

                              <?php  } ?>
</div> 
@endsection
