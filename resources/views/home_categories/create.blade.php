
<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{translate('Category')}}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <form class="form-horizontal" action="{{ route('home_categories.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                          <div class="form-body">
                                <div class="row">
                                
                                    <div class="col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                        <span class="required">{{translate('Category')}}</span>
                      </div>
                                            <div class="col-md-8">
                                                <select class="form-control demo-select2-placeholder" name="category_id" id="category_id" required>
                                                    @foreach(\App\Category::all() as $category)
                                                        @if (\App\HomeCategory::where('category_id', $category->id)->first() == null)
                                                            <option value="{{$category->id}}">{{__($category->name)}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                          </div>
                                            </div>
                                        </div>
                                    </div>
                                   
                  <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                        <button type="reset" onclick="document.location.reload()" class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                        <a href="{{route('home_settings.index')}}" class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>
  
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
  </section>



{{-- <div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{translate('Home Categories')}}</h3>
    </div>

    <!--Horizontal Form-->
    <!--===================================================-->
    <form class="form-horizontal" action="{{ route('home_categories.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="panel-body">
            <div class="form-group row" id="category">
                <label class="col-lg-2 control-label">{{translate('Category')}}</label>
                <div class="col-lg-7">
                    <select class="form-control demo-select2-placeholder" name="category_id" id="category_id" required>
                        @foreach(\App\Category::all() as $category)
                            @if (\App\HomeCategory::where('category_id', $category->id)->first() == null)
                                <option value="{{$category->id}}">{{__($category->name)}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="panel-footer text-center">
            <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
            <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
            <a href="{{route('home_settings.index')}}" class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>
        </div>
    </form>
    <!--===================================================-->
    <!--End Horizontal Form-->

</div> --}}
