     
@extends('admin.layout.admin_template')
@section('content')
 
 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
     
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
  <div class="row match-height">
      <div class="col-md-12 col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">{{translate('Cupon Configuration')}}</h4>
              </div>
              <div class="card-content">
                  <div class="card-body">
  <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('coupon.store') }}" method="POST" enctype="multipart/form-data">
            	@csrf
            <div class="form-body">
			<div class="row">
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span  class="required">{{translate('Coupon Type')}}</span></div>
					       <div class="col-md-8">
						   <select name="coupon_type" id="coupon_type" class="form-control demo-select2" onchange="coupon_form()" required>
                                <option value="">{{translate('Select One') }}</option>
                                <option value="product_base">{{translate('For Products')}}</option>
                                <option value="cart_base">{{translate('For Total Orders')}}</option>
                            </select>
					       </div>
				        </div>
			        </div>
			        
                    <div id="coupon_form" class="col-12">

                    </div> 
				
				 <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn btn-primary mr-1 mb-1" id="btnSubmit">{{translate('Save')}}</button>
                    <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                    <a href="{{route('coupon.index')}}" class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>

                </div>
								  
            </div>
             </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->


                  </div>
              </div>
          </div>
      </div>
      
  </div>
</section>
<!-- // Basic Horizontal form layout section end -->


<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->

  @endsection



@section('script')

<script type="text/javascript">

    function coupon_form(){
        var coupon_type = $('#coupon_type').val();
		$.post('{{ route('coupon.get_coupon_form') }}',{_token:'{{ csrf_token() }}', coupon_type:coupon_type}, function(data){
            $('#coupon_form').html(data);
      
            $('#start_date').datepicker({
                startDate: '-0d',
                todayBtn: "linked",
                autoclose: true,
                todayHighlight: true
        	});
            $('#end_date').datepicker({
                startDate: '-0d',
                todayBtn: "linked",
                autoclose: true,
                todayHighlight: true
        	});
            // $('#demo-dp-range .input-daterange').datepicker({
            //     startDate: '-0d',
            //     todayBtn: "linked",
            //     autoclose: true,
            //     todayHighlight: true
        	// });
		});
    }

</script>

@stop
