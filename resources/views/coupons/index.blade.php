@extends('admin.layout.admin_template')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body">
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{translate('Coupon Information')}}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">
                    <div class="row">
                            <div class="col-sm-12">
                            <a href="{{ route('coupon.create')}}" class="btn btn-rounded btn-info pull-right">{{translate('Add New Coupon')}}</a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                        <th>SL#</th>
                                        <th>{{translate('Code')}}</th>
                                        <th>{{translate('Type')}}</th>
                                        <th>{{translate('Start Date')}}</th>
                                        <th>{{translate('End Date')}}</th>
                                        <th width="10%">{{translate('Options')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($coupons as $key => $coupon)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$coupon->code}}</td>
                                        <td>@if ($coupon->type == 'cart_base')
                                                {{ translate('Cart Base') }}
                                            @elseif ($coupon->type == 'product_base')
                                                {{ translate('Product Base') }}
                                        @endif</td>
                                        <td>{{ date('d-m-Y', $coupon->start_date) }}</td>
                                        <td>{{ date('d-m-Y', $coupon->end_date) }}</td>
                                        <td>
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    {{translate('Actions')}} <i class="dropdown-caret"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="{{route('coupon.edit', encrypt($coupon->id))}}">{{translate('Edit')}}</a></li>
                                                    <li><a onclick="confirm_modal('{{route('coupon.destroy', $coupon->id)}}');">{{translate('Delete')}}</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                              <tfoot>
                                    <tr>
                                        <th>SL#</th>
                                        <th>{{translate('Code')}}</th>
                                        <th>{{translate('Type')}}</th>
                                        <th>{{translate('Start Date')}}</th>
                                        <th>{{translate('End Date')}}</th>
                                        <th width="10%">{{translate('Options')}}</th>
                                    </tr>
                                </tfoot>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
</div>
</div>

@endsection
