@extends('layouts.login')

@section('content')

@php
$generalsetting = \App\GeneralSetting::first();
@endphp
<form method="POST" action="{{ route('login') }}">
    @csrf
    <fieldset class="form-label-group form-group position-relative has-icon-left">
        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
            value="{{ old('email') }}" required autocomplete="email" autofocus>

        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
        <div class="form-control-position">
            <i class="fa fa-user"></i>
        </div>
        <label for="user-name">{{ __('E-Mail Address') }}</label>
    </fieldset>

    <fieldset class="form-label-group position-relative has-icon-left">
        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
            name="password" required autocomplete="current-password">

        @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
        <div class="form-control-position">
            <i class="fa fa-lock"></i>
        </div>
        <label for="user-password">{{ __('Password') }}</label>
    </fieldset>
    <div class="form-group d-flex justify-content-between align-items-center">
        <div class="text-left">
            <fieldset class="checkbox">
                <div class="vs-checkbox-con vs-checkbox-primary">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember"
                        {{ old('remember') ? 'checked' : '' }}>

                    <span class="vs-checkbox">
                        <span class="vs-checkbox--check">
                            <i class="fa fa-check" style="color:white;"></i>
                        </span>
                    </span>
                    <span class=""> {{ __('Remember Me') }}</span>
                </div>
            </fieldset>
        </div>
        <!-- <div class="text-right">
                                        @if (Route::has('password.request'))
                                          <a href="{{ route('password.request') }}" class="card-link">{{ __('Forgot Your Password?') }}</a>
                                          @endif
                                        </div> -->
    </div>
    <div class="form-group d-flex justify-content-between align-items-center">
        <div class="text-left">
           
                <div class="">
            
                    <?php 
                                $subDomain = request()->getHttpHost();
     
                                if ($subDomain == env('APP_SUBDOMAIN')) {
                                    ?>
                                        <input type="hidden" value="S" name="u_type"/>
                                  <span>New User? &nbsp;&nbsp;<a href="{{route('shops.create')}}"><i
                        class="fa fa-sign-in">&nbsp;&nbsp;Register here</i> </a></span>
                                    <?php
                                }
                                else{ ?>
                                    <input type="hidden" value="A" name="u_type"/>
                               <?php }
                                ?>
                    
                </div>
           
        </div>
       
    </div>
    <div class="form-group d-flex justify-content-between align-items-center">
        <div class="text-left">
        
                <div class="">
                <?php 
                                $subDomain = request()->getHttpHost();
     
                                if ($subDomain == env('APP_SUBDOMAIN')) {
                                    ?>
                <span>Forgot Passsword? &nbsp;&nbsp;<a href="{{route('getforgetPassword')}}"><i
                        class="fa fa-sign-in">&nbsp;&nbsp;Click here</i> </a></span>
                </div>
                <?php
                                }
                                else{ ?>
                <span>Forgot Passsword? &nbsp;&nbsp;<a href="{{route('admingetforgetPassword')}}"><i
                        class="fa fa-sign-in">&nbsp;&nbsp;Click here</i> </a></span>
                </div>
                <?php }
                                ?>
        </div>
       
    </div>
    <button type="submit" class="btn btn-primary">
        {{ __('Login') }}
    </button>
</form>
@endsection

@section('script')
<script type="text/javascript">
    function autoFill(){
            $('#email').val('admin@example.com');
            $('#password').val('123456');
        }
</script>
    <!-- END: Page JS-->
    @foreach (session('flash_notification', collect())->toArray() as $message)
<script type="text/javascript">
       $(function() {
  
swal({
            position: 'center',
            type: 'error',
            title: "{{ $message['message'] }}",
            showCancelButton: false,
            confirmButtonColor: '#ff7200', // There won't be any cancel button
            showConfirmButton: true ,
            timer: 10000
        });

       });     
</script>
@endforeach
@endsection