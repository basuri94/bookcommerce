@extends('admin.layout.admin_template')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body">
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{translate('Product Reviews')}}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">
                    <div class="row">
                        <form class="" id="sort_by_rating" action="{{ route('reviews.index') }}" method="GET">
                            <div class="box-inline pad-rgt pull-left">
                                <div class="select" style="min-width: 300px;">
                                    <select class="form-control demo-select2" name="rating" id="rating" >
                                        <option value="">{{translate('Filter by Rating')}}</option>
                                        <option value="rating,desc">{{translate('Rating (High > Low)')}}</option>
                                        <option value="rating,asc">{{translate('Rating (Low > High)')}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="box-inline pad-rgt pull-left">
                                    <div class="" style="min-width: 200px; margin-left:10px;">
                                    <button class="btn btn-primary mr-1 mb-1" type="submit">{{ translate('Filter') }}</button>
                                    </div>
                                </div>
                        </form>
                        </div>
                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{translate('Product')}}</th>
                                        <th>{{translate('Product Owner')}}</th>
                                        <th>{{translate('Customer')}}</th>
                                        <th>{{translate('Rating')}}</th>
                                        <th>{{translate('Comment')}}</th>
                                        <th>{{translate('Published')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($reviews as $key => $review)
                                    @if ($review->product != null && $review->user != null)
                                        <tr>
                                            <td>{{ ($key+1) + ($reviews->currentPage() - 1)*$reviews->perPage() }}</td>
                                            <td><a href="{{ route('product', $review->product->slug) }}" target="_blank">{{ translate($review->product->name) }}</a>@if ($review->viewed == 0) <span class="badge badge-success">{{ translate('New') }}</span> @endif</td>
                                            <td>{{ $review->product->added_by }}</td>
                                            <td>{{ $review->user->name }} ({{ $review->user->email }})</td>
                                            <td>{{ $review->rating }}</td>
                                            <td>{{ $review->comment }}</td>
                                            <td><label class="switch">
                                                <input onchange="update_published(this)" value="{{ $review->id }}" type="checkbox" <?php if($review->status == 1) echo "checked";?> >
                                                <span class="slider round"></span></label></td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                              <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>{{translate('Product')}}</th>
                                        <th>{{translate('Product Owner')}}</th>
                                        <th>{{translate('Customer')}}</th>
                                        <th>{{translate('Rating')}}</th>
                                        <th>{{translate('Comment')}}</th>
                                        <th>{{translate('Published')}}</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="clearfix">
                                <div class="pull-right">
                                {{ $reviews->appends(request()->input())->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
</div>
</div>

@endsection

@section('script')
    <script type="text/javascript">
        function update_published(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('reviews.published') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    showAlert('success', 'Published reviews updated successfully');
                }
                else{
                    showAlert('danger', 'Something went wrong');
                }
            });
        }
        function filter_by_rating(el){
            var rating = $('#rating').val();
            if (rating != '') {
                $('#sort_by_rating').submit();
            }
        }
    </script>
@endsection
