@extends('admin.layout.admin_template')
@section('content')
 
 
 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
     
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
  <div class="row match-height">
      <div class="col-md-12 col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">{{translate('Digital Product Information')}}</h4>
              </div>
              <div class="card-content">
                  <div class="card-body">
  <!--Horizontal Form-->
  


        <!--===================================================-->
        <form class="form form-horizontal mar-top" action="{{route('digitalproducts.store')}}" method="POST" enctype="multipart/form-data" id="choice_form">
		@csrf
		<input type="hidden" name="added_by" value="admin">
            <div class="form-body">
			<div class="row">
			<div class="tab">
				<a class="tablinks" onclick="openCity(event, 'aa')" id="defaultOpen">{{translate('General')}}</a>
				<a class="tablinks" onclick="openCity(event, 'bb')">{{translate('Images')}}</a>
				<a class="tablinks" onclick="openCity(event, 'KK')">{{translate('Videos')}}</a>
				<a class="tablinks" onclick="openCity(event, 'cc')">{{translate('Meta Tags')}}</a>
				<a class="tablinks" onclick="openCity(event, 'dd')">{{translate('Price')}}</a>
				<a class="tablinks" onclick="openCity(event, 'ee')">{{translate('Description')}}</a>
			</div>

				<div id="aa" class="tabcontent">
				<div class="col-12">
									<div class="form-group row">
										<div class="col-md-4"><span>{{translate('Product Name')}}</span></div>
									<div class="col-md-8">
										<input type="text" class="form-control" name="name" placeholder="{{translate('Product Name')}}" required>
									</div>
									</div>
								</div>
								<div class="col-12">
									<div class="form-group row">
										<div class="col-md-4"><span>{{translate('Category')}}</span></div>
									<div class="col-md-8">
									<select class="form-control demo-select2-placeholder" name="category_id" id="category_id" required>
										@foreach(\App\Category::where('digital', 1)->get() as $category)
										   @if($category->digital == 1)
											<option value="{{$category->id}}">{{__($category->name)}}</option>
											@endif
										@endforeach
									</select>
									</div>
									</div>
								</div>
								<div class="col-12">
									<div class="form-group row">
										<div class="col-md-4"><span>{{translate('Subcategory')}}</span></div>
									<div class="col-md-8">
									<select class="form-control demo-select2-placeholder" name="subcategory_id" id="subcategory_id" required>

									</select>
									</div>
									</div>
								</div>
								<div class="col-12">
									<div class="form-group row">
										<div class="col-md-4"><span>{{translate('Sub Subcategory')}}</span></div>
									<div class="col-md-8">
									<!-- <select class="form-control demo-select2-placeholder" name="subsubcategory_id" id="subsubcategory_id">

									</select> -->
									<select class="form-control demo-select2-placeholder" name="subsubcategory_id" id="subsubcategory_id" required>

							</select>
									</div>
									</div>
								</div>

							

								

								<!-- <div class="col-12">
									<div class="form-group row">
										<div class="col-md-4"><span>{{translate('Product File')}}</span></div>
									<div class="col-md-8">
									<input type="file" class="form-control" name="file" required>
									</div>
									</div>
								</div> -->
								<div class="col-12">
									<div class="form-group row">
										<div class="col-md-4"><span>{{translate('Tags')}}</span></div>
									<div class="col-md-8">
									<input type="text" class="form-control" name="tags[]" placeholder="{{ translate('Type to add a tag') }}" data-role="tagsinput">
								</div>
									</div>
								</div>
								<br><div class="col-md-8 offset-md-4" style="padding-top:1%;">
                                    <a class="tablinks btn btn-primary" onclick="openCity(event, 'bb')">{{translate('Next')}}</a>
                                  </div>
				</div>

				<div id="bb" class="tabcontent">
				<div class="col-12">
									<div class="form-group row">
										<div class="col-md-4"><span>{{translate('Main Images')}} </span></div>
									<div class="col-md-8">
									<input type="file" name="photos[]" id="photos-1" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" accept="image/*" />
                                                <label for="photos-1" class="mw-100 mb-3">
                                                    <span></span>
                                                    <strong>
                                                        <i class="fa fa-upload"></i>
                                                        {{ translate('Choose image')}}
                                                    </strong>
                                                </label>
									</div>
									</div>
								</div>
								<div class="col-12">
									<div class="form-group row">
										<div class="col-md-4"><span>{{translate('Thumbnail Image')}}</span><small>(200x280 px)</small></div>
									<div class="col-md-8">
									<input type="file" name="thumbnail_img" id="file-2" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" accept="image/*" />
                                            <label for="file-2" class="mw-100 mb-3">
                                                <span></span>
                                                <strong>
                                                    <i class="fa fa-upload"></i>
                                                    {{ translate('Choose image')}}
                                                </strong>
                                            </label>
									</div>
									</div>
								</div>
								<div class="col-12">
									<div class="form-group row">
										<div class="col-md-4"><span>{{translate('Featured')}} </span><small>(200x280 px)</small></div>
									<div class="col-md-8">
									<input type="file" name="featured_img" id="file-3" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" accept="image/*" />
                                            <label for="file-3" class="mw-100 mb-3">
                                                <span></span>
                                                <strong>
                                                    <i class="fa fa-upload"></i>
                                                    {{ translate('Choose image')}}
                                                </strong>
                                            </label>
									</div>
									</div>
								</div>
								<div class="col-12">
									<div class="form-group row">
										<div class="col-md-4"><span>{{ translate('Flash Deal')}}</span><small>(200x280 px)</small></div>
									<div class="col-md-8">
									<input type="file" name="flash_deal_img" id="file-4" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" accept="image/*" />
                                            <label for="file-4" class="mw-100 mb-3">
                                                <span></span>
                                                <strong>
                                                    <i class="fa fa-upload"></i>
                                                    {{ translate('Choose image')}}
                                                </strong>
                                            </label>
									</div>
									</div>
								</div>

								<br><div class="col-md-8 offset-md-4" style="padding-top:1%;">
                                    <a class="tablinks btn btn-primary" onclick="openCity(event, 'KK')">{{translate('Next')}}</a>
									<a class="tablinks btn btn-warning " onclick="openCity(event, 'aa')">{{translate('Back')}}</a>
                                  </div>

				</div>
				<div id="KK" class="tabcontent">
				                <div class="col-12">
									<div class="form-group row">
										<div class="col-md-4"><span>{{ translate('Video From')}}</span></div>
									<div class="col-md-8">
									<select class="form-control selectpicker" data-minimum-results-for-search="Infinity" name="video_provider">
                                                    <option value="youtube">{{ translate('Youtube')}}</option>
            										<option value="dailymotion">{{ translate('Dailymotion')}}</option>
            										<option value="vimeo">{{ translate('Vimeo')}}</option>
                                                </select>
									</div>
									</div>
								</div>
								<div class="col-12">
									<div class="form-group row">
										<div class="col-md-4"><span>{{ translate('Video URL')}}</span></div>
									<div class="col-md-8">
									<input type="text" class="form-control mb-3" name="video_link" placeholder="{{ translate('Video link')}}">
									</div>
									</div>
								</div>
							
								<br><div class="col-md-8 offset-md-4" style="padding-top:1%;">
                                    <a class="tablinks btn btn-primary" onclick="openCity(event, 'cc')">{{translate('Next')}}</a>
									<a class="tablinks btn btn-warning " onclick="openCity(event, 'bb')">{{translate('Back')}}</a>
                                  </div>

				</div>
				<div id="cc" class="tabcontent">
				<div class="col-12">
									<div class="form-group row">
										<div class="col-md-4"><span>{{translate('Meta Title')}}</span></div>
									<div class="col-md-8">
									<input type="text" class="form-control" name="meta_title" placeholder="{{translate('Meta Title')}}">
								</div>
									</div>
								</div>
								<div class="col-12">
									<div class="form-group row">
										<div class="col-md-4"><span>{{translate('Description')}}</span></div>
									<div class="col-md-8">
									<textarea name="meta_description" rows="8" class="form-control"></textarea>
								</div>
									</div>
								</div>
								<div class="col-12">
									<div class="form-group row">
										<div class="col-md-4"><span>{{ translate('Meta Image') }}</span></div>
									<div class="col-md-8">
									<div id="meta_photo">

									</div>
									</div>
									</div>
								</div>
								<br><div class="col-md-8 offset-md-4" style="padding-top:1%;">
                                    <a class="tablinks btn btn-primary" onclick="openCity(event, 'dd')">{{translate('Next')}}</a>
									<a class="tablinks btn btn-warning " onclick="openCity(event, 'KK')">{{translate('Back')}}</a>
                                  </div>
				</div>

				<div id="dd" class="tabcontent">
				<div class="col-12">
									<div class="form-group row">
										<div class="col-md-4"><span>{{translate('M.R.P.')}}</span></div>
									<div class="col-md-8">
										<input type="number" min="0" value="0" step="0.01" placeholder="{{translate('M.R.P.')}}" name="unit_price" class="form-control" required>
									</div>
									</div>
								</div>
								<div class="col-12">
									<div class="form-group row">
										<div class="col-md-4"><span>{{translate('Hsn No')}}</span></div>
									<div class="col-md-8">
									<select class="form-control demo-select2-placeholder" name="SubCategoryHsn_id" id="SubCategoryHsn_id">

										</select>
									</div>
									</div>
								</div>
								<!-- <div class="col-12">
									<div class="form-group row">
										<div class="col-md-4"><span>{{translate('Purchase price')}}</span></div>
									<div class="col-md-8">
										<input type="number" min="0" value="0" step="0.01" placeholder="{{translate('Purchase price')}}" name="purchase_price" class="form-control" required>
									</div>
									</div>
								</div> -->
								<div class="col-12">
									<div class="form-group row">
										<div class="col-md-4"><span>{{translate('GST')}}</span></div>
									<div class="col-md-8">
									<input type="number" min="0" value="0" step="0.01" placeholder="{{translate('GST')}}" name="tax" id="tax" class="form-control" required>
								</div>
								
									</div>
								</div>
								<div class="col-12">
									<div class="form-group row">
										<div class="col-md-4"><span>{{translate('Discount')}}</span></div>
									<div class="col-md-7">
										<input type="number" min="0" value="0" step="0.01" placeholder="{{translate('Discount')}}" name="discount" class="form-control" required>
									</div>
									<div class="col-md-1">
									<select class="demo-select2" name="discount_type">
										<option value="amount">₹</option>
										<option value="percent">%</option>
									</select>
									</div>
									</div>
								</div>
								<br><div class="col-md-8 offset-md-4" style="padding-top:1%;">
                                    <a class="btn btn-primary tablinks" onclick="openCity(event, 'ee')">{{translate('Next')}}</a>
									<a class="tablinks btn btn-warning " onclick="openCity(event, 'cc')">{{translate('Back')}}</a>
                                  </div>
				</div>

				<div id="ee" class="tabcontent">
				<div class="col-12">
									<div class="form-group row">
										<div class="col-md-4"><span>{{translate('Description')}}</span></div>
									<div class="col-md-12">
									<!-- <textarea class="editor" name="description"></textarea> -->
									<textarea name="description1" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  
									</div>
									</div>
								</div><br>

								<div class="col-md-8 offset-md-4" style="padding-top:1%;">
                                      <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                      <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
									  <a href="{{route('digitalproducts.index')}}" class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>
                                  </div>
				</div>
				    
				
				
								  
            </div>
             </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->


                  </div>
              </div>
          </div>
      </div>
      
  </div>
</section>
<!-- // Basic Horizontal form layout section end -->


<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->

  @endsection



@section('script')
<script>

$(document).ready(function(){

var editor1 = CKEDITOR.replace('description1', {
      extraAllowedContent: 'div(*);h3(*);img(*);h1(*);button(*);h2(*);h4(*);ul(*);a(*);section(*);li(*);span(*);p(*)',
      height: 400,
    
    });
    editor1.on('instanceReady', function() {
      // Output self-closing tags the HTML4 way, like <br>.
      this.dataProcessor.writer.selfClosingEnd = '>';

      // Use line breaks for block elements, tables, and lists.
      var dtd = CKEDITOR.dtd;
      for (var e in CKEDITOR.tools.extend({}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent)) {
        this.dataProcessor.writer.setRules(e, {
          indent: true,
          breakBeforeOpen: true,
          breakAfterOpen: true,
          breakBeforeClose: true,
          breakAfterClose: true
        });
      }
      // Start in source mode.
      this.setMode('source');
    });

	$('#file-2').change(function () {
//  alert('hi');
 var file = $(this)[0].files[0];
var fil_det=$(this);
 img = new Image();
 var imgwidth = 0;
 var imgheight = 0;
 var maxwidth = 200;
 var maxheight = 280;
 var isvalid_image=true;
 img.src = _URL.createObjectURL(file);
 img.onload = function(){
  
  imgwidth = this.width;
  imgheight = this.height;

  if(imgwidth != maxwidth && imgheight != maxheight){
    $('#file-2').val("");
    $('#file-2').next('label').find('span').html("");
  var msg= "Image size must be "+maxwidth+"X"+maxheight;
  showAlert("error",msg);
 }
};
img.onerror = function() {
$(this).val("");
  $(this).next('label').find('span').html("");
  var msg="not a valid file: " + file.type;
  showAlert("error",msg);
}
});
$('#file-3').change(function () {
//  alert('hi');
 var file = $(this)[0].files[0];
var fil_det=$(this);
 img = new Image();
 var imgwidth = 0;
 var imgheight = 0;
 var maxwidth = 200;
 var maxheight = 280;
 var isvalid_image=true;
 img.src = _URL.createObjectURL(file);
 img.onload = function(){
  
  imgwidth = this.width;
  imgheight = this.height;

  if(imgwidth != maxwidth && imgheight != maxheight){
    $('#file-3').val("");
    $('#file-3').next('label').find('span').html("");
  var msg= "Image size must be "+maxwidth+"X"+maxheight;
  showAlert("error",msg);
 }
};
img.onerror = function() {
$(this).val("");
  $(this).next('label').find('span').html("");
  var msg="not a valid file: " + file.type;
  showAlert("error",msg);
}
});
$('#file-4').change(function () {
//  alert('hi');
 var file = $(this)[0].files[0];
var fil_det=$(this);
 img = new Image();
 var imgwidth = 0;
 var imgheight = 0;
 var maxwidth = 200;
 var maxheight = 280;
 var isvalid_image=true;
 img.src = _URL.createObjectURL(file);
 img.onload = function(){
  
  imgwidth = this.width;
  imgheight = this.height;

  if(imgwidth != maxwidth && imgheight != maxheight){
    $('#file-4').val("");
    $('#file-4').next('label').find('span').html("");
  var msg= "Image size must be "+maxwidth+"X"+maxheight;
  showAlert("error",msg);
 }
};
img.onerror = function() {
$(this).val("");
  $(this).next('label').find('span').html("");
  var msg="not a valid file: " + file.type;
  showAlert("error",msg);
}
});
});
  </script>
  
<script type="text/javascript">
    $("#subsubcategory_id").on('change', function() {
		get_brands_by_subsubcategory();
	});
    $('#subcategory_id').on('change', function() { 
		
		var subsubcategory_id=$("#subcategory_id").val();
	   get_hsn_gst_by_subsubcategory_id(subsubcategory_id);
	});

	$("#SubCategoryHsn_id").on('change', function() {
		var SubCategoryHsn_id=$("#SubCategoryHsn_id").val();
	    get_gst_by_hsn_id(SubCategoryHsn_id);
	});

	function get_hsn_gst_by_subsubcategory_id(subsubcategory_id){ //alert(subsubcategory_id);

	$.post('{{ route('subcategories.get_subcategories_by_SubCategoryHsn') }}',{_token:'{{ csrf_token() }}', subsubcategory_id:subsubcategory_id}, function(data){
		$('#SubCategoryHsn_id').html(null);
		for (var i = 0; i < data.length; i++) {
			$('#SubCategoryHsn_id').append($('<option>', {
				value: data[i].id,
				text: data[i].hns_no
			}));
			$('.demo-select2').select2();
		}
	});

	}

	function get_gst_by_hsn_id(SubCategoryHsn_id){

	$.post('{{ route('subcategories.get_gst_by_hsn_id') }}',{_token:'{{ csrf_token() }}', has_id:SubCategoryHsn_id}, function(data){
	
		$('#tax').val(data.gst_amount);
		
	});
	}

	function get_subcategories_by_category(){
		var category_id = $('#category_id').val();
		$.post('{{ route('subcategories.get_subcategories_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){
			$('#subcategory_id').html(null);
			for (var i = 0; i < data.length; i++) {
				$('#subcategory_id').append($('<option>', {
					value: data[i].id,
					text: data[i].name
				}));
				$('.demo-select2').select2();
			}
			get_subsubcategories_by_subcategory();
		});
	}

	function get_subsubcategories_by_subcategory(){
		var subcategory_id = $('#subcategory_id').val();
		$.post('{{ route('subsubcategories.get_subsubcategories_by_subcategory') }}',{_token:'{{ csrf_token() }}', subcategory_id:subcategory_id}, function(data){
			$('#subsubcategory_id').html(null);
			for (var i = 0; i < data.length; i++) {
				$('#subsubcategory_id').append($('<option>', {
					value: data[i].id,
					text: data[i].name
				}));
				$('.demo-select2').select2();
			}
		});
	}

	function get_brands_by_subsubcategory(){
		var subsubcategory_id = $('#subsubcategory_id').val();
		$.post('{{ route('subsubcategories.get_brands_by_subsubcategory') }}',{_token:'{{ csrf_token() }}', subsubcategory_id:subsubcategory_id}, function(data){
			$('#brand_id').html(null);
			for (var i = 0; i < data.length; i++) {
				$('#brand_id').append($('<option>', {
					value: data[i].id,
					text: data[i].name
				}));
				$('.demo-select2').select2();
			}
		});
	}

	$(document).ready(function(){
		$('#container').removeClass('mainnav-lg').addClass('mainnav-sm');
		get_subcategories_by_category();
		$("#photos").spartanMultiImagePicker({
			fieldName:        'photos[]',
			maxCount:         10,
			rowHeight:        '200px',
			groupClassName:   'col-md-4 col-sm-4 col-xs-6',
			maxFileSize:      '',
			dropFileLabel : "Drop Here",
			onExtensionErr : function(index, file){
				console.log(index, file,  'extension err');
				alert('Please only input png or jpg type file')
			},
			onSizeErr : function(index, file){
				console.log(index, file,  'file size too big');
				alert('File size too big');
			}
		});
		$("#thumbnail_img").spartanMultiImagePicker({
			fieldName:        'thumbnail_img',
			maxCount:         1,
			rowHeight:        '200px',
			groupClassName:   'col-md-4 col-sm-4 col-xs-6',
			maxFileSize:      '',
			dropFileLabel : "Drop Here",
			onExtensionErr : function(index, file){
				console.log(index, file,  'extension err');
				alert('Please only input png or jpg type file')
			},
			onSizeErr : function(index, file){
				console.log(index, file,  'file size too big');
				alert('File size too big');
			}
		});
		$("#featured_img").spartanMultiImagePicker({
			fieldName:        'featured_img',
			maxCount:         1,
			rowHeight:        '200px',
			groupClassName:   'col-md-4 col-sm-4 col-xs-6',
			maxFileSize:      '',
			dropFileLabel : "Drop Here",
			onExtensionErr : function(index, file){
				console.log(index, file,  'extension err');
				alert('Please only input png or jpg type file')
			},
			onSizeErr : function(index, file){
				console.log(index, file,  'file size too big');
				alert('File size too big');
			}
		});
		$("#flash_deal_img").spartanMultiImagePicker({
			fieldName:        'flash_deal_img',
			maxCount:         1,
			rowHeight:        '200px',
			groupClassName:   'col-md-4 col-sm-4 col-xs-6',
			maxFileSize:      '',
			dropFileLabel : "Drop Here",
			onExtensionErr : function(index, file){
				console.log(index, file,  'extension err');
				alert('Please only input png or jpg type file')
			},
			onSizeErr : function(index, file){
				console.log(index, file,  'file size too big');
				alert('File size too big');
			}
		});
		$("#meta_photo").spartanMultiImagePicker({
			fieldName:        'meta_img',
			maxCount:         1,
			rowHeight:        '200px',
			groupClassName:   'col-md-4 col-sm-4 col-xs-6',
			maxFileSize:      '',
			dropFileLabel : "Drop Here",
			onExtensionErr : function(index, file){
				console.log(index, file,  'extension err');
				alert('Please only input png or jpg type file')
			},
			onSizeErr : function(index, file){
				console.log(index, file,  'file size too big');
				alert('File size too big');
			}
		});
	});

	$('#category_id').on('change', function() {
		get_subcategories_by_category();
	});

	$('#subcategory_id').on('change', function() {
		get_subsubcategories_by_subcategory();
	});

	

</script>

@endsection
