@extends('admin.layout.admin_template')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body">
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{ translate('TL2V Assurance')}}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">
                    <div class="row">
                            <div class="col-sm-12">
                            <a href="{{ route('Band_Trust.create')}}" class="btn btn-rounded btn-info pull-right">{{ translate('Add New TL2V Assurance')}}</a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                        <th>SL#</th>
                                        <th>{{ translate('Image')}}</th>
                                        <th>{{ translate('Title')}}</th>
                                        <th width="10%">{{ translate('Options')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $key => $row)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>
                                            @if (file_exists($row->image_name)) 
                                              <img loading="lazy"  class="img-md" src="{{ asset($row->image_name)}}" style="width :150px; height:150px;" alt="Image">
                                            @endif
                                            </td>
                                            <td>{{$row->title}}</td>
                                            <td>
                                                <div class="btn-group dropdown">
                                                    <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                        {{ translate('Actions')}} <i class="dropdown-caret"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="{{route('Band_Trust.edit', encrypt($row->id))}}">{{ translate('Edit')}}</a></li>
                                                        <li><a onclick="confirm_modal('{{route('Band_Trust.destroy', $row->id)}}');">{{ translate('Delete')}}</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                              <tfoot>
                                    <tr>
                                        <th>SL#</th>
                                        <th>{{ translate('Referby Amount')}}</th>
                                        <th>{{ translate('Getrefer Amount')}}</th>
                                        <th width="10%">{{ translate('Options')}}</th>
                                    
                                    </tr>
                                </tfoot>
                            </table>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
</div>
</div>

@endsection
