     
@extends('admin.layout.admin_template')
@section('content')
 
 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
     
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
  <div class="row match-height">
      <div class="col-md-12 col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">{{ translate('TL2V Assurance Information')}}</h4>
              </div>
              <div class="card-content">
                  <div class="card-body">
  <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('Band_Trust.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-body">
			<div class="row">
                    <div class="col-12">
                          <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Title')}}</span></div>
                            <div class="col-md-8">
                              <input type="text" placeholder="{{translate('Title')}}" id="title" name="title" class="form-control" required>
                            </div>
                          </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span  class="required" >{{translate('Image')}} </span><small>( 512x512 )</small></div>
					       <div class="col-md-8">
                           <input type="file" id="logo_img" name="logo_img" class="form-control" required>
                        </div>
				        </div>
			        </div>
				
				 <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                    <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                    <a href="{{route('Band_Trust.index')}}" class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>

            </div>
								  
            </div>
             </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->


                  </div>
              </div>
          </div>
      </div>
      
  </div>
</section>
<!-- // Basic Horizontal form layout section end -->


<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->

  @endsection
