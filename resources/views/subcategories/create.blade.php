     
@extends('admin.layout.admin_template')
@section('content')
 
 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
     
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
  <div class="row match-height">
      <div class="col-md-12 col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">{{translate('Subcategory Information')}}</h4>
              </div>
              <div class="card-content">
                  <div class="card-body">
  <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('subcategories.store') }}" method="POST" enctype="multipart/form-data">
        	@csrf
            <div class="form-body">
			<div class="row">
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Name')}}</span></div>
					       <div class="col-md-8">
                           <input type="text" placeholder="{{translate('Name')}}" id="name" name="name" class="form-control" required>
					       </div>
				        </div>
			        </div>
			        <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span  class="required">{{translate('Category')}}</span></div>
					       <div class="col-md-8">
                           <select name="category_id" required class="form-control demo-select2-placeholder">
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{__($category->name)}}</option>
                            @endforeach
                        </select>
					       </div>
				        </div>
			        </div>
			
			        <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Meta Title')}}</span></div>
					       <div class="col-md-8">
						   <input type="text" class="form-control" name="meta_title" placeholder="{{translate('Meta Title')}}">
					       </div>
				        </div>
			        </div>
                <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Description')}}</span></div>
					       <div class="col-md-8">
                           <textarea name="meta_description" rows="8" class="form-control"></textarea>
					       </div>
				        </div>
			        </div>
               
                    @if (\App\BusinessSetting::where('type', 'category_wise_commission')->first()->value == 1)
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span >{{translate('Commission Rate')}}</span></div>
					       <div class="col-md-6">
                           <input type="number" min="0" step="0.01" placeholder="{{translate('Commission Rate')}}" id="commision_rate" name="commision_rate" class="form-control">
					       </div>
                           <div class="col-lg-2">
                            <option class="form-control">%</option>
                        </div>
				        </div>
			        </div>
                    @endif 
				
				 <div class="col-md-8 offset-md-4">
                                      <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                      <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                      <a href="{{route('subcategories.index')}}" class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>
                                  </div>
								  
            </div>
             </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->


                  </div>
              </div>
          </div>
      </div>
      
  </div>
</section>
<!-- // Basic Horizontal form layout section end -->


<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->

  @endsection

