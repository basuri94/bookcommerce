@extends('admin.layout.admin_template')
@section('content')

<!-- BEGIN: Content-->
<div class="app-content content">
  <div class="content-overlay"></div>
  <div class="header-navbar-shadow"></div>
  <div class="content-wrapper">

    <div class="content-body">
      <!-- Basic Horizontal form layout section start -->
      <section id="basic-horizontal-layouts">
        <div class="row match-height">
          <div class="col-md-12 col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">{{translate('Promotional Banner Details')}}</h4>
              </div>
              <div class="card-content">
                <div class="card-body">
                  <!--Horizontal Form-->
                  <!--===================================================-->
                  <form class="form-horizontal" action="{{ route('promotional_banner.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">
                      <div class="row">
                      
                      <div class="col-12">
                        <div class="form-group row">
                          <div class="col-md-4"><span>{{translate('Banner Image')}}</span>&nbsp;<span>(850x315 px)</span></div>
                          <div class="col-md-8">
                          <div id="thumbnail_img"></div>
                            </div>
                        </div>
                      </div>
                     

                        <div class="col-md-8 offset-md-4">
                          <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                          <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                          <a href="{{route('email_template.index')}}"
                            class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>
                        </div>

                      </div>
                    </div>
                  </form>
                  <!--===================================================-->
                  <!--End Horizontal Form-->
                </div>
              </div>
            </div>
          </div>

        </div>
      </section>
     
    </div>
  </div>
</div>
<!-- END: Content-->

@endsection
@section('script')
<script>
$('#file-2').change(function () {
//  alert('hi');
 var file = $(this)[0].files[0];
var fil_det=$(this);
 img = new Image();
 var imgwidth = 0;
 var imgheight = 0;
 var maxwidth = 200;
 var maxheight = 280;
 var isvalid_image=true;
 img.src = _URL.createObjectURL(file);
 img.onload = function(){
  
  imgwidth = this.width;
  imgheight = this.height;

  if(imgwidth != maxwidth && imgheight != maxheight){
    $('#file-2').val("");
    $('#file-2').next('label').find('span').html("");
  var msg= "Image size must be "+maxwidth+"X"+maxheight;
  showAlert("error",msg);
 }
};
img.onerror = function() {
$(this).val("");
  $(this).next('label').find('span').html("");
  var msg="not a valid file: " + file.type;
  showAlert("error",msg);
}
});
$("#thumbnail_img").spartanMultiImagePicker({
			fieldName:        'thumbnail_img',
			maxCount:         1,
			rowHeight:        '200px',
			groupClassName:   'col-md-3 col-sm-4 col-xs-6',
			maxFileSize:      '',
			dropFileLabel : "Drop Here",
			onExtensionErr : function(index, file){
				console.log(index, file,  'extension err');
				alert('Please only input png or jpg type file')
			},
			onSizeErr : function(index, file){
				console.log(index, file,  'file size too big');
				alert('File size too big');
			}
		});
  </script>
  @stop
