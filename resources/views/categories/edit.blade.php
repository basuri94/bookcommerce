     
@extends('admin.layout.admin_template')
@section('content')
 
 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
     
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
  <div class="row match-height">
      <div class="col-md-12 col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">{{translate('Category Information')}}</h4>
              </div>
              <div class="card-content">
                  <div class="card-body">
  <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('categories.update', $category->id) }}" method="POST" enctype="multipart/form-data">
        <input name="_method" type="hidden" value="PATCH">
            @csrf
            <div class="form-body">
			<div class="row">
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Name')}}</span></div>
					       <div class="col-md-8">
                           <input type="text" placeholder="{{translate('Name')}}" id="name" name="name" class="form-control" required value="{{$category->name}}">
					       </div>
				        </div>
			        </div>
			        <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Type')}}</span></div>
					       <div class="col-md-8">
                           <select name="digital" required class="form-control demo-select2-placeholder">
                            <option value="0" @if ($category->digital == '0') selected @endif>{{translate('Physical')}}</option>
                            <option value="1" @if ($category->digital == '1') selected @endif>{{translate('Digital')}}</option>
                        </select>
					       </div>
				        </div>
			        </div>
			
			        <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Banner')}}</span>&nbsp;<span >({{ translate('380x135') }})</span></div>
					       <div class="col-md-8">
                           <input type="file" id="banner" name="banner" class="form-control"  onchange="readURL1(this);">
 @if($category->banner != '')
                           <img src="{{asset($category->banner)}}"  id="blah" width="380"  height="135"alt="Banner"><br>
                           @endif
                           @if($category->banner == '')
                           <img src="{{asset('img/Blank.jpg')}}"  id="blahicon" width="180"  height="185"alt="Top Category Icon"><br>
                           @endif
                           <!-- <span style="color: #002a80;font-size:85%;" class="info_adin_text">Max Size(2MB)</span> -->
                           <span class="info_adin_text mt-3">Upload an image with 380px By 135px.</span>
                           
                           <br><button type="reset" id="pseudoCancel">
                             Cancel
                           </button>
					       </div>
				        </div>
			        </div>
<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Home Text')}}</span></div>
					       <div class="col-md-8">
                           <input type="text" placeholder="{{translate('Home Text')}}" id="home_text" name="home_text" class="form-control" maxlength="20" value="{{$category->home_text}}">
					       </div>
				        </div>
			        </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Top Category Icon')}}</span>&nbsp;<span>({{ translate('180x185') }})</span></div>
					       <div class="col-md-8">
                          
                           <input type="file" id="icon" name="icon" class="form-control" onchange="readURL2(this);">
                           @if($category->icon != '')
                           <img src="{{asset($category->icon)}}"  id="blahicon" width="180"  height="185"alt="Top Category"><br>
                           @endif
                           @if($category->icon == '')
                           <img src="{{asset('img/Blank.jpg')}}"  id="blahicon" width="180"  height="185"alt="Top Category Icon"><br>
                           @endif
                           <!-- <span style="color: #002a80;font-size:85%;" class="info_adin_text">Max Size(2MB)</span> -->
                           <span class="info_adin_text mt-3">Upload an image with 180px By 185px.</span>
                           
                           <br><button type="reset" id="pseudoCancelicon">
                             Cancel
                           </button>
					       </div>
				        </div>
              </div>
              <div class="col-12">
										<div class="form-group row">
                      <div class="col-md-4"><span>{{translate('Gallery Images')}}</span></div>
                      @if(is_array(json_decode($category->sliders)))
                      <div class="col-md-8 row">
									        @foreach (json_decode($category->sliders) as $key => $photo)
											 <div class="col-md-10">
											 <img loading="lazy"  src="{{ asset($photo) }}" alt="" class="img-responsive" width="450px">
												<input type="hidden" name="previous_photos[]" value="{{ $photo }}">
												<button type="button" class="btn btn-danger close-btn remove-files"><i class="fa fa-times"></i></button>
											  </div>
											
											  @endforeach
                        @endif
                      </div>
										</div>
									</div>
                  <div class="col-12 justify-content-md-center" style="margin-left: 30%;">

<div class="col-md-offset-4 col-md-8">
  
<div id="photos" ></div>
 </div>
 </div>
                <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Meta Title')}}</span></div>
					       <div class="col-md-8">
                           <input type="text" class="form-control" name="meta_title" value="{{ $category->meta_title }}" placeholder="{{translate('Meta Title')}}">
					       </div>
				        </div>
			        </div>
               
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Description')}}</span></div>
					       <div class="col-md-8">
                           <textarea name="meta_description" rows="8" class="form-control">{{ $category->meta_description }}</textarea>
					       </div>
				        </div>
			        </div>

                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Slug')}}</span></div>
					       <div class="col-md-8">
                           <input type="text" placeholder="{{translate('Slug')}}" id="slug" name="slug" value="{{ $category->slug }}" class="form-control">
                    </div>
				        </div>
			        </div>

                   
				 <div class="col-md-8 offset-md-4">
                                      <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                      <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                      <a href="{{route('categories.index')}}" class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>
                                  </div>
								  
            </div>
             </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->


                  </div>
              </div>
          </div>
      </div>
      
  </div>
</section>
<!-- // Basic Horizontal form layout section end -->


<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->

  @endsection
  @section('script')
  <script>
    $("#photos").spartanMultiImagePicker({
			fieldName:        'photos[]',
			maxCount:         10,
			rowHeight:        '150px',
			groupClassName:   'col-md-10 col-sm-10 col-xs-10',
			maxFileSize:      '',
			dropFileLabel : "Drop Here",
			onExtensionErr : function(index, file){
				console.log(index, file,  'extension err');
				alert('Please only input png or jpg type file')
			},
			onSizeErr : function(index, file){
				console.log(index, file,  'file size too big');
				alert('File size too big');
			}
		});

      
    </script>
  <script type="text/javascript">
      $(document).ready(function() {
    
        $('#pseudoCancel').click(function(){
          
          $('#banner').removeAttr('src');
        $('#banner').val('');
          $('#blah').val('');
          $('#blah').removeAttr('src');
          $('#blah').attr('src',"{{asset('img/Blank.jpg')}}");
     
        });
  
        $('#pseudoCancelicon').click(function(){
          
          $('#icon').removeAttr('src');
        $('#icon').val('');
          $('#blahicon').val('');
          $('#blahicon').removeAttr('src');
          $('#blahicon').attr('src',"{{asset('img/Blank.jpg')}}");
     
        });
  
      });
      function readURL1(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();
  
              reader.onload = function (e) {
                  $('#blah')
                          .attr('src', e.target.result)
                          .width(380)
                          .height(135);
              };
  
              reader.readAsDataURL(input.files[0]);
          }
      }

      function readURL2(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();
  
              reader.onload = function (e) {
                  $('#blahicon')
                          .attr('src', e.target.result)
                          .width(180)
                          .height(185);
              };
  
              reader.readAsDataURL(input.files[0]);
          }
      }
  </script>
  
  
    @endsection

