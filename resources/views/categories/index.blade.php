
    
@extends('admin.layout.admin_template')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body">
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{translate('Categories')}}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">
                    <div class="row">
                            <div class="col-sm-12">
                            <a href="{{ route('categories.create')}}" class="btn btn-rounded btn-info pull-right">{{translate('Add New Category')}}</a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                    <th>#</th>
                                    <th>{{translate('Name')}}</th>
                                    <th>{{translate('Banner')}}</th>
                                    <th>{{translate('Top Category Icon')}}</th>
                                    <th>{{translate('Featured')}}</th>
                                    <th width="10%">{{translate('Options')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($categories as $key => $category)
                                    <tr>
                                        <td>{{ ($key+1) + ($categories->currentPage() - 1)*$categories->perPage() }}</td>
                                        <td>{{__($category->name)}}</td>
                                        <td>
                                        @if ($category->banner != '')
                                        <img loading="lazy"  class="img-md" src="{{ asset($category->banner) }}" alt="{{translate('banner')}}">
                                        @endif
                                        </td>
                                        <td>
                                        @if ($category->icon != '')
                                        <img loading="lazy"  class="img-xs" src="{{ asset($category->icon) }}" alt="{{translate('icon')}}">
                                        @endif
                                        </td>
                                        <td><label class="switch">
                                            <input onchange="update_featured(this)" value="{{ $category->id }}" type="checkbox" <?php if($category->featured == 1) echo "checked";?> >
                                            <span class="slider round"></span></label></td>
                                        
                                        <td>
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    {{translate('Actions')}} <i class="dropdown-caret"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="{{route('categories.edit', encrypt($category->id))}}">{{translate('Edit')}}</a></li>
                                                    <li><a onclick="confirm_modal('{{route('categories.destroy', $category->id)}}');">{{translate('Delete')}}</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                              <tfoot>
                                  <tr>
                                    <th>#</th>
                                    <th>{{translate('Name')}}</th>
                                    <th>{{translate('Banner')}}</th>
                                    <th>{{translate('Top Category Icon')}}</th>
                                    <th>{{translate('Featured')}}</th>
                                   
                                    <th width="10%">{{translate('Options')}}</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="clearfix">
                                <div class="pull-right">
                                    {{ $categories->appends(request()->input())->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
</div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        function update_featured(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('categories.featured') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    showAlert('success', 'Featured categories updated successfully');
                }
                else{
                    showAlert('danger', 'Something went wrong');
                }
            });
        }
    </script>
@endsection

