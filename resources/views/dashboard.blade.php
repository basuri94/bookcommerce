@extends('admin.layout.admin_template')
@section('content')
   
    <!-- BEGIN: Content-->
    <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><!-- Dashboard Ecommerce Starts -->
<section id="dashboard-ecommerce">
@if(Auth::user()->user_type == 'admin' || in_array('1', json_decode(Auth::user()->staff->role->permissions)))
  <div class="row">
    <div class="col-lg-3 col-sm-6 col-12">
        <div class="card" >
            <div class="card-header d-flex flex-column align-items-start pb-0">
                <div class="avatar bg-rgba-primary p-50 m-0">
                    <div class="avatar-content">
                    <i class="fa fa-product-hunt text-danger font-medium-5" aria-hidden="true"></i>
                    </div>
                </div><br>
                <p class="text-lg text-main">{{translate('Total published products')}} : <span class="text-bold-700 mt-1"> {{ \App\Product::where('published', 1)->get()->count() }}</span></p>
                @if (\App\BusinessSetting::where('type', 'vendor_system_activation')->first()->value == 1)
                    <p class="text-lg text-main">{{translate('Total sellers products')}} : <span class="text-bold-700 mt-1"> {{ \App\Product::where('published', 1)->where('added_by', 'seller')->get()->count() }}</span></p>
                @endif
                <p class="text-lg text-main">{{translate('Total admin products')}} : <span class="text-bold-700 mt-1"> {{ \App\Product::where('published', 1)->where('added_by', 'admin')->get()->count() }}</span></p>
                <br>
                 </div>
            <div class="card-content">
            <a href="{{ route('products.admin') }}" class="btn btn-primary mar-top btn-block top-border-radius-n">{{ translate('Manage Products') }} <i class="fa fa-long-arrow-right"></i></a>
           
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-sm-6 col-12">
        <div class="card">
            <div class="card-header d-flex flex-column align-items-start pb-0">
                <div class="avatar bg-rgba-success p-50 m-0">
                    <div class="avatar-content">
                    <i class="fa fa-instagram text-danger font-medium-5"></i>
                    </div>
                </div><br><br>
                <p class="text-normal text-main">{{translate('Total product category')}} : 
                <span class="text-bold-700 mt-1"> {{ \App\Category::all()->count() }}</span></p>
                <br><br><br></div>
            <div class="card-content">
            <a href="{{ route('categories.create') }}" class="btn btn-primary mar-top btn-block top-border-radius-no">{{translate('Create Category')}}</a>
                    
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-sm-6 col-12">
        <div class="card">
            <div class="card-header d-flex flex-column align-items-start pb-0">
                <div class="avatar bg-rgba-danger p-50 m-0">
                    <div class="avatar-content">
                        <i class="fa fa-podcast text-danger font-medium-5"></i>
                    </div>
                </div><br>
                <p class="mb-0">{{translate('Total product sub sub category')}} : 
                <span class="text-bold-700 mt-1">{{ \App\SubSubCategory::all()->count() }}</span></p>
                
            </div><br><br><br><br>
            <div class="card-content">
            <a href="{{ route('subsubcategories.create') }}" class="btn btn-primary mar-top btn-block top-border-radius-no">{{translate('Create Sub Sub Category')}}</a>
                    </div>
        </div>
    </div>
    <div class="col-lg-3 col-sm-6 col-12">
        <div class="card">
            <div class="card-header d-flex flex-column align-items-start pb-0">
                <div class="avatar bg-rgba-warning p-50 m-0">
                    <div class="avatar-content">
                    <i class="feather icon-shopping-cart text-danger font-medium-5"></i>
                    </div>
                </div><br>
                <p class="mb-0">{{translate('Total product sub category')}} : 
                <span class="text-bold-700 mt-1">{{ \App\SubCategory::all()->count() }}</span></p>
                
            </div><br><br><br><br><br>
            <div class="card-content">
            <a href="{{ route('subcategories.create') }}" class="btn btn-primary mar-top btn-block top-border-radius-no">{{translate('Create Sub Category')}}</a>
                    </div>
        </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-3 col-sm-6 col-12">
        <div class="card">
            <div class="card-header d-flex flex-column align-items-start pb-0">
                <div class="avatar bg-rgba-primary p-50 m-0">
                    <div class="avatar-content">
                    <i class="fa fa-meetup text-danger font-medium-5"></i>
                    </div>
                </div><br>
                <p class="mb-0">{{translate('Total product brand')}} :
                <span class="text-bold-700 mt-1">{{ \App\Brand::all()->count() }}</span></p><br>
              
                 </div>
            <div class="card-content">
            
            <a href="{{ route('brands.create') }}" class="btn btn-primary mar-top btn-block top-border-radius-no">{{translate('Create Brand')}}</a>
           
             </div>
        </div>
    </div>


@endif

@if((Auth::user()->user_type == 'admin' || in_array('5', json_decode(Auth::user()->staff->role->permissions))) && \App\BusinessSetting::where('type', 'vendor_system_activation')->first()->value == 1)
  


    <div class="col-lg-3 col-sm-6 col-12">
        <div class="card">
            <div class="card-header d-flex flex-column align-items-start pb-0">
                <div class="avatar bg-rgba-success p-50 m-0" >
                    <div class="avatar-content">
                    <i class="fa fa-sellsy text-danger font-medium-5"></i>
                    </div>
                </div><br>
                <p class="text-normal text-main">{{translate('Total sellers')}} : 
                <span class="text-bold-700 mt-1"> {{ \App\Seller::all()->count() }}</span></p>
                
                </div>
            <div class="card-content" >
            
            <a href="{{ route('sellers.index') }}" class="btn btn-primary mar-top btn-block top-border-radius-no">{{translate('Manage Sellers')}} <i class="fa fa-long-arrow-right"></i></a>     
           
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-sm-6 col-12">
        <div class="card">
            <div class="card-header d-flex flex-column align-items-start pb-0">
                <div class="avatar bg-rgba-danger p-50 m-0">
                    <div class="avatar-content">
                    <i class="fa fa-smile-o text-danger font-medium-5"></i>
                    </div>
                </div><br>
                <p class="mb-0">{{translate('Total approved sellers')}} : 
                <span class="text-bold-700 mt-1">{{ \App\Seller::where('verification_status', 1)->get()->count() }}</span></p><br>
               
            </div>
            <div class="card-content">
            
            <a href="{{ route('sellers.index') }}" class="btn btn-primary mar-top btn-block top-border-radius-no">{{translate('Manage Sellers')}} <i class="fa fa-long-arrow-right"></i></a>
           
                    </div>
        </div>
    </div>
    <div class="col-lg-3 col-sm-6 col-12">
        <div class="card">
            <div class="card-header d-flex flex-column align-items-start pb-0">
                <div class="avatar bg-rgba-warning p-50 m-0">
                    <div class="avatar-content">
                    <i class="fa fa-address-book text-danger font-medium-5"></i>
                    </div>
                </div><br>
                <p class="mb-0">{{translate('Total pending sellers')}} : 
                <span class="text-bold-700 mt-1">{{ \App\Seller::where('verification_status', 0)->count() }}</span></p><br>
               
            </div>
            <div class="card-content">
            
            <a href="{{ route('sellers.index') }}" class="btn btn-primary mar-top btn-block top-border-radius-no">{{translate('Manage Sellers')}} <i class="fa fa-long-arrow-right"></i></a>
             
                   </div>
        </div>
    </div>
  </div>
  @endif
  @if(Auth::user()->user_type == 'admin' || in_array('1', json_decode(Auth::user()->staff->role->permissions)))
  <div class="row">
      <div class="col-lg-8 col-md-6 col-12">
          <div class="card">
              <!--Panel heading-->
            <div class="panel-heading">
                <h3 class="panel-title">{{translate('Category wise product sale')}}</h3>
            </div>

            <!--Panel body-->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped mar-no">
                        <thead>
                            <tr>
                                <th>{{translate('Category Name')}}</th>
                                <th>{{translate('Sale')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach (\App\Category::all() as $key => $category)
                                <tr>
                                    <td>{{ __($category->name) }}</td>
                                    <td>{{ \App\Product::where('category_id', $category->id)->sum('num_of_sale') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
          </div>
      </div>
      <div class="col-lg-4 col-md-6 col-12">
          <div class="card">
                <!--Panel heading-->
            <div class="panel-heading">
                <h3 class="panel-title">{{translate('Category wise product stock')}}</h3>
            </div>

            <!--Panel body-->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped mar-no">
                        <thead>
                            <tr>
                                <th>{{translate('Category Name')}}</th>
                                <th>{{translate('Stock')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach (\App\Category::all() as $key => $category)
                                @php
                                    $products = \App\Product::where('category_id', $category->id)->get();
                                    $qty = 0;
                                    foreach ($products as $key => $product) {
                                        if ($product->variant_product) {
                                            foreach ($product->stocks as $key => $stock) {
                                                $qty += $stock->qty;
                                            }
                                        }
                                        else {
                                            $qty = $product->current_stock;
                                        }
                                    }
                                @endphp
                                <tr>
                                    <td>{{ __($category->name) }}</td>
                                    <td>{{ $qty }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
          </div>
      </div>
  </div>
  @endif
  
  
  
<!--/ Zero configuration table -->
</section>
<!-- Dashboard Ecommerce ends -->

        </div>
      </div>
    </div>
    <!-- END: Content-->


   @endsection

    
