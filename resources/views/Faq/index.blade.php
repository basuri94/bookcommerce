@extends('admin.layout.admin_template')
@section('content')
    <style>
        th,
        td {
            padding: 10px;
        }

    </style>
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section id="basic-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{ translate('FAQ') }}</h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body card-dashboard">
                                        <form class="form-horizontal" action="{{ route('faq.store') }}" method="POST"
                                            enctype="multipart/form-data">
                                            @csrf

                                            <input type="hidden" id="tot_count" name="tot_count" value="1">
                                            <input type="hidden" id="question_array" name="question_array">
                                            <input type="hidden" id="answer_array" name="answer_array">
                                            <div class="form-group row">
                                                <div class="col-lg-4">
                                                    <label
                                                        class="col-lg-3 control-label">{{ translate('FAQ Type') }}</label>
                                                </div>
                                                <div class="col-lg-8">
                                                    <select name="type" id="type" class="form-control" required>
                                                        <option value="Customer">{{ translate('Customer') }}</option>
                                                        <option value="Seller">{{ translate('Seller') }}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="table-responsive">

                                                <table style="width:100%;border-top:#1adab5 solid; "
                                                    id="sub_fees_allocation">

                                                    <thead>
                                                        <tr>
                                                            <th>Question</th>
                                                            <th>Answer</th>
                                                            <th>Action</th>
                                                        </tr>
                                                        <tr>

                                                            <td>

                                                                <input type="text" placeholder="{{ translate('Question') }}"
                                                                    id="question" class="form-control">
                                                            </td>
                                                            <td><textarea id="answer" class="form-control"
                                                                    placeholder="{{ translate('Answer') }}"></textarea></td>
                                                            <td> <a href='javascript:' class='btn btn-info btn-md add-row' ><i
                                                                        class='fa fa-plus'></i>&nbsp;Add</a>
                                                            </td>

                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>

                                                </table><br>
                                                <!--<div class="col-md-8 offset-md-4">-->
                                                <!--    <button type="submit"-->
                                                <!--        class="btn btn-primary mr-1 mb-1">{{ translate('Save') }}</button>-->
                                                <!--    <button type="reset"-->
                                                <!--        class="btn btn-outline-warning mr-1 mb-1">Reset</button>-->
                                                <!--</div>-->

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        var question_array = [];
        var answer_array = [];
        $(".add-row").click(function() {

            savefaq();
        });

        $("#type").change(function() {

            type_select_fun();
        });

        type_select_fun();




        function savefaq() {
            var type = $('#type').val();
            var question = $("#question").val();
            var answer = $("#answer").val();

            $.ajax({
                type: 'post',
                url: "{{ route('faq.store') }}",
                data: {
                    'type': type,
                    question: question,
                    answer: answer,
                    '_token': $('input[name="_token"]').val()
                },
                dataType: 'json',
                success: function(data) {
                    $('#question').val('');
                            $("#answer").val('');
                    type_select_fun();
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: "Faq  details saved successfully",
                        showConfirmButton: true,

                    }).then(function(e) {
                        console.log(e)
                        if (e.value === true) {
                          
                        } else {
                            e.dismiss;
                        }

                    }, function(dismiss) {
                        return false;
                    });


                },
                error: function(jqXHR, textStatus, errorThrown) {

                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            var count = 0;
                            $.each(jqXHR.responseJSON['errors'], function(key, value) {
                                count++;
                                msg += "<li>" + count + ". " + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    Swal.fire({
                        icon: 'error',
                        title: '',
                        html: msg,
                        footer: ''
                    });
                }
            });
        }

        function type_select_fun() {

            var type = $("#type").val();
            $.ajax({
                type: 'post',
                url: 'faq.view_faq',
                data: {
                    'type': type,
                    '_token': $('input[name="_token"]').val()
                },
                dataType: 'json',
                success: function(data) {
                    $("table tbody").html('');
                    $("#tot_count").val(data.length + 1);
                    var k = 1;
                    $.each(data, function(key, value) {

                        var markup ="<tr class='table_tr'><td><input type='text' class='form-control' name='question" + k + "'  value='" + value.question +"' readonly='readonly' id='question_" + value.id +"'></td><td><textarea class='form-control' name='answer" +k + "'  readonly='readonly' id='answer_" + value.id + "'>" + value.answer +"</textarea></td>";


                        markup +="<td><button type='button' id='deleteId_" + value.id +"'  onclick='deleteRow(" + value.id +");' class='btn btn-danger btn-md'><i class='fa fa-trash'></i> Delete</button></td>";
                        markup += "<td ><button type='button' id='editId_" + value.id +"'   onclick='editRow(" + value.id +");' class='btn btn-info btn-md'><i class='fa fa-edit'></i> Edit</button></td></tr>";
                            markup += "<td ><button type='button'  style='display:none;' id='updateId" + value.id +"'   onclick='updateRow(" + value.id +");' class='btn btn-info btn-md'><i class='fa fa-file'></i> Update</button></td></tr>";
                        $("table tbody").append(markup);

                        k++;


                    });
                    // }


                }
            });
        }

        function updateRow(editid){
            var question = $('#question_'+editid).val();
            var answer = $('#answer_'+editid).val();
            var type = $('#type').val();
            $.ajax({
                type: 'post',
                url: "{{ route('faq.update') }}",
                data: {
                    'type': type,
                    'question': question,
                    'answer': answer,
                    'editid':editid,
                    '_token': $('input[name="_token"]').val()
                },
                dataType: 'json',
                success: function(data) {
                    type_select_fun();
                    $('#question_'+editid).attr('readonly', true);
                    $('#answer_'+editid).attr('readonly', true);
                    $('#editId_'+editid).show();
                    $('#updateId'+editid).hide();
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: "Faq  details updated successfully",
                        showConfirmButton: true,

                    }).then(function(e) {
                        console.log(e)
                        if (e.value === true) {


                        } else {
                            e.dismiss;
                        }

                    }, function(dismiss) {
                        return false;
                    });


                },
                error: function(jqXHR, textStatus, errorThrown) {

                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            var count = 0;
                            $.each(jqXHR.responseJSON['errors'], function(key, value) {
                                count++;
                                msg += "<li>" + count + ". " + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    Swal.fire({
                        icon: 'error',
                        title: '',
                        html: msg,
                        footer: ''
                    });
                }
            });
        }
        function editRow(editid) {
            $('#question_'+editid).removeAttr('readonly', true);
            $('#answer_'+editid).removeAttr('readonly', true);
            $('#editId_'+editid).hide();
            $('#updateId'+editid).show();

        }

        function deleteRow(editid) {
            Swal.fire({
            title: "Delete?",
            text: "Are you sure to delete this record?",
            type: "",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: !0
        }).then(function (e) {
            console.log(e)
            if (e.value === true) {
                $.ajax({
                type: 'post',
                url: "{{ route('faq.delete') }}",
                data: {
                   
                    'editid':editid,
                    '_token': $('input[name="_token"]').val()
                },
                dataType: 'json',
                success: function(data) {
                    type_select_fun();
                  
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: "Faq  details deleted successfully",
                        showConfirmButton: true,

                    }).then(function(e) {
                        console.log(e)
                        if (e.value === true) {


                        } else {
                            e.dismiss;
                        }

                    }, function(dismiss) {
                        return false;
                    });


                },
                error: function(jqXHR, textStatus, errorThrown) {

                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            var count = 0;
                            $.each(jqXHR.responseJSON['errors'], function(key, value) {
                                count++;
                                msg += "<li>" + count + ". " + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    Swal.fire({
                        icon: 'error',
                        title: '',
                        html: msg,
                        footer: ''
                    });
                }
            });

            } else {
                e.dismiss;
            }

        }, function (dismiss) {
            return false;
        })
           

        }

    </script>
@endsection
