     
@extends('admin.layout.admin_template')
@section('content')
 
 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
     
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
  <div class="row match-height">
      <div class="col-md-12 col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">{{ $ticket->subject }} #{{ $ticket->code }}</h4>
              </div>
              <div class="card-content">
                  <div class="card-body">
  <!--Horizontal Form-->
        <!--===================================================-->
       
             <ul class="mar-top list-inline">
                <li>{{ $ticket->user->name }}</li>
                <li>{{ date('d/m/Y h:m A', strtotime(trim(str_replace('/', '-', $ticket->created_at)))) }}</li>
                <li><span class="badge badge-pill badge-secondary">{{ ucfirst($ticket->status) }}</span></li>
            </ul>
        <form class="" action="{{ route('support_ticket.admin_store') }}" method="post" id="ticket-reply-form" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="ticket_id" value="{{$ticket->id}}" required>
                <input type="hidden" name="status" value="{{ $ticket->status }}" required>
                <div class="form-group">
                <textarea class="editor" name="reply" id="reply" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                    <!-- <textarea class="editor" name="reply"  required></textarea> -->
                   
                </div>
                <div class="form-group">
                    <input type="file" name="attachments[]" class="form-control" multiple>
                </div>
                <div class="form-group text-right pos-rel">
                   <a href="{{ route('support_ticket.admin_index') }}" class="btn btn-outline-success mr-1 mb-1" style="margin-top:12px;">{{translate('Cancel')}}</a>

                    <button type="button" class="btn btn-primary" onclick="submit_reply('pending')">{{ translate('Submit as') }} <strong>{{ ucfirst($ticket->status) }}</strong></button>
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li onclick="submit_reply('open')"><a href="#">{{ translate('Submit as') }} <strong>{{ translate('Open') }}</strong></a></li>
                        <li onclick="submit_reply('solved')"><a href="#">{{ translate('Submit as') }} <strong>{{ translate('Solved') }}</strong></a></li>
                        <!-- default new ticket status pending. after admin first reply it will be open -->
                    </ul>
                </div>
            </form>
            <div class="pad-top">
                @foreach($ticket->ticketreplies as $ticketreply)
                    <div class="media bord-top pad-top">
                        <a class="media-left" href="#">
                            @if($ticketreply->user->avatar_original != '')
                                <img class="img-circle img-sm" src="{{ asset($ticketreply->user->avatar_original) }}">
                            @else
                                <img class="img-circle img-sm" src="{{ asset('frontend/images/user.png') }}">
                            @endif
                        </a>
                        <div class="media-body">
                            <div class="comment-header">
                                <a href="#" class="media-heading box-inline text-main text-bold">{{ $ticketreply->user->name }}</a>
                                <p class="text-muted text-sm">{{$ticketreply->created_at}}</p>
                            </div>
                            <div>
                                @php
                                    echo $ticketreply->reply;
                                @endphp
                                @if($ticketreply->files != null && is_array(json_decode($ticketreply->files)))
                                    <div>
                                        @foreach (json_decode($ticketreply->files) as $key => $file)
                                            <div>
                                                <a href="{{ asset($file->path) }}" download="{{ $file->name }}" class="support-file-attach bg-gray pad-all rounded">
                                                    <i class="fa fa-link mar-rgt"></i> {{ $file->name }}
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="media bord-top pad-top">
                    <a class="media-left" href="#">
                    @if ($ticket->user->avatar_original != '')
                    <img loading="lazy"  class="img-circle img-sm" alt="Profile Picture" src="{{ asset($ticket->user->avatar_original) }}">
                    @else
                    <img loading="lazy"  class="img-circle img-sm" alt="Profile Picture" src="/img/Blank.jpg">
                  
                    @endif
                    </a>
                    
                    <div class="media-body">
                        <div class="comment-header">
                            <a href="#" class="media-heading box-inline text-main text-bold">{{ $ticket->user->name }}</a>
                            <p class="text-muted text-sm">{{ date('d/m/Y h:m A', strtotime(trim(str_replace('/', '-', $ticket->created_at))))}}</p>
                        </div>
                        <p>
                            @php
                                echo $ticket->details;
                            @endphp
                            @if($ticket->files != null && is_array(json_decode($ticket->files)))
                                <div>
                                    @foreach (json_decode($ticket->files) as $key => $file)
                                        <div>
                                            <a href="{{ asset($file->path) }}" download="{{ $file->name }}" class="support-file-attach bg-gray pad-all rounded">
                                                <i class="fa fa-link mar-rgt"></i> {{ $file->name }}
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </p>
                        
                    </div>
                </div>
            </div>
        <!--===================================================-->
        <!--End Horizontal Form-->
                  </div>
              </div>
          </div>
      </div>
      
  </div>
</section>
<!-- // Basic Horizontal form layout section end -->
<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->
  
@endsection



@section('script')
<script type="text/javascript">
        function submit_reply(status){  
            $('input[name=status]').val(status);
           //var reply= $('#reply').val();
           var reply = CKEDITOR.instances['reply'].getData();
           //alert(reply);
            if(reply !=''){ //alert('bi');
                $('#ticket-reply-form').submit();
            }
        }

        var editor1 = CKEDITOR.replace('reply', {
      extraAllowedContent: 'div(*);h3(*);img(*);h1(*);button(*);h2(*);h4(*);ul(*);a(*);section(*);li(*);span(*);p(*)',
      height: 400,
    
    });
    editor1.on('instanceReady', function() {
      // Output self-closing tags the HTML4 way, like <br>.
      this.dataProcessor.writer.selfClosingEnd = '>';

      // Use line breaks for block elements, tables, and lists.
      var dtd = CKEDITOR.dtd;
      for (var e in CKEDITOR.tools.extend({}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent)) {
        this.dataProcessor.writer.setRules(e, {
          indent: true,
          breakBeforeOpen: true,
          breakAfterOpen: true,
          breakBeforeClose: true,
          breakAfterClose: true
        });
      }
      // Start in source mode.
      //this.setMode('source');
    });
  </script>
    
@endsection
