@extends('admin.layout.admin_template')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body">
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{translate('Customer Support Desk')}}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">
                    <div class="row">
                            <div class="col-sm-12">
                            <form class="" id="sort_support" action="" method="GET">
                                <div class="box-inline pad-rgt pull-left">
                                    <div class="" style="min-width: 300px;">
                                        <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="{{ translate('Type ticket code & Enter') }}">
                                    </div>
                                </div>
                                <div class="box-inline pad-rgt pull-left">
                                    <div class="" style="min-width: 300px;">
                                        <input type="text" class="form-control" id="search_email" name="search_email" placeholder="{{ translate('Enter user Email') }}">
                                    </div>
                                </div>
                                <div class="box-inline pad-rgt pull-left">
                                    <div class="" style="min-width: 200px;margin-left:10px;">
                                    <button class="btn btn-primary mr-1 mb-1" type="submit">{{ translate('Filter') }}</button>
                                    </div>
                                </div>
                            </form>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                        <th>{{ translate('Ticket ID') }}</th>
                                        <th>{{ translate('Sending Date') }}</th>
                                        <th>{{ translate('Subject') }}</th>
                                        <th>{{ translate('User') }}</th>
                                        <th>{{ translate('Email Id') }}</th>
                                        <th>{{ translate('Status') }}</th>
                                        <th>{{ translate('Last reply') }}</th>
                                        <th>{{ translate('View') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($tickets as $key => $ticket)
                                        @if ($ticket->user != null)
                                            <tr>
                                                <td>#{{ $ticket->code }}</td>
                                                <td>{{ date('d/m/Y h:m A', strtotime(trim(str_replace('/', '-', $ticket->created_at)))) }} @if($ticket->viewed == 0) <span class="pull-right badge badge-info">{{ translate('New') }}</span> @endif</td>
                                                <td>{{ $ticket->subject }}</td>
                                                <td>{{ $ticket->user->name }}</td>
                                                <td>{{ $ticket->user->email }}</td>
                                                <td>
                                                    @if ($ticket->status == 'pending')
                                                        <span class="badge badge-pill badge-danger">{{ translate('Pending') }}</span>
                                                    @elseif ($ticket->status == 'open')
                                                        <span class="badge badge-pill badge-secondary">{{ translate('Open') }}</span>
                                                    @else
                                                        <span class="badge badge-pill badge-success">{{ translate('Solved') }}</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if (count($ticket->ticketreplies) > 0)
                                                        {{ date('d/m/Y h:m A', strtotime(trim(str_replace('/', '-', $ticket->ticketreplies->last()->created_at)))) }}
                                                    @else
                                                        {{ date('d/m/Y h:m A', strtotime(trim(str_replace('/', '-', $ticket->created_at)))) }}
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{route('support_ticket.admin_show', encrypt($ticket->id))}}" class="btn-link"><i class="fa fa-eye"></i></a>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                              <tfoot>
                                    <tr>
                                    <th>{{ translate('Ticket ID') }}</th>
                                        <th>{{ translate('Sending Date') }}</th>
                                        <th>{{ translate('Subject') }}</th>
                                        <th>{{ translate('User') }}</th>
                                        <th>{{ translate('Email Id') }}</th>
                                        <th>{{ translate('Status') }}</th>
                                        <th>{{ translate('Last reply') }}</th>
                                        <th>{{ translate('View') }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="clearfix">
                                <div class="pull-right">
                                {{ $tickets->appends(request()->input())->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
</div>
</div>

@endsection
