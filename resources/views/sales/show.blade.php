@extends('admin.layout.admin_template')
@section('content')

   <!-- BEGIN: Content-->
   <div class="app-content content">
	<div class="content-overlay"></div>
	<div class="header-navbar-shadow"></div>
	<div class="content-wrapper">
	  <div class="content-header row">
		<div class="content-header-left col-md-9 col-12 mb-2">
		  <div class="row breadcrumbs-top">
			<div class="col-12">
			  <h2 class="content-header-title float-left mb-0">{{translate('Order Details')}}</h2>
			  <div class="breadcrumb-wrapper col-12">
				<ol class="breadcrumb">
				  <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">{{translate('Home')}}</a>
				  </li>
				  <li class="breadcrumb-item"><a href="{{route('sales.index')}}">{{translate('Total Sales')}}</a>
				  </li>
				  <li class="breadcrumb-item active">{{translate('Order Details')}}
				  </li>
				</ol>
			  </div>
			</div>
		  </div>
		</div>
		<!--<div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
		  <div class="form-group breadcrum-right">
			<div class="dropdown">
			  <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>
			  <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a></div>
			</div>
		  </div>
		</div>-->
	  </div>
	  <div class="content-body">

		<section id="grid-options" class="row">
			<div class="col-md-6">
			  <div class="card">
				<div class="card-header">
				  <h4 class="card-title">{{translate('Purchased By')}}</h4>
				</div>
				<div class="card-content">
				  <div class="card-body">
					<p style="font-weight:bold;  font-size:17px;">
						{{ json_decode($order->shipping_address)->name }}<br>
						{{ json_decode($order->shipping_address)->email }}<br>
						{{ json_decode($order->shipping_address)->address }}, {{ json_decode($order->shipping_address)->city }}, {{ json_decode($order->shipping_address)->country }}
					</p>
					<p style="font-weight:bold;  font-size:17px;">
						@if ($order->manual_payment && is_array(json_decode($order->manual_payment_data, true)))
                        <br>
                        <strong class="text-main">{{ translate('Payment Information') }}</strong><br>
                        {{ translate('Name') }}: {{ json_decode($order->manual_payment_data)->name }}, {{ translate('Amount') }}: {{ single_price(json_decode($order->manual_payment_data)->amount) }}, {{ translate('TRX ID') }}: {{ json_decode($order->manual_payment_data)->trx_id }}
                        <br>
                        <a href="{{ asset(json_decode($order->manual_payment_data)->photo) }}" target="_blank"><img src="{{ asset(json_decode($order->manual_payment_data)->photo) }}" alt="" height="100"></a>
                    @endif
					</p>
				
					
				  </div>
				</div>
			  </div>
			</div>



			<div class="col-md-6">
				<div class="card">
				  <div class="card-header">
					<h4 class="card-title">{{translate('Order Details')}}</h4>
				  </div>
				  <div class="card-content">
					<div class="card-body">
					  
				  
					  <div class="table-responsive">
						<table class="table table-bordered table-striped">
						  <thead>
							<tr>
							 
							  <th class="text-center">
								{{translate('Order #')}}<br />
							
							  </th>
							  <th class="text-center">
								{{translate('Order Status')}}<br />
								
							  </th>
							  <th class="text-center">
								{{translate('Order Date')}}<br />
							
							  </th>
							  <th class="text-center">
								{{translate('Total amount')}}<br />
								
							  </th>
							  <th class="text-center">
								{{translate('Payment method')}}<br />
								
							  </th>
							</tr>
						  </thead>
						  <tbody>
							<tr>
							 
							  <td>	{{ $order->code }}</td>
							  @php
							  $status = $order->orderDetails->first()->delivery_status;
						  @endphp
							  <td>   @if($status == 'delivered')
                                <span class="badge badge-success">{{ ucfirst(str_replace('_', ' ', $status)) }}</span>
                            @else
                                <span class="badge badge-info">{{ ucfirst(str_replace('_', ' ', $status)) }}</span>
                            @endif</td>
							  <td>{{ date('d-m-Y h:i A', $order->date) }} (UTC)</td>
							  <td>{{ single_price($order->orderDetails->sum('price') + $order->orderDetails->sum('tax')) }}</td>
							  <td>{{ ucfirst(str_replace('_', ' ', $order->payment_type)) }}</td>
							</tr>
							
						  </tbody>
						</table>
					  </div>
					</div>
				  </div>
				</div>
			  </div>


			  <!-- Striped rows start -->

<!-- Striped rows end -->
		  </section>

<section>
	<div class="row" id="table-striped">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Order Summary</h4>
				</div>
				<div class="card-content">
					<div class="card-body">
						
					</div>
					<div class="table-responsive">
						<table class="table table-striped mb-0">
							<thead>
								<tr>
									<th scope="col">{{translate('Sl No')}}</th>
									<th scope="col">{{translate('Photo')}}</th>
									<th scope="col">{{translate('Description')}}</th>
									<th scope="col">{{translate('Delivery Type')}}</th>
									<th scope="col">{{translate('Qty')}}</th>
									<th scope="col">{{translate('Price')}}</th>
									<th scope="col">{{translate('Total')}}</th>
								</tr>
						  </thead>
						  <tbody>
							@foreach ($order->orderDetails as $key => $orderDetail)
							<tr>
								<td>{{ $key+1 }}</td>
								<td>
									@if ($orderDetail->product != null)
										<a href="{{ route('product', $orderDetail->product->slug) }}" target="_blank"><img height="50" src={{ asset($orderDetail->product->thumbnail_img) }}/></a>
									@else
										<strong>{{ translate('N/A') }}</strong>
									@endif
								</td>
								<td>
									@if ($orderDetail->product != null)
										<strong><a href="{{ route('product', $orderDetail->product->slug) }}" target="_blank">{{ $orderDetail->product->name }}</a></strong>
										<small>{{ $orderDetail->variation }}</small>
									@else
										<strong>{{ translate('Product Unavailable') }}</strong>
									@endif
								</td>
								<td>
									@if ($orderDetail->shipping_type != null && $orderDetail->shipping_type == 'home_delivery')
										{{ translate('Home Delivery') }}
									@elseif ($orderDetail->shipping_type == 'pickup_point')
										@if ($orderDetail->pickup_point != null)
											{{ $orderDetail->pickup_point->name }} ({{ translate('Pickup Point') }})
										@else
											{{ translate('Pickup Point') }}
										@endif
									@endif
								</td>
								<td class="text-center">
									{{ $orderDetail->quantity }}
								</td>
								<td class="text-center">
									{{ single_price($orderDetail->price/$orderDetail->quantity) }}
								</td>
								<td class="text-center">
									{{ single_price($orderDetail->price) }}
								</td>
							</tr>
						@endforeach
						  </tbody>
						</table>
						<div class="clearfix">
							<table class="table invoice-total">
							<tbody>
							<tr>
								<td>
									<strong>{{translate('Sub Total')}} :</strong>
								</td>
								<td>
									{{ single_price($order->orderDetails->sum('price')) }}
								</td>
							</tr>
							<tr>
								<td>
									<strong>{{translate('Tax')}} :</strong>
								</td>
								<td>
									{{ single_price($order->orderDetails->sum('tax')) }}
								</td>
							</tr>
							<tr>
								<td>
									<strong>{{translate('Shipping')}} :</strong>
								</td>
								<td>
									{{ single_price($order->orderDetails->sum('shipping_cost')) }}
								</td>
							</tr>
							<tr>
								<td>
									<strong>{{translate('Coupon Discount')}} :</strong>
								</td>
								<td>
									{{ single_price($order->coupon_discount) }}
								</td>
							</tr>
							<tr>
								<td>
									<strong>{{translate('Wallet Amount Used')}} :</strong>
								</td>
								<td>
									{{ single_price($order->wallet_amount) }}
								</td>
							</tr>
							<tr>
								<td>
									<strong>{{translate('TOTAL')}} :</strong>
								</td>
								<td class="text-bold h4">
									{{ single_price($order->grand_total) }}
								</td>
							</tr>
							</tbody>
							</table>
						</div>
						<div class="text-right no-print">
							<a href="{{ route('customer.invoice.download', $order->id) }}" class="btn btn-default"><i class="fa fa-print" aria-hidden="true"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


</section>







	  </div>
	</div>
  </div>
  <!-- END: Content-->
@endsection
