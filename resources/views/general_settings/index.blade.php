     
@extends('admin.layout.admin_template')
@section('content')
 
 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
     
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
  <div class="row match-height">
      <div class="col-md-12 col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">{{translate('General Settings')}}</h4>
              </div>
              <div class="card-content">
                  <div class="card-body">
  <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('generalsettings.update',$generalsetting->id ) }}" method="POST" enctype="multipart/form-data">
            	@csrf
                <input type="hidden" name="_method" value="PATCH">
            <div class="form-body">
			<div class="row">
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Site Name')}}</span></div>
					       <div class="col-md-8">
						   <input type="text" id="name" name="name" value="{{ $generalsetting->site_name }}" class="form-control" required>
                        </div>
				        </div>
			        </div>
			        <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Address')}}</span></div>
					       <div class="col-md-8">
                           <input type="text" id="address" name="address" value="{{ $generalsetting->address }}" class="form-control" required>
                        </div>
				        </div>
			        </div>
			
			        <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Footer Text')}}</span></div>
					       <div class="col-md-8">
                           <textarea class="editor" name="content" required><?php echo $generalsetting->description; ?></textarea>
						   <!-- <textarea class="form-control editor" rows="4" name="description11" required>{{$generalsetting->description}}</textarea> -->
                        </div>
				        </div>
			        </div>
                <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Phone')}}</span></div>
					       <div class="col-md-8">
                           <input type="text" id="phone" name="phone" value="{{ $generalsetting->phone }}" class="form-control" required>
                        </div>
				        </div>
			        </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Email')}}</span></div>
					       <div class="col-md-8">
                           <input type="text" id="email" name="email" value="{{ $generalsetting->email }}" class="form-control" required>
                        </div>
				        </div>
			        </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Facebook')}}</span></div>
					       <div class="col-md-8">
                           <input type="text" id="facebook" name="facebook" value="{{ $generalsetting->facebook }}" class="form-control">
                        </div>
				        </div>
			        </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Instagram')}}</span></div>
					       <div class="col-md-8">
                           <input type="text" id="instagram" name="instagram" value="{{ $generalsetting->instagram }}" class="form-control">
                        </div>
				        </div>
			        </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Twitter')}}</span></div>
					       <div class="col-md-8">
                           <input type="text" id="twitter" name="twitter" value="{{ $generalsetting->twitter }}" class="form-control">
                        </div>
				        </div>
			        </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Youtube')}}</span></div>
					       <div class="col-md-8">
                           <input type="text" id="youtube" name="youtube" value="{{ $generalsetting->youtube }}" class="form-control">
                        </div>
				        </div>
			        </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Google Plus')}}</span></div>
					       <div class="col-md-8">
                           <input type="text" id="google_plus" name="google_plus" value="{{ $generalsetting->google_plus }}" class="form-control">
                        </div>
				        </div>
			        </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{__('System Timezone')}}</span></div>
					       <div class="col-md-8">
                           <select name="timezone" class="form-control demo-select2" data-live-search="true">
                                @foreach (timezones() as $key => $value)
                                    <option value="{{ $value }}" @if (app_timezone() == $value)
                                        selected
                                    @endif>{{ $key }}</option>
                                @endforeach
                            </select>
                        </div>
				        </div>
			        </div>
               
				
				 <div class="col-md-8 offset-md-4">
                                      <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                      <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                  </div>
								  
            </div>
             </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->


                  </div>
              </div>
          </div>
      </div>
      
  </div>
</section>
<!-- // Basic Horizontal form layout section end -->


<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->
 
  @endsection
  @section('script')
  <script>
var editor1 = CKEDITOR.replace('content', {
      extraAllowedContent: 'div(*);h3(*);img(*);h1(*);button(*);h2(*);h4(*);ul(*);a(*);section(*);li(*);span(*);p(*)',
      height: 400,
      filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
      filebrowserUploadMethod: 'form'
    
    });
    editor1.on('instanceReady', function() {
      // Output self-closing tags the HTML4 way, like <br>.
      this.dataProcessor.writer.selfClosingEnd = '>';

      // Use line breaks for block elements, tables, and lists.
      var dtd = CKEDITOR.dtd;
      for (var e in CKEDITOR.tools.extend({}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent)) {
        this.dataProcessor.writer.setRules(e, {
          indent: true,
          breakBeforeOpen: true,
          breakAfterOpen: true,
          breakBeforeClose: true,
          breakAfterClose: true
        });
      }
      // Start in source mode.
      //this.setMode('source');
    });


  </script>
  @stop