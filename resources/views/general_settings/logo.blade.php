     
@extends('admin.layout.admin_template')
@section('content')
 
 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
     
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
  <div class="row match-height">
      <div class="col-md-12 col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">{{translate('Logo Settings')}}</h4>
              </div>
              <div class="card-content">
                  <div class="card-body">
  <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('generalsettings.logo.store') }}" method="POST" enctype="multipart/form-data">
            	@csrf
            <div class="form-body">
			<div class="row">
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Frontend logo')}}</span><small>(max height 40px)</small></div>
					       <div class="col-md-8">
						   <input type="file" id="logo" name="logo" class="form-control">
                        </div>
				        </div>
			        </div>
			        <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Admin logo')}} </span><small>(60x60)</small></div>
					       <div class="col-md-8">
                           <input type="file" id="admin_logo" name="admin_logo" class="form-control">
                        </div>
				        </div>
			        </div>
			
			        <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Favicon')}}</span><small>(32x32)</small></div>
					       <div class="col-md-8">
						   <input type="file" id="favicon" name="favicon" class="form-control">
                        </div>
				        </div>
			        </div>
                <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Admin login background image')}}</span> <small>(1920x1080)</small></div>
					       <div class="col-md-8">
                           <input type="file" id="admin_login_background" name="admin_login_background" class="form-control">
                        
					       </div>
				        </div>
			        </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Admin login sidebar image')}} </span><small>(600x500)</small></div>
					       <div class="col-md-8">
                           <input type="file" id="admin_login_sidebar" name="admin_login_sidebar" class="form-control">
                        </div>
				        </div>
			        </div>
               
				
				 <div class="col-md-8 offset-md-4">
                                      <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                      <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                  </div>
								  
            </div>
             </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->


                  </div>
              </div>
          </div>
      </div>
      
  </div>
</section>
<!-- // Basic Horizontal form layout section end -->


<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->

  @endsection
