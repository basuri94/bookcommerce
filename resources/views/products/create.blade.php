@extends('admin.layout.admin_template')
@section('content')

 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
    <div class="content-body"><!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
  <div class="row match-height">
      <div class="col-md-12 col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">{{ translate('Add Book Details') }}</h4>
              </div>
              <div class="card-content">
                  <div class="card-body">
  <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form form-horizontal mar-top" action="{{route('products.store')}}" method="POST" enctype="multipart/form-data" id="choice_form">
			@csrf
			<input type="hidden" name="added_by" value="admin">
            <div class="form-body">
			<div class="row" style="padding:1%;">
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span class="required">{{translate('Name ( Bengali & English )')}}</span></div>
					         <div class="col-md-8">
							  <input type="text" class="form-control" name="name" placeholder="{{ translate('Name ( Bengali & English )') }}" onchange="update_sku()" required>
							</div>
				        </div>
			        </div>
			        <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span class="required">{{translate('Category')}}</span></div>
					       <div class="col-md-8">
						   <select class="form-control demo-select2-placeholder" name="category_id" id="category_id" required>
								@foreach($categories as $category)
								   @if($category->digital == 0)
									<option value="{{$category->id}}">{{__($category->name)}}</option>
									@endif
								@endforeach
							</select>
					       </div>
				        </div>
			        </div>
			
			        <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span class="required">{{translate('Subcategory')}}</span></div>
					       <div class="col-md-8">
						   <select class="form-control demo-select2-placeholder" name="subcategory_id" id="subcategory_id" required>

							</select>
					       </div>
				        </div>
			        </div>
                <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span class="">{{translate('Sub Subcategory')}}</span></div>
					       <div class="col-md-8">
						   <select class="form-control demo-select2-placeholder" name="subsubcategory_id" id="subsubcategory_id" >

							</select>
					       </div>
				        </div>
			        </div>

					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span class="required">{{translate('Hsn No')}}</span></div>
					       <div class="col-md-8">
						   <select class="form-control demo-select2-placeholder" name="SubCategoryHsn_id" id="SubCategoryHsn_id" required>

							</select>
					       </div>
				        </div>
			        </div>

					

					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span class="required">{{translate('Publishers')}}</span></div>
					       <div class="col-md-8">
						   <select class="form-control demo-select2-placeholder" name="brand_id" id="brand_id" required>
								<option value="">{{ ('Select Publishers') }}</option>
								@foreach (\App\Brand::all() as $brand)
									<option value="{{ $brand->id }}">{{ $brand->name }}</option>
								@endforeach
							</select>
					       </div>
				        </div>
			        </div>

					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span class="required">{{translate('Book Pages')}}</span></div>
					       <div class="col-md-8">
						   <input type="text" class="form-control" name="unit" placeholder="{{ translate('Book Pages') }}" value="">
                                            <span Style="font-weight: bold; font-size:15px;"></span>

								</div>
				        </div>
			        </div>
				
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span>{{translate('Tags')}}</span></div>
					       <div class="col-md-8">
							<input type="text" class="form-control" name="tags[]" placeholder="{{ translate('Type to add a tag') }}" data-role="tagsinput">
							</div>
				        </div>
			        </div>
					@php
					    $pos_addon = \App\Addon::where('unique_identifier', 'pos_system')->first();
					@endphp
					@if ($pos_addon != null && $pos_addon->activated == 1)
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span>{{translate('Barcode')}}</span></div>
					       <div class="col-md-8">
						   <select class="form-control demo-select2-placeholder" name="brand_id" id="brand_id">
								<option value="">{{ ('Select Brand') }}</option>
								@foreach (\App\Brand::all() as $brand)
									<option value="{{ $brand->id }}">{{ $brand->name }}</option>
								@endforeach
							</select>
					       </div>
				        </div>
			        </div>
					@endif
					@php
					    $refund_request_addon = \App\Addon::where('unique_identifier', 'refund_request')->first();
					@endphp
					{{-- @if ($refund_request_addon != null && $refund_request_addon->activated == 1) --}}
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span>{{translate('Refundable')}}</span></div>
					       <div class="col-md-8">
						   <label class="switch" style="margin-top:5px;">
									<input type="checkbox" name="refundable" checked>
		                            <span class="slider round"></span></label>
								</label>
					       </div>
				        </div>
					</div>
					
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span>{{translate('No Contact Delivary')}}</span></div>
					       <div class="col-md-8">
						   <label class="switch" style="margin-top:5px;">
									<input type="checkbox" name="no_delivery" checked>
		                            <span class="slider round"></span></label>
								</label>
					       </div>
				        </div>
					</div>
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span>{{translate('7 Days Return')}}</span></div>
					       <div class="col-md-8">
						   <label class="switch" style="margin-top:5px;">
									<input type="checkbox" name="days_return" checked>
		                            <span class="slider round"></span></label>
								</label>
					       </div>
				        </div>
					</div>
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span>{{translate('Brand Warrenty')}}</span></div>
					       <div class="col-md-8">
						   <label class="switch" style="margin-top:5px;">
									<input type="checkbox" name="b_warrenty" checked>
		                            <span class="slider round"></span></label>
								</label>
					       </div>
				        </div>
					</div>
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span>{{translate('Cash On Delivery')}}</span></div>
					       <div class="col-md-8">
						   <label class="switch" style="margin-top:5px;">
									<input type="checkbox" name="cod" checked>
		                            <span class="slider round"></span></label>
								</label>
					       </div>
				        </div>
					</div>
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span>{{translate('Online Payment')}}</span></div>
					       <div class="col-md-8">
						   <label class="switch" style="margin-top:5px;">
									<input type="checkbox" name="online_payment" checked>
		                            <span class="slider round"></span></label>
								</label>
					       </div>
				        </div>
					</div>
					
					{{-- @endif --}}
                </div>
             </div>
					<div class="panel" style="padding:1%; background-color:#FFFFFF;">
						<div class="panel-heading bord-btm">
							<h3 class="panel-title">{{translate('Book Images')}}</h3>
						</div>
						 <div class="form-body">
							<div class="form-group row">
									<div class="col-12">
										<div class="form-group row">
											<div class="col-md-3"><span>{{translate('Gallery Images')}}</span></div>
											 <div class="col-md-8">
											 <div id="photos" ></div>
											  </div>
										</div>
									</div>
									
									<div class="col-12">
										<div class="form-group row">
											<div class="col-md-3"><span>{{translate('Thumbnail Image')}}</span>&nbsp;<span>(200x280 px)</span></div>
											 <div class="col-md-8">
											 <div  for="file-2" id="thumbnail_img"></div>
											  </div>
										</div>
									</div>
									
									<div class="col-12">
										<div class="form-group row">
											<div class="col-md-3" ><span>{{translate('Featured')}}</span> <span>(200x280 px)</span></div>
											 <div class="col-md-8">
											 <div for="file-3" id="featured_img"></div>
											  </div>
										</div>
									</div>
									
									<div class="col-12">
										<div class="form-group row">
											<div class="col-md-3" ><span>{{translate('Flash Deal')}}</span><span>(200x280 px)</span></div>
											 <div class="col-md-8">
											 <div for="file-4" id="flash_deal_img"></div>
											  </div>
										</div>
									</div>
									
									
									
							</div>
						</div>
						
					   </div>
				    </div>
			<div class="panel" style="padding:1%;">
				<div class="panel-heading bord-btm">
					<h3 class="panel-title">{{translate('Product Videos')}}</h3>
				</div>
				<div class="form-body">
					<div class="row">
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{translate('Video Provider')}}</span></div>
									 <div class="col-md-8">
									 <select class="form-control demo-select2-placeholder" name="video_provider" id="video_provider">
										<option value="youtube">{{translate('Youtube')}}</option>
										<option value="dailymotion">{{translate('Dailymotion')}}</option>
										<option value="vimeo">{{translate('Vimeo')}}</option>
									</select>
									  </div>
								</div>
							</div>
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{translate('Video Link')}}</span></div>
									 <div class="col-md-8">
									 <input type="text" class="form-control" name="video_link" placeholder="{{ translate('Video Link') }}">
									  </div>
								</div>
							</div>
				    </div>
				</div>
				
			</div>
	
			<div class="panel" >
				<div class="panel-heading bord-btm">
					<h3 class="panel-title">{{translate('Product Variation')}}</h3>
				</div>
				<div class="panel-body ">
				
					<div class="form-group row">
						<div class="col-lg-3">
							<input type="text" class="form-control" value="{{translate('Languages')}}" disabled>
						</div>
						<div class="col-lg-7">
							<select class="form-control color-var-select" name="colors" id="colors"  disabled>
								@foreach (\App\Color::orderBy('name', 'asc')->get() as $key => $color)
									<option value="{{ $color->code }}">{{ $color->name }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-lg-2">
							<label class="switch" style="margin-top:5px;">
								<input value="1" type="checkbox" name="colors_active">
								<span class="slider round"></span>
							</label>
						</div>
					</div>
				
					<div class="form-group row">
						<div class="col-lg-3">
							<input type="text" class="form-control" value="{{translate('Weight Class')}}" disabled>
						</div>
	                    <div class="col-lg-8">
	                        <select name="choice_attributes" id="choice_attributes" class="form-control demo-select2" placeholder="{{ translate('Choose Weight Class') }}">
								@foreach (\App\Attribute::all() as $key => $attribute)
									<option value="{{ $attribute->id }}">{{ $attribute->name }}</option>
								@endforeach
	                        </select>
	                    </div>
	                </div>

					<div>
						<p>{{ translate('Choose the attributes of this product and then input values of each attribute') }}</p>
						<br>
					</div>
					
					<div class="customer_choice_options " id="customer_choice_options">

					</div>
					
					
				</div>
			</div>
			<div class="panel">
				<div class="panel-heading bord-btm">
					<h3 class="panel-title">{{translate('Book price + stock')}}</h3>
				</div>
				<div class="panel-body">
					<div class="form-group row">
						<label class="col-lg-3 control-label">{{translate('M.R.P.')}}</label>
						<div class="col-lg-8">
							<input type="number" min="0" value="0" step="0.01" placeholder="{{ translate('M.R.P.') }}" name="unit_price" class="form-control" required>
						</div>
					</div>
					<!-- <div class="form-group row">
						<label class="col-lg-3 control-label">{{translate('Purchase price')}}</label>
						<div class="col-lg-8">
							<input type="number" min="0" value="0" step="0.01" placeholder="{{ translate('Purchase price') }}" name="purchase_price" class="form-control" required>
						</div>
					</div> -->
					<div class="form-group row">
						<label class="col-lg-3 control-label">{{translate('GST')}}</label>
						<div class="col-lg-7">
						<input type="number" min="0" value="0" step="0.01" placeholder="{{ translate('GST') }}" name="tax" id="tax" class="form-control" readonly="readonly" required>
						</div>
						<div class="col-md-1">
						<input type="text"  value="Percent" placeholder="{{ translate('Percent') }}" name="tax_type"  class="form-control" readonly="readonly" required>
					
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-3 control-label">{{translate('Discount')}}</label>
						<div class="col-lg-7">
							<input type="number" min="0" value="0" step="0.01" placeholder="{{ translate('Discount') }}" name="discount" class="form-control" required>
						</div>
						<div class="col-lg-1">
							<select class="demo-select2" name="discount_type">
								<option value="amount">{{translate('Flat')}}</option>
								<option value="percent">{{translate('Percent')}}</option>
							</select>
						</div>
					</div>
					<div class="form-group row" id="quantity">
						<label class="col-lg-3 control-label">{{translate('Quantity')}}</label>
						<div class="col-lg-8">
							<input type="number" min="0" value="0" step="1" placeholder="{{ translate('Quantity') }}" name="current_stock" class="form-control" required>
						</div>
					</div>
					<br>
					<div class="form-group row col-lg-12">
					<div class="sku_combination col-lg-12" id="sku_combination"></div>

					</div>
				</div>
			</div>
			
			<div class="panel" style="padding:1%;">
				<div class="panel-heading bord-btm">
					<h3 class="panel-title">{{translate('Book Description')}}</h3>
				</div>
				<div class="form-body">
					<div class="row">
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{translate('Description')}}</span></div>
									 <div class="col-md-8">
									 <textarea class="editor" name="description_product"></textarea>
									  </div>
								</div>
							</div>
				    </div>
				</div>
				
			</div>

			<div class="panel" style="padding:1%;">
				<div class="panel-heading bord-btm">
					<h3 class="panel-title">{{ translate('Specification')}}</h3>
				</div>
				<div class="form-body">
					<div class="row">
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{ translate('Specification')}}</span></div>
									 <div class="col-md-8">
									 <textarea class="" name="specification_product"></textarea>
									  </div>
								</div>
							</div>
				    </div>
				</div>
				
			</div>


            @if (\App\BusinessSetting::where('type', 'shipping_type')->first()->value == 'product_wise_shipping')
                <div class="panel" style="padding:1%;">
    				<div class="panel-heading bord-btm">
    					<h3 class="panel-title">{{translate('Product Shipping Cost')}}</h3>
    				</div>
					<div class="form-body">
					<div class="row">
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{translate('Free Shipping')}}</span></div>
									 <div class="col-md-2">
									 <span>{{translate('Status')}}</span>
									  </div>
									 <div class="col-md-7">
									 <label class="switch" style="margin-top:5px;">
    										<input type="radio" name="shipping_type" value="free" checked>
    										<span class="slider round"></span>
    									</label>
									  </div>
								</div>
							</div>
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{translate('Flat Rate')}}</span></div>
									<div class="col-md-2"><span>{{translate('Status')}}</span></div>
									 <div class="col-md-7">
									     <label class="switch" style="margin-top:5px;">
    										<input type="radio" name="shipping_type" value="flat_rate" checked>
    										<span class="slider round"></span>
    									</label>
									  </div>
								</div>
							</div>
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{translate('Shipping cost')}}</span></div>
									 <div class="col-md-8">
									    <input type="number" min="0" value="0" step="0.01" placeholder="{{ translate('Shipping cost') }}" name="flat_shipping_cost" class="form-control" required>
    								</div>
								</div>
							</div>
				    </div>
				</div>
					
    			</div>
            @endif
			<div class="panel" style="padding:1%;">
				<div class="panel-heading bord-btm">
					<h3 class="panel-title">{{translate('Size Chart')}}</h3>
				</div>
				<div class="form-body">
					<div class="row">
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{translate('Size Chart')}}</span></div>
									 <div class="col-md-8">
									 <input type="file" class="form-control" placeholder="{{ translate('PDF') }}" name="pdf" accept="application/pdf">
									  </div>
								</div>
							</div>
				    </div>
				</div>
				
			</div>
			<div class="panel" style="padding:1%;">
				<div class="panel-heading bord-btm">
					<h3 class="panel-title">{{translate('SEO Meta Tags')}}</h3>
				</div>
				<div class="form-body">
					<div class="row">
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{translate('Meta Title')}}</span></div>
									 <div class="col-md-8">
									 <input type="text" class="form-control" name="meta_title" placeholder="{{ translate('Meta Title') }}">
									  </div>
								</div>
							</div>
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{translate('Description')}}</span></div>
									 <div class="col-md-8">
									 <textarea name="meta_description" rows="8" class="form-control"></textarea>
									  </div>
								</div>
							</div>
							<div class="col-12" >
								<div class="form-group row">
									<div class="col-md-3"><span>{{ translate('Meta Image') }}</span></div>
									 <div class="col-md-8">
									 <div id="meta_photo" style="background-color:#FFFFFF;">

							          </div>
									  </div>
								</div>
							</div>
				    </div>
				</div>
				
				
			</div>
			
				
				<div class="col-md-8 offset-md-4">
					<button type="submit" name="button" class="btn btn-primary mr-1 mb-1">{{ translate('Add New Book') }}</button>
					<button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
					<a href="{{route('products.admin')}}" class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>
			   </div>
								  
            
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->


                  </div>
              </div>
          </div>
      </div>
      
  </div>
</section>
      </div>
    </div>
  </div>

  

@endsection

@section('script')
<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script type="text/javascript">
$(document).ready(function(){
$('.pdf_type').bind('change', function() {
    $('.label-important').remove(); 
var allowedExtensions =  
          /(\.pdf)$/i; 
    
  if (!allowedExtensions.exec(this.value)) { 
    $('.label-important').remove(); 
  $(this).after("<span class='label label-important' style='margin-left: 5%; color:#e63b05;font-weight: 600;font-size: 12px;'>Invalid file type Only .PDF Allowed</span>");
      this.value = ''; 
      return false; 
  }
  if(this.files[0].size/1024/1024 > 5){
    $('.label-important').remove(); 
    $(this).after("<span class='label label-important' style='margin-left: 5%;color: #e63b05;font-weight: 600;font-size: 12px;'>File size must be less than 5 mb</span>");
      this.value = ''; 
      return false; 
    }  
});
$('#file-2').change(function () {
//  alert('hi');
 var file = $(this)[0].files[0];
var fil_det=$(this);
 img = new Image();
 var imgwidth = 0;
 var imgheight = 0;
 var maxwidth = 200;
 var maxheight = 280;
 var isvalid_image=true;
 img.src = _URL.createObjectURL(file);
 img.onload = function(){
  
  imgwidth = this.width;
  imgheight = this.height;

  if(imgwidth != maxwidth && imgheight != maxheight){
    $('#file-2').val("");
    $('#file-2').next('label').find('span').html("");
  var msg= "Image size must be "+maxwidth+"X"+maxheight;
  showAlert("error",msg);
 }
};
img.onerror = function() {
$(this).val("");
  $(this).next('label').find('span').html("");
  var msg="not a valid file: " + file.type;
  showAlert("error",msg);
}
});
$('#file-3').change(function () {
//  alert('hi');
 var file = $(this)[0].files[0];
var fil_det=$(this);
 img = new Image();
 var imgwidth = 0;
 var imgheight = 0;
 var maxwidth = 200;
 var maxheight = 280;
 var isvalid_image=true;
 img.src = _URL.createObjectURL(file);
 img.onload = function(){
  
  imgwidth = this.width;
  imgheight = this.height;

  if(imgwidth != maxwidth && imgheight != maxheight){
    $('#file-3').val("");
    $('#file-3').next('label').find('span').html("");
  var msg= "Image size must be "+maxwidth+"X"+maxheight;
  showAlert("error",msg);
 }
};
img.onerror = function() {
$(this).val("");
  $(this).next('label').find('span').html("");
  var msg="not a valid file: " + file.type;
  showAlert("error",msg);
}
});
$('#file-4').change(function () {
//  alert('hi');
 var file = $(this)[0].files[0];
var fil_det=$(this);
 img = new Image();
 var imgwidth = 0;
 var imgheight = 0;
 var maxwidth = 200;
 var maxheight = 280;
 var isvalid_image=true;
 img.src = _URL.createObjectURL(file);
 img.onload = function(){
  
  imgwidth = this.width;
  imgheight = this.height;

  if(imgwidth != maxwidth && imgheight != maxheight){
    $('#file-4').val("");
    $('#file-4').next('label').find('span').html("");
  var msg= "Image size must be "+maxwidth+"X"+maxheight;
  showAlert("error",msg);
 }
};
img.onerror = function() {
$(this).val("");
  $(this).next('label').find('span').html("");
  var msg="not a valid file: " + file.type;
  showAlert("error",msg);
}
});
});
	function add_more_customer_choice_option(i, name){
		$('#customer_choice_options').append('<div class="form-group row"><div class="col-lg-3"><input type="hidden" name="choice_no[]" value="'+i+'"><input type="text" class="form-control" name="choice[]" value="'+name+'" placeholder="{{ translate('Choice Title') }}" readonly></div><div class="col-lg-8"><input type="text" class="form-control" name="choice_options_'+i+'[]" placeholder="{{ translate('Enter choice values') }}" data-role="tagsinput" onchange="update_sku()"></div></div>');

		$("input[data-role=tagsinput], select[multiple][data-role=tagsinput]").tagsinput();
	}


	$("#unit_name").on('change', function() {
		var unit_name=$("#unit_name").val();
		if(unit_name=='instock'){
           $('#ins_div').show();
		}else{
			$('#ins_div').hide();
		}
	    
	});


	$("#subcategory_id").on('change', function() { 
		var subcategory_id=$("#subcategory_id").val();
	    get_hsn_gst_by_subcategory_id(subcategory_id);
	});
	$("#SubCategoryHsn_id").on('change', function() {
		var SubCategoryHsn_id=$("#SubCategoryHsn_id").val();
	    get_gst_by_hsn_id(SubCategoryHsn_id);
	});

	$('input[name="colors_active"]').on('change', function() {
	    if(!$('input[name="colors_active"]').is(':checked')){
			$('#colors').prop('disabled', true);
		}
		else{
			$('#colors').prop('disabled', false);
		}
		update_sku();
	});

	$('#colors').on('change', function() {
	    update_sku();
	});

	$('input[name="unit_price"]').on('keyup', function() {
	    update_sku();
	});

	$('input[name="name"]').on('keyup', function() {
	    update_sku();
	});

	function delete_row(em){
		$(em).closest('.form-group').remove();
		update_sku();
	}

	function update_sku(){
		$.ajax({
		   type:"POST",
		   url:'{{ route('products.sku_combination') }}',
		   data:$('#choice_form').serialize(),
		   success: function(data){
			   $('#sku_combination').html(data);
			   if (data.length > 1) {
				   $('#quantity').hide();
			   }
			   else {
					$('#quantity').show();
			   }
		   }
	   });
	}

	function get_hsn_gst_by_subcategory_id(subcategory_id){ //alert(subcategory_id);
		
		$.post('{{ route('subcategories.get_subcategories_by_SubCategoryHsn') }}',{_token:'{{ csrf_token() }}', subcategory_id:subcategory_id}, function(data){
			$('#SubCategoryHsn_id').html('<option value="">Select HSN</option>');
		    for (var i = 0; i < data.length; i++) {
		        $('#SubCategoryHsn_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].hns_no
		        }));
		        $('.demo-select2').select2();
		    }
		});

	}

	function get_gst_by_hsn_id(SubCategoryHsn_id){ //alert(SubCategoryHsn_id);

		$.post('{{ route('subcategories.get_gst_by_hsn_id') }}',{_token:'{{ csrf_token() }}', has_id:SubCategoryHsn_id}, function(data){
		    for (var i = 0; i < data.length; i++) {
				$('#tax').val(data[0].gst_amount);
		    }
			
		});
	}

	function get_subcategories_by_category(){ 
		var category_id = $('#category_id').val();
		$.post('{{ route('subcategories.get_subcategories_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){
		    $('#subcategory_id').html('<option value="">Select Subcategory</option>');
		    for (var i = 0; i < data.length; i++) {
		        $('#subcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		        $('.demo-select2').select2();
		    }
		    get_subsubcategories_by_subcategory();
		});
	}

	function get_subsubcategories_by_subcategory(){ 
		var subcategory_id = $('#subcategory_id').val();
		$.post('{{ route('subsubcategories.get_subsubcategories_by_subcategory') }}',{_token:'{{ csrf_token() }}', subcategory_id:subcategory_id}, function(data){
		   $('#subsubcategory_id').html('<option value="">Select Subsubcategory</option>');
			$('#subsubcategory_id').append($('<option>', {
				value: null,
				text: null
			}));
		    for (var i = 0; i < data.length; i++) {
		        $('#subsubcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		        $('.demo-select2').select2();
		    }
		    //get_brands_by_subsubcategory();
			//get_attributes_by_subsubcategory();
		});
	}

	function get_brands_by_subsubcategory(){  
		var subsubcategory_id = $('#subsubcategory_id').val();
		$.post('{{ route('subsubcategories.get_brands_by_subsubcategory') }}',{_token:'{{ csrf_token() }}', subsubcategory_id:subsubcategory_id}, function(data){
		   $('#brand_id').html('<option value="">Select Brand</option>');
		    for (var i = 0; i < data.length; i++) {
		        $('#brand_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		        $('.demo-select2').select2();
		    }
		});
	}

	function get_attributes_by_subsubcategory(){ 
		var subsubcategory_id = $('#subsubcategory_id').val();
		$.post('{{ route('subsubcategories.get_attributes_by_subsubcategory') }}',{_token:'{{ csrf_token() }}', subsubcategory_id:subsubcategory_id}, function(data){
		    $('#choice_attributes').html(null);
		    for (var i = 0; i < data.length; i++) {
		        $('#choice_attributes').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		    }
			$('.demo-select2').select2();
		});
	}

	$(document).ready(function(){

		var editor2 = CKEDITOR.replace('specification_product', {
      extraAllowedContent: 'div(*);h3(*);img(*);h1(*);button(*);h2(*);h4(*);ul(*);a(*);section(*);li(*);span(*);p(*)',
      height: 400,
  {{--  filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
    //   filebrowserUploadMethod: 'form', --}}
    });
    editor2.on('instanceReady', function() {
      // Output self-closing tags the HTML4 way, like <br>.
      this.dataProcessor.writer.selfClosingEnd = '>';

      // Use line breaks for block elements, tables, and lists.
      var dtd = CKEDITOR.dtd;
      for (var e in CKEDITOR.tools.extend({}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent)) {
        this.dataProcessor.writer.setRules(e, {
          indent: true,
          breakBeforeOpen: true,
          breakAfterOpen: true,
          breakBeforeClose: true,
          breakAfterClose: true
        });
      }
      // Start in source mode.
      //this.setMode('source');
    });


		var editor1 = CKEDITOR.replace('description_product', {
      extraAllowedContent: 'div(*);h3(*);img(*);h1(*);button(*);h2(*);h4(*);ul(*);a(*);section(*);li(*);span(*);p(*)',
      height: 400,
  {{--  filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
    //   filebrowserUploadMethod: 'form', --}}
    });
    editor1.on('instanceReady', function() {
      // Output self-closing tags the HTML4 way, like <br>.
      this.dataProcessor.writer.selfClosingEnd = '>';

      // Use line breaks for block elements, tables, and lists.
      var dtd = CKEDITOR.dtd;
      for (var e in CKEDITOR.tools.extend({}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent)) {
        this.dataProcessor.writer.setRules(e, {
          indent: true,
          breakBeforeOpen: true,
          breakAfterOpen: true,
          breakBeforeClose: true,
          breakAfterClose: true
        });
      }
      // Start in source mode.
      //this.setMode('source');
    });


		$(".color-var-select").select2({
        templateResult: colorCodeSelect,
        templateSelection: colorCodeSelect,
        escapeMarkup: function (m) {
            return m;
        },
    });
	    get_subcategories_by_category();
		$("#photos").spartanMultiImagePicker({
			fieldName:        'photos[]',
			maxCount:         10,
			rowHeight:        '150px',
			groupClassName:   'col-md-2 col-sm-3 col-xs-5',
			maxFileSize:      '',
			dropFileLabel : "Drop Here",
			onExtensionErr : function(index, file){
				console.log(index, file,  'extension err');
				alert('Please only input png or jpg type file')
			},
			onSizeErr : function(index, file){
				console.log(index, file,  'file size too big');
				alert('File size too big');
			}
		});
		$("#thumbnail_img").spartanMultiImagePicker({
			fieldName:        'thumbnail_img',
			maxCount:         1,
			rowHeight:        '200px',
			groupClassName:   'col-md-3 col-sm-4 col-xs-6',
			maxFileSize:      '',
			dropFileLabel : "Drop Here",
			onExtensionErr : function(index, file){
				console.log(index, file,  'extension err');
				alert('Please only input png or jpg type file')
			},
			onSizeErr : function(index, file){
				console.log(index, file,  'file size too big');
				alert('File size too big');
			}
		});
		$("#featured_img").spartanMultiImagePicker({
			fieldName:        'featured_img',
			maxCount:         1,
			rowHeight:        '200px',
			groupClassName:   'col-md-3 col-sm-4 col-xs-6',
			maxFileSize:      '',
			dropFileLabel : "Drop Here",
			onExtensionErr : function(index, file){
				console.log(index, file,  'extension err');
				alert('Please only input png or jpg type file')
			},
			onSizeErr : function(index, file){
				console.log(index, file,  'file size too big');
				alert('File size too big');
			}
		});
		$("#flash_deal_img").spartanMultiImagePicker({
			fieldName:        'flash_deal_img',
			maxCount:         1,
			rowHeight:        '200px',
			groupClassName:   'col-md-3 col-sm-4 col-xs-6',
			maxFileSize:      '',
			dropFileLabel : "Drop Here",
			onExtensionErr : function(index, file){
				console.log(index, file,  'extension err');
				alert('Please only input png or jpg type file')
			},
			onSizeErr : function(index, file){
				console.log(index, file,  'file size too big');
				alert('File size too big');
			}
		});
		$("#meta_photo").spartanMultiImagePicker({
			fieldName:        'meta_img',
			maxCount:         1,
			rowHeight:        '200px',
			groupClassName:   'col-md-3 col-sm-4 col-xs-6',
			maxFileSize:      '',
			dropFileLabel : "Drop Here",
			onExtensionErr : function(index, file){
				console.log(index, file,  'extension err');
				alert('Please only input png or jpg type file')
			},
			onSizeErr : function(index, file){
				console.log(index, file,  'file size too big');
				alert('File size too big');
			}
		});
	});

	$('#category_id').on('change', function() {
	    get_subcategories_by_category();
	});

	$('#subcategory_id').on('change', function() {
	    get_subsubcategories_by_subcategory();
	});

	$('#subsubcategory_id').on('change', function() {
	    // get_brands_by_subsubcategory();
		//get_attributes_by_subsubcategory();
	});

	$('#choice_attributes').on('change', function() {
		$('#customer_choice_options').html(null);
		$.each($("#choice_attributes option:selected"), function(){
			//console.log($(this).val());
            add_more_customer_choice_option($(this).val(), $(this).text());
        });
		update_sku();
	});
	function colorCodeSelect(state) {
        var colorCode = $(state.element).val();
        if (!colorCode) return state.text;
        return (
            "<span class='color-preview' style='background-color:" +
            colorCode +
            ";'></span>" +
            state.text
        );
    }

</script>

@endsection
