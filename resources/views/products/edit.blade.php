@extends('admin.layout.admin_template')
@section('content')
<style>
	img.img-responsive {
    width: 150px;
}
</style>
 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
    <div class="content-body"><!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
  <div class="row match-height">
      <div class="col-md-12 col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">{{ translate('Edit Product') }}</h4>
              </div>
              <div class="card-content">
                  <div class="card-body">
  <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form form-horizontal mar-top" action="{{route('products.update', $product->id)}}" method="POST" enctype="multipart/form-data" id="choice_form">
			<input name="_method" type="hidden" value="POST">
			<input type="hidden" name="id" value="{{ $product->id }}">
			@csrf
            <div class="form-body">
			<div class="panel-heading bord-btm">
					<h3 class="panel-title">{{translate('Product Information')}}</h3>
				</div>
			<div class="row" style="padding:1%;">
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span class="required">{{translate('Product Name')}}</span></div>
					         <div class="col-md-8">
							  <input type="text" class="form-control" name="name" placeholder="{{translate('Product Name')}}" value="{{$product->name}}" required>
                        </div>
				        </div>
			        </div>
			        <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span class="required">{{translate('Category')}}</span></div>
					       <div class="col-md-8">
						    <select class="form-control demo-select2-placeholder" name="category_id" id="category_id" required>
                            	<option>{{ translate('Select an option') }}</option>
                            	@foreach($categories as $category)
								  @if($category->digital == 0)
                            	    <option value="{{$category->id}}" <?php if($product->category_id == $category->id) echo "selected"; ?> >{{__($category->name)}}</option>
									@endif
								@endforeach
                            </select>
					       </div>
				        </div>
			        </div>
			
			        <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span class="required">{{translate('Subcategory')}}</span></div>
					       <div class="col-md-8">
						   <select class="form-control demo-select2-placeholder" name="subcategory_id" id="subcategory_id" required>

                            </select>
					       </div>
				        </div>
			        </div>
                <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span class="">{{translate('Sub Subcategory')}}</span></div>
					       <div class="col-md-8">
						   <select class="form-control demo-select2-placeholder" name="subsubcategory_id" id="subsubcategory_id" >

                            </select>
					       </div>
				        </div>
			        </div>

					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span class="required">{{translate('Hsn No')}}</span></div>
					       <div class="col-md-8">
						   <select class="form-control demo-select2-placeholder" name="SubCategoryHsn_id" id="SubCategoryHsn_id" required>

							</select>
					       </div>
				        </div>
			        </div>					
					@if ($product->digital == 0)
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span class="required">{{translate('Brand')}}</span></div>
					       <div class="col-md-8">
						   <select class="form-control demo-select2-placeholder" name="brand_id" id="brand_id" required>
								<option value="">{{ ('Select Brand') }}</option>
								@foreach (\App\Brand::all() as $brand)
									<option value="{{ $brand->id }}" @if($product->brand_id == $brand->id) selected @endif>{{ $brand->name }}</option>
								@endforeach
                            </select>
					       </div>
				        </div>
			        </div>

					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span class="required">{{translate('Unit')}}</span></div>
					       <div class="col-md-8">
						   <select class="form-control" name="unit_name" id="unit_name" required>
								<option value="instock" <?php if($product->unit_name == 'instock') echo "selected";?> >{{ ('Instock') }}</option>
								<option value="out of stock" <?php if($product->unit_name == 'out of stock') echo "selected";?> >{{ ('Out of stock') }}</option>
							</select>

								</div>
				        </div>
			        </div>

					<div class="col-12" id="ins_div" style="display:none;">
                        <div class="form-group row">
                            <div class="col-md-3"><span class="required">{{translate('Instock Quantity')}}</span></div>
					       <div class="col-md-8">
							<input type="text" class="form-control" name="unit" id="unit" placeholder="{{ translate('Unit (e.g. KG, Pc etc)') }}" value="" required>
                        </div>
				        </div>
			        </div>
@endif
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span>{{translate('Tags')}}</span></div>
					       <div class="col-md-8">
							<input type="text" class="form-control" name="tags[]" id="tags" value="{{ $product->tags }}" placeholder="{{ translate('Type to add a tag') }}" data-role="tagsinput">
                        </div>
				        </div>
			        </div>
					@php
					    $pos_addon = \App\Addon::where('unique_identifier', 'pos_system')->first();
					@endphp
					@if ($pos_addon != null && $pos_addon->activated == 1)
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span>{{translate('Barcode')}}</span></div>
					       <div class="col-md-8">
						   <input type="text" class="form-control" name="barcode" placeholder="{{ translate('Barcode') }}" value="{{ $product->barcode }}">
							</div>
					       </div>
				        </div>
			        </div>
					@endif
					@php
					    $refund_request_addon = \App\Addon::where('unique_identifier', 'refund_request')->first();
					@endphp
					{{-- @if ($refund_request_addon != null && $refund_request_addon->activated == 1) --}}
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span>{{translate('Refundable')}}</span></div>
					       <div class="col-md-8">
                                <label class="switch" style="margin-top:5px;">
									<input type="checkbox" name="refundable" @if ($product->refundable == 1) checked @endif>
		                            <span class="slider round"></span>
							     </label>
					       </div>
				        </div>
					</div>
					
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span>{{translate('No Contact Delivary')}}</span></div>
					       <div class="col-md-8">
						   <label class="switch" style="margin-top:5px;">
									<input type="checkbox" name="no_delivery" @if ($product->no_delivery == 1) checked @endif>
		                            <span class="slider round"></span></label>
								</label>
					       </div>
				        </div>
					</div>
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span>{{translate('7 Days Return')}}</span></div>
					       <div class="col-md-8">
						   <label class="switch" style="margin-top:5px;">
									<input type="checkbox" name="days_return" @if ($product->days_return == 1) checked @endif>
		                            <span class="slider round"></span></label>
								</label>
					       </div>
				        </div>
					</div>
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span>{{translate('Brand Warrenty')}}</span></div>
					       <div class="col-md-8">
						   <label class="switch" style="margin-top:5px;">
									<input type="checkbox" name="b_warrenty" @if ($product->b_warrenty == 1) checked @endif>
		                            <span class="slider round"></span></label>
								</label>
					       </div>
				        </div>
					</div>
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span>{{translate('Cash On Delivery')}}</span></div>
					       <div class="col-md-8">
						   <label class="switch" style="margin-top:5px;">
									<input type="checkbox" name="cod" @if ($product->cod == 1) checked @endif>
		                            <span class="slider round"></span></label>
								</label>
					       </div>
				        </div>
					</div>
					<div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-3"><span>{{translate('Online Payment')}}</span></div>
					       <div class="col-md-8">
						   <label class="switch" style="margin-top:5px;">
									<input type="checkbox" name="online_payment" @if ($product->online_payment == 1) checked @endif>
		                            <span class="slider round"></span></label>
								</label>
					       </div>
				        </div>
					</div>
					{{-- @endif --}}
                </div>
             </div>
					<div class="panel" style="padding:1%; background-color:#FFFFFF;">
						<div class="panel-heading bord-btm">
							<h3 class="panel-title">{{translate('Product Images')}}</h3>
						</div>
						 <div class="form-body">
							<div class="row">
									<div class="col-12">
										<div class="form-group row">
											<div class="col-md-3"><span>{{translate('Gallery Images')}}</span></div>
											@if(is_array(json_decode($product->photos)))
									        @foreach (json_decode($product->photos) as $key => $photo)
											 <div class="col-md-2">
											 <img loading="lazy"  src="{{ asset($photo) }}" alt="" class="img-responsive">
												<input type="hidden" name="previous_photos[]" value="{{ $photo }}">
												<button type="button" class="btn btn-danger close-btn remove-files"><i class="fa fa-times"></i></button>
											  </div>
											
											  @endforeach
											  @endif
										</div>
									</div>
									
									<div class="col-12">
										<div class="form-group row">
											<div class="col-md-3"><span>{{translate('Thumbnail Image')}}</span>&nbsp;<span>(200x280 px)</span></div>
											 
											 @if ($product->thumbnail_img != null)
												<div class="col-md-2">
													<div class="img-upload-preview">
														<img loading="lazy"  src="{{ asset($product->thumbnail_img) }}" alt="" class="img-responsive">
														<input type="hidden" name="previous_thumbnail_img" value="{{ $product->thumbnail_img }}">
														<button type="button" class="btn btn-danger close-btn remove-files"><i class="fa fa-times"></i></button>
													</div>
												</div>
											@endif
											 
										</div>
									</div>
									
									<div class="col-12">
										<div class="form-group row">
											<div class="col-md-3"><span>{{translate('Featured')}}</span> <span>(200x280 px)</span></div>
											
											 @if ($product->featured_img != null)
												<div class="col-md-2">
													<div class="img-upload-preview">
														<img loading="lazy"  src="{{ asset($product->featured_img) }}" alt="" class="img-responsive">
														<input type="hidden" name="previous_featured_img" value="{{ $product->featured_img }}">
														<button type="button" class="btn btn-danger close-btn remove-files"><i class="fa fa-times"></i></button>
													</div>
												</div>
											@endif
											  
										</div>
									</div>
									
									<div class="col-12">
										<div class="form-group row">
											<div class="col-md-3"><span>{{translate('Flash Deal')}}</span><span>(200x280 px)</span></div>
											 
											 @if ($product->flash_deal_img != null)
													<div class="col-md-2">
														<div class="img-upload-preview">
															<img loading="lazy"  src="{{ asset($product->flash_deal_img) }}" alt="" class="img-responsive">
															<input type="hidden" name="previous_flash_deal_img" value="{{ $product->flash_deal_img }}">
															<button type="button" class="btn btn-danger close-btn remove-files"><i class="fa fa-times"></i></button>
														</div>
													</div>
												@endif
										</div>
									</div>
									
									
									
							</div>
						</div>
						
					   </div>
				    </div>
			<div class="panel" style="padding:1%;">
				<div class="panel-heading bord-btm">
					<h3 class="panel-title">{{translate('Product Videos')}}</h3>
				</div>
				<div class="form-body">
					<div class="row">
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{translate('Video Provider')}}</span></div>
									 <div class="col-md-8">
									 <select class="form-control demo-select2-placeholder" name="video_provider" id="video_provider">
										<option value="youtube" <?php if($product->video_provider == 'youtube') echo "selected";?> >{{translate('Youtube')}}</option>
										<option value="dailymotion" <?php if($product->video_provider == 'dailymotion') echo "selected";?> >{{translate('Dailymotion')}}</option>
										<option value="vimeo" <?php if($product->video_provider == 'vimeo') echo "selected";?> >{{translate('Vimeo')}}</option>
									  </select>
									 </div>
								</div>
							</div>
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{translate('Video Link')}}</span></div>
									 <div class="col-md-8">
									 <input type="text" class="form-control" name="video_link" value="{{ $product->video_link }}" placeholder="{{ translate('Video Link') }}">
						             </div>
								</div>
							</div>
				    </div>
				</div>
				
			</div>
			@if ($product->digital == 0)
			<div class="panel">
				<div class="panel-heading bord-btm">
					<h3 class="panel-title">{{translate('Product Variation')}}</h3>
				</div>
				<div class="panel-body">
					<div class="form-group row">
						<div class="col-lg-3">
							<input type="text" class="form-control" value="{{translate('Colors')}}" disabled>
						</div>
						<div class="col-lg-8">
							<select class="form-control color-var-select" name="colors[]" id="colors" multiple>
								@foreach (\App\Color::orderBy('name', 'asc')->get() as $key => $color)
									<option value="{{ $color->code }}" <?php if(in_array($color->code, json_decode($product->colors))) echo 'selected'?> >{{ $color->name }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-lg-1">
							<label class="switch" style="margin-top:5px;">
								<input value="1" type="checkbox" name="colors_active" <?php if(count(json_decode($product->colors)) > 0) echo "checked";?> >
								<span class="slider round"></span>
							</label>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-lg-3">
							<input type="text" class="form-control" value="{{translate('Attributes')}}" disabled>
						</div>
	                    <div class="col-lg-8">
	                        <select name="choice_attributes[]" id="choice_attributes" class="form-control demo-select2" multiple data-placeholder="{{ translate('Choose Attributes') }}">
								@foreach (\App\Attribute::all() as $key => $attribute)
									<option value="{{ $attribute->id }}" @if($product->attributes != null && in_array($attribute->id, json_decode($product->attributes, true))) selected @endif>{{ $attribute->name }}</option>
								@endforeach
	                        </select>
	                    </div>
	                </div>

					<div class="">
						<p>{{ translate('Choose the attributes of this product and then input values of each attribute') }}</p>
						<br>
					</div>

					<div class="customer_choice_options" id="customer_choice_options">
						@foreach (json_decode($product->choice_options) as $key => $choice_option)
							<div class="form-group row">
								<div class="col-lg-3">
									<input type="hidden" name="choice_no[]" value="{{ $choice_option->attribute_id }}">
									<input type="text" class="form-control " name="choice[]" value="{{ \App\Attribute::find($choice_option->attribute_id)->name }}" placeholder="{{ translate('Choice Title') }}" disabled>
								</div>
								<div class="col-lg-8">
									<input type="text" class="form-control " name="choice_options_{{ $choice_option->attribute_id }}[]" placeholder="{{ translate('Enter choice values') }}" value="{{ implode(',', $choice_option->values) }}" data-role="tagsinput" onchange="update_sku()">
								</div>
								<div class="col-lg-1">
									<button onclick="delete_row(this)" class="btn btn-danger btn-icon"><i class="demo-psi-recycling icon-lg"></i></button>
								</div>
							</div>
						@endforeach
					</div>
					
				</div>
			</div>
			@endif
			<div class="panel" style="padding:1%;">
				<div class="panel-heading bord-btm">
					<h3 class="panel-title">{{translate('Product price + stock')}}</h3>
				</div>
				<div class="form-body">
					<div class="row">
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{translate('M.R.P')}}</span></div>
									 <div class="col-md-8">
										<input type="text" placeholder="{{translate('M.R.P')}}" name="unit_price" class="form-control" value="{{$product->unit_price}}" required>
                                     </div>
								</div>
							</div>
							<!-- <div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{translate('Purchase price')}}</span></div>
									 <div class="col-md-8">
									 <input type="number" min="0" value="0" step="0.01" placeholder="{{ translate('Purchase price') }}" name="purchase_price" class="form-control" value="{{$product->purchase_price}}"  required>
						            </div>
								</div>
							</div> -->
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span class="required">{{translate('GST')}}</span></div>
									 <div class="col-md-8">
												 <input type="number" min="0" value="0" step="0.01" placeholder="{{ translate('GST') }}" name="tax" id="tax" class="form-control" value="{{$product->tax}}" readonly="readonly" required>
												</div>
												<div class="col-md-1">
												<input type="text"  value="Percent" placeholder="{{ translate('Percent') }}" name="tax_type"  class="form-control" readonly="readonly" required>
					
									  </div>
								</div>
							</div>
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{translate('Discount')}}</span></div>
									 <div class="col-md-8">
									 <input type="number" min="0" value="0" step="0.01" placeholder="{{ translate('Discount') }}" name="discount" class="form-control" value="{{$product->discount}}" required>
						            </div>
									<div class="col-md-1">
									<select class="demo-select2" name="tax_type" required>
										<option value="amount" <?php if($product->tax_type == 'amount') echo "selected";?> >{{translate('Flat')}}</option>
										<option value="percent" <?php if($product->tax_type == 'percent') echo "selected";?> >{{translate('Percent')}}</option>
									</select>
									</div>
								</div>
							</div>
							@if ($product->digital == 0)
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{translate('Quantity')}}</span></div>
									 <div class="col-md-8">
									 <input type="number" min="0" value="{{ $product->current_stock }}" step="1" placeholder="{{translate('Quantity')}}" name="current_stock" class="form-control"  value="{{$product->current_stock}}" required>
						</div>
						<br><br><br>
					<div class="form-group row col-lg-12">
					<div class="sku_combination col-lg-12" id="sku_combination"></div>

					</div>
									
								</div>
							</div>
							@endif
				    </div>
				</div>
				
			</div>
			<div class="panel" style="padding:1%;">
				<div class="panel-heading bord-btm">
					<h3 class="panel-title">{{translate('Product Description')}}</h3>
				</div>
				<div class="form-body">
					<div class="row">
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{translate('Description')}}</span></div>
									 <div class="col-md-8">
									 <textarea class="editor" name="description_product">{{$product->description}}</textarea>
									  </div>
								</div>
							</div>
				    </div>
				</div>
				
			</div>

			<div class="panel" style="padding:1%;">
				<div class="panel-heading bord-btm">
					<h3 class="panel-title">{{ translate('Specification')}}</h3>
				</div>
				<div class="form-body">
					<div class="row">
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{ translate('Specification')}}</span></div>
									 <div class="col-md-8">
									 <textarea class="" name="specification_product">{{$product->specification}}</textarea>
									  </div>
								</div>
							</div>
				    </div>
				</div>
				
			</div>

            @if (\App\BusinessSetting::where('type', 'shipping_type')->first()->value == 'product_wise_shipping')
                <div class="panel" style="padding:1%;">
    				<div class="panel-heading bord-btm">
    					<h3 class="panel-title">{{translate('Product Shipping Cost')}}</h3>
    				</div>
					<div class="form-body">
					<div class="row">
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{translate('Free Shipping')}}</span></div>
									 <div class="col-md-2">
									 <span>{{translate('Status')}}</span>
									  </div>
									 <div class="col-md-7">
									<label class="switch" style="margin-top:5px;">
    										<input type="radio" name="shipping_type" value="free" @if($product->shipping_type == 'free') checked @endif>
    										<span class="slider round"></span>
    									</label>
									  </div>
								</div>
							</div>
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{translate('Flat Rate')}}</span></div>
									<div class="col-md-2"><span>{{translate('Status')}}</span></div>
									 <div class="col-md-7">
									    <label class="switch" style="margin-top:5px;">
    										<input type="radio" name="shipping_type" value="flat_rate" @if($product->shipping_type == 'flat_rate') checked @endif>
    										<span class="slider round"></span>
    									</label>
									  </div>
								</div>
							</div>
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{translate('Shipping cost')}}</span></div>
									 <div class="col-md-8">
									    <input type="number" min="0" step="0.01" placeholder="{{translate('Shipping cost')}}" name="flat_shipping_cost" class="form-control" value="{{ $product->shipping_cost }}" required>
    								</div>
								</div>
							</div>
				    </div>
				</div>
					
    			</div>
			@endif
			@if ($product->digital == 0)
			<div class="panel" style="padding:1%;">
				<div class="panel-heading bord-btm">
					<h3 class="panel-title">{{translate('Size Chart')}}</h3>
				</div>
				<div class="form-body">
					<div class="row">
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{translate('Size Chart')}}</span></div>
									 <div class="col-md-8">
									 <input type="file" class="form-control" placeholder="{{translate('PDF')}}" name="pdf" accept="application/pdf">
									 <br>
									 @if(!empty($product->pdf))
									 <span class="font-weight:bold; font-size:17px;"><a href="{{asset($product->pdf)}}" target="_blank">{{translate('View PDF')}}</a></span>
									 @endif
									  </div>

									
								</div>
							</div>
				    </div>
				</div>
				
			</div>
			@endif
			<div class="panel" style="padding:1%;">
				<div class="panel-heading bord-btm">
					<h3 class="panel-title">{{translate('SEO Meta Tags')}}</h3>
				</div>
				<div class="form-body">
					<div class="row">
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{translate('Meta Title')}}</span></div>
									 <div class="col-md-8">
									<input type="file" class="form-control" placeholder="{{translate('PDF')}}" name="pdf" accept="application/pdf">
						</div>
								</div>
							</div>
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{translate('Description')}}</span></div>
									 <div class="col-md-8">
									 <textarea name="meta_description" rows="8" class="form-control">{{ $product->meta_description }}</textarea>
									  </div>
								</div>
							</div>
							<div class="col-12">
								<div class="form-group row">
									<div class="col-md-3"><span>{{ translate('Meta Image') }}</span></div>
									
									@if ($product->meta_img != null)
									 <div class="col-md-8">
										<div class="img-upload-preview">
											<img loading="lazy"  src="{{ asset($product->meta_img) }}" alt="" class="img-responsive">
											<input type="hidden" name="previous_meta_img" value="{{ $product->meta_img }}">
											<button type="button" class="btn btn-danger close-btn remove-files"><i class="fa fa-times"></i></button>
										</div>
									</div>
								@endif
								</div>
							</div>
				    </div>
				</div>
				
				
			</div>
			
				
				<div class="col-md-8 offset-md-4">
					<button type="submit" name="button" class="btn btn-primary mr-1 mb-1">{{ translate('Update Product') }}</button>
					<button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
					<a href="{{route('products.admin')}}" class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>
			   </div>
								  
            
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->


                  </div>
              </div>
          </div>
      </div>
      
  </div>
</section>
      </div>
    </div>
  </div>

@endsection
@section('script')

<script type="text/javascript">
$(document).ready(function(){
	$('#tax').val(<?php echo $product->tax; ?>);

$('.pdf_type').bind('change', function() {
    $('.label-important').remove(); 
var allowedExtensions =  
          /(\.pdf)$/i; 
    
  if (!allowedExtensions.exec(this.value)) { 
    $('.label-important').remove(); 
  $(this).after("<span class='label label-important' style='margin-left: 5%; color:#e63b05;font-weight: 600;font-size: 12px;'>Invalid file type Only .PDF Allowed</span>");
      this.value = ''; 
      return false; 
  }
  if(this.files[0].size/1024/1024 > 5){
    $('.label-important').remove(); 
    $(this).after("<span class='label label-important' style='margin-left: 5%;color: #e63b05;font-weight: 600;font-size: 12px;'>File size must be less than 5 mb</span>");
      this.value = ''; 
      return false; 
    }  
});
$('#file-2').change(function () {
//  alert('hi');
 var file = $(this)[0].files[0];
var fil_det=$(this);
 img = new Image();
 var imgwidth = 0;
 var imgheight = 0;
 var maxwidth = 200;
 var maxheight = 280;
 var isvalid_image=true;
 img.src = _URL.createObjectURL(file);
 img.onload = function(){
  
  imgwidth = this.width;
  imgheight = this.height;

  if(imgwidth != maxwidth && imgheight != maxheight){
    $('#file-2').val("");
    $('#file-2').next('label').find('span').html("");
  var msg= "Image size must be "+maxwidth+"X"+maxheight;
  showAlert("error",msg);
 }
};
img.onerror = function() {
$(this).val("");
  $(this).next('label').find('span').html("");
  var msg="not a valid file: " + file.type;
  showAlert("error",msg);
}
});
$('#file-3').change(function () {
//  alert('hi');
 var file = $(this)[0].files[0];
var fil_det=$(this);
 img = new Image();
 var imgwidth = 0;
 var imgheight = 0;
 var maxwidth = 200;
 var maxheight = 280;
 var isvalid_image=true;
 img.src = _URL.createObjectURL(file);
 img.onload = function(){
  
  imgwidth = this.width;
  imgheight = this.height;

  if(imgwidth != maxwidth && imgheight != maxheight){
    $('#file-3').val("");
    $('#file-3').next('label').find('span').html("");
  var msg= "Image size must be "+maxwidth+"X"+maxheight;
  showAlert("error",msg);
 }
};
img.onerror = function() {
$(this).val("");
  $(this).next('label').find('span').html("");
  var msg="not a valid file: " + file.type;
  showAlert("error",msg);
}
});
$('#file-4').change(function () {
//  alert('hi');
 var file = $(this)[0].files[0];
var fil_det=$(this);
 img = new Image();
 var imgwidth = 0;
 var imgheight = 0;
 var maxwidth = 200;
 var maxheight = 280;
 var isvalid_image=true;
 img.src = _URL.createObjectURL(file);
 img.onload = function(){
  
  imgwidth = this.width;
  imgheight = this.height;

  if(imgwidth != maxwidth && imgheight != maxheight){
    $('#file-4').val("");
    $('#file-4').next('label').find('span').html("");
  var msg= "Image size must be "+maxwidth+"X"+maxheight;
  showAlert("error",msg);
 }
};
img.onerror = function() {
$(this).val("");
  $(this).next('label').find('span').html("");
  var msg="not a valid file: " + file.type;
  showAlert("error",msg);
}
});
});
//alert('<?php //echo $product->unit_name; ?>');

  

    $("#unit_name").on('change', function() {
		var unit_name=$("#unit_name").val();
		chenge_unit_fun(unit_name,'');
	    
	});
	chenge_unit_fun('<?php echo $product->unit_name; ?>','<?php echo $product->unit; ?>');

	function chenge_unit_fun(unit_name,unit){
	
		
		if(unit_name=='instock'){  
           $('#ins_div').show();
		   $('#unit').val(unit);
		}else{
			$('#ins_div').hide();
			$('#unit').val('');
		}
	}

	function add_more_customer_choice_option(i, name){
		$('#customer_choice_options').append('<div class="form-group row"><div class="col-lg-3"><input type="hidden" name="choice_no[]" value="'+i+'"><input type="text" class="form-control" name="choice[]" value="'+name+'" readonly></div><div class="col-lg-8"><input type="text" class="form-control" name="choice_options_'+i+'[]" placeholder="{{ translate('Enter choice values') }}" data-role="tagsinput" onchange="update_sku()"></div><div class="col-lg-1"><button onclick="delete_row(this)" class="btn btn-danger btn-icon"><i class="demo-psi-recycling icon-lg"></i></button></div></div>');
		$("input[data-role=tagsinput], select[multiple][data-role=tagsinput]").tagsinput();
	}

	$('input[name="colors_active"]').on('change', function() {
	    if(!$('input[name="colors_active"]').is(':checked')){
			$('#colors').prop('disabled', true);
		}
		else{
			$('#colors').prop('disabled', false);
		}
		update_sku();
	});
	//get_hsn_gst_by_subsubcategory_id('',<?php //echo $product->hsn_id; ?>);

	$("#subcategory_id").on('change', function() {
		var subcategory_id=$("#subcategory_id").val();
	    get_hsn_gst_by_subsubcategory_id(subcategory_id,'');
	});
	$("#SubCategoryHsn_id").on('change', function() {
		var SubCategoryHsn_id=$("#SubCategoryHsn_id").val();
	    get_gst_by_hsn_id(SubCategoryHsn_id);
	});

	$('#colors').on('change', function() {
	    update_sku();
	});

	// $('input[name="unit_price"]').on('keyup', function() {
	//     update_sku();
	// });

	function delete_row(em){
		$(em).closest('.form-group').remove();
		update_sku();
	}

	function update_sku(){
		$.ajax({
		   type:"POST",
		   url:'{{ route('products.sku_combination_edit') }}',
		   data:$('#choice_form').serialize(),
		   success: function(data){
			   $('#sku_combination').html(data);
			   if (data.length > 1) {
				   $('#quantity').hide();
			   }
			   else {
					$('#quantity').show();
			   }
		   }
	   });
	}

	function get_subcategories_by_category(){
		var category_id = $('#category_id').val();
		$.post('{{ route('subcategories.get_subcategories_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){
		    
			$('#subcategory_id').html('<option value="">Select Subcategory</option>');
		    for (var i = 0; i < data.length; i++) {
		        $('#subcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		    }
		    $("#subcategory_id > option").each(function() {
		        if(this.value == '{{$product->subcategory_id}}'){
		            $("#subcategory_id").val(this.value).change();
		        }
		    });

		    $('.demo-select2').select2();

		    get_subsubcategories_by_subcategory();
		});
	}

	function get_subsubcategories_by_subcategory(){
		var subcategory_id = $('#subcategory_id').val();
		$.post('{{ route('subsubcategories.get_subsubcategories_by_subcategory') }}',{_token:'{{ csrf_token() }}', subcategory_id:subcategory_id}, function(data){
			$('#subsubcategory_id').html('<option value="">Select Subsubcategory</option>');
			$('#subsubcategory_id').append($('<option>', {
				value: null,
				text: null
			}));
		    for (var i = 0; i < data.length; i++) {
		        $('#subsubcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		    }
		    $("#subsubcategory_id > option").each(function() {
		        if(this.value == '{{$product->subsubcategory_id}}'){
		            $("#subsubcategory_id").val(this.value).change();
		        }
		    });

		    $('.demo-select2').select2();

		    //get_brands_by_subsubcategory();
			//get_attributes_by_subsubcategory();
		});
	}


	function get_hsn_gst_by_subsubcategory_id(subcategory_id,hsn_id){ //alert(hsn_id);
		if(subcategory_id==''){
			var subcategory_id = $('#subcategory_id').val();
		}
		

		$.post('{{ route('subcategories.get_subcategories_by_SubCategoryHsn') }}',{_token:'{{ csrf_token() }}', subcategory_id:subcategory_id}, function(data){
			//$('#SubCategoryHsn_id').html(null);
			$('#SubCategoryHsn_id').html('<option value="">Select HSN</option>');
			for (var i = 0; i < data.length; i++) {
				$('#SubCategoryHsn_id').append($('<option>', {
					value: data[i].id,
					text: data[i].hns_no
				}));
				$('.demo-select2').select2();
			}
			<?php if($product->hsn_id!=''){ ?>
			
				$('#SubCategoryHsn_id').val(<?php echo $product->hsn_id; ?>);
			<?php } ?>
			
		});

		}

		function get_gst_by_hsn_id(SubCategoryHsn_id){  

		$.post('{{ route('subcategories.get_gst_by_hsn_id') }}',{_token:'{{ csrf_token() }}', has_id:SubCategoryHsn_id}, function(data){
		    for (var i = 0; i < data.length; i++) {
				$('#tax').val(data[0].gst_amount);
		    }
			
		});
		}
	// function get_brands_by_subsubcategory(){
	// 	var subsubcategory_id = $('#subsubcategory_id').val();
	// 	$.post('{{ route('subsubcategories.get_brands_by_subsubcategory') }}',{_token:'{{ csrf_token() }}', subsubcategory_id:subsubcategory_id}, function(data){
	// 	    $('#brand_id').html(null);
	// 	    for (var i = 0; i < data.length; i++) {
	// 	        $('#brand_id').append($('<option>', {
	// 	            value: data[i].id,
	// 	            text: data[i].name
	// 	        }));
	// 	    }
	// 	    $("#brand_id > option").each(function() {
	// 	        if(this.value == '{{$product->brand_id}}'){
	// 	            $("#brand_id").val(this.value).change();
	// 	        }
	// 	    });
	//
	// 	    $('.demo-select2').select2();
	//
	// 	});
	// }

	function get_attributes_by_subsubcategory(){
		var subsubcategory_id = $('#subsubcategory_id').val();
		$.post('{{ route('subsubcategories.get_attributes_by_subsubcategory') }}',{_token:'{{ csrf_token() }}', subsubcategory_id:subsubcategory_id}, function(data){
		   // $('#choice_attributes').html(null);
		   $('#choice_attributes').html('<option value="">--Select--</option>');
		    for (var i = 0; i < data.length; i++) {
		        $('#choice_attributes').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		    }
			$("#choice_attributes > option").each(function() {
				var str = @php echo $product->attributes @endphp;
		        $("#choice_attributes").val(str).change();
		    });

			$('.demo-select2').select2();
		});
	}

	$(document).ready(function(){
		var editor1 = CKEDITOR.replace('specification_product', {
      extraAllowedContent: 'div(*);h3(*);img(*);h1(*);button(*);h2(*);h4(*);ul(*);a(*);section(*);li(*);span(*);p(*)',
      height: 400,
  {{--  filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
    //   filebrowserUploadMethod: 'form', --}}
    });
    editor1.on('instanceReady', function() {
      // Output self-closing tags the HTML4 way, like <br>.
      this.dataProcessor.writer.selfClosingEnd = '>';

      // Use line breaks for block elements, tables, and lists.
      var dtd = CKEDITOR.dtd;
      for (var e in CKEDITOR.tools.extend({}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent)) {
        this.dataProcessor.writer.setRules(e, {
          indent: true,
          breakBeforeOpen: true,
          breakAfterOpen: true,
          breakBeforeClose: true,
          breakAfterClose: true
        });
      }
      // Start in source mode.
      //this.setMode('source');
    });
		var editor1 = CKEDITOR.replace('description_product', {
      extraAllowedContent: 'div(*);h3(*);img(*);h1(*);button(*);h2(*);h4(*);ul(*);a(*);section(*);li(*);span(*);p(*)',
      height: 400,
  {{--  filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
    //   filebrowserUploadMethod: 'form', --}}
    });
    editor1.on('instanceReady', function() {
      // Output self-closing tags the HTML4 way, like <br>.
      this.dataProcessor.writer.selfClosingEnd = '>';

      // Use line breaks for block elements, tables, and lists.
      var dtd = CKEDITOR.dtd;
      for (var e in CKEDITOR.tools.extend({}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent)) {
        this.dataProcessor.writer.setRules(e, {
          indent: true,
          breakBeforeOpen: true,
          breakAfterOpen: true,
          breakBeforeClose: true,
          breakAfterClose: true
        });
      }
      // Start in source mode.
      //this.setMode('source');
    });
		$(".color-var-select").select2({
        templateResult: colorCodeSelect,
        templateSelection: colorCodeSelect,
        escapeMarkup: function (m) {
            return m;
        },
    });
	    get_subcategories_by_category();
		$("#photos").spartanMultiImagePicker({
			fieldName:        'photos[]',
			maxCount:         10,
			rowHeight:        '200px',
			groupClassName:   'col-md-3 col-sm-4 col-xs-6',
			maxFileSize:      '',
			dropFileLabel : "Drop Here",
			onExtensionErr : function(index, file){
				console.log(index, file,  'extension err');
				alert('Please only input png or jpg type file')
			},
			onSizeErr : function(index, file){
				console.log(index, file,  'file size too big');
				alert('File size too big');
			}
		});
		$("#thumbnail_img").spartanMultiImagePicker({
			fieldName:        'thumbnail_img',
			maxCount:         1,
			rowHeight:        '200px',
			groupClassName:   'col-md-3 col-sm-4 col-xs-6',
			maxFileSize:      '',
			dropFileLabel : "Drop Here",
			onExtensionErr : function(index, file){
				console.log(index, file,  'extension err');
				alert('Please only input png or jpg type file')
			},
			onSizeErr : function(index, file){
				console.log(index, file,  'file size too big');
				alert('File size too big');
			}
		});
		$("#featured_img").spartanMultiImagePicker({
			fieldName:        'featured_img',
			maxCount:         1,
			rowHeight:        '200px',
			groupClassName:   'col-md-3 col-sm-4 col-xs-6',
			maxFileSize:      '',
			dropFileLabel : "Drop Here",
			onExtensionErr : function(index, file){
				console.log(index, file,  'extension err');
				alert('Please only input png or jpg type file')
			},
			onSizeErr : function(index, file){
				console.log(index, file,  'file size too big');
				alert('File size too big');
			}
		});
		$("#flash_deal_img").spartanMultiImagePicker({
			fieldName:        'flash_deal_img',
			maxCount:         1,
			rowHeight:        '200px',
			groupClassName:   'col-md-3 col-sm-4 col-xs-6',
			maxFileSize:      '',
			dropFileLabel : "Drop Here",
			onExtensionErr : function(index, file){
				console.log(index, file,  'extension err');
				alert('Please only input png or jpg type file')
			},
			onSizeErr : function(index, file){
				console.log(index, file,  'file size too big');
				alert('File size too big');
			}
		});
		$("#meta_photo").spartanMultiImagePicker({
			fieldName:        'meta_img',
			maxCount:         1,
			rowHeight:        '200px',
			groupClassName:   'col-md-3 col-sm-4 col-xs-6',
			maxFileSize:      '',
			dropFileLabel : "Drop Here",
			onExtensionErr : function(index, file){
				console.log(index, file,  'extension err');
				alert('Please only input png or jpg type file')
			},
			onSizeErr : function(index, file){
				console.log(index, file,  'file size too big');
				alert('File size too big');
			}
		});

		update_sku();

		$('.remove-files').on('click', function(){
			//alert('hi')
            $(this).parents(".col-md-3").remove();
        });
	});

	$('#category_id').on('change', function() {
	    get_subcategories_by_category();
	});

	$('#subcategory_id').on('change', function() {
	    get_subsubcategories_by_subcategory();
	});

	$('#subsubcategory_id').on('change', function() {
	    //get_brands_by_subsubcategory();
		//get_attributes_by_subsubcategory();
	});

	$('#choice_attributes').on('change', function() {
		//$('#customer_choice_options').html(null);
		$.each($("#choice_attributes option:selected"), function(j, attribute){
			flag = false;
			$('input[name="choice_no[]"]').each(function(i, choice_no) {
				if($(attribute).val() == $(choice_no).val()){
					flag = true;
				}
			});
            if(!flag){
				add_more_customer_choice_option($(attribute).val(), $(attribute).text());
			}
        });

		var str = @php echo $product->attributes @endphp;

		$.each(str, function(index, value){
			flag = false;
			$.each($("#choice_attributes option:selected"), function(j, attribute){
				if(value == $(attribute).val()){
					flag = true;
				}
			});
            if(!flag){
				//console.log();
				$('input[name="choice_no[]"][value="'+value+'"]').parent().parent().remove();
			}
		});

		update_sku();
	});
	function colorCodeSelect(state) {
        var colorCode = $(state.element).val();
        if (!colorCode) return state.text;
        return (
            "<span class='color-preview' style='background-color:" +
            colorCode +
            ";'></span>" +
            state.text
        );
    }
</script>

@endsection
