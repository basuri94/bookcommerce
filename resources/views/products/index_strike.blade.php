@extends('admin.layout.admin_template')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body">
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{ translate('Strike '.$type.' Products') }}</h4>
                    <div class="row">
                     
                        </div>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">
                        <div class="pull-right clearfix">
                               <form class="" id="sort_products" action="" method="GET">
                                    @if($type == 'Seller')
                                        <div class="box-inline pad-rgt pull-left">
                                            <div class="select" style="min-width: 200px;">
                                                <select class="form-control demo-select2" id="user_id" name="user_id" onchange="sort_products()">
                                                    <option value="">All Sellers</option>
                                                    @foreach (App\Seller::all() as $key => $seller)
                                                        @if ($seller->user != null && $seller->user->shop != null)
                                                            <option value="{{ $seller->user->id }}" @if ($seller->user->id == $seller_id) selected @endif>{{ $seller->user->shop->name }} ({{ $seller->user->name }})</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="box-inline pad-rgt pull-left">
                                        <div class="select" style="min-width: 200px;">
                                            <select class="form-control demo-select2" name="type" id="type" onchange="sort_products()">
                                                <option value="">Sort by</option>
                                                <option value="rating,desc" @isset($col_name , $query) @if($col_name == 'rating' && $query == 'desc') selected @endif @endisset>{{translate('Rating (High > Low)')}}</option>
                                                <option value="rating,asc" @isset($col_name , $query) @if($col_name == 'rating' && $query == 'asc') selected @endif @endisset>{{translate('Rating (Low > High)')}}</option>
                                                <option value="num_of_sale,desc"@isset($col_name , $query) @if($col_name == 'num_of_sale' && $query == 'desc') selected @endif @endisset>{{translate('Num of Sale (High > Low)')}}</option>
                                                <option value="num_of_sale,asc"@isset($col_name , $query) @if($col_name == 'num_of_sale' && $query == 'asc') selected @endif @endisset>{{translate('Num of Sale (Low > High)')}}</option>
                                                <option value="unit_price,desc"@isset($col_name , $query) @if($col_name == 'unit_price' && $query == 'desc') selected @endif @endisset>{{translate('Base Price (High > Low)')}}</option>
                                                <option value="unit_price,asc"@isset($col_name , $query) @if($col_name == 'unit_price' && $query == 'asc') selected @endif @endisset>{{translate('Base Price (Low > High)')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- <div class="box-inline pad-rgt pull-left">
                                        <div class="" style="min-width: 200px;">
                                            <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="{{ translate('Type & Enter') }}">
                                        </div>
                                    </div> -->

                                    <div class="box-inline pad-rgt pull-left">
                                    <div class="" style="min-width: 200px;margin-left:10px;">
                                    <button class="btn btn-primary mr-1 mb-1" type="submit">{{ translate('Filter') }}</button>
                                    </div>
                                </div>
                                </form>

                        </div>
                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th width="20%">{{translate('Name')}}</th>
                                        @if($type == 'Seller')
                                            <th>{{translate('Seller Name')}}</th>
                                        @endif
                                        <th>{{translate('Num of Sale')}}</th>
                                        <th>{{translate('Total Stock')}}</th>
                                        <th>{{translate('Base Price')}}</th>
                                        <th>{{translate('Todays Deal')}}</th>
                                        <th>{{translate('Rating')}}</th>
                                        <th>{{translate('Approve')}}</th>
                                        <th>{{translate('Published')}}</th>
                                      
                                        <th>{{translate('Featured')}}</th>
                                        <th>{{translate('Options')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $key => $product)
                                    <tr>
                                        <td>{{ ($key+1) + ($products->currentPage() - 1)*$products->perPage() }}</td>
                                        <td>
                                            <a href="{{ route('product', $product->slug) }}" target="_blank" class="media-block">
                                                <div class="media-left">
                                                @if (file_exists($product->thumbnail_img)) 
                                                    <img loading="lazy"  class="img-md" src="{{ asset($product->thumbnail_img)}}" style="width :150px; height:150px;" alt="Image">
                                                    @endif
                                                </div>
                                                <div class="media-body">{{ __($product->name) }}</div>
                                            </a>
                                        </td>
                                        @if($type == 'Seller')
                                            <td>{{ $product->user->name }}</td>
                                        @endif
                                        <td>{{ $product->num_of_sale }} {{translate('times')}}</td>
                                        <td>
                                            @php
                                                $qty = 0;
                                                if($product->variant_product){
                                                    foreach ($product->stocks as $key => $stock) {
                                                        $qty += $stock->qty;
                                                    }
                                                }
                                                else{
                                                    $qty = $product->current_stock;
                                                }
                                                echo $qty;
                                            @endphp
                                        </td>
                                        <td>{{ number_format($product->unit_price,2) }}</td>
                                        <td><label class="switch">
                                                <input onchange="update_todays_deal(this)" value="{{ $product->id }}" type="checkbox" <?php if($product->todays_deal == 1) echo "checked";?> >
                                                <span class="slider round"></span></label></td>
                                        <td>{{ $product->rating }}</td>
                                        <td><label class="switch">
                                            <input onchange="update_approved(this)" value="{{ $product->id }}" type="checkbox" <?php if($product->is_approve == 1) echo "checked";?> >
                                            <span class="slider round"></span></label></td>
                                        <td><label class="switch">
                                                <input onchange="update_published(this)" value="{{ $product->id }}" type="checkbox" <?php if($product->published == 1) echo "checked";?> >
                                                <span class="slider round"></span></label></td>
                                        <td><label class="switch">
                                                <input onchange="update_featured(this)" value="{{ $product->id }}" type="checkbox" <?php if($product->featured == 1) echo "checked";?> >
                                                <span class="slider round"></span></label></td>
                                        <td>
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    {{translate('Actions')}} <i class="dropdown-caret"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                  <li><a href="{{route('products.view',encrypt($product->id))}}">{{translate('View')}}</a></li>
                                                     </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                              <tfoot>
                                    <tr>
                                       <th>#</th>
                                        <th width="20%">{{translate('Name')}}</th>
                                        @if($type == 'Seller')
                                            <th>{{translate('Seller Name')}}</th>
                                        @endif
                                        <th>{{translate('Num of Sale')}}</th>
                                        <th>{{translate('Total Stock')}}</th>
                                        <th>{{translate('Base Price')}}</th>
                                        <th>{{translate('Todays Deal')}}</th>
                                        <th>{{translate('Rating')}}</th>
                                        <th>{{translate('Published')}}</th>
                                        <th>{{translate('Featured')}}</th>
                                        <th>{{translate('Options')}}</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="clearfix">
                           
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
</div>
</div>

@endsection


@section('script')
    <script type="text/javascript">

        $(document).ready(function(){
            //$('#container').removeClass('mainnav-lg').addClass('mainnav-sm');
        });

        function update_todays_deal(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('products.todays_deal') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){ 
                    showAlert('success', 'Todays Deal updated successfully');
                   
                }
                else{
                    showAlert('danger', 'Something went wrong');
                   
                }
            });
        }

        function update_published(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('products.published') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){ 
                    showAlert('success', 'Published products updated successfully');
                   
                }
                else{
                    showAlert('danger', 'Something went wrong');
                   
                }
            });
        }
function update_approved(el){
    if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('products.approved') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){ 
                    showAlert('success', 'Product  Disapproved successfully');
                    location.reload();
                }
                else if(data == 2){
                    showAlert('success', 'Product  Approved successfully');
                }
                else{
                    showAlert('danger', 'Something went wrong');
                   
                }
               
            });
}
        function update_featured(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('products.featured') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){ 
                    showAlert('success', 'Featured products updated successfully');
                    
                }
                else{
                    showAlert('danger', 'Something went wrong');
                   
                }
            });
        }

        function sort_products(el){
            $('#sort_products').submit();
        }

    </script>
@endsection
