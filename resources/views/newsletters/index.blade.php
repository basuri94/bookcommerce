@extends('admin.layout.admin_template')
@section('content')


<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">{{translate('Send Newsletter')}}</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a
                                        href="{{route('admin.dashboard')}}">{{translate('Home')}}</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">{{translate('Send Newsletter')}}</a>
                                </li>

                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="content-body">
            <!-- Basic Horizontal form layout section start -->
            <section id="basic-horizontal-layouts">
                <div class="row match-height">
                    <div class="col-md-12 col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Newsletter</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <form class="form-horizontal" action="{{ route('newsletters.send') }}" method="POST"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4">
                                                            <span class="required">{{translate('Emails')}} ({{translate('Users')}})</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <select class="form-control selectpicker" name="user_emails[]" multiple data-selected-text-format="count" data-actions-box="true">
                                                                @foreach($users as $user)
                                                                    <option value="{{$user->email}}">{{$user->email}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div> 
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4">
                                                            <span class="required">{{translate('Emails')}} ({{translate('Subscribers')}})</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <select class="form-control selectpicker" name="subscriber_emails[]" multiple data-selected-text-format="count" data-actions-box="true">
                                                                @foreach($subscribers as $subscriber)
                                                                    <option value="{{$subscriber->email}}">{{$subscriber->email}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4">
                                                            <span>{{translate('Newsletter subject')}}</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" name="subject" id="subject" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4">
                                                            <span>{{translate('Newsletter content')}}
                                                              
                                                        </div>
                                                        <div class="col-md-8">
                                                            <textarea class="" name="content" required></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                     
                                               



                                                <div class="col-md-8 offset-md-4">
                                                    <button type="submit"
                                                        class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                                    <button type="reset" onclick="document.location.reload()"
                                                        class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                                    <a href="{{route('flash_deals.index')}}"
                                                        class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>

                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <!-- // Basic Horizontal form layout section end -->


            <!-- // Basic Floating Label Form section end -->

        </div>
    </div>
</div>
<!-- END: Content-->

@endsection
@section('script')

<script>
    $(function(){
  $('.demo-select2').select2();
  $(".selectpicker").select2({});
  var editor1 = CKEDITOR.replace('content', {
      extraAllowedContent: 'div(*);h3(*);img(*);h1(*);button(*);h2(*);h4(*);ul(*);a(*);section(*);li(*);span(*);p(*)',
      height: 400,
  {{--  filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
    //   filebrowserUploadMethod: 'form', --}}
    });
    editor1.on('instanceReady', function() {
      // Output self-closing tags the HTML4 way, like <br>.
      this.dataProcessor.writer.selfClosingEnd = '>';

      // Use line breaks for block elements, tables, and lists.
      var dtd = CKEDITOR.dtd;
      for (var e in CKEDITOR.tools.extend({}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent)) {
        this.dataProcessor.writer.setRules(e, {
          indent: true,
          breakBeforeOpen: true,
          breakAfterOpen: true,
          breakBeforeClose: true,
          breakAfterClose: true
        });
      }
      // Start in source mode.
      //this.setMode('source');
    });
});

</script>
@stop