@extends('admin.layout.admin_template')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body">
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{translate('Publishers List')}}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">
                    <div class="row">
                            <div class="col-sm-12">
                                <a href="{{ route('brands.create')}}" class="btn btn-rounded btn-info pull-right">{{translate('Add New Publishers')}}</a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{translate('Name')}}</th>
                                        <th>{{translate('Photo')}}</th>
                                        <th width="10%">{{translate('Options')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($brands as $key => $brand)
                                        <tr>
                                            <td>{{ ($key+1) + ($brands->currentPage() - 1)*$brands->perPage() }}</td>
                                            <td>{{$brand->name}}</td>
                                            <td>
                                            @if($brand->logo != '')
                                            <img loading="lazy"  class="img-md" src="{{ asset($brand->logo) }}" alt="Publishers Photo">
                                            @endif
                                            </td>
                                            <td>
                                                <div class="btn-group dropdown">
                                                    <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                        {{translate('Actions')}} <i class="dropdown-caret"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="{{route('brands.edit', encrypt($brand->id))}}">{{translate('Edit')}}</a></li>
                                                        <li><a onclick="confirm_modal('{{route('brands.destroy', $brand->id)}}');">{{translate('Delete')}}</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                              <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>{{translate('Name')}}</th>
                                        <th>{{translate('Logo')}}</th>
                                        <th width="10%">{{translate('Options')}}</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="clearfix">
                                <div class="pull-right">
                                    {{ $brands->appends(request()->input())->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
</div>
</div>

@endsection
@section('script')
    <script type="text/javascript">
        function sort_brands(el){
            $('#sort_brands').submit();
        }
    </script>
@endsection