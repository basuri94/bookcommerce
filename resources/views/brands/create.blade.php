@extends('admin.layout.admin_template')
@section('content')

<!-- BEGIN: Content-->
<div class="app-content content">
  <div class="content-overlay"></div>
  <div class="header-navbar-shadow"></div>
  <div class="content-wrapper">

    <div class="content-body">
      <!-- Basic Horizontal form layout section start -->
      <section id="basic-horizontal-layouts">
        <div class="row match-height">
          <div class="col-md-12 col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">{{translate('Publishers  Details')}}</h4>
              </div>
              <div class="card-content">
                <div class="card-body">
                  <!--Horizontal Form-->
                  <!--===================================================-->
                  <form class="form-horizontal" action="{{ route('brands.store') }}" method="POST"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">
                      <div class="row">
                        <div class="col-12">
                          <div class="form-group row">
                            <div class="col-md-4"><span class="required">{{translate('Name ( Bengali / English )')}}</span></div>
                            <div class="col-md-8">
                              <input type="text" placeholder="{{translate('Name  ( Bengali / English )')}}" id="name" name="name"
                                class="form-control" required>
                            </div>
                          </div>
                        </div>
                        <div class="col-12">
                          <div class="form-group row">
                            <div class="col-md-4"><span
                                class="required">{{translate('Photo')}}</span>&nbsp;<span>({{ translate('100x100') }})</span>
                            </div>
                            <div class="col-md-8">
                              <input type="file" id="logo" name="logo" required class="form-control"
                                onchange="readURL1(this);">
                              <img src="{{asset('img/Blank.jpg')}}" id="blah" width="100" height="100" alt="Publishers Photo"><br>
                              <!-- <span style="color: #002a80;font-size:85%;" class="info_adin_text">Max Size(2MB)</span> -->
                              <span class="info_adin_text mt-3">Upload an image with 100px By 100px.</span>

                              <br><button type="reset" id="pseudoCancel">
                                Cancel
                              </button>
                            </div>
                          </div>
                        </div>

                        <div class="col-12">
                          <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Phone No')}}</span></div>
                            <div class="col-md-8">
                              <input type="text" class="form-control" name="meta_title"
                                placeholder="{{translate('Phone No')}}">
                            </div>
                          </div>
                        </div>
                        <div class="col-12">
                          <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Description')}}</span></div>
                            <div class="col-md-8">
                              <textarea name="meta_description" rows="8" class="form-control"></textarea>
                            </div>
                          </div>
                        </div>



                        <div class="col-md-8 offset-md-4">
                          <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                          <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                          <a href="{{route('brands.index')}}"
                            class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>
                        </div>

                      </div>
                    </div>
                  </form>
                  <!--===================================================-->
                  <!--End Horizontal Form-->


                </div>
              </div>
            </div>
          </div>

        </div>
      </section>
      <!-- // Basic Horizontal form layout section end -->


      <!-- // Basic Floating Label Form section end -->

    </div>
  </div>
</div>
<!-- END: Content-->

@endsection
@section('script')
<script type="text/javascript">
  $(document).ready(function() {
  
      $('#pseudoCancel').click(function(){
        
        $('#logo').removeAttr('src');
      $('#logo').val('');
        $('#blah').val('');
        $('#blah').removeAttr('src');
        $('#blah').attr('src',"{{asset('img/Blank.jpg')}}");
   
      });



    });
    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                        .attr('src', e.target.result)
                        .width(100)
                        .height(100);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>


@endsection