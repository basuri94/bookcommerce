<!DOCTYPE html>
<html lang="en-US">

<!-- Mirrored from www.thecowisgonemoo.com/TheLocal2Vocal-html/index-first.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 27 Sep 2020 16:23:31 GMT -->
<head>
	
	<!-- ==============================================
	Title and basic Meta Tags
	=============================================== -->
	<meta charset="utf-8">
	<title>TheLocal2Vocal - Sellers Onboard</title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">

	<!-- ==============================================
	Mobile Metas
	=============================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

	<!-- ==============================================
	CSS
	=============================================== -->
	<link rel="stylesheet" href="{{ asset('landing_page/css/rev-grid.css')}}" media="all">
	<link rel="stylesheet" href="{{ asset('landing_page/css/font-awesome.min.css')}}" media="all">
	<link rel="stylesheet" href="{{ asset('landing_page/css/base.css')}}" media="all">	
	<link rel="stylesheet" href="{{ asset('landing_page/css/first-demo.css')}}" media="all">
	<link rel="stylesheet" href="{{ asset('landing_page/css/responsive.css')}}" media="all">

	<!--[if lt IE 9]>
		<script src="js/html5.js"></script>
		<script src="js/respond.min.js"></script>
	<![endif]-->

	<!--[if lte IE 8]> 
		<link rel="stylesheet" type="text/css" href="css/ie.css" />
	<![endif]-->

	<!-- ==============================================
	Fonts
	=============================================== -->
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro%7cPT+Sans:300,400,600' rel='stylesheet' type='text/css'>

	<!-- ==============================================
	Favicons
	=============================================== -->

	<!-- ==============================================
	JS
	=============================================== -->
  	<script type="text/javascript" src="{{ asset('landing_page/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landing_page/js/modernizr.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

</head>

<body class="no-preloader" id="top">

	<div class="preloader">
		<div class="preloader-logo">
			<div class="preloader-spinner"></div>
			<p>TheLocal2Vocal is Loading</p>
		</div>
	</div>

	<section class="section intro">
		<div class="container">
			<div class="row">
				<div class="column-1000 text-center intro-heading-block">
					<img src="{{ asset('landing_page/images/logo-2.png') }}" alt="TheLocal2Vocal" title="TheLocal2Vocal Landing Page" class="logo">
					<h1>TIME FOR MADE-IN-INDIA TO GO GLOBAL</h1>
			<!--		<h5>We're not just here to set your business up, but to support you as it grows.</h5> -->
				</div>
			</div>
			<div class="row">
				<div class="column-60 text-column text-center">
					
					<div class="row">
                    
					<!--	<div class="column-20">
							<img src="images/product-image-1.png" alt="intro-image" class="intro-image">
						</div> -->
						<div class="column-80">
                        <p class="intro-description">TheLocal2vocal, is helping homegrown brands like you to prosper<br>and make India a global manufacturing leader.local2vocal<br>support local busniesses to leverage e-commerce sales channel.<br>#AatmanirbharBharat</p>
					<h2>Reliable, Fast and Low Cost</h2>
							<ul class="intro-features">
								<li><img class="feat" src="{{ asset('landing_page/images/icon-check1.png') }}" alt="check icon">Free Registration</li>
								<li><img class="feat" src="{{ asset('landing_page/images/icon-check2.png') }}" alt="check icon">On-time payment</li>
                                <li><img class="feat" src="{{ asset('landing_page/images/icon-check3.png') }}" alt="check icon">Easy online shop set-up</li>
								<li><img class="feat" src="{{ asset('landing_page/images/icon-check4.png') }}" alt="check icon">Pan India Reach</li>
							
							</ul>
                        </div>
                        <img class="show_img" src="{{ asset('landing_page/images/14430.png') }}">
					</div>
                </div>
                <div class="column-40 form-column text-center">
					<form action="{{route('seller_onboard_save')}}" class="main-form" method="post">
                        @csrf
						<h4>Interested in out product?</h4>
						<p class="main-form-description">Let us walk you through our innovative product and let us give you the opportunity to try it for free.</p>
						<label for="name">Full Name <span>*</span></label>
						<input type="text" name="name" id="name" maxlength="50" class="required">
						<label for="email">Email <span>*</span></label>
						<input type="email" name="email" id="email" maxlength="50" class="form-email required">
						<label for="phone">Phone Number <span>*</span></label>
						<input type="text" name="phone" id="phone" maxlength="10" class="required">
						<label for="name">Shop Name <span>*</span></label>
						<input type="text" name="shopname" id="shopname" maxlength="50" class="required">
						<label for="name">Product Category <span>*</span></label>
						<input type="text" name="category" id="category" maxlength="50" class="required">
						<label for="name">State <span>*</span></label>
						<input type="text" name="state" id="state" maxlength="50" class="required">
						<label for="name">GST<span>*</span></label>
						<select name="gst" id="gst" class="required">
                            <option value="">Chose one</option>
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
						</select>
						<div class="submit-holder">
							<div class="submit-arrow animate-arrow">Request new a <span>free sample</span> of our product</div>
							<input type="submit" name="submit" id="submit" value="Request Free Sample">
						</div>
						<p class="disclaimer">We will never share your email address with anyone and won't bombard you with emails. Promise.</p>
					</form>
				</div>
			</div>
		</div>
	</section>

	<section class="section form-clients">
		<div class="container">
			<div class="row">
				
			<!--	<div class="column-60 clients">
					<div class="row">
						<div class="column-40"><p>Who already believe in us</p></div>
						<div class="column-30"><img src="images/logo-clients-1.png" alt="client-logo" title="Client Logo"></div>
						<div class="column-30"><img src="images/logo-clients-2.png" alt="client-logo" title="Client Logo"></div>
					</div>
				</div> -->
			</div>
		</div>
	</section>

<!--	<section class="section separator">
		<div class="container">
			<div class="row">
				<div class="column-1000">
					<hr>
				</div>
			</div>
		</div>
	</section> -->

	<!-- <section class="section content">
		<div class="container">
			<div class="row">
				<div class="column-1000">
					<h2>The story behind our product</h2>
					<div class="row content-block">
						<div class="column-70">
							<h4>Unspoiled Nature</h4>
							<p>Curabitur at dolor id est elementum auctor. Nunc placerat, arcu ut iaculis suscipit, ante lorem hendrerit dui, sit amet malesuada sapien risus vitae sapien. Sed sagittis diam consequat leo fringilla feugiat.<br>
							Ut rutrum cursus quam, ac hendrerit tortor lobortis sed. Mauris sollicitudin tincidunt lectus eget laoreet.</p>
						</div>
						<div class="column-30">
							<img src="images/content-image-1.jpg" alt="content-image">
						</div>
					</div>
					<div class="row content-block">
						<div class="column-70 float-right">
							<h4>Variety of Fruits</h4>
							<p>Duis sit amet velit suscipit, mollis neque sit amet, consectetur ipsum. Aenean id tortor tempus, pulvinar turpis a, imperdiet sapien.<br>
							Suspendisse sagittis nibh elit, ac euismod purus suscipit id. Vivamus diam leo, pellentesque sit amet felis non, eleifend bibendum turpis. </p>
						</div>
						<div class="column-30">
							<img src="images/content-image-2.jpg" alt="content-image">
						</div>
					</div>
					<div class="row content-block">
						<div class="column-70">
							<h4>Made with Love</h4>
							<p>Curabitur at dolor id est elementum auctor. Nunc placerat, arcu ut iaculis suscipit, ante lorem hendrerit dui, sit amet malesuada sapien risus vitae sapien. Sed sagittis diam consequat leo fringilla feugiat.<br> 
							Duis sit amet velit suscipit, mollis neque sit amet, consectetur ipsum. Aenean id tortor tempus, pulvinar turpis a, imperdiet sapien.</p>
						</div>
						<div class="column-30">
							<img src="images/content-image-3.jpg" alt="content-image">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> -->

<!--	<section class="section">
		<div class="container">
			<div class="row">
				<div class="column-1000">
					<div class="testimonial-block">
						<p class="testimonial">"We manufacture our products with the utmost care and dedication, we check carefully the process from the raw materials to your home."</p>
						<p class="author"><span>Mark Wright</span> - Founder & CEO</p>
						<img src="images/testimonial-1.jpg" alt="testimonial-image" title="Testimonial Image">
					</div>
				</div>
			</div>
		</div>
	</section> -->

<!--	<section class="section bottom-cta">
		<div class="container">
			<div class="row">
				<div class="column-60">
					<h2>Get a free sample today and join the growing community of "TheLocal2Vocal" lovers</h2>
					<a class="button orange cta" href="#top">Request Free Sample</a>
				</div>
				<div class="column-40">
					<img src="images/product-image-2.png" alt="product-image" class="bottom-cta-image">
				</div>
			</div>
		</div>
	</section> -->

<!--	<section class="section footer">
		<div class="container">
			<div class="row">
				<div class="column-1000">

					<div class="social">

						
						<div id="fb-root"></div>
						<script>(function(d, s, id) {
						  var js, fjs = d.getElementsByTagName(s)[0];
						  if (d.getElementById(id)) return;
						  js = d.createElement(s); js.id = id;
						  js.src = "../../connect.facebook.net/it_IT/all.js#xfbml=1&appId=183501305061516";
						  fjs.parentNode.insertBefore(js, fjs);
						}(document, 'script', 'facebook-jssdk'));</script>
						<div class="fb-like" data-href="" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>

						
						<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

						
						<div class="g-plusone" data-size="medium"></div>
						<script type="text/javascript">
						  (function() {
						    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
						    po.src = '../../apis.google.com/js/platform.js';
						    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
						  })();
						</script>

					</div>

					<p>Copyright © 2014 TheLocal2Vocal. All rights reserved.</p>
				</div>
			</div>
		</div>
	</section> -->

	<div id="thanks-popup" class="section thanks-popup mfp-hide">
 		<div class="thanks-heading">
 			<h1>Thank you for trusting us!</h1>
 		</div>
 		<div class="thanks-body">
 			<p>We will contact you shortly!<br>
			In the meantime you can download our <a href="#">services brochure</a>.</p>
			<p>Want to know when we have special deals?</p>
			<a class="button orange" href="#">Visit special deals page</a>
 		</div>
	</div>
    <div class="need_help">
    <p>
        <span class="flt_lt">Need help? </span>
        <span class="call_us"> 
            <img src="{{ asset('landing_page/images/call_us.png') }}">
                <em>Call us:&nbsp; <b>120 420 5675</b>Monday to Sunday (10.00 am to 6.00 pm)</em>
        </span>
        <span class="flt_rt">Copyright © 2020  L2V E Market Pvt. Ltd.</span>
        </p>
</div>
	<!-- ==============================================
	JS
	=============================================== -->
  	<script type="text/javascript" src="{{ asset('landing_page/js/components.js') }}"></script>
  	<script type="text/javascript" src="{{ asset('landing_page/js/custom.js') }}"></script>
      <script>
jQuery(document).ready(function($){
$("#submit").click( function() {
    swal("Thank You!", "We will contact you shortly.", "success");
});
});</script>
</body>

<!-- Mirrored from www.thecowisgonemoo.com/TheLocal2Vocal-html/index-first.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 27 Sep 2020 16:23:54 GMT -->
</html>