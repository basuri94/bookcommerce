<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{translate('Banner Information')}}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <form class="form-horizontal" action="{{ route('home_banners.update', $banner->id) }}"
                            method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="_method" value="PATCH">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span class="required">{{translate('Maximum 2 Image can be upload')}}</span>
                                            </div>
                                         
                                        </div>
                                        <input type="hidden" name="position" value="{{ $banner->position }}">
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span class="required">{{translate('URL')}}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" value="{{ $banner->url }}"
                                                    placeholder="{{translate('URL')}}" id="url" name="url"
                                                    class="form-control" required>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span class="required"> {{translate('Banner Images')}}</span>
                                                <strong>({{ translate('850px*420px') }})</strong>
                                            </div>
                                            <div class="col-sm-8" style="background-color:#fff;">
                                                @if ($banner->photo != null)
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                    <div class="img-upload-preview">
                                                        <img loading="lazy" src="{{ asset($banner->photo) }}" alt=""
                                                            class="img-responsive">
                                                        <input type="hidden" name="previous_photo"
                                                            value="{{ $banner->photo }}">
                                                        <button type="button"
                                                            class="btn btn-danger close-btn remove-files"><i
                                                                class="fa fa-times"></i></button>
                                                    </div>
                                                </div>
                                                @endif
                                                <div id="photo">

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                <div class="col-md-8 offset-md-4">
                                    <button type="submit"
                                        class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                    <button type="reset" onclick="document.location.reload()"
                                        class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                    <a href="{{route('home_settings.index')}}"
                                        class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>

                                </div>
                            </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    </div>
</section>







<script type="text/javascript">
    $(document).ready(function(){

        $('.remove-files').on('click', function(){
            $(this).parents(".col-md-4").remove();
        });

        $("#photo").spartanMultiImagePicker({
            fieldName:        'photo',
            maxCount:         1,
            rowHeight:        '200px',
            groupClassName:   'col-md-4 col-sm-9 col-xs-6',
            maxFileSize:      '',
            dropFileLabel : "Drop Here",
            onExtensionErr : function(index, file){
                console.log(index, file,  'extension err');
                alert('Please only input png or jpg type file')
            },
            onSizeErr : function(index, file){
                console.log(index, file,  'file size too big');
                alert('File size too big');
            }
        });
    });

</script>