


<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{translate('Banner Information')}}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <form class="form-horizontal" action="{{ route('home_banners.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf

                            <div class="form-body">
                                <div class="row">
                                  
                                    <div class="col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                @if($position==2)
                                                <span class="required">{{translate('Maximum 4 Image can be upload')}}</span>
                                                @else 
                                                <span class="required">{{translate('Maximum 2 Image can be upload')}}</span>
                                                @endif
                                            </div>
                                         
                                        </div>
                                        <input type="hidden" name="position" value="{{ $position }}">
                                    </div>
                                    @if($position==2)
                                    <div class="col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span class="required">{{translate('Type')}}</span>
                                            </div>
                                            <div class="col-md-8">
                                                @php
                                                    $type_arr=\App\Banner::where('position', $position)->pluck('type')->toArray();
                                                //    echo"<pre>"; print_r( $type_arr);die;
                                                @endphp
                                                <select class="form-control" id="type" name="type" required>
                                                    @foreach(Config::get('constants.IMAGE_TYPE') as $key =>$val)
                                                    
                                                    @if(!in_array($key,$type_arr))
                                                <option value="{{$key}}">{{$val}}</option>
                                                   @endif
                                                   
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <input type="hidden" name="position" value="{{ $position }}">
                                    </div>
                                    @endif
                                    <div class="col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span class="required">{{translate('URL')}}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" placeholder="{{translate('URL')}}" id="url" name="url" class="form-control" required>
                                            </div>
                                        </div>
                                        <input type="hidden" name="position" value="{{ $position }}">
                                    </div>

                                    <div class="col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span class="required"> {{translate('Banner Images')}}</span>
                                                @if($position!=2)
                                                <strong>(850px*315px)</strong>
                                                @endif
                                            </div>
                                            <div class="col-sm-8" style="background-color:#fff;">
                                                <div id="photo">

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                <div class="col-md-8 offset-md-4">
                                    <button type="submit"
                                        class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                    <button type="reset" onclick="document.location.reload()"
                                        class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                    <a href="{{route('home_settings.index')}}"
                                        class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>

                                </div>
                            </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    </div>
</section>









<script type="text/javascript">

    $(document).ready(function(){

        $('.demo-select2').select2();

        $("#photo").spartanMultiImagePicker({
            fieldName:        'photo',
            maxCount:         1,
            rowHeight:        '200px',
            groupClassName:   'col-md-4 col-sm-9 col-xs-6',
            maxFileSize:      '',
            dropFileLabel : "Drop Here",
            allowedExt:       'png|jpg|svg',
            onExtensionErr : function(index, file){
                console.log(index, file,  'extension err');
                alert('Please only input png or jpg type file')
            },
            onSizeErr : function(index, file){
                console.log(index, file,  'file size too big');
                alert('File size too big');
            }
        });
    });

</script>
