@extends('admin.layout.admin_template')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" integrity="sha512-bYPO5jmStZ9WI2602V2zaivdAnbAhtfzmxnEGh9RwtlI00I9s8ulGe4oBa5XxiC6tCITJH/QG70jswBhbLkxPw==" crossorigin="anonymous" />

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h2 class="content-header-title float-left mb-0">{{translate('Flash Deal Information')}}</h2>
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">{{translate('Home')}}</a>
                  </li>
                  <li class="breadcrumb-item"><a href="{{route('flash_deals.index')}}">{{translate('Flash Flash Deals')}}</a>
                  </li>
                  <li class="breadcrumb-item active"><a href="#">Update New Flash Deal Products</a>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--<div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
          <div class="form-group breadcrum-right">
            <div class="dropdown">
              <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>
              <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a></div>
            </div>
          </div>
        </div>-->
      </div>
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
  <div class="row match-height">
      <div class="col-md-12 col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Flash Deal Information</h4>
              </div>
              <div class="card-content">
                  <div class="card-body">
                    <form class="form-horizontal" action="{{ route('flash_deals.update', $flash_deal->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="form-body">
                              <div class="row">
                                  <div class="col-12">
                                      <div class="form-group row">
                                          <div class="col-md-4">
                      <span class="required">{{translate('Title')}}</span>
                    </div>
                                          <div class="col-md-8">
                                            <input type="text" placeholder="{{translate('Title')}}" id="name" name="title" value="{{ $flash_deal->title }}" class="form-control" required>
                                          </div>
                                      </div>
                                  </div>
                                  <!-- <div class="col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                      <span class="required">{{translate('Title')}}</span>
                      <span class="required">{{translate('Background Color')}}<small>(Hexa-code)</small></span>
                  </div>
                                        <div class="col-md-8">
                                            <input type="text" placeholder="{{translate('#0000ff')}}" id="background_color" name="background_color" value="{{ $flash_deal->background_color }}" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                  <div class="col-12">
                                      <div class="form-group row">
                                          <div class="col-md-4">
                      <span class="required">{{translate('Text Color')}}</span>
                    </div>
                                          <div class="col-md-8">
                                            <select name="text_color" id="text_color" class="form-control demo-select2" required>
                                                <option value="">Select One</option>
                                                <option value="white" @if ($flash_deal->text_color == 'white') selected @endif>{{translate('White')}}</option>
                                                <option value="dark" @if ($flash_deal->text_color == 'dark') selected @endif>{{translate('Dark')}}</option>
                                            </select>
                                        </div>
                                          </div>
                                      </div>
                                  </div> -->
                                  <div class="col-12">
                                      <div class="form-group row">
                                          <div class="col-md-4">
                      <span>{{translate('Banner')}} <small>(1308x475)</small></span>
                    </div>
                                          <div class="col-md-8">
                                            <input type="file" id="banner" name="banner" class="form-control">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                    <span>{{translate('Date & Time')}}</span>
                  </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="start_date" name="start_date" value="{{ date('d/m/Y H:i', $flash_deal->start_date) }}">
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                    <span>{{translate('To Date')}}</span>
                  </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="end_date" value="{{ date('m/d/Y', $flash_deal->end_date) }}">
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                    <span class="required">{{translate('Products')}}</span>
                  </div>
                                        <div class="col-md-8">
                                            <select name="products[]" id="products" class="form-control demo-select2" multiple required data-placeholder="{{ translate('Choose Products') }}">
                                                @foreach(\App\Product::all() as $product)
                                                    @php
                                                        $flash_deal_product = \App\FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $product->id)->first();
                                                    @endphp
                                                    <option value="{{$product->id}}" <?php if($flash_deal_product != null) echo "selected";?> >{{__($product->name)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                <div class="col-md-8 offset-md-4">
                                      <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                      <button type="reset" onclick="document.location.reload()" class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                      <a href="{{route('flash_deals.index')}}" class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>

                                  </div>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
      
  </div>
</section>
<!-- // Basic Horizontal form layout section end -->


<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->

@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js" integrity="sha512-AIOTidJAcHBH2G/oZv9viEGXRqDNmfdPVPYOYKGy3fti0xIplnlgMHUGfuNRzC6FkzIo0iIxgFnr9RikFxK+sw==" crossorigin="anonymous"></script>
<script>
$(function(){
   
  // $("#start_date").datepicker({ 
  //       autoclose: true, 
  //       todayHighlight: true,
  //       format:'mm/dd/yyyy'
  // }).datepicker('update', new Date());
  // $("#to_date").datepicker({ 
  //       autoclose: true, 
  //       todayHighlight: true,
  //       format:'mm/dd/yyyy'
  // }).datepicker('update', new Date());
  
  var dateToday = new Date();
        $('#start_date').datetimepicker({
   // dateFormat: 'dd-mm-yy',
   format:'d/m/Y H:i',
   minDate: dateToday,
    onSelect: function(selectedDate) {
        var option = this.id == "from" ? "minDate" : "maxDate",
            instance = $(this).data("datepicker"),
            date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
        dates.not(this).datepicker("option", option, date);
    }
        });
            
            get_flash_deal_discount();

            $('#products').on('change', function(){
                get_flash_deal_discount();
            });

            function get_flash_deal_discount(){
                var product_ids = $('#products').val();
                if(product_ids.length > 0){
                    $.post('{{ route('flash_deals.product_discount_edit') }}', {_token:'{{ csrf_token() }}', product_ids:product_ids, flash_deal_id:{{ $flash_deal->id }}}, function(data){
                        $('#discount_table').html(data);
                        $('.demo-select2').select2();
                    });
                }
                else{
                    $('#discount_table').html(null);
                }
            }
});

</script>
@stop