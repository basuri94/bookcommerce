@extends('admin.layout.admin_template')
@section('content')


    <!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- Nav Filled Starts -->
<section id="nav-filled">
    <div class="row">
      <div class="col-sm-12">
        <div class="card overflow-hidden">
          <div class="card-header">
            <h4 class="card-title">{{translate('Home Settings')}}</h4>
          </div>
          <div class="card-content">
            <div class="card-body">
              @csrf
              <!-- Nav tabs -->
              <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="home-tab-fill" data-toggle="tab" href="#home-fill" role="tab"
                    aria-controls="home-fill" aria-selected="true">{{translate('Home Slider')}}</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="banner1-tab-fill" data-toggle="tab" href="#banner1-fill" role="tab"
                    aria-controls="banner1-fill" aria-selected="false">{{translate('Home Banner 1')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="categories-tab-fill" data-toggle="tab" href="#category-fill" role="tab"
                      aria-controls="category-fill" aria-selected="false">{{translate('Home Categories')}}</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="banner2-tab-fill" data-toggle="tab" href="#banner2-fill" role="tab"
                      aria-controls="banner2-fill" aria-selected="false">{{translate('Home Banner 2')}}</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="top-9-tab-fill" data-toggle="tab" href="#top-9-fill" role="tab"
                      aria-controls="top-9-fill" aria-selected="false">{{translate('Top 9')}}</a>
                  </li>
              </ul>
  
              <!-- Tab panes -->
              <div class="tab-content pt-1">
                <div class="tab-pane active" id="home-fill" role="tabpanel" aria-labelledby="home-tab-fill">
                    <div class="row">
                        <div class="col-sm-12">
                            <a onclick="add_slider()" class="btn btn-rounded btn-info pull-right">{{translate('Add New Slider')}}</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table zero-configuration">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{translate('Photo')}}</th>
                                    <th >{{translate('Link')}}</th>
                                    <th>{{translate('Published')}}</th>
                                    <th width="10%">{{translate('Options')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(\App\Slider::all() as $key => $slider)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td><img loading="lazy"  class="img-md" src="{{ asset($slider->photo)}}" alt="Slider Image"></td>
                                    <td>{{$slider->link}}</td>
                                    <td><label class="switch">
                                        <input onchange="update_slider_published(this)" value="{{ $slider->id }}" type="checkbox" <?php if($slider->published == 1) echo "checked";?> >
                                        <span class="slider round"></span></label></td>
                                    <td>
                                        <div class="btn-group dropdown">
                                            <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                {{translate('Actions')}} <i class="dropdown-caret"></i>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a onclick="confirm_modal('{{route('sliders.destroy', $slider->id)}}');">{{translate('Delete')}}</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>{{translate('Photo')}}</th>
                                    <th >{{translate('Link')}}</th>
                                    <th>{{translate('Published')}}</th>
                                    <th width="10%">{{translate('Options')}}</th>
                                </tr>
                            </tfoot>
                        </table>
                     
                    </div>
                </div>
                <div class="tab-pane" id="banner1-fill" role="tabpanel" aria-labelledby="banner1-tab-fill">
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <a onclick="add_banner_1()" class="btn btn-rounded btn-info pull-right">{{translate('Add New Banner')}}</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table zero-configuration">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{translate('Photo')}}</th>
                                    <th>{{translate('Position')}}</th>
                                    <th>{{translate('Published')}}</th>
                                    <th width="10%">{{translate('Options')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(\App\Banner::where('position', 1)->get() as $key => $banner)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td><img loading="lazy"  class="img-md" src="{{ asset($banner->photo)}}" alt="banner Image"></td>
                                    <td>{{ translate('Banner Position ') }}{{ $banner->position }}</td>
                                    <td><label class="switch">
                                        <input onchange="update_banner_published(this)" value="{{ $banner->id }}" type="checkbox" <?php if($banner->published == 1) echo "checked";?> >
                                        <span class="slider round"></span></label></td>
                                    <td>
                                        <div class="btn-group dropdown">
                                            <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                {{translate('Actions')}} <i class="dropdown-caret"></i>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a onclick="edit_home_banner_1({{ $banner->id }})">{{translate('Edit')}}</a></li>
                                                <li><a onclick="confirm_modal('{{route('home_banners.destroy', $banner->id)}}');">{{translate('Delete')}}</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>{{translate('Photo')}}</th>
                                    <th>{{translate('Position')}}</th>
                                    <th>{{translate('Published')}}</th>
                                    <th width="10%">{{translate('Options')}}</th>
                                </tr>
                            </tfoot>
                        </table>
                     
                    </div>
                </div>
                
                <div class="tab-pane" id="category-fill" role="tabpanel" aria-labelledby="categories-tab-fill">
                    <div class="row">
                        <div class="col-sm-12">
                            <a onclick="add_home_category()" class="btn btn-rounded btn-info pull-right">{{translate('Add New Category')}}</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table zero-configuration">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{translate('Category')}}</th>
                                    <th>{{ translate('Status') }}</th>
                                    <th width="10%">{{translate('Options')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(\App\HomeCategory::all() as $key => $home_category)
                                @if ($home_category->category != null)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$home_category->category->name}}</td>
                                        <td><label class="switch">
                                            <input onchange="update_home_category_status(this)" value="{{ $home_category->id }}" type="checkbox" <?php if($home_category->status == 1) echo "checked";?> >
                                            <span class="slider round"></span></label></td>
                                        <td>
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    {{translate('Actions')}} <i class="dropdown-caret"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a onclick="edit_home_category({{ $home_category->id }})">{{translate('Edit')}}</a></li>
                                                    <li><a onclick="confirm_modal('{{route('home_categories.destroy', $home_category->id)}}');">{{translate('Delete')}}</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>{{translate('Category')}}</th>
                                    <th>{{ translate('Status') }}</th>
                                    <th width="10%">{{translate('Options')}}</th>
                                </tr>
                            </tfoot>
                        </table>
                     
                    </div>
                </div>


                <div class="tab-pane" id="banner2-fill" role="tabpanel" aria-labelledby="banner2-tab-fill">
                    <div class="row">
                        <div class="col-sm-12">
                            <a onclick="add_banner_2()" class="btn btn-rounded btn-info pull-right">{{translate('Add New Banner')}}</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table zero-configuration">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{translate('Photo')}}</th>
                                    <th>{{translate('Position')}}</th>
                                    <th>{{translate('Type')}}</th>
                                    <th>{{translate('Published')}}</th>
                                    <th width="10%">{{translate('Options')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(\App\Banner::where('position', 2)->get() as $key => $banner)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td><img loading="lazy"  class="img-md" src="{{ asset($banner->photo)}}" alt="banner Image"></td>
                                    <td>{{ translate('Banner Position ') }}{{ $banner->position }}</td>
                                    <td>{{ Config::get('constants.IMAGE_TYPE.'.$banner->type ) }}</td>
                                    <td><label class="switch">
                                        <input onchange="update_banner_published(this)" value="{{ $banner->id }}" type="checkbox" <?php if($banner->published == 1) echo "checked";?> >
                                        <span class="slider round"></span></label></td>
                                    <td>
                                        <div class="btn-group dropdown">
                                            <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                {{translate('Actions')}} <i class="dropdown-caret"></i>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a onclick="edit_home_banner_2({{ $banner->id }})">{{translate('Edit')}}</a></li>
                                                <li><a onclick="confirm_modal('{{route('home_banners.destroy', $banner->id)}}');">{{translate('Delete')}}</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>{{translate('Photo')}}</th>
                                    <th>{{translate('Position')}}</th>
                                    <th>{{translate('Published')}}</th>
                                    <th width="10%">{{translate('Options')}}</th>
                                </tr>
                            </tfoot>
                        </table>
                     
                    </div>
                </div>

                <div class="tab-pane" id="top-9-fill" role="tabpanel" aria-labelledby="top-9-tab-fill">
                    <form class="form-horizontal" action="{{ route('top_10_settings.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-3" for="url">{{translate('Top Categories (Max 9)')}}</label>
                                <div class="col-sm-9">
                                    <select class="form-control demo-select2-max-9" name="top_categories[]" multiple required>
                                        @foreach (\App\Category::all()->sortBy('name') as $key => $category)
                                            <option value="{{ $category->id }}" @if($category->top == 1) selected @endif>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3" for="url">{{translate('Top Brands (Max 9)')}}</label>
                                <div class="col-sm-9">
                                    <select class="form-control demo-select2-max-9" name="top_brands[]" multiple required>
                                        @foreach (\App\Brand::all()->sortBy('name') as $key => $brand)
                                            <option value="{{ $brand->id }}" @if($brand->top == 1) selected @endif>{{ $brand->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer text-right">
                            <button class="btn btn-primary mr-1 mb-1" type="submit">{{translate('Save')}}</button>
                        </div>
                    </form>
                </div>

              

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Nav Filled Ends -->
           

    </div>
</div>
</div>
<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="refundviewmodal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Refund View</h5>
                {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"> --}}
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="refund_body">
                ...
            </div>
            <div class="modal-footer" id="footer_id">

            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(function(){
    $(".demo-select2-max-9").select2({
        maximumSelectionLength: 9,
    });
});
  
    function updateSettings(el, type){
        if($(el).is(':checked')){
            var value = 1;
        }
        else{
            var value = 0;
        }
        $.post('{{ route('business_settings.update.activation') }}', {_token:'{{ csrf_token() }}', type:type, value:value}, function(data){
            if(data == 1){
                showAlert('success', 'Settings updated successfully');
            }
            else{
                showAlert('danger', 'Something went wrong');
            }
        });
    }

    function add_slider(){
        $.get('{{ route('sliders.create')}}', {}, function(data){
            $('#home-fill').html(data);
        });
    }

    function add_banner_1(){
        $.get('{{ route('home_banners.create', 1)}}', {}, function(data){
            $('#banner1-fill').html(data);
        });
    }

    function add_banner_2(){
        $.get('{{ route('home_banners.create', 2)}}', {}, function(data){
            $('#banner2-fill').html(data);
        });
    }

    function edit_home_banner_1(id){
        var url = '{{ route("home_banners.edit", "home_banner_id") }}';
        url = url.replace('home_banner_id', id);
        $.get(url, {}, function(data){
            $('#banner1-fill').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }

    function edit_home_banner_2(id){
        var url = '{{ route("home_banners.edit", "home_banner_id") }}';
        url = url.replace('home_banner_id', id);
        $.get(url, {}, function(data){
            $('#banner2-fill').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }

    function add_home_category(){
        $.get('{{ route('home_categories.create')}}', {}, function(data){
            $('#category-fill').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }

    function edit_home_category(id){
        var url = '{{ route("home_categories.edit", "home_category_id") }}';
        url = url.replace('home_category_id', id);
        $.get(url, {}, function(data){
            $('#category-fill').html(data);
            $('.demo-select2-placeholder').select2();
        });
    }

    function update_home_category_status(el){
        if(el.checked){
            var status = 1;
        }
        else{
            var status = 0;
        }
        $.post('{{ route('home_categories.update_status') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
            if(data == 1){
                showAlert('success', 'Home Page Category status updated successfully');
            }
            else{
                showAlert('danger', 'Something went wrong');
            }
        });
    }

    function update_banner_published(el){
        if(el.checked){
            var status = 1;
        }
        else{
            var status = 0;
        }
        $.post('{{ route('home_banners.update_status') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
            if(data == 1){
                showAlert('success', 'Banner status updated successfully');
            }
            else{
                showAlert('danger', 'Maximum 4 banners to be published');
            }
        });
    }

    function update_slider_published(el){
        if(el.checked){
            var status = 1;
        }
        else{
            var status = 0;
        }
        var url = '{{ route('sliders.update', 'slider_id') }}';
        url = url.replace('slider_id', el.value);

        $.post(url, {_token:'{{ csrf_token() }}', status:status, _method:'PATCH'}, function(data){
            if(data == 1){
                showAlert('success', 'Published sliders updated successfully');
            }
            else{
                showAlert('danger', 'Something went wrong');
            }
        });
    }

</script>

@endsection
