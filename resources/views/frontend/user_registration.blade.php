@extends('frontend.layouts.app')

@section('content')
<style>
    .password_confirm_style {
        color: #e63b05;
        font-weight: 600;
        font-size: 12px;
    }
</style>
<section class="reg-bg py-4 ">
    <div class="profile">
        <div class="container">
            <div class="row">
                <div class="pad-0 col-xxl-4 col-xl-5 col-lg-6 col-md-8 mx-auto">
                    <div class="card">
                        <div class="text-center px-35 pt-5">
                            <h1 class="heading heading-4 strong-500">
                                {{ translate('Create  account')}}
                            </h1>
                        </div>
                        <div class="px-5 py-3 py-lg-4">
                            <div class="">
                                <form class="form-default" role="form" action="#" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <div class="input-group input-group--style-1">
                                            <span class="log_addon input-group-addon">
                                                <i class="text-md fa fa-user"></i>
                                            </span>
                                            <input type="text"
                                                class="log_input form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                                value="{{ old('name') }}" placeholder="{{  translate('Name*') }}"
                                                name="name">

                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group input-group--style-1">
                                            <span class="log_addon input-group-addon">
                                                <i class="text-md fa fa-phone"></i>
                                            </span>
                                            <input type="text"
                                                class="log_input form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                                value="{{ old('phone') }}" placeholder="{{  translate('Phone*') }}"
                                                name="phone" maxlength="13">

                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>




                                    <div class="form-group">
                                        <div class="input-group input-group--style-1">
                                            <span class="log_addon input-group-addon">
                                                <i class="text-md fa fa-envelope"></i>
                                            </span>
                                            <input type="email"
                                                class="log_input form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                value="{{ old('email') }}" placeholder="{{  translate('Email*') }}"
                                                name="email">

                                            @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>




                                    <div class="form-group">
                                        <!-- <label>{{  translate('password') }}</label> -->
                                        <div class="input-group input-group--style-1">
                                            <span class="log_addon input-group-addon">
                                                <i class="text-md fa fa-lock"></i>
                                            </span>
                                            <input type="password" style="border-radius: 0px 19px 19px 0px; width:80%"
                                                class="log_input form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                placeholder="{{  translate('Password*') }}" name="password">

                                            @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <!-- <label>{{  translate('confirm_password') }}</label> -->
                                        <div class="input-group input-group--style-1">
                                            <span class="log_addon input-group-addon">
                                                <i class="text-md fa fa-lock"></i>
                                            </span>
                                            <input style="border-radius: 0px 19px 19px 0px; width:80%" type="password" class="log_input form-control"
                                                placeholder="{{  translate('Confirm Password*') }}"
                                                name="password_confirmation">

                                           

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_KEY') }}">
                                            @if ($errors->has('g-recaptcha-response'))
                                            <span class="invalid-feedback" style="display:block">
                                                <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="checkbox pad-btm text-left">
                                        <input class="magic-checkbox" type="checkbox" name="checkbox_example_1"
                                            id="checkboxExample_1a" required>
                                        <label for="checkboxExample_1a"
                                            class="text-sm">{{ translate('By signing up you agree to our ')}}<a
                                                href="{{route('privacypolicy')}}" target="_blank">Privacy Policy</a> and
                                            <a href="{{route('terms')}}" target="_blank">Terms of Services</a></label>
                                    </div>

                                    <div class="text-right mt-3">
                                        <button type="submit" id="btnsave"
                                            class="btn btn-styled btn-base-1 w-100 btn-md">{{  translate('Sign Up') }}</button>
                                    </div>
                                </form>
                                @if(!request()->get('referral_code'))
                                @if(\App\BusinessSetting::where('type', 'google_login')->first()->value == 1 ||
                                \App\BusinessSetting::where('type', 'facebook_login')->first()->value == 1 ||
                                \App\BusinessSetting::where('type', 'twitter_login')->first()->value == 1)
                                <div class="or or--1 mt-3 text-center">
                                    <span>or</span>
                                </div>
                                <div>
                                    @if (\App\BusinessSetting::where('type', 'facebook_login')->first()->value == 1)
                                    <a href="{{ route('social.login', ['provider' => 'facebook']) }}"
                                        class="btn btn-styled btn-block btn-facebook btn-icon--2 btn-icon-left px-4 mb-3">
                                        <i class="icon fa fa-facebook"></i> {{ translate('Login with Facebook')}}
                                    </a>
                                    @endif
                                    @if(\App\BusinessSetting::where('type', 'google_login')->first()->value == 1)
                                    <a href="{{ route('social.login', ['provider' => 'google']) }}"
                                        class="btn btn-styled btn-block btn-google btn-icon--2 btn-icon-left px-4 mb-3">
                                        <i class="icon fa fa-google"></i> {{ translate('Login with Google')}}
                                    </a>
                                    @endif
                                    @if (\App\BusinessSetting::where('type', 'twitter_login')->first()->value == 1)
                                    <a href="{{ route('social.login', ['provider' => 'twitter']) }}"
                                        class="btn btn-styled btn-block btn-twitter btn-icon--2 btn-icon-left px-4">
                                        <i class="icon fa fa-twitter"></i> {{ translate('Login with Twitter')}}
                                    </a>
                                    @endif
                                </div>
                                @endif
                                @endif
                            </div>
                        </div>

                        <div class="text-center px-35 pb-3">
                            <p class="text-md">
                                {{ translate('Already have an account?')}}<a href="{{ route('user.login') }}"
                                    class="strong-600">{{ translate('Log In')}}</a>
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
<script type="text/javascript">
    //  $.confirm({
//                         boxWidth: '30%',
//                         useBootstrap: false,
//                         title: 'SUCCESS!',
//                         content: "asdsadsa",
//                         type: 'green',
//                         icon: 'fa fa-check',
//                         typeAnimated: true,
//                         smoothContent:true,
//                          // hides the cancel button.
//                         buttons: {
//                         ok: function () {
//                             window.location.href = "{{url('/')}}";
//                         },
//                         cancelButton: false,
//                          }
//                         });
//  $.confirm({
//                         boxWidth: '30%',
//                         useBootstrap: false,
//                         title: 'ERROR!',
//                         content: "asdsadsa",
//                         type: 'green',
//                         icon: 'fa fa-warning',
//                         typeAnimated: true,
//                         smoothContent:true,
//                         cancelButton: false, // hides the cancel button.
                        
//                         });
//   $.confirm({
//                         type: 'green',
//                         icon:'fa fa-check',
//                         title: 'SUCCESS!',
//                         content:  "registration success" ,
//                         boxWidth: '100%',
//                         cancelButton: false,
//                         buttons: {
//                         ok: function () {
//                             window.location.href = "{{url('/')}}";
//                         },

//                         }
//                         });
    // var isPhoneShown = true;

    //     var input = document.querySelector("#phone-code");
    //     var iti = intlTelInput(input, {
    //         separateDialCode: true,
    //         preferredCountries: []
    //     });

    //     var countryCode = iti.getSelectedCountryData();


    //     input.addEventListener("countrychange", function() {
    //         var country = iti.getSelectedCountryData();
    //         $('input[name=country_code]').val(country.dialCode);
    //     });

        $(document).ready(function(){
//             $("input[name=phone]").keyup(function(){
//     var prefix = "+91"

//     if(this.value.indexOf(prefix) !== 0 ){
//         this.value = prefix + this.value;
//     }
// });
$('input[type="password"]').bind('keyup', function() {
      $('.label-important').remove(); 
      var password=$("input[name=password]").val();
        var password_confirmation=$("input[name=password_confirmation]").val();
        if(password_confirmation!=""){
           
            if(password==password_confirmation){
                return true;
               
            }
            else{
                $('.label-important').remove(); 
                $(this).after("<span class='label label-important' style='margin:4px; color: #e63b05;font-weight: 600;font-size: 12px;'>Password and Confirm Password does not match</span>");
            }
        }
      var strongRegex =/^((?!.*[\s])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#\$%\^&\*\s])(?=.*\d).{8,15})/;
    if(this.value.match(strongRegex)) 
        {  
          
        
        //alert('Correct, try another...')
        return true;
        }
        else
        { 
            $('.label-important').remove(); 
       
          $(this).after("<span class='label label-important' style='margin:4px;color: #e63b05;font-weight: 600;font-size: 12px;'>Your password must contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character, but cannot contain whitespace and must be at least eight characters long. </span>");
               return false;
        }
    });
   
$("input[name=phone]").focus(function(){
    if($("input[name=phone]").val()=="")
  $("input[name=phone]").val("+91");
});

  $("input[name=phone]").keyup(function(){
    // var prefix = "+91"

    // if(this.value.indexOf(prefix) !== 0 ){
    //     this.value = prefix + this.value;
    // }
    if($(this).val().indexOf('+91') == 0) {
        $(this).val($(this).val());
    }else{
      $phone_val=$(this).val();
      if($phone_val!=""){
        $(this).val("");
      }
      $(this).val("+91" + $(this).val());
    }
});
                        $('.email-form-group').hide();

                        $('form').submit(function(event) {

                            var strongRegex_second =/^((?!.*[\s])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#\$%\^&\*\s])(?=.*\d).{8,15})/;
                        var name=$("input[name=name]").val();
                        var phone=$("input[name=phone]").val();
                        var email=$("input[name=email]").val();
                        var password=$("input[name=password]").val();
                        var password_confirmation=$("input[name=password_confirmation]").val();
                        var token = $("input[name='_token']").val();
                        if(name=='' || phone=='' || email=='' || password=='' || password_confirmation==''){

                        var error_show='All *filled are mandatory';
                        showFrontendAlert("error", error_show);
                        return false;
                        }
                        $("#btnsave").attr("disabled", "disabled");
                   
                        if(!password.match(strongRegex_second) || !password_confirmation.match(strongRegex_second)){
                            $('.label-important').remove(); 
                            $('input[type="password"]').after("<span class='label label-important' style='margin:4px;color: #e63b05;font-weight: 600;font-size: 12px;'>Your password must contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character, but cannot contain whitespace and must be at least eight characters long. </span>");
           
                                if(password!=password_confirmation){
                                $('input[type="password"]').after("<span class='label label-important' style='margin:4px; color: #e63b05;font-weight: 600;font-size: 12px;'>Password and Confirm Password does not match</span>");
                                    }
                                    $("#btnsave").removeAttr("disabled", true);
                                return false;

                        }
               
                        var form_data=new FormData();
                        form_data.append('name',name);
                        form_data.append('phone',phone);
                        form_data.append('email',email);
                        form_data.append('password',password);
                        form_data.append('password_confirmation',password_confirmation);
                        form_data.append('_token',token);
                     
                        
                        $.ajax({
                        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url         : "{{route('registeruser')}}" ,// the url where we want to POST
                        data        : form_data, // our data object
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType:"json",
                     
                        success: function(data) {
                            $("#btnsave").removeAttr("disabled", true);

                        if(data.status==0){
                        showFrontendAlert("error", data.message);
                        }
                        else{
                        var jc = $.confirm({
                        boxWidth: '30%',
                        useBootstrap: false,
                        title: 'Please enter OTP',
                        content: '<div class="input-group input-group--style-1" style="padding:18px;"><span class="log_addon input-group-addon"><i class="text-md fa fa-lock"></i></span><input type="text" class="log_input form-control"   placeholder="OTP" id="otp" name="otp"  autocomplete="off" maxlength="6"></div>',
                        type: 'green',
                        icon: 'fa fa-mobile',
                        typeAnimated: true,
                        smoothContent:true,
                        buttons: {
           
                        resend: {
                        btnText: 'Send OTP',
                        btnClass: 'btn-danger',
                        action: function () {
                        jc.showLoading(true);


                        return $.ajax({
                            method: 'post',
                        url: "{{route('registeruser')}}",
                        dataType: 'json',
                        data: form_data,
                        processData: false,
                        contentType: false,
                        dataType:"json",
                     
                       
                        }).done(function (response) {

                        jc.hideLoading(true);
                        if (response.status == 1) {
                        jc.open(true);
                        } else {
                        //$("#login").attr("disabled", false);
                        $.alert({
                        title: 'Error!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: 'OTP Sending Proceess Faild',
                        boxWidth: '30%',
                        useBootstrap: false,
                    });
                       
                        }
                        }).fail(function () {
                       // $("#login").attr("disabled", false);
                       $.alert({
                        title: 'Error!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: 'OTP Sending Proceess Faild',
                        boxWidth: '30%',
                        useBootstrap: false,
                    });
                       
                    
                        
                        });
                        }
                        },
                        Next: {
                        btnClass: 'btn-primary',
                        action: function () {
                        jc.showLoading(true);

                        var otp = $("#otp").val();
                        if(otp==""){
                            jc.hideLoading(true);
                            $.alert({
                        title: 'Error!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: 'Please enter otp',
                        boxWidth: '30%',
                        useBootstrap: false,
                    });
                      
                        return false;
                        jc.open(true);
                        }
                        if (isNaN(otp)) {
                        jc.hideLoading(true);
                        $.alert({
                        title: 'Error!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: 'Otp must be an integer',
                        boxWidth: '30%',
                        useBootstrap: false,
                    });
                       
                        return false;
                        jc.open(true);
                        }
                   
                        
                        return $.ajax({
                            url: '{{route("customer_otp_check")}}',
                            dataType: 'json',
                            data: {'name': name, 'password': password,'phone': phone,'email': email, 'otp': otp, '_token': $("input[name='_token']").val()},
                            method: 'post'
                        }).done(function (response) {
                        //alert('hi');
                        jc.hideLoading(true);
                        if (response.status == 1 || response.status==2) {
                          
                        $.confirm({
                        boxWidth: '30%',
                        useBootstrap: false,
                        title: 'SUCCESS!',
                        content:response.message,
                        type: 'green',
                        icon: 'fa fa-check',
                        typeAnimated: true,
                        smoothContent:true,
                         // hides the cancel button.
                        buttons: {
                        ok: function () {
                            window.location.href = "{{url('/')}}";
                        },
                        cancelButton: false,
                         }
                        });
                           
                        } else {

                            $.confirm({
                        boxWidth: '30%',
                        useBootstrap: false,
                        title: 'ERROR!',
                        content:response.message,
                        type: 'red',
                        icon: 'fa fa-warning',
                        typeAnimated: true,
                        smoothContent:true,
                         // hides the cancel button.
                        buttons: {
                        ok: function () {
                            jc.open(true);
                           // window.location.href = "{{url('/')}}";
                        },
                        cancelButton: false,
                         }
                        });
                        
                        }
                        }).fail(function () {

                        $.confirm({
                        boxWidth: '30%',
                        type: 'red',
                        icon: 'fa fa-warning',
                        title: 'ERROR!',
                        content: 'OTP Verification Proceess Faild',
                        useBootstrap: false,
                        smoothContent:true,
                        
                        buttons: {
                        ok: function () {
                        //$("#login").attr("disabled", false);
                        },

                        }
                        });

                        });
                        }
                        },
                        close: function () {

                        }
                        },
                        onOpen: function () {


                        }
                        });
                        }

                        },

                        
                        error: function (jqXHR, textStatus, errorThrown) {
                            $("#btnsave").removeAttr("disabled", true);
                        var msg = "";
                        if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>Something Went Wrong, Please Reload and Try Again</strong>";
                        } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                        msg += "<strong><ul>";
                        $.each(jqXHR.responseJSON['errors'], function (key, value) {
                        msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                        }
                        }
                        isValid=false;
                        showFrontendAlert("error", msg);
                        }

                        });
                        // stop the form from submitting the normal way and refreshing the page
                         event.preventDefault();


                        });
                        });

        function autoFillSeller(){
            $('#email').val('seller@example.com');
            $('#password').val('123456');
        }
        function autoFillCustomer(){
            $('#email').val('customer@example.com');
            $('#password').val('123456');
        }

        function toggleEmailPhone(el){
            if(isPhoneShown){
                $('.phone-form-group').hide();
                $('.email-form-group').show();
                isPhoneShown = false;
                $(el).html('Use Phone Instead');
            }
            else{
                $('.phone-form-group').show();
                $('.email-form-group').hide();
                isPhoneShown = true;
                $(el).html('Use Email Instead');
            }
        }

        function startTimer() {
        $("#resend").attr('disabled', true);
        var counter = 30;
        $("#resend").text(counter + " Sec Remaining");
        setInterval(function() {
            counter--;
            if (counter >= 0) {
                //alert(counter);
                $("#resend").text(counter + " Sec Remaining");

            }
            if (counter === 0) {
                //alert('done');
                $("#resend").removeClass("btn-danger");
                $("#resend").text("Resend");
                $("#resend").addClass("btn-warning");
                $("#resend").attr('disabled', false);
            }
        }, 1000);
    }
</script>
@endsection