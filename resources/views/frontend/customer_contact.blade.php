@extends('frontend.layouts.app')

@section('content')
            <!-- Breadcrumb Area start -->
<style>
    /* Style the tab */
.tab {
  float: left;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
  width: 30%;
  height: 140px;
}

/* Style the buttons inside the tab */
.tab button {
  display: block;
  background-color: inherit;
  color: black;
  padding: 22px 16px;
  width: 100%;
  border: none;
  outline: none;
  text-align: left;
  cursor: pointer;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current "tab button" class */
.tab button.active {
  background-color: #ccc;
  border-right: 5px solid #ff7200;
}

/* Style the tab content */
.tabcontent {
float: left;
    padding: 0px 12px;
    border: 1px solid #ccc;
    width: 70%;
    height: 200px;
}
.contact-form {
    background-color: #ffffff;
}
.footer-herading {
    font-size: 16px;
    text-transform:none !important;
    padding-bottom: 8px;
    margin-bottom: 10px;
    position: relative;
    color: #253237;
    border-bottom: 1px solid #e3e3e3;
    font-weight: 700;
    line-height: 24px;
}
.home-cosmatics .footer-herading:after {
    background: #ff7200;
}
</style>            
            <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('home') }}">{{ translate('Home')}}</a></li>
                        <li><a href="javascript:">{{ translate('Contact Us')}}</a></li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
            <!-- Breadcrumb Area End -->
            <!-- contact area start -->
            <div class="contact-area" style="margin-bottom:60px;">
                <div class="container">
                    
                    <div class="custom-row-2">
                        
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="contact-form">
                                <div class="contact-title mb-30">
                                    <h2>{{translate('Get In Touch')}}</h2>
                                </div>
                                <div class="tab">
  <button class="tablinks" onclick="openCity(event, 'London')" id="defaultOpen">Merchant Support</button>
  <button class="tablinks" onclick="openCity(event, 'Paris')">Customer Support</button>
</div>

<div id="London" class="tabcontent">
  <h3 style="color: #ff7200;padding: 10px;">Merchant Support</h3>
   <div class="row col-lg-12 col-md-12 col-sm-12">
       <div class="col-md-4">
           <div class="about-footer">
                        @php
                        $generalsetting = \App\GeneralSetting::first();
                    @endphp
                        <h5 class="footer-herading">Head Office</h5>
                        <p class="text-info">Address: {{ $generalsetting->address }}
                           

                    </div>
       </div>
       <div class="col-md-3">
           <div class="about-footer">
                    
                        <h5 class="footer-herading">Raise a Query</h5>
                        <p class="text-info"><a href="https://www.seller.thelocal2vocal.com/support_ticket">{{ translate('Support Ticket') }}</a></p>

                    </div>
       </div>
       <div class="col-md-5">
           <div class="about-footer">
                        @php
                        $generalsetting = \App\GeneralSetting::first();
                    @endphp
                        <h5 class="footer-herading">Call Us</h5>
                        <p class="text-info">
                            Phone: {{ $generalsetting->phone }}
                            <br>Monday to Saturday (10am -6 pm)
                            <br>Email: {{ $generalsetting->email }}</p>

                    </div>
       </div>
    </div>
</div>

<div id="Paris" class="tabcontent">
  <h3 style="color: #ff7200;padding: 10px;">Customer Support</h3>
  <div class="row col-lg-12 col-md-12 col-sm-12">
       <div class="col-md-4">
           <div class="about-footer">
                        @php
                        $generalsetting = \App\GeneralSetting::first();
                    @endphp
                        <h5 class="footer-herading">Head Office</h5>
                        <p class="text-info">Address: {{ $generalsetting->address }}
                           

                    </div>
       </div>
       <div class="col-md-3">
           <div class="about-footer">
                    
                        <h5 class="footer-herading">Raise a Query</h5>
                        <p class="text-info"><a href="{{ route('support_ticket.index') }}">{{ translate('Support Ticket') }}</a></p>

                    </div>
       </div>
       <div class="col-md-5">
           <div class="about-footer">
                        @php
                        $generalsetting = \App\GeneralSetting::first();
                    @endphp
                        <h5 class="footer-herading">Call Us</h5>
                        <p class="text-info">
                            Phone: {{ $generalsetting->phone }}
                            <br>Monday to Saturday (10am -6 pm)
                            <br>Email: {{ $generalsetting->email }}</p>

                    </div>
       </div>
    </div>
</div>


<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
                                <!--<form class="contact-form-style" id="contact-form" action="{{ route('store_contact') }}" method="POST" enctype="multipart/form-data" id="form1">-->
                                <!--    @csrf-->
                                <!--    <div class="row">-->
                                <!--        <div class="col-lg-6">-->
                                <!--            <input name="name" placeholder="Name*" type="text" required/>-->
                                <!--        </div>-->
                                <!--        <div class="col-lg-6">-->
                                <!--            <input name="email" placeholder="Email*" type="email" required/>-->
                                <!--        </div>-->
                                <!--        <div class="col-lg-12">-->
                                <!--            <input name="subject" placeholder="Subject*" type="text" required/>-->
                                <!--        </div>-->
                                <!--        <div class="col-lg-12">-->
                                <!--            <textarea name="message" placeholder="Your Message*" required></textarea>-->
                                <!--            <button class="submit" type="submit">SEND</button>-->
                                <!--        </div>-->
                                <!--    </div>-->
                                <!--</form>-->
                                <p class="form-messege"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- contact area end -->
			
			
            @stop