<!-- Menu Content Start -->
                <div class="header-buttom-nav sticky-nav">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-left d-none d-lg-block">
                                <div class="d-flex align-items-start justify-content-start">
                                    <!-- Beauty Category -->
                                    <div class="beauty-category vertical-menu">
                                        <h3 class="vertical-menu-heading vertical-menu-toggle">All Categories</h3>
                                        <ul class="vertical-menu-wrap open-menu-toggle">
                                            @foreach (\App\Category::all()->take(5) as $key => $category)
                                            @php
                                                $brands = array();
                                            @endphp
                                           
                                            <li class="menu-dropdown">
                                                <a href="{{ route('products.category', $category->slug) }}">{{ __($category->name) }}<i class="ion-ios-arrow-down"></i></a>  
                                                @if(count($category->subcategories)>0)
                                                <ul class="mega-menu-wrap">
                                                @foreach ($category->subcategories as $key=> $subcategory)
                                                @if ($key==4)
                                                @php
                                                    break;
                                                @endphp
                                            @endif
                                         
                                                    <li>
                                                        <ul>
                                                            <li class="mega-menu-title"><a href="{{ route('products.subcategory', $subcategory->slug) }}">{{ $subcategory->name}}</a></li>
                                                            @foreach ($subcategory->subsubcategories as $subsubcategory)
                                                 
                                                            <li><a href="{{ route('products.subsubcategory', $subsubcategory->slug) }}">{{ __($subsubcategory->name) }}</a></li>
                                                            @endforeach
                                                        </ul>
                                                    </li>

                                                   
                                                @endforeach
                                            </ul>
                                                @endif
                                            </li>
                                          
                                           
                                          
                                            @endforeach
                                            <li>
                                                {{-- <a href="{{route('products')}}" id="more-btn"><i class="ion-ios-plus-empty" aria-hidden="true"></i> More Categories</a> --}}
                                                <a href="{{route('categories.all')}}" ><i class="ion-ios-plus-empty" aria-hidden="true"></i> More Categories</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- Beauty Category -->
                                    <!--Main Navigation Start -->
                                    <div class="main-navigation d-none d-lg-block">
                                        <ul>
                                            <li class="menu-dropdown">
                                                <a href="{{route('products.daily_deals')}}">Daily Deals </a>
                                                
                                            </li>
                                            <li class="menu-dropdown">
                                                <a href="{{route('products.best_sellers_profile')}}">Best Seller</a>
                                               
                                            </li>
                                            <li class="menu-dropdown">
                                                <a href="{{route('products.services')}}">Digital Products</a>
                                                
                                            </li>
                                            <li class="menu-dropdown">
                                                <a href="{{route('products.new_arrival')}}">New Release</a>
                                                
                                            </li>
                                            
                                           
                                        </ul>
                                    </div>
                                    <!--Main Navigation End -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Menu Content End -->
                