<style>
    .about_footer{
        width: 90%;
    }
    .pay_sec{
            position: absolute;
    bottom: 25px;
        font-weight: 600;
color:black;
    }
    .foot_pay{
        right: 0px;
    position: absolute;
    }
</style>
<!-- Footer Area start -->
<footer class="footer-area">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <!-- footer single wedget -->
                <div class="col-md-4 col-lg-4">
                    <!-- footer logo -->
                    <div class="footer-logo">
                        <a href="/"><img src="{{ asset('assets/images/logo/logo-cosmatics.png')}}" alt=""
                                style="width:60%;" /></a>
                    </div>
                    <!-- footer logo -->
                    <div class="about-footer">
                        @php
                        $generalsetting = \App\GeneralSetting::first();
                    @endphp
                        <h5>About Us</h5>
                        <p class="text-info">Address: {{ $generalsetting->address }}
                           
                            <br>Phone: {{ $generalsetting->phone }}
                            <br>Email: {{ $generalsetting->email }}</p>

                    </div>
                    <div class="social-info">
                        @php
                        $generalsetting = \App\GeneralSetting::first();
                        @endphp
                        <ul class="my-3 my-md-0 social-nav model-2">
                        @if ($generalsetting->facebook != null)
                            <li>
                                <a href="{{ $generalsetting->facebook }}" class="facebook" target="_blank" data-toggle="tooltip" data-original-title="Facebook">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                        @endif
                        @if ($generalsetting->instagram != null)
                            <li>
                                <a href="{{ $generalsetting->instagram }}" class="instagram" target="_blank" data-toggle="tooltip" data-original-title="Instagram">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                        @endif
                        @if ($generalsetting->twitter != null)
                            <li>
                                <a href="{{ $generalsetting->twitter }}" class="twitter" target="_blank" data-toggle="tooltip" data-original-title="Twitter">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                        @endif
                        @if ($generalsetting->youtube != null)
                            <li>
                                <a href="{{ $generalsetting->youtube }}" class="youtube" target="_blank" data-toggle="tooltip" data-original-title="Youtube">
                                    <i class="fa fa-youtube"></i>
                                </a>
                            </li>
                        @endif
                        @if ($generalsetting->google_plus != null)
                            <li>
                                <a href="{{ $generalsetting->google_plus }}" class="google-plus" target="_blank" data-toggle="tooltip" data-original-title="Google Plus">
                                    <i class="fa fa-google-plus"></i>
                                </a>
                            </li>
                        @endif
                    </ul>
                    </div>
                </div>
                <!-- footer single wedget -->
                <div class="col-md-2 col-lg-2 mt-res-sx-30px mt-res-md-30px">
                    <div class="single-wedge">
                        <h4 class="footer-herading">Information</h4>
                        <div class="footer-links">
                            <ul>
                                <li><a href="/about-us">About Us</a></li>
                                <li><a href="{{ route('customer_contact') }}">{{ translate('Contact Us') }}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- footer single wedget -->
                <div class="col-md-2 col-lg-2 mt-res-md-50px mt-res-sx-30px mt-res-md-30px">
                    <div class="single-wedge">
                        <h4 class="footer-herading">Policy</h4>
                        <div class="footer-links">
                            <ul>
                            <li><a href="{{ route('sellerpolicy') }}">{{ translate('Seller Policy') }}</a></li>
                                <li><a href="{{ route('returnpolicy') }}">{{ translate('Return Policy') }}</a></li>
                                <li><a href="{{ route('supportpolicy') }}">{{ translate('Support Policy') }}</a></li>
                                <li><a href="{{ route('terms') }}">{{ translate('Terms & Conditions') }}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- footer single wedget -->
                <div class="col-md-2 col-lg-2 mt-res-md-50px mt-res-sx-30px mt-res-md-30px">
                    <div class="single-wedge">
                        <h4 class="footer-herading">FAQ</h4>
                        <div class="footer-links">
                            <ul>
                                <li><a href="{{ route('customerfaq') }}">{{ translate('Customers FAQ') }}</a></li>
                                <li><a href="{{ route('sellerfaq') }}">{{ translate('Sellers FAQ') }}</a></li>
                                <li><img class="about_footer" src="/uploads/ckupload/Made_in_India_Local2vocal.png"></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- footer single wedget -->
                <div class="col-md-2 col-lg-2 mt-res-md-50px mt-res-sx-30px mt-res-md-30px">
                    <div class="single-wedge">
                        <h4 class="footer-herading"> {{ translate('My Account') }}</h4>
                        <div class="footer-links">
                            <ul>
                                @if (Auth::check())
                                <li>
                                    <a href="{{ route('logout') }}" title="Logout">
                                        {{ translate('Logout') }}
                                    </a>
                                </li>
                                @else
                                <li>
                                    <a href="{{ route('user.login') }}" title="Login">
                                        {{ translate('Login') }}
                                    </a>
                                </li>
                                @endif
                                <li>
                                    <a href="{{ route('purchase_history.index') }}" title="Order History">
                                        {{ translate('Order History') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('wishlists.index') }}" title="My Wishlist">
                                        {{ translate('My Wishlist') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('orders.track') }}" title="Track Order">
                                        {{ translate('Track Order') }}
                                    </a>
                                </li>
                            </ul>
                        </div>
                        @if (\App\BusinessSetting::where('type', 'vendor_system_activation')->first()->value == 1)
                        @guest
                        <div class="col text-center text-md-left" style="padding-left: 0px;">
                            <div class="mt-4">
                                <h4 class="heading heading-xs strong-600 text-uppercase mb-2">
                                    {{ translate('Be a Seller') }}
                                </h4>
                                <a href="https://{{env('APP_SUBDOMAIN')}}/shops/create" class="btn btn-base-1 btn-icon-left">
                                    {{ translate('Apply Now') }}
                                </a>
                            </div>
                        </div>
                        @endguest
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  Footer Bottom Area start -->
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-8" style="display: contents;">
                @php
                            echo $generalsetting->description;
                        @endphp
            <p>&nbsp;Designed and Developed by <a href="https://socialplanet.in/" target="_blank">Social Planet</a>
</p>

                </div>
                <div class="col-md-6 col-lg-4 foot_pay">
                <div class="text-center text-md-right">
                    <span class="pay_sec">Secure and Trusted Payment</span>
                        <ul class="inline-links">
                            <li>
                                    <img loading="lazy" alt="cash on delivery" src="{{ asset('frontend/images/icons/cards/download1.png')}}" height="20">
                                </li>
                                <li>
                                    <img loading="lazy" alt="cash on delivery" src="{{ asset('frontend/images/icons/cards/download2.png')}}" height="20">
                                </li>
                                <li>
                                    <img loading="lazy" alt="cash on delivery" src="{{ asset('frontend/images/icons/cards/download3.png')}}" height="20">
                                </li>
                                <li>
                                    <img loading="lazy" alt="cash on delivery" src="{{ asset('frontend/images/icons/cards/download4.png')}}" height="20">
                                </li>
                                <li>
                                    <img loading="lazy" alt="cash on delivery" src="{{ asset('frontend/images/icons/cards/download5.png')}}" height="20">
                                </li>
                                
                            <!-- @if (\App\BusinessSetting::where('type', 'paypal_payment')->first()->value == 1)
                                <li>
                                    <img loading="lazy" alt="paypal" src="{{ asset('frontend/images/icons/cards/paypal.png')}}" height="20">
                                </li>
                            @endif
                            @if (\App\BusinessSetting::where('type', 'stripe_payment')->first()->value == 1)
                                <li>
                                    <img loading="lazy" alt="stripe" src="{{ asset('frontend/images/icons/cards/stripe.png')}}" height="20">
                                </li>
                            @endif
                            @if (\App\BusinessSetting::where('type', 'sslcommerz_payment')->first()->value == 1)
                                <li>
                                    <img loading="lazy" alt="sslcommerz" src="{{ asset('frontend/images/icons/cards/sslcommerz.png')}}" height="20">
                                </li>
                            @endif
                            @if (\App\BusinessSetting::where('type', 'instamojo_payment')->first()->value == 1)
                                <li>
                                    <img loading="lazy" alt="instamojo" src="{{ asset('frontend/images/icons/cards/instamojo.png')}}" height="20">
                                </li>
                            @endif -->
                            @if (\App\BusinessSetting::where('type', 'razorpay')->first()->value == 1)
                                <li>
                                    <!--<img loading="lazy" alt="razorpay" src="{{ asset('frontend/images/icons/cards/rozarpay.png')}}" height="20">-->
                                </li>
                            @endif
                            <!-- @if (\App\BusinessSetting::where('type', 'voguepay')->first()->value == 1)
                                <li>
                                    <img loading="lazy" alt="voguepay" src="{{ asset('frontend/images/icons/cards/voguepay.png')}}" height="20">
                                </li>
                            @endif
                            @if (\App\BusinessSetting::where('type', 'paystack')->first()->value == 1)
                                <li>
                                    <img loading="lazy" alt="paystack" src="{{ asset('frontend/images/icons/cards/paystack.png')}}" height="20">
                                </li>
                            @endif
                            @if (\App\BusinessSetting::where('type', 'payhere')->first()->value == 1)
                                <li>
                                    <img loading="lazy" alt="payhere" src="{{ asset('frontend/images/icons/cards/payhere.png')}}" height="20">
                                </li>
                            @endif -->
                            @if (\App\BusinessSetting::where('type', 'cash_payment')->first()->value == 1)
                                <!--<li>-->
                                <!--    <img loading="lazy" alt="cash on delivery" src="{{ asset('frontend/images/icons/cards/cod.png')}}" height="20">-->
                                <!--</li>-->
                            @endif
                            <!-- @if (\App\Addon::where('unique_identifier', 'offline_payment')->first() != null && \App\Addon::where('unique_identifier', 'offline_payment')->first()->activated)
                                @foreach(\App\ManualPaymentMethod::all() as $method)
                                  <li>
                                    <img loading="lazy" alt="{{ $method->heading }}" src="{{ asset($method->photo)}}" height="20">
                                </li>
                                @endforeach
                            @endif -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  Footer Bottom Area End-->
</footer>
<!--  Footer Area End -->