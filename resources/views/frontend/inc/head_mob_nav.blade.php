<style>
    @media screen and (max-width: 970px) {
        .navbar1 {
    overflow: hidden;
    background-color: #ffffff;
    position: fixed;
    bottom: 0;
    width: 100%;
    padding: 0px;
    box-shadow: 0 0 12px black;
    visibility: visible;
        z-index: 99999;
}
}
  .mean-container a.meanmenu-reveal span {
    display: block;
    background: #333;
    height: 2px;
    margin-top: 3px;
    position: relative;
    z-index: 999999;
    width: 18px;
    margin: 4px 0 4px 0;
}  
@media only screen and (min-width: 970px) {
.navbar1 {
    overflow: hidden;
    background-color: #ffffff;
    position: fixed;
    bottom: 0;
    width: 100%;
    padding: 0px;
    box-shadow: 0 0 12px black;
    visibility: hidden !important;
}
}  
.navbar1 a {
    float: left;
    display: block;
    color: #f27b00;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
    font-size: 26px;
    font-weight: 800;
    border-left: 1px solid #d1c6bb;
    width: 20%;
}

.navbar1 a:hover {
  background: #f1f1f1;
  color: black;
}

.navbar1 a.active {
    background-color: #f27b00;
    color: white;
}
</style>
<div class="navbar1">
  <a href="{{ route('home') }}" class="active"><i class="fa fa-home" aria-hidden="true"></i></a>
  <a href="{{ route('compare') }}"><i class="fa fa-compress"></i></a>
  <a href="{{ route('wishlists.index') }}"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
  <a href="{{ route('cart') }}"><i class="fa fa-shopping-bag" aria-hidden="true"></i></a>
  <a href=/dashboard><i class="fa fa-user" aria-hidden="true"></i></a>
</div>
<!-- Header Buttom Start -->
<div class="header-navigation sticky-nav d-lg-none">
                    <div class="container position-relative">
                        <div class="row">
                            <!-- mobile menu -->
    <div class="mobile-side-menu d-lg-none">
        <div class="side-menu-overlay opacity-0" onclick="sideMenuClose()"></div>
        <div class="side-menu-wrap opacity-0">
            <div class="side-menu closed">
                <div class="side-menu-header ">
                    <div class="side-menu-close" onclick="sideMenuClose()">
                        <i class="fa fa-close"></i>
                    </div>
                    @auth
                            @if(Auth::user()->user_type=="customer")
                    @auth
                        <div class="widget-profile-box px-3 py-4 d-flex align-items-center">
                            @if (Auth::user()->avatar_original != null)
                                <div class="image " style="background-image:url('{{ asset(Auth::user()->avatar_original) }}')"></div>
                            @else
                                <div class="image " style="background-image:url('{{ asset('frontend/images/user.png') }}')"></div>
                            @endif

                            <div class="name">{{ Auth::user()->name }}</div>
                        </div>
                        <div class="side-login px-3 pb-3">
                            <a href="{{ route('logout') }}">{{translate('Sign Out')}}</a>
                        </div>
                    @else
                        <div class="widget-profile-box px-3 py-4 d-flex align-items-center">
                                <div class="image " style="background-image:url('{{ asset('frontend/images/icons/user-placeholder.jpg') }}')"></div>
                        </div>
                        <div class="side-login px-3 pb-3">
                            <a href="{{ route('user.login') }}">{{translate('Sign In')}}</a>
                            <a href="{{ route('user.registration') }}">{{translate('Registration')}}</a>
                        </div>
                        
                    @endauth
                </div>
                <div class="side-menu-list px-3">
                    <ul class="side-user-menu">
                    <li>
                    <a href="{{ route('dashboard') }}" class="{{ areActiveRoutesHome(['dashboard'])}}">
                        <i class="la la-dashboard"></i>
                        <span class="category-name">
                            {{translate('Dashboard')}}
                        </span>
                    </a>
                </li>

                @if(\App\BusinessSetting::where('type', 'classified_product')->first()->value == 1)
                <!-- <li>
                    <a href="{{ route('customer_products.index') }}" class="{{ areActiveRoutesHome(['customer_products.index', 'customer_products.create', 'customer_products.edit'])}}">
                        <i class="la la-diamond"></i>
                        <span class="category-name">
                            {{translate('Classified Products')}}
                        </span>
                    </a>
                </li> -->
                @endif
                @php
                    $delivery_viewed = App\Order::where('user_id', Auth::user()->id)->where('delivery_viewed', 0)->get()->count();
                    $payment_status_viewed = App\Order::where('user_id', Auth::user()->id)->where('payment_status_viewed', 0)->get()->count();
                    $refund_request_addon = \App\Addon::where('unique_identifier', 'refund_request')->first();
                    $club_point_addon = \App\Addon::where('unique_identifier', 'club_point')->first();
                @endphp
                <li>
                    <a href="{{ route('purchase_history.index') }}" class="{{ areActiveRoutesHome(['purchase_history.index'])}}">
                        <i class="la la-file-text"></i>
                        <span class="category-name">
                            {{translate('Purchase History')}} @if($delivery_viewed > 0 || $payment_status_viewed > 0)<span class="ml-2" style="color:green"><strong>({{ translate('New Notifications') }})</strong></span>@endif
                        </span>
                    </a>
                </li>
                @php
                $license = \App\OrderDetail::join('orders','orders.id','=','order_details.order_id')->where('orders.user_id','=',Auth::user()->id)
                            ->where('is_viewed', 0)
                           
                            ->count();
            @endphp
                <li>
                    <a href="{{ route('digital_purchase_history.index') }}" class="{{ areActiveRoutesHome(['digital_purchase_history.index'])}}">
                        <i class="la la-download"></i>
                        <span class="category-name">
                            {{translate('Digital Product')}} @if($license > 0)<span class="ml-2" style="color:green"><strong>({{ $license }} {{ translate('New') }})</strong></span></span>@endif
                        </span>
                        
                    </a>
                </li>

                @if ($refund_request_addon != null && $refund_request_addon->activated == 1)
                    <li>
                        <a href="{{ route('customer_refund_request') }}" class="{{ areActiveRoutesHome(['customer_refund_request'])}}">
                            <i class="la la-file-text"></i>
                            <span class="category-name">
                                {{translate('Sent Refund Request')}}
                            </span>
                        </a>
                    </li>
                @endif
                <li>
                    <a href="{{ route('refund.index') }}" class="{{ areActiveRoutesHome(['refund.index'])}}">
                        <i class="fa fa-compress"></i>
                        <span class="category-name">
                            {{translate('Return/Refund')}}
                        </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('wishlists.index') }}" class="{{ areActiveRoutesHome(['wishlists.index'])}}">
                        <i class="la la-heart-o"></i>
                        <span class="category-name">
                            {{translate('Wishlist')}}
                        </span>
                    </a>
                </li>
                @if (\App\BusinessSetting::where('type', 'conversation_system')->first()->value == 1)
                    @php
                        $conversation = \App\Conversation::where('sender_id', Auth::user()->id)->where('sender_viewed', 0)->get();
                    @endphp
                    <li>
                        <a href="{{ route('conversations.index') }}" class="{{ areActiveRoutesHome(['conversations.index', 'conversations.show'])}}">
                            <i class="la la-comment"></i>
                            <span class="category-name">
                                {{translate('Conversations')}}
                                @if (count($conversation) > 0)
                                    <span class="ml-2" style="color:green"><strong>({{ count($conversation) }})</strong></span>
                                @endif
                            </span>
                        </a>
                    </li>
                @endif
                <li>
                    <a href="{{ route('profile') }}" class="{{ areActiveRoutesHome(['profile'])}}">
                        <i class="la la-user"></i>
                        <span class="category-name">
                            {{translate('Manage Profile')}}
                        </span>
                    </a>
                </li>
                @if (\App\BusinessSetting::where('type', 'wallet_system')->first()->value == 1)
                    <li>
                        <a href="{{ route('wallet.index') }}" class="{{ areActiveRoutesHome(['wallet.index'])}}">
                            <i class="la la-inr"></i>
                            <span class="category-name">
                                {{translate('My Wallet')}}
                            </span>
                        </a>
                    </li>
                @endif

                @if ($club_point_addon != null && $club_point_addon->activated == 1)
                    <li>
                        <a href="{{ route('earnng_point_for_user') }}" class="{{ areActiveRoutesHome(['earnng_point_for_user'])}}">
                            <i class="la la-dollar"></i>
                            <span class="category-name">
                                {{translate('Earning Points')}}
                            </span>
                        </a>
                    </li>
                @endif

                @if (\App\Addon::where('unique_identifier', 'affiliate_system')->first() != null && \App\Addon::where('unique_identifier', 'affiliate_system')->first()->activated && Auth::user()->affiliate_user != null && Auth::user()->affiliate_user->status)
                    <li>
                        <a href="{{ route('affiliate.user.index') }}" class="{{ areActiveRoutesHome(['affiliate.user.index', 'affiliate.payment_settings'])}}">
                            <i class="la la-inr"></i>
                            <span class="category-name">
                                {{translate('Affiliate System')}}
                            </span>
                        </a>
                    </li>
                @endif
                @php
                    $support_ticket = DB::table('tickets')
                                ->where('client_viewed', 0)
                                ->where('user_id', Auth::user()->id)
                                ->count();
                @endphp
                <li>
                    <a href="{{ route('support_ticket.index') }}" class="{{ areActiveRoutesHome(['support_ticket.index'])}}">
                        <i class="la la-support"></i>
                        <span class="category-name">
                            {{translate('Support Ticket')}} @if($support_ticket > 0)<span class="ml-2" style="color:green"><strong>({{ $support_ticket }} {{ translate('New') }})</strong></span></span>@endif
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#" class="{{ areActiveRoutesHome(['refer_show'])}}" onclick="getReferLink();">
                        <i class="la la-support"></i>
                        <span class="category-name">
                            {{translate('Referral Link')}} 
                        </span>
                    </a>
                </li>
                <li>
                    <a href="/logout" class="">
                        <i class="fa fa-sign-out"></i>
                        <span class="category-name">
                          <span class="ml-2" style="color:green"><strong>{{ translate('Logout') }}</strong></span></span>
                        </span>
                    </a>
                </li>  @endif
                    @endauth
                        </ul>
                       
                      
                    <!-- <div class="sidebar-widget-title py-0">
                        <span>Categories</span>
                    </div>
                    <ul class="side-seller-menu">
                        @foreach (\App\Category::all() as $key => $category)
                            <li>
                            <a href="{{ route('products.category', $category->slug) }}" class="text-truncate">
                                <img class="cat-image lazyload" src="{{ asset('frontend/images/placeholder.jpg') }}" data-src="{{ asset($category->icon) }}" width="13" alt="{{ __($category->name) }}">
                                <span>{{ __($category->name) }}</span>
                            </a>
                        </li>
                        @endforeach
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
    <!-- end mobile menu -->
    
                            <!-- Logo Start -->
                            <div class="col-md-2 col-sm-2">
                            
                            <div class="logo">
                                    <a href="{{env('APP_URL')}}"><img src="{{asset('assets/images/logo/logo-cosmatics.png') }}" style="width:160%;" alt="" /></a>
                                </div>
                            </div>
                            <!-- Logo End -->
                            <!-- Navigation Start -->
                            <div class="col-md-10 col-sm-10">
                               
                                <!--Main Navigation End -->
                                <!--Header Bottom Account Start -->
                                <div class="header_account_area">
                                    <!--Seach Area Start -->
                                    <div class="header_account_list search_list">
                                        <a href="javascript:void(0)"><i class="ion-ios-search-strong"></i></a>
                                        <div class="dropdown_search">
                                            <form action="{{ route('search') }}" method="GET">
                                            <i class="fa fa-search icon"></i> 
                                                <input style="font-family: FontAwesome,sans-serif;" aria-label="Search" id="search" name="q"  autocomplete="off" placeholder="Search Now..." type="text"  class="search_front"/>

                                                <div class="typed-search-box d-none">
                                            <div class="search-preloader">
                                                <div class="loader"><div></div><div></div><div></div></div>
                                            </div>
                                            <div class="search-nothing d-none">

                                            </div>
                                            <div id="search-content">

                                            </div>
                                        </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!--Seach Area End -->
                                    <!--Contact info Start -->
                                   
                                    <!--Contact info End -->
                                    <!--Cart info Start -->
                                    <div class="d-inline-block" data-hover="dropdown">
                                    <div class="nav-cart-box dropdown" id="cart_items">
                                        <a href="" class="nav-box-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="ion-bag d-inline-block nav-box-icon cart_icon_home"></i>
                                            
                                            @if(Session::has('cart'))
                                                <span class="nav-box-number">{{ count(Session::get('cart'))}}</span>
                                            @else
                                                <span class="nav-box-number">0</span>
                                            @endif
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right px-0">
                                            <li>
                                                <div class="dropdown-cart px-0">
                                                    @if(Session::has('cart'))
                                                        @if(count($cart = Session::get('cart')) > 0)
                                                            <div class="dc-header">
                                                                <h3 class="heading heading-6 strong-700">{{translate('Cart Items')}}</h3>
                                                            </div>
                                                            <div class="dropdown-cart-items c-scrollbar">
                                                                @php
                                                                    $total = 0;
                                                                @endphp
                                                                @foreach($cart as $key => $cartItem)
                                                                    @php
                                                                        $product = \App\Product::find($cartItem['id']);
                                                                        $total = $total + $cartItem['price']+$cartItem['tax']*$cartItem['quantity'];
                                                                    @endphp
                                                                    <div class="dc-item">
                                                                        <div class="d-flex align-items-center">
                                                                            <div class="dc-image">
                                                                                <a href="{{ route('product', $product->slug) }}">
                                                                                    <img src="{{ asset('frontend/images/placeholder.jpg') }}" data-src="{{ asset($product->thumbnail_img) }}" class="img-fluid lazyload" alt="{{ __($product->name) }}">
                                                                                </a>
                                                                            </div>
                                                                            <div class="dc-content">
                                                                                <span class="d-block dc-product-name text-capitalize strong-600 mb-1">
                                                                                    <a href="{{ route('product', $product->slug) }}">
                                                                                        {{ __($product->name) }}
                                                                                    </a>
                                                                                </span>

                                                                                <span class="dc-quantity">x{{ $cartItem['quantity'] }}</span>
                                                                                <span class="dc-price">{{ single_price($cartItem['price']+$cartItem['tax']*$cartItem['quantity']) }}</span>
                                                                            </div>
                                                                            <div class="dc-actions">
                                                                                <button onclick="removeFromCart({{ $key }})">
                                                                                    <i class="la la-close"></i>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                            <div class="dc-item py-3">
                                                                <span class="subtotal-text">{{translate('Subtotal')}}</span>
                                                                <span class="subtotal-amount">{{ single_price($total) }}</span>
                                                            </div>
                                                            <div class="py-2 text-center dc-btn">
                                                                <ul class="inline-links inline-links--style-3">
                                                                    <li class="px-1">
                                                                        <a href="{{ route('cart') }}" class="link link--style-1 text-capitalize btn btn-base-1 px-3 py-1">
                                                                            <i class="la la-shopping-cart"></i> {{translate('View cart')}}
                                                                        </a>
                                                                    </li>
                                                                    @if (Auth::check())
                                                                    <li class="px-1">
                                                                        <a href="{{ route('checkout.shipping_info') }}" class="link link--style-1 text-capitalize btn btn-base-1 px-3 py-1 light-text">
                                                                            <i class="la la-mail-forward"></i> {{translate('Checkout')}}
                                                                        </a>
                                                                    </li>
                                                                    @endif
                                                                </ul>
                                                            </div>
                                                        @else
                                                            <div class="dc-header">
                                                                <h3 class="heading heading-6 strong-700">{{translate('Your Cart is empty')}}</h3>
                                                            </div>
                                                        @endif
                                                    @else
                                                        <div class="dc-header">
                                                            <h3 class="heading heading-6 strong-700">{{translate('Your Cart is empty')}}</h3>
                                                        </div>
                                                    @endif
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                    <!--Cart info End -->
                                </div>
                            </div>
                        </div>
                        <!-- mobile menu -->
                        <div class="mobile-menu-area">
                            <div class="mobile-menu">
                                <nav id="mobile-menu-active">
                                    <ul class="menu-overflow">
                                            <li class="menu-dropdown">
                                                <a href="{{route('products.daily_deals')}}">Daily Deals </a>
                                                
                                            </li>
                                            <li class="menu-dropdown">
                                                <a href="{{route('products.best_sellers_profile')}}">Best Seller</a>
                                               
                                            </li>
                                            <li class="menu-dropdown">
                                                <a href="{{route('products.services')}}">Digital Products</a>
                                                
                                            </li>
                                            <li class="menu-dropdown">
                                                <a href="{{route('products.new_arrival')}}">New Release</a>
                                                
                                            </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <!-- mobile menu end-->
                    </div>
                </div>
                <!--Header Bottom Account End -->