<!-- Menu Content Start -->
                <div class="header-buttom-nav sticky-nav">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-left d-none d-lg-block">
                                <div class="d-flex align-items-start justify-content-start">
                                    <!-- Beauty Category -->
                                    <div class="beauty-category vertical-menu">
                                        <h3 class="vertical-menu-heading vertical-menu-toggle">All Categories</h3>
                                        <ul class="vertical-menu-wrap open-menu-toggle">
                                            <li class="menu-dropdown">
                                                <a href="#">Beauty & Healt<i class="ion-ios-arrow-down"></i></a>
                                                <ul class="mega-menu-wrap">
                                                    <li>
                                                        <ul>
                                                            <li class="mega-menu-title"><a href="#">Makeup</a></li>
                                                            <li><a href="#">Eyes</a></li>
                                                            <li><a href="#">Lips</a></li>
                                                            <li><a href="#">Face</a></li>
                                                            <li><a href="#">Makeup Tools</a></li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <ul>
                                                            <li class="mega-menu-title"><a href="#">Skin Care</a></li>
                                                            <li><a href="#">Face</a></li>
                                                            <li><a href="#">Eyes</a></li>
                                                            <li><a href="#">Body</a></li>
                                                            <li><a href="#">Skin Care Tools</a></li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <ul>
                                                            <li class="mega-menu-title"><a href="#">Health Care</a></li>
                                                            <li><a href="#">Massage & Relaxation</a></li>
                                                            <li><a href="#">Household Health Monitors</a></li>
                                                            <li><a href="#">Chinese Medicine</a></li>
                                                            <li><a href="#">Personal Health Care Items</a></li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <ul>
                                                            <li class="mega-menu-title"><a href="#">Nail Art & Tools</a></li>
                                                            <li><a href="#">Gel Nail Polish</a></li>
                                                            <li><a href="#">Nail Drills</a></li>
                                                            <li><a href="#">Nail Dryers</a></li>
                                                            <li><a href="#">Nail Glitter</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="banner-wrapper">
                                                        <a href="single-product.html"><img src="assets/images/banner-image/banner-menu-2.jpg" alt="" /></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Massage & Relaxation</a></li>
                                            <li><a href="#">Household Health Monitors</a></li>
                                            <li><a href="#">Chinese Medicine</a></li>
                                            <li><a href="#">Gel Nail Polish</a></li>
                                            <li><a href="#">Makeup Tools</a></li>
                                            <li class="hidden"><a href="shop-4-column.html">Skin Care Tools</a></li>
                                            <li>
                                                <a href="#" id="more-btn"><i class="ion-ios-plus-empty" aria-hidden="true"></i> More Categories</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- Beauty Category -->
                                    <!--Main Navigation Start -->
                                    <div class="main-navigation d-none d-lg-block">
                                        <ul>
                                            <li class="menu-dropdown">
                                                <a href="#">Home <i class="ion-ios-arrow-down"></i></a>
                                                <ul class="sub-menu">
                                                    <li class="menu-dropdown position-static">
                                                        <a href="#">Home Organic <i class="ion-ios-arrow-down"></i></a>
                                                        <ul class="sub-menu sub-menu-2">
                                                            <li><a href="index.html">Organic 1</a></li>
                                                            <li><a href="index-2.html">Organic 2</a></li>
                                                            <li><a href="index-3.html">Organic 3</a></li>
                                                            <li><a href="index-4.html">Organic 4</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="menu-dropdown position-static">
                                                        <a href="#">Home Cosmetic <i class="ion-ios-arrow-down"></i></a>
                                                        <ul class="sub-menu sub-menu-2">
                                                            <li><a href="index-5.html">Cosmetic 1</a></li>
                                                            <li><a href="index-6.html">Cosmetic 2</a></li>
                                                            <li><a href="index-7.html">Cosmetic 3</a></li>
                                                            <li><a href="index-8.html">Cosmetic 4</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="menu-dropdown position-static">
                                                        <a href="#">Home Digital <i class="ion-ios-arrow-down"></i></a>
                                                        <ul class="sub-menu sub-menu-2">
                                                            <li><a href="index-9.html">Digital 1</a></li>
                                                            <li><a href="index-10.html">Digital 2</a></li>
                                                            <li><a href="index-11.html">Digital 3</a></li>
                                                            <li><a href="index-12.html">Digital 4</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="menu-dropdown position-static">
                                                        <a href="#">Home Furniture <i class="ion-ios-arrow-down"></i></a>
                                                        <ul class="sub-menu sub-menu-2">
                                                            <li><a href="index-13.html">Furniture 1</a></li>
                                                            <li><a href="index-14.html">Furniture 2</a></li>
                                                            <li><a href="index-15.html">Furniture 3</a></li>
                                                            <li><a href="index-16.html">Furniture 4</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="menu-dropdown position-static">
                                                        <a href="#">Home Medical <i class="ion-ios-arrow-down"></i></a>
                                                        <ul class="sub-menu sub-menu-2">
                                                            <li><a href="index-17.html">Medical 1</a></li>
                                                            <li><a href="index-18.html">Medical 2</a></li>
                                                            <li><a href="index-19.html">Medical 3</a></li>
                                                            <li><a href="index-20.html">Medical 4</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="menu-dropdown">
                                                <a href="#">Shop <i class="ion-ios-arrow-down"></i></a>
                                                <ul class="mega-menu-wrap">
                                                    <li>
                                                        <ul>
                                                            <li class="mega-menu-title"><a href="#">Shop Grid</a></li>
                                                            <li><a href="shop-3-column.html">Shop Grid 3 Column</a></li>
                                                            <li><a href="shop-4-column.html">Shop Grid 4 Column</a></li>
                                                            <li><a href="shop-left-sidebar.html">Shop Grid Left Sidebar</a></li>
                                                            <li><a href="shop-right-sidebar.html">Shop Grid Right Sidebar</a></li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <ul>
                                                            <li class="mega-menu-title"><a href="#">Shop List</a></li>
                                                            <li><a href="shop-list.html">Shop List</a></li>
                                                            <li><a href="shop-list-left-sidebar.html">Shop List Left Sidebar</a></li>
                                                            <li><a href="shop-list-right-sidebar.html">Shop List Right Sidebar</a></li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <ul>
                                                            <li class="mega-menu-title"><a href="#">Shop Single</a></li>
                                                            <li><a href="single-product.html">Shop Single</a></li>
                                                            <li><a href="single-product-variable.html">Shop Variable</a></li>
                                                            <li><a href="single-product-affiliate.html">Shop Affiliate</a></li>
                                                            <li><a href="single-product-group.html">Shop Group</a></li>
                                                            <li><a href="single-product-tabstyle-2.html">Shop Tab 2</a></li>
                                                            <li><a href="single-product-tabstyle-3.html">Shop Tab 3</a></li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <ul>
                                                            <li class="mega-menu-title"><a href="#">Shop Single</a></li>
                                                            <li><a href="single-product-slider.html">Shop Slider</a></li>
                                                            <li><a href="single-product-gallery-left.html">Shop Gallery Left</a></li>
                                                            <li><a href="single-product-gallery-right.html">Shop Gallery Right</a></li>
                                                            <li><a href="single-product-sticky-left.html">Shop Sticky Left</a></li>
                                                            <li><a href="single-product-sticky-right.html">Shop Sticky Right</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="banner-wrapper">
                                                        <a href="single-product.html"><img src="assets/images/banner-image/banner-menu-2.jpg" alt="" /></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="menu-dropdown">
                                                <a href="#">Pages <i class="ion-ios-arrow-down"></i></a>
                                                <ul class="sub-menu">
                                                    <li><a href="about.html">About Page</a></li>
                                                    <li><a href="cart.html">Cart Page</a></li>
                                                    <li><a href="checkout.html">Checkout Page</a></li>
                                                    <li><a href="compare.html">Compare Page</a></li>
                                                    <li><a href="login.html">Login & Regiter Page</a></li>
                                                    <li><a href="my-account.html">Account Page</a></li>
                                                    <li><a href="wishlist.html">Wishlist Page</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-dropdown">
                                                <a href="#">Blog <i class="ion-ios-arrow-down"></i></a>
                                                <ul class="sub-menu">
                                                    <li class="menu-dropdown position-static">
                                                        <a href="#">Blog Grid <i class="ion-ios-arrow-down"></i></a>
                                                        <ul class="sub-menu sub-menu-2">
                                                            <li><a href="blog-grid-left-sidebar.html">Blog Grid Left Sidebar</a></li>
                                                            <li><a href="blog-grid-right-sidebar.html">Blog Grid Right Sidebar</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="menu-dropdown position-static">
                                                        <a href="#">Blog List <i class="ion-ios-arrow-down"></i></a>
                                                        <ul class="sub-menu sub-menu-2">
                                                            <li><a href="blog-list-left-sidebar.html">Blog List Left Sidebar</a></li>
                                                            <li><a href="blog-list-right-sidebar.html">Blog List Right Sidebar</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="menu-dropdown position-static">
                                                        <a href="#">Blog Single <i class="ion-ios-arrow-down"></i></a>
                                                        <ul class="sub-menu sub-menu-2">
                                                            <li><a href="blog-single-left-sidebar.html">Blog Single Left Sidebar</a></li>
                                                            <li><a href="blog-single-right-sidebar.html">Blog Single Right Sidebar</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a href="contact.html">Contact Us</a></li>
                                        </ul>
                                    </div>
                                    <!--Main Navigation End -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Menu Content End -->