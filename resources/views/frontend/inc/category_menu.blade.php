<!-- Beauty Category -->
                <div class="container d-lg-none">
                    <!--=======  category menu  =======-->
                    <div class="hero-side-category">
                        <!-- Category Toggle Wrap -->
                        <div class="category-toggle-wrap">
                            <!-- Category Toggle -->
                            <button class="category-toggle"><i class="fa fa-bars"></i> All Categories</button>
                        </div>

                        <!-- Category Menu -->
                        <nav class="category-menu mean-container">
                        <nav class="mean-nav1">
                                    <ul class="menu-overflow" style="display: block;height: 350px;">
                                   
                                            @foreach (\App\Category::all()->take(5) as $key => $category)
                                            @php
                                                $brands = array();
                                            @endphp   
                                        <li>
                                        <a href="{{ route('products.category', $category->slug) }}">{{ __($category->name) }}</a>  
                                        @if(count($category->subcategories)>0)    
                                        <ul style="display: none;">
                                        @foreach ($category->subcategories as $key=> $subcategory)
                                                @if ($key==4)
                                                @php
                                                    break;
                                                @endphp
                                            @endif
                                                <li>
                                                <a href="{{ route('products.subcategory', $subcategory->slug) }}">{{ $subcategory->name}}</a>
                                                    <ul style="display: none;">
                                                    @foreach ($subcategory->subsubcategories as $subsubcategory)
                                                        <li><a href="{{ route('products.subsubcategory', $subsubcategory->slug) }}">{{ __($subsubcategory->name) }}</a></li>
                                                        @endforeach
                                                    </ul>
                                                <a class="mean-expand" href="#" style="font-size: 18px">+</a></li>
                                                @endforeach  
                                            </ul>
                                        <a class="mean-expand" href="#" style="font-size: 18px">+</a></li>
                                        @endif
                                        @endforeach
                                        <li>
                                                {{-- <a href="{{route('products')}}" id="more-btn"><i class="ion-ios-plus-empty" aria-hidden="true"></i> More Categories</a> --}}
                                                <a href="{{route('categories.all')}}" > More Categories</a>
                                                <a href="{{route('categories.all')}}" class="mean-expand1" style="font-size: 18px">+</a>
                                            </li>
                                    </ul>
                                </nav>
                        </nav>
                    </div>

                    <!--=======  End of category menu =======-->
                </div>
                <!-- Beauty Category -->