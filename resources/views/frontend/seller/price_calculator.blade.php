@extends('frontend.seller.layouts.seller_app')

@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <h2 class="content-header-title float-left mb-0">  {{ translate('Price Calculator')}}</h2>
                <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
             
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">{{ translate('Dashboard')}}</a></li>
                    <li  class="breadcrumb-item active"><a href="{{ route('payments.index') }}">{{ translate('Price Calculator')}}</a></li>
                </ol>
                </div>
              </div>
            </div>
          </div>
          
        </div>
     <div class="col-md-12 content-body">
    <section class="gry-bg profile">
        
         <div class="card mt-3">
 <div class="card-header py-2 px-3 ">
 <div class="heading-6 strong-600">{{ translate('Calculate Delivery Price')}}</div>
 </div>
 <div class="card-body pb-0">
     <div class="row">
         <div class="col-lg-12">
         <form  class="form-default" role="form" action="" method="POST">

             @csrf
             <div class="modal-body">
                 <div class="">
                     <div class="row">
                     <div class="col-md-1">
                             <label>{{ translate('Source Pincode')}}</label>
                         </div>
                         <div class="col-md-2">
                             <input type="text" class="form-control mb-3" placeholder="{{ translate('Source Pincode')}}" name="source_pin" id="source_pin" required>
                         </div>
                         <div class="col-md-1">
                             <label>{{ translate('Destination Pincode')}}</label>
                         </div>
                         <div class="col-md-2">
                             <input type="text" class="form-control mb-3" placeholder="{{ translate('Destination Pincode')}}" name="destination_pin" id="destination_pin" required>
                         </div>
                         <div class="col-md-1">
                             <label>{{ translate('Delivery Type')}}</label>
                         </div>
                         <div class="col-md-2">
                             <select class="form-control mb-3"  name="orderType" id="orderType" required>
                                 <option value="">Select</option>
                                 <option value="2">COD</option>
                                 <option value="1">Online</option>
                             </select>
                         </div>
                         <div class="col-md-1">
                             <label>{{ translate('Invoice Price')}}</label>
                         </div>
                         <div class="col-md-2">
                             <input type="text" class="form-control mb-3" placeholder="{{ translate('Invoice Price')}}" name="invoice_value" id="invoice_value" required>
                         </div>
                         <div class="col-md-1">
                             <label>{{ translate('Length(cm)')}}</label>
                         </div>
                         <div class="col-md-2">
                             <input type="text" class="form-control mb-3" placeholder="{{ translate('Length')}}" name="length" id="length" required>
                         </div>
                        
                         <div class="col-md-1">
                             <label>{{ translate('Height(cm)')}}</label>
                         </div>
                         <div class="col-md-2">
                             <input type="text" class="form-control mb-3" placeholder="{{ translate('Height')}}" name="height" id="height" required>
                         </div>
                         <div class="col-md-1">
                             <label>{{ translate('Width(cm)')}}</label>
                         </div>
                         <div class="col-md-2">
                             <input type="text" class="form-control mb-3" placeholder="{{ translate('Width')}}" name="width" id="width" required>
                         </div>
                         
                         <div class="col-md-1">
                             <label>{{ translate('Weight(kg)')}}</label>
                         </div>
                         
                         <div class="col-md-2">
                             <input type="text" class="form-control mb-3" placeholder="{{ translate('Weight')}}" name="weight" id="weight" required>
                         </div>
                         
                         <div class="col-md-12 text-center">
                         <a class="btn btn-base-1" href="javascript:" id="check_price">{{  translate('Check Price & Availability') }}</a>
                         </div>
                         </div>
                         <div class="row pt-1 service_set" style="display: none;">
                         
                         <div class="col-md-2">
                             <label>{{ translate('Mode/Service') }}</label>
                         </div>
                         <div class="col-md-4">
                         <select class="form-control mb-3 test11" data-placeholder="{{translate('Select your Service')}}" name="mode_type" id="mode_type" required>
                         <option value="">Select Mode</option>
                         @foreach(Config::get('constants.SERVICE_CURIOR') as $key=>$val)

                         <option value="{{$key}}" class="{{$key}}">{{$val}}</option>

                         @endforeach
                                         
                         </select>
                         </div>
                         <div class="col-md-2">
                             <label>{{ translate('Delivery Cost')}}</label>
                         </div>
                         
                         <div class="col-md-4">
                             <input type="text" class="form-control mb-3" placeholder="{{ translate('Delivery Cost')}}" name="dv_cost" id="dv_cost" readonly>
                         </div>
                        
                     </div>
                 </div>
             </div>
             
             </form>
         </div>
        
     </div>
     
        
 </div>
</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>
            </div>
          </div>
          <script type="text/javascript">

$('#check_price').on('click', function(){
        $(".service_set").hide();
        $("#mode_type").val("");
        $("#dv_cost").val("");
        var order_id ="";
        var length = $('#length').val();
        var height = $('#height').val();
        var width = $('#width').val();
        var weight = $('#weight').val();
        var source_pin=$("#source_pin").val();
        var destination_pin=$("#destination_pin").val();
        var orderType=$("#orderType").val();
        var invoice_value=$("#invoice_value").val();
        var mode_type=$("#mode_type").val();
        $.post('{{ route('orders.curior_check_price') }}', {_token:'{{ @csrf_token() }}',source_pin:source_pin,destination_pin:destination_pin,orderType:orderType,invoice_value:invoice_value,order_id:order_id,length:length,height:height,width:width,weight:weight,mode_type:mode_type}, function(data){
            // if(data.calculatedPrice)
          $(".service_set").show();
            var obj = JSON.parse(data);
            $.each( obj.service, function( key, value ) {
            if(value==false){
               
                $("."+key).hide();
            }
            });
            $("#mode_type").on('change', function(){ 
                  
                   var box_value= this.value;
                //    var chekc_cost="pricing."+box_value;
                   $.each(obj.pricing, function( key, value ) {
            if(key==box_value){
                $("#dv_cost").val(value);
                // alert(value);
            }
            });
                //    alert(obj.pricing.surface-10kg);
                    // $("#dv_cost").val(obj.pricing.box_value);
            });
               
        });
    });
</script>
@endsection


