@extends('frontend.seller.layouts.seller_app')

@section('content')
<link rel="stylesheet" href="{{ asset('frontend/css/the-datepicker.css') }}">
		<script src="{{ asset('frontend/js/the-datepicker.js') }}"></script>
<!-- BEGIN: Content-->
<div class="app-content content">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                
                <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                    
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ translate('Home')}}</a></li>
                    {{-- <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">{{ translate('Dashboard')}}</a></li> --}}
                    <li  class="breadcrumb-item active"><a href="javascript:">{{ translate('Payment invoice')}}</a></li>
                </ol>
                </div>
              </div>
            </div>
          </div>
          
        </div>
     <div class="col-md-12 content-body">
    <section class="gry-bg profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                
                <div class="col-lg-12">
                    <div class="main-content">
                        <!-- Page title -->
                        @csrf
                        
                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                        <th>{{ translate('Sl#') }}</th>
                                        <th>{{ translate('Date') }}</th>
                                        <th>{{ translate('Invoice No') }}</th>
                                         <th>{{ translate('For Month') }}</th>
                                         <th>{{ translate('Total Amount') }}</th>
                                         <th>{{ translate('Download') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($seller_data))
                                    @foreach ($seller_data as $key => $sales_inv)
                                   
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            @php 
                                            $timestamp = strtotime($sales_inv['created_at']);
                                            $new_date = date("d/m/Y", $timestamp);
                                            @endphp
                                            <td>{{ $new_date }}</td>
                                            <td>{{ $sales_inv['invoice_no'] }}</td>
                                            <td>{{ $sales_inv['for_month'] }}</td>
                                            <td>{{ $sales_inv['total'] }}</td>
                                            <td> <a onclick="download_invoice('{{route('seller_report_invoice_download_from_seller')}}','{{$sales_inv['id']}}');"><i class="fa fa-download" aria-hidden="true"></i></a></td>
                                           
                                        </tr>
                                
                                    @endforeach
                                    @endif
                                </tbody>
                              <tfoot>
                                    <tr>
                                    <th>{{ translate('Sl#') }}</th>
                                        <th>{{ translate('Date') }}</th>
                                        <th>{{ translate('Invoice No') }}</th>
                                         <th>{{ translate('For Month') }}</th>
                                         <th>{{ translate('Total Amount') }}</th>
                                         <th>{{ translate('Download') }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                           
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>
            </div>
          </div>
@endsection

@section('script')
<script>
 function download_invoice(url,inv_id){
                var from_date=$('#from_date').val();
                var token = $("input[name='_token']").val();
        // var fd = new FormData();
        // fd.append('_token', token);
        // fd.append('from_date', from_date);
        // fd.append('to_date', to_date);
        var data={inv_id:inv_id,_token:token};
        redirectPost(url,data); 

               
            }

            var redirectPost = function (url, data = null, method = 'post') {
                var form = document.createElement('form');
                form.method = method;
                form.action = url;
                for (var name in data) {
                    var input = document.createElement('input');
                    input.type = 'hidden';
                    input.name = name;
                    input.value = data[name];
                    form.appendChild(input);
                }
                $('body').append(form);
                form.submit();
            };
</script>
@endsection
