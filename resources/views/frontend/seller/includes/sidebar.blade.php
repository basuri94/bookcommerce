<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="javascript:">
                    <!-- <div class="brand-logo"></div> -->
                    <img src="{{asset('assets/images/logo/logo-cosmatics.png')}}" style="width: 100%;">
                    <!-- <h2 class="brand-text mb-0">Local2Vocal</h2> -->
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i
                        class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i
                        class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary"
                        data-ticon="icon-disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            @php
            $verifystatus = App\Seller::where('user_id', Auth::user()->id)->value('verification_status');

            @endphp
            @if($verifystatus==1)
            <li class="nav-item {{ areActiveRoutesHome(['dashboard'])}}">
                <a href="{{ route('dashboard') }}" class="{{ areActiveRoutesHome(['dashboard'])}}">
                    <i class="la la-dashboard"></i>
                    <span class="category-name">
                        {{ translate('Dashboard')}}
                    </span>
                </a>
            </li>
            @php
            $delivery_viewed = App\Order::where('user_id', Auth::user()->id)->where('delivery_viewed',
            0)->get()->count();
            $payment_status_viewed = App\Order::where('user_id', Auth::user()->id)->where('payment_status_viewed',
            0)->get()->count();
            $refund_request_addon = \App\Addon::where('unique_identifier', 'refund_request')->first();
            $club_point_addon = \App\Addon::where('unique_identifier', 'club_point')->first();
            @endphp
            <!-- <li class="nav-item">
                    <a href="{{ route('purchase_history.index') }}" class="{{ areActiveRoutesHome(['purchase_history.index'])}}">
                        <i class="la la-file-text"></i>
                        <span class="category-name">
                            {{ translate('Purchase History')}} @if($delivery_viewed > 0 || $payment_status_viewed > 0)<span class="ml-2" style="color:green"><strong>({{  translate(' New Notifications') }})</strong></span>@endif
                        </span>
                    </a>
                </li> -->
            <!-- <li class="nav-item">
                    <a href="{{ route('digital_purchase_history.index') }}" class="{{ areActiveRoutesHome(['digital_purchase_history.index'])}}">
                        <i class="la la-download"></i>
                        <span class="category-name">
                            {{ translate('Downloads')}}
                        </span>
                    </a>
                </li> -->
            <!-- <li class="nav-item">
                    <a href="{{ route('wishlists.index') }}" class="{{ areActiveRoutesHome(['wishlists.index'])}}">
                        <i class="la la-heart-o"></i>
                        <span class="category-name">
                            {{ translate('Wishlist')}}
                        </span>
                    </a>
                </li> -->
            <li
                class="nav-item {{ areActiveRoutesHome(['seller.products', 'seller.products.upload', 'seller.products.edit'])}}">
                <a href="{{ route('seller.products') }}"
                    class="{{ areActiveRoutesHome(['seller.products', 'seller.products.upload', 'seller.products.edit'])}}">
                    <i class="la la-diamond"></i>
                    <span class="category-name">
                        {{ translate('Book Details')}}
                    </span>
                </a>
            </li>
            <li
                class="nav-item {{ areActiveRoutesHome(['seller.digitalproducts', 'seller.digitalproducts.upload', 'seller.digitalproducts.edit'])}}">
                <a href="{{ route('seller.digitalproducts') }}"
                    class="{{ areActiveRoutesHome(['seller.digitalproducts', 'seller.digitalproducts.upload', 'seller.digitalproducts.edit'])}}">
                    <i class="la la-globe"></i>
                    <span class="category-name">
                        {{ translate('Digital Products')}}
                    </span>
                </a>
            </li> {{--
            @if(\App\BusinessSetting::where('type', 'classified_product')->first()->value == 1)
            <!-- <li class="nav-item">
                    <a href="{{ route('customer_products.index') }}" class="{{ areActiveRoutesHome(['customer_products.index', 'customer_products.create', 'customer_products.edit'])}}">
                        <i class="la la-diamond"></i>
                        <span class="category-name">
                            {{ translate('Classified Products')}}
                        </span>
                    </a>
                </li> -->
            @endif --}}
            {{-- @if (\App\Addon::where('unique_identifier', 'pos_system')->first() != null &&
            \App\Addon::where('unique_identifier', 'pos_system')->first()->activated)
            @if (\App\BusinessSetting::where('type', 'pos_activation_for_seller')->first() != null &&
            \App\BusinessSetting::where('type', 'pos_activation_for_seller')->first()->value != 0)
            <li class="nav-item {{ areActiveRoutesHome(['poin-of-sales.seller_index'])}}">
                <a href="{{route('poin-of-sales.seller_index')}}"
                    class="{{ areActiveRoutesHome(['poin-of-sales.seller_index'])}}">
                    <i class="la la-fax"></i>
                    <span class="category-name">
                        {{ translate('POS Manager')}}
                    </span>
                </a>
            </li>
            @endif
            @endif --}}
            <li class="nav-item {{ areActiveRoutesHome(['product_bulk_upload.index'])}}">
                <a href="{{route('product_bulk_upload.index')}}"
                    class="{{ areActiveRoutesHome(['product_bulk_upload.index'])}}">
                    <i class="la la-upload"></i>
                    <span class="category-name">
                        {{ translate('Product Bulk Upload')}}
                    </span>
                </a>
            </li>
            @php
            $orders = DB::table('orders')
            ->orderBy('code', 'desc')
            ->join('order_details', 'orders.id', '=', 'order_details.order_id')
            ->where('order_details.seller_id', Auth::user()->id)
            ->where('orders.viewed', 0)
            ->select('orders.id')
            ->distinct()
            ->count();
            @endphp
            <li class="nav-item {{ areActiveRoutesHome(['orders.index'])}}">
                <a href="{{ route('orders.index') }}" class="{{ areActiveRoutesHome(['orders.index'])}}">
                    <i class="la la-file-text"></i>
                    <span class="category-name">
                        {{ translate('Orders')}} @if($orders > 0)<span class="ml-2"
                            style="color:green"><strong>({{ $orders }}
                                {{  translate('New') }})</strong></span></span>@endif
                    </span>
                </a>
            </li>
            {{--
            @if ($refund_request_addon != null && $refund_request_addon->activated == 1)
            <li class="nav-item {{ areActiveRoutesHome(['vendor_refund_request'])}}">
                <a href="{{ route('vendor_refund_request') }}"
                    class="{{ areActiveRoutesHome(['vendor_refund_request'])}}">
                    <i class="la la-file-text"></i>
                    <span class="category-name">
                        {{ translate('Recieved Refund Request')}}
                    </span>
                </a>
            </li>

            <li class="nav-item {{ areActiveRoutesHome(['customer_refund_request'])}}">
                <a href="{{ route('customer_refund_request') }}"
                    class="{{ areActiveRoutesHome(['customer_refund_request'])}}">
                    <i class="la la-file-text"></i>
                    <span class="category-name">
                        {{ translate('Sent Refund Request')}}
                    </span>
                </a>
            </li>
            @endif
            --}}
            @php
            $review_count = DB::table('reviews')
            ->orderBy('code', 'desc')
            ->join('products', 'products.id', '=', 'reviews.product_id')
            ->where('products.user_id', Auth::user()->id)
            ->where('reviews.viewed', 0)
            ->select('reviews.id')
            ->distinct()
            ->count();
            @endphp
            <li class="nav-item {{ areActiveRoutesHome(['reviews.seller'])}}">
                <a href="{{ route('reviews.seller') }}" class="{{ areActiveRoutesHome(['reviews.seller'])}}">
                    <i class="la la-star-o"></i>
                    <span class="category-name">
                        {{ translate('Product Reviews')}}@if($review_count > 0)<span class="ml-2"
                            style="color:green"><strong>({{ $review_count }}
                                {{  translate('New') }})</strong></span>@endif
                    </span>
                </a>
            </li>
            @if (\App\BusinessSetting::where('type', 'conversation_system')->first()->value == 1)
            @php
            $conversation_sent = \App\Conversation::where('sender_id', Auth::user()->id)->where('sender_viewed',
            0)->get();
            $conversation_recieved = \App\Conversation::where('receiver_id', Auth::user()->id)->where('receiver_viewed',
            0)->get();
            @endphp
            <li class="nav-item {{ areActiveRoutesHome(['conversations.index', 'conversations.show'])}}">
                <a href="{{ route('conversations.index') }}"
                    class="{{ areActiveRoutesHome(['conversations.index', 'conversations.show'])}}">
                    <i class="la la-comment"></i>
                    <span class="category-name">
                        {{ translate('Conversations')}}
                        @if (count($conversation_sent)+count($conversation_recieved) > 0)
                        <span class="ml-2"
                            style="color:green"><strong>({{ count($conversation_sent)+count($conversation_recieved) }})</strong></span>
                        @endif
                    </span>
                </a>
            </li>
            @endif 
            <li class="nav-item {{ areActiveRoutesHome(['shops.index'])}}">
                <a href="{{ route('shops.index') }}" class="{{ areActiveRoutesHome(['shops.index'])}}">
                    <i class="la la-cog"></i>
                    <span class="category-name">
                        {{ translate('Shop Setting')}}
                    </span>
                </a>
            </li>
         <li class="nav-item {{ areActiveRoutesHome(['payments.index'])}}">
                <a href="{{ route('payments.index') }}" class="{{ areActiveRoutesHome(['payments.index'])}}">
                    <i class="la la-cc-mastercard"></i>
                    <span class="category-name">
                        {{ translate('Payment History')}}
                    </span>
                </a>
            </li> 
            <li class="nav-item {{ areActiveRoutesHome(['profile'])}}">
                <a href="{{ route('profile') }}" class="{{ areActiveRoutesHome(['profile'])}}">
                    <i class="la la-user"></i>
                    <span class="category-name">
                        {{ translate('Manage Profile')}}
                    </span>
                </a>
            </li>
            {{-- <li class="nav-item {{ areActiveRoutesHome(['withdraw_requests.index'])}}">
                <a href="{{ route('withdraw_requests.index') }}"
                    class="{{ areActiveRoutesHome(['withdraw_requests.index'])}}">
                    <i class="la la-money"></i>
                    <span class="category-name">
                        {{ translate('Money Withdraw')}}
                    </span>
                </a>
            </li> --}}
            @if (\App\BusinessSetting::where('type', 'wallet_system')->first()->value == 1)
            <!-- <li class="nav-item">
                        <a href="{{ route('wallet.index') }}" class="{{ areActiveRoutesHome(['wallet.index'])}}">
                            <i class="la la-dollar"></i>
                            <span class="category-name">
                                {{ translate('My Wallet')}}
                            </span>
                        </a>
                    </li> -->
            @endif
            {{-- @if (\App\Addon::where('unique_identifier', 'affiliate_system')->first() != null &&
            \App\Addon::where('unique_identifier', 'affiliate_system')->first()->activated &&
            Auth::user()->affiliate_user != null && Auth::user()->affiliate_user->status)
            <li class="nav-item {{ areActiveRoutesHome(['affiliate.user.index', 'affiliate.payment_settings'])}}">
                <a href="{{ route('affiliate.user.index') }}"
                    class="{{ areActiveRoutesHome(['affiliate.user.index', 'affiliate.payment_settings'])}}">
                    <i class="la la-dollar"></i>
                    <span class="category-name">
                        {{ translate('Affiliate System')}}
                    </span>
                </a>
            </li>
            @endif
            @if ($club_point_addon != null && $club_point_addon->activated == 1)
            <li class="nav-item {{ areActiveRoutesHome(['earnng_point_for_user'])}}">
                <a href="{{ route('earnng_point_for_user') }}"
                    class="{{ areActiveRoutesHome(['earnng_point_for_user'])}}">
                    <i class="la la-dollar"></i>
                    <span class="category-name">
                        {{ translate('Earning Points')}}
                    </span>
                </a>
            </li>
            @endif --}}
            @php
            $support_ticket = DB::table('tickets')
            ->where('client_viewed', 0)
            ->where('user_id', Auth::user()->id)
            ->count();
            @endphp
            <li class="nav-item {{ areActiveRoutesHome(['support_ticket.index', 'support_ticket.show'])}}">
                <a href="{{ route('support_ticket.index') }}"
                    class="{{ areActiveRoutesHome(['support_ticket.index', 'support_ticket.show'])}}">
                    <i class="la la-support"></i>
                    <span class="category-name">
                        {{ translate('Support Ticket')}} @if($support_ticket > 0)<span class="ml-2"
                            style="color:green"><strong>({{ $support_ticket }}
                                {{  translate('New') }})</strong></span></span>@endif
                    </span>
                </a>
            </li>
            <li class="nav-item {{ areActiveRoutesHome(['sellerefundlist'])}}">
                <a href="{{ route('sellerefundlist') }}" class="{{ areActiveRoutesHome(['sellerefundlist'])}}">
                    <i class="fa fa-clock-o"></i>
                    <span class="category-name">
                        {{ translate('Return/Refund Request')}}
                    </span>
                </a>
            </li> 
            <li class="nav-item {{ areActiveRoutesHome(['subcategories_seller_index','price_calculator_view'])}}">
                <a href="{{route('subcategories_seller_index')}}" class="{{ areActiveRoutesHome(['subcategories_seller_index'])}}">
                    <i class="fa fa-inr"></i>
                    <span class="category-name">
                        {{translate('Commission')}}
                    </span>
                    <i class="fa fa-sort-desc" style="float: right;"></i>
                    </a>
                    
                    <ul class="menu-content">
                    <li class="nav-item {{ areActiveRoutesHome(['subcategories_seller_index'])}}">
                        <a href="{{route('subcategories_seller_index')}}" class="{{ areActiveRoutesHome(['subcategories_seller_index'])}}">
                            <i class="fa fa-list"></i>
                            <span class="category-name">
                                {{translate('Commission Chart')}}
                            </span>
                        </a>
                    </li>
                    <li class="nav-item {{ areActiveRoutesHome(['price_calculator_view'])}}">
                        <a href="{{ route('price_calculator_view') }}" class="{{ areActiveRoutesHome(['price_calculator_view'])}}">
                            <i class="fa fa-inr"></i>
                            <span class="category-name">
                                {{translate('Price Calculator')}}
                            </span>
                        </a>
                    </li>
                    </ul>
                
            </li>
            <li class="nav-item {{ areActiveRoutesHome(['sales_vendor_commision'])}}">
                <a href="{{ route('sales_vendor_commision') }}" class="{{ areActiveRoutesHome(['sales_vendor_commision'])}}">
                    <i class="fa fa-list"></i>
                    <span class="category-name">
                        {{ translate('Commision Report')}}
                    </span>
                </a>
            </li> 
            <li class="nav-item {{ areActiveRoutesHome(['seller_invoice_list'])}}">
                <a href="{{ route('seller_invoice_list') }}" class="{{ areActiveRoutesHome(['seller_invoice_list'])}}">
                    <i class="fa fa-info"></i>
                    <span class="category-name">
                        {{ translate('Payment Invoice')}}
                    </span>
                </a>
            </li> 
            @else
            <li class="nav-item {{ areActiveRoutesHome(['shops.index'])}}">
                <a href="{{ route('shops.index') }}" class="{{ areActiveRoutesHome(['shops.index'])}}">
                    <i class="la la-cog"></i>
                    <span class="category-name">
                        {{ translate('Shop Setting')}}
                    </span>
                </a>
            </li>
            <li class="nav-item {{ areActiveRoutesHome(['approvalStatus'])}}">
                <a href="{{ route('approvalStatus') }}" class="{{ areActiveRoutesHome(['approvalStatus'])}}">
                    <i class="fa fa-clock-o"></i>
                    <span class="category-name">
                        {{ translate('Approval Status')}}
                    </span>
                </a>
            </li>
            @endif
           
           
        </ul>

    </div>
</div>
<!-- END: Main Menu-->