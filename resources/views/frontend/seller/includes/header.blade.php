<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
  <div class="navbar-wrapper">
    <div class="navbar-container content">
      <div class="navbar-collapse" id="navbar-mobile">
        <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
          <ul class="nav navbar-nav">
            <li class="nav-item mobile-menu d-xl-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ficon feather icon-menu"></i></a></li>
          </ul>
          <ul class="nav navbar-nav bookmark-icons">
            <!-- li.nav-item.mobile-menu.d-xl-none.mr-auto-->
            <!--   a.nav-link.nav-menu-main.menu-toggle.hidden-xs(href='#')-->
            <!--     i.ficon.feather.icon-menu-->
            </ul>
          <ul class="nav navbar-nav">
            <li class="nav-item d-none d-lg-block">
              
              <!-- select.bookmark-select-->
              <!--   option Chat-->
              <!--   option email-->
              <!--   option todo-->
              <!--   option Calendar-->
            </li>
          </ul>
        </div>
        <ul class="nav navbar-nav float-right">
        
          <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i class="ficon feather icon-maximize"></i></a></li>
          
          <!-- <li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="ficon feather icon-bell"></i><span class="badge badge-pill badge-primary badge-up">5</span></a>
            <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
              <li class="dropdown-menu-header">
              
              <li class="dropdown-menu-footer"><a class="dropdown-item p-1 text-center" href="javascript:void(0)">Read all notifications</a></li>
            </ul>
          </li> -->
          <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
              <div class="user-nav d-sm-flex d-none"><span class="user-name text-bold-600">
              {{ucwords(Auth::user()->name)}}</span></div>
                <span><img class="round" 
                  @if (empty(Auth::user()->avatar_original))
                  src="{{ asset('frontend/images/user.png')}}"
                  @else
                  src="{{ asset(Auth::user()->avatar_original)}}"
                  @endif
                  alt="avatar" height="40" width="40">
                </span></a>
                <!-- <div class="dropdown-content">
  <p>Hello World!</p>
  </div> -->
            <div class="dropdown-menu dropdown-menu-right dropdown-content">
              <div class="dropdown-divider"></div><a class="dropdown-item" href="/logout"><i class="feather icon-power"></i> Logout</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</nav>

<!-- END: Header-->