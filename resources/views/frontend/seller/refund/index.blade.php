@extends('frontend.seller.layouts.seller_app')

@section('content')

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- Dashboard Ecommerce Starts -->
            <!-- Zero configuration table -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">{{translate('Return/Refund Request')}}</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    @csrf
                                    {{-- <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="retun-tab-fill" data-toggle="tab"
                                                href="#retun-fill" role="tab" aria-controls="retun-fill"
                                                aria-selected="true">{{translate('Return Request')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="refund-tab-fill" data-toggle="tab"
                                                href="#refund-fill" role="tab" aria-controls="refund-fill"
                                                aria-selected="false">{{translate('Return/Refund Request')}}</a>
                                        </li>

                                    </ul> --}}
                                    <!-- Tab panes -->
                                    <div class="table-responsive">
                                        <table class="table zero-configuration">
                                            <thead>
                                                <tr>
                                                    <th>{{ translate('#')}}</th>
                                                    <th>{{ translate('Date')}}</th>
                                                    <th>{{ translate('Order Id')}}</th>
                                                    <th>{{ translate('Product')}}</th>
                                                    <th>{{ translate('Amount')}}</th>
<th>{{ translate('Request Type')}}</th>
                                                    <th>{{ translate('Status')}}</th>
                                                    <th width="10%">{{translate('Options')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($datarefund as $key => $datarefund_list)

                                                <tr>
                                                    <td>{{ ($key+1) + ($datarefund->currentPage() - 1)*$datarefund->perPage() }}
                                                    </td>
                                                    <td>
                                                        {{ date('d-m-Y', strtotime($datarefund_list->created_at))}}
                                                    </td>
                                                    <td>{{$datarefund_list->code}}</td>
                                                    <td>{{$datarefund_list->name}}</td>
                                                    <td>{{$datarefund_list->amount}}</td>
                                                     <td>{{$datarefund_list->type}}</td>
                                                    <td>

                                                       
                                                        <span style="color:green">
                                                            {{ $datarefund_list->status }}</span>
                                                      

                                                    </td>
                                                    <td>
                                                        <div class="dropdown">
                                                            <button class="btn" type="button"
                                                                id="dropdownMenuButton-{{ $key }}"
                                                                data-toggle="dropdown" aria-haspopup="true"
                                                                aria-expanded="false">
                                                                <i class="fa fa-ellipsis-v"></i>
                                                            </button>

                                                            <div class="dropdown-menu dropdown-menu-right"
                                                                aria-labelledby="dropdownMenuButton-{{ $key }}">
                                                                <button
                                                                    onclick="sellerrefundview({{$datarefund_list->id}});"
                                                                    class="dropdown-item">{{ translate('View')}}</button>


                                                            </div>
                                                        </div>
                                                    </td>

                                                </tr>

                                                @endforeach
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>{{ translate('#')}}</th>
                                                    <th>{{ translate('Date')}}</th>
                                                    <th>{{ translate('Order Id')}}</th>
                                                    <th>{{ translate('Product')}}</th>
                                                    <th>{{ translate('Amount')}}</th>
<th>{{ translate('Request Type')}}</th>
                                                    <th>{{ translate('Status')}}</th>
                                                    <th width="10%">{{translate('Options')}}</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <div class="clearfix">
                                            <div class="pull-right">
                                                {{ $datarefund->appends(request()->input())->links() }}
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="tab-content pt-1">
                                        <div class="tab-pane active" id="retun-fill" role="tabpanel"
                                            aria-labelledby="retun-tab-fill">

                                            <div class="table-responsive">
                                                <table class="table zero-configuration">
                                                    <thead>
                                                        <tr>
                                                            <th>{{ translate('#')}}</th>
                                                            <th>{{ translate('Date')}}</th>
                                                            <th>{{ translate('Order Id')}}</th>
                                                            <th>{{ translate('Product')}}</th>
                                                            <th>{{ translate('Amount')}}</th>
                                                            <th>{{ translate('Type')}}</th>
                                                            <th>{{ translate('Status')}}</th>
                                                            <th width="10%">{{translate('Options')}}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($datareturn as $key => $datareturn_list)

                                                        <tr>
                                                            <td>{{ ($key+1) + ($datareturn->currentPage() - 1)*$datareturn->perPage() }}
                                                            </td>
                                                            <td>
                                                                {{ date('d-m-Y', strtotime($datareturn_list->created_at))}}
                                                            </td>
                                                            <td>{{$datareturn_list->code}}</td>
                                                            <td>{{$datareturn_list->name}}</td>
                                                            <td>{{$datareturn_list->amount}}</td>
                                                            <td>{{$datareturn_list->type_select}}</td>
                                                            <td>

                                                                @if ($datareturn_list->status === "Pending")
                                                                <span style="color:red">{{ translate('Pending')}}</span>
                                                                @elseif($datareturn_list->status === "Approved")
                                                                <span style="color:green">
                                                                    {{ translate('Approved')}}</span>
                                                                @else
                                                                <span style="color:green">
                                                                    {{ translate('Rejected')}}</span>
                                                                @endif

                                                            </td>
                                                            <td>
                                                                <div class="dropdown">
                                                                    <button class="btn" type="button"
                                                                        id="dropdownMenuButton-{{ $key }}"
                                                                        data-toggle="dropdown" aria-haspopup="true"
                                                                        aria-expanded="false">
                                                                        <i class="fa fa-ellipsis-v"></i>
                                                                    </button>

                                                                    <div class="dropdown-menu dropdown-menu-right"
                                                                        aria-labelledby="dropdownMenuButton-{{ $key }}">
                                                                        <button
                                                                            onclick="sellerrefundview({{$datareturn_list->id}},'{{$datareturn_list->type}}');"
                                                                            class="dropdown-item">{{ translate('View')}}</button>


                                                                    </div>
                                                                </div>
                                                            </td>

                                                        </tr>

                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>{{ translate('#')}}</th>
                                                            <th>{{ translate('Date')}}</th>
                                                            <th>{{ translate('Order Id')}}</th>
                                                            <th>{{ translate('Product')}}</th>
                                                            <th>{{ translate('Amount')}}</th>
                                                            <th>{{ translate('Type')}}</th>
                                                            <th>{{ translate('Status')}}</th>
                                                            <th width="10%">{{translate('Options')}}</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                                <div class="clearfix">
                                                    <div class="pull-right">
                                                        {{ $datarefund->appends(request()->input())->links() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="tab-pane" id="refund-fill" role="tabpanel" aria-labelledby="refund-tab-fill">
                    
                                           
                                            
                                        </div>
                                    </div> --}}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<!-- END: Content-->


<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="refundviewmodal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Refund View</h5>
                {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"> --}}
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="refund_body">
                ...
            </div>
            <div class="modal-footer" id="footer_id">

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="curiourviewmodal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Refund View</h5>
                {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"> --}}
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body  row" id="curiour_body">
                <input type="hidden" id="tracking_id" name="tracking_id" value="">
                <div class="not_replace row">
                <div class="col-md-2">
                                    <label>{{ translate('Mode/Service') }}</label>
                                </div>
                                <div class="col-md-4">
                                <select class="form-control mb-3 test11" data-placeholder="{{translate('Select your Service')}}" name="mode_type" id="mode_type" required>
                                <option value="">Select Mode</option>
                                @foreach(Config::get('constants.SERVICE_CURIOR') as $key=>$val)

                                <option value="{{$key}}" class="{{$key}}">{{$val}}</option>

                                @endforeach
                                                
                                </select>
                                </div>
                                <div class="col-md-2">
                                    <label>{{ translate('Delivery Cost')}}</label>
                                </div>
                                
                                <div class="col-md-4">
                                    <input type="text" class="form-control mb-3" placeholder="{{ translate('Delivery Cost')}}" name="dv_cost" id="dv_cost" readonly>
                                </div>
                                <div class="col-md-12 text-center refund_curior" style="display: none;">
                                <a class="btn btn-base-1" href="javascript:" onclick="sellerrefundreturn_confirm()" id="confirm_order">{{  translate('Confirm Order') }}</a>
                                    
                                </div>
                                <div class="col-md-12 text-center replace_curior" style="display: none;">
                                <a class="btn btn-base-1" href="javascript:" onclick="sellerrefundreturn_replace_confirm()" id="confirm_order">{{  translate('Confirm Order') }}</a>
                                    
                                </div>
                </div>
            
                                <div class="row replace_orders" style="display: none;margin-left:2%;">
                                <div class="col-md-4">
    <a class="btn btn-base-1 cancel_order" style="width:100%" >{{  translate('Cancel Order') }}</a>
    </div>
    <div class="col-md-4">
    <a class="btn btn-base-1 generate_silp" style="width:100%" >{{  translate('Generate Slip') }}</a>
    </div>
    <div class="col-md-4">
    <a class="btn btn-base-1 generate_manifest" style="width:100%" >{{  translate('Generate Manifest') }}</a>
    </div>
    </div>
    </div>
            </div>
            <div class="modal-footer" id="footer_id">

            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script type="text/javascript">
    function sellerrefundview(refundid){
            $.ajax({
                    type: 'post',
                    url: '{{route("sellerrefundview")}}',
                    data: {'refundid': refundid, '_token': $('input[name="_token"]').val()},
                    dataType: 'json',
                    success: function (response) {
                        $("#refundviewmodal").modal('show');
                        $('#refund_body').html(response.html)
                        $('#footer_id').html(response.footer)
                        

                    },
                 
                });
        }
        function sellerrefundreturn(){
            var refundid=$('#refundid').val();
            var orderdetails_id=$('#orderdetails_id').val();
            Swal.fire({
            title: "Are you sure you want to return Via courier?",
            type: "",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        }).then((result) => {
          if (result.value) {
            $.ajax({
                    type: 'post',
                    url: '{{route("sellerrefundreturn")}}',
                    data: {'refundid': refundid,'orderdetails_id':orderdetails_id, '_token': $('input[name="_token"]').val()},
                    dataType: 'json',
                    success: function (response) {
                        $("#refundviewmodal").modal('hide');
                          $("#curiourviewmodal").modal('show');
                          $(".refund_curior").show();
                          if(response.status==2){
                            var obj = JSON.parse(response.response_data);
            $.each( obj.service, function( key, value ) {
            if(value==false){
               
                $("."+key).hide();
            }
            });
            $("#mode_type").on('change', function(){ 
                  
                   var box_value= this.value;
                //    var chekc_cost="pricing."+box_value;
                   $.each(obj.pricing, function( key, value ) {
            if(key==box_value){
                $("#dv_cost").val(value);
                // alert(value);
            }
            });
                //    alert(obj.pricing.surface-10kg);
                    // $("#dv_cost").val(obj.pricing.box_value);
            });
                          }
                    //     Swal.fire({
                    //    // position: 'top-end',
                    //     icon: 'success',
                    //     title: response.msg,
                    //     showConfirmButton: true,
                        
                    //     }).then(function (e) {
                    //     console.log(e)
                    //     if (e.value === true) {
                    //     location.reload();

                    //     }

                    // },)
                     


                    },
                 
                });
          }
         
        });
           
        }
        function sellerrefundreturn_replace(){
            var refundid=$('#refundid').val();
            var orderdetails_id=$('#orderdetails_id').val();
            Swal.fire({
            title: "Are you sure you want to replace product Via courier?",
            type: "",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        }).then((result) => {
          if (result.value) {
            $.ajax({
                    type: 'post',
                    url: '{{route("sellerrefundreturn_replace")}}',
                    data: {'type':'refund','refundid': refundid,'orderdetails_id':orderdetails_id, '_token': $('input[name="_token"]').val()},
                    dataType: 'json',
                    success: function (response) {
                        $("#refundviewmodal").modal('hide');
                          $("#curiourviewmodal").modal('show');
                          $(".replace_curior").show();
                          if(response.status==2){
                            var obj = JSON.parse(response.response_data);
            $.each( obj.service, function( key, value ) {
            if(value==false){
               
                $("."+key).hide();
            }
            });
            $("#mode_type").on('change', function(){ 
                  
                   var box_value= this.value;
                //    var chekc_cost="pricing."+box_value;
                   $.each(obj.pricing, function( key, value ) {
            if(key==box_value){
                $("#dv_cost").val(value);
                // alert(value);
            }
            });
                //    alert(obj.pricing.surface-10kg);
                    // $("#dv_cost").val(obj.pricing.box_value);
            });
                          }else{
                            $("#tracking_id").val(response.tracking_id);
                            $(".not_replace").hide();
                        $(".replace_orders").show();
                          }
            
                    //     Swal.fire({
                    //    // position: 'top-end',
                    //     icon: 'success',
                    //     title: response.msg,
                    //     showConfirmButton: true,
                        
                    //     }).then(function (e) {
                    //     console.log(e)
                    //     if (e.value === true) {
                    //     location.reload();

                    //     }

                    // },)
                     


                    },
                 
                });
          }
         
        });   
        }
        function sellerrefundreturn_confirm(){
            var refundid=$('#refundid').val();
            var orderdetails_id=$('#orderdetails_id').val();
            var mode_type=$('#mode_type').val();
            var dv_cost=$('#dv_cost').val();
            Swal.fire({
            title: "Are you sure you want to return via courier?",
            type: "",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        }).then((result) => {
          if (result.value) {
            $.ajax({
                    type: 'post',
                    url: '{{route("sellerrefundreturn_confirm")}}',
                    data: {'type':'refund','refundid': refundid,'orderdetails_id':orderdetails_id,'mode_type':mode_type,'dv_cost':dv_cost, '_token': $('input[name="_token"]').val()},
                    dataType: 'json',
                    success: function (response) {
                         
                        Swal.fire({
                       // position: 'top-end',
                        icon: 'success',
                        title: response.msg,
                        showConfirmButton: true,
                        
                        }).then(function (e) {
                        console.log(e)
                        if (e.value === true) {
                        location.reload();

                        }

                    },)
                     


                    },
                 
                });
          }
         
        });
           
        }
        function sellerrefundreturn_replace_confirm(){
            var refundid=$('#refundid').val();
            var orderdetails_id=$('#orderdetails_id').val();
            var mode_type=$('#mode_type').val();
            var dv_cost=$('#dv_cost').val();
            Swal.fire({
            title: "Are you sure you want to return via courier?",
            type: "",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        }).then((result) => {
          if (result.value) {
            $.ajax({
                    type: 'post',
                    url: '{{route("sellerrefundreturn_confirm")}}',
                    data: {'type':'replacement','refundid': refundid,'orderdetails_id':orderdetails_id,'mode_type':mode_type,'dv_cost':dv_cost, '_token': $('input[name="_token"]').val()},
                    dataType: 'json',
                    success: function (response) {
                         $("#tracking_id").val(response.tracking_id);
                        Swal.fire({
                       // position: 'top-end',
                        icon: 'success',
                        title: response.msg,
                        showConfirmButton: true,
                        
                        }).then(function (e) {
                        console.log(e)
                        if (e.value === true) {
                            $(".not_replace").hide();
                        $(".replace_orders").show();
                        }

                    },)
                     


                    },
                 
                });
          }
         
        });
           
        }
        $('.generate_silp').on('click', function(){
        var order_id =  $("#tracking_id").val();
        $.post('{{ route('replace_generate_silp') }}', {_token:'{{ @csrf_token() }}',order_id:order_id}, function(data){
// alert(data['filepath']);
            window.open(data, '_blank');
            //   $('#order_details').modal('hide');
            //console.log(data);
            // showFrontendAlert('success', 'Order Created Successfully');
            // location.reload().setTimeOut(500);
        });
    });
    $('.generate_manifest').on('click', function(){
        var order_id =  $("#tracking_id").val();
        $.post('{{ route('replace_generate_manifest') }}', {_token:'{{ @csrf_token() }}',order_id:order_id}, function(data){
            //   $('#order_details').modal('hide');
            //console.log(data);
            // showFrontendAlert('success', 'Order Created Successfully');
            // location.reload().setTimeOut(500);
            window.open(data, '_blank')
        });
    });
    $('.cancel_order').on('click', function(){
        var order_id =  $("#tracking_id").val();
        $.post('{{ route('sellerrefundreturn_cancel_order') }}', {_token:'{{ @csrf_token() }}',order_id:order_id}, function(data){
            //   $('#order_details').modal('hide');
            //console.log(data);
            // showFrontendAlert('success', 'Order Created Successfully');
            // location.reload().setTimeOut(500);
            showFrontendAlert('success', 'Order Canceled Successfully');
            location.reload().setTimeOut(500);
        });
    });
        function sellerrefundreturn_cancel($tracking_id){
            var order_id = $tracking_id;
        Swal.fire({
            title: "Are you sure?",
            text: "Once cancled, you will not be able Process further",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        }).then((result) => {
          if (result.value) {
            $.post('{{ route('sellerrefundreturn_cancel_order') }}', {_token:'{{ @csrf_token() }}',order_id:order_id}, function(data){
            //   $('#order_details').modal('hide');
            //console.log(data);
            showFrontendAlert('success', 'Order Canceled Successfully');
            location.reload().setTimeOut(500);
        });
          }
         
        });
       
        }
        function sellerrefundapprove(){
            $(this).attr('disabled',true);
            var refundid=$('#refundid').val();
            
            Swal.fire({
            title: "Are you sure you want to approve refund?",
            type: "",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        }).then((result) => {
          if (result.value) {
            $.ajax({
                    type: 'post',
                    url: '{{route("sellerrefundapprove")}}',
                    data: {'refundid': refundid, '_token': $('input[name="_token"]').val()},
                    dataType: 'json',
                    success: function (response) {
                         
                        Swal.fire({
                       // position: 'top-end',
                        icon: 'success',
                        title: response.msg,
                        showConfirmButton: true,
                        
                        }).then(function (e) {
                        console.log(e)
                        if (e.value === true) {
                        location.reload();

                        }

                    },)
                     


                    },
                 
                });
          }
         
        });
           
        }

        function sellerrefundreject(){
            var refundid=$('#refundid').val();
            
            Swal.fire({
            title: "Are you sure you want to Reject refund?",
            type: "",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        }).then((result) => {
          if (result.value) {
            $.ajax({
                    type: 'post',
                    url: '{{route("sellerrefundreject")}}',
                    data: {'refundid': refundid, '_token': $('input[name="_token"]').val()},
                    dataType: 'json',
                    success: function (response) {
                         
                        Swal.fire({
                       // position: 'top-end',
                        icon: 'success',
                        title: 'Refund Rejected Successfully',
                        showConfirmButton: true,
                        
                        }).then(function (e) {
                        console.log(e)
                        if (e.value === true) {
                        location.reload();

                        }

                    },)
                     


                    },
                 
                });
          }
         
        });
           
        }
</script>
@endsection