@extends('frontend.layouts.app')

@section('content')
<style>
  body {
    background-color: #ffffff;
    color: #444444;
    font-family: 'Roboto', sans-serif;
    font-size: 16px;
    font-weight: 300;
    margin: 0;
    padding: 0;
  }

  .process-steps li.active {
    background: #7bb128 !important;
    z-index: 8;
  }

  /* .wizard-content-left {
  background-blend-mode: darken;
  background-color: rgba(0, 0, 0, 0.45);
  background-image: url("https://i.ibb.co/X292hJF/form-wizard-bg-2.jpg");
  background-position: center center;
  background-size: cover;
  height: 100vh;
  padding: 30px;
}
.wizard-content-left h1 {
  color: #ffffff;
  font-size: 38px;
  font-weight: 600;
  padding: 12px 20px;
  text-align: center;
} */

  .form-wizard {
    color: #888888;
    padding: 30px;
  }

  .form-wizard .wizard-form-radio {
    display: inline-block;
    margin-left: 5px;
    position: relative;
  }

  .form-wizard .wizard-form-radio input[type="radio"] {
    -webkit-appearance: none;
    -moz-appearance: none;
    -ms-appearance: none;
    -o-appearance: none;
    appearance: none;
    background-color: #dddddd;
    height: 25px;
    width: 25px;
    display: inline-block;
    vertical-align: middle;
    border-radius: 50%;
    position: relative;
    cursor: pointer;
  }

  .form-wizard .wizard-form-radio input[type="radio"]:focus {
    outline: 0;
  }

  .form-wizard .wizard-form-radio input[type="radio"]:checked {
    background-color: #fb1647;
  }

  .form-wizard .wizard-form-radio input[type="radio"]:checked::before {
    content: "";
    position: absolute;
    width: 10px;
    height: 10px;
    display: inline-block;
    background-color: #ffffff;
    border-radius: 50%;
    left: 1px;
    right: 0;
    margin: 0 auto;
    top: 8px;
  }

  .form-wizard .wizard-form-radio input[type="radio"]:checked::after {
    content: "";
    display: inline-block;
    webkit-animation: click-radio-wave 0.65s;
    -moz-animation: click-radio-wave 0.65s;
    animation: click-radio-wave 0.65s;
    background: #000000;
    content: '';
    display: block;
    position: relative;
    z-index: 100;
    border-radius: 50%;
  }

  .form-wizard .wizard-form-radio input[type="radio"]~label {
    padding-left: 10px;
    cursor: pointer;
  }

  .form-wizard .form-wizard-header {
    text-align: center;
  }

  .form-wizard .form-wizard-next-btn,
  .form-wizard .form-wizard-previous-btn,
  .form-wizard .form-wizard-submit {
    background-color: #58bb13;
    color: #ffffff;
    display: inline-block;
    min-width: 100px;
    min-width: 120px;
    padding: 10px;
    text-align: center;
  }

  .form-wizard .form-wizard-next-btn:hover,
  .form-wizard .form-wizard-next-btn:focus,
  .form-wizard .form-wizard-previous-btn:hover,
  .form-wizard .form-wizard-previous-btn:focus,
  .form-wizard .form-wizard-submit:hover,
  .form-wizard .form-wizard-submit:focus {
    color: #ffffff;
    opacity: 0.6;
    text-decoration: none;
  }

  .form-wizard .wizard-fieldset {
    display: none;
  }

  .form-wizard .wizard-fieldset.show {
    display: block;
  }

  .form-wizard .wizard-form-error {
    display: none;
    background-color: #d70b0b;
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    height: 2px;
    width: 100%;
  }

  .form-wizard .form-wizard-previous-btn {
    background-color: #fb1647;
  }

  .form-wizard .form-control {
    font-weight: 300;
    height: auto !important;
    padding: 10px 0px 10px 11px;
    color: #5f5f5f;
    background-color: #f5f5f5 !important;
    border: none;
  }

  .form-wizard .form-control:focus {
    box-shadow: none;
  }

  .form-wizard .form-group {
    position: relative;
    margin: 25px 0;
  }

  .form-wizard .wizard-form-text-label {
    position: absolute;
    left: 10px;
    top: 16px;
    transition: 0.2s linear all;
  }

  .form-wizard .focus-input .wizard-form-text-label {
    color: #373737;
    top: -23px;
    transition: 0.2s linear all;
    font-size: 14px;
  }

  .form-wizard .form-wizard-steps {
    margin: 30px 0;
  }

  .form-wizard .form-wizard-steps li {
    width: 25%;
    float: left;
    position: relative;
  }

  .form-wizard .form-wizard-steps li::after {
    background-color: #f3f3f3;
    content: "";
    height: 5px;
    left: 0;
    position: absolute;
    right: 0;
    top: 50%;
    transform: translateY(-50%);
    width: 100%;
    border-bottom: 1px solid #dddddd;
    border-top: 1px solid #dddddd;
  }

  .form-wizard .form-wizard-steps li span {
    background-color: #dddddd;
    border-radius: 50%;
    display: inline-block;
    height: 40px;
    line-height: 40px;
    position: relative;
    text-align: center;
    width: 40px;
    z-index: 1;
  }

  .form-wizard .form-wizard-steps li:last-child::after {
    width: 50%;
  }

  .form-wizard .form-wizard-steps li.active span,
  .form-wizard .form-wizard-steps li.activated span {
    background-color: #58bb13;
    color: #ffffff;
  }

  .form-wizard .form-wizard-steps li.active::after,
  .form-wizard .form-wizard-steps li.activated::after {
    background-color: #58bb13;
    left: 50%;
    width: 50%;
    border-color: #58bb13;
  }

  .form-wizard .form-wizard-steps li.activated::after {
    width: 100%;
    border-color: #58bb13;
  }

  .form-wizard .form-wizard-steps li:last-child::after {
    left: 0;
  }

  .form-wizard .wizard-password-eye {
    position: absolute;
    right: 32px;
    top: 50%;
    transform: translateY(-50%);
    cursor: pointer;
  }

  @keyframes click-radio-wave {
    0% {
      width: 25px;
      height: 25px;
      opacity: 0.35;
      position: relative;
    }

    100% {
      width: 60px;
      height: 60px;
      margin-left: -15px;
      margin-top: -15px;
      opacity: 0.0;
    }
  }

  @media screen and (max-width: 767px) {
    /* .wizard-content-left {
    height: auto;
  } */
  }

  .otp-field {
    width: 45px;
    height: 40px;
    border-radius: 3px;
    border: 1.2px solid #d3d3d3;
    text-align: center;
    background-color: #f1f1f1;
  }

  .nice-select {
    width: 100% !important;
  }

  .form-wizard .form-control {
    font-weight: 300;
    height: auto !important;
    padding: 15px;
    color: #888888;
    background-color: #e2d6d6 !important;
    border: none;


  }

  .msgerror: {
    color: red;
    font-weight: bold;
  }

  .help-block {
    color: #ff4700 !important;
    font-weight: 600 !important;
  }



  /* Absolute Center Spinner */
  .loading {
    display: block !important;
    position: fixed;
    z-index: 999;
    height: 2em;
    width: 2em;
    overflow: show;
    margin: auto;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
  }

  /* Transparent Overlay */
  .loading:before {
    content: '';
    display: block;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: radial-gradient(rgba(20, 20, 20, .8), rgba(0, 0, 0, .8));

    background: -webkit-radial-gradient(rgba(20, 20, 20, .8), rgba(0, 0, 0, .8));
  }

  /* :not(:required) hides these rules from IE9 and below */
  .loading:not(:required) {
    /* hide "loading..." text */
    font: 0/0 a;
    color: transparent;
    text-shadow: none;
    background-color: transparent;
    border: 0;
  }

  .loading:not(:required):after {
    content: '';
    display: block;
    font-size: 10px;
    width: 1em;
    height: 1em;
    margin-top: -0.5em;
    -webkit-animation: spinner 150ms infinite linear;
    -moz-animation: spinner 150ms infinite linear;
    -ms-animation: spinner 150ms infinite linear;
    -o-animation: spinner 150ms infinite linear;
    animation: spinner 150ms infinite linear;
    border-radius: 0.5em;
    -webkit-box-shadow: rgba(255, 255, 255, 0.75) 1.5em 0 0 0, rgba(255, 255, 255, 0.75) 1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) 0 1.5em 0 0, rgba(255, 255, 255, 0.75) -1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) -1.5em 0 0 0, rgba(255, 255, 255, 0.75) -1.1em -1.1em 0 0, rgba(255, 255, 255, 0.75) 0 -1.5em 0 0, rgba(255, 255, 255, 0.75) 1.1em -1.1em 0 0;
    box-shadow: rgba(255, 255, 255, 0.75) 1.5em 0 0 0, rgba(255, 255, 255, 0.75) 1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) 0 1.5em 0 0, rgba(255, 255, 255, 0.75) -1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) -1.5em 0 0 0, rgba(255, 255, 255, 0.75) -1.1em -1.1em 0 0, rgba(255, 255, 255, 0.75) 0 -1.5em 0 0, rgba(255, 255, 255, 0.75) 1.1em -1.1em 0 0;
  }

  /* Animation */

  @-webkit-keyframes spinner {
    0% {
      -webkit-transform: rotate(0deg);
      -moz-transform: rotate(0deg);
      -ms-transform: rotate(0deg);
      -o-transform: rotate(0deg);
      transform: rotate(0deg);
    }

    100% {
      -webkit-transform: rotate(360deg);
      -moz-transform: rotate(360deg);
      -ms-transform: rotate(360deg);
      -o-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }

  @-moz-keyframes spinner {
    0% {
      -webkit-transform: rotate(0deg);
      -moz-transform: rotate(0deg);
      -ms-transform: rotate(0deg);
      -o-transform: rotate(0deg);
      transform: rotate(0deg);
    }

    100% {
      -webkit-transform: rotate(360deg);
      -moz-transform: rotate(360deg);
      -ms-transform: rotate(360deg);
      -o-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }

  @-o-keyframes spinner {
    0% {
      -webkit-transform: rotate(0deg);
      -moz-transform: rotate(0deg);
      -ms-transform: rotate(0deg);
      -o-transform: rotate(0deg);
      transform: rotate(0deg);
    }

    100% {
      -webkit-transform: rotate(360deg);
      -moz-transform: rotate(360deg);
      -ms-transform: rotate(360deg);
      -o-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }

  @keyframes spinner {
    0% {
      -webkit-transform: rotate(0deg);
      -moz-transform: rotate(0deg);
      -ms-transform: rotate(0deg);
      -o-transform: rotate(0deg);
      transform: rotate(0deg);
    }

    100% {
      -webkit-transform: rotate(360deg);
      -moz-transform: rotate(360deg);
      -ms-transform: rotate(360deg);
      -o-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }

  .elmt {
    display: none;
  }
</style>
<div class="elmt">Loading&#8230;</div>
<section class="gry-bg py-4 profile seller_reg">
  <div class="container">
    <div class="row cols-xs-space cols-sm-space cols-md-space">
      <div class="col-lg-9 mx-auto">
        <div class="main-content">
          <!-- Page title -->
          <!-- <div class="page-title">
            <div class="row align-items-center">
              <div class="col-md-6">
                <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                  {{ translate('Shop Informations')}}
                </h2>
              </div>
              <div class="col-md-6">
                <div class="float-md-right">
                  <ul class="breadcrumb">
                    <li><a href="{{ route('home') }}">{{ translate('Home')}}</a></li>
                    <li><a href="{{ route('dashboard') }}">{{ translate('Dashboard')}}</a></li>
                    <li class="active"><a href="{{ route('shops.create') }}">{{ translate('Create Shop')}}</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div> -->
          @csrf
          @if (!Auth::check())
          <div class="form-box bg-white mt-4">
            <div class="form-box-title px-3 py-2">

            </div>
            <div class="form-box-content p-3" style="box-shadow: 2px 1px 12px black;">
              <div class="col-lg-12 col-md-12">
                @if (count($errors) > 0)
                <div class="alert alert-danger successErrorMessage">
                  <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
                @endif
                <div class="form-wizard">
                  <form class="" action="{{ route('shops.store') }}" method="POST" enctype="multipart/form-data"
                    id="form1">
                    @csrf
                    <div class="form-wizard-header">
                      <p>Fill all form field to go next step</p>
                      <ul class="list-unstyled form-wizard-steps clearfix">
                        <li class="active" id="step-1"><span>1</span></li>
                        <li id="step-2"><span>2</span></li>
                        <li id="step-3"><span>3</span></li>
                        <li id="step-4"><span>4</span></li>
                      </ul>
                    </div>
                    <fieldset class="wizard-fieldset show">
                      <h5>Personal Information</h5>
                      <div class="form-group">
                        <input type="text" autocomplete="off" value="{{old('fname')}}"
                          class="form-control wizard-required" name="fname" id="fname">
                        @if ($errors->has('fname'))
                        <span class="help-block">
                          <strong>{{ $errors->first('fname') }}</strong>
                        </span>
                        @endif
                        <label for="fname" class="wizard-form-text-label">First Name*</label>
                        <div class="wizard-form-error"></div>
                      </div>
                      <div class="form-group">
                        <input type="text" autocomplete="off" value="{{old('lname')}}"
                          class="form-control wizard-required" name="lname" id="lname">
                        @if ($errors->has('lname'))
                        <span class="help-block">
                          <strong>{{ $errors->first('lname') }}</strong>
                        </span>
                        @endif
                        <label for="lname" class="wizard-form-text-label">Last Name*</label>
                        <div class="wizard-form-error"></div>
                      </div>
                      <div class="form-group">
                        <input type="text" autocomplete="off" value="{{old('email')}}"
                          class="form-control wizard-required" name="email" id="email">
                        @if ($errors->has('email'))
                        <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                        <label for="text" class="wizard-form-text-label">Email Id*</label>
                        <div class="wizard-form-error"></div>
                      </div>
                      <div class="form-group">
                        <input type="text" autocomplete="off" value="{{old('phone_no')}}"
                          class="form-control wizard-required" name="phone_no" id="phone_no" maxlength="13">
                        @if ($errors->has('phone_no'))
                        <span class="help-block">
                          <strong>{{ $errors->first('phone_no') }}</strong>
                        </span>
                        @endif
                        <label for="phone_no" class="wizard-form-text-label">Mobile No*</label>
                        <div class="wizard-form-error"></div>
                      </div>

                      <div class="form-group">
                        <input type="password" autocomplete="off" class="form-control wizard-required" name="password"
                          id="password">
                        @if ($errors->has('password'))
                        <span class="help-block error">
                          <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                        <label for="text" class="wizard-form-text-label passcls">Password*</label>
                        <div class="wizard-form-error"></div>
                      </div>
                      <div class="form-group">
                        <input type="password" autocomplete="off" value="" class="form-control wizard-required"
                          name="password_confirmation" id="password_confirmation">
                        <span class="help-block msgerror" id="passconfirm">
                          <strong></strong>
                        </span>
                        @if ($errors->has('password_confirmation'))
                        <span class="help-block error">
                          <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                        @endif
                        <label for="password_confirmation" class="wizard-form-text-label">Confirm Password*</label>
                        <div class="wizard-form-error"></div>
                      </div>


                     
                   
                      <span>Already Registered? &nbsp;&nbsp;<a href="{{route('login')}}"><i
                            class="fa fa-sign-in">&nbsp;&nbsp;Login here</i> </a></span>
                      <div class="form-group clearfix">
                        <a href="javascript:;" class="form-wizard-next-btn float-right">Next</a>
                      </div>
                    </fieldset>
                    <fieldset class="wizard-fieldset">
                      <h5>Please Enter the One Time Password</h5>
                      <div class="form-group">
                        <input type="text" autocomplete="off" class="form-control wizard-required" name="otp_verify"
                          id="otp_verify">
                        <label for="otp_verify" class="wizard-form-text-label">Enter Otp*</label>
                        <div class="wizard-form-error"></div>
                      </div>
                      <span>Already Registered? &nbsp;&nbsp;<a href="{{route('login')}}"><i
                            class="fa fa-sign-in">&nbsp;&nbsp;Login here</i> </a></span>
                      <div class="form-group clearfix">
                        <a href="javascript:;" class="form-wizard-previous-btn float-left">Previous</a>
                        <a href="javascript:;" class="form-wizard-next-btn float-right">Next</a>
                      </div>
                    </fieldset>
                    <fieldset class="wizard-fieldset">
                      <h5>Business Info</h5>

                      <div class="row">
                        <div class="col-12">Nature Of Business*</div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                          <div class="form-group">
                            <select class="form-control" name="business_nature" id="business_nature">
                              <option>--Select--</option>
                              @foreach(Config::get('constants.BUSINESS_NATURE') as $key=>$val)

                              <option value="{{$key}}" @if(old('business_nature')===$key) selected @endif>{{$val}}
                              </option>

                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="form-group  ">
                        <input type="text" value="" autocomplete="off" class="form-control wizard-required" name="name"
                          id="name">
                        @if ($errors->has('name'))
                        <span class="help-block error">
                          <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                        <label for="name" class="wizard-form-text-label">Business Name*</label>
                        <div class="wizard-form-error"></div>
                      </div>

                      <div class="form-group">
                        <input type="text" class="form-control wizard-required" value="{{old('hsn_code')}}"
                          name="hsn_code" id="hsn_code">
                        <label for="hsn_code" class="wizard-form-text-label">HSN Code*</label>
                        <div class="wizard-form-error"></div>
                      </div>
                      <div class="row">
                        <div class="col-12">Category Product*</div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                          <div class="form-group">
                            <select class="form-control" name="product_category" id="product_category">
                              <option>--Select--</option>
                              @foreach($category as $key=>$val)

                              <option value="{{$val['id']}}" @if(old('product_category')===$val['id']) selected @endif>
                                {{$val['name']}}</option>

                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="row" id="product_invoice_div" style="display: none;">
                        <div class="col-12">Product Purchase Invoice*</div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                          <div class="form-group">
                            <input type="file" class="form-control " name="product_invoice"
                              id="product_invoice" data-multiple-caption="{count} files selected">
                          </div>
                        </div>
                      </div>
                      <div class="row" id="product_licence">
                        <div class="col-12">Product Licence*</div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                          <div class="form-group">
                            <input type="file" class="form-control wizard-required" name="product_licence"
                              id="product_licence" data-multiple-caption="{count} files selected">
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <textarea class="form-control wizard-required" name="address_business"
                          id="address_business">{{ old('address_business') }}</textarea>
                        <label for="address_business" class="wizard-form-text-label">Business Address*</label>
                        <div class="wizard-form-error"></div>
                      </div>
                      <div class="form-group">
                        <input type="text" maxlength="50" autocomplete="off" class="form-control wizard-required"
                          name="city" id="city">
                        <label for="city" class="wizard-form-text-label">City*</label>
                        <div class="wizard-form-error"></div>
                      </div>
                      <div class="form-group">
                        <input type="text" value="{{old('pincode')}}" maxlength="10" autocomplete="off"
                          class="form-control wizard-required" name="postal_code" id="postal_code">
                        <label for="pincode" class="wizard-form-text-label">Pincode*</label>
                        <div class="wizard-form-error"></div>
                      </div>
                      <div class="form-group">
                        <input type="text" autocomplete="off" value="{{old('pan_card')}}"
                          class="form-control wizard-required" name="pan_card" id="pan_card">
                        <label for="pan_card" class="wizard-form-text-label">Pan Card*</label>
                        <div class="wizard-form-error"></div>
                      </div>

                      <div class="row">
                        <div class="col-12">Pancard File*</div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                          <div class="form-group">
                            <input type="file" autocomplete="off" name="file_pan" class="form-control wizard-required"
                              id="file_pan" data-multiple-caption="{count} files selected">
                          </div>
                        </div>
                      </div>
                      <div class="form-group {{ $errors->has('gst') ? ' has-error' : '' }}">
                        <input type="text" value="{{ old('gst') }}" class="form-control wizard-required" name="gst"
                          id="gst">
                        @if ($errors->has('gst'))
                        <span class="help-block">
                          <strong>{{ $errors->first('gst') }}</strong>
                        </span>
                        @endif
                        <label for="gst" class="wizard-form-text-label">GST*</label>
                        <div class="wizard-form-error"></div>

                      </div>
                      <div class="row">
                        <div class="col-12">GST File*</div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                          <div class="form-group">
                            <input type="file" name="file_gst" id="file_gst" class="form-control wizard-required"
                              data-multiple-caption="{count} files selected">
                          </div>
                        </div>
                      </div>

                      <span>Already Registered? &nbsp;&nbsp;<a href="{{route('login')}}"><i
                            class="fa fa-sign-in">&nbsp;&nbsp;Login here</i> </a></span>
                      <div class="form-group clearfix">
                        <a href="javascript:;" class="form-wizard-previous-btn float-left">Previous</a>
                        <a href="javascript:;" class="form-wizard-next-btn float-right">Next</a>
                      </div>
                    </fieldset>

                    <fieldset class="wizard-fieldset">
                      <h5>Bank Information</h5>
                      <div class="form-group">
                        <input type="text" value="{{old('bank_acc_no')}}" autocomplete="off"
                          class="form-control wizard-required" id="bank_acc_no" name="bank_acc_no">
                        <label for="bank_acc_no" class="wizard-form-text-label">Bank Account Number*</label>
                        <div class="wizard-form-error"></div>
                      </div>
                      <div class="form-group">
                        <input type="text" value="{{old('ifsc_code')}}" autocomplete="off"
                          class="form-control wizard-required" name="ifsc_code" id="ifsc_code">
                        <label for="ifsc_code" class="wizard-form-text-label">Branch IFSC Code*</label>
                        <div class="wizard-form-error"></div>
                      </div>
                      <div class="form-group">
                        <input type="text" autocomplete="off" value="{{old('bname')}}"
                          class="form-control wizard-required" name="bname" id="bname">
                        <label for="bname" class="wizard-form-text-label">Branch Name*</label>
                        <div class="wizard-form-error"></div>
                      </div>
                      <div class="row">
                        <div class="col-12">Trademark Logo*</div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                          <div class="form-group">
                            <input type="file" class="form-control wizard-required" name="trademark_icon" id="acon"
                              data-multiple-caption="{count} files selected">
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-12">Cancelled Cheque Upload*</div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                          <div class="form-group">
                            <input type="file" class="form-control wizard-required" name="cancelled_cheque" id="cheque"
                              data-multiple-caption="{count} files selected">
                          </div>
                        </div>
                      </div>
                      
                      <div class="row">
                        <div class="col s12">
                          <div class="blog-info">
                             
                            <label>
                              <input type="checkbox"  name="declaration" id="declaration"   class="filled-in info colored"  required/>
                              <span style="font-weight:bold; font-size:14px"> I do hereby declare that PAN furnished/stated above is correct and belongs to me, registered as an account holder with
                                www.flipkart.com. I further declare that i shall solely be held responsible for the consequences, in case of any false PAN 
                                declaration</span>
                            </label>
                           <span id="show_checkbox_error" style='color: #e63b05;font-weight: 600;font-size: 12px; display:none;'>Please tick on checkbox</span>
                          </div>
                        </div>
                      </div>

                      <span>Already Registered? &nbsp;&nbsp;<a href="{{route('login')}}"><i
                            class="fa fa-sign-in">&nbsp;&nbsp;Login here</i> </a></span>
                      <div class="form-group clearfix">
                        <a href="javascript:;" class="form-wizard-previous-btn float-left">Previous</a>
                        <a href="javascript:;"
                          class="form-wizard-next-btn float-right">Submit</a>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
          @endif

        </div>
      </div>
    </div>
  </div>
</section>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script>
  // or in pure javascript
//  window.onload=function(){                                              
//     setTimeout(function(){  
//       alert('hi');
//         document.getElementById('password').type = 'password';
//     },10);
//   }   
//    // or in pure javascript
//  window.onload=function(){                                              
//     setTimeout(function(){  
//         document.getElementById('password_confirmation').type = 'password';
//     },10);
//   }  
//   // or in pure javascript
//  window.onload=function(){                                              
//     setTimeout(function(){  
//         document.getElementById('email').type = 'email';
//     },10);
//   }

     
  // ------------step-wizard-------------
jQuery(document).ready(function() {
  var timer;
$(document).ajaxStart(function(){
 
       timer = setTimeout(function(){
          $(".elmt").addClass("loading");
       },1500);
    });

    $(document).ajaxStop(function(){
      clearTimeout(timer);
      $(".elmt").removeClass("loading");
    });
    $('input[type="password"]').bind('keyup', function() {
      $('.label-important').remove(); 
      var strongRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})";
    if(this.value.match(strongRegex)) 
        { 
        //alert('Correct, try another...')
        return true;
        }
        else
        { 
          $('.label-important').remove(); 
          $(this).after("<span class='label label-important' style='color: #e63b05;font-weight: 600;font-size: 12px;'>Pasword Must Contain at least 1 lowercase,1 uppercase,1 numeric,one special character and greater than 8 charcter</span>");
          return false;  
        }
    });
  $('input[type="file"]').bind('change', function() {
    $('.label-important').remove(); 
var allowedExtensions =  
          /(\.pdf)$/i; 
    
  if (!allowedExtensions.exec(this.value)) { 
    $('.label-important').remove(); 
  $(this).after("<span class='label label-important' style='color: #e63b05;font-weight: 600;font-size: 12px;'>Invalid file type Only PDF Allowed</span>");
      this.value = ''; 
      return false; 
  }
  if(this.files[0].size/1024/1024 > 5){
    $('.label-important').remove(); 
  $(this).after("<span class='label label-important' style='color: #e63b05;font-weight: 600;font-size: 12px;'>File size must be less than 5 mb</span>");
      this.value = ''; 
      return false; 
  }  
});
$("#phone_no").focus(function(){
  $("#phone_no").val("+91");
});

  $("#phone_no").keyup(function(){
    // var prefix = "+91"

    // if(this.value.indexOf(prefix) !== 0 ){
    //     this.value = prefix + this.value;
    // }
    if($(this).val().indexOf('+91') == 0) {
        $(this).val($(this).val());
    }else{
      $phone_val=$(this).val();
      if($phone_val!=""){
        $(this).val("");
      }
      $(this).val("+91" + $(this).val());
    }
});
  setTimeout(function(){  
     
      
        document.getElementById('password').type = 'password';

    },1000);
  //$("#password").prop("type", "password");
	// click on next button
  $("#business_nature").change(function(){
    $("#product_invoice").val("");
    if(this.value=="Reseller"){
      $("#product_invoice_div").show();
      $('#product_invoice').addClass('wizard-required');
    }else{
      $("#product_invoice_div").hide();
    }
  });
	jQuery('.form-wizard-next-btn').click(function() {
    var  isValid = "";
		var parentFieldset = jQuery(this).parents('.wizard-fieldset');
		var currentActiveStep = jQuery(this).parents('.form-wizard').find('.form-wizard-steps .active');
    console.log(currentActiveStep);
    console.log(currentActiveStep[0].id);
    var parentid=currentActiveStep[0].id;
		var next = jQuery(this);
		var nextWizardStep = true;
		parentFieldset.find('.wizard-required').each(function(){
			var thisValue = jQuery(this).val();
    
			if( thisValue == "") {
				jQuery(this).siblings(".wizard-form-error").slideDown();
				nextWizardStep = false;
			}
			else {
				jQuery(this).siblings(".wizard-form-error").slideUp();
			}
		});
		if( nextWizardStep) {
if(parentid=="step-1"){
  var strongRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})";
  var password=$("#password").val();
  var password_confirmation=$("#password_confirmation").val();
  $('#passconfirm').html('');
  if(password!=password_confirmation){
$('#passconfirm').html('Password and confirm password does not match');

return false;
  }
  if(!password.match(strongRegex)){
return false;
  }
  $('#passconfirm').html('');
            var user_phone=$("#phone_no").val();
            var user_email=$("#email").val();
            var token = $("input[name='_token']").val();
   
           var form_data=new FormData();
           form_data.append('user_phone',user_phone);
           form_data.append('user_email',user_email);
           form_data.append('_token',token);
           $.ajax({
               type: "post",
               url: "{{ route('shops.vendor_send_otp') }}",
               cache: false,
               processData: false,
               contentType: false,
               data: form_data,
               dataType:"json",
               success: function(response) {
                   if(response.status==1){
                       isValid=1;
                       wizardfunc(next,currentActiveStep);
               }else{
                   isValid=0;
                   showFrontendAlert("error", response.message);
                //  wizardfunc(next,currentActiveStep)
               }
                   },
                           error: function (jqXHR, textStatus, errorThrown) {
                               $(".se-pre-con").fadeOut("slow");
                               var msg = "";
                               if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                                   msg += "<strong>Something Went Wrong,Please Reload and Try Again</strong>";
                               } else {
                                   if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                       msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                   } else {
                                       msg += "Error(s):<strong><ul>";
                                       $.each(jqXHR.responseJSON['errors'], function (key, value) {
                                           msg += "<li>" + value + "</li>";
                                       });
                                       msg += "</ul></strong>";
                                   }
                               }
                               isValid=0;
                               showFrontendAlert("error", msg);
                           }
           });
}
if(parentid=="step-2"){

  var otp_new='';
                    var otp_verify=$("#otp_verify").val();
                    // var otpb=$("#otpb").val();
                    // var otpc=$("#otpc").val();
                    // var otpd=$("#otpd").val();
                    // var otpe=$("#otpe").val();
                  //  if(otp_verify=='' || otpb=='' || otpc=='' || otpd=='' || otpe==''){
                      if(otp_verify==''){
                  var error_show='Otp must be 5 digit';
                  showFrontendAlert("error", error_show);
                  isValid=0;
              
                }else{
                    var otp_new=otp_verify;
                }
                    $.ajax({
            url: "{{ route('shops.check_otp_for_signup') }}",
            dataType: 'json',
            data:{'otp':otp_new,'_token':$("input[name='_token']").val()},
            method: 'post'
        }).done(function (response) {
            if(response.status==1){
                isValid=1;
                wizardfunc(next,currentActiveStep)
            }else{
                
            showFrontendAlert("error", response.message);
            isValid=0;
         //   wizardfunc(next,currentActiveStep)
            }
        }).fail(function(){
            showFrontendAlert("error", 'OTP Verification Proceess Faild');
            isValid=0;
          
        });

}
// if(parentid=="step-3" || parentid=="step-4"){
//   wizardfunc(next,currentActiveStep)
// }

 if(parentid=="step-3"){

  var name=$("#name").val();
            var gst=$("#gst").val();
            var token = $("input[name='_token']").val();
   
           var form_data=new FormData();
           form_data.append('name',name);
           form_data.append('gst',gst);
           form_data.append('_token',token);
           $.ajax({
               type: "post",
               url: "{{ route('shops.gstshop') }}",
               cache: false,
               processData: false,
               contentType: false,
               data: form_data,
               dataType:"json",
               success: function(response) {
                   if(response.status==1){
                       isValid=1;
                       wizardfunc(next,currentActiveStep);
               }else{
                   isValid=0;
                   showFrontendAlert("error", response.message);
                  //wizardfunc(next,currentActiveStep)
               }
                   },
                           error: function (jqXHR, textStatus, errorThrown) {
                               //$(".se-pre-con").fadeOut("slow");
                               var msg = "";
                               if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                                   msg += "<strong>Something Went Wrong,Please Reload and Try Again</strong>";
                               } else {
                                   if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                       msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                   } else {
                                       msg += "Error(s):<strong><ul>";
                                       $.each(jqXHR.responseJSON['errors'], function (key, value) {
                                           msg += "<li>" + value + "</li>";
                                       });
                                       msg += "</ul></strong>";
                                   }
                               }
                               isValid=0;
                               showFrontendAlert("error", msg);
                           }
           });
 }
 if(parentid=="step-4"){
 
        document.getElementById('form1').submit();
 
 }

	
		}
	});
	//click on previous button
	jQuery('.form-wizard-previous-btn').click(function() {
		var counter = parseInt(jQuery(".wizard-counter").text());;
		var prev =jQuery(this);
		var currentActiveStep = jQuery(this).parents('.form-wizard').find('.form-wizard-steps .active');
		prev.parents('.wizard-fieldset').removeClass("show","400");
		prev.parents('.wizard-fieldset').prev('.wizard-fieldset').addClass("show","400");
		currentActiveStep.removeClass('active').prev().removeClass('activated').addClass('active',"400");
		jQuery(document).find('.wizard-fieldset').each(function(){
			if(jQuery(this).hasClass('show')){
				var formAtrr = jQuery(this).attr('data-tab-content');
				jQuery(document).find('.form-wizard-steps .form-wizard-step-item').each(function(){
					if(jQuery(this).attr('data-attr') == formAtrr){
						jQuery(this).addClass('active');
						var innerWidth = jQuery(this).innerWidth();
						var position = jQuery(this).position();
						jQuery(document).find('.form-wizard-step-move').css({"left": position.left, "width": innerWidth});
					}else{
						jQuery(this).removeClass('active');
					}
				});
			}
		});
	});
	//click on form submit button
	jQuery(document).on("click",".form-wizard .form-wizard-submit" , function(){
		var parentFieldset = jQuery(this).parents('.wizard-fieldset');
		var currentActiveStep = jQuery(this).parents('.form-wizard').find('.form-wizard-steps .active');
		parentFieldset.find('.wizard-required').each(function() {
			var thisValue = jQuery(this).val();
			if( thisValue == "" ) {
				jQuery(this).siblings(".wizard-form-error").slideDown();
			}
			else {
				jQuery(this).siblings(".wizard-form-error").slideUp();
			}
		});
	});
	// focus on input field check empty or not
	jQuery(".form-control").on('focus', function(){
		var tmpThis = jQuery(this).val();
		if(tmpThis == '' ) {
			jQuery(this).parent().addClass("focus-input");
		}
		else if(tmpThis !='' ){
			jQuery(this).parent().addClass("focus-input");
		}
	}).on('blur', function(){
		var tmpThis = jQuery(this).val();
		if(tmpThis == '' ) {
			jQuery(this).parent().removeClass("focus-input");
			jQuery(this).siblings('.wizard-form-error').slideDown("3000");
		}
		else if(tmpThis !='' ){
			jQuery(this).parent().addClass("focus-input");
			jQuery(this).siblings('.wizard-form-error').slideUp("3000");
		}
	});

  $(".otp-field").keyup(function() {
            if (this.value.length == this.maxLength) {
                var $next = $(this).next('.otp-field');
                if ($next.length)
                    $(this).next('.otp-field').focus();
                else
                    $(this).blur();
            }
        });
$('.otp-field').focus(function() {
            this.value = "";
        });

        //Enumerate click submit on [ENTER]-keypress
        $('.otp-field').keypress(function(e) {
            if (e.which == 13) {
                jQuery(this).blur();
                jQuery('#submit').click();
            }
        }).keydown(function(e) {
            if ((e.which === 8 || e.which === 46) && $(this).text() === '') {
                $(this).prev('.otp-field').focus();
            }
        });

$(".otp-field").keypress(function(event) {
            return /\d/.test(String.fromCharCode(event.keyCode));
        });
});

function wizardfunc(next,currentActiveStep){
  next.parents('.wizard-fieldset').removeClass("show","400");
			currentActiveStep.removeClass('active').addClass('activated').next().addClass('active',"400");
			next.parents('.wizard-fieldset').next('.wizard-fieldset').addClass("show","400");
			jQuery(document).find('.wizard-fieldset').each(function(){
				if(jQuery(this).hasClass('show')){
					var formAtrr = jQuery(this).attr('data-tab-content');
       
					jQuery(document).find('.form-wizard-steps .form-wizard-step-item').each(function(){
						if(jQuery(this).attr('data-attr') == formAtrr){
							jQuery(this).addClass('active');
							var innerWidth = jQuery(this).innerWidth();
							var position = jQuery(this).position();
           
							jQuery(document).find('.form-wizard-step-move').css({"left": position.left, "width": innerWidth});
						}else{
							jQuery(this).removeClass('active');
						}
					});
				}
			});
}

</script>
@endsection