@extends('frontend.seller.layouts.seller_app')

@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-12 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0"> {{ translate('Request to Admin')}}</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">

                                <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ translate('Home')}}</a>
                                </li>
                                <li class="breadcrumb-item"><a
                                        href="{{ route('dashboard') }}">{{ translate('Dashboard')}}</a></li>
                                <li class="breadcrumb-item active"><a
                                        href="{{ route('profile') }}">{{ translate('New Request')}}</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-12 content-body">
            <section class="gry-bg profile">
                <div class="container">
                    <div class="row cols-xs-space cols-sm-space cols-md-space">


                        <div class="col-lg-12">
                            <div class="main-content">
                                <!-- Page title -->

                            
                     <div class="form-box">
                           <div class="form-box-title px-3 py-2">
                                    {{ translate('Request for Category/Subcategory/Subsubcategory/HSN')}}
                            </div>
                            <div class="form-box-content p-3">
                                <div class="row gutters-10">
                                <div class="col-md-4 mx-auto">
                                <a class="dashboard-widget text-center plus-widget mt-4 d-block" href="javascript:"  onclick="add_new_request()">
                                    <i class="la la-plus"></i>
                                    <span class="d-block title heading-6 strong-400 c-base-1">{{  translate('Add New Request') }}</span>
                                </a>
                            </div>
                                   
                                </div>
                            </div>
                            <div class="card no-border mt-4">
                           
                            <div class="card-body">
                                <table class="table table-sm table-hover table-responsive-md">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                           
                                            <th>{{ translate('Category')}}</th>
                                            <th>{{ translate('Sub category')}}</th>
                                            <th>{{ translate('Sub Subcategory')}}</th>
                                            <th>{{ translate('HSN')}}</th>
                                            <th>{{ translate('GST(%)')}}</th>
                                            <th>{{ translate('Status')}}</th>
                                            <th>{{ translate('Options')}}</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                    @foreach ($sellerRequest as $key => $sellerRequests)
                                            <tr>
                                                <td>{{ ($key+1) + ($sellerRequest->currentPage() - 1)*$sellerRequest->perPage() }}</td>
                                                
                                                <td>{{ $sellerRequests->category }}</td>
                                                 <td>{{ $sellerRequests->subcategory }}</td>
                                                <td>
                                                    @if ($sellerRequests->subsubcategory != null)
                                                        {{ $sellerRequests->subsubcategory}}
                                                    @endif
                                                </td>
                                                <td>{{ $sellerRequests->hsn }}</td>
                                                <td>{{ $sellerRequests->gst }}%</td>
                                                <td>
                                                @if ($sellerRequests->status == 0)
                                                                <span class="badge badge-pill badge-secondary">{{ translate('Pending')}}</span>
                                                            @elseif ($sellerRequests->status == 1)
                                                                <span class="badge badge-pill badge-success">{{ translate('Approved')}}</span>
                                                            @else
                                                                <span class="badge badge-pill badge-danger">{{ translate('Rejected')}}</span>
                                                            @endif
                                                </td>
                                                <td>
                                                @if ($sellerRequests->status == 0)
                                                <a onclick="confirm_modal('{{route('request_destroy', ['id' => $sellerRequests->id] )}}')" ><i class="fa fa-trash"></i></a>
                                                @endif
                                            </td>
                                                
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div> 
</div>
</div>
</div>
</div>
</section>
</div>
</div>
</div>
<div class="modal fade" id="new-request-modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-zoom" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="exampleModalLabel">{{ translate('New Request')}}</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form-default" role="form" action="{{ route('new_request') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="p-3">
                        <div class="row">
                            <div class="col-md-5">
                                <label>{{ translate('Category')}}<span class="required-star">*</span></label>
                            </div>
                            <div class="col-md-7">
                            <input type="text" class="form-control mb-3" placeholder="{{ translate('Enter Category')}}"
                                    name="category" value="" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <label>{{ translate('Sub Category')}}<span class="required-star">*</span></label>
                            </div>
                            <div class="col-md-7">
                            <input type="text" class="form-control mb-3" placeholder="{{ translate('Enter Sub Category')}}"
                                    name="subcategory" value="" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <label>{{ translate('Sub Sub Category')}}</label>
                            </div>
                            <div class="col-md-7">
                            <input type="text" class="form-control mb-3" placeholder="{{ translate('Enter Sub Sub Category')}}"
                                    name="subsubcategory" value="" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <label>{{ translate('HSN No')}}<span class="required-star">*</span></label>
                            </div>
                            <div class="col-md-7">
                            <input type="text" class="form-control mb-3" placeholder="{{ translate('Enter HSN No')}}"
                                    name="hsn" value="" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <label>{{ translate('GST(%)')}}<span class="required-star">*</span></label>
                            </div>
                            <div class="col-md-7">
                            <!-- <input type="number" pattern="[0-9]*" name="cost"> -->
                            <input type="text" maxlength="2" pattern="[0-9]*" class="form-control mb-3" placeholder="{{ translate('Enter GST')}}"
                                    name="gst" id="gst" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                        <button type="submit" class="btn btn-base-1" style="float:right;">{{  translate('Save') }}</button>
                        </div>
                        </div>
                    </div>
                </div>
                
            </form>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
    function add_new_request(){
            $('#new-request-modal').modal('show');
        }
        $("#gst").keyup(function() {
    $("#gst").val(this.value.match(/[0-9]*/));
});
</script>
@endsection