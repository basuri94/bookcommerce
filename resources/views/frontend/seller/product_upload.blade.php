@extends('frontend.seller.layouts.seller_app')

@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <h2 class="content-header-title float-left mb-0"> {{ translate('Add Book Details')}}</h2>
                <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                    
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ translate('Home')}}</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">{{ translate('Dashboard')}}</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('seller.products') }}">{{ translate('Book')}}</a></li>
                    <li  class="breadcrumb-item active"><a href="{{ route('seller.products.upload') }}">{{ translate('Add Book Details')}}</a></li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
          
        </div>
     <div class="col-md-12 content-body">
    <section class="gry-bg profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                

                <div class="col-lg-12">
                    <div class="main-content">
                        
                        <form class="" action="{{route('products.store')}}" method="POST" enctype="multipart/form-data" id="choice_form">
                            @csrf
                    		<input type="hidden" name="added_by" value="seller">

                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{ translate('General')}}
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Book ( Bengali/English )')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" name="name" placeholder="{{ translate('Book Name ( Bengali / English )')}}" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Category')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="form-control mb-3 c-pointer" data-toggle="modal" data-target="#categorySelectModal" id="product_category">{{ translate('Select a category')}}</div>
                                            <input type="hidden" name="category_id" id="category_id" value="" required>
                                            <input type="hidden" name="subcategory_id" id="subcategory_id" value="" required>
                                            <input type="hidden" name="subsubcategory_id" id="subsubcategory_id" value="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Publishers Name')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="mb-3">
                                                <select class="form-control mb-3 selectpicker" data-placeholder="{{ translate('Select Publishers Name') }}" id="brands" name="brand_id" onchange="addbrand();">
                                                    <option value="">{{ ('Select Publishers Name') }}</option>
                                                    @foreach (\App\Brand::all() as $brand)
                                                        <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                                    @endforeach
                                                    <option value="add">{{ ('Add Publishers Nam') }}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="display: none" id="brand_div">
                                        <div class="col-md-2">
                                            <label>{{ translate('Add Publishers Name')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3"  name="add_brand" id="add_brand" placeholder="{{ translate('Enter Publishers Name') }}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Book Pages')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="unit" placeholder="{{ translate('Book Pages') }}" required>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top:20px;">
                                        <div class="col-md-2">
                                            <label>{{ translate('Binding')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                               <select class="form-control mb-3 " data-placeholder="{{ translate('Select Publishers Name') }}" id="country_of_origin" name="country_of_origin" >
                                                    <option value="">{{ ('Select Binding') }}</option>
                                                    <option value="Paper">{{ ('Paper') }}</option>
                                                    <option value="Hard cover">{{ ('Hard cover') }}</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Book Pages')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="unit" placeholder="{{ translate('Book Pages') }}" required>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top:20px;">
                                        <div class="col-md-2">
                                            <label>{{ translate('Current Publishing Year')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3 tagsInput" name="publishing_year" id="publishing_year" placeholder="{{ translate('Current Publishing Year') }}" data-role="tagsinput" required>
                                        </div>
                                    </div>
                                    
                                    @php
                                        $pos_addon = \App\Addon::where('unique_identifier', 'pos_system')->first();
                                    @endphp
                                    @if ($pos_addon != null && $pos_addon->activated == 1)
            							<div class="row mt-2">
            								<label class="col-md-2">{{ translate('Barcode')}}</label>
            								<div class="col-md-10">
            									<input type="text" class="form-control mb-3" name="barcode" placeholder="{{  translate('Barcode') }}">
            								</div>
            							</div>
                                    @endif

                                   <div class="row mt-2">
            								<label class="col-md-2">{{ translate('Refundable')}}</label>
            								<div class="col-md-10">
            									<label class="switch" style="margin-top:5px;">
            										<input type="checkbox" name="refundable" checked>
            			                            <span class="slider round"></span></label>
            									</label>
            								</div>
                                        </div>
                                        <div class="row mt-2">
            								<label class="col-md-2">{{ translate('No Contact Delivery')}}</label>
            								<div class="col-md-10">
            									<label class="switch" style="margin-top:5px;">
            										<input type="checkbox" name="no_delivery" checked>
            			                            <span class="slider round"></span></label>
            									</label>
            								</div>
                                        </div>
                                        <div class="row mt-2">
            								<label class="col-md-2">{{ translate('7 Days Return')}}</label>
            								<div class="col-md-10">
            									<label class="switch" style="margin-top:5px;">
            										<input type="checkbox" name="days_return" checked>
            			                            <span class="slider round"></span></label>
            									</label>
            								</div>
                                        </div>
                                        
                                        <div class="row mt-2">
            								<label class="col-md-2">{{ translate('Brand Warrenty')}}</label>
            								<div class="col-md-10">
            									<label class="switch" style="margin-top:5px;">
            										<input type="checkbox" name="b_warrenty" checked>
            			                            <span class="slider round"></span></label>
            									</label>
            								</div>
                                        </div>
                                        <div class="row mt-2">
            								<label class="col-md-2">{{ translate('Cash On Delivery')}}</label>
            								<div class="col-md-10">
            									<label class="switch" style="margin-top:5px;">
            										<input type="checkbox" name="cod" checked>
            			                            <span class="slider round"></span></label>
            									</label>
            								</div>
                                        </div>
                                        <div class="row mt-2">
            								<label class="col-md-2">{{ translate('Online Payment')}}</label>
            								<div class="col-md-10">
            									<label class="switch" style="margin-top:5px;">
            										<input type="checkbox" name="online_payment" checked>
            			                            <span class="slider round"></span></label>
            									</label>
            								</div>
            							</div>
                                   
                                </div>
                            </div>
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{ translate('Customer Choice')}}
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row mb-3">
                                        <div class="col-8 col-md-3 order-1 order-md-0">
        									<input type="text" class="form-control" value="{{ translate('Languages')}}" disabled>
        								</div>
        								<div class="col-12 col-md-7 col-xl-8 order-3 order-md-0 mt-2 mt-md-0">
        									<select class="form-control " name="colors" id="colors" >
        										@foreach (\App\Color::orderBy('name', 'asc')->get() as $key => $color)
        											<option value="{{ $color->code }}">{{ $color->name }}</option>
        										@endforeach
        									</select>
        								</div>
        								<div class="col-4 col-xl-1 col-md-2 order-2 order-md-0 text-right">
        									<label class="switch" style="margin-top:5px;">
        										<input value="1" type="checkbox" name="colors_active" checked>
        										<span class="slider round"></span>
        									</label>
        								</div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-md-3">
                                            <label>{{ translate('Weight Class')}}</label>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="">
                                                <select name="choice_attributes" id="choice_attributes" class="form-control "  data-placeholder="{{ translate('Choose Weight Class') }}">
                                                <option value="">Select Weight Class</option>
                                                     @foreach (\App\Attribute::all() as $key => $attribute)
            											<option value="{{ $attribute->id }}">{{ $attribute->name }}</option>
                                                    @endforeach
                                                    
            			                        </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="mb-3">
        								<p>{{ translate('Choose the attributes of this product and then input values of each attribute') }}</p>
        							</div>
                                    <div id="customer_choice_options">

                                    </div>
                                    {{-- <div class="row">
                                        <div class="col-2">
        									<button type="button" class="btn btn-info" onclick="add_more_customer_choice_option()">{{  translate('Add More Customer Choice Option') }}</button>
        								</div>
                                    </div> --}}
                                </div>
                            </div>
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{ translate('Images')}}
                                </div>
                                <div class="form-box-content p-3">
                                    
                                    <div class="row">
                                        <div class="col-12" id="color_wise_image">

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Thumbnail Image')}} <small>(200x280 px)</small> <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="file" name="thumbnail_img" id="file-2" class="custom-input-file custom-input-file--4 imgpx_check" data-multiple-caption="{count} files selected" accept="image/*" />
                                            <label for="file-2" class="mw-100 mb-3">
                                                <span></span>
                                                <strong>
                                                    <i class="fa fa-upload"></i>
                                                    {{ translate('Choose image')}}
                                                </strong>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Featured')}} <small>(200x280 px)</small></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="file" name="featured_img" id="file-3" class="custom-input-file custom-input-file--4 imgpx_check" data-multiple-caption="{count} files selected" accept="image/*" />
                                            <label for="file-3" class="mw-100 mb-3">
                                                <span></span>
                                                <strong>
                                                    <i class="fa fa-upload"></i>
                                                    {{ translate('Choose image')}}
                                                </strong>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Flash Deal')}} <small>(200x280 px)</small></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="file" name="flash_deal_img" id="file-4" class="custom-input-file custom-input-file--4 imgpx_check" data-multiple-caption="{count} files selected" accept="image/*" />
                                            <label for="file-4" class="mw-100 mb-3">
                                                <span></span>
                                                <strong>
                                                    <i class="fa fa-upload"></i>
                                                    {{ translate('Choose image')}}
                                                </strong>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{ translate('Videos')}}
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Video From')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="mb-3">
                                                <select class="form-control selectpicker" data-minimum-results-for-search="Infinity" name="video_provider">
                                                    <option value="youtube">{{ translate('Youtube')}}</option>
            										<option value="dailymotion">{{ translate('Dailymotion')}}</option>
            										<option value="vimeo">{{ translate('Vimeo')}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Video Link')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="video_link" placeholder="{{ translate('Video Link')}}">
                                            <span Style="font-weight: bold; font-size:15px;">Ex:- YouTube.com/product_video_Url</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{ translate('Meta Tags')}}
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Meta Title')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" name="meta_title" class="form-control mb-3" placeholder="{{ translate('Meta Title')}}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Description')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <textarea name="meta_description" rows="8" class="form-control mb-3"></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Meta Image')}} </label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="file" name="meta_img" id="file-5" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" accept="image/*" />
                                            <label for="file-5" class="mw-100 mb-3">
                                                <span></span>
                                                <strong>
                                                    <i class="fa fa-upload"></i>
                                                    {{ translate('Choose image')}}
                                                </strong>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{ translate('Price') }}
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('M.R.P') }} <span class="required-star">*</span>(inclusive of all taxes) </label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="number" min="0" value="0" step="0.01" class="form-control mb-3" name="unit_price" placeholder="{{ translate('M.R.P') }} " required>
                                        </div>
                                    </div>
                                    <!-- <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Purchase Price') }} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="number" min="0" value="0" step="0.01" class="form-control mb-3" name="purchase_price" placeholder="{{ translate('Purchase Price')}}" required>
                                        </div>
                                    </div> -->
                                    <div class="row">
                                        <div class="col-md-2">
                                        <label>{{translate('Hsn No')}} <span class="required-star">*</span></label>
                                        
                                        </div>
                                        <div class="col-md-10">
                                        <select class="form-control selectpicker" name="SubCategoryHsn_id" id="SubCategoryHsn_id" required>

                                        </select>
                                        </div>
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-md-2">
                                            <label>{{ translate('GST')}}<span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                        <select class="form-control selectpicker" name="tax" id="tax" required>

                                        </select>
                                        </div>
                                        
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-md-2">
                                            <label>{{ translate('Discount')}}</label>
                                        </div>
                                        <div class="col-8">
                                            <input type="number" min="0" value="0" step="0.01" class="form-control mb-3" name="discount" placeholder="{{ translate('Discount') }}" required>
                                        </div>
                                        <div class="col-4 col-md-2">
                                            <div class="mb-3">
                                                <select class="form-control selectpicker" name="discount_type" data-minimum-results-for-search="Infinity">
                                                    <option value="amount">₹</option>
                                                    <option value="percent">%</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="quantity">
                                        <div class="col-md-2">
                                            <label>{{ translate('Quantity') }} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="number" min="0" value="0" step="1" class="form-control mb-3" name="current_stock" placeholder="{{ translate('Quantity') }}" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12" id="sku_combination">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if (\App\BusinessSetting::where('type', 'shipping_type')->first()->value == 'product_wise_shipping')
                                <div class="form-box bg-white mt-4">
                                    <div class="form-box-title px-3 py-2">
                                        {{ translate('Shipping') }}
                                    </div>
                                    <div class="form-box-content p-3">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>{{ translate('Flat Rate') }}</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="number" min="0" step="0.01" value="0" class="form-control mb-3" name="flat_shipping_cost" placeholder="{{ translate('Flat Rate Cost') }}">
                                            </div>
                                            <div class="col-md-2">
                                                <label class="switch" style="margin-top:5px;">
                                                    <input type="radio" name="shipping_type" value="flat_rate" checked>
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>{{ translate('Free Shipping') }}</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="number" min="0" step="0.01" value="0" class="form-control mb-3" name="free_shipping_cost" value="0" disabled placeholder="{{ translate('Flat Rate Cost') }}">
                                            </div>
                                            <div class="col-md-2">
                                                <label class="switch" style="margin-top:5px;">
                                                    <input type="radio" name="shipping_type" value="free" checked>
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{ translate('Description')}}
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Description')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="mb-3">
                                                <textarea class="" name="description_product_editor"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{ translate('Specification')}}
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Specification')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="mb-3">
                                                <textarea class="" name="specification_product"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{ translate('Size Chart PDF')}}
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Size Chart PDF')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="file" accept=".pdf" name="pdf" id="file-6" class="custom-input-file custom-input-file--4 pdf_type" data-multiple-caption="{count} files selected" accept="pdf/*" />
                                            <label for="file-6" class="mw-100 mb-3">
                                                <span></span>
                                                <strong>
                                                    <i class="fa fa-upload"></i>
                                                    {{ translate('Choose PDF')}}
                                                </strong>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-box mt-4 text-right">
                                <button type="submit" class="btn btn-styled btn-base-1">{{  translate('Save') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>
            </div>
        </div>
    <!-- Modal -->
    <div class="modal fade" id="categorySelectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">{{ translate('Select Category') }}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="target-category heading-6">
                        <span class="mr-3">{{ translate('Target Category')}}:</span>
                        <span>{{ translate('category')}} > {{ translate('subcategory')}} > {{ translate('subsubcategory')}}</span>
                    </div>
                    <div class="row no-gutters modal-categories mt-4 mb-2">
                        <div class="col-4">
                            <div class="modal-category-box c-scrollbar">
                                <div class="sort-by-box">
                                    <form role="form" class="search-widget">
                                        <input class="form-control input-lg" type="text" placeholder="{{ translate('Search Category') }}" onkeyup="filterListItems(this, 'categories')">
                                        <button type="button" class="btn-inner">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </form>
                                </div>
                                <div class="modal-category-list has-right-arrow">
                                    <ul id="categories">
                                        @foreach ($categories as $key => $category)
                                            <li onclick="get_subcategories_by_category(this, {{ $category->id }})">{{  __($category->name) }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="modal-category-box c-scrollbar" id="subcategory_list">
                                <div class="sort-by-box">
                                    <form role="form" class="search-widget">
                                        <input class="form-control input-lg" type="text" placeholder="{{ translate('Search SubCategory') }}" onkeyup="filterListItems(this, 'subcategories')">
                                        <button type="button" class="btn-inner">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </form>
                                </div>
                                <div class="modal-category-list has-right-arrow">
                                    <ul id="subcategories">

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="modal-category-box c-scrollbar" id="subsubcategory_list">
                                <div class="sort-by-box">
                                    <form role="form" class="search-widget">
                                        <input class="form-control input-lg" type="text" placeholder="{{ translate('Search SubSubCategory') }}" onkeyup="filterListItems(this, 'subsubcategories')">
                                        <button type="button" class="btn-inner">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </form>
                                </div>
                                <div class="modal-category-list">
                                    <ul id="subsubcategories">

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ translate('cancel')}}</button>
                    <button type="button" class="btn btn-primary" onclick="closeModal()">{{ translate('Confirm')}}</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

    <script type="text/javascript">
  
 $(document).ready(function(){
    //$("#publishing_year" ).datepicker();
     update_image();
    $('.pdf_type').bind('change', function() {
    $('.label-important').remove(); 
var allowedExtensions =  
          /(\.pdf)$/i; 
    
  if (!allowedExtensions.exec(this.value)) { 
    $('.label-important').remove(); 
  $(this).after("<span class='label label-important' style='margin-left: 5%; color:#e63b05;font-weight: 600;font-size: 12px;'>Invalid file type Only .PDF Allowed</span>");
      this.value = ''; 
      return false; 
  }
  if(this.files[0].size/1024/1024 > 5){
    $('.label-important').remove(); 
    $(this).after("<span class='label label-important' style='margin-left: 5%;color: #e63b05;font-weight: 600;font-size: 12px;'>File size must be less than 5 mb</span>");
      this.value = ''; 
      return false; 
    }  
});
var editor1 = CKEDITOR.replace('description_product_editor', {
      extraAllowedContent: 'div(*);h3(*);img(*);h1(*);button(*);h2(*);h4(*);ul(*);a(*);section(*);li(*);span(*);p(*)',
      height: 400,
  {{--  filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
    //   filebrowserUploadMethod: 'form', --}}
    });
    editor1.on('instanceReady', function() {
      // Output self-closing tags the HTML4 way, like <br>.
      this.dataProcessor.writer.selfClosingEnd = '>';

      // Use line breaks for block elements, tables, and lists.
      var dtd = CKEDITOR.dtd;
      for (var e in CKEDITOR.tools.extend({}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent)) {
        this.dataProcessor.writer.setRules(e, {
          indent: true,
          breakBeforeOpen: true,
          breakAfterOpen: true,
          breakBeforeClose: true,
          breakAfterClose: true
        });
      }
      // Start in source mode.
      //this.setMode('source');
    });

    var editor2 = CKEDITOR.replace('specification_product', {
      extraAllowedContent: 'div(*);h3(*);img(*);h1(*);button(*);h2(*);h4(*);ul(*);a(*);section(*);li(*);span(*);p(*)',
      height: 400,
  {{--  filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
    //   filebrowserUploadMethod: 'form', --}}
    });
    editor2.on('instanceReady', function() {
      // Output self-closing tags the HTML4 way, like <br>.
      this.dataProcessor.writer.selfClosingEnd = '>';

      // Use line breaks for block elements, tables, and lists.
      var dtd = CKEDITOR.dtd;
      for (var e in CKEDITOR.tools.extend({}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent)) {
        this.dataProcessor.writer.setRules(e, {
          indent: true,
          breakBeforeOpen: true,
          breakAfterOpen: true,
          breakBeforeClose: true,
          breakAfterClose: true
        });
      }
      // Start in source mode.
      //this.setMode('source');
    });
    
var _URL = window.URL || window.webkitURL;

$('#file-2').change(function () {
//  alert('hi');
 var file = $(this)[0].files[0];
var fil_det=$(this);
 img = new Image();
 var imgwidth = 0;
 var imgheight = 0;
 var maxwidth = 200;
 var maxheight = 280;
 var isvalid_image=true;
 img.src = _URL.createObjectURL(file);
 img.onload = function(){
  
  imgwidth = this.width;
  imgheight = this.height;

  if(imgwidth != maxwidth && imgheight != maxheight){
    $('#file-2').val("");
    $('#file-2').next('label').find('span').html("");
  var msg= "Image size must be "+maxwidth+"X"+maxheight;
  showAlert("error",msg);
 }
};
img.onerror = function() {
$(this).val("");
  $(this).next('label').find('span').html("");
  var msg="not a valid file: " + file.type;
  showAlert("error",msg);
}
});
$('#file-3').change(function () {
//  alert('hi');
 var file = $(this)[0].files[0];
var fil_det=$(this);
 img = new Image();
 var imgwidth = 0;
 var imgheight = 0;
 var maxwidth = 200;
 var maxheight = 280;
 var isvalid_image=true;
 img.src = _URL.createObjectURL(file);
 img.onload = function(){
  
  imgwidth = this.width;
  imgheight = this.height;

  if(imgwidth != maxwidth && imgheight != maxheight){
    $('#file-3').val("");
    $('#file-3').next('label').find('span').html("");
  var msg= "Image size must be "+maxwidth+"X"+maxheight;
  showAlert("error",msg);
 }
};
img.onerror = function() {
$(this).val("");
  $(this).next('label').find('span').html("");
  var msg="not a valid file: " + file.type;
  showAlert("error",msg);
}
});
$('#file-4').change(function () {
//  alert('hi');
 var file = $(this)[0].files[0];
var fil_det=$(this);
 img = new Image();
 var imgwidth = 0;
 var imgheight = 0;
 var maxwidth = 200;
 var maxheight = 280;
 var isvalid_image=true;
 img.src = _URL.createObjectURL(file);
 img.onload = function(){
  
  imgwidth = this.width;
  imgheight = this.height;

  if(imgwidth != maxwidth && imgheight != maxheight){
    $('#file-4').val("");
    $('#file-4').next('label').find('span').html("");
  var msg= "Image size must be "+maxwidth+"X"+maxheight;
  showAlert("error",msg);
 }
};
img.onerror = function() {
$(this).val("");
  $(this).next('label').find('span').html("");
  var msg="not a valid file: " + file.type;
  showAlert("error",msg);
}
});
});
        var category_name = "";
        var subcategory_name = "";
        var subsubcategory_name = "";

        var category_id = null;
        var subcategory_id = null;
        var subsubcategory_id = null;

        $(document).ready(function(){
            $('#subcategory_list').hide();
            $('#subsubcategory_list').hide();
        });

        function list_item_highlight(el){
            $(el).parent().children().each(function(){
                $(this).removeClass('selected');
            });
            $(el).addClass('selected');
        }

        function get_subcategories_by_category(el, cat_id){
            list_item_highlight(el);
            category_id = cat_id;
            subcategory_id = null;
            subsubcategory_id = null;
            category_name = $(el).html();
            $('#subcategories').html(null);
            $('#subsubcategory_list').hide();
            $.post('{{ route('subcategories.get_subcategories_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){
                for (var i = 0; i < data.length; i++) {
                    $('#subcategories').append('<li onclick="get_subsubcategories_by_subcategory(this, '+data[i].id+')">'+data[i].name+'</li>');
                }
                $('#subcategory_list').show();
            });
        }

        function get_subsubcategories_by_subcategory(el, subcat_id){
            list_item_highlight(el);
            subcategory_id = subcat_id;
            subsubcategory_id = null;
            subcategory_name = $(el).html();
            $('#subsubcategories').html(null);
            $.post('{{ route('subsubcategories.get_subsubcategories_by_subcategory') }}',{_token:'{{ csrf_token() }}', subcategory_id:subcategory_id}, function(data){
                for (var i = 0; i < data.length; i++) {
                    $('#subsubcategories').append('<li onclick="confirm_subsubcategory(this, '+data[i].id+')">'+data[i].name+'</li>');
                }
                $('#subsubcategory_list').show();
            });
        }

        function confirm_subsubcategory(el, subsubcat_id){
            list_item_highlight(el);
            subsubcategory_id = subsubcat_id;
            subsubcategory_name = $(el).html();
    	}

        function get_attributes_by_subsubcategory(subsubcategory_id){
    		$.post('{{ route('subsubcategories.get_attributes_by_subsubcategory') }}',{_token:'{{ csrf_token() }}', subsubcategory_id:subsubcategory_id}, function(data){
    		    $('#choice_attributes').html(null);
    		    for (var i = 0; i < data.length; i++) {
    		        $('#choice_attributes').append($('<option>', {
    		            value: data[i].id,
    		            text: data[i].name
    		        }));
    		    }
    		});
    	}

        function filterListItems(el, list){
            filter = el.value.toUpperCase();
            li = $('#'+list).children();
            for (i = 0; i < li.length; i++) {
                if ($(li[i]).html().toUpperCase().indexOf(filter) > -1) {
                    $(li[i]).show();
                } else {
                    $(li[i]).hide();
                }
            }
        }

        function closeModal(){
            if(category_id > 0 && subcategory_id > 0){
                $('#category_id').val(category_id);
                $('#subcategory_id').val(subcategory_id);
                $('#subsubcategory_id').val(subsubcategory_id);
                $('#product_category').html(category_name+'>'+subcategory_name+'>'+subsubcategory_name);
                $('#categorySelectModal').modal('hide');
                $(".modal-backdrop").hide();
                get_hsn_gst_by_subcategory_id(subcategory_id);
                get_attributes(subcategory_id);
            }
            else{
                alert('Please choose categories...');
                // console.log(category_id);
                // console.log(subcategory_id);
                // console.log(subsubcategory_id);
            }
        }

        //var i = 0;
        function add_more_customer_choice_option(i, name){
    		$('#customer_choice_options').append('<div class="row mb-3"><div class="col-8 col-md-3 order-1 order-md-0"><input type="hidden" name="choice_no[]" value="'+i+'"><input type="text" class="form-control" name="choice[]" value="'+name+'" placeholder="{{ translate('Choice Title') }}" readonly></div><div class="col-12 col-md-7 col-xl-8 order-3 order-md-0 mt-2 mt-md-0"><input type="text" class="form-control tagsInput" name="choice_options_'+i+'[]" placeholder="{{ translate('Type and hit enter') }}" onchange="update_sku()"></div><div class="col-4 col-xl-1 col-md-2 order-2 order-md-0 text-right"><button type="button" onclick="delete_row(this)" class="btn btn-link btn-icon text-danger"><i class="fa fa-trash-o"></i></button></div></div>');
    		// i++;
            $('.tagsInput').tagsinput('items');
    	}

    	$('input[name="colors_active"]').on('change', function() {
    	    if(!$('input[name="colors_active"]').is(':checked')){
    			$('#colors').prop('disabled', true);
    		}
    		else{
    			$('#colors').prop('disabled', false);
    		}
            update_sku();
            update_image();
    	});

    	$('#colors').on('change', function() {
            update_sku();
            update_image();
    	});

    	$('input[name="unit_price"]').on('keyup', function() {
    	    update_sku();
    	});

        $('input[name="name"]').on('keyup', function() {
    	    update_sku();
    	});

        $('#choice_attributes').on('change', function() {
    		$('#customer_choice_options').html(null);
    		$.each($("#choice_attributes option:selected"), function(){
                add_more_customer_choice_option($(this).val(), $(this).text());
            });
    		update_sku();
    	});

    	function delete_row(em){
    		$(em).closest('.row').remove();
    		update_sku();
    	}

    	function update_sku(){
            $.ajax({
    		   type:"POST",
    		   url:'{{ route('products.sku_combination') }}',
    		   data:$('#choice_form').serialize(),
    		   success: function(data){
    			   $('#sku_combination').html(data);
    			   if (data.length > 1) {
    				   $('#quantity').hide();
    			   }
    			   else {
    			   		$('#quantity').show();
    			   }
    		   }
    	   });
        }
        
        function update_image(){
            $.ajax({
    		   type:"POST",
    		   url:'{{ route('products.color_wise_image') }}',
    		   data:$('#choice_form').serialize(),
    		   success: function(data){
    			   $('#color_wise_image').html(data);
    			   
    		   }
    	   });
    	}

        var photo_id = 2;
        function add_more_slider_image1(){
            let imagefileCount=$('#imagefileCount').val();

            var count = $("#product-images").children().length;
            if(count>5){
               
            
                Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'You can not add more than 5 images',
            showConfirmButton: false,
            timer: 1500
            });
            return false;
            }
         
            var photoAdd =  '<div class="row">';
            photoAdd +=  '<div class="col-2">';
            photoAdd +=  '<button type="button" onclick="delete_this_row1(this)" class="btn btn-link btn-icon text-danger"><i class="fa fa-trash-o"></i></button>';
            photoAdd +=  '</div>';
            photoAdd +=  '<div class="col-10">';
            photoAdd +=  '<input type="file" name="photos[]" id="photos-'+photo_id+'" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" multiple accept="image/*" />';
            photoAdd +=  '<label for="photos-'+photo_id+'" class="mw-100 mb-3">';
            photoAdd +=  '<span></span>';
            photoAdd +=  '<strong>';
            photoAdd +=  '<i class="fa fa-upload"></i>';
            photoAdd +=  "{{ translate('Choose image')}}";
            photoAdd +=  '</strong>';
            photoAdd +=  '</label>';
            photoAdd +=  '</div>';
            photoAdd +=  '</div>';
            $('#product-images').append(photoAdd);
            imagefileCount=parseInt(imagefileCount)+parseInt(1);  
            photo_id++;
            $('#imagefileCount').val(imagefileCount);
            imageInputInitialize();
        }
        function delete_this_row1(em){
            let imagefileCount=$('#imagefileCount').val();
            let removecount=imagefileCount-1;
            $('#imagefileCount').val(removecount);
            $(em).closest('.row').remove();
        }
        function add_more_slider_image(color_id){
            let imagefileCount=$('#imagefileCount'+color_id).val();

            var count = $(".productmain"+color_id).children().length;
            if(count>5){
               
            
                Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'You can not add more than 5 images',
            showConfirmButton: false,
            timer: 1500
            });
            return false;
            }
         
            var photoAdd =  '<div class="row">';
            photoAdd +=  '<div class="col-2">';
            photoAdd +=  '<button type="button" onclick="delete_this_row(this,'+color_id+')" class="btn btn-link btn-icon text-danger"><i class="fa fa-trash-o"></i></button>';
            photoAdd +=  '</div>';
            photoAdd +=  '<div class="col-10">';
            photoAdd +=  '<input type="file" name="photos'+color_id+'[]" id="photos-'+photo_id+'-'+color_id+'" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" multiple accept="image/*" />';
            photoAdd +=  '<label for="photos-'+photo_id+'-'+color_id+'" class="mw-100 mb-3">';
            photoAdd +=  '<span></span>';
            photoAdd +=  '<strong>';
            photoAdd +=  '<i class="fa fa-upload"></i>';
            photoAdd +=  "{{ translate('Choose image')}}";
            photoAdd +=  '</strong>';
            photoAdd +=  '</label>';
            photoAdd +=  '</div>';
            photoAdd +=  '</div>';
            $(".productmain"+color_id).append(photoAdd);
            imagefileCount=parseInt(imagefileCount)+parseInt(1);  
            photo_id++;
            $('#imagefileCount'+color_id).val(imagefileCount);
            imageInputInitialize();
        }
        function delete_this_row(em,color_id){
            let imagefileCount=$('#imagefileCount'+color_id).val();
            let removecount=imagefileCount-1;
            $('#imagefileCount'+color_id).val(removecount);
            $(em).closest('.row').remove();
        }
        $("#subcategory_id").on('change', function() {
		var subcategory_id=$("#subcategory_id").val();
	    get_hsn_gst_by_subcategory_id(subcategory_id);
	});
	$("#SubCategoryHsn_id").on('change', function() {
		var SubCategoryHsn_id=$("#SubCategoryHsn_id").val();
	    get_gst_by_hsn_id(SubCategoryHsn_id);
	});
        function get_hsn_gst_by_subcategory_id(subcategory_id){ //alert(subsubcategory_id);
		
		$.post('{{ route('subcategories.get_subcategories_by_SubCategoryHsn') }}',{_token:'{{ csrf_token() }}', subcategory_id:subcategory_id}, function(data){
			$('#SubCategoryHsn_id').html('<option value="">Select HSN</option>');
		    for (var i = 0; i < data.length; i++) {
		        $('#SubCategoryHsn_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].hns_no
		        }));
		        $('.demo-select2').select2();
		    }
		});

	}
function get_attributes(subcategory_id){
    $.post('{{ route('subcategories.get_attributes') }}',{_token:'{{ csrf_token() }}', subcategory_id:subcategory_id}, function(data){
			$('#choice_attributes').html('<option value="">Select Attribute</option>');
		    for (var i = 0; i < data.length; i++) {
                if(data[i].mandatory==1){
                    var mandatory="*";
                }else{
                    var mandatory="";
                }
		        $('#choice_attributes').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name+mandatory
		        }));
		        $('.demo-select2').select2();
		    }
		});
}
	function get_gst_by_hsn_id(SubCategoryHsn_id){

		$.post('{{ route('subcategories.get_gst_by_hsn_id') }}',{_token:'{{ csrf_token() }}', has_id:SubCategoryHsn_id}, function(data){
            $('#tax').html('<option value="">Select GST</option>');
		    for (var i = 0; i < data.length; i++) {
		        $('#tax').append($('<option>', {
		            value: data[i].gst_amount,
		            text: data[i].gst_amount
		        }));
		        $('.demo-select2').select2();
		    }
			
			
		});
    }
    
    function addbrand(){
let brands=$('#brands').val();
if(brands=="add"){
    $('#brand_div').show();
}
else{
    $('#brand_div').hide();
    $('#add_brand').val('');
}
    }  
    </script>
@endsection
