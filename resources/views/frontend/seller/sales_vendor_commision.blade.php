@extends('frontend.seller.layouts.seller_app')

@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                
                <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                    
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ translate('Home')}}</a></li>
                    {{-- <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">{{ translate('Dashboard')}}</a></li> --}}
                    <li  class="breadcrumb-item active"><a href="javascript:">{{ translate('Commision Report')}}</a></li>
                </ol>
                </div>
              </div>
            </div>
          </div>
          
        </div>
     <div class="col-md-12 content-body">
    <section class="gry-bg profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                
                <div class="col-lg-12">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="pull-right clearfix">
                            <form class="" id="sort_sellers" action="{{route('sales_vendor_commision')}}" method="GET">
                                @csrf
                                <div class="box-inline pad-rgt pull-left">
                                    <div class="box-inline pad-rgt pull-left">
                                        <div class="" style="min-width: 200px;">
                                            <input type="text" autocomplete="off"  value="<?php echo isset($_GET['from_date'])?$_GET['from_date']:'' ?>" class="form-control" id="from_date" name="from_date"
                                         
                                              
                                            
                                            
                                            placeholder="{{ translate('From Date') }}">
                                           
                                        </div>
                                    </div>
                                    <div class="box-inline pad-rgt pull-left">
                                        <div class="" style="min-width: 200px;">
                                            <input type="text" autocomplete="off" class="form-control" id="to_date" name="to_date"
                                            value="<?php echo isset($_GET['to_date'])?$_GET['to_date']:'' ?>"
                                            
                                            placeholder="{{ translate('To Date') }}">
                                            
                                        </div>
                                    </div>
                                </div>
                             

                                <div class="box-inline pad-rgt pull-left">
                                    <div class="" style="min-width: 200px;margin-left:10px;">
                                        <button class="btn btn-primary mr-1 mb-1" type="submit">{{ translate('Filter') }}</button>
                                    <button class="btn btn-primary mr-1 mb-1" type="button" onclick="excelReport();">{{ translate('Export Excel') }}</button>
                                    <button class="btn btn-primary mr-1 mb-1" type="button"  onclick="window.location.href='{{route('sales_vendor_commision')}}'">{{ translate('Reset') }}</button>
                                </div>
                                </div>
                            </form>
                        </div>
                        
                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                        <th>{{ translate('Sl#') }}</th>
                                        <th>{{ translate('Date') }}</th>
                                        <th>{{ translate('Invoice No') }}</th>
                                         <th>{{ translate('Sale  (Wth Tax)') }}</th>
                                         <th>{{ translate('Return Amount') }}</th>
                                          <th>{{ translate('Commission Charges') }}</th>
                                          <th>{{ translate('Courier Charge') }}</th>
                                          <th>{{ translate('TCS') }}</th>
                                          <th>{{ translate('TDS') }}</th>
                                          <th>{{ translate('Payment Handling Charge') }}</th>
                                         <th>{{ translate('Amount Payable') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($data))
                                    @foreach ($data as $key => $sales_budget)
                                   
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{ $sales_budget['created_date'] }}</td>
                                            <td>{{ $sales_budget['invoice'] }}</td>
                                            <td>{{ $sales_budget['sale_with_tax'] }}</td>
                                            <td>{{ $sales_budget['return_amount'] }}</td>
                                            <td>{{ $sales_budget['payment_commision'] }}</td>
                                            <td>{{ $sales_budget['curior_charge'] }}</td>
                                            <td>{{ $sales_budget['tcs'] }}</td>
                                            <td>{{ $sales_budget['tds'] }}</td>
                                            <td>{{ $sales_budget['payment_handling'] }}</td>
                                            <td>{{ $sales_budget['amount_pay'] }}</td>
                                           
                                        </tr>
                                
                                    @endforeach
                                    @endif
                                </tbody>
                              <tfoot>
                                    <tr>
                                    <th>{{ translate('Sl#') }}</th>
                                        <th>{{ translate('Date') }}</th>
                                        <th>{{ translate('Invoice No') }}</th>
                                         <th>{{ translate('Sale  (Wth Tax)') }}</th>
                                         <th>{{ translate('Return Amount') }}</th>
                                          <th>{{ translate('Commission Charges') }}</th>
                                          <th>{{ translate('Courier Charge') }}</th>
                                          <th>{{ translate('TCS') }}</th>
                                          <th>{{ translate('TDS') }}</th>
                                          <th>{{ translate('Payment Handling Charge') }}</th>
                                         <th>{{ translate('Amount Payable') }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                           
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>
            </div>
          </div>
@endsection

@section('script')

<script>
 $("#to_date" ).datepicker();
//   $('#from_date').datepicker({
//               // startDate: '-0d',
//                 todayBtn: "linked",
//                 autoclose: true,
//                 todayHighlight: true,
//                 format: 'dd/mm/yyyy'
//         	});
//             $('#to_date').datepicker({
//               //  startDate: '-0d',
//                 todayBtn: "linked",
//                 autoclose: true,
//                 todayHighlight: true,
//                 format: 'dd/mm/yyyy'
//         	});
            

            function excelReport(){
                var from_date=$('#from_date').val();
                var to_date=$('#to_date').val();
                var token = $("input[name='_token']").val();
        // var fd = new FormData();
        // fd.append('_token', token);
        // fd.append('from_date', from_date);
        // fd.append('to_date', to_date);
        var data={from_date:from_date,to_date:to_date,_token:token};
        redirectPost("{{route('commision.export')}}",data); 

               
            }

            var redirectPost = function (url, data = null, method = 'post') {
                var form = document.createElement('form');
                form.method = method;
                form.action = url;
                for (var name in data) {
                    var input = document.createElement('input');
                    input.type = 'hidden';
                    input.name = name;
                    input.value = data[name];
                    form.appendChild(input);
                }
                $('body').append(form);
                form.submit();
            };
</script>
@endsection
