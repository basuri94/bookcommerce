@extends('frontend.seller.layouts.seller_app')

@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <h2 class="content-header-title float-left mb-0"> {{ translate('Bulk Products upload')}}</h2>
                <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                    
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ translate('Home')}}</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">{{ translate('Dashboard')}}</a></li>
                    <li class="breadcrumb-item"><a href="#">{{ translate('Bulk Products upload')}}</a></li>
                </ol>
                </div>
              </div>
            </div>
          </div>
          
        </div>
     <div class="col-md-12 content-body">
    <section class="gry-bg profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                
                <div class="col-lg-12">
                    <div class="main-content">
                        <!-- Page title -->
                        
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-content p-3">
                                    <table class="table mb-0 table-bordered" style="font-size:14px;background-color: #cce5ff;border-color: #b8daff">
                                        <tr>
                                            <td>{{ translate('1. Download the skeleton file and fill it with data.')}}:</td>
                                        </tr>
                                        <tr >
                                            <td>{{ translate('2. You can download the example file to understand how the data must be filled.')}}:</td>
                                        </tr>
                                        <tr>
                                            <td>{{ translate('3. Once you have downloaded and filled the skeleton file, upload it in the form below and submit.')}}:</td>
                                        </tr>
                                        <tr>
                                            <td>{{ translate('4. After uploading products you need to edit them and set products images and choices.')}}</td>
                                        </tr>
                                    </table>
                                   
                                    <a href="{{ asset('download/product_bulk_demo.xlsx') }}" download> <button type="button" class="btn btn-success mr-1 mb-1 waves-effect waves-light">{{ translate('Download CSV') }}</button></a>
                                </div>
                            </div>

                            <div class="form-box bg-white mt-4">
                                <div class="form-box-content p-3">
                                    <table class="table mb-0 table-bordered" style="font-size:14px;background-color: #cce5ff;border-color: #b8daff">
                                        <tr>
                                            <td>{{ translate('1. Category,Sub category,Sub Sub category and Brand should be in numerical ids.')}}:</td>
                                        </tr>
                                        <tr >
                                            <td>{{ translate('2. You can download the pdf to get Category,Sub category,Sub Sub category and Brand id.')}}:</td>
                                        </tr>
                                    </table>
                                    <a href="{{ route('pdf.download_category') }}"><button class="btn btn-success mr-1 mb-1 waves-effect waves-light">{{ translate('Download Category')}}</button></a>
                                    <a href="{{ route('pdf.download_sub_category') }}"><button class="btn btn-success mr-1 mb-1 waves-effect waves-light">{{ translate('Download Sub category')}}</button></a>
                                    <a href="{{ route('pdf.download_sub_sub_category') }}"><button class="btn btn-success mr-1 mb-1 waves-effect waves-light">{{ translate('Download Sub Sub category')}}</button></a>
                                    <a href="{{ route('pdf.download_brand') }}"><button class="btn btn-success mr-1 mb-1 waves-effect waves-light">{{ translate('Download Brand')}}</button></a>
                                </div>
                            </div>

                            <form class="form-horizontal" action="{{ route('bulk_product_upload') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-box bg-white mt-4">
                                    <div class="form-box-title px-3 py-2">
                                        {{ translate('Upload CSV File')}}
                                    </div>


                                    <div class="col-lg-6 col-md-12">
                                        <div class="custom-file">
                                            <input type="file" name="bulk_file" id="file-6" class="custom-file-input">
                                            <label class="custom-file-label" for="file-6"> {{ translate('Choose CSV File')}}</label>
                                          </div>
                                    </div>
                                    {{-- <div class="form-box-content p-3">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>{{ translate('CSV')}}</label>
                                            </div>
                                            <div class="col-md-10">
                                                <input type="file" name="bulk_file" id="file-6" class="custom-input-file custom-input-file--4"/>
                                                <label for="file-6" class="mw-100 mb-3">
                                                    <span></span>
                                                    <strong>
                                                        <i class="fa fa-upload"></i>
                                                        {{ translate('Choose CSV File')}}
                                                    </strong>
                                                </label>
                                            </div>
                                        </div>
                                    </div> --}}
                                </div>
                                <div class="form-box mt-4 text-right">
                                    <button type="submit" class="btn btn-success mr-1 mb-1 waves-effect waves-light">{{ translate('Upload') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>
            </div>
          </div>
@endsection
