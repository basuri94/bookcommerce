@extends('frontend.seller.layouts.seller_app')

@section('content')
<style>
    .modal-dialog{
        min-width: 1200px !important;
    }
    .process-steps li .icon {
    height: 40px;
    width: 40px;
    margin: auto;
    background: #fff;
    border-radius: 50%;
    line-height: 20px;
    font-size: 14px;
    font-weight: 700;
    padding: 10px;
    color: #400b0b;
    position: relative;
}
.process-steps li.active .icon {
    background-color: #5060a9;
    padding: 10px;
}
.process-steps li.done:after{
    background: #30c100 !important;
}
    .process-steps li + li:after {
    position: absolute;
    content: "";
    height: 10px;
    width: calc(100% - 40px);
    background: #fff;
    top: 14px;
    z-index: 0;
    right: calc(50% + 20px);
}
.process-steps li.done .icon:before {
    position: absolute;
    content: "";
    left: 16px;
    top: 10px;
    width: 8px;
    height: 14px;
    border: solid #fff;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}
.process-steps li.done .icon {
    color: #30c100;
    background: #30c100;
    padiing: 10px !important;
}
</style>
<!-- BEGIN: Content-->
<div class="app-content content">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <h2 class="content-header-title float-left mb-0">  {{ translate('Orders')}}</h2>
                <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                    
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ translate('Home')}}</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">{{ translate('Dashboard')}}</a></li>
                    <li  class="breadcrumb-item active"><a href="{{ route('orders.index') }}">{{ translate('Orders')}}</a></li>
                </ol>
                </div>
              </div>
            </div>
          </div>
          
        </div>
     <div class="col-md-12 content-body">
    <section class="gry-bg profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
               

                <div class="col-lg-12">
                    <div class="main-content">
                        <!-- Page title -->
                        

                        <!-- Order history table -->
                        <div class="card no-border mt-4">
                            <div class="card-header">
                                <form class="container" id="sort_orders" action="" method="GET">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" id="search" name="search" @isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="{{ translate('Type Order code & hit Enter') }}">
                                        </div>
                                        <div class="col-md-3 ml-auto">
                                            <select class="form-control mb-3 selectpicker" data-placeholder="{{ translate('Filter by Product Type')}}" name="product_type" onchange="sort_orders()">
                                                <option value="">{{ translate('Filter by Product Type')}}</option>
                                                <option value="0" @isset($product_type) @if($payment_status == '0') selected @endif @endisset>{{ translate('Physical')}}</option>
                                                <option value="1" @isset($product_type) @if($payment_status == '1') selected @endif @endisset>{{ translate('Digital')}}</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3 ml-auto">
                                            <select class="form-control mb-3 selectpicker" data-placeholder="{{ translate('Filter by Payment Status')}}" name="payment_status" onchange="sort_orders()">
                                                <option value="">{{ translate('Filter by Payment Status')}}</option>
                                                <option value="paid" @isset($payment_status) @if($payment_status == 'paid') selected @endif @endisset>{{ translate('Paid')}}</option>
                                                <option value="unpaid" @isset($payment_status) @if($payment_status == 'unpaid') selected @endif @endisset>{{ translate('Un-Paid')}}</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select class="form-control mb-3 selectpicker" data-placeholder="{{ translate('Filter by Delivery Status')}}" name="delivery_status" onchange="sort_orders()">
                                                <option value="">{{ translate('Filter by Delivery Status')}}</option>
                                                <option value="pending" @isset($delivery_status) @if($delivery_status == 'pending') selected @endif @endisset>{{ translate('Pending')}}</option>
                                                <option value="on_review" @isset($delivery_status) @if($delivery_status == 'on_review') selected @endif @endisset>{{ translate('On review')}}</option>
                                                <option value="on_delivery" @isset($delivery_status) @if($delivery_status == 'on_delivery') selected @endif @endisset>{{ translate('On delivery')}}</option>
                                                <option value="delivered" @isset($delivery_status) @if($delivery_status == 'delivered') selected @endif @endisset>{{ translate('Delivered')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="card-body">
                               
                                    <table class="table table-sm table-hover table-responsive-md">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th width="20%">{{ translate('Order Code')}}</th>
                                            <th>{{ translate('Num. of Products')}}</th>
                                            <th>{{ translate('Customer')}}</th>
                                            <th>{{ translate('Amount')}}</th>
                                            <th>{{ translate('Order Type')}}</th>
                                            <th>{{ translate('Delivery Status')}}</th>
                                            <th>{{ translate('Payment Status')}}</th>
                                            <th>{{ translate('Options')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($orders) > 0)
                                        @foreach ($orders as $key => $order_id)
                                            @php
                                                $order = \App\Order::find($order_id->id);
                                            @endphp
                                            @if($order != null)
                                                <tr>
                                                    <td>
                                                        {{ $key+1 }}
                                                    </td>
                                                    <td>
                                                        <a href="#{{ $order->code }}" onclick="show_order_details({{ $order->id }})">{{ $order->code }}</a>
                                                    </td>
                                                    <td>
                                                        {{ count($order->orderDetails->where('seller_id', Auth::user()->id)) }}
                                                    </td>
                                                    <td>
                                                        @if ($order->user_id != null)
                                                            {{ $order->user->name }}
                                                        @else
                                                            Guest ({{ $order->guest_id }})
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{ single_price($order->orderDetails->where('seller_id', Auth::user()->id)->sum('price') + $order->orderDetails->where('seller_id', Auth::user()->id)->sum('tax') + $order->orderDetails->where('seller_id', Auth::user()->id)->sum('shipping_cost'))  }}
                                                    </td>
                                                    <td>
                                                        @php
                                                            $status1 = $order->payment_type;
                                                        @endphp
                                                        {{ ucfirst(str_replace('_', ' ', $status1)) }}
                                                    </td>
                                                    <td>
                                                        @php
                                                            $status = $order->orderDetails->first()->delivery_status;
                                                        @endphp
                                                        {{ ucfirst(str_replace('_', ' ', $status)) }}
                                                    </td>
                                                    <td>
                                                        <span class="badge badge--2 mr-4">
                                                            @if ($order->orderDetails->where('seller_id', Auth::user()->id)->first()->payment_status == 'paid')
                                                                <i class="bg-green"></i> {{ translate('Paid')}}
                                                            @else
                                                                <i class="bg-red"></i> {{ $order->payment_status }}
                                                            @endif
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <div class="dropdown">
                                                            <button class="btn" type="button" id="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="fa fa-ellipsis-v"></i>
                                                            </button>

                                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="">
                                                                <button onclick="show_order_details({{ $order->id }})" class="dropdown-item">{{ translate('Order Details')}}</button>
                                                                    @if($order->payment_status!="cancelled")
                                                                <a href="{{ route('seller.invoice.download', $order->id) }}" class="dropdown-item">{{ translate('Download Invoice')}}</a>
                                                            @endif
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                        @else 
                                        <tr><td colspan="8">No Record Found</td></tr>
                                        @endif
                                    </tbody>
                                </table>
                              
                            </div>
                        </div>

                        <div class="pagination-wrapper py-4">
                            <ul class="pagination justify-content-end">
                                {{ $orders->links() }}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>
            </div>
          </div>

          <div class="modal" id="order_details">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Order Details</h4>    
                  <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> -->
                </div><div class="container"></div>
                <div class="modal-body" id="order-details-modal-body">
                 
                </div>
                <div class="modal-footer">
                  <a href="#" class="btn btn-styled btn-sm btn-base-1" data-dismiss="modal" class="btn">Close</a>
                  <!-- <a href="#" class="btn btn-primary">Save changes</a> -->
                </div>
              </div>
            </div>
        </div>
       


    {{-- <div class="modal fade" id="order_details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="c-preloader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
                <div id="order-details-modal-body">

                </div>
            </div>
        </div>
    </div> --}}

@endsection

@section('script')
    <script type="text/javascript">
        function sort_orders(el){
            $('#sort_orders').submit();
        }
    </script>
@endsection
