
    <!DOCTYPE html>
    <!--
    Template Name: Vuexy - Vuejs, HTML & Laravel frontend.seller Dashboard Template
    Author: PixInvent
    Website: http://www.pixinvent.com/
    Contact: hello@pixinvent.com
    Follow: www.twitter.com/pixinvents
    Like: www.facebook.com/pixinvents
    Purchase: https://1.envato.market/vuexy_frontend.seller
    Renew Support: https://1.envato.market/vuexy_frontend.seller
    License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
    
    -->
    <html class="loading" lang="en" data-textdirection="ltr">
      <!-- BEGIN: Head-->
      
    <!-- Mirrored from pixinvent.com/demo/vuexy-html-bootstrap-frontend.seller-template/html/ltr/vertical-menu-template/dashboard-ecommerce.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Aug 2020 19:44:13 GMT -->
    <head>
        @include('frontend.seller.includes.head')
    
    
        
    
    </head>
    <!-- END: Head-->
    
    <!-- BEGIN: Body-->
    
    <body class="vertical-layout vertical-menu-modern 2-columns  navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
        @include('frontend.seller.includes.header')
        @include('frontend.seller.includes.sidebar')
    <style>
        .container {
            border: 2px solid #dedede;
            background-color: #f1f1f1;
            border-radius: 5px;
            padding: 10px;
            margin: 10px 0;
        }

        .darker {
            border-color: #ccc;
            background-color: #ddd;
        }

        .container::after {
            content: "";
            clear: both;
            display: table;
        }

        .container img {
            float: left;
            max-width: 60px;
            width: 100%;
            margin-right: 20px;
            border-radius: 50%;
        }

        .container img.right {
            float: right;
            margin-left: 20px;
            margin-right: 0;
        }

        .time-right {
            float: right;
            color: #aaa;
        }

        .time-left {
            float: left;
            color: #999;
        }
        div.scroll {
  background-color: lightblue;
  width: 1000px;
  height: 180px;
  overflow: scroll;
}


    </style>
<style>
div.scroll {
    overflow-y: scroll;
    background-color: lightblue;
    width: 1000px;
    height: 300px;
    overflow-x: hidden;
}
* {
    font-family: Montserrat,Helvetica,Arial,serif; !important;
}
.heading {
    margin: 0 0 6px;
    padding: 0;
    text-transform: none;
    font-family: "Open Sans", sans-serif;
    font-weight: 600;
    color: #012747;
    line-height: 1.46;
}
.heading > a {
    color: #273b71;
}
.chat-app-form{
    margin-top: 10px;
}
</style>


    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-area-wrapper">

            <div class="content-right">
                <div class="content-wrapper">
                    <div class="content-header row">
                    </div>
                    <div class="content-body">



                        <section class="">

                            <div >
                                <div class="card no-border" style="padding: 5px 5px 2px 10px;">
                                     @php 
                                  $product_dta=\App\Product::find($conversation->product_id);
                                  @endphp
                                  
                                    <h2 class="heading heading-6 text-capitalize strong-600 mb-0 d-inline-block">
                                     @if($product_dta!="" && $product_dta!=null)
                                   <a href="{{ route('product', $product_dta->slug) }}" target="_blank">{{ $conversation->title }} </a>   
                                     
                                  
                                  @endif
                                       
                                    </h2>
                                    {{ translate('Between you and') }}
                                    @if ($conversation->sender_id == Auth::user()->id)
                                    {{ $conversation->receiver->name }}
                                    @else
                                    {{ $conversation->sender->name }}
                                    @endif
                                    
                                    @if ($conversation->sender_id == Auth::user()->id && $conversation->receiver->shop
                                    !=
                                    null)
                                    <a
                                        href="{{ route('shop.visit', $conversation->receiver->shop->slug) }}">{{ $conversation->receiver->shop->name }}</a>
                                    @endif
                                </div>

                            </div>
                            <div id="messages" class="scroll">
                              
                            </div>

                            <div class="chat-app-form">
                                <form class="chat-app-input d-flex" action="{{ route('messages.store') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="conversation_id" value="{{ $conversation->id }}">
                                  <input type="text" name="message" class="form-control message mr-1 ml-50" id="iconLeft4-1" placeholder="Type your message">
                                  <button type="submit" class="btn btn-primary send" ><i class="fa fa-paper-plane-o d-lg-none"></i> <span class="d-none d-lg-block">{{translate('Send')}}</span></button>
                                </form>
                              </div>
                            {{-- <div class="col-md-6">
                                <form  action="{{ route('messages.store') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="conversation_id" value="{{ $conversation->id }}">
                                    <div class="row">
                                       
                                            <textarea class="form-control" rows="4" name="message"
                                                placeholder="{{ translate('Type your reply') }}"
                                                required></textarea>
                                        
                                    </div>
                                    <div class="text-right">
                                        <button type="submit"
                                            class="btn btn-base-1 mt-3">{{ translate('Send') }}</button>
                                    </div>
                                </form>
                            
                            </div> --}}
                        </section>

                    </div>
                 
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->
    @include('partials.modal')

   

    @include('frontend.seller.includes.customizer')
    @include('frontend.seller.includes.footer')
    <script type="text/javascript">
    $(function(){
     
refresh_messages(); 
    });
        function refresh_messages(){
        $.post('{{ route('conversations.refresh') }}', {_token:'{{ @csrf_token() }}', id:'{{ encrypt($conversation->id) }}'}, function(data){
            $('#messages').html('');
            $('#messages').html(data);
        })
    }

  // This will run on page load
    setInterval(function(){
        refresh_messages() // this will run after every 5 seconds
    },15000);
    </script>
</body>
<!-- END: Body-->

<!-- Mirrored from pixinvent.com/demo/vuexy-html-bootstrap-admin-template/html/ltr/vertical-menu-template/dashboard-ecommerce.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Aug 2020 19:44:14 GMT -->

</html>