<!DOCTYPE html>
<!--
Template Name: Vuexy - Vuejs, HTML & Laravel frontend.seller Dashboard Template
Author: PixInvent
Website: http://www.pixinvent.com/
Contact: hello@pixinvent.com
Follow: www.twitter.com/pixinvents
Like: www.facebook.com/pixinvents
Purchase: https://1.envato.market/vuexy_frontend.seller
Renew Support: https://1.envato.market/vuexy_frontend.seller
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.

-->
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<!-- Mirrored from pixinvent.com/demo/vuexy-html-bootstrap-frontend.seller-template/html/ltr/vertical-menu-template/dashboard-ecommerce.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Aug 2020 19:44:13 GMT -->

<head>
    @include('frontend.seller.includes.head')




</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 2-columns  navbar-floating footer-static  " data-open="click"
    data-menu="vertical-menu-modern" data-col="2-columns">
    @include('frontend.seller.includes.header')
    @include('frontend.seller.includes.sidebar')
    <style>
        .heading-6 {
    font-size: 2rem !important;
}
.comment-text{
    font-size: 16px;
}
.avatar.avatar-lg {
    font-size: 1.2rem;
}
.avatar {
    /* white-space: nowrap; */
    /* background-color: #C3C3C3; */
    /* position: relative; */
    /* cursor: pointer; */
    /* display: -webkit-inline-box; */
    /* display: -webkit-inline-flex; */
    display: -ms-inline-flexbox;
    /* display: inline-flex; */
    /* font-size: .75rem; */
    /* text-align: center; */
    margin: 11px;
}
.user_nm{
    font-size: 11px;
    margin-top: 11%;
}
.convr{
    border-left: 1px solid #fad13a;
}
.heading {
    margin: 0 0 6px;
    padding: 0;
    text-transform: none;
    font-family: "Open Sans", sans-serif;
    font-weight: 600;
    color: #012747;
    line-height: 1.46;
}
.heading > a {
    color: #273b71;
}
    </style>
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0"> {{ translate('Conversations') }}</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">

                                    <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ translate('Home')}}</a>
                                    </li>
                                    <li class="breadcrumb-item"><a
                                            href="{{ route('dashboard') }}">{{ translate('Dashboard') }}</a></li>
                                    <li class="breadcrumb-item"><a
                                            href="{{ route('conversations.index') }}">{{ translate('Conversations') }}</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Sizes Avatar Starts -->
            @foreach ($conversations as $key => $conversation)

            <section id="sizes-avatar">
                <div class="card">
                    
                    <div class="card-body">
                        <div class="row">

                            <div class="col-sm-3 row">
                               
        
                                <div class="avatar mr-1 avatar-lg">
                                    @if (Auth::user()->id == $conversation->sender_id)
                                    <img @if ($conversation->receiver->avatar_original == null)
                                    src="{{ asset('frontend/images/user.png') }}" @else
                                    src="{{ asset($conversation->receiver->avatar_original) }} @endif"
                                    alt="auser Image">
                                    @else
                                    <img @if ($conversation->sender->avatar_original == null)
                                    src="{{ asset('frontend/images/user.png') }}" @else
                                    src="{{ asset($conversation->sender->avatar_original) }}"
                                    @endif alt="user Image">
                                    @endif
                                </div>
        
        
                                <div class="col-sm-8">
                                    <p class="user_nm">
                                        @if (Auth::user()->id == $conversation->sender_id)
                                        <a href="javascript:;">{{ $conversation->receiver->name }}</a>
                                        @else
                                        <a href="javascript:;">{{ $conversation->sender->name }}</a>
                                        @endif
                                        <br>
                                        <span class="">
                                            {{ date('h:i:m d-m-Y', strtotime($conversation->messages->last()->created_at)) }}
                                        </span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-sm-9 convr">
                                <h2 class="heading heading-6">
                                    <a href="{{ route('conversations.show', encrypt($conversation->id)) }}">
                                        {{ $conversation->title }}
                                    </a>
                                    @if ((Auth::user()->id ==
                                    $conversation->sender_id &&
                                    $conversation->sender_viewed == 0) ||
                                    (Auth::user()->id ==
                                    $conversation->receiver_id &&
                                    $conversation->receiver_viewed == 0))
                                    <span class="badge badge-pill badge-danger">{{ translate('New') }}</span>
                                    @endif
                                    <p class="comment-text mt-0">
                                        {{ $conversation->messages->last()->message }}
                                    </p>
                                </h4>
                            </div>
        
        
                        </div>
                    </div>
                  </div>
               



            </section>
            @endforeach
            <!-- Sizes Avatar Ends -->

</div>
    </div>
    </div>
    <!-- END: Content-->
   
    @include('frontend.seller.includes.customizer')
    @include('frontend.seller.includes.footer')

    @include('partials.modal')

    @yield('script')

    <script>
        $(".dropdown-user-link").click(function(){
  $(".prof").toggleClass("show");
});
  
    </script>
</body>
<!-- END: Body-->

<!-- Mirrored from pixinvent.com/demo/vuexy-html-bootstrap-frontend.seller-template/html/ltr/vertical-menu-template/dashboard-ecommerce.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Aug 2020 19:44:14 GMT -->

</html>