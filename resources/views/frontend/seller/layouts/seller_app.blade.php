<!DOCTYPE html>
<!--
Template Name: Vuexy - Vuejs, HTML & Laravel frontend.seller Dashboard Template
Author: PixInvent
Website: http://www.pixinvent.com/
Contact: hello@pixinvent.com
Follow: www.twitter.com/pixinvents
Like: www.facebook.com/pixinvents
Purchase: https://1.envato.market/vuexy_frontend.seller
Renew Support: https://1.envato.market/vuexy_frontend.seller
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.

-->
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
  
<!-- Mirrored from pixinvent.com/demo/vuexy-html-bootstrap-frontend.seller-template/html/ltr/vertical-menu-template/dashboard-ecommerce.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Aug 2020 19:44:13 GMT -->
<head>
    @include('frontend.seller.includes.head')


  

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 2-columns  navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
    @include('frontend.seller.includes.header')
    @include('frontend.seller.includes.sidebar')
    

    @yield('content')
    @include('frontend.seller.includes.customizer')
    @include('frontend.seller.includes.footer') 
    
    @include('partials.modal')

    @yield('script')

<script>
  $(".dropdown-user-link").click(function(){
  $(".prof").toggleClass("show");
});
  
</script>
<script>
  function showAlert(icon,msg){ 
          Swal.fire({
            position: 'center',
            icon: icon,
            title: msg,
            showCancelButton: false, // There won't be any cancel button
            confirmButtonColor: '#ff7200', // There won't be any cancel button
            showConfirmButton: true ,
            timer: 15000
          });
          
       }
      
</script>
</body>
<!-- END: Body-->

<!-- Mirrored from pixinvent.com/demo/vuexy-html-bootstrap-frontend.seller-template/html/ltr/vertical-menu-template/dashboard-ecommerce.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Aug 2020 19:44:14 GMT -->
</html>