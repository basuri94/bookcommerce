
   
    <!DOCTYPE html>
    <!--
    Template Name: Vuexy - Vuejs, HTML & Laravel frontend.seller Dashboard Template
    Author: PixInvent
    Website: http://www.pixinvent.com/
    Contact: hello@pixinvent.com
    Follow: www.twitter.com/pixinvents
    Like: www.facebook.com/pixinvents
    Purchase: https://1.envato.market/vuexy_frontend.seller
    Renew Support: https://1.envato.market/vuexy_frontend.seller
    License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
    
    -->
    <html class="loading" lang="en" data-textdirection="ltr">
      <!-- BEGIN: Head-->
      
    <!-- Mirrored from pixinvent.com/demo/vuexy-html-bootstrap-frontend.seller-template/html/ltr/vertical-menu-template/dashboard-ecommerce.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Aug 2020 19:44:13 GMT -->
    <head>
        @include('frontend.seller.includes.head')
    
    
        
    
    </head>
    <!-- END: Head-->
    
    <!-- BEGIN: Body-->
    
    <body class="vertical-layout vertical-menu-modern 2-columns  navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
        @include('frontend.seller.includes.header')
        @include('frontend.seller.includes.sidebar')
    


   <!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h2 class="content-header-title float-left mb-0">&nbsp;&nbsp;&nbsp;View Tickit Details</h2>
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  
                  <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ translate('Home')}}</a></li>
                  </ol>
              </div>
            </div>
          </div>
        </div>
        
      </div>
   <div class="col-md-12 content-body">
  <section class="gry-bg profile">
      <div class="container">
          <div class="row cols-xs-space cols-sm-space cols-md-space">
              
              <div class="col-lg-12">
                  <div class="main-content">
                      <div class="card">
                          <div class="card-header py-3">
                              <h3 class="heading-5">{{ $ticket->subject }} #{{ $ticket->code }}</h3>
                              <ul class="list-inline alpha-6 mb-0">
                                  <li class="list-inline-item">{{ date('d/m/Y h:m A', strtotime($ticket->created_at)) }}</li>
                                  <li class="list-inline-item"><span class="badge badge-pill badge-primary">Open</span></li>
                              </ul>
                          </div>
                          <div class="card-body">
                              <div class="border-bottom pb-4">
                                  <form class="" action="{{route('support_ticket.seller_store')}}" method="POST" enctype="multipart/form-data">
                                      @csrf
                                      <input type="hidden" name="ticket_id" value="{{$ticket->id}}">
                                      <input type="hidden" name="user_id" value="{{$ticket->user_id}}">
                                      <div class="form-group">
                                          <textarea class="form-control editor" id="reply" name="reply" placeholder="{{ translate('Type your reply') }}" data-buttons="bold,underline,italic,|,ul,ol,|,paragraph,|,undo,redo"></textarea>
                                      </div>
                                      <div class="form-group">
                                        <div class="custom-file">
                                            <input type="file" name="attachments[]" id="file-2" class="custom-file-input" data-multiple-caption="{count} files selected" multiple>
                                            <label class="custom-file-label" for="file-2"> <i class="fa fa-upload"></i> {{ translate('Attach files.')}}</label>
                                          </div>
                                          {{-- <input type="file" name="attachments[]" id="file-2" class="custom-input-file custom-input-file--2" data-multiple-caption="{count} files selected" multiple />
                                          <label for="file-2" class=" mw-100 mb-0">
                                              <i class="fa fa-upload"></i>
                                              <span>{{ translate('Attach files.')}}</span>
                                          </label> --}}
                                      </div>
                                      <div class="text-right">
                                          <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">{{ translate('Send Reply')}}</button>
                                      </div>
                                  </form>
                              </div>
                              <div class="pt-4">
                                  @foreach ($ticket_replies as $ticketreply)
                                      @if($ticket->user_id == $ticketreply->user_id)
                                          <div class="block block-comment mb-3 border-0">
                                              <div class="d-flex flex-row-reverse">
                                                  <div class="pl-3">
                                                      <div class="block-image d-block size-40" data-toggle="tooltip" data-title="{{ $ticketreply->user->name }}">

                                                          @if($ticketreply->user->avatar_original != '')
                                                              <img src="{{ asset($ticketreply->user->avatar_original) }}" class="rounded-circle">
                                                          @else
                                                              <img src="{{ asset('frontend/images/user.png') }}" class="rounded-circle">
                                                          @endif
                                                      </div>
                                                  </div>
                                                  <div class="flex-grow-1 ml-5 pl-5">
                                                      <div class="p-3 bg-gray rounded">
                                                          @php echo $ticketreply->reply; @endphp
                                                          @if($ticketreply->files != null && is_array(json_decode($ticketreply->files)))
                                                              <div class="mt-3 clearfix">
                                                                  @foreach (json_decode($ticketreply->files) as $key => $file)
                                                                      <div class="float-right bg-white p-2 rounded ml-2">
                                                                          <a href="{{ asset($file->path) }}" download="{{ $file->name }}" class="file-preview d-block text-black-50" style="width:100px">
                                                                              <div class="text-center h4">
                                                                                  <i class="la la-file"></i>
                                                                              </div>
                                                                              <div class="d-flex">
                                                                                  <div class="flex-grow-1 minw-0">
                                                                                      <div class="text-truncate">
                                                                                          {{ explode('.', $file->name)[0] }}
                                                                                      </div>
                                                                                  </div>
                                                                                  <div>
                                                                                      .{{ explode('.', $file->name)[1] }}
                                                                                  </div>
                                                                              </div>
                                                                          </a>
                                                                      </div>
                                                                  @endforeach
                                                              </div>
                                                          @endif
                                                      </div>
                                                      <span class="comment-date alpha-5 text-sm mt-1 d-block text-right">
                                                          {{ date('d/m/Y h:m A', strtotime($ticketreply->created_at)) }}
                                                      </span>
                                                  </div>
                                              </div>
                                          </div>
                                      @else
                                          <div class="block block-comment mb-3 border-0">
                                              <div class="d-flex">
                                                  <div class="pr-3">
                                                      <div class="block-image d-block size-40" data-toggle="tooltip" data-title="{{ $ticketreply->user->name }}">
                                                      @if($ticketreply->user->avatar_original != '')
                                                          <img loading="lazy"  src="{{ asset($ticketreply->user->avatar_original) }}" class="rounded-circle" data-toggle="tooltip" data-title="fsdfsf">
                                                          @else
                                                              <img src="{{ asset('frontend/images/user.png') }}" class="rounded-circle">
                                                          @endif
                                                      </div>
                                                  </div>
                                                  <div class="flex-grow-1 mr-5 pr-5">
                                                      <div class="p-3 bg-gray rounded">
                                                          @php echo $ticketreply->reply; @endphp
                                                          @if($ticketreply->files != null && is_array(json_decode($ticketreply->files)))
                                                              <div class="mt-3 clearfix">
                                                                  @foreach (json_decode($ticketreply->files) as $key => $file)
                                                                      <div class="float-right bg-white p-2 rounded ml-2">
                                                                          <a href="{{ asset($file->path) }}" download="{{ $file->name }}" class="file-preview d-block text-black-50" style="width:100px">
                                                                              <div class="text-center h4">
                                                                                  <i class="la la-file"></i>
                                                                              </div>
                                                                              <div class="d-flex">
                                                                                  <div class="flex-grow-1 minw-0">
                                                                                      <div class="text-truncate">
                                                                                          {{ explode('.', $file->name)[0] }}
                                                                                      </div>
                                                                                  </div>
                                                                                  <div>
                                                                                      .{{ explode('.', $file->name)[1] }}
                                                                                  </div>
                                                                              </div>
                                                                          </a>
                                                                      </div>
                                                                  @endforeach
                                                              </div>
                                                          @endif
                                                      </div>
                                                      <span class="comment-date alpha-5 text-sm mt-1 d-block">
                                                          {{ date('d/m/Y h:m A', strtotime($ticketreply->created_at)) }}
                                                      </span>
                                                  </div>
                                              </div>
                                          </div>
                                      @endif
                                  @endforeach
                                  <div class="block block-comment mb-3 border-0">
                                      <div class="d-flex flex-row-reverse">
                                          <div class="pl-3">
                                              <div class="block-image d-block size-40">
                                              @if($ticket->user->avatar_original != '')
                                                  <img loading="lazy"  src="{{ asset($ticket->user->avatar_original) }}" class="rounded-circle">
                                                  @else
                                                              <img src="{{ asset('frontend/images/user.png') }}" class="rounded-circle">
                                                          @endif
                                              </div>
                                          </div>
                                          <div class="flex-grow-1 ml-5 pl-5">
                                              <div class="p-3 bg-gray rounded">
                                                  @php echo $ticket->details; @endphp
                                                  @if($ticket->files != null && is_array(json_decode($ticket->files)))
                                                      <div class="mt-3 clearfix">
                                                          @foreach (json_decode($ticket->files) as $key => $file)
                                                              <div class="float-right bg-white p-2 rounded ml-2">
                                                                  <a href="{{ asset($file->path) }}" download="{{ $file->name }}" class="file-preview d-block text-black-50" style="width:100px">
                                                                      <div class="text-center h4">
                                                                          <i class="la la-file"></i>
                                                                      </div>
                                                                      <div class="d-flex">
                                                                          <div class="flex-grow-1 minw-0">
                                                                              <div class="text-truncate">
                                                                                  {{ explode('.', $file->name)[0] }}
                                                                              </div>
                                                                          </div>
                                                                          <div>
                                                                              .{{ explode('.', $file->name)[1] }}
                                                                          </div>
                                                                      </div>
                                                                  </a>
                                                              </div>
                                                          @endforeach
                                                      </div>
                                                  @endif
                                              </div>
                                              <span class="comment-date alpha-5 text-sm mt-1 d-block text-right">
                                                  {{ date('d/m/Y h:m A', strtotime($ticket->created_at)) }}
                                              </span>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>

          </div>
      </div>
</section>
</div>
          </div>
        </div>
    @include('partials.modal')

   

    @include('frontend.seller.includes.customizer')
    <script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
    @include('frontend.seller.includes.footer')

</body>
<!-- END: Body-->

<!-- Mirrored from pixinvent.com/demo/vuexy-html-bootstrap-admin-template/html/ltr/vertical-menu-template/dashboard-ecommerce.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Aug 2020 19:44:14 GMT -->

</html>