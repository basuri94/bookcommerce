@extends('frontend.seller.layouts.seller_app')

@section('content')
<style>

.required:after {
        content:" *";
        color: red;
      }
</style>
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">

        <div class="content-body">
            <!-- Basic Horizontal form layout section start -->
            <section id="basic-horizontal-layouts">
                <div class="row match-height">
                    <div class="col-md-12 col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">{{translate('Approval Status')}}</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <!--Horizontal Form-->
                                    <!--===================================================-->
                                    @php
                                    $sellerdata = App\Seller::where('user_id',Auth::user()->id)->first();
                                    $shopdata=App\Shop::where('user_id',Auth::user()->id)->first();
                                    @endphp
                                    @if($sellerdata->verification_status==0)
                                    <div class="alert danger-alert">Your Approval Status Is Pending. Check After Some Time</div>
                                    

                                    @elseif($sellerdata->verification_status==2)
                                    <div class="alert danger-alert">Your Approval Rejected.Update Following Points :<br>{{ $sellerdata->remarks }}</div>
                                    <form class="form-horizontal" action="{{ route('sellerUpdate', $sellerdata->id) }}"
                                        method="POST" enctype="multipart/form-data">

                                        @csrf
                                        <div class="form-body">
                                            <div class="row">
                                                @php
                                                $exp=explode(" ",$sellerdata->user->name);

                                                $fname=$exp[0];
                                                $lname=$exp[1];
                                                @endphp
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4"><span class="required">{{translate('First Name')}}</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" placeholder="{{translate('First Name')}}"
                                                                id="fname" name="fname" class="form-control" required
                                                                value="{{ $fname}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4"><span
                                                                class="required">{{translate('Last Name')}}</span></div>
                                                        <div class="col-md-8">
                                                            <input type="text" placeholder="{{translate('Last Name')}}"
                                                                id="lname" name="lname" class="form-control" required
                                                                value="{{ $lname}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4"><span
                                                                class="required">{{translate('Email')}}</span></div>
                                                        <div class="col-md-8">
                                                            <input type="text" placeholder="{{translate('Email')}}"
                                                                id="email" readonly name="email" class="form-control" required
                                                                value="{{ $sellerdata->user->email}}">
                                                        </div>
                                                    </div>
                                                </div>




                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4"><span class="required">{{translate('Phone No')}}</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" readonly class="form-control" name="phone_no"
                                                                value="{{ $sellerdata->user->phone_no }}"
                                                                placeholder="{{translate('Phone No')}}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4">
                                                            <span class="required">{{translate('Nature Of Business')}}</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <select class="form-control" name="business_nature"
                                                                id="business_nature">
                                                                <option value="">--Select--</option>
                                                                @foreach(Config::get('constants.BUSINESS_NATURE') as
                                                                $key=>$val)
                                                                     
                                                                <option value="{{$key}}" @if($key===$sellerdata->business_nature) selected @endif>{{$val}}</option>

                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4"><span class="required">{{translate('HSN Code')}}</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" placeholder="{{translate('HSN Code')}}"
                                                                id="hsn_code" name="hsn_code"
                                                                value="{{ $sellerdata->hsn_code }}"
                                                                class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4">
                                                            <span class="required">{{translate('Category Product')}}</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                           
                                                            <select class="form-control" name="product_category"
                                                                id="product_category" onchange="categoryChange();">
                                                                <option value="">--Select--</option>
                                                                @foreach($category as $key=>$val)

                                                                <option value="{{$val['id']}}" @if($val['id']==$sellerdata->product_category) selected @endif>{{$val['name']}}</option>

                                                                @endforeach
                                                                @if($sellerdata->product_category==0)
                                                                <option value="0" selected>--Others--</option>
                                                                @endif
                                                            </select>
                                                           
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12" style="display: none" id="other_cat">
                                                    <div class="form-group row">
                                                        <div class="col-md-4"><span class="required">{{translate('Category Name')}}</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" placeholder="{{translate('Enter Category Name')}}"
                                                                id="other_category" name="other_category"
                                                                value="{{ $sellerdata->hsn_code }}"
                                                                class="form-control">
                                                        </div>
                                                    </div>
                                                </div>

                                              
                                                
                                                  <div class="col-12" id="product_invoice_div" style="display: none;">
                                                      <div class="form-group row">
                                                          <div class="col-md-4"><span class="required">{{translate('Product Invoice ')}}</span>
                                                          </div>
                                                          <div class="col-md-8">
                                                              <input type="file" class="form-control" name="product_invoice">
                                                              @if(!empty($sellerdata->product_invoice))
                                                              <a href="{{asset('uploads/verification_form/'.$sellerdata->product_invoice)}}"
                                                                  target="_blank"><span> View Product Invoice</span></a>
                                                                  @endif
                                                          </div>
                                                      </div>
                                                  </div>
                                                 
                                                
                                                  <div class="col-12">
                                                      <div class="form-group row">
                                                          <div class="col-md-4"><span class="required">{{translate('Product License/Invoice')}}</span>
                                                          </div>
                                                          <div class="col-md-8">
                                                              <input type="file" class="form-control" name="product_licence">
                                                              <a href="{{asset('uploads/verification_form/'.$sellerdata->product_licence)}}"
                                                                  target="_blank"><span> View Product License/Invoice</span></a>
                                                          </div>
                                                      </div>
                                                  </div>
                                               
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4">
                                                            <span class="required">{{translate('Business Address')}}</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <textarea type="text" class="form-control"
                                                                name="address_business"
                                                                
                                                                placeholder="{{translate('Business Address')}}">{{ $shopdata->address }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4">
                                                            <span class="required">{{translate('City')}}</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" 
                                                                name="city"
                                                                value="{{ $shopdata->city }}"
                                                                placeholder="{{translate('City')}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4">
                                                            <span class="required">{{translate('Pincode')}}</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control"
                                                                name="postal_code"
                                                                value="{{ $shopdata->postal_code }}"
                                                                placeholder="{{translate('Pincode')}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                               
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4"><span class="required">{{translate('Pan Card')}}</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" name="pan_card"
                                                                value="{{ $sellerdata->pan_card }}"
                                                                placeholder="{{translate('Pan Card')}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4"><span class="required">{{translate('Pancard File')}}</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="file" class="form-control" name="file_pan">
                                                            <a href="{{asset('uploads/verification_form/'.$sellerdata->file_pan)}}"
                                                                target="_blank"><span> View Pancard</span></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4"><span class="required">{{translate('GST')}}</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" name="gst"
                                                                value="{{ $sellerdata->gst }}"
                                                                placeholder="{{translate('GST')}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4"><span  class="required">{{translate('GST File')}}</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="file" class="form-control" name="file_gst">
                                                            <a href="{{asset('uploads/verification_form/'.$sellerdata->file_gst)}}"
                                                                target="_blank"><span> View GST </span></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4">
                                                            <span  class="required">{{translate('Bank Account Number')}}</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" name="bank_acc_no"
                                                                value="{{ $sellerdata->bank_acc_no }}"
                                                                placeholder="{{translate('Bank Account Number')}}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4">
                                                            <span  class="required">{{translate('Branch IFSC Code')}}</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" name="ifsc_code"
                                                                value="{{ $sellerdata->ifsc_code }}"
                                                                placeholder="{{translate('Branch IFSC Code')}}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4"><span  class="required">{{translate('Branch Name')}}</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" name="bname"
                                                                value="{{ $sellerdata->bname }}"
                                                                placeholder="{{translate('Branch Name')}}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4">
                                                            <span  class="required">{{translate('Trademark Logo')}}</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="file" class="form-control"
                                                                name="trademark_icon">
                                                            <a href="{{asset('uploads/verification_form/'.$sellerdata->trademark_icon)}}"
                                                                target="_blank"><span> View Trademark</span></a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4">
                                                            <span  class="required">{{translate('Cancelled Cheque Upload')}}</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="file" class="form-control"
                                                                name="cancelled_cheque">
                                                            <a href="{{asset('uploads/verification_form/'.$sellerdata->cancelled_cheque)}}"
                                                                target="_blank"><span> View Cancelled Cheque</span></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-4">
                                                            <span>{{translate('Admin Remarks')}}</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <textarea type="text" class="form-control"
                                                                name="remarks"
                                                                disabled
                                                                placeholder="{{translate('Admin Remarks')}}">{{ $sellerdata->remarks }}</textarea>
                                                        </div>
                                                    </div>
                                                </div> -->




                                                <div class="col-md-8 offset-md-4">
                                                    <button type="submit"
                                                        class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                                    <button type="reset"
                                                        class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                                    <a href="{{route('categories.index')}}"
                                                        class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>
                                                </div>

                                            </div>
                                        </div>
                                    </form>


                                    @endif

                                    <!--===================================================-->
                                    <!--End Horizontal Form-->


                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <!-- // Basic Horizontal form layout section end -->


            <!-- // Basic Floating Label Form section end -->

        </div>
    </div>
</div>
<!-- END: Content-->
@endsection

@section('script')
<script>

    $(function(){
        setTimeout(function () {
        
        location.reload(true);
      }, 60000);
        @if(!empty($sellerdata->product_invoice))
    $("#product_invoice_div").show();
    @endif
    @if($sellerdata->product_category==0)
    $("#other_cat").show();
    @endif
    
  $("#business_nature").change(function(){
    $("#product_invoice").val("");
    if(this.value=="Reseller"){
      $("#product_invoice_div").show();
      $('#product_invoice').addClass('wizard-required');
    }else{
      $("#product_invoice_div").hide();
    }
  });
    });
    function categoryChange(){
  let product_category=$('#product_category').val();
  if(product_category=="0"){
    $('#other_cat').show();
   
  }
  else{
    $('#other_cat').hide();
  
    $('#other_category').val('');
  }
}
</script>
@endsection