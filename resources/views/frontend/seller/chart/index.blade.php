@extends('frontend.seller.layouts.seller_app')

@section('content')

<!-- BEGIN: Content-->
<div class="app-content content" >
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- Dashboard Ecommerce Starts -->
            <!-- Zero configuration table -->
            <section class="gry-bg profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
               
                <div class="col-lg-12">
                    <div class="main-content">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">{{translate('Commission Chart')}}</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    @csrf
                                    {{-- <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="retun-tab-fill" data-toggle="tab"
                                                href="#retun-fill" role="tab" aria-controls="retun-fill"
                                                aria-selected="true">{{translate('Return Request')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="refund-tab-fill" data-toggle="tab"
                                                href="#refund-fill" role="tab" aria-controls="refund-fill"
                                                aria-selected="false">{{translate('Return/Refund Request')}}</a>
                                        </li>

                                    </ul> --}}
                                    <!-- Tab panes -->
                                    <div class="table-responsive">
                                        <table class="table zero-configuration">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>{{translate('Category')}}</th>
                                                    <th>{{translate('Subcategory')}}</th>
                                                    <th>{{translate('Commission')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($subcategories as $key => $subcategory)
                                        @if ($subcategory->category != null)
                                            <tr>
                                                <td>{{ ($key+1) + ($subcategories->currentPage() - 1)*$subcategories->perPage() }}</td>
                                                <td>{{$subcategory->category->name}}</td>
                                                <td>{{__($subcategory->name)}}</td>
                                                <td>{{ $subcategory->commision_rate }} %</td>
                                               
                                            </tr>
                                        @endif
                                    @endforeach
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>#</th>
                                                    <th>{{translate('Category')}}</th>
                                                    <th>{{translate('Subcategory')}}</th>
                                                    <th>{{translate('Commission')}}</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <div class="clearfix">
                                            <div class="pull-right">
                                            {{ $subcategories->appends(request()->input())->links() }}
                                            </div>
                                        </div>
                                    </div>
                                

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<!-- END: Content-->


<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="refundviewmodal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Refund View</h5>
                {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"> --}}
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="refund_body">
                ...
            </div>
            <div class="modal-footer" id="footer_id">

            </div>
        </div>
    </div>
</div>


@endsection
