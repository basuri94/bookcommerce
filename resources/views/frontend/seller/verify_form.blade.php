@extends('frontend.seller.layouts.seller_app')

@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
              <div class="col-12">
                <h2 class="content-header-title float-left mb-0">  {{ translate('Shop Verification')}}
                                        <a href="{{ route('shop.visit', $shop->slug) }}" class="btn btn-link btn-sm" target="_blank">({{ translate('Visit Shop')}} <i class="la la-external-link"></i>)</a>
                                   </h2>
                <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                    
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ translate('Home')}}</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">{{ translate('Dashboard')}}</a></li>
                    <li  class="breadcrumb-item active"><a href="{{ route('shops.index') }}">{{ translate('Shop Settings')}}</a></li>
                </ol>
                </div>
              </div>
            </div>
          </div>
          
        </div>
     <div class="col-md-12 content-body">
    <section class="gry-bg profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                
                <div class="col-lg-12">
                    <div class="main-content">
                        <!-- Page title -->
                       
                        <form class="" action="{{ route('shop.verify.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{ translate('Verification info')}}
                                </div>
                                @php
                                    $verification_form = \App\BusinessSetting::where('type', 'verification_form')->first()->value;
                                @endphp
                                <div class="form-box-content p-3">
                                    @foreach (json_decode($verification_form) as $key => $element)
                                        @if ($element->type == 'text')
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>{{ $element->label }} <span class="required-star">*</span></label>
                                                </div>
                                                <div class="col-md-10">
                                                    <input type="{{ $element->type }}" class="form-control mb-3" placeholder="{{ translate($element->label) }}" name="element_{{ $key }}" required>
                                                </div>
                                            </div>
                                        @elseif($element->type == 'file')
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>{{ $element->label }}</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <input type="{{ $element->type }}" name="element_{{ $key }}" id="file-{{ $key }}" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" required/>
                                                    <label for="file-{{ $key }}" class="mw-100 mb-3">
                                                        <span></span>
                                                        <strong>
                                                            <i class="fa fa-upload"></i>
                                                            {{ translate('Choose file')}}
                                                        </strong>
                                                    </label>
                                                </div>
                                            </div>
                                        @elseif ($element->type == 'select' && is_array(json_decode($element->options)))
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>{{ $element->label }}</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <div class="mb-3">
                                                        <select class="form-control selectpicker" data-minimum-results-for-search="Infinity" name="element_{{ $key }}" required>
                                                            @foreach (json_decode($element->options) as $value)
                                                                <option value="{{ $value }}">{{ $value }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        @elseif ($element->type == 'multi_select' && is_array(json_decode($element->options)))
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>{{ $element->label }}</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <div class="mb-3">
                                                        <select class="form-control selectpicker" data-minimum-results-for-search="Infinity" name="element_{{ $key }}[]" multiple required>
                                                            @foreach (json_decode($element->options) as $value)
                                                                <option value="{{ $value }}">{{ $value }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        @elseif ($element->type == 'radio')
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>{{ $element->label }}</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <div class="mb-3">
                                                        @foreach (json_decode($element->options) as $value)
                                                            <div class="radio radio-inline">
                                                                <input type="radio" name="element_{{ $key }}" value="{{ $value }}" id="{{ $value }}" required>
                                                                <label for="{{ $value }}">{{ $value }}</label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                             </div>
                            </div>
                            <div class="text-right mt-4">
                                <button type="submit" class="btn btn-styled btn-base-1">{{ translate('Apply')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>
            </div>
          </div>
@endsection
