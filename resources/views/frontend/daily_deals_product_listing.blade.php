@extends('frontend.layouts.app')
@php
        $meta_title = env('APP_NAME');
        $meta_description = \App\SeoSetting::first()->description;
    @endphp

@section('meta_title'){{ $meta_title }}@stop
@section('meta_description'){{ $meta_description }}@stop

@section('meta')
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="{{ $meta_title }}">
    <meta itemprop="description" content="{{ $meta_description }}">

    <!-- Twitter Card data -->
    <meta name="twitter:title" content="{{ $meta_title }}">
    <meta name="twitter:description" content="{{ $meta_description }}">

    <!-- Open Graph data -->
    <meta property="og:title" content="{{ $meta_title }}" />
    <meta property="og:description" content="{{ $meta_description }}" />
@endsection

@section('content')


<style>

.breadcrumb-area {
    text-align: center;
    position: relative;
    padding: 70px 0;
    background: none no-repeat !important;
    background-size: cover;
}

.breadcrumb-area {
    padding: 5px 0;
    border-bottom: 1px solid #e9e9e9;
    background-color: #fafafa;
}
.breadcrumb {
    margin-bottom: 0rem !important;
}
.add-to-link ul li {
    display: inline-flex;
    justify-content: center;
    float: none;
    width: 20%;
}
</style>
    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('home') }}">{{ translate('Home')}}</a></li>
                        <li><a href="{{ route('products') }}">{{ translate('Daily Deals')}}</a></li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <section class="gry-bg py-4">
        <div class="container sm-px-0">
            <form class="" id="search-form" action="{{ route('search') }}" method="GET">
                <div class="row">
               
                <div class="col-xl-12">
                    <!-- <div class="bg-white"> -->
                    
                        <!-- <hr class=""> -->
                        <div class="products-box-bar p-3 bg-white">
                            <div class="row sm-no-gutters gutters-5">
                                <?php //echo json_encode($products);die; ?>
                                @foreach (filter_products(\App\Product::where('is_approve', 1)->where('published', 1)->where('todays_deal', '1'))->get() as $key => $product)
                                
                                    <div class="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-6">
                                    <article class="list-product mb-30px">
                                <div class="img-block">
                                    <a href="{{ route('product', $product->slug) }}" class="thumbnail">
                                        <img class="first-img" src="{{ asset($product->thumbnail_img) }}" alt="{{ __($product->name) }} " style="width:100%"/>
                                        <img class="second-img" src="{{ asset($product->thumbnail_img) }}" alt="{{ __($product->name) }}" style="width:100%"/>
                                    </a>
                                    <div class="quick-view">
                                        <a class="quick_view" href="{{ route('product', $product->slug) }}" data-link-action="quickview" title="Quick view">
                                            <i class="ion-ios-search-strong"></i>
                                        </a>
                                    </div>
                                </div>
                                <ul class="product-flag">
                                    @if ($product->discount_type=="percent")
                                    @php
                                        $show_type="%";
                                    @endphp
                                    <li class="new">{{ $product->discount.$show_type }} off</li>
                                    @else 
                                    @php
                                        $show_type="₹";
                                    @endphp
                                    <li class="new">{{ $show_type.$product->discount }} off</li>
                                    @endif
                                </ul>
                                <div class="product-decs1">
                                    <div class="product-decs2">
                                    <div class="star-rating star-rating-sm mt-1">
                                                    {{ renderStarRating($product->rating) }}
                                                </div>
                                        <div class="pricing-meta"  style="float:right;">
                                            <ul>
                                            @if(home_base_price($product->id) != home_discounted_base_price($product->id))
                                    <li class="current-price">{{ home_discounted_base_price($product->id) }}</li>
                                    <li class="old-price">{{ home_base_price($product->id) }}</li>

                                    @else 

                                    <li class="current-price">{{ home_discounted_base_price($product->id) }}</li>
                                    @endif
                                            </ul>
                                        </div>
                                    </div>
                                    

                                    <h2 ><a href="{{ route('product', $product->slug) }}" class="product-link">{{ __($product->name) }}</a></h2>
                                </div>
                                <div class="add-to-link">
                                    <ul>
                                    <li  class="compare_add cart">
                                            <a href="#" onclick="addToCompare({{ $product->id }})"><span class="iconify" data-icon="ion:git-compare" data-inline="false"></span></a>
                                        </li>
                                        <li style="float: none;" class="cart_li"><a class="cart-btn" href="#" onclick="showAddToCartModal({{ $product->id }})">ADD TO CART </a></li>
                                      <li class="favart_add">
                                            <a href="#" onclick="addToWishList({{ $product->id }});"><i class="ion-android-favorite-outline"></i></a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </article>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        

                    <!-- </div> -->
                </div>
            </div>
            </form>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
        function filter(){
            $('#search-form').submit();
        }
        function rangefilter(arg){
            $('input[name=min_price]').val(arg[0]);
            $('input[name=max_price]').val(arg[1]);
            filter();
        }
    </script>
@endsection
