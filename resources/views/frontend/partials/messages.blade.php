{{-- 
 <style>
     
        
    .container {
      border: 2px solid #dedede;
      background-color: #f1f1f1;
      border-radius: 5px;
      padding: 10px;
      margin: 10px 0;
    }
    
    .darker {
      border-color: #ccc;
      background-color: #ddd;
    }
    
    .container::after {
      content: "";
      clear: both;
      display: table;
    }
    
    .container img {
      float: left;
      max-width: 60px;
      width: 100%;
      margin-right: 20px;
      border-radius: 50%;
    }
    
    .container img.right {
      float: right;
      margin-left: 20px;
      margin-right:0;
    }
    
    .time-right {
      float: right;
      color: #aaa;
    }
    
    .time-left {
      float: left;
      color: #999;
    }
    </style>
@foreach ($conversation->messages as $key => $message)
@if ($message->user_id == Auth::user()->id)
<div class="container">
    @if ($message->user->avatar_original != null)
    <img src="{{ asset($message->user->avatar_original) }}" alt="Avatar" style="width:100%;">
    @else
    <img src="{{ asset('frontend/images/user.png') }}"
    alt="Avatar" style="width:100%;">
    @endif
    <p> {{ $message->message }}</p>
    <span class="time-right"> {{ date('h:i:m d-m-Y', strtotime($message->created_at)) }}</span>
  </div>
  @else
  <div class="container darker">
    @if ($message->user->avatar_original != null)
    <img src="{{ asset($message->user->avatar_original) }}" alt="Avatar" class="right" style="width:100%;">
    @else
    <img src="{{ asset('frontend/images/user.png') }}"  class="right"
    alt="Avatar" style="width:100%;">
    @endif
    <p> {{ $message->message }}</p>
    <span class="time-left">{{ date('h:i:m d-m-Y', strtotime($message->created_at)) }}</span>
  </div>
  @endif
 @endforeach --}}
<style>
 
.bubbleWrapper {
	padding: 10px 10px;
	display: flex;
	justify-content: flex-end;
	flex-direction: column;
	align-self: flex-end;
  color: #fff;
}
.inlineContainer {
  display: inline-flex;
}
.inlineContainer.own {
  flex-direction: row-reverse;
}
.inlineIcon {
  width:20px;
  object-fit: contain;
}
.ownBubble {
	min-width: 60px;
	max-width: 700px;
	padding: 14px 18px;
  margin: 6px 8px;
	background-color: #5b5377;
	border-radius: 16px 16px 0 16px;
	border: 1px solid #443f56;
 
}
.otherBubble {
	min-width: 60px;
	max-width: 700px;
	padding: 14px 18px;
  margin: 6px 8px;
	background-color: #6C8EA4;
	border-radius: 16px 16px 16px 0;
	border: 1px solid #54788e;
  
}
.own {
	align-self: flex-end;
}
.other {
	align-self: flex-start;
}
span.own,
span.other{
  font-size: 14px;
  color: grey;
}
</style>

@foreach ($conversation->messages as $key => $message)
@if ($message->user_id == Auth::user()->id)
<div class="bubbleWrapper">
  <div class="inlineContainer">
  @if ($message->user->avatar_original != null)
    <img class="inlineIcon" src="{{ asset($message->user->avatar_original) }}" alt="Avatar">
    @else
    <img class="inlineIcon"  src="{{ asset('frontend/images/user.png') }}"
    alt="Avatar" >
    @endif
   
    <div class="otherBubble other">
      {{ $message->message }}
    </div>
  </div><span class="other">{{ date('h:i:m d-m-Y', strtotime($message->created_at)) }}</span>
</div>
@else 

<div class="bubbleWrapper">
  <div class="inlineContainer own">
    @if ($message->user->avatar_original != null)
    <img class="inlineIcon" src="{{ asset($message->user->avatar_original) }}" alt="Avatar" >
    @else
    <img class="inlineIcon"  height="62px" src="{{ asset('frontend/images/user.png') }}"
    alt="Avatar">
    @endif
    <div class="ownBubble own">
      {{ $message->message }}
    </div>
  </div><span class="own">{{ date('h:i:m d-m-Y', strtotime($message->created_at)) }}</span>
</div>


@endif
@endforeach

{{-- <script>
$(function(){
        
        var objDiv = document.getElementById("messages");
objDiv.scrollTop = objDiv.scrollHeight;

    });
</script> --}}
