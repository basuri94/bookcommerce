<style>
    .test11 {
        height: calc(2.25rem + 8px) !important;
    }

    .process-steps li.active:after {
        background: #30c100 !important;
    }
    .process-steps li + li:after {
    position: absolute;
    content: "";
    height: 10px;
    width: calc(100% - 40px);
    background: #f1e2e2;
    top: 14px;
    z-index: 0;
    right: calc(50% + 20px);
}
.process-steps li .icon {
    height: 40px;
    width: 40px;
    margin: auto;
    background: #f1e2e2;
    border-radius: 50%;
    line-height: 20px;
    font-size: 14px;
    font-weight: 700;
    padding: 10px;
    color: #400b0b;
    position: relative;
}
.alert {
  padding: 20px;
  background-color: #f44336;
  color: white;
}
</style>
<div class="modal-header">
    <h5 class="modal-title strong-600 heading-5">{{ translate('Order id')}}: {{ $order->code }}</h5>
    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button> -->
</div>

@php
$status = $order->orderDetails->where('seller_id', Auth::user()->id)->first()->delivery_status;
$payment_status = $order->orderDetails->where('seller_id', Auth::user()->id)->first()->payment_status;
$refund_request_addon = \App\Addon::where('unique_identifier', 'refund_request')->first();
@endphp


    <div class="pt-4">
          @if($order->payment_status!="cancelled")
        <ul class="process-steps clearfix">
            <li @if($status=='pending' ) class="active" @else class="done" @endif>
                <div class="icon">1</div>
                <div class="title">{{ translate('Order placed')}}</div>
            </li>
            <li @if($status=='on_review' ) class="active" @elseif($status=='on_delivery' || $status=='delivered' )
                class="done" @endif>
                <div class="icon">2</div>
                <div class="title">{{ translate('On review')}}</div>
            </li>
            <li @if($status=='on_delivery' ) class="active" @elseif($status=='delivered' ) class="done" @endif>
                <div class="icon">3</div>
                <div class="title">{{ translate('On delivery')}}</div>
            </li>
            <li @if($status=='delivered' ) class="done" @endif>
                <div class="icon">4</div>
                <div class="title">{{ translate('Delivered')}}</div>
            </li>
        </ul>
          @else
        <div class="alert">
          Order has been cancelled.
</div>
        @endif
    </div>
    <div class="row mt-5">
        <!-- <div class="offset-lg-2 col-lg-4 col-sm-6">
            <div class="form-inline">
                <select class="form-control selectpicker form-control-sm" data-minimum-results-for-search="Infinity"
                    id="update_payment_status">
                    <option value="unpaid" @if ($payment_status=='unpaid' ) selected @endif>{{ translate('Unpaid')}}
                    </option>
                    <option value="paid" @if ($payment_status=='paid' ) selected @endif>{{ translate('Paid')}}</option>
                </select>
                <label class="my-2">{{ translate('Payment Status')}}</label>
            </div>
        </div> -->
          @if($order->payment_status!="cancelled")
        <div class="col-lg-12 col-sm-12">
            <div class="form-inline">
                <select class="form-control selectpicker form-control-sm" data-minimum-results-for-search="Infinity"
                    id="update_delivery_status">
                    <option value="pending" @if ($status=='pending' ) selected @endif>{{ translate('Pending')}}</option>
                    <option value="on_review" @if ($status=='on_review' ) selected @endif>{{ translate('On review')}}
                    </option>
                    <option value="on_delivery" @if ($status=='on_delivery' ) selected @endif>
                        {{ translate('On delivery')}}</option>
                    <option value="delivered" @if ($status=='delivered' ) selected @endif>{{ translate('Delivered')}}
                    </option>
                </select>
                <label class="my-2">{{ translate('Delivery Status')}}</label>
            </div>
        </div>
         @endif
    </div>
        
    @php $digiflag=0; @endphp
    @foreach ($order->orderDetails->where('seller_id', Auth::user()->id) as $key =>
                            $orderDetailss)
                            @if($orderDetailss->product->digital==1)
@php $digiflag=1; @endphp
                            @endif
                            @endforeach
                            @if($digiflag==0)
    @if($status=='on_review')

    
    @if($order->curiorOrderDetails->count()==0)
    
    <div class="table-container" id="pricing-tables">
             
                    
                </div>
                <div class="card mt-3">
        <div class="card-header py-2 px-3 ">
        <div class="heading-6 strong-600">{{ translate('Product Details')}}</div>
        </div>
        <div class="card-body pb-0">
            <div class="row">
                <div class="col-lg-12">
                <form  class="form-default" role="form" action="{{ route('orders.set_order') }}" method="POST" id="set_order">
<input type="hidden" value="{{ $order->id }}" name="order_id" id="order_id" >
                    @csrf
                    <div class="modal-body">
                        <div class="">
                            <div class="row">
                            @foreach ($order->orderDetails->where('seller_id', Auth::user()->id) as $key =>
                            $orderDetail)
                            @if($orderDetail->product->digital==0)
                            <h4>{{$orderDetail->product->name}}</h4>
                            <div class="row">
                            <div class="col-md-1">
                                    <label>{{ translate('Length(cm)')}}</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control mb-3 prod_length" placeholder="{{ translate('Length')}}" name="length{{ $orderDetail->product->id}}" id="length{{ $orderDetail->product->id }}" required>
                                </div>
                               
                                <div class="col-md-1">
                                    <label>{{ translate('Height(cm)')}}</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control mb-3 prod_height" placeholder="{{ translate('Height')}}" name="height{{ $orderDetail->product->id }}" id="height{{ $orderDetail->product->id }}" required>
                                </div>
                                <div class="col-md-1">
                                    <label>{{ translate('Width(cm)')}}</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control mb-3 prod_width" placeholder="{{ translate('Width')}}" name="width{{ $orderDetail->product->id }}" id="width{{ $orderDetail->product->id }}" required>
                                </div>
                                
                                <div class="col-md-1">
                                    <label>{{ translate('Weight(kg)')}}</label>
                                </div>
                                
                                <div class="col-md-2">
                                    <input type="text" class="form-control mb-3 prod_weight" placeholder="{{ translate('Weight')}}" name="weight{{ $orderDetail->product->id }}" id="weight{{ $orderDetail->product->id }}" required>
                                </div>
                            </div>
                            
                            @endif
                            @endforeach
                            <h4>{{ translate('Total Product Details')}}</h4>
                            <div class="row">
                            <div class="col-md-1">
                                    <label>{{ translate('Length(cm)')}}</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control mb-3" placeholder="{{ translate('Length')}}" name="length" id="length" required>
                                </div>
                               
                                <div class="col-md-1">
                                    <label>{{ translate('Height(cm)')}}</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control mb-3" placeholder="{{ translate('Height')}}" name="height" id="height" required>
                                </div>
                                <div class="col-md-1">
                                    <label>{{ translate('Width(cm)')}}</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control mb-3" placeholder="{{ translate('Width')}}" name="width" id="width" required>
                                </div>
                                
                                <div class="col-md-1">
                                    <label>{{ translate('Weight(kg)')}}</label>
                                </div>
                                
                                <div class="col-md-2">
                                    <input type="text" class="form-control mb-3" placeholder="{{ translate('Weight')}}" name="weight" id="weight" required>
                                </div>
                            </div>
                                <div class="col-md-12 text-center">
                                <a class="btn btn-base-1" href="javascript:" id="check_price">{{  translate('Check Price & Availability') }}</a>
                                </div>
                                </div>
                                <div class="row pt-1 service_set" style="display: none;">
                                
                                <div class="col-md-2">
                                    <label>{{ translate('Mode/Service') }}</label>
                                </div>
                                <div class="col-md-4">
                                <select class="form-control mb-3 test11" data-placeholder="{{translate('Select your Service')}}" name="mode_type" id="mode_type" required>
                                <option value="">Select Mode</option>
                                @foreach(Config::get('constants.SERVICE_CURIOR') as $key=>$val)

                                <option value="{{$key}}" class="{{$key}}">{{$val}}</option>

                                @endforeach
                                                
                                </select>
                                </div>
                                <div class="col-md-2">
                                    <label>{{ translate('Delivery Cost')}}</label>
                                </div>
                                
                                <div class="col-md-4">
                                    <input type="text" class="form-control mb-3" placeholder="{{ translate('Delivery Cost')}}" name="dv_cost" id="dv_cost" readonly>
                                </div>
                                <div class="col-md-12 text-center">
                                <a class="btn btn-base-1" href="javascript:" id="confirm_order">{{  translate('Confirm Order') }}</a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    </form>
                </div>
               
            </div>
            
               
        </div>
    </div>
    @endif
  <?php if($order->curiorOrderDetails->count()!=0) {  ?>
      

    @php
$tracking_id = $order->curiorOrderDetails->where('seller_id', Auth::user()->id)->first()->tracking_id;
@endphp
<div class="row">
    <div class="col-md-4">
    <a class="btn btn-base-1 cancel_order" style="width:100%" id="{{$tracking_id}}">{{  translate('Cancel Order') }}</a>
    </div>
    <div class="col-md-4">
    <a class="btn btn-base-1 generate_silp" style="width:100%" id="{{$tracking_id}}">{{  translate('Generate Slip') }}</a>
    </div>
    <div class="col-md-4">
    <a class="btn btn-base-1 generate_manifest" style="width:100%" id="{{$tracking_id}}">{{  translate('Generate Manifest') }}</a>
    </div>
    </div>
   <?php } ?>
    @endif
   @if($tracking_dt!=null)

<div class="card mt-3">
        <div class="card-header py-2 px-3 ">
            <div class="heading-4 strong-600">{{ translate('Tracking Summary')}}</div>
            <div>Cariar Name :{{$tracking_dt->data->carrierName}}</div>
                <div>Tracking ID :{{$tracking_dt->data->awbNo}}</div>
        </div>
        <div class="card-body pb-0">
            <div class="row">
                
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <table class="table table-sm table-hover table-responsive-md">
                    <thead>
                    <tr>
                            <th class="strong-600" width="25%">{{ translate('Tracking Status')}}</th>
                            <th class="strong-600" width="25%">{{ translate('Tracking Location')}}</th>
                            <th class="strong-600" width="25%">{{ translate('Description')}}</th>
                            <th class="strong-600" width="25%">{{ translate('Time')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tracking_dt->data->events as $tracking_data )
                        @if(isset($tracking_data->status))
                        <tr>
                            <td class="strong-600" width="25%"><span class="badge badge--2 mr-4"><i class="bg-green"></i> {{ isset($tracking_data->status) ? Config::get('constants.TRACKING_TYPE')[$tracking_data->status] :""}}</span></td>
                            <td width="25%">{{ $tracking_data->Location }}</td>
                            <td width="25%">{{ $tracking_data->Remarks }}</td>
                            <?php
                            $date = date_create($tracking_data->Time);
                            $date_dt= date_format($date, 'd/m/Y h:i:s A');
                            ?>
                            <td width="25%">{{ $date_dt }}</td>
                        </tr>
                        @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
               
            </div>
        </div>
    </div>
@endif
@endif
    <div class="card mt-3">
        <div class="card-header py-2 px-3 ">
            <div class="heading-6 strong-600">{{ translate('Order Summary')}}</div>
        </div>
        <div class="card-body pb-0">
            <div class="row">
                <div class="col-lg-6">
                    <table class="details-table table">
                        <tr>
                            <td class="w-50 strong-600">{{ translate('Order Code')}}:</td>
                            <td>{{ $order->code }}</td>
                        </tr>
                        <tr>
                            <td class="w-50 strong-600">{{ translate('Customer')}}:</td>
                            <td>{{ json_decode($order->shipping_address)->name }}</td>
                        </tr>
                        <tr>
                            <td class="w-50 strong-600">{{ translate('Email')}}:</td>
                            @if ($order->user_id != null)
                            <td>{{ $order->user->email }}</td>
                            @endif
                        </tr>
                        <tr>
                            <td class="w-50 strong-600">{{ translate('Shipping address')}}:</td>
                            <td>{{ json_decode($order->shipping_address)->address }},
                                {{ json_decode($order->shipping_address)->city }},
                                {{ json_decode($order->shipping_address)->postal_code }},
                                {{ json_decode($order->shipping_address)->country }}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-lg-6">
                    <table class="details-table table">
                        <tr>
                            <td class="w-50 strong-600">{{ translate('Order date')}}:</td>
                            <td>{{ date('d-m-Y H:m A', $order->date) }}</td>
                        </tr>
                        <tr>
                            <td class="w-50 strong-600">{{ translate('Order status')}}:</td>
                            <td>{{ $status }}</td>
                        </tr>
                        <tr>
                            <td class="w-50 strong-600">{{ translate('Total order amount')}}:</td>
                            <td>{{ single_price($order->orderDetails->where('seller_id', Auth::user()->id)->sum('price') + $order->orderDetails->where('seller_id', Auth::user()->id)->sum('tax')) }}
                            </td>
                        </tr>
                        <tr>
                            <td class="w-50 strong-600">{{ translate('Contact')}}:</td>
                            <td>{{ json_decode($order->shipping_address)->phone }}</td>
                        </tr>
                        <tr>
                            <td class="w-50 strong-600">{{ translate('Order Type')}}:</td>
                            <td>{{ ucfirst(str_replace('_', ' ', $order->payment_type)) }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-9">
            <div class="card mt-4">
                <div class="card-header py-2 px-3 heading-6 strong-600">{{ translate('Order Details')}}</div>
                <div class="card-body pb-0">
                    <table class="details-table table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th width="40%">{{ translate('Product')}}</th>
                                <th>{{ translate('Variation')}}</th>
                                <th>{{ translate('Quantity')}}</th>
                                <th>{{ translate('Delivery Type')}}</th>
                                <th>{{ translate('Price')}}</th>
                                @if ($status == 'on_delivery')
                                <th></th>
                                @endif
                                @if ($refund_request_addon != null && $refund_request_addon->activated == 1)
                                <th>{{ translate('Refund')}}</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($order->orderDetails->where('seller_id', Auth::user()->id) as $key =>
                            $orderDetail)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>
                                    @if ($orderDetail->product != null)
                                    <a href="{{ route('product', $orderDetail->product->slug) }}"
                                        target="_blank">{{ $orderDetail->product->name }}</a>
                                    @else
                                    <strong>{{  translate('Product Unavailable') }}</strong>
                                    @endif
                                </td>
                                <td>
                                    {{ $orderDetail->variation }}
                                </td>
                                <td>
                                    {{ $orderDetail->quantity }}
                                </td>
                                <td>
                                    @if ($orderDetail->shipping_type != null && $orderDetail->shipping_type ==
                                    'home_delivery')
                                    {{  translate('Home Delivery') }}
                                    @elseif ($orderDetail->shipping_type == 'pickup_point')
                                    @if ($orderDetail->pickup_point != null)
                                    {{ $orderDetail->pickup_point->name }} ({{  translate('Pickip Point') }})
                                    @endif
                                    @endif
                                </td>
                                <td>{{ $orderDetail->price }}</td>
                                @if ($status == 'on_delivery' && $orderDetail->product->digital==1)
                                <td> <button type="button"  class="btn btn-styled btn-sm btn-base-1"
                                    onclick="licenseUpload('{{ $orderDetail->id }}','@if(!empty($orderDetail->license_key)){{ decrypt($orderDetail->license_key) }}    @endif ')">@if(!empty($orderDetail->license_key)){{  translate('View') }}  @else {{  translate('Licence Upload') }}  @endif</td>
                                    @endif
                                    @if ($refund_request_addon != null && $refund_request_addon->activated == 1)
                               
                                <td>
                                    @if ($orderDetail->product != null && $orderDetail->product->refundable != 0 &&
                                    $orderDetail->refund_request == null)
                                    <button type="submit" class="btn btn-styled btn-sm btn-base-1"
                                        onclick="send_refund_request('{{ $orderDetail->id }}')">{{  translate('Send') }}</button>
                                    @elseif ($orderDetail->refund_request != null &&
                                    $orderDetail->refund_request->status == 0)
                                    <span class="strong-600">{{  translate('Pending') }}</span>
                                    @elseif ($orderDetail->refund_request != null &&
                                    $orderDetail->refund_request->status == 1)
                                    <span class="strong-600">{{  translate('Paid') }}</span>
                                    @elseif ($orderDetail->refund_request != null &&
                                    $orderDetail->refund_request->status == 2)
                                    <span class="strong-600">{{  translate('Rejected') }}</span>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card mt-4">
                <div class="card-header py-2 px-3 heading-6 strong-600">{{ translate('Order Ammount')}}</div>
                <div class="card-body pb-0">
                    <table class="table details-table">
                        <tbody>
                            <tr>
                                <th>{{ translate('Subtotal')}}</th>
                                <td class="text-right">
                                    <span
                                        class="strong-600">{{ single_price($order->orderDetails->where('seller_id', Auth::user()->id)->sum('price')) }}</span>
                                </td>
                            </tr>
                            <tr>
                                <th>{{ translate('Shipping')}}</th>
                                <td class="text-right">
                                    <span
                                        class="text-italic">{{ single_price($order->orderDetails->where('seller_id', Auth::user()->id)->sum('shipping_cost')) }}</span>
                                </td>
                            </tr>
                            <tr>
                                <th>{{ translate('Tax')}}</th>
                                <td class="text-right">
                                    <span
                                        class="text-italic">{{ single_price($order->orderDetails->where('seller_id', Auth::user()->id)->sum('tax')) }}</span>
                                </td>
                            </tr>
                            <tr>
                                <th><span class="strong-600">{{ translate('Total')}}</span></th>
                                <td class="text-right">
                                    <strong>
                                        <span>{{ single_price($order->orderDetails->where('seller_id', Auth::user()->id)->sum('price') + $order->orderDetails->where('seller_id', Auth::user()->id)->sum('tax') + $order->orderDetails->where('seller_id', Auth::user()->id)->sum('shipping_cost')) }}
                                        </span>
                                    </strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @if ($status == 'on_delivery')
    <div class="modal" id="license_details" data-backdrop="static">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Add License key</h4>
              <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> -->
            </div><div class="container"></div>
            <div class="modal-body">
                
               
                    <div class="card-header py-2 px-3 ">
                        <div  style="font-weight: bold;color:red" >{{ translate('Note:-For each product you have maximum 2 chance to upload a license key')}}</div>
                    </div>
                    <div class="card-body pb-0">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group clearfix ">
                                    <div class="row">
                                        <input type="hidden" id="orderdetailsid" value="">
                                        <div class="col-md-2">
                                            <label>{{ translate('License key')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-6">
                                        <input type="text" value="" class="form-control mb-3" id="license_key" name="license_key"
                                                placeholder="{{ translate('License key')}}" required>
                                        </div>
                                        <div class="col-md-4">
                                            <button type="button" class="btn btn-styled btn-sm btn-base-1" id="savelicensebtn" class="btn btn-primary" onclick="saveliscense()">Save </button>
                                        </div>
                                    </div>
            
                                </div>
                            </div>
            
                        </div>
                        <div class="row">
                            <div class="col-lg-12" id="listlicense">
                             
                            </div>
                         
                        </div>
                    </div>
                
                
            </div>
            <div class="modal-footer">
              <a href="#" class="btn btn-styled btn-sm btn-base-1" onclick="modelClose()"  class="btn">Close</a>
              <!-- <a href="#" id="savelicensebtn" class="btn btn-primary" onclick="saveliscense()">Save changes</a> -->
            </div>
          </div>
        </div>
    </div>
    @endif

  

  
<script type="text/javascript">

(function( $ ){
   $.fn.sum=function () {
    var sum=0;
        $(this).each(function(index, element){
            if($(element).val()!="")
            sum += parseFloat($(element).val());
        });
    return sum;
    }; 
})( jQuery );
$(".prod_length").keyup(function(){
    $('#length').val($('.prod_length').sum());
});
$(".prod_height").keyup(function(){
    $('#height').val($('.prod_height').sum());
});
$(".prod_width").keyup(function(){
    $('#width').val($('.prod_width').sum());
});
$(".prod_weight").keyup(function(){
    $('#weight').val($('.prod_weight').sum());
});

    $('#update_delivery_status').on('change', function(){
        var order_id = {{ $order->id }};
        var status = $('#update_delivery_status').val();
        $.post('{{ route('orders.update_delivery_status') }}', {_token:'{{ @csrf_token() }}',order_id:order_id,status:status}, function(data){
           $('#order_details').modal('hide');
           showFrontendAlert('success', 'Order status has been updated');
            location.reload().setTimeOut(500);
        });
    });

    $('#update_payment_status').on('change', function(){
        var order_id = {{ $order->id }};
        var status = $('#update_payment_status').val();
        $.post('{{ route('orders.update_payment_status') }}', {_token:'{{ @csrf_token() }}',order_id:order_id,status:status}, function(data){
            $('#order_details').modal('hide');
            //console.log(data);
            showFrontendAlert('success', 'Payment status has been updated');
            location.reload().setTimeOut(500);
        });
    });

    $('#check_price').on('click', function(){
        $(".service_set").hide();
        $("#mode_type").val("");
        $("#dv_cost").val("");
        var order_id = {{ $order->id }};
        var length = $('#length').val();
        var height = $('#height').val();
        var width = $('#width').val();
        var weight = $('#weight').val();
        var mode_type=$("#mode_type").val();
        $.post('{{ route('orders.curior_check_price') }}', {_token:'{{ @csrf_token() }}',order_id:order_id,length:length,height:height,width:width,weight:weight,mode_type:mode_type}, function(data){
            // if(data.calculatedPrice)
          $(".service_set").show();
            var obj = JSON.parse(data);
            $.each( obj.service, function( key, value ) {
            if(value==false){
               
                $("."+key).hide();
            }
            });
            $("#mode_type").on('change', function(){ 
                  
                   var box_value= this.value;
                //    var chekc_cost="pricing."+box_value;
                   $.each(obj.pricing, function( key, value ) {
            if(key==box_value){
                $("#dv_cost").val(value);
                // alert(value);
            }
            });
                //    alert(obj.pricing.surface-10kg);
                    // $("#dv_cost").val(obj.pricing.box_value);
            });
               
        });
    });
    $('#confirm_order').on('click', function(){
        $("#set_order").submit();
        // var order_id = {{ $order->id }};
        // var length = $('#length').val();
        // var height = $('#height').val();
        // var width = $('#width').val();
        // var weight = $('#weight').val();
        // var mode_type=$("#mode_type option:selected").text();
        // $.post('{{ route('orders.set_order') }}', {_token:'{{ @csrf_token() }}',order_id:order_id,length:length,height:height,width:width,weight:weight,mode_type:mode_type}, function(data){
        //     //   $('#order_details').modal('hide');
        //     //console.log(data);
        //    // showFrontendAlert('success', 'Order Created Successfully');
        //    // location.reload().setTimeOut(500);
        // });
    });
    $('.cancel_order').on('click', function(){
        var order_id = this.id;
        Swal.fire({
            title: "Are you sure?",
            text: "Once cancled, you will not be able Process further",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        }).then((result) => {
          if (result.value) {
            $.post('{{ route('orders.cancel_order') }}', {_token:'{{ @csrf_token() }}',order_id:order_id}, function(data){
            //   $('#order_details').modal('hide');
            //console.log(data);
            showFrontendAlert('success', 'Order Canceled Successfully');
            location.reload().setTimeOut(500);
        });
          }
         
        });
       

        
    });
    $('.generate_silp').on('click', function(){
        var order_id = this.id;
        $.post('{{ route('orders.generate_silp') }}', {_token:'{{ @csrf_token() }}',order_id:order_id}, function(data){
// alert(data['filepath']);
            window.open(data, '_blank');
            //   $('#order_details').modal('hide');
            //console.log(data);
            // showFrontendAlert('success', 'Order Created Successfully');
            // location.reload().setTimeOut(500);
        });
    });
    $('.generate_manifest').on('click', function(){
        var order_id = this.id;
        $.post('{{ route('orders.generate_manifest') }}', {_token:'{{ @csrf_token() }}',order_id:order_id}, function(data){
            //   $('#order_details').modal('hide');
            //console.log(data);
            // showFrontendAlert('success', 'Order Created Successfully');
            // location.reload().setTimeOut(500);
            window.open(data, '_blank')
        });
    });

    function licenseUpload(orderdetailsid,licensekey){
        
                $.ajax({
                type: 'post',
                url: '{{ route("orders.listlicense") }}',
                data: {_token:'{{ @csrf_token() }}',orderdetailsid:orderdetailsid},

                dataType: 'json',
                success: function (response) {

                $('#license_details').modal('show');
                $('#orderdetailsid').val(orderdetailsid);
                $('#license_key').val(licensekey);
                $('#listlicense').html(response.html);
                if(response.dataCount==2){
                $('#savelicensebtn').hide();
                $("#license_key").attr('disabled',true);
                }



                },
                error: function (jqXHR, textStatus, errorThrown) {

                var msg = "<strong>Failed to Load data.</strong><br/>";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                } else {
                msg += "Error(s):<strong><ul>";
                $.each(jqXHR.responseJSON['errors'], function (key, value) {
                msg += "<li>" + value + "</li>";
                });
                msg += "</ul></strong>";
                }
                }
                Swal.fire({
                title: 'Error',
                icon: 'error',
                html:msg,

                showCloseButton: true,
                showCancelButton: true,
                focusConfirm: false,


                });
                

                }
                });


       
  }
  function saveliscense(){
                var license_key = $('#license_key').val();
                var orderdetailsid=$('#orderdetailsid').val();
                var order_id = {{ $order->id }};

                $.ajax({
                type: 'post',
                url: '{{ route("orders.licenseUpload") }}',
                data: {_token:'{{ @csrf_token() }}',license_key:license_key,order_id:order_id,orderdetailsid:orderdetailsid},

                dataType: 'json',
                success: function (response) {
                $('#order_details').modal('hide');
                $('#license_details').modal('hide');
                showFrontendAlert('success', response.msg);


                },
                error: function (jqXHR, textStatus, errorThrown) {

                var msg = "<strong>Failed to Load data.</strong><br/>";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                } else {
                msg += "Error(s):<strong><ul>";
                $.each(jqXHR.responseJSON['errors'], function (key, value) {
                msg += "<li>" + value + "</li>";
                });
                msg += "</ul></strong>";
                }
                }
                Swal.fire({
                title: 'Error',
                icon: 'error',
                html:msg,

                showCloseButton: true,
                showCancelButton: true,
                focusConfirm: false,


                });
                // showFrontendAlert('error', msg);

                }
                });
  }
function modelClose(){
$('#license_details').modal('hide');

}
 
</script>