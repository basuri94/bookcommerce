@extends('frontend.layouts.app')

@section('content')
            <!-- Slider Arae Start -->
            <div class="slider-area">
                <div class="slider-active-3 owl-carousel slider-hm8 owl-dot-style">
                    <!-- Slider Single Item Start -->
                    <div class="slider-height-6 d-flex align-items-start justify-content-start bg-img" style="background-image: url(assets/images/slider-image/sample-12.jpg);">
                        <!-- <div class="container">
                            <div class="slider-content-5 slider-animated-1 text-right">
                                <span class="animated">FRESH FRUIT-NATURAL</span>
                                <h1 class="animated">
                                    Pro Skin Whitening <br />
                                    Face Creams
                                </h1>
                                <a href="shop-4-column.html" class="shop-btn animated">SHOP NOW</a>
                            </div>
                        </div> -->
                    </div>
                    <!-- Slider Single Item End -->
                    <!-- Slider Single Item Start -->
                    <div class="slider-height-6 d-flex align-items-start justify-content-start bg-img" style="background-image: url(assets/images/slider-image/sample-15.jpg);">
                        <!-- <div class="container">
                            <div class="slider-content-5 slider-animated-1 text-right">
                                <span class="animated">MOROCCAN ARGAN OIL</span>
                                <h1 class="animated">
                                    Argan Oil A Beauty<br />
                                    Secret Dating Back Over
                                </h1>
                                <a href="shop-4-column.html" class="shop-btn animated">SHOP NOW</a>
                            </div>
                        </div> -->
                    </div>
                    <div class="slider-height-6 d-flex align-items-start justify-content-start bg-img" style="background-image: url(assets/images/slider-image/sample-15.jpg);">
                        <!-- <div class="container">
                            <div class="slider-content-5 slider-animated-1 text-right">
                                <span class="animated">MOROCCAN ARGAN OIL</span>
                                <h1 class="animated">
                                    Argan Oil A Beauty<br />
                                    Secret Dating Back Over
                                </h1>
                                <a href="shop-4-column.html" class="shop-btn animated">SHOP NOW</a>
                            </div>
                        </div> -->
                    </div>
                    <div class="slider-height-6 d-flex align-items-start justify-content-start bg-img" style="background-image: url(assets/images/slider-image/sample-15.jpg);">
                        <!-- <div class="container">
                            <div class="slider-content-5 slider-animated-1 text-right">
                                <span class="animated">MOROCCAN ARGAN OIL</span>
                                <h1 class="animated">
                                    Argan Oil A Beauty<br />
                                    Secret Dating Back Over
                                </h1>
                                <a href="shop-4-column.html" class="shop-btn animated">SHOP NOW</a>
                            </div>
                        </div> -->
                    </div>
                    <div class="slider-height-6 d-flex align-items-start justify-content-start bg-img" style="background-image: url(assets/images/slider-image/sample-15.jpg);">
                        <!-- <div class="container">
                            <div class="slider-content-5 slider-animated-1 text-right">
                                <span class="animated">MOROCCAN ARGAN OIL</span>
                                <h1 class="animated">
                                    Argan Oil A Beauty<br />
                                    Secret Dating Back Over
                                </h1>
                                <a href="shop-4-column.html" class="shop-btn animated">SHOP NOW</a>
                            </div>
                        </div> -->
                    </div>
                    
                    <!-- Slider Single Item End -->
                </div>
            </div>
            <!-- Slider Arae End -->

            
            <br><br>

            <!-- Category Area Start -->
            <section class="categorie-area">
                <div class="container">
                  <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 category-list mb-30px">
                        <div class="category-thumb">
                            <a href="shop-4-column.html">
                                <img src="assets/images/product-image/1.png" alt="" />
                            </a>
                        </div>
                        <div class="desc-listcategoreis">
                            <div class="name_categories">
                                <h4>Fresh Vegetables</h4>
                            </div>
                            <span class="number_product">Starting At 50</span>
                            <a href="shop-4-column.html"> Shop Now <i class="ion-android-arrow-dropright-circle"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 category-list">
                        <div class="category-thumb">
                            <a href="shop-4-column.html">
                                <img src="assets/images/product-image/2.png" alt="" />
                            </a>
                        </div>
                        <div class="desc-listcategoreis">
                            <div class="name_categories">
                                <h4>Fresh Salad & Dips</h4>
                            </div>
                            <span class="number_product">Starting At 50</span>
                            <a href="shop-4-column.html"> Shop Now <i class="ion-android-arrow-dropright-circle"></i></a>
                        </div>
                    </div>
               
               
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 category-list mb-30px">
                        <div class="category-thumb">
                            <a href="shop-4-column.html">
                                <img src="assets/images/product-image/3.png" alt="" />
                            </a>
                        </div>
                        <div class="desc-listcategoreis">
                            <div class="name_categories">
                                <h4>Fresh Fruit</h4>
                            </div>
                            <span class="number_product">Starting At 50</span>
                            <a href="shop-4-column.html"> Shop Now <i class="ion-android-arrow-dropright-circle"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 category-list">
                        <div class="category-thumb">
                            <a href="shop-4-column.html">
                                <img src="assets/images/product-image/4.png" alt="" />
                            </a>
                        </div>
                        <div class="desc-listcategoreis">
                            <div class="name_categories">
                                <h4>Baking & Cooking</h4>
                            </div>
                            <span class="number_product">Starting At 50</span>
                            <a href="shop-4-column.html"> Shop Now <i class="ion-android-arrow-dropright-circle"></i></a>
                        </div>
                    </div>
               
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 category-list mb-30px">
                        <div class="category-thumb">
                            <a href="shop-4-column.html">
                                <img src="assets/images/product-image/5.png" alt="" />
                            </a>
                        </div>
                        <div class="desc-listcategoreis">
                            <div class="name_categories">
                                <h4>Fresh Cream </h4>
                            </div>
                            <span class="number_product">Starting At 50</span>
                            <a href="shop-4-column.html"> Shop Now <i class="ion-android-arrow-dropright-circle"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 category-list">
                        <div class="category-thumb">
                            <a href="shop-4-column.html">
                                <img src="assets/images/product-image/6.png" alt="" />
                            </a>
                        </div>
                        <div class="desc-listcategoreis">
                            <div class="name_categories">
                                <h4>Milk, Butter</h4>
                            </div>
                            <span class="number_product">Starting At 50</span>
                            <a href="shop-4-column.html"> Shop Now <i class="ion-android-arrow-dropright-circle"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 category-list">
                        <div class="category-thumb">
                            <a href="shop-4-column.html">
                                <img src="assets/images/product-image/4.png" alt="" />
                            </a>
                        </div>
                        <div class="desc-listcategoreis">
                            <div class="name_categories">
                                <h4>Baking & Cooking</h4>
                            </div>
                            <span class="number_product">Starting At 50</span>
                            <a href="shop-4-column.html"> Shop Now <i class="ion-android-arrow-dropright-circle"></i></a>
                        </div>
                    </div>
               
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 category-list mb-30px">
                        <div class="category-thumb">
                            <a href="shop-4-column.html">
                                <img src="assets/images/product-image/5.png" alt="" />
                            </a>
                        </div>
                        <div class="desc-listcategoreis">
                            <div class="name_categories">
                                <h4>Fresh Cream</h4>
                            </div>
                            <span class="number_product">Starting At 50</span>
                            <a href="shop-4-column.html"> Shop Now <i class="ion-android-arrow-dropright-circle"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 category-list">
                        <div class="category-thumb">
                            <a href="shop-4-column.html">
                                <img src="assets/images/product-image/6.png" alt="" />
                            </a>
                        </div>
                        <div class="desc-listcategoreis">
                            <div class="name_categories">
                                <h4>Milk, Butter</h4>
                            </div>
                            <span class="number_product">Starting At 50</span>
                            <a href="shop-4-column.html"> Shop Now <i class="ion-android-arrow-dropright-circle"></i></a>
                        </div>
                    </div>
                  </div>
                </div>
            </section>
            <!-- Category Area End  -->
            <br><br>
            <!-- Best Sells Area Start -->
            <div class="row">
                <div class="col-md-12">
                    <!-- Section Title -->
                    <div class="section-title">
                        <h2>Trending Product</h2>
                        <p>Add products to weekly line up</p>
                    </div>
                    <!-- Section Title -->
                </div>
            </div>
            <section class="best-sells-area">
                
                <div class="container">
                   
                    <!-- Best Sell Slider Carousel Start -->
                    <div class="best-sell-slider owl-carousel owl-nav-style">
                        <!-- Single Item -->
                        <div class="list-product-2">
                            <article class="list-product mb-30px">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img" src="assets/images/product-image/Layer 58.png" alt="" />
                                        <img class="second-img" src="assets/images/product-image/Layer 58.png" alt="" />
                                    </a>
                                    <div class="quick-view">
                                        <a class="quick_view" href="#" data-link-action="quickview" title="Quick view" data-toggle="modal" data-target="#exampleModal">
                                            <i class="ion-ios-search-strong"></i>
                                        </a>
                                    </div>
                                </div>
                                <ul class="product-flag">
                                    <li class="new">66% off</li>
                                </ul>
                                <div class="product-decs1">
                                    <div class="product-decs2">
                                        <div class="rating-product" style="float:left;">
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                        </div>
                                        <div class="pricing-meta"  style="float:right;">
                                            <ul>
                                                <li class="current-price">₹134.00</li>
                                                <li class="old-price">₹180.00</li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    

                                    <h2 ><a href="single-product.html" class="product-link">New Balance Fresh </a></h2>
                                </div>
                                <div class="add-to-link">
                                    <ul>
                                        
                                        <li>
                                            <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                                        </li>
                                        <li style="float: none; margin-left:6%;"><a class="cart-btn" href="#">ADD TO CART </a></li>
                                        <li class="cart">
                                            <a href="compare.html"><i class="ion-ios-shuffle-strong"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </article>
                            <article class="list-product">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img" src="assets/images/product-image/Layer 52.png" alt="" />
                                        <img class="second-img" src="assets/images/product-image/Layer 52.png" alt="" />
                                    </a>
                                    <div class="quick-view">
                                        <a class="quick_view" href="#" data-link-action="quickview" title="Quick view" data-toggle="modal" data-target="#exampleModal">
                                            <i class="ion-ios-search-strong"></i>
                                        </a>
                                    </div>
                                </div>
                                <ul class="product-flag">
                                    <li class="new">66% off</li>
                                </ul>
                                <div class="product-decs1">
                                    <div class="product-decs2">
                                        <div class="rating-product" style="float:left;">
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                        </div>
                                        <div class="pricing-meta"  style="float:right;">
                                            <ul>
                                                <li class="current-price">₹134.00</li>
                                                <li class="old-price">₹180.00</li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    

                                    <h2 ><a href="single-product.html" class="product-link">New Balance Fresh </a></h2>
                                </div>
                                <div class="add-to-link">
                                    <ul>
                                        
                                        <li>
                                            <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                                        </li>
                                        <li style="float: none; margin-left:6%;"><a class="cart-btn" href="#">ADD TO CART </a></li>
                                        <li class="cart">
                                            <a href="compare.html"><i class="ion-ios-shuffle-strong"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </article>
                        </div>
                        <!-- Single Item -->
                        <div class="list-product-2">
                            <article class="list-product">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img" src="assets/images/product-image/Layer 55.png" alt="" />
                                        <img class="second-img" src="assets/images/product-image/Layer 55.png" alt="" />
                                    </a>
                                    <div class="quick-view">
                                        <a class="quick_view" href="#" data-link-action="quickview" title="Quick view" data-toggle="modal" data-target="#exampleModal">
                                            <i class="ion-ios-search-strong"></i>
                                        </a>
                                    </div>
                                </div>
                                <ul class="product-flag">
                                    <li class="new">66% off</li>
                                </ul>
                                <div class="product-decs1">
                                    <div class="product-decs2">
                                        <div class="rating-product" style="float:left;">
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                        </div>
                                        <div class="pricing-meta"  style="float:right;">
                                            <ul>
                                                <li class="current-price">₹134.00</li>
                                                <li class="old-price">₹180.00</li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    

                                    <h2 ><a href="single-product.html" class="product-link">New Balance Fresh </a></h2>
                                </div>
                                <div class="add-to-link">
                                    <ul>
                                        
                                        <li>
                                            <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                                        </li>
                                        <li style="float: none; margin-left:6%;"><a class="cart-btn" href="#">ADD TO CART </a></li>
                                        <li class="cart">
                                            <a href="compare.html"><i class="ion-ios-shuffle-strong"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </article>
                            <article class="list-product">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img" src="assets/images/product-image/Layer 58.png" alt="" />
                                        <img class="second-img" src="assets/images/product-image/Layer 58.png" alt="" />
                                    </a>
                                    <div class="quick-view">
                                        <a class="quick_view" href="#" data-link-action="quickview" title="Quick view" data-toggle="modal" data-target="#exampleModal">
                                            <i class="ion-ios-search-strong"></i>
                                        </a>
                                    </div>
                                </div>
                                <ul class="product-flag">
                                    <li class="new">66% off</li>
                                </ul>
                                <div class="product-decs1">
                                    <div class="product-decs2">
                                        <div class="rating-product" style="float:left;">
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                        </div>
                                        <div class="pricing-meta"  style="float:right;">
                                            <ul>
                                                <li class="current-price">₹134.00</li>
                                                <li class="old-price">₹180.00</li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    

                                    <h2 ><a href="single-product.html" class="product-link">New Balance Fresh </a></h2>
                                </div>
                                <div class="add-to-link">
                                    <ul>
                                        
                                        <li>
                                            <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                                        </li>
                                        <li style="float: none; margin-left:6%;"><a class="cart-btn" href="#">ADD TO CART </a></li>
                                        <li class="cart">
                                            <a href="compare.html"><i class="ion-ios-shuffle-strong"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </article>
                        </div>
                        <!-- Single Item -->
                        <div class="list-product-2">
                            <article class="list-product mb-30px">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img" src="assets/images/product-image/Layer 59.png" alt="" />
                                        <img class="second-img" src="assets/images/product-image/Layer 59.png" alt="" />
                                    </a>
                                    <div class="quick-view">
                                        <a class="quick_view" href="#" data-link-action="quickview" title="Quick view" data-toggle="modal" data-target="#exampleModal">
                                            <i class="ion-ios-search-strong"></i>
                                        </a>
                                    </div>
                                </div>
                                <ul class="product-flag">
                                    <li class="new">66% off</li>
                                </ul>
                                <div class="product-decs1">
                                    <div class="product-decs2">
                                        <div class="rating-product" style="float:left;">
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                        </div>
                                        <div class="pricing-meta"  style="float:right;">
                                            <ul>
                                                <li class="current-price">₹134.00</li>
                                                <li class="old-price">₹180.00</li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    

                                    <h2 ><a href="single-product.html" class="product-link">New Balance Fresh </a></h2>
                                </div>
                                <div class="add-to-link">
                                    <ul>
                                        
                                        <li>
                                            <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                                        </li>
                                        <li style="float: none; margin-left:6%;"><a class="cart-btn" href="#">ADD TO CART </a></li>
                                        <li class="cart">
                                            <a href="compare.html"><i class="ion-ios-shuffle-strong"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </article>
                            <article class="list-product">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img" src="assets/images/product-image/Layer 60.png" alt="" />
                                        <img class="second-img" src="assets/images/product-image/Layer 60.png" alt="" />
                                    </a>
                                    <div class="quick-view">
                                        <a class="quick_view" href="#" data-link-action="quickview" title="Quick view" data-toggle="modal" data-target="#exampleModal">
                                            <i class="ion-ios-search-strong"></i>
                                        </a>
                                    </div>
                                </div>
                                <ul class="product-flag">
                                    <li class="new">66% off</li>
                                </ul>
                                <div class="product-decs1">
                                    <div class="product-decs2">
                                        <div class="rating-product" style="float:left;">
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                        </div>
                                        <div class="pricing-meta"  style="float:right;">
                                            <ul>
                                                <li class="current-price">₹134.00</li>
                                                <li class="old-price">₹180.00</li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    

                                    <h2 ><a href="single-product.html" class="product-link">New Balance Fresh </a></h2>
                                </div>
                                <div class="add-to-link">
                                    <ul>
                                        
                                        <li>
                                            <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                                        </li>
                                        <li style="float: none; margin-left:6%;"><a class="cart-btn" href="#">ADD TO CART </a></li>
                                        <li class="cart">
                                            <a href="compare.html"><i class="ion-ios-shuffle-strong"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </article>
                        </div>
                        <!-- Single Item -->
                        <div class="list-product-2">
                            <article class="list-product mb-30px">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img" src="assets/images/product-image/Layer 61.png" alt="" />
                                        <img class="second-img" src="assets/images/product-image/Layer 61.png" alt="" />
                                    </a>
                                    <div class="quick-view">
                                        <a class="quick_view" href="#" data-link-action="quickview" title="Quick view" data-toggle="modal" data-target="#exampleModal">
                                            <i class="ion-ios-search-strong"></i>
                                        </a>
                                    </div>
                                </div>
                                <ul class="product-flag">
                                    <li class="new">66% off</li>
                                </ul>
                                <div class="product-decs1">
                                    <div class="product-decs2">
                                        <div class="rating-product" style="float:left;">
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                        </div>
                                        <div class="pricing-meta"  style="float:right;">
                                            <ul>
                                                <li class="current-price">₹134.00</li>
                                                <li class="old-price">₹180.00</li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    

                                    <h2 ><a href="single-product.html" class="product-link">New Balance Fresh </a></h2>
                                </div>
                                <div class="add-to-link">
                                    <ul>
                                        
                                        <li>
                                            <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                                        </li>
                                        <li style="float: none; margin-left:6%;"><a class="cart-btn" href="#">ADD TO CART </a></li>
                                        <li class="cart">
                                            <a href="compare.html"><i class="ion-ios-shuffle-strong"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </article>
                            <article class="list-product">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img" src="assets/images/product-image/Layer 62.png" alt="" />
                                        <img class="second-img" src="assets/images/product-image/Layer 62.png" alt="" />
                                    </a>
                                    <div class="quick-view">
                                        <a class="quick_view" href="#" data-link-action="quickview" title="Quick view" data-toggle="modal" data-target="#exampleModal">
                                            <i class="ion-ios-search-strong"></i>
                                        </a>
                                    </div>
                                </div>
                                <ul class="product-flag">
                                    <li class="new">66% off</li>
                                </ul>
                                <div class="product-decs1">
                                    <div class="product-decs2">
                                        <div class="rating-product" style="float:left;">
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                        </div>
                                        <div class="pricing-meta"  style="float:right;">
                                            <ul>
                                                <li class="current-price">₹134.00</li>
                                                <li class="old-price">₹180.00</li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    

                                    <h2 ><a href="single-product.html" class="product-link">New Balance Fresh </a></h2>
                                </div>
                                <div class="add-to-link">
                                    <ul>
                                        
                                        <li>
                                            <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                                        </li>
                                        <li style="float: none; margin-left:6%;"><a class="cart-btn" href="#">ADD TO CART </a></li>
                                        <li class="cart">
                                            <a href="compare.html"><i class="ion-ios-shuffle-strong"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </article>
                        </div>
                        <!-- Single Item -->
                        <div class="list-product-2">
                            <article class="list-product mb-30px">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img" src="assets/images/product-image/Layer 63.png" alt="" />
                                        <img class="second-img" src="assets/images/product-image/Layer 63.png" alt="" />
                                    </a>
                                    <div class="quick-view">
                                        <a class="quick_view" href="#" data-link-action="quickview" title="Quick view" data-toggle="modal" data-target="#exampleModal">
                                            <i class="ion-ios-search-strong"></i>
                                        </a>
                                    </div>
                                </div>
                                <ul class="product-flag">
                                    <li class="new">66% off</li>
                                </ul>
                                <div class="product-decs1">
                                    <div class="product-decs2">
                                        <div class="rating-product" style="float:left;">
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                        </div>
                                        <div class="pricing-meta"  style="float:right;">
                                            <ul>
                                                <li class="current-price">₹134.00</li>
                                                <li class="old-price">₹180.00</li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    

                                    <h2 ><a href="single-product.html" class="product-link">New Balance Fresh </a></h2>
                                </div>
                                <div class="add-to-link">
                                    <ul>
                                        
                                        <li>
                                            <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                                        </li>
                                        <li style="float: none; margin-left:6%;"><a class="cart-btn" href="#">ADD TO CART </a></li>
                                        <li class="cart">
                                            <a href="compare.html"><i class="ion-ios-shuffle-strong"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </article>
                            <article class="list-product">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img" src="assets/images/product-image/Layer 64.png" alt="" />
                                        <img class="second-img" src="assets/images/product-image/Layer 64.png" alt="" />
                                    </a>
                                    <div class="quick-view">
                                        <a class="quick_view" href="#" data-link-action="quickview" title="Quick view" data-toggle="modal" data-target="#exampleModal">
                                            <i class="ion-ios-search-strong"></i>
                                        </a>
                                    </div>
                                </div>
                                <ul class="product-flag">
                                    <li class="new">66% off</li>
                                </ul>
                                <div class="product-decs1">
                                    <div class="product-decs2">
                                        <div class="rating-product" style="float:left;">
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                        </div>
                                        <div class="pricing-meta"  style="float:right;">
                                            <ul>
                                                <li class="current-price">₹134.00</li>
                                                <li class="old-price">₹180.00</li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    

                                    <h2 ><a href="single-product.html" class="product-link">New Balance Fresh </a></h2>
                                </div>
                                <div class="add-to-link">
                                    <ul>
                                        
                                        <li>
                                            <a href="wishlist.html"><i class="ion-android-favorite-outline"></i></a>
                                        </li>
                                        <li style="float: none; margin-left:6%;"><a class="cart-btn" href="#">ADD TO CART </a></li>
                                        <li class="cart">
                                            <a href="compare.html"><i class="ion-ios-shuffle-strong"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </article>
                        </div>
                        
                    </div>
                    <!-- Best Sell Slider Carousel End -->
                </div>
            </section>
            <!-- Best Sell Area End -->
             <!-- Banner Area Start -->
             <div class="banner-3-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mb-res-xs-30">
                            <div class="banner-wrapper mb-30px">
                                <a href="shop-4-column.html"><img src="assets/images/banner-image/long.png" alt="" style="height: 660px;"/></a>
                            </div>
                            
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="banner-wrapper mb-30px">
                                        <a href="shop-4-column.html"><img src="assets/images/banner-image/small.png" alt="" /></a>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="banner-wrapper mb-30px">
                                        <a href="shop-4-column.html"><img src="assets/images/banner-image/small_1.png" alt="" /></a>
                                    </div>
                                </div>
                            </div>
                            <div class="banner-wrapper">
                                <a href="shop-4-column.html"><img src="assets/images/banner-image/small baner.png" alt="" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Banner Area End -->
            <!-- Banner Area Start -->
            <div class="banner-3-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-res-xs-30">
                            <div class="banner-wrapper mb-30px">
                                <a href="shop-4-column.html"><img src="assets/images/banner-image/Long_Banner.png" alt="" /></a>
                              
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- Banner Area End -->
           
            <!-- Feature Area Start -->
            <section class="feature-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Section Title -->
                            <div class="section-title">
                                <h2>Top Categories</h2>
                                <p>Add products to weekly line up</p>
                            </div>
                            <!-- Section Title -->
                        </div>
                    </div>
                    <!-- Feature Slider Start -->
                    <div class="feature-slider owl-carousel owl-nav-style">
                        <!-- Single Item -->
                        <div class="feature-slider-item">
                            <article class="list-product">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img" src="assets/images/product-image/Layer 49.png" alt="" />
                                        <img class="second-img" src="assets/images/product-image/cLayer 49.png" alt="" />
                                    </a>
                                    
                                </div>
                                <div class="product-decs">
                                    
                                    <h2><a href="single-product.html" class="product-link" style="color:#373737;">Mobile Tablate</a></h2>
                                    <div class="rating-product">
                                        iphone
                                    </div>
                                    <div class="rating-product">
                                        Nokia
                                    </div>
                                    <div class="rating-product">
                                        Samsung
                                    </div>
                                    
                                    
                                    <div class="pricing-meta">
                                        <ul>
                                           <li class="old-price not-cut" style="color:#ff7200;border-bottom:1px solid #ff7200;"> <a>View More ..</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </article>
                            <article class="list-product">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img" src="assets/images/product-image/Layer 50.png" alt="" />
                                        <img class="second-img" src="assets/images/product-image/Layer 50.png" alt="" />
                                    </a>
                                    
                                </div>
                                <div class="product-decs">
                                    
                                    <h2><a href="single-product.html" class="product-link" style="color:#373737;">Mobile Tablate</a></h2>
                                    <div class="rating-product">
                                        iphone
                                    </div>
                                    <div class="rating-product">
                                        Nokia
                                    </div>
                                    <div class="rating-product">
                                        Samsung
                                    </div>
                                    
                                    
                                    <div class="pricing-meta">
                                        <ul>
                                           <li class="old-price not-cut" style="color:#ff7200;border-bottom:1px solid #ff7200;"> <a>View More ..</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <!-- Single Item -->
                        <div class="feature-slider-item">
                            <article class="list-product">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img" src="assets/images/product-image/Layer 51.png" alt="" />
                                        <img class="second-img" src="assets/images/product-image/Layer 51.png" alt="" />
                                    </a>
                                    
                                </div>
                                <div class="product-decs">
                                    
                                    <h2><a href="single-product.html" class="product-link" style="color:#373737;">Mobile Tablate</a></h2>
                                    <div class="rating-product">
                                        iphone
                                    </div>
                                    <div class="rating-product">
                                        Nokia
                                    </div>
                                    <div class="rating-product">
                                        Samsung
                                    </div>
                                    
                                    
                                    <div class="pricing-meta">
                                        <ul>
                                           <li class="old-price not-cut" style="color:#ff7200;border-bottom:1px solid #ff7200;"> <a>View More ..</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </article>
                            <article class="list-product">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img" src="assets/images/product-image/Layer 4811.png" alt="" />
                                        <img class="second-img" src="assets/images/product-image/Layer 4811.png" alt="" />
                                    </a>
                                    
                                </div>
                                <div class="product-decs">
                                    
                                    <h2><a href="single-product.html" class="product-link" style="color:#373737;">Mobile Tablate</a></h2>
                                    <div class="rating-product">
                                        iphone
                                    </div>
                                    <div class="rating-product">
                                        Nokia
                                    </div>
                                    <div class="rating-product">
                                        Samsung
                                    </div>
                                    
                                    
                                    <div class="pricing-meta">
                                        <ul>
                                           <li class="old-price not-cut" style="color:#ff7200;border-bottom:1px solid #ff7200;"> <a>View More ..</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <!-- Single Item -->
                        <div class="feature-slider-item">
                            <article class="list-product">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img" src="assets/images/product-image/Layer 53.png" alt="" />
                                        <img class="second-img" src="assets/images/product-image/Layer 53.png" alt="" />
                                    </a>
                                   
                                </div>
                                <div class="product-decs">
                                    
                                    <h2><a href="single-product.html" class="product-link" style="color:#373737;">Mobile Tablate</a></h2>
                                    <div class="rating-product">
                                        iphone
                                    </div>
                                    <div class="rating-product">
                                        Nokia
                                    </div>
                                    <div class="rating-product">
                                        Samsung
                                    </div>
                                    
                                    
                                    <div class="pricing-meta">
                                        <ul>
                                           <li class="old-price not-cut" style="color:#ff7200;border-bottom:1px solid #ff7200;"> <a>View More ..</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </article>
                            <article class="list-product">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img" src="assets/images/product-image/Layer 51.png" alt="" />
                                        <img class="second-img" src="assets/images/product-image/Layer 51.png" alt="" />
                                    </a>
                                    
                                </div>
                                <div class="product-decs">
                                    
                                    <h2><a href="single-product.html" class="product-link" style="color:#373737;">Mobile Tablate</a></h2>
                                    <div class="rating-product">
                                        iphone
                                    </div>
                                    <div class="rating-product">
                                        Nokia
                                    </div>
                                    <div class="rating-product">
                                        Samsung
                                    </div>
                                    
                                    
                                    <div class="pricing-meta">
                                        <ul>
                                           <li class="old-price not-cut" style="color:#ff7200;border-bottom:1px solid #ff7200;"> <a>View More ..</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </article>
                        </div>
                         <!-- Single Item -->
                         <div class="feature-slider-item">
                            <article class="list-product">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img" src="assets/images/product-image/Layer 51.png" alt="" />
                                        <img class="second-img" src="assets/images/product-image/Layer 51.png" alt="" />
                                    </a>
                                    
                                </div>
                                <div class="product-decs">
                                    
                                    <h2><a href="single-product.html" class="product-link" style="color:#373737;">Mobile Tablate</a></h2>
                                    <div class="rating-product">
                                        iphone
                                    </div>
                                    <div class="rating-product">
                                        Nokia
                                    </div>
                                    <div class="rating-product">
                                        Samsung
                                    </div>
                                    
                                    
                                    <div class="pricing-meta">
                                        <ul>
                                           <li class="old-price not-cut" style="color:#ff7200;border-bottom:1px solid #ff7200;"> <a>View More ..</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </article>
                            <article class="list-product">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img" src="assets/images/product-image/Layer 4811.png" alt="" />
                                        <img class="second-img" src="assets/images/product-image/Layer 4811.png" alt="" />
                                    </a>
                                  
                                </div>
                                <div class="product-decs">
                                    
                                    <h2><a href="single-product.html" class="product-link" style="color:#373737;">Mobile Tablate</a></h2>
                                    <div class="rating-product">
                                        iphone
                                    </div>
                                    <div class="rating-product">
                                        Nokia
                                    </div>
                                    <div class="rating-product">
                                        Samsung
                                    </div>
                                    
                                    
                                    <div class="pricing-meta">
                                        <ul>
                                           <li class="old-price not-cut" style="color:#ff7200;border-bottom:1px solid #ff7200;"> <a>View More ..</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <!-- Single Item -->
                        <div class="feature-slider-item">
                            <article class="list-product">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img" src="assets/images/product-image/Layer 53.png" alt="" />
                                        <img class="second-img" src="assets/images/product-image/Layer 53.png" alt="" />
                                    </a>
                                    
                                </div>
                                <div class="product-decs">
                                    
                                    <h2><a href="single-product.html" class="product-link" style="color:#373737;">Mobile Tablate</a></h2>
                                    <div class="rating-product">
                                        iphone
                                    </div>
                                    <div class="rating-product">
                                        Nokia
                                    </div>
                                    <div class="rating-product">
                                        Samsung
                                    </div>
                                    
                                    
                                    <div class="pricing-meta">
                                        <ul>
                                           <li class="old-price not-cut" style="color:#ff7200;border-bottom:1px solid #ff7200;"> <a>View More ..</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </article>
                            <article class="list-product">
                                <div class="img-block">
                                    <a href="single-product.html" class="thumbnail">
                                        <img class="first-img" src="assets/images/product-image/Layer 51.png" alt="" />
                                        <img class="second-img" src="assets/images/product-image/Layer 51.png" alt="" />
                                    </a>
                                   
                                </div>
                                <div class="product-decs">
                                    
                                    <h2><a href="single-product.html" class="product-link" style="color:#373737;">Mobile Tablate</a></h2>
                                    <div class="rating-product">
                                        iphone
                                    </div>
                                    <div class="rating-product">
                                        Nokia
                                    </div>
                                    <div class="rating-product">
                                        Samsung
                                    </div>
                                    
                                    
                                    <div class="pricing-meta">
                                        <ul>
                                           <li class="old-price not-cut" style="color:#ff7200;border-bottom:1px solid #ff7200;"> <a>View More ..</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </article>
                        </div>
                       
                    </div>
                    <!-- Feature Slider End -->
                </div>
            </section>
            <!-- Feature Area End -->
             <!-- Banner Area Start -->
             <div class="banner-3-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-res-xs-30">
                            <div class="banner-wrapper mb-30px">
                                <a href="shop-4-column.html"><img src="assets/images/banner-image/Layer 65.png" alt="" /></a>
                                <div class="container" style="    margin-top: -25%;margin-bottom: 12%;margin-left: 12%;">
                                    <div class="slider-content-5 slider-animated-1 text-left">
                                        
                                        <h1 class="animated">
                                            Deal Of The Day<br />
                                           
                                        </h1>
                                        <a href="shop-4-column.html" class="shop-btn animated" style="background-color:#ff7200;">SHOP NOW</a>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- Banner Area End -->
             <!-- Banner Area Start -->
             <div class="banner-3-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-res-xs-30">
                            <div class="banner-wrapper mb-30px">
                                <a href="shop-4-column.html"><img src="assets/images/banner-image/10.png" alt="" /></a>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- Banner Area End -->
            <!-- Blog area Start -->
            <section class="blog-area mb-30px mt-30">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Section title -->
                            <div class="section-title">
                                <h2>Testimonial</h2>
                                <p>Present posts in a best way to highlight interesting moments of your blog.</p>
                            </div>
                            <!-- Section title -->
                        </div>
                    </div>
                    <!-- Blog Slider Start -->
                    <div class="blog-slider-active owl-carousel owl-nav-style">
                        <!-- single item -->
                        <article class="blog-post">
                            <div class="blog-post-top">
                                <div class="blog-img">
                                    <img src="assets/images/blog-image/blog-5.jpg" alt="" />
                                </div>
                            </div>
                            <div class="blog-post-content d-flex">
                                <div class="blog-post-content-cell">
                                    <a href="blog-grid-left-sidebar.html" class="blog-meta">Cosmetic</a>
                                    <h4 class="blog-post-heading"><a href="blog-single-left-sidebar.html">This is First Post For XipBlog</a></h4>
                                    <p class="blog-text">
                                        Lorem Ipsum is simply dummy text of the printing and typeSettings industry. Lorem Ipsum has been the industrys ...
                                    </p>
                                    <a class="read-more-btn" href="blog-single-left-sidebar.html"> Read More <i class="ion-android-arrow-dropright-circle"></i></a>
                                </div>
                            </div>
                        </article>
                        <!-- single item -->
                        <article class="blog-post">
                            <div class="blog-post-top">
                                <div class="blog-img">
                                    <img src="assets/images/blog-image/blog-6.jpg" alt="" />
                                </div>
                            </div>
                            <div class="blog-post-content d-flex">
                                <div class="blog-post-content-cell">
                                    <a href="blog-grid-left-sidebar.html" class="blog-meta">Cosmetic</a>
                                    <h4 class="blog-post-heading"><a href="blog-single-left-sidebar.html">This is Secound Post For XipBlog</a></h4>
                                    <p class="blog-text">
                                        Lorem Ipsum is simply dummy text of the printing and typeSettings industry. Lorem Ipsum has been the industrys ...
                                    </p>
                                    <a class="read-more-btn" href="blog-single-left-sidebar.html"> Read More <i class="ion-android-arrow-dropright-circle"></i></a>
                                </div>
                            </div>
                        </article>
                        <!-- single item -->
                        <article class="blog-post">
                            <div class="blog-post-top">
                                <div class="blog-img">
                                    <img src="assets/images/blog-image/blog-7.jpg" alt="" />
                                </div>
                            </div>
                            <div class="blog-post-content d-flex">
                                <div class="blog-post-content-cell">
                                    <a href="blog-grid-left-sidebar.html" class="blog-meta">Cosmetic</a>
                                    <h4 class="blog-post-heading"><a href="blog-single-left-sidebar.html">This is Thrid Post For XipBlog</a></h4>
                                    <p class="blog-text">
                                        Lorem Ipsum is simply dummy text of the printing and typeSettings industry. Lorem Ipsum has been the industrys ...
                                    </p>
                                    <a class="read-more-btn" href="blog-single-left-sidebar.html"> Read More <i class="ion-android-arrow-dropright-circle"></i></a>
                                </div>
                            </div>
                        </article>
                        <!-- single item -->
                        <article class="blog-post">
                            <div class="blog-post-top">
                                <div class="blog-img">
                                    <img src="assets/images/blog-image/blog-8.jpg" alt="" />
                                </div>
                            </div>
                            <div class="blog-post-content">
                                <a href="blog-grid-left-sidebar.html" class="blog-meta">Fashion</a>
                                <h4 class="blog-post-heading"><a href="blog-single-left-sidebar.html">This is Foruth Post For XipBlog</a></h4>
                                <p class="blog-text">
                                    Lorem Ipsum is simply dummy text of the printing and typeSettings industry. Lorem Ipsum has been the industrys ...
                                </p>
                                <a class="read-more-btn" href="blog-single-left-sidebar.html"> Read More <i class="ion-android-arrow-dropright-circle"></i></a>
                            </div>
                        </article>
                        <!-- single item -->
                    </div>
                    <!-- Blog Slider Start -->
                </div>
            </section>
            <!-- Blog Area End -->
            <!-- Brand area start -->
            <div class="brand-area">
                <div class="container">
                    <div class="brand-slider owl-carousel owl-nav-style owl-nav-style-2">
                        <div class="brand-slider-item">
                            <a href="#"><img src="assets/images/brand-logo/1.jpg" alt="" /></a>
                        </div>
                        <div class="brand-slider-item">
                            <a href="#"><img src="assets/images/brand-logo/2.jpg" alt="" /></a>
                        </div>
                        <div class="brand-slider-item">
                            <a href="#"><img src="assets/images/brand-logo/3.jpg" alt="" /></a>
                        </div>
                        <div class="brand-slider-item">
                            <a href="#"><img src="assets/images/brand-logo/4.jpg" alt="" /></a>
                        </div>
                        <div class="brand-slider-item">
                            <a href="#"><img src="assets/images/brand-logo/5.jpg" alt="" /></a>
                        </div>
                        <div class="brand-slider-item">
                            <a href="#"><img src="assets/images/brand-logo/1.jpg" alt="" /></a>
                        </div>
                        <div class="brand-slider-item">
                            <a href="#"><img src="assets/images/brand-logo/2.jpg" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Brand area end -->
           @stop