@extends('frontend.layouts.app')
@php
        $meta_title = env('APP_NAME');
        $meta_description = \App\SeoSetting::first()->description;
    @endphp

@section('meta_title'){{ $meta_title }}@stop
@section('meta_description'){{ $meta_description }}@stop

@section('meta')
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="{{ $meta_title }}">
    <meta itemprop="description" content="{{ $meta_description }}">

    <!-- Twitter Card data -->
    <meta name="twitter:title" content="{{ $meta_title }}">
    <meta name="twitter:description" content="{{ $meta_description }}">

    <!-- Open Graph data -->
    <meta property="og:title" content="{{ $meta_title }}" />
    <meta property="og:description" content="{{ $meta_description }}" />
@endsection

@section('content')


<style>

.breadcrumb-area {
    text-align: center;
    position: relative;
    padding: 70px 0;
    background: none no-repeat !important;
    background-size: cover;
}

.breadcrumb-area {
    padding: 5px 0;
    border-bottom: 1px solid #e9e9e9;
    background-color: #fafafa;
}
.breadcrumb {
    margin-bottom: 0rem !important;
}
.add-to-link ul li {
    display: inline-flex;
    justify-content: center;
    float: none;
    width: 32%;
}
</style>
    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('home') }}">{{ translate('Home')}}</a></li>
                        <li><a href="{{ route('products') }}">{{ translate('Best Sellers')}}</a></li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>

    @if (\App\BusinessSetting::where('type', 'vendor_system_activation')->first()->value == 1)
    @php
        $array = array();
        foreach (\App\Seller::where('verification_status', 1)->get() as $key => $seller) {
            if($seller->user != null && $seller->user->shop != null){
                $total_sale = 0;
                foreach ($seller->user->products as $key => $product) {
                    $total_sale += $product->num_of_sale;
                }
                $array[$seller->id] = $total_sale;
            }
        }
        asort($array);
    @endphp
    @if(!empty($array))
        <section class="mb-5">
        <div class="container">
            <div class="px-2 py-4 p-md-4 bg-white shadow-sm">
                <div class="section-title-1 clearfix">
                    <h3 class="heading-5 strong-700 mb-0 float-left">
                        <span class="mr-4">{{ translate('Best Sellers')}}</span>
                    </h3>
                    <ul class="inline-links float-right">
                        <li><a  class="active" style="background: #ff7200;">{{ translate('Top 20')}}</a></li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-12" style="display: contents;">
                        @php
                            $count = 0;
                        @endphp
                        @foreach ($array as $key => $value)
                            @if ($count < 20)
                                @php
                                    $count ++;
                                    $seller = \App\Seller::find($key);
                                    $total = 0;
                                    $rating = 0;
                                    foreach ($seller->user->products as $key => $seller_product) {
                                        $total += $seller_product->reviews->count();
                                        $rating += $seller_product->reviews->sum('rating');
                                    }
                                @endphp
                                <div class="col-md-4" style="margin-bottom: 5px;">
                                <div class="row no-gutters box-3 align-items-center border">
                                        <div class="col-4">
                                            <a href="{{ route('shop.visit', $seller->user->shop->slug) }}" class="d-block product-image p-3">
                                                <img
                                                    src="{{ asset('frontend/images/placeholder.jpg') }}"
                                                    data-src="@if ($seller->user->shop->logo !== null) {{ asset($seller->user->shop->logo) }} @else {{ asset('frontend/images/placeholder.jpg') }} @endif"
                                                    alt="{{ $seller->user->shop->name }}"
                                                    class="img-fluid lazyload">
                                            </a>
                                        </div>
                                        <div class="col-8 border-left">
                                            <div class="p-3">
                                                <h2 class="product-title mb-0 p-0 text-truncate">
                                                    <a href="{{ route('shop.visit', $seller->user->shop->slug) }}">{{ translate($seller->user->shop->name) }}</a>
                                                </h2>
                                                <div class="star-rating star-rating-sm mb-2">
                                                    @if ($total > 0)
                                                        {{ renderStarRating($rating/$total) }}
                                                    @else
                                                        {{ renderStarRating(0) }}
                                                    @endif
                                                </div>
                                                <div class="">
                                                    <a href="{{ route('shop.visit', $seller->user->shop->slug) }}" class="icon-anim">
                                                        {{ translate('Visit Store') }} <i class="la la-angle-right text-sm"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
@endif


@endsection

@section('script')

@endsection
