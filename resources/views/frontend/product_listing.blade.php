@extends('frontend.layouts.app')
<style>

    </style>
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/css/multi-select.css" integrity="sha512-2sFkW9HTkUJVIu0jTS8AUEsTk8gFAFrPmtAxyzIhbeXHRH8NXhBFnLAMLQpuhHF/dL5+sYoNHWYYX2Hlk+BVHQ==" crossorigin="anonymous" /> -->
@if(isset($subsubcategory_id))
    @php
        $meta_title = \App\SubSubCategory::find($subsubcategory_id)->meta_title;
        $meta_description = \App\SubSubCategory::find($subsubcategory_id)->meta_description;
    @endphp
@elseif (isset($subcategory_id))
    @php
        $meta_title = \App\SubCategory::find($subcategory_id)->meta_title;
        $meta_description = \App\SubCategory::find($subcategory_id)->meta_description;
    @endphp
@elseif (isset($category_id))
    @php
        $meta_title = \App\Category::find($category_id)->meta_title;
        $meta_description = \App\Category::find($category_id)->meta_description;
    @endphp
@elseif (isset($brand_id))
    @php
        $meta_title = \App\Brand::find($brand_id)->meta_title;
        $meta_description = \App\Brand::find($brand_id)->meta_description;
    @endphp
@else
    @php
        $meta_title = env('APP_NAME');
        $meta_description = \App\SeoSetting::first()->description;
    @endphp
@endif

@section('meta_title'){{ $meta_title }}@stop
@section('meta_description'){{ $meta_description }}@stop

@section('meta')
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="{{ $meta_title }}">
    <meta itemprop="description" content="{{ $meta_description }}">

    <!-- Twitter Card data -->
    <meta name="twitter:title" content="{{ $meta_title }}">
    <meta name="twitter:description" content="{{ $meta_description }}">

    <!-- Open Graph data -->
    <meta property="og:title" content="{{ $meta_title }}" />
    <meta property="og:description" content="{{ $meta_description }}" />
@endsection

@section('content')


<style>

.breadcrumb-area {
    text-align: center;
    position: relative;
    padding: 70px 0;
    background: none no-repeat !important;
    background-size: cover;
}

.breadcrumb-area {
    padding: 5px 0;
    border-bottom: 1px solid #e9e9e9;
    background-color: #fafafa;
}
.breadcrumb {
    margin-bottom: 0rem !important;
}
.not_found_text{
    padding: 10px;
    background: azure;
    font-size: 20px;
    font-weight: 700;
}
.noUi-connect {
    background: #ff7200;
}
.noUi-horizontal {
    height: 5px;
}
.noUi-horizontal .noUi-handle {
    width: 28px;
    height: 28px;
    right: -17px;
    top: -14px;
}
.noUi-touch-area:focus{
    outline: 2px solid transparent !important;
}
.select2-container--default .select2-selection--multiple .select2-selection__rendered {
    box-sizing: border-box;
    list-style: none;
    margin: 0;
    padding: 0 5px;
    width: 100%;
    display: grid;
    /* position: absolute; */
}
.header-right-nav ul li {
    display: inline-block;
    margin: 0 10px;
    font-weight: 400;
    color: #fff;
    background: transparent;
    text-shadow: none;
    cursor: pointer;
    line-height: 28px !important;
    font-size: 13px;
    display: inline-block;
    text-transform: capitalize;
}
.add-to-link ul li a {
    display: inline-block;
    background: #ff7200;
    color: #f6f6f7;
    padding: 0;
    text-align: center;
    text-transform: capitalize;
    padding: 12px;
    /* padding-left: 2%; */
    /* padding-left: 9px; */
    border-radius: 50%;
    width: 41px;
}
a.quick_view{
    padding: 14px;
}
.star-rating i {
    display: inline-block;
    color: #ffcc00;
    margin-right: 3px;
    font-size: 9px !important;
}
.old-price {
    color: #9b9b9b;
    font-weight: 400;
    text-decoration: line-through;
    margin-right: 2px;
    font-size: 11px !important;
}
.dropdown_search{
    height: 48px;
}
.select2-selection{
    min-height: calc(1.5em + .75rem + 0px) !important;
    font-size: 0.9rem !important;
}
/*.select2-selection__rendered{*/
/*      height: calc(1.5em + .75rem + 0px) !important;*/
/*    font-size: 0.9rem !important;*/
/*}*/
.select2-container--default.select2-container--focus .select2-selection--multiple {
    color: #555;
    border-color: #cccccc !important;
}
.sc_actLinks .actLink_heading a {
    margin: 10px 0px 0px 15px !important;
}
.noUi-horizontal .noUi-handle {
    width: 15px;
    height: 15px;
    right: -17px;
    top: -7px;
    left: 19px;
}
@media only screen and (max-width: 786px){
.header-right-nav a {
    line-height: 12px;
}
 img.first-img {
    height: 250px;
    width: 100%;
}
img.second-img {
    height: 250px;
    width: 100%;
}
}
</style>
    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('home') }}">{{ translate('Home')}}</a></li>
                        <li><a href="{{ route('products') }}">{{ translate('All Categories')}}</a></li>
                        @if(isset($category_id))
                            <li class="active"><a href="{{ route('products.category', \App\Category::find($category_id)->slug) }}">{{ \App\Category::find($category_id)->name }}</a></li>
                        @endif
                        @if(isset($subcategory_id))
                            <li ><a href="{{ route('products.category', \App\SubCategory::find($subcategory_id)->category->slug) }}">{{ \App\SubCategory::find($subcategory_id)->category->name }}</a></li>
                            <li class="active"><a href="{{ route('products.subcategory', \App\SubCategory::find($subcategory_id)->slug) }}">{{ \App\SubCategory::find($subcategory_id)->name }}</a></li>
                        @endif
                        @if(isset($subsubcategory_id))
                            <li ><a href="{{ route('products.category', \App\SubSubCategory::find($subsubcategory_id)->subcategory->category->slug) }}">{{ \App\SubSubCategory::find($subsubcategory_id)->subcategory->category->name }}</a></li>
                            <li ><a href="{{ route('products.subcategory', \App\SubsubCategory::find($subsubcategory_id)->subcategory->slug) }}">{{ \App\SubsubCategory::find($subsubcategory_id)->subcategory->name }}</a></li>
                            <li class="active"><a href="{{ route('products.subsubcategory', \App\SubSubCategory::find($subsubcategory_id)->slug) }}">{{ \App\SubSubCategory::find($subsubcategory_id)->name }}</a></li>
                        @endif
                        @if(isset($extra))
                             <li class="active"><a href="javascript:">{{ ucwords(str_replace( '_', ' ', $extra)) }}</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @if (isset($category))
  @if ($category->sliders != null && $category->sliders != "")
        <div class="container">
                <div class="home-slide">
                    <div class="slick-carousel" data-slick-arrows="true" data-slick-dots="true">
                  
                   
                            @foreach (json_decode($category->sliders) as $key => $slide)
                                <div class="">
                                    <img class="d-block w-100 lazyload" src="{{ asset('frontend/images/placeholder-rect.jpg') }}" data-src="{{ asset($slide) }}" alt="{{ $key }} slide" style="max-height:300px;">
                                </div>
                            @endforeach
                       
                    </div>
                </div>
            </div>   
             @endif
              @endif

    <section class="gry-bg py-4">
        <div class="container sm-px-0">
            <form class="" id="search-form" action="{{ route('search') }}" method="GET">
                <div class="row">
                <div class="col-xl-3  d-xl-block">
                    <div class="filter-overlay filter-close"></div>
                    <div class="filter-wrapper c-scrollbar">
                        <div class="filter-title d-flex d-xl-none justify-content-between pb-3 align-items-center">
                            <h3 class="h6">Filters</h3>
                            <!-- <button type="button" class="close filter-close">
                              <span aria-hidden="true">&times;</span>
                            </button> -->
                        </div>
                        <div class="bg-white sidebar-box mb-3">
                            <div class="box-title text-center">
                                {{ translate('Categories')}}
                            </div>
                            <div class="box-content">
                                <div class="category-filter">
                                    <ul>
                                        @if(!isset($category_id) && !isset($category_id) && !isset($subcategory_id) && !isset($subsubcategory_id))
                                            @foreach(\App\Category::all() as $category)
                                                <li class=""><a href="{{ route('products.category', $category->slug) }}">{{  __($category->name) }}</a></li>
                                            @endforeach
                                        @endif
                                        @if(isset($category_id))
                                            <li class="active"><a href="{{ route('products') }}">{{ translate('All Categories')}}</a></li>
                                            <li class="active"><a href="{{ route('products.category', \App\Category::find($category_id)->slug) }}">{{  translate(\App\Category::find($category_id)->name) }}</a></li>
                                            @foreach (\App\Category::find($category_id)->subcategories as $key2 => $subcategory)
                                                <li class="child"><a href="{{ route('products.subcategory', $subcategory->slug) }}">{{  __($subcategory->name) }}</a></li>
                                            @endforeach
                                        @endif
                                        @if(isset($subcategory_id))
                                            <li class="active"><a href="{{ route('products') }}">{{ translate('All Categories')}}</a></li>
                                            <li class="active"><a href="{{ route('products.category', \App\SubCategory::find($subcategory_id)->category->slug) }}">{{  translate(\App\SubCategory::find($subcategory_id)->category->name) }}</a></li>
                                            <li class="active"><a href="{{ route('products.subcategory', \App\SubCategory::find($subcategory_id)->slug) }}">{{  translate(\App\SubCategory::find($subcategory_id)->name) }}</a></li>
                                            @foreach (\App\SubCategory::find($subcategory_id)->subsubcategories as $key3 => $subsubcategory)
                                                <li class="child"><a href="{{ route('products.subsubcategory', $subsubcategory->slug) }}">{{  __($subsubcategory->name) }}</a></li>
                                            @endforeach
                                        @endif
                                        @if(isset($subsubcategory_id))
                                            <li class="active"><a href="{{ route('products') }}">{{ translate('All Categories')}}</a></li>
                                            <li class="active"><a href="{{ route('products.category', \App\SubsubCategory::find($subsubcategory_id)->subcategory->category->slug) }}">{{  translate(\App\SubSubCategory::find($subsubcategory_id)->subcategory->category->name) }}</a></li>
                                            <li class="active"><a href="{{ route('products.subcategory', \App\SubsubCategory::find($subsubcategory_id)->subcategory->slug) }}">{{  translate(\App\SubsubCategory::find($subsubcategory_id)->subcategory->name) }}</a></li>
                                            <li class="current"><a href="{{ route('products.subsubcategory', \App\SubsubCategory::find($subsubcategory_id)->slug) }}">{{  translate(\App\SubsubCategory::find($subsubcategory_id)->name) }}</a></li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="bg-white sidebar-box mb-3">
                            <div class="box-title text-center">
                                {{ translate('Price range')}}
                            </div>
                            
                            <div class="box-content">
                            
                                <div class="range-slider-wrapper mt-3">
                                   
                                    <!-- Range slider container -->
                                    <div id="input-slider-range" data-range-value-min="@if(count(\App\Product::query()->get()) < 1) 0 @else {{ filter_products(\App\Product::query())->get()->min('unit_price') }} @endif"   data-range-value-max="@if(count(\App\Product::query()->get()) < 1) 0 @else {{ filter_products(\App\Product::query())->get()->max('unit_price') }} @endif"></div>

                                    <!-- Range slider values -->
                                    <div class="row">
                                        <div class="col-6">
                                            <span class="range-slider-value value-low"
                                                @if (isset($min_price))
                                                    data-range-value-low="{{ $min_price }}"
                                                @elseif($products->min('unit_price') > 0)
                                                    data-range-value-low="{{ $products->min('unit_price') }}"
                                                @else
                                                    data-range-value-low="0"
                                                @endif
                                                id="input-slider-range-value-low">
                                        </div>

                                        <div class="col-6 text-right">
                                            <span class="range-slider-value value-high"
                                                @if (isset($max_price))
                                                    data-range-value-high="{{ $max_price }}"
                                                @elseif($products->max('unit_price') > 0)
                                                    data-range-value-high="{{ $products->max('unit_price') }}"
                                                @else
                                                    data-range-value-high="0"
                                                @endif
                                                id="input-slider-range-value-high">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="bg-white sidebar-box mb-3">
                            <div class="box-title text-center">
                                {{ translate('Filter by color')}}
                            </div>
                            <div class="box-content">
                                <!-- Filter by color -->
                                <ul class="list-inline checkbox-color checkbox-color-circle mb-0">
                                    @foreach ($all_colors as $key => $color)
                                        <li>
                                            <input type="radio" id="color-{{ $key }}" name="color" value="{{ $color }}" @if(isset($selected_color) && $selected_color == $color) checked @endif onchange="filter()">
                                            <label style="background: {{ $color }};" for="color-{{ $key }}" data-toggle="tooltip" data-original-title="{{ \App\Color::where('code', $color)->first()->name }}"></label>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                        @foreach ($attributes as $key => $attribute)
                            @if (\App\Attribute::find($attribute['id']) != null)
                                <div class="bg-white sidebar-box mb-3">
                                    <div class="box-title text-center">
                                        Filter by {{ \App\Attribute::find($attribute['id'])->name }}
                                    </div>
                                    <div class="box-content">
                                        <!-- Filter by others -->
                                        <div class="filter-checkbox">
                                            @if(array_key_exists('values', $attribute))
                                                @foreach ($attribute['values'] as $key => $value)
                                                    @php
                                                        $flag = false;
                                                        if(isset($selected_attributes)){
                                                            foreach ($selected_attributes as $key => $selected_attribute) {
                                                                if($selected_attribute['id'] == $attribute['id']){
                                                                    if(in_array($value, $selected_attribute['values'])){
                                                                        $flag = true;
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    @endphp
                                                    <div class="checkbox">
                                                        <input type="checkbox" id="attribute_{{ $attribute['id'] }}_value_{{ $value }}" name="attribute_{{ $attribute['id'] }}[]" value="{{ $value }}" @if ($flag) checked @endif onchange="filter()">
                                                        <label for="attribute_{{ $attribute['id'] }}_value_{{ $value }}">{{ $value }}</label>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach

                        {{-- <button type="submit" class="btn btn-styled btn-block btn-base-4">Apply filter</button> --}}
                    </div>
                </div>
                <div class="col-xl-9">
                    <!-- <div class="bg-white"> -->
                        @isset($category_id)
                            <input type="hidden" name="category" value="{{ \App\Category::find($category_id)->slug }}">
                        @endisset
                        @isset($subcategory_id)
                            <input type="hidden" name="subcategory" value="{{ \App\SubCategory::find($subcategory_id)->slug }}">
                        @endisset
                        @isset($subsubcategory_id)
                            <input type="hidden" name="subsubcategory" value="{{ \App\SubSubCategory::find($subsubcategory_id)->slug }}">
                        @endisset

                        <div class="sort-by-bar row no-gutters bg-white mb-3 px-3 pt-2">
                            <!-- <div class="col-xl-4 d-flex d-xl-block justify-content-between align-items-end ">
                                <div class="sort-by-box flex-grow-1">
                                    <div class="form-group">
                                        <label>{{ translate('Search')}}</label>
                                        <div class="search-widget">
                                            <input class="form-control input-lg" type="text" name="q" placeholder="{{ translate('Search products')}}" @isset($query) value="{{ $query }}" @endisset>
                                            <button type="submit" class="btn-inner">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-xl-none ml-3 form-group">
                                    <button type="button" class="btn p-1 btn-sm" id="side-filter">
                                        <i class="la la-filter la-2x"></i>
                                    </button>
                                </div>
                            </div> -->
                            <div class="col-xl-12">
                                <div class="row no-gutters">
                                <div class="col-md-2 col-sm-12">
                                        <div class="sort-by-box px-1">
                                            <div class="form-group">
                                            <label>{{ translate('Search Product')}}</label>
                                            <input class="form-control" style="height: calc(1.5em + .75rem + 0px);font-size: 0.9rem;" type="text" name="q" placeholder="{{ translate('Search')}}" @isset($query) value="{{ $query }}" @endisset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <div class="sort-by-box px-1">
                                            <div class="form-group">
                                                <label>{{ translate('Sort by')}}</label>
                                                <select class="form-control sortSelect" data-minimum-results-for-search="Infinity" name="sort_by" onchange="filter()">
                                                    <option value="1" @isset($sort_by) @if ($sort_by == '1') selected @endif @endisset>{{ translate('Newest')}}</option>
                                                    <option value="2" @isset($sort_by) @if ($sort_by == '2') selected @endif @endisset>{{ translate('Oldest')}}</option>
                                                    <option value="3" @isset($sort_by) @if ($sort_by == '3') selected @endif @endisset>{{ translate('Price low to high')}}</option>
                                                    <option value="4" @isset($sort_by) @if ($sort_by == '4') selected @endif @endisset>{{ translate('Price high to low')}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <div class="sort-by-box px-1">
                                            <div class="form-group">
                                                <label>{{ translate('Brands')}}</label>
                                                <select class="form-control sortSelect " data-placeholder="{{ translate('All Brands')}}" name="brand[]" multiple=multiple>
                                                    <option value="">{{ translate('All Brands')}}</option>
                                                    @foreach (\App\Brand::all() as $brand)
                                                  {{-- <option value="{{ $brand->slug }}" @isset($brand_id) @if ($brand_id == $brand->id) selected @endif @endisset>{{ $brand->name }}</option> --}}
                                                        <option value="{{ $brand->slug }}" <?php if(in_array($brand->slug,$brand_arr)){ echo 'selected';}?>>{{ $brand->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <div class="sort-by-box px-1">
                                            <div class="form-group">
                                                <label>{{ translate('Sellers')}}</label>
                                                <select class="form-control sortSelect" data-placeholder="{{ translate('All Sellers')}}" name="seller_id[]" multiple=multiple>
                                                    <option value="">{{ translate('All Sellers')}}</option>
                                                    @foreach (\App\Seller::all() as $key => $seller)
                                                        @if ($seller->user != null && $seller->user->shop != null)
                                                            <option value="{{ $seller->user_id }}" <?php if(in_array($seller->user_id,$seller_arr)){ echo 'selected';}?>>{{ $seller->user->shop->name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                  
                                        <div class="col-md-2 col-sm-12">
                                        <button style=" background: #ff7200;color: white;margin-top: 15%;height: calc(1.5em + .75rem + 0px) !important;font-size: 0.9rem !important;" onclick="filter()"id="myButton" class="btn btn-styled btn-block btn-base-4" ><span style="color:white">Apply</span></button>
                                        </div>
                                    
                                </div>
                            </div>
                            
                        </div>
                        
                        <input type="hidden" name="min_price" value="">
                        <input type="hidden" name="max_price" value="">
                        <!-- <hr class=""> -->
                        @if($products->count()>0)
                        <div class="products-box-bar p-3 bg-white">
                            <div class="row sm-no-gutters gutters-5">
                                <?php //echo json_encode($products);die; ?>
                                @foreach ($products as $key => $product)
                                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-6">
                                    <article class="list-product mb-30px">
                                <div class="img-block">
                                    <a href="{{ route('product', $product->slug) }}" class="thumbnail">
                                        <img class="first-img" src="{{ asset($product->thumbnail_img) }}" alt="{{ __($product->name) }}" />
                                        <img class="second-img" src="{{ asset($product->thumbnail_img) }}" alt="{{ __($product->name) }}" />
                                    </a>
                                    <div class="quick-view">
                                        <a class="quick_view" href="{{ route('product', $product->slug) }}" data-link-action="quickview" title="Quick view">
                                            <i class="ion-ios-search-strong"></i>
                                        </a>
                                    </div>
                                </div>
                                <ul class="product-flag">
                                @if($product->discount!=0 && $product->discount!='' && $product->discount!=null)                
                                @if ($product->discount_type=="percent")
                                @php
                                    $show_type="%";
                                @endphp
                            <li class="new">{{ $product->discount.$show_type }} off</li>
                                @else 
                                @php
                                    $show_type="₹";
                                @endphp
                                <li class="new">{{ $show_type.$product->discount }} off</li>
                                @endif
                                @endif
                                </ul>
                                <div class="product-decs1">
                                    <div class="product-decs2">
                                    <div class="star-rating star-rating-sm mt-1">
                                                    {{ renderStarRating($product->rating) }}
                                                </div>
                                        <div class="pricing-meta"  style="float:right;">
                                            <ul>
                                                @if(home_base_price($product->id) != home_discounted_base_price($product->id))
                                                <li class="current-price">{{ home_discounted_base_price($product->id) }}</li>
                                                <li class="old-price">{{ home_base_price($product->id) }}</li>
                                                @else 
                                                <li class="current-price">{{ home_base_price($product->id) }}</li>
                                                @endif
                                               
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    

                                    <h2 ><a href="{{ route('product', $product->slug) }}" class="product-link">{{ __($product->name) }}</a></h2>
                                </div>
                                <div class="add-to-link">
                                    <ul>
                                    <li class="favart_add">
                                            <a href="#" onclick="addToWishList({{ $product->id }});"><i class="ion-android-favorite-outline"></i></a>
                                        </li>
                                        <li style="float: none; margin-left:6%;" class="cart_li"><a class="cart-btn" href="#" onclick="showAddToCartModal({{ $product->id }})">ADD TO CART </a></li>
                                        <li  class="compare_add cart">
                                            <a href="#" onclick="addToCompare({{ $product->id }})"><span class="iconify" data-icon="ion:git-compare" data-inline="false"></span></a>
                                        </li>
                                    </ul>
                                </div>
                            </article>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="products-pagination bg-white p-3">
                            <nav aria-label="Center aligned pagination">
                                <ul class="pagination justify-content-center">
                                    {{ $products->links() }}
                                </ul>
                            </nav>
                        </div>
                        @else
                        <p class="not_found_text">It seems we can't find what you're looking for.</p>
                        @endif

                    <!-- </div> -->
                </div>
            </div>
            </form>
        </div>
    </section>

@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/js/jquery.multi-select.min.js" integrity="sha512-vSyPWqWsSHFHLnMSwxfmicOgfp0JuENoLwzbR+Hf5diwdYTJraf/m+EKrMb4ulTYmb/Ra75YmckeTQ4sHzg2hg==" crossorigin="anonymous"></script>
    <script type="text/javascript">
//     $(function(){
//         $(".vertical-menu-toggle").click(function(){
//             var $yourUl = $(".open-menu-toggle"); 
//             $yourUl.css("display", $yourUl.css("display") === 'none' ? 'block' : 'none');
//         });
//         $('input[name=brand]').multiselect({
//             includeSelectAllOption: true
//     });
//     $('input[name=seller_id]').multiselect({
            
//     });
    
// });
        function filter(){
            $('#search-form').submit();
        }
        function rangefilter(arg){
            $('input[name=min_price]').val(arg[0]);
            $('input[name=max_price]').val(arg[1]);
            filter();
        }
      
    </script>
@endsection
