@extends('frontend.layouts.app')

@section('content')
<?php 
use App\Http\Controllers\OrderController; 
?>
<style>
.alert {
    margin-top:10px;
  padding: 20px;
  background-color: #f44336;
  color: white;
}

.closebtn {
  margin-left: 15px;
  color: white;
  font-weight: bold;
  float: right;
  font-size: 22px;
  line-height: 20px;
  cursor: pointer;
  transition: 0.3s;
}

.closebtn:hover {
  color: black;
}
</style>
<style>
       .process-steps li .icon {
    height: 40px;
    width: 40px;
    margin: auto;
    background: #fff;
    border-radius: 50%;
    line-height: 20px;
    font-size: 14px;
    font-weight: 700;
    padding: 10px;
    color: #400b0b;
    position: relative;
}
.process-steps li.active .icon {
    background-color: #5060a9;
    padding: 10px;
}
.process-steps li.done:after{
    background: #30c100 !important;
}
    .process-steps li + li:after {
    position: absolute;
    content: "";
    height: 10px;
    width: calc(100% - 40px);
    background: #fff;
    top: 14px;
    z-index: 0;
    right: calc(50% + 20px);
}
.process-steps li.done .icon:before {
    position: absolute;
    content: "";
    left: 16px;
    top: 10px;
    width: 8px;
    height: 14px;
    border: solid #fff;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}
.process-steps li.done .icon {
    color: #30c100;
    background: #30c100;
    padding: 10px !important;
}
    .test11 {
        height: calc(2.25rem + 8px) !important;
    }

    .process-steps li.active:after {
        background: #30c100 !important;
    }
    .process-steps li + li:after {
    position: absolute;
    content: "";
    height: 10px;
    width: calc(100% - 40px);
    background: #f1e2e2;
    top: 14px;
    z-index: 0;
    right: calc(50% + 20px);
}
.process-steps li .icon {
    height: 40px;
    width: 40px;
    margin: auto;
    background: #f1e2e2;
    border-radius: 50%;
    line-height: 20px;
    font-size: 14px;
    font-weight: 700;
    padding: 10px;
    color: #400b0b;
    position: relative;
}
</style>
    <section class="gry-bg py-4 profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-12 mx-auto">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="page-title">
                            <div class="row align-items-center">
                                <div class="col-md-6">
                                    <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                                        {{ translate('Track Order')}}
                                    </h2>
                                </div>
                            </div>
                        </div>
                        <form class="" action="{{ route('orders.track') }}" method="GET" enctype="multipart/form-data">
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{ translate('Order Info')}}
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{ translate('Order Code')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="{{ translate('Order Code')}}" name="order_code" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-right mt-4">
                                <button type="submit" class="btn btn-styled btn-base-1">{{ translate('Track Order')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            @if(isset($order))
                <div class="card mt-4">
                    <div class="card-header py-2 px-3 heading-6 strong-600 clearfix">
                        <div class="float-left">{{ translate('Order Summary')}}</div>
                    </div>
                    <div class="card-body pb-0">
                        <div class="row">
                            <div class="col-lg-6">
                                <table class="details-table table">
                                    <tr>
                                        <td class="w-50 strong-600">{{ translate('Order Code')}}:</td>
                                        <td>{{ $order->code }}</td>
                                    </tr>
                                    <tr>
                                        <td class="w-50 strong-600">{{ translate('Customer')}}:</td>
                                        <td>{{ json_decode($order->shipping_address)->name }}</td>
                                    </tr>
                                    <tr>
                                        <td class="w-50 strong-600">{{ translate('Email')}}:</td>
                                        @if ($order->user_id != null)
                                            <td>{{ $order->user->email }}</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <td class="w-50 strong-600">{{ translate('Shipping address')}}:</td>
                                        <td>{{ json_decode($order->shipping_address)->address }}, {{ json_decode($order->shipping_address)->city }}, {{ json_decode($order->shipping_address)->country }}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-lg-6">
                                <table class="details-table table">
                                    <tr>
                                        <td class="w-50 strong-600">{{ translate('Order date')}}:</td>
                                        <td>{{ date('d-m-Y H:m A', $order->date) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="w-50 strong-600">{{ translate('Total order amount')}}:</td>
                                        <td>{{ single_price($order->orderDetails->sum('price') + $order->orderDetails->sum('tax')) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="w-50 strong-600">{{ translate('Shipping method')}}:</td>
                                        <td>{{ translate('Flat shipping rate')}}</td>
                                    </tr>
                                    <tr>
                                        <td class="w-50 strong-600">{{ translate('Payment method')}}:</td>
                                        <td>{{ ucfirst(str_replace('_', ' ', $order->payment_type)) }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


                @foreach ($order->orderDetails as $key => $orderDetail)
                    @php
                        $status = $orderDetail->delivery_status;
                    @endphp
                    <div class="card mt-4">
                        <div class="card-header py-2 px-3 heading-6 strong-600 clearfix">
                            <ul class="process-steps clearfix">
                                <li @if($status == 'pending') class="active" @else class="done" @endif>
                                    <div class="icon">1</div>
                                    <div class="title">{{ translate('Order placed')}}</div>
                                </li>
                                <li @if($status == 'on_review') class="active" @elseif($status == 'on_delivery' || $status == 'delivered') class="done" @endif>
                                    <div class="icon">2</div>
                                    <div class="title">{{ translate('On review')}}</div>
                                </li>
                                <li @if($status == 'on_delivery') class="active" @elseif($status == 'delivered') class="done" @endif>
                                    <div class="icon">3</div>
                                    <div class="title">{{ translate('On delivery')}}</div>
                                </li>
                                <li @if($status == 'delivered') class="done" @endif>
                                    <div class="icon">4</div>
                                    <div class="title">{{ translate('Delivered')}}</div>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body p-4">
                            <div class="col-6">
                                <table class="details-table table">
                                    @if($orderDetail->product != null)
                                        <tr>
                                            <td class="w-50 strong-600">{{ translate('Product Name')}}:</td>
                                            <td>{{ $orderDetail->product->name }} ({{ $orderDetail->variation }})</td>
                                        </tr>
                                        <tr>
                                            <td class="w-50 strong-600">{{ translate('Quantity')}}:</td>
                                            <td>{{ $orderDetail->quantity }}</td>
                                        </tr>
                                        <tr>
                                            <td class="w-50 strong-600">{{ translate('Shipped By')}}:</td>
                                            <td>{{ $orderDetail->product->user->name }}</td>
                                        </tr>
                                    @endif
                                </table>
                            </div>
                        </div>
                        @php
                        
                        $tracking=$orderDetail->OrderDetailscurior;
                        
                        if($tracking!=null){
                            
                            if($tracking->first()->awbNo!=null){
                          $OrderController = new OrderController;
                                $tracking_dt=$OrderController->track($tracking->first()->awbNo);
                                //echo json_encode($tracking_dt);die;
                                //$tracking->first()->awbNo;die;
                                @endphp
                                @if($tracking_dt!=null)

<div class="card mt-3">
        <div class="card-header py-2 px-3 ">
            <div class="heading-4 strong-600">{{ translate('Tracking Summary')}}</div>
            <div>Curior Name :{{$tracking_dt->data->carrierName}}</div>
                <div>Curior Tracking ID :{{$tracking_dt->data->awbNo}}</div>
        </div>
        <div class="card-body pb-0">
            <div class="row">
                
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <table class="table table-sm table-hover table-responsive-md">
                    <thead>
                    <tr>
                            <th class="strong-600" width="25%">{{ translate('Tracking Status')}}</th>
                            <th class="strong-600" width="25%">{{ translate('Tracking Location')}}</th>
                            <th class="strong-600" width="25%">{{ translate('Description')}}</th>
                            <th class="strong-600" width="25%">{{ translate('Time')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tracking_dt->data->events as $tracking_data )
                        @if(isset($tracking_data->status))
                        <tr>
                            <td class="strong-600" width="25%"><span class="badge badge--2 mr-4"><i class="bg-green"></i> {{ isset($tracking_data->status) ? Config::get('constants.TRACKING_TYPE')[$tracking_data->status] :""}}</span></td>
                            <td width="25%">{{ $tracking_data->Location }}</td>
                            <td width="25%">{{ $tracking_data->Remarks }}</td>
                            <?php
                            $date = date_create($tracking_data->Time);
                            $date_dt= date_format($date, 'd/m/Y h:i:s A');
                            ?>
                            <td width="25%">{{ $date_dt }}</td>
                        </tr>
                        @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
               
            </div>
        </div>
    </div>
@endif

                                @php
                        }
                        }
                        

                        @endphp
                    </div>
                @endforeach
@else
@if(request()->has('order_code'))
<div class="alert">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
  <strong>Sorry!</strong> Invalid Code
</div>
@endif
            @endif
        </div>
    </section>

@endsection
