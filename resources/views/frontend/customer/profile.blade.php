@extends('frontend.layouts.app')

@section('content')
<style>
    .required:after {
        content: " *";
        color: red;
    }
</style>
<section class="gry-bg py-4 profile">
    <div class="container">
        <div class="row cols-xs-space cols-sm-space cols-md-space">
            <div class="col-lg-3 d-none d-lg-block">
                @if(Auth::user()->user_type == 'seller')
                @include('frontend.inc.seller_side_nav')
                @elseif(Auth::user()->user_type == 'customer')
                @include('frontend.inc.customer_side_nav')
                @endif
            </div>

            <div class="col-lg-9">
                <div class="main-content">
                    <!-- Page title -->
                    <div class="page-title">
                        <div class="row align-items-center">
                            <div class="col-md-6 col-12">
                                <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                                    {{ translate('Manage Profile') }}
                                </h2>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="float-md-right">
                                    <ul class="breadcrumb">
                                        <li><a href="{{ route('home') }}">{{ translate('Home') }}</a></li>
                                        <li><a href="{{ route('dashboard') }}">{{ translate('Dashboard') }}</a></li>
                                        <li class="active"><a
                                                href="{{ route('profile') }}">{{ translate('Manage Profile') }}</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <form class="" action="{{ route('customer.profile.update') }}" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="form-box bg-white mt-4">
                            @if (count($errors) > 0)
                            <div class="alert alert-danger successErrorMessage">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <div class="form-box-title px-3 py-2">


                                {{ translate('Basic info') }}
                            </div>
                            <div class="form-box-content p-3">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>{{ translate('Your Name') }}</label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control mb-3"
                                            placeholder="{{ translate('Your Name') }}" name="name"
                                            value="{{ Auth::user()->name }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>{{ translate('Your Email') }}</label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="email" class="form-control mb-3"
                                            placeholder="{{ translate('Your Email')}}" name="email"
                                            value="{{ Auth::user()->email }}" disabled>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>{{ translate('Your Phone')}}</label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control mb-3"
                                            placeholder="{{ translate('Your Phone')}}" name="phone" id="phone"
                                            value="{{ Auth::user()->phone_no }}" disabled>
                                        <a href="#" onclick="changPhoneNumber();"><span
                                                style="font-weight:bold;color:#ff7200">Change phone number?</span></a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>{{ translate('Photo') }}</label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="file" name="photo" id="file-3"
                                            class="custom-input-file custom-input-file--4"
                                            data-multiple-caption="{count} files selected" accept="image/*" />
                                        <label for="file-3" class="mw-100 mb-3">
                                            <span></span>
                                            <strong>
                                                <i class="fa fa-upload"></i>
                                                {{ translate('Choose image') }}
                                            </strong>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>{{ translate('Your Password') }}</label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="password" class="form-control mb-3"
                                            placeholder="{{ translate('New Password') }}" name="new_password">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>{{ translate('Confirm Password') }}</label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="password" class="form-control mb-3"
                                            placeholder="{{ translate('Confirm Password') }}" name="confirm_password">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="text-right mt-4">
                            <button type="submit"
                                class="btn btn-styled btn-base-1">{{ translate('Update Profile') }}</button>
                        </div>

                        <div class="form-box bg-white mt-4">
                            <div class="form-box-title px-3 py-2">
                                {{ translate('Addresses') }}
                            </div>
                            <div class="form-box-content p-3">
                                <div class="row gutters-10">
                                    @foreach (Auth::user()->addresses as $key => $address)
                                    <div class="col-lg-6">
                                        <div class="border p-3 pr-5 rounded mb-3 position-relative">
                                            <div>
                                                <span class="alpha-6">{{ translate('Address') }}:</span>
                                                <span class="strong-600 ml-2">{{ $address->address }}</span>
                                            </div>
                                            <div>
                                                <span class="alpha-6">{{ translate('District') }}:</span>
                                                <span class="strong-600 ml-2">{{ $address->district }}</span>
                                            </div>
                                            <div>
                                                <span class="alpha-6">{{ translate('State') }}:</span>
                                                <span class="strong-600 ml-2">{{ $address->state }}</span>
                                            </div>
                                            <div>
                                                <span class="alpha-6">{{ translate('City') }}:</span>
                                                <span class="strong-600 ml-2">{{ $address->city }}</span>
                                            </div>
                                            <div>
                                                <span class="alpha-6">{{ translate('postal Code') }}:</span>
                                                <span class="strong-600 ml-2">{{ $address->postal_code }}</span>
                                            </div>
                                            <div>
                                                <span class="alpha-6">{{ translate('Region') }}:</span>
                                                <span class="strong-600 ml-2">{{ $address->region }}</span>
                                            </div>
                                            <div>
                                                <span class="alpha-6">{{ translate('Landmark') }}:</span>
                                                <span class="strong-600 ml-2">{{ $address->landmark }}</span>
                                            </div>

                                            <div>
                                                <span class="alpha-6">{{ translate('Phone') }}:</span>
                                                <span class="strong-600 ml-2">{{ $address->phone }}</span>
                                            </div>
                                            @if ($address->set_default)
                                            <div class="position-absolute right-0 bottom-0 pr-2 pb-3">
                                                <span
                                                    class="badge badge-primary bg-base-1">{{ translate('Default') }}</span>
                                            </div>
                                            @endif
                                            <div class="dropdown position-absolute right-0 top-0">
                                                <button class="btn bg-gray px-2" type="button" data-toggle="dropdown">
                                                    <i class="la la-ellipsis-v"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right"
                                                    aria-labelledby="dropdownMenuButton">
                                                    @if (!$address->set_default)
                                                    <a class="dropdown-item"
                                                        href="{{ route('addresses.set_default', $address->id) }}">{{ translate('Make This Default') }}</a>
                                                    @endif
                                                    {{-- <a class="dropdown-item" href="">Edit</a> --}}
                                                    <a class="dropdown-item"
                                                        href="{{ route('addresses.destroy', $address->id) }}">{{ translate('Delete') }}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    <div class="col-lg-6 mx-auto" onclick="add_new_address()">
                                        <div class="border p-3 rounded mb-3 c-pointer text-center bg-light">
                                            <i class="la la-plus la-2x"></i>
                                            <div class="alpha-7">{{ translate('Add New Address') }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="new-address-modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-zoom" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="exampleModalLabel">{{ translate('New Address') }}</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form-default" role="form" action="{{ route('addresses.store') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="p-3">
                        <div class="row">
                            <div class="col-md-2">
                                <label class="required">{{ translate('Address') }}</label>
                            </div>
                            <div class="col-md-10">
                                <textarea class="form-control textarea-autogrow mb-3"
                                    placeholder="{{ translate('Your Address') }}" rows="1" name="address"
                                    required></textarea>
                            </div>
                        </div>
                        <!-- <div class="row">
                                <div class="col-md-2">
                                    <label>{{ translate('Country') }}</label>
                                </div>
                                <div class="col-md-10">
                                    <div class="mb-3">
                                        <select class="form-control mb-3 selectpicker" data-placeholder="{{translate('Select your country')}}" name="country" required>
                                            @foreach (\App\Country::where('status', 1)->get() as $key => $country)
                                                <option value="{{ $country->name }}">{{ $country->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div> -->

                        <div class="row">
                            <div class="col-md-2">
                                <label class="required">{{ translate('Postal code')}}</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" pattern="[0-9]{6}" onchange="userAction();" class="form-control mb-3"
                                    placeholder="{{ translate('Enter Postal Code')}}" name="postal_code"
                                    id="postal_code" maxlength="6" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="required">{{ translate('District')}}</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" class="form-control mb-3"
                                    placeholder="{{ translate('Enter District')}}" name="district_id" id="district_id"
                                    maxlength="30" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="required">{{ translate('State')}}</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" class="form-control mb-3" placeholder="{{ translate('Enter State')}}"
                                    name="state_id" id="state_id" maxlength="30" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="required">{{ translate('Region')}}</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" class="form-control mb-3"
                                    placeholder="{{ translate('Enter Region')}}" name="region_id" id="region_id"
                                    maxlength="30" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="required">{{ translate('Landmark')}}</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" class="form-control mb-3"
                                    placeholder="{{ translate('Enter Landmark')}}" name="landmark_id" id="landmark_id"
                                    maxlength="50" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="required">{{ translate('City')}}</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" class="form-control mb-3" placeholder="{{ translate('Enter City')}}"
                                    name="city" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="required">{{ translate('Phone')}}</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" class="form-control mb-3" placeholder="{{ translate('+91')}}"
                                    name="phone" maxlength="13" required pattern="[+0-9]{13}">
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-base-1">{{  translate('Save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
    $("input[name=phone]").focus(function(){
  $("input[name=phone]").val("+91");
});

  $("input[name=phone]").keyup(function(){
    // var prefix = "+91"

    // if(this.value.indexOf(prefix) !== 0 ){
    //     this.value = prefix + this.value;
    // }
    if($(this).val().indexOf('+91') == 0) {
        $(this).val($(this).val());
    }else{
      $phone_val=$(this).val();
      if($phone_val!=""){
        $(this).val("");
      }
      $(this).val("+91" + $(this).val());
    }
});
    function add_new_address(){
        $('#new-address-modal').modal('show');
    }

   
  function userAction(val){
let postal_code=$('#postal_code').val();
    $.ajax({
        url:'https://api.postalpincode.in/pincode/'+postal_code,
        dataType:'json',
        type: 'GET',
      
        success:function(response){
            //console.log(response[0].PostOffice[0])
            $('#district_id').val(response[0].PostOffice[0].District);
            $('#state_id').val(response[0].PostOffice[0].State);
            $('#region_id').val(response[0].PostOffice[0].Region);
          
             
        }
    })
  }
  function changPhoneNumber(){
    var content="";
    Swal.fire({
            title: "Change Phone Number?",
            text: "Are you sure to proceed?",
            type: "",
            showCancelButton: !0,
            confirmButtonText: "Yes, proceed!",
            cancelButtonText: "No, cancel!",
            reverseButtons: !0
        }).then(function (e) {
            console.log(e)
            if (e.value === true) {
                var token = $("input[name='_token']").val();
            
                return $.ajax({
                        method: 'post',
                    url: "{{route('userChangePhoneNumber')}}",
                   
                    data: {"_token": "{{ csrf_token() }}"},
                   
                    dataType:"json",
                 
                   
                    }).done(function (response) {

                  //  jc.hideLoading(true);
                    if (response.status == 1) {
                        content +='<div class="input-group input-group--style-1" style="padding:18px;">';
                    content +='<span class="log_addon input-group-addon"><i class="text-md fa fa-lock"></i></span>';
                    content +='<input type="text" class="log_input form-control" placeholder="Enter Otp" id="previous_phone_otp" name="previous_phone_otp"  autocomplete="off" >';
                    content +='</div>';
                    content +='<div class="input-group input-group--style-1" style="padding:18px;">';
                    content +='<span class="log_addon input-group-addon"><i class="text-md fa fa-phone"></i></span>';
                    content +='<input type="text" class="log_input form-control" placeholder="Enter Mobile No" id="new_phone" name="new_phone"  autocomplete="off" >';
                    content +='</div>';


                    var jc=$.confirm({
                    boxWidth: '30%',
                    useBootstrap: false,
                    title: 'Please enter new phone number & otp',
                    content: content,
                    type: 'green',
                    icon: 'fa fa-mobile',
                    typeAnimated: true,
                    smoothContent:true,
                    buttons: {
                        resend: {
                        btnText: 'Send OTP',
                        btnClass: 'btn-danger',
                        action: function () {
                        jc.showLoading(true);
                        

                        return $.ajax({
                            method: 'post',
                        url: "{{route('userChangePhoneNumber')}}",
                        dataType: 'json',
                        data: {"_token": "{{ csrf_token() }}"},
                   
                
                     
                       
                        }).done(function (response) {

                        jc.hideLoading(true);
                        if (response.status == 1) {
                        jc.open(true);
                        } else {
                            $.confirm({
                        boxWidth: '30%',
                        useBootstrap: false,
                        title: 'ERROR!',
                        content: response.message,
                        type: 'red',
                        icon: 'fa fa-warning',
                        typeAnimated: true,
                        smoothContent:true,
                       
                        buttons: {
                        ok: function () {
                            jc.showLoading(true);
                        //$("#login").attr("disabled", false);
                        },
                        cancelButton: false, // hides the cancel button.
                        }
                        });
                       
                        }
                        }).fail(function () {
                  
                      
                       $.confirm({
                        boxWidth: '30%',
                        type: 'red',
                        icon: 'fa fa-warning',
                        title: 'ERROR!',
                        content: 'OTP Verification Proceess Faild',
                        useBootstrap: false,
                        smoothContent:true,
                        
                        
                        });
                        });
                        }
                        },
                        Next: {
                        btnClass: 'btn-primary',
                        action: function () {
                        jc.showLoading(true);

                        var new_phone=$("#new_phone").val();
                        var previous_phone_otp=$("#previous_phone_otp").val();
                        var token = $("input[name='_token']").val();
                        var type="customer";
                        // var form_data=new FormData();
                        // form_data.append('_token',token);
                        // form_data.append('new_phone',new_phone);
                        // form_data.append('previous_phone_otp',previous_phone_otp);
                        
                        return $.ajax({
                            url: '{{route("userChangePhoneOtpcheck")}}',
                            dataType: 'json',
                            data: {'new_phone': new_phone,'otp': previous_phone_otp, '_token': token,'type':type},
                            method: 'post'
                        }).done(function (response) {
                        //alert('hi');
                        jc.hideLoading(true);
                        if (response.status == 1) {
                          
                        $.confirm({
                        boxWidth: '30%',
                        useBootstrap: false,
                        title: 'SUCCESS!',
                        content:response.message,
                        type: 'green',
                        icon: 'fa fa-check',
                        typeAnimated: true,
                        smoothContent:true,
                         // hides the cancel button.
                        buttons: {
                        ok: function () {
                           location.reload();
                        },
                        cancelButton: false,
                         }
                        });
                           
                        }
                        else if(response.status == 2){

                            $.confirm({
                        boxWidth: '30%',
                        useBootstrap: false,
                        title: 'ERROR!',
                        content: response.message,
                        type: 'red',
                        icon: 'fa fa-warning',
                        typeAnimated: true,
                        smoothContent:true,
                       
                        buttons: {
                        ok: function () {
                           
                        },
                        cancelButton: false, // hides the cancel button.
                        }
                        });
                        }
                        
                         else {

                            $.confirm({
                        boxWidth: '30%',
                        useBootstrap: false,
                        title: 'ERROR!',
                        content: response.message,
                        type: 'red',
                        icon: 'fa fa-warning',
                        typeAnimated: true,
                        smoothContent:true,
                       
                        buttons: {
                        ok: function () {
                            jc.open(true);
                           // jc.showLoading(true);
                        //$("#login").attr("disabled", false);
                        },
                        cancelButton: false, // hides the cancel button.
                        }
                        });
                        
                        }
                        }).fail(function () {

                        $.confirm({
                        boxWidth: '30%',
                        type: 'red',
                        icon: 'fa fa-warning',
                        title: 'ERROR!',
                        content: 'OTP Verification Proceess Faild',
                        useBootstrap: false,
                        smoothContent:true,
                        
                        
                        });

                        });
                        }
                        },
                    Close: function () {


                    }
                    },
                    onContentReady: function () {
                        
                        $("input[name=new_phone]").focus(function(){
                    $("input[name=new_phone]").val("+91");
                    });
                    $("input[name=new_phone]").keyup(function(){
                        
                        if($(this).val().indexOf('+91') == 0) {
                            $(this).val($(this).val());
                        }else{
                        $phone_val=$(this).val();
                        if($phone_val!=""){
                            $(this).val("");
                        }
                        $('#new_phone').val("+91" + $(this).val());
                        }
                    });
}
                });
                  
                    } else {
                    //$("#login").attr("disabled", false);
                    $.confirm({
                    boxWidth: '30%',
                    type: 'red',
                    icon: 'fa fa-warning',
                    title: 'ERROR!',
                    content: response.message,
                    useBootstrap: false,
                    smoothContent:true,
                    
                    
                    });
                    }
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                   // $("#login").attr("disabled", false);
                   var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>Something Went Wrong,Please Reload and Try Again</strong>";
                    } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                    msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                    msg += "Error(s):<strong><ul>";
                    $.each(jqXHR.responseJSON['errors'], function (key, value) {
                    msg += "<li>" + value + "</li>";
                    });
                    msg += "</ul></strong>";
                    }
                    }
                    isValid=false;
                    showFrontendAlert("error", msg);
                    });
















                 

            } else {
                e.dismiss;
            }

        }, function (dismiss) {
            return false;
        })

               

                   
}
</script>
@endsection