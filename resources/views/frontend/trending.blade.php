@extends('frontend.layouts.app')
@php
        $meta_title = env('APP_NAME');
        $meta_description = \App\SeoSetting::first()->description;
    @endphp

@section('meta_title'){{ $meta_title }}@stop
@section('meta_description'){{ $meta_description }}@stop

@section('meta')
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="{{ $meta_title }}">
    <meta itemprop="description" content="{{ $meta_description }}">

    <!-- Twitter Card data -->
    <meta name="twitter:title" content="{{ $meta_title }}">
    <meta name="twitter:description" content="{{ $meta_description }}">

    <!-- Open Graph data -->
    <meta property="og:title" content="{{ $meta_title }}" />
    <meta property="og:description" content="{{ $meta_description }}" />
@endsection

@section('content')


<style>

.breadcrumb-area {
    text-align: center;
    position: relative;
    padding: 70px 0;
    background: none no-repeat !important;
    background-size: cover;
}

.breadcrumb-area {
    padding: 5px 0;
    border-bottom: 1px solid #e9e9e9;
    background-color: #fafafa;
}
.breadcrumb {
    margin-bottom: 0rem !important;
}
.add-to-link ul li {
    display: inline-flex;
    justify-content: center;
    float: none;
    width: 32%;
}
.home-cosmatics .new1 {
    background-color: #3d5a73;
}
.home-cosmatics .new1 {
    background-color: #ff7200;
}
.new1 {
    position: absolute;
    top: 10px!important;
    right: 10px;
    display: inline-block;
    color: #fff;
    padding: 0 5px;
    text-align: center;
    line-height: 20px;
    height: 20px;
    font-size: 12px;
    font-weight: 700;
    border-radius: 5px;
    text-transform: capitalize;
    text-align: center;
    z-index: 1;
    background-color: #ff7200;
}
</style>
    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li><a href="{{ route('home') }}">{{ translate('Home')}}</a></li>
                        <li><a href="{{ route('products.trending') }}">{{ translate('Trending Product')}}</a></li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <section class="gry-bg py-4">
        <div class="container sm-px-0">
            <form class="" id="search-form" action="{{ route('search') }}" method="GET">
                <div class="row">
               
                <div class="col-xl-12">
                    <!-- <div class="bg-white"> -->
                    
                        <!-- <hr class=""> -->
                        <div class="products-box-bar p-3 bg-white">
                            <div class="row sm-no-gutters gutters-5">
                                <?php //echo json_encode($products);die;
                                $date = \Carbon\Carbon::today()->subDays(10);
                                ?>
                                @foreach (filter_products(\App\Product::where('featured', 1)->where('is_approve', 1)->where('published', 1)->where('created_at', '>=', $date))->get() as $key => $product)
                                
                                    <div class="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-6">
                                    <article class="list-product mb-30px">
                                <div class="img-block">
                                    <a href="{{ route('product', $product->slug) }}" class="thumbnail">
                                        <img class="first-img" src="{{ asset($product->thumbnail_img) }}" alt="{{ __($product->name) }} " style="width:100%"/>
                                        <img class="second-img" src="{{ asset($product->thumbnail_img) }}" alt="{{ __($product->name) }}" style="width:100%"/>
                                    </a>
                                    <div class="quick-view">
                                        <a class="quick_view" href="{{ route('product', $product->slug) }}" data-link-action="quickview" title="Quick view">
                                            <i class="ion-ios-search-strong"></i>
                                        </a>
                                    </div>
                                </div>
                                <ul class="product-flag">
                                    @php
                                    if($product->discount_type=="percent"){
                                        $show_type="%";
                                    }else{
                                        $show_type="₹";  
                                    }
                                    @endphp
                                    <li class="new">{{ $product->discount.$show_type }} off</li>
                                </ul>
                                <ul class="product-flag" style="float: right;">
                                  
                                    <li class="new1">New</li>
                                </ul>
                                <div class="product-decs1">
                                    <div class="product-decs2">
                                        <div class="rating-product" style="float:left;">
                                           
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                            <i class="ion-android-star"></i>
                                               
                                             
                                      
                                           
                                           
                                        </div>
                                        <div class="pricing-meta"  style="float:right;">
                                            <ul>
                                                @if(home_base_price($product->id) != home_discounted_base_price($product->id))
                                                <li class="current-price">{{ home_discounted_base_price($product->id) }}</li>
                                                @endif
                                                <li class="old-price">{{ home_base_price($product->id) }}</li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    

                                    <h2 ><a href="{{ route('product', $product->slug) }}" class="product-link">{{ __($product->name) }}</a></h2>
                                </div>
                                <div class="add-to-link">
                                    <ul>
                                    <li  class="compare_add cart">
                                            <a href="#" onclick="addToCompare({{ $product->id }})"><span class="iconify" data-icon="ion:git-compare" data-inline="false"></span></a>
                                        </li>
                                        <li style="float: none;" class="cart_li"><a class="cart-btn" href="#" onclick="showAddToCartModal({{ $product->id }})">ADD TO CART </a></li>
                                      <li class="favart_add">
                                            <a href="#" onclick="addToWishList({{ $product->id }});"><i class="ion-android-favorite-outline"></i></a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </article>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        

                    <!-- </div> -->
                </div>
            </div>
            </form>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
        function filter(){
            $('#search-form').submit();
        }
        function rangefilter(arg){
            $('input[name=min_price]').val(arg[0]);
            $('input[name=max_price]').val(arg[1]);
            filter();
        }
    </script>
@endsection
