<!DOCTYPE html>
@if(\App\Language::where('code', Session::get('locale', Config::get('app.locale')))->first()->rtl == 1)
<html dir="rtl" lang="en">
@else
<html lang="en">
@endif
<head>

@php
    $seosetting = \App\SeoSetting::first();
@endphp

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="index, follow">
<title>@yield('meta_title', config('app.name', 'Laravel'))</title>
<meta name="description" content="@yield('meta_description', $seosetting->description)" />
<meta name="keywords" content="@yield('meta_keywords', $seosetting->keyword)">
<meta name="author" content="{{ $seosetting->author }}">
<meta name="sitemap_link" content="{{ $seosetting->sitemap_link }}">

@yield('meta')

@if(!isset($detailedProduct))
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="{{ config('app.name', 'Laravel') }}">
    <meta itemprop="description" content="{{ $seosetting->description }}">
    <meta itemprop="image" content="{{ asset(\App\GeneralSetting::first()->logo) }}">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="product">
    <meta name="twitter:site" content="@publisher_handle">
    <meta name="twitter:title" content="{{ config('app.name', 'Laravel') }}">
    <meta name="twitter:description" content="{{ $seosetting->description }}">
    <meta name="twitter:creator" content="@author_handle">
    <meta name="twitter:image" content="{{ asset(\App\GeneralSetting::first()->logo) }}">

    <!-- Open Graph data -->
    <meta property="og:title" content="{{ config('app.name', 'Laravel') }}" />
    <meta property="og:type" content="Ecommerce Site" />
    <meta property="og:url" content="{{ route('home') }}" />
    <meta property="og:image" content="{{ asset(\App\GeneralSetting::first()->logo) }}" />
    <meta property="og:description" content="{{ $seosetting->description }}" />
    <meta property="og:site_name" content="{{ env('APP_NAME') }}" />
@endif

<!-- Favicon -->
<link type="image/x-icon" href="{{ asset(\App\GeneralSetting::first()->favicon) }}" rel="shortcut icon" />

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet" media="none" onload="if(media!='all')media='all'">

<!-- Bootstrap -->
<link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}" type="text/css" media="all">

<!-- Icons -->
<link rel="stylesheet" href="{{ asset('frontend/css/font-awesome.min.css') }}" type="text/css" media="none" onload="if(media!='all')media='all'">
<link rel="stylesheet" href="{{ asset('frontend/css/line-awesome.min.css') }}" type="text/css" media="none" onload="if(media!='all')media='all'">

<link type="text/css" href="{{ asset('frontend/css/bootstrap-tagsinput.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('frontend/css/jodit.min.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('frontend/css/sweetalert2.min.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('frontend/css/slick.css') }}" rel="stylesheet" media="all">
<link type="text/css" href="{{ asset('frontend/css/xzoom.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('frontend/css/jssocials.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('frontend/css/jssocials-theme-flat.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('frontend/css/intlTelInput.min.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('css/spectrum.css')}}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">

<!-- Global style (main) -->
<link type="text/css" href="{{ asset('frontend/css/active-shop.css') }}" rel="stylesheet" media="all">


<link type="text/css" href="{{ asset('frontend/css/main.css') }}" rel="stylesheet" media="all">

@if(\App\Language::where('code', Session::get('locale', Config::get('app.locale')))->first()->rtl == 1)
     <!-- RTL -->
    <link type="text/css" href="{{ asset('frontend/css/active.rtl.css') }}" rel="stylesheet" media="all">
@endif

<!-- color theme -->
<link href="{{ asset('frontend/css/colors/'.\App\GeneralSetting::first()->frontend_color.'.css')}}" rel="stylesheet" media="all">

<!-- Custom style -->
<link type="text/css" href="{{ asset('frontend/css/custom-style.css') }}" rel="stylesheet" media="all">
<link type="text/css" href="{{ asset('css/jquery-confirm.min.css') }}" rel="stylesheet" media="all">
<!-- jQuery -->
<script src="{{ asset('frontend/js/vendor/jquery.min.js') }}"></script>


@if (\App\BusinessSetting::where('type', 'google_analytics')->first()->value == 1)
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ env('TRACKING_ID') }}"></script>

    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', '{{ env('TRACKING_ID') }}');
    </script>
@endif

@if (\App\BusinessSetting::where('type', 'facebook_pixel')->first()->value == 1)
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', {{ env('FACEBOOK_PIXEL_ID') }});
  fbq('track', 'PageView');
</script>
<noscript>
  <img height="1" width="1" style="display:none"
       src="https://www.facebook.com/tr?id={{ env('FACEBOOK_PIXEL_ID') }}/&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
@endif
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/logo/logo-cosmatics.png" />
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800&amp;display=swap" rel="stylesheet" />

        <!-- All CSS Flies   -->
        <!--===== Vendor CSS (Bootstrap & Icon Font) =====-->
        <!-- <link rel="stylesheet" href="assets/css/plugins/bootstrap.min.css" />
        <link rel="stylesheet" href="assets/css/plugins/font-awesome.min.css" />
        <link rel="stylesheet" href="assets/css/plugins/ionicons.min.css" /> -->
        <!--===== Plugins CSS (All Plugins Files) =====-->
        <!-- <link rel="stylesheet" href="assets/css/plugins/jquery-ui.min.css" />
        <link rel="stylesheet" href="assets/css/plugins/meanmenu.css" />
        <link rel="stylesheet" href="assets/css/plugins/nice-select.css" />
        <link rel="stylesheet" href="assets/css/plugins/owl-carousel.css" />
        <link rel="stylesheet" href="assets/css/plugins/slick.css" /> -->
        <!--===== Main Css Files =====-->
        <!-- <link rel="stylesheet" href="assets/css/style.css" /> -->
        <!-- ===== Responsive Css Files ===== -->
        <!-- <link rel="stylesheet" href="assets/css/responsive.css" /> -->

        <!--====== Use the minified version files listed below for better performance and remove the files listed above ======-->

        <link rel="stylesheet" href="{{ asset('assets/css/vendor/plugins.min.css')}}">
        <link rel="stylesheet" href="{{ asset('assets/css/style.min.css')}}">
        <link rel="stylesheet" href="{{ asset('assets/css/responsive.min.css')}}">
		        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Development -->
<script src="https://unpkg.com/@popperjs/core@2/dist/umd/popper.min.js"></script>
<script src="https://unpkg.com/tippy.js@6/dist/tippy-bundle.umd.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/14.6.2/nouislider.min.css" integrity="sha512-O3hRQ2OLCWeTZjAqJzcWMwFGDuv/hzjfwgrKm1Q5+RhyM0Y0rhPF7epp5cdQN39zYNwGeNuixF3fE5AhHOiSzA==" crossorigin="anonymous" />
<!-- Production -->
<script src="https://unpkg.com/@popperjs/core@2"></script>
<script src="https://unpkg.com/tippy.js@6"></script>
<script src="{{ asset('js/jquery-confirm.min.js')}}"></script>
        <style>
        .sc_trust .wrapper {
    margin: 0 auto;
    min-width: 200px;
    max-width: 1680px;
    height: 100%;
    padding: 0px 20px;
    position: relative;
}
        @media only screen and (max-width: 786px){
            .side-menu-wrap {
    z-index: 9999;
            }
               .footer-area {
    background-color: #efefef;
}
            .mean-container .mean-bar {
    position: absolute;
    width: 100%;
    z-index: 99999;
    right: 0;
}
           .jconfirm-box{
                width:100% !important;
            }
            .foot_pay{
                position: inherit !important;
            }
            .pay_sec {
    position: inherit !important;
            }
            .footer-bottom{
                    margin-bottom: 100px;
            }
            .about_footer {
    width: 55% !important;
}
.sc_trust li {
    display: inline_flex;
    list-style: none;
    width: 20%;
    border-right: 1px solid #ddd;
    position: relative;
    /* margin-top: 38px; */
    border-bottom: 1px solid aliceblue;
    border-top: 1px solid aliceblue;
    padding: 26px;
}
}
        @media only screen and (max-width: 469px){
        .slider-area{
            height:90px;
        }
        .pre_img{
               width: 15% !important; 
               
           }
        }
        @media only screen and (max-width: 1169px) {
           
            .dropdown_search {
    width: 500px;
    background: #fff;
    display: none;
    border: 2px solid#4fb68d;
    border-radius: 30px;
    position: absolute;
    right: 0;
    top: 60px;
    z-index: 9;
}
    .desc-listcategoreis .name_categories h4 {
    font-size: 12px !important;
    font-weight: 600;
    text-transform: capitalize;
    margin-bottom: 12px;
    line-height: 1;
}
.desc-listcategoreis {
    left: 55% !important;
}
.desc-listcategoreis .number_product {
    font-size: 15px;
    font-weight: bold;
    color: #212121;
    text-transform: capitalize;
    margin-bottom: 12px;
    line-height: 1;
    display: block;
}
.mobile-menu-icon-box {
    padding: 8px 0;
    margin-right: 20px;
    position: absolute;
}
}
            .slick-slider {
    position: relative;
    display: block;
    box-sizing: border-box;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    -webkit-touch-callout: none;
    -khtml-user-select: none;
    -ms-touch-action: pan-y;
    touch-action: pan-y;
    -webkit-tap-highlight-color: transparent;
    height: 300px;
}
.log_reg:hover{
    color:#ff7200;
}
            .non_refund_text{
                background: #ff7200;
    padding: 10px;
    /* border-radius: 5px; */
    color: white;
    font-weight: 600;
            }
            .refund_text{
                background: #ff7200;
    padding: 10px;
    /* border-radius: 5px; */
    color: white;
    font-weight: 600;
            }
            .non_refund_text:hover{
                background: #ff7200;
    padding: 10px;
    /* border-radius: 5px; */
    color: white;
    font-weight: 600;
            }
            .refund_text:hover{
                background: #ff7200;
    padding: 10px;
    /* border-radius: 5px; */
    color: white;
    font-weight: 600;
            }
  .social-nav a .fa {
    padding: 7px;
    }
    .social-nav a .fa {
    padding: 7px;
    }
.add-to-link ul li >.cart-btn {
    padding: 7px !important;
}
.add-to-link ul li {
  
    width: 20%;
    /* float: right; */
}
.cart_li {
    margin-left: 4% !important;
    width: 53% !important;
}

@media only screen and (max-width: 567px){
    .social-nav a .fa {
    padding: 7px;
    }
.add-to-link ul li >.cart-btn {
    padding: 7px !important;
}
.add-to-link ul li {
  
    width: 20%;
    /* float: right; */
}
.cart_li {
    margin-left: 4% !important;
    width: 53% !important;
}
.hero-side-category nav.category-menu {
    background-color: #fff;
    float: left;
    width: 100%;
    left: 0;
    top: 100%;
    border: 1px solid #ebebeb;
    position: absolute;
    display: none;
    z-index: 9999;
    height: 500px;
    overflow-x: auto;
}
    .home-5 .slider-height-6 {
    height: 145px;
}
.category-list {
    width: 100%;
}
}


            .button.one{
    border:1px solid #fc743a;
}

.button.one {
    background-repeat: no-repeat !important;
    background-position: -120px -120px, 0 0 !important;
    
    background-image: -webkit-linear-gradient(
      top left,
      rgba(255, 255, 255, 0.2) 0%,
      rgba(255, 255, 255, 0.2) 37%,
      rgba(255, 255, 255, 0.8) 45%,
      rgba(255, 255, 255, 0.0) 50%
    ) !important;
    background-image: -moz-linear-gradient(
      0 0,
      rgba(255, 255, 255, 0.2) 0%,
      rgba(255, 255, 255, 0.2) 37%,
      rgba(255, 255, 255, 0.8) 45%,
      rgba(255, 255, 255, 0.0) 50%
    ) !important;    
    background-image: -o-linear-gradient(
      0 0,
      rgba(255, 255, 255, 0.2) 0%,
      rgba(255, 255, 255, 0.2) 37%,
      rgba(255, 255, 255, 0.8) 45%,
      rgba(255, 255, 255, 0.0) 50%
    ) !important;
    background-image: linear-gradient(
      0 0,
      rgba(255, 255, 255, 0.2) 0%,
      rgba(255, 255, 255, 0.2) 37%,
      rgba(255, 255, 255, 0.8) 45%,
      rgba(255, 255, 255, 0.0) 50%
    ) !important;
    
    -moz-background-size: 250% 250%, 100% 100% !important;
         background-size: 250% 250%, 100% 100% !important;
    
    -webkit-transition: background-position 0s ease !important;
       -moz-transition: background-position 0s ease !important;       
         -o-transition: background-position 0s ease !important;
            transition: background-position 0s ease !important;
  }
  .hoverContent {
    display: none;
    opacity: 1;
    padding-top: 5px;
    -webkit-transition: all .2s ease-in-out;
    -moz-transition: all .2s ease-in-out;
    -ms-transition: all .2s ease-in-out;
    -o-transition: all .2s ease-in-out;
    top: 130%;
    position: absolute;
    right: 0;
    z-index: 9999;
    margin-top: -17px;
}
.sc_actLinks {
    width: 238px;
    background: #fff;
    -webkit-box-shadow: 0px 6px 14px 0px rgba(0,0,0,0.2);
    -moz-box-shadow: 0px 6px 14px 0px rgba(0,0,0,0.2);
    box-shadow: 0px 6px 14px 0px rgba(0,0,0,0.2);
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
    -moz-background-clip: padding;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
}
.sc_actLinks .actLink_heading {
    width: 100%;
    background: linear-gradient(45deg, #ff5959a8, #583ab3);
    min-height: 60px;
    color: #fff;
    position: relative;
    height: 60px;
    float: left;
    -webkit-border-radius: 4px 4px 0 0;
    -moz-border-radius: 4px 4px 0 0;
    border-radius: 4px 4px 0 0;
    -moz-background-clip: padding;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
}
.sc_actLinks .actLink_heading a {
    font-size: 18px;
    color: #fff;
    font-weight: 600;
    display: inline-block;
    width: 100%;
    padding-right: 20px;
    line-height: 39px;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
    margin: 7px 0px 0px 55px;
}
.sc_actLinks .actLink_heading.vip_on a.vip_nuser_btn {
    position: absolute;
    top: 34px;
    left: 71px;
    width: auto;
    padding: 3px 11px;
    line-height: normal;
    font-size: 12px;
    font-weight: normal;
    background-color: #222533;
}
.sc_actLinks ul {
    display: inline-block;
    width: 100%;
    padding: 0;
}
.sc_actLinks ul li {
    list-style-type: none;
    display: inline-block;
    width: 100%;
    position: relative;
    text-align: left;
    border-bottom: 1px solid #f7a05826;
}
.sc_actLinks ul li a {
    display: inline-block;
    text-indent: 0px;
    font-size: 15px;
    font-weight: 400;
    color: #222533;
    padding: 12px;
}
.sc_actLinks ul li a i{
    margin-right: 10px;
}
.sc_header .sc_actLinks ul li .usr_ordr {
    background-position: -249px -48px;
}

.log_reg:hover + .hoverContent {
    display: block !important;
}
.cart_val{
    background: #ff7200;
    color: white;
    padding: 0px 0px 4px 6px;
    border-radius: 100%;
    position: absolute;
    margin: 11px 0px 0px -20px;
    width: 22px;
    height: 23px;
}
.login_top > a:hover{
    color:#ff7200 !important;
}
.log_addon{
    border-radius: 19px 0px 0px 19px;
    border-right: none !important;
    background: white;
    box-shadow: 6px 6px 12px 0px #3236ff99;
    color: #3d49dc !important;
}
.log_input{
    border-radius: 0px 19px 19px 0px;
    box-shadow: 6px 6px 12px 0px #3236ff99;
    background-color: white !important;
    border-left: none !important;
}
.log-bg{
    background: url('{{asset("assets/images/Login_BG.png")}}');
}
.log-bg1{
    background: url('{{asset("assets/images/shapes.png")}}');
    padding-top: 10%;
}
.text-md1{
    color: white;
    font-size: 24px !important;
    font-weight: 900;
    line-height: 45px;
}
.text-md2{
    color: white;
    font-size: 20px !important;
}
.need1{
    margin-top: 8%;
}
.reg_but{
    margin-top: 3%;
}

.pad-0{
    box-shadow: 1px 1px 14px 5px #888290;
    padding-left: 0px !important;
    padding-right: 0px !important;
}
.btn_reg{
    padding: 4%;
    
    color: white;
    border-radius: 6px;
    box-shadow: 5px 6px 7px 2px #504848;
    background-color: #ff7200 !important;
    color: white;
    background-repeat: no-repeat !important;
    background-position: -120px -120px, 0 0 !important;
    
    background-image: -webkit-linear-gradient(
      top left,
      rgba(255, 255, 255, 0.2) 0%,
      rgba(255, 255, 255, 0.2) 37%,
      rgba(255, 255, 255, 0.8) 45%,
      rgba(255, 255, 255, 0.0) 50%
    ) !important;
    background-image: -moz-linear-gradient(
      0 0,
      rgba(255, 255, 255, 0.2) 0%,
      rgba(255, 255, 255, 0.2) 37%,
      rgba(255, 255, 255, 0.8) 45%,
      rgba(255, 255, 255, 0.0) 50%
    ) !important;    
    background-image: -o-linear-gradient(
      0 0,
      rgba(255, 255, 255, 0.2) 0%,
      rgba(255, 255, 255, 0.2) 37%,
      rgba(255, 255, 255, 0.8) 45%,
      rgba(255, 255, 255, 0.0) 50%
    ) !important;
    background-image: linear-gradient(
      0 0,
      rgba(255, 255, 255, 0.2) 0%,
      rgba(255, 255, 255, 0.2) 37%,
      rgba(255, 255, 255, 0.8) 45%,
      rgba(255, 255, 255, 0.0) 50%
    ) !important;
    
    -moz-background-size: 250% 250%, 100% 100% !important;
         background-size: 250% 250%, 100% 100% !important;
    
    -webkit-transition: background-position 0s ease !important;
       -moz-transition: background-position 0s ease !important;       
         -o-transition: background-position 0s ease !important;
            transition: background-position 0s ease !important;
}
.btn_reg:hover {
    padding: 4%;
    
    color: white;
    border-radius: 6px;
    box-shadow: 5px 6px 7px 2px #504848;
    background-position: 0 0, 0 0 !important;
    color: white !important;
    -webkit-transition-duration: 0.5s !important;
       -moz-transition-duration: 0.5s !important;
            transition-duration: 0.5s !important;
  }
  .reg-bg{
    background: url('{{asset("assets/images/Vendor_BG.png")}}') no-repeat !important;
  }
  .seller_reg{
    background: url('{{asset("assets/images/Regis_BG.png")}}') no-repeat !important;   
  }
  .breadcrumb-area {
    padding: 5px 0 !important;
    border-bottom: 1px solid #e9e9e9;
    background-color: #fafafa;
}
.breadcrumb-area {
    text-align: center;
    position: relative;
    padding: 70px 0;
    background: none no-repeat !important;
    background-size: cover;
}
.breadcrumb {
    
    margin-bottom: 0rem !important;
}
.aiz-rounded-check:after {
    background: #ff7200 !important;
}
.sc_actLinks .actLink_heading {
    width: 100%;
    /* background: linear-gradient(236deg, #f0456c, #faad3b); */
    min-height: 60px;
    color: #fff;
    position: relative;
    height: 60px;
    float: left;
    background: linear-gradient(450deg, #ff7200, #ff7200);
    -webkit-border-radius: 4px 4px 0 0;
    -moz-border-radius: 4px 4px 0 0;
    border-radius: 4px 4px 0 0;
    -moz-background-clip: padding;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
}
/* .middle{
     transition: .5s ease; 
    transition: .5s cubic-bezier(0, 0.81, 0.57, 1.45);
    opacity: 1;
    position: absolute;
    top: 50%;
    left: 50%;
    background: #9e6e1e70;
    width: 100%;
    height: 100%;
    transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    text-align: center;
} */
/* .sc_actLinks .actLink_heading:hover  .middle{
    opacity:1;
    width:0;
} */
.jconfirm-box{
    width: 30%;
}
.swal2-popup .swal2-select {
    display: none;
}
.swal-button {
  padding: 7px 19px;
  border-radius: 2px;
  background-color: #4962B3;
  font-size: 12px;
  border: 1px solid #3e549a;
  text-shadow: 0px -1px 0px rgba(0, 0, 0, 0.3);
}
.cart_icon_home{
    font-size: 40px;
    margin-left: 25px;
    color: #362b33;
}
.nav-box-number{
    position: absolute;
    top: 0;
    left: 53px;
    min-width: 20px;
    height: 20px;
    line-height: 20px;
    text-align: center;
    border-radius: 18px;
    color: #fff;
    font-size: 12px;
    background-color: #ff2a00;
}
.dropdown-menu {
    border: 1px solid #eceff1;
    border-radius: 1px;
    line-height: 1;
    min-width: 12rem;
    box-shadow: 0 5px 25px 0 rgba(123, 123, 123, 0.15);
}
.dropdown-cart {
    min-width: 360px;
    padding: 0 1rem;
}
.dropdown-cart .dc-header {
    padding: 5px;
    text-align: center;
}
.dropdown-cart .dc-header {
    padding: 1rem 1.5rem;
    border-bottom: 1px solid rgba(0, 0, 0, 0.05);
}
.dropdown-cart .dc-header .heading {
    margin: 0;
    color: rgba(0, 0, 0, 0.7);
}
.dropdown-menu.show {
    display: block;
    z-index: 9999;
}
.log_reg {
    margin-right: 9%;
}
.cart_icon_home:hover{
color: #ff7200;
}
.vertical-menu ul li ul.mega-menu-wrap>li ul li.mega-menu-title a{
    line-height: 1.6 !important;
}

@media only screen and (max-width: 568px) {
    .banner-wrapper {
   margin-bottom: 0px !important;
}
.mb-res-xs-30 {
    margin-bottom: 0px !important;
}
.animated{
    font-size: 15px;
}
    .made_india{
        width: 40%;
    }
    .made_india_right{
        width: 60%;
        padding-left: 0px;
    }
    .header-right-nav {
    display: inline-flex !important;
    float: none;
    text-align: center;
}
.res-xs-flex {
    margin: -9px 0 !important;
}
.header-right-nav .after-n a {
    display: inline-flex;
    width: 112%;
}
.cdown p{
    font-size: 10px !important;
}
.cdown {
    font-size: 10px !important;
}
  /* For mobile phones: */
  .c-scrollbar {
    min-height: 500px;
  }
  .filter-title{
    height: 20px;
  }
  .home-6 .dropdown_search {
    margin-left: -46px;
    top: 60px;
    /* width: 378px; */
    box-shadow: 0 0 4px black;
}
  .cart_icon_home {
    font-size: 24px;
    margin-left: 3px;
    color: #362b33;
    margin-top: 18px;
    margin-right: 12px;
}
.nav-box-number {
    position: absolute;
    top: 27px;
    left: 20px;
    min-width: 20px;
    height: 20px;
    line-height: 20px;
    text-align: center;
    border-radius: 18px;
    color: #fff;
    font-size: 12px;
    background-color: #ff2a00;
}
.mean-nav1{
    float: left;
    width: 100%;
    background: transparent;
}
.mean-container .mean-nav1 ul li {
    background: #f8f8f8 none repeat scroll 0 0;
    float: left;
    position: relative;
    width: 100%;
    border-top: 1px solid#ddd;
}
.mean-container .mean-nav1 ul li a {
    background: #f8f8f8 none repeat scroll 0 0;
    color: #333;
    display: block;
    float: left;
    font-size: 13px;
    margin: 0;
    padding: 10px 5%;
    text-align: left;
    text-decoration: none;
    text-transform: uppercase;
    width: 90%;
    font-weight: 500;
}
.mean-container .mean-nav1 ul li li a {
    border-top: 1px solid rgba(255, 255, 255, 0.25);
    opacity: 1;
    padding: 1em 10%;
    text-shadow: none !important;
    visibility: visible;
    width: 80%;
    font-weight: normal;
    text-transform: capitalize;
    color: #444;
}
.mean-container .mean-nav1 ul li a.mean-expand {
    width: 26px;
    height: 15px;
    margin-top: 1px;
    padding: 6px 12px !important;
    text-align: center;
    position: absolute;
    right: 0;
    top: 0;
    z-index: 2;
    font-weight: 700;
    background: rgba(255,255,255,.1);
    border: 0!important;
    background: #F8F8F8;
}
.mean-container .mean-nav1 ul li a.mean-expand1 {
    width: 26px;
    height: 15px;
    margin-top: 1px;
    padding: 6px 12px !important;
    text-align: center;
    position: absolute;
    right: 0;
    top: 0;
    z-index: 2;
    font-weight: 700;
    background: rgba(255,255,255,.1);
    border: 0!important;
    background: #F8F8F8;
}

.sc_trust li {
    display: block;
    list-style: none;
    width: 100%;
    border-right: 1px solid #ddd;
    position: relative;
    /* margin-top: 38px; */
    border-bottom: 1px solid aliceblue;
    border-top: 1px solid aliceblue;
    padding: 26px;
}
.sc_trust li span {
    display: inline-block;
    font-size: 20px;
    color: #757575;
    width: 100%;
}
.sc_trust .wrapper {
    margin: 0 auto;
    min-width: 100%;
    max-width: 1680px;
    height: 100%;
    padding: 0px 20px;
    position: relative;
}
.nwes_desc{
    display: none;
}
.news_icon{
    display: none !important;
}
.desc-listcategoreis .name_categories h4 {
    font-size: 12px !important;
    font-weight: 600;
    text-transform: capitalize;
    margin-bottom: 12px;
    line-height: 1;
}
.desc-listcategoreis {
    left: 55% !important;
}
.desc-listcategoreis .number_product {
    font-size: 15px;
    font-weight: bold;
    color: #212121;
    text-transform: capitalize;
    margin-bottom: 12px;
    line-height: 1;
    display: block;
}
.mobile-menu-icon-box {
    padding: 8px 0;
    margin-right: 20px;
    position: absolute;
    margin-left: 15px;
}
}
.sticky-top {
    position: -webkit-sticky;
    position: sticky;
    top: 66px !important;
    z-index: 1020;
}
.goog-logo-link {
   display:none !important;
}

.goog-te-gadget {
   color: transparent !important;
  
}

.goog-te-gadget .goog-te-combo {
   color: white !important;
   background: #222533;
    border: 1px solid #222533;
    /* width: 70%; */
}
.goog-te-banner-frame.skiptranslate {
    display: none !important;
    } 
body {
    top: 0px !important; 
    }
    .google_translate_element{
        /* height: 70%; */
    }
    .skiptranslate.goog-te-gadget >div{
        height: 10px;
    }
    .goog-te-gadget .goog-te-combo{
        cursor: pointer;
    }
   img.first-img {
    height: 280px;
    width: 100%;
}
img.second-img {
    height: 280px;
    width: 100%;
}
.lblCartCount {
    position: absolute;
    top: 0;
    /* left: 53px; */
    right: 61px;
    min-width: 20px;
    height: 21px;
    line-height: 20px;
    text-align: center;
    border-radius: 18px;
    color: #fff;
    font-size: 12px;
    background-color: #ff2a00
}
a.sell_us:hover{
    color: #ff7200 !important;
   
}
.sc_actLinks .actLink_heading {
    width: 100%;
    /* background: linear-gradient(236deg, #f0456c, #faad3b); */
    min-height: 51px;
    color: #fff;
    position: relative;
    height: 44px;
    padding-top: 5px;
    float: left;
    background: linear-gradient(450deg, #ff7200, #ff7200);
    -webkit-border-radius: 4px 4px 0 0;
    -moz-border-radius: 4px 4px 0 0;
    border-radius: 4px 4px 0 0;
    -moz-background-clip: padding;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
}

.bg-img {
    background-position: center center;
    background-size: contain !important;
    position: relative;
    z-index: 1;
    background-color: #ffffff;
    background-repeat: no-repeat;
}
.sc_actLinks .actLink_heading a {
    font-size: 18px;
    color: #fff;
    font-weight: 600;
    display: inline-block;
    width: 100%;
    padding-right: 20px;
    line-height: 39px;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
    margin: 0px 0px 0px 55px;
}
#preloader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #fbfbfb;
    z-index: 999999;
}
.pre_img {
position: absolute;
    left: 50%;
    top: 40%;
    margin-left: -32px;
    margin-top: -32px;
    width:8%;
}
.sc_actLinks .actLink_heading a {
    margin: 0px 0px 0px 15px;
}
/* 
.hero-side-category nav.category-menu>ul>li.menu-item-has-children>i {
    display: inline-flex;
}
.hero-side-category nav.category-menu>ul>li>i {
    padding: 12px 0px 0px 240px;
} */
        </style>
        
    </head>

    <body class="home-5 home-6 home-8 home-cosmatics">
        <!-- main layout start from here -->
        <!--====== PRELOADER PART START ======-->

        <div id="preloader">
        <!--<div class="preloader">-->
            <!-- <span></span>
            <span></span> -->
            <img class="pre_img" src="/img/L2V_Gif.gif">
        <!--</div>-->
    </div>
    
<script>
    function googleTranslateElementInit() {
        new google.translate.TranslateElement(
            {pageLanguage: 'en',includedLanguages: 'en,hi'},
            'google_translate_element'
        );
    }
</script>
<script src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        <!--====== PRELOADER PART ENDS ======-->
        <div id="main">
            <!-- Header Start -->
            <header class="main-header">
                <!-- Header Top Start -->
                <div class="header-top-nav">
                    <div class="container">
                        <div class="row">
                            <!--Left Start-->
                            <div class="col-lg-4 col-md-4 made_india">
                                <div class="left-text">
                                    <p>Made In India <img src="{{asset('assets/images/icons/2.jpg')}}"></p>
                                </div>
                            </div>
                            <!--Left End-->
                            <!--Right Start-->
                            <div class="col-lg-8 col-md-8 text-right made_india_right">
                                <div class="header-right-nav">
                                    <ul class="res-xs-flex">
                                        @auth
                                        @if(Auth::user()->user_type=="seller")
                                        <li class="after-n" style="color:white;cursor: default;">
                                        <a href="{{route('dashboard')}}"><i class="fa fa-user"></i>{{Auth::user()->name}}</a>
                                        </li>
                                        @else
                                        <li class="after-n" style="color:white;cursor: default;">
                                        <a href="https://{{env('APP_SUBDOMAIN')}}/shops/create" class="sell_us">  Sell with Us &nbsp;&nbsp;<i class="fa fa-chevron-right"></i></a>
                                        </li>
                                        @endif
                                        @endauth
                                        @guest
                                        <li class="after-n" style="color:white;cursor: default;">
                                        <a href="https://{{env('APP_SUBDOMAIN')}}/shops/create"  class="sell_us">  Sell with Us &nbsp;&nbsp;<i class="fa fa-chevron-right"></i></a>
                                        </li>
                                         @endguest
                                       
                                       
                                      
                                        
                                    </ul>
                               
                                    <div class="dropdown-navs">
                                        <ul>
                                            
                                            <!-- Currency End -->
                                            <!-- Language Start -->
                                            <li class="top-10px mr-15px" style="background-color: #222533;">
                                                <!-- <select>
                                                    <option value="1">English</option>
                                                    <option value="2">Hindi</option>
                                                </select> -->
                                                <div id="google_translate_element"></div>
                                            </li>
                                            <!-- Language End -->
                                        </ul>
                                    </div>
                                </div>
                                

                            </div>
                            <!--Right End-->
                        </div>
                    </div>
                </div>
                <!-- Header Top End -->
                <!-- Header Buttom Start -->
                <div class="header-navigation d-lg-block d-none">
                    <div class="container">
                        <div class="row">
                            <!-- Logo Start -->
                            <div class="col-md-2 col-sm-2">
                                <div class="logo">
                                    <a href="{{ route('home') }}"><img src="{{ asset('assets/images/logo/logo-cosmatics.png') }}" alt="" style="width:100%;"/></a>
                                </div>
                            </div>
                            <!-- Logo End -->
                            <div class="col-md-10 col-sm-10">
                                <!--Header Bottom Account Start -->
                                <div class="header_account_area">
                                    <!--Seach Area Start -->
                                    <div class="header_account_list search_list">
                                        <a href="javascript:void(0)"><i class="ion-ios-search-strong"></i></a>
                                        <div class="dropdown_search">
                                            <form action="{{ route('search') }}" method="GET">
                                            <i class="fa fa-search icon"></i> 
                                                <input style="font-family: FontAwesome,sans-serif;" aria-label="Search" id="search" name="q"  autocomplete="off" placeholder="Search Now..." type="text"  class="search_front"/>

                                                <button type="submit" class="button one">SEARCH</button>
                                                <div class="typed-search-box d-none">
                                            <div class="search-preloader">
                                                <div class="loader"><div></div><div></div><div></div></div>
                                            </div>
                                            <div class="search-nothing d-none">

                                            </div>
                                            <div id="search-content">

                                            </div>
                                        </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!--Seach Area End -->
                                    <!--Contact info Start -->
                                    <div class="contact-link-wrap">
                                    
                                        <i class="fa fa-user-o log_reg" style="font-size: 33px;color: #222533;cursor: pointer;"></i>
                                        <a href="{{ route('wishlists.index') }}" style="color: #222533;"><i class="fa fa-heart-o favart_add1" style="font-size: 33px;"></i>
                                            @php
                                            $wish_count=0;
                                            if(Auth::check()){
                                             $wish_count=\App\Wishlist::where('user_id', Auth::user()->id)->count();
                                            }
                                            
                                            @endphp
                                            @if( $wish_count>0)
                                             <span class="lblCartCount">{{ $wish_count}}</span>
                                         @else
                                             <span class="lblCartCount">0</span>
                                         @endif
                                        </a>
                                        <div class="hoverContent">
                          <div class="sc_actLinks">
                            @auth
                            @if(Auth::user()->user_type=="customer")
                            <div class="actLink_heading vip_on" id="vip_on">
                            <a href="/dashboard" id="show_loginpop" >Hi , {{Auth::user()->name}}</a>
                            
                               
                            </div>
                          
                            @else 
                             <div class="actLink_heading vip_on" id="vip_on">
                                <a href="/users/login" id="show_loginpop" ><i class="fa fa-sign-in" aria-hidden="true">&nbsp;&nbsp;Login</i></a> 
                                
                          
                                </div>
                                <!-- <div class="actLink_heading vip_on" id="vip_on">
                                   
     
                                <a href="{{url('/users/registration')}}" id="show_loginpop" ><i class="fa fa-user-plus" aria-hidden="true">&nbsp;&nbsp;Register</i></a>
                                
                            </div> -->
                                    @endif
                            @endauth
                            @guest
                            <div class="actLink_heading vip_on" id="vip_on">
                                <a href="/users/login" id="show_loginpop" ><i class="fa fa-sign-in" aria-hidden="true">&nbsp;&nbsp;Login</i></a> 
                                
                          
                                </div>
                                <!-- <div class="actLink_heading vip_on" id="vip_on">
                                   
     
                                <a href="{{url('/users/registration')}}" id="show_loginpop" ><i class="fa fa-user-plus" aria-hidden="true">&nbsp;&nbsp;Register</i></a>
                                
                            </div> -->
                            @endguest
                            <ul> 
                            @auth
                            @if(Auth::user()->user_type=="customer")
                                <li><a href="/dashboard" ><i class="fa fa-dashboard"></i>My Dashboard</a></li>
                               
                                                               <li><a href="{{ route('purchase_history.index') }}" ><i class="fa fa-list"></i>My Orders</a></li>
                                                               @endif
                                                               @endauth
                                                            {{--  <li><a href="javascript:void(0);" ><i class="fa fa-envelope"></i>My Returns</a></li> --}}
                                
                                                              <li><a href="{{ route('compare') }}" ><i class="fa fa-compress"></i>Compare</a></li>

                                                              <li><a href="{{ route('wishlists.index') }}" ><i class="fa fa-heart"></i>Wishlist</a></li>
                                                              @auth
                                                              @if(Auth::user()->user_type=="customer")

                                                              <li><a href="{{ route('profile') }}"><i class="fa fa-user"></i>My Profile</a></li>
                                                              
                                                              <li><a href="{{ route('logout') }}"> <i class="fa fa-sign-out"></i>{{ translate('Logout')}}</a></li>
                                    @endif
                                    @endauth
                                                            </ul>
                          </div>
                        </div>
                                        <!--Contact info End -->
                                        <!--Cart info Start -->
                                        <div class="d-inline-block" data-hover="dropdown">
                                    <div class="nav-cart-box dropdown" id="cart_items">
                                        <a href="" class="nav-box-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="ion-bag d-inline-block nav-box-icon cart_icon_home cart_add"></i>
                                            
                                            @if(Session::has('cart'))
                                                <span class="nav-box-number">{{ count(Session::get('cart'))}}</span>
                                            @else
                                                <span class="nav-box-number">0</span>
                                            @endif
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right px-0">
                                            <li>
                                                <div class="dropdown-cart px-0">
                                                    @if(Session::has('cart'))
                                                        @if(count($cart = Session::get('cart')) > 0)
                                                            <div class="dc-header">
                                                                <h3 class="heading heading-6 strong-700">{{translate('Cart Items')}}</h3>
                                                            </div>
                                                            <div class="dropdown-cart-items c-scrollbar">
                                                                @php
                                                                    $total = 0;
                                                                @endphp
                                                                @foreach($cart as $key => $cartItem)
                                                                    @php
                                                                        $product = \App\Product::find($cartItem['id']);
                                                                        $total = $total + $cartItem['price']+$cartItem['tax']*$cartItem['quantity'];
                                                                    @endphp
                                                                    <div class="dc-item">
                                                                        <div class="d-flex align-items-center">
                                                                            <div class="dc-image">
                                                                                <a href="{{ route('product', $product->slug) }}">
                                                                                    <img src="{{ asset('frontend/images/placeholder.jpg') }}" data-src="{{ asset($product->thumbnail_img) }}" class="img-fluid lazyload" alt="{{ __($product->name) }}">
                                                                                </a>
                                                                            </div>
                                                                            <div class="dc-content">
                                                                                <span class="d-block dc-product-name text-capitalize strong-600 mb-1">
                                                                                    <a href="{{ route('product', $product->slug) }}">
                                                                                        {{ __($product->name) }}
                                                                                    </a>
                                                                                </span>

                                                                                <span class="dc-quantity">x{{ $cartItem['quantity'] }}</span>
                                                                                <span class="dc-price">{{ single_price($cartItem['price']+$cartItem['tax']*$cartItem['quantity']) }}</span>
                                                                            </div>
                                                                            <div class="dc-actions">
                                                                                <button onclick="removeFromCart({{ $key }})">
                                                                                    <i class="la la-close"></i>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                            <div class="dc-item py-3">
                                                                <span class="subtotal-text">{{translate('Subtotal')}}</span>
                                                                <span class="subtotal-amount">{{ single_price($total) }}</span>
                                                            </div>
                                                            <div class="py-2 text-center dc-btn">
                                                                <ul class="inline-links inline-links--style-3">
                                                                    <li class="px-1">
                                                                        <a href="{{ route('cart') }}" class="link link--style-1 text-capitalize btn btn-base-1 px-3 py-1">
                                                                            <i class="la la-shopping-cart"></i> {{translate('View cart')}}
                                                                        </a>
                                                                    </li>
                                                                    @if (Auth::check())
                                                                    <li class="px-1">
                                                                        <a href="{{ route('checkout.shipping_info') }}" class="link link--style-1 text-capitalize btn btn-base-1 px-3 py-1 light-text">
                                                                            <i class="la la-mail-forward"></i> {{translate('Checkout')}}
                                                                        </a>
                                                                    </li>
                                                                    @endif
                                                                </ul>
                                                            </div>
                                                        @else
                                                            <div class="dc-header">
                                                                <h3 class="heading heading-6 strong-700">{{translate('Your Cart is empty')}}</h3>
                                                            </div>
                                                        @endif
                                                    @else
                                                        <div class="dc-header">
                                                            <h3 class="heading heading-6 strong-700">{{translate('Your Cart is empty')}}</h3>
                                                        </div>
                                                    @endif
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                        <!-- <div class="cart-info d-flex">
                                            <div class="mini-cart-warp">
                                            @if(Session::has('cart'))
                                                        @if(count($cart = Session::get('cart')) > 0)
                                                <a href="#" class="count-cart"><span class="cart_val nav-box-number">{{ count(Session::get('cart')) }}</span></a>
                                                @else
                                                <a href="#" class="count-cart"><span class="cart_val nav-box-number">0</span></a>
                                                @endif
                                                @else
                                                <a href="#" class="count-cart"><span class="cart_val nav-box-number">0</span></a>
                                                @endif
                                                <div class="mini-cart-content">
                                                @if(Session::has('cart'))
                                                        @if(count($cart = Session::get('cart')) > 0)
                                                    <ul>
                                                    @php
                                                                    $total = 0;
                                                                @endphp
                                                                @foreach($cart as $key => $cartItem)
                                                                    @php
                                                                        $product = \App\Product::find($cartItem['id']);
                                                                        $total = $total + $cartItem['price']*$cartItem['quantity'];
                                                                    @endphp
                                                        <li class="single-shopping-cart">
                                                            <div class="shopping-cart-img">
                                                                <a href="{{ route('product', $product->slug) }}"><img alt="" src="{{ asset($product->thumbnail_img) }}" /></a>
                                                                <span class="product-quantity">x{{ $cartItem['quantity'] }}</span>
                                                            </div>
                                                            <div class="shopping-cart-title">
                                                                <h4><a href="{{ route('product', $product->slug) }}"> {{ __($product->name) }}</a></h4>
                                                                <span>{{ single_price($cartItem['price']*$cartItem['quantity']) }}</span>
                                                                <div class="shopping-cart-delete">
                                                                    <a href="javascript:" onclick="removeFromCart({{ $key }})"><i class="ion-android-cancel"></i></a>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        @endforeach
                                                        
                                                    </ul>
                                                    <div class="shopping-cart-total">
                                                        <h4>{{translate('Subtotal')}} : <span>{{ single_price($total) }}</span></h4>
                                                        
                                                    </div>
                                                    
                                                    <div class="shopping-cart-btn text-center">
                                                        <a class="default-btn" href="{{ route('cart') }}">checkout</a>
                                                    </div>
                                                    @else
                                                            <div class="dc-header">
                                                                <h3 class="heading heading-6 strong-700">{{translate('Your Cart is empty')}}</h3>
                                                            </div>
                                                        @endif
                                                        @else
                                                        <div class="dc-header">
                                                            <h3 class="heading heading-6 strong-700">{{translate('Your Cart is empty')}}</h3>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div> -->
                                        <!--Cart info End -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Header Bottom Account End -->
                
                @include('frontend.inc.nav')
                @include('frontend.inc.head_mob_nav')
                @include('frontend.inc.category_menu')
                
				{{-- @include('frontend.inc.category_menu') --}}
				
                
            </header>
            <!-- Header End -->
            @auth
                            @if(Auth::user()->user_type=="customer")
    
            <div class="d-block d-lg-none mobile-menu-icon-box">
                                <!-- Navbar toggler  -->
                                <a href="" onclick="sideMenuOpen(this)">
                                    <div class="hamburger-icon">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </a>
                            </div>
                            @endif
                                    @endauth
    @yield('content')
	
    @include('frontend.inc.footer')
    
            
        </div>

       @include('frontend.partials.modal')
       <div class="modal fade" id="addToCart">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="c-preloader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
                <button type="button" class="close absolute-close-btn" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div id="addToCart-modal-body">

                </div>
            </div>
        </div>
    </div>
       <!-- SCRIPTS -->
<!-- <a href="#" class="back-to-top btn-back-to-top"></a> -->

<!-- Core -->
<script src="{{ asset('frontend/js/vendor/popper.min.js') }}"></script>
<script src="{{ asset('frontend/js/vendor/bootstrap.min.js') }}"></script>

<!-- Plugins: Sorted A-Z -->
<script src="{{ asset('frontend/js/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('frontend/js/select2.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/14.6.2/nouislider.min.js" integrity="sha512-hwgsSKm7cPjNHLACxyMGVRrd7cH+wveyYM1ZxWxxbCZd/pXdMukyuIrrCjt2I7iLsmYDk02B1ZUcarEv1A3RVQ==" crossorigin="anonymous"></script>
<!-- <script src="{{ asset('frontend/js/nouislider.min.js') }}"></script> -->
<script src="{{ asset('frontend/js/sweetalert2.min.js') }}"></script>
<script src="{{ asset('frontend/js/slick.min.js') }}"></script>
<script src="{{ asset('frontend/js/jssocials.min.js') }}"></script>
<script src="{{ asset('frontend/js/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('frontend/js/jodit.min.js') }}"></script>
<script src="{{ asset('frontend/js/xzoom.min.js') }}"></script>
<script src="{{ asset('frontend/js/fb-script.js') }}"></script>
<script src="{{ asset('frontend/js/lazysizes.min.js') }}"></script>
<script src="{{ asset('frontend/js/intlTelInput.min.js') }}"></script>

<!-- App JS -->
<script src="{{ asset('frontend/js/active-shop.js') }}"></script>
<script src="{{ asset('frontend/js/main.js') }}"></script>
<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

<script>
slickInit();
    $(".log_reg").hover(function(){
    $('.hoverContent').show();
},function(){
    $('.hoverContent').hide();
    $(".hoverContent").hover(function(){
    $('.hoverContent').show();
},function(){
    $('.hoverContent').hide();
});
  
});
    function showFrontendAlert(type, message){
        if(type == 'danger'){
            type = 'error';
        }

       
        swal({
            position: 'center',
            type: type,
            title: message,
            showCancelButton: false,
            confirmButtonColor: '#ff7200', // There won't be any cancel button
            showConfirmButton: true ,
            timer: 10000
        });
    }

function getReferLink(){

            var token = '{{csrf_token()}}';
   
           var form_data=new FormData();
       
           form_data.append('_token',token);
           $.ajax({
               type: "post",
               url: "{{ route('refer_show') }}",
               cache: false,
               processData: false,
               contentType: false,
               data: form_data,
               dataType:"json",
               success: function(response) {
                $.confirm({
                    boxWidth: '30%',
                        type: 'green',
                        icon:'fa fa-check',
                        title: 'Referral Code',
                        content: response.html,
                       
                        useBootstrap: false,
                        smoothContent:true,
                        buttons: {
                        ok: function () {
                            
                        },

                        }
                        });
                   },
                           error: function (jqXHR, textStatus, errorThrown) {
                             
                               var msg = "";
                               if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                                   msg += "<strong>Something Went Wrong,Please Reload and Try Again</strong>";
                               } else {
                                   if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                       msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                   } else {
                                       msg += "Error(s):<strong><ul>";
                                       $.each(jqXHR.responseJSON['errors'], function (key, value) {
                                           msg += "<li>" + value + "</li>";
                                       });
                                       msg += "</ul></strong>";
                                   }
                               }
                               isValid=0;
                               showFrontendAlert("error", msg);
                           }
           });
}
    function myFunction() {
  /* Get the text field */
  var copyText = document.getElementById("myInput");

  /* Select the text field */
  copyText.select();
  copyText.setSelectionRange(0, 99999); /*For mobile devices*/

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  //alert("Copied the text: " + copyText.value);
}
</script>

@foreach (session('flash_notification', collect())->toArray() as $message)
    <script>
        showFrontendAlert('{{ $message['level'] }}', '{{ $message['message'] }}');
    </script>
@endforeach
<script>

    $(document).ready(function() {
        $('#google_translate_element').bind('DOMNodeInserted', function(event) {
      $('.goog-te-combo option:first').html('English');
    
    });
        //$('.goog-te-combo option:first').html('English');
        $(".slick-slide").addClass("margin11");
        $('.successErrorMessage').delay(8000).slideUp(300);
        $('.category-nav-element').each(function(i, el) {
            $(el).on('mouseover', function(){
                if(!$(el).find('.sub-cat-menu').hasClass('loaded')){
                    $.post('{{ route('category.elements') }}', {_token: '{{ csrf_token()}}', id:$(el).data('id')}, function(data){
                        $(el).find('.sub-cat-menu').addClass('loaded').html(data);
                    });
                }
            });
        });
        if ($('#lang-change').length > 0) {
            $('#lang-change .dropdown-item a').each(function() {
                $(this).on('click', function(e){
                    e.preventDefault();
                    var $this = $(this);
                    var locale = $this.data('flag');
                    $.post('{{ route('language.change') }}',{_token:'{{ csrf_token() }}', locale:locale}, function(data){
                        location.reload();
                    });

                });
            });
        }

        if ($('#currency-change').length > 0) {
            $('#currency-change .dropdown-item a').each(function() {
                $(this).on('click', function(e){
                    e.preventDefault();
                    var $this = $(this);
                    var currency_code = $this.data('currency');
                    $.post('{{ route('currency.change') }}',{_token:'{{ csrf_token() }}', currency_code:currency_code}, function(data){
                        location.reload();
                    });

                });
            });
        }
    });

    $('#search').on('keyup', function(){
        search();
    });

    $('#search').on('focus', function(){
        search();
    });

    function search(){
        var search = $('#search').val();
        if(search.length > 0){
            $('body').addClass("typed-search-box-shown");

            $('.typed-search-box').removeClass('d-none');
            $('.search-preloader').removeClass('d-none');
            $.post('{{ route('search.ajax') }}', { _token: '{{ @csrf_token() }}', search:search}, function(data){
                if(data == '0'){
                    // $('.typed-search-box').addClass('d-none');
                    $('#search-content').html(null);
                    $('.typed-search-box .search-nothing').removeClass('d-none').html('Sorry, nothing found for <strong>"'+search+'"</strong>');
                    $('.search-preloader').addClass('d-none');

                }
                else{
                    $('.typed-search-box .search-nothing').addClass('d-none').html(null);
                    $('#search-content').html(data);
                    $('.search-preloader').addClass('d-none');
                }
            });
        }
        else {
            $('.typed-search-box').addClass('d-none');
            $('body').removeClass("typed-search-box-shown");
        }
    }

    function updateNavCart(){
        $.post('{{ route('cart.nav_cart') }}', {_token:'{{ csrf_token() }}'}, function(data){
            $('#cart_items').html(data);
        });
    }

    function removeFromCart(key){
        $.post('{{ route('cart.removeFromCart') }}', {_token:'{{ csrf_token() }}', key:key}, function(data){
            updateNavCart();
            $('#cart-summary').html(data);
            showFrontendAlert('success', 'Item has been removed from cart');
            $('#cart_items_sidenav').html(parseInt($('#cart_items_sidenav').html())-1);
        });
    }
    function removeFromCompare(key){
        $.post('{{ route('compare.removeFromCompare') }}', {_token:'{{ csrf_token() }}', id:key}, function(data){
            
           showFrontendAlert('success', 'Item has been removed from Compare List');
            location.reload().setTimeOut(500);
           
        });
    }

    function addToCompare(id){
        $.post('{{ route('compare.addToCompare') }}', {_token:'{{ csrf_token() }}', id:id}, function(data){
            
            if(data != 0){

                $('#compare').html(data);
                showFrontendAlert('success', 'Item has been added to compare list');
                $('#compare_items_sidenav').html(parseInt($('#compare_items_sidenav').html())+1);
                }
                else{
                    showFrontendAlert('warning', 'You can only compare 3 products at the same time. Please remove any product if you wish to add this product.');
                }
            
           
        });
    }

    function addToWishList(id){
        @if (Auth::check() && (Auth::user()->user_type == 'customer' || Auth::user()->user_type == 'seller'))
            $.post('{{ route('wishlists.store') }}', {_token:'{{ csrf_token() }}', id:id}, function(data){
                if(data != 0){
                    $('#wishlist').html(data);
                    showFrontendAlert('success', 'Item has been added to wishlist');
                    setInterval(function(){ location.reload(); }, 3000);
                    
                }
                else{
                    showFrontendAlert('warning', 'Please login first');
                 
                }
            });
        @else
            showFrontendAlert('warning', 'Please login first');
        @endif
    }

    function showAddToCartModal(id){
        if(!$('#modal-size').hasClass('modal-lg')){
            $('#modal-size').addClass('modal-lg');
        }
        $('#addToCart-modal-body').html(null);
        $('#addToCart').modal();
        $('.c-preloader').show();
        $.post('{{ route('cart.showCartModal') }}', {_token:'{{ csrf_token() }}', id:id}, function(data){
            $('.c-preloader').hide();
            $('#addToCart-modal-body').html(data);
            $('.xzoom, .xzoom-gallery').xzoom({
                Xoffset: 20,
                bg: true,
                tint: '#000',
                defaultScale: -1
            });
            getVariantPrice();
        });
    }

    $('#option-choice-form input').on('change', function(){
        getVariantPrice();
    });

    function getVariantPrice(){
        if($('#option-choice-form input[name=quantity]').val() > 0 && checkAddToCartValidity()){
            $.ajax({
               type:"POST",
               url: '{{ route('products.variant_price') }}',
               data: $('#option-choice-form').serializeArray(),
               success: function(data){
                   $('#option-choice-form #chosen_price_div').removeClass('d-none');
                   $('#option-choice-form #chosen_price_div #chosen_price').html(data.price);
                   $('#available-quantity').html(data.quantity);
                   $('.input-number').prop('max', data.quantity);
                   //console.log(data.quantity);
                   if(parseInt(data.quantity) < 1 && data.digital  != 1){
                       $('.buy-now').hide();
                       $('.add-to-cart').hide();
                   }
                   else{
                       $('.buy-now').show();
                       $('.add-to-cart').show();
                   }
               }
           });
        }
    }

    function checkAddToCartValidity(){
        var names = {};
        $('#option-choice-form input:radio').each(function() { // find unique names
              names[$(this).attr('name')] = true;
        });
        var count = 0;
        $.each(names, function() { // then count them
              count++;
        });

        if($('#option-choice-form input:radio:checked').length == count){
            return true;
        }

        return false;
    }

    function addToCart(){
        if(checkAddToCartValidity()) {
            $('#addToCart').modal();
            $('.c-preloader').show();
            $.ajax({
               type:"POST",
               url: '{{ route('cart.addToCart') }}',
               data: $('#option-choice-form').serializeArray(),
               success: function(data){
                   $('#addToCart-modal-body').html(null);
                   $('.c-preloader').hide();
                   $('#modal-size').removeClass('modal-lg');
                   $('#addToCart-modal-body').html(data);
                   updateNavCart();
                   $('#cart_items_sidenav').html(parseInt($('#cart_items_sidenav').html())+1);
               }
           });
        }
        else{
            showFrontendAlert('warning', 'Please choose all the Mandetory(*) Attribute');
        }
    }

    function buyNow(){
        if(checkAddToCartValidity()) {
            $('#addToCart').modal();
            $('.c-preloader').show();
            $.ajax({
               type:"POST",
               url: '{{ route('cart.addToCart') }}',
               data: $('#option-choice-form').serializeArray(),
               success: function(data){
                   //$('#addToCart-modal-body').html(null);
                   //$('.c-preloader').hide();
                   //$('#modal-size').removeClass('modal-lg');
                   //$('#addToCart-modal-body').html(data);
                   updateNavCart();
                   $('#cart_items_sidenav').html(parseInt($('#cart_items_sidenav').html())+1);
                   window.location.replace("{{ route('cart') }}");
               }
           });
        }
        else{
            showFrontendAlert('warning', 'Please choose all the options');
        }
    }

    function show_purchase_history_details(order_id)
    {
        $('#order-details-modal-body').html(null);

        if(!$('#modal-size').hasClass('modal-lg')){
            $('#modal-size').addClass('modal-lg');
        }

        $.post('{{ route('purchase_history.details') }}', { _token : '{{ @csrf_token() }}', order_id : order_id}, function(data){
            $('#order-details-modal-body').html(data);
            $('#order_details').modal();
            $('.c-preloader').hide();
        });
    }

    function show_order_details(order_id)
    {
        $('#order-details-modal-body').html(null);

        if(!$('#modal-size').hasClass('modal-lg')){
            $('#modal-size').addClass('modal-lg');
        }

        $.post('{{ route('orders.details') }}', { _token : '{{ @csrf_token() }}', order_id : order_id}, function(data){
            $('#order-details-modal-body').html(data);
            $('#order_details').modal();
            $('.c-preloader').hide();
        });
    }

    function cartQuantityInitialize(){
        $('.btn-number').click(function(e) {
            e.preventDefault();

            fieldName = $(this).attr('data-field');
            type = $(this).attr('data-type');
            var input = $("input[name='" + fieldName + "']");
            var currentVal = parseInt(input.val());

            if (!isNaN(currentVal)) {
                if (type == 'minus') {

                    if (currentVal > input.attr('min')) {
                        input.val(currentVal - 1).change();
                    }
                    if (parseInt(input.val()) == input.attr('min')) {
                        $(this).attr('disabled', true);
                    }

                } else if (type == 'plus') {

                    if (currentVal < input.attr('max')) {
                        input.val(currentVal + 1).change();
                    }
                    if (parseInt(input.val()) == input.attr('max')) {
                        $(this).attr('disabled', true);
                    }

                }
            } else {
                input.val(0);
            }
        });

        $('.input-number').focusin(function() {
            $(this).data('oldValue', $(this).val());
        });

        $('.input-number').change(function() {

            minValue = parseInt($(this).attr('min'));
            maxValue = parseInt($(this).attr('max'));
            valueCurrent = parseInt($(this).val());

            name = $(this).attr('name');
            if (valueCurrent >= minValue) {
                $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
            } else {
                alert('Sorry, the minimum value was reached');
                $(this).val($(this).data('oldValue'));
            }
            if (valueCurrent <= maxValue) {
                $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
            } else {
                alert('Sorry, the maximum value was reached');
                $(this).val($(this).data('oldValue'));
            }


        });
        $(".input-number").keydown(function(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    }

     function imageInputInitialize(){
         $('.custom-input-file').each(function() {
             var $input = $(this),
                 $label = $input.next('label'),
                 labelVal = $label.html();

             $input.on('change', function(e) {
                 var fileName = '';

                 if (this.files && this.files.length > 1)
                     fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
                 else if (e.target.value)
                     fileName = e.target.value.split('\\').pop();

                 if (fileName)
                     $label.find('span').html(fileName);
                 else
                     $label.html(labelVal);
             });

             // Firefox bug fix
             $input
                 .on('focus', function() {
                     $input.addClass('has-focus');
                 })
                 .on('blur', function() {
                     $input.removeClass('has-focus');
                 });
         });
     }

</script>
       @yield('script')
  
        <!--====== Vendors js ======-->
        <!-- <script src="{{ asset('assets/js/vendor/jquery-3.5.1.min.js') }}"></script> -->
        <script src="{{ asset('assets/js/vendor/modernizr-3.7.1.min.js') }}"></script>
        <script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
        <script>
$('#iconified').on('keyup', function() {
    var input = $(this);
    if(input.val().length === 0) {
        input.addClass('empty');
    } else {
        input.removeClass('empty');
    }
});
        </script>
            <script>
      // With the above scripts loaded, you can call `tippy()` with a CSS
      // selector and a `content` prop:
      tippy('.favart_add', {
        content: 'Add To Wishlist',
        placement: 'bottom',
      });
      tippy('.compare_add', {
        content: 'Compare',
        placement: 'bottom',
      });
     
      tippy('.favart_add1', {
        content: 'View Wishlist',
        placement: 'bottom',
      });
      tippy('.cart_add', {
        content: 'View Cart',
        placement: 'bottom',
      });
    </script>
<script>
  $(document).ready(function(){
//     $(document).ajaxStart(function(){
//   $("#preloader").css("display", "block");
// });

// $(document).ajaxComplete(function(){
//   $("#preloader").css("display", "none");
// });
    
  });
  $(window).load(function(){
    $(".goog-logo-link").empty();
    $('.goog-te-gadget').html($('.goog-te-gadget').children());
})
</script>
        <!--====== Plugins js ======-->
        <!-- <script src="assets/js/plugins/bootstrap.min.js"></script>
        <script src="assets/js/plugins/popper.min.js"></script>
        <script src="assets/js/plugins/meanmenu.js"></script>
        <script src="assets/js/plugins/owl-carousel.js"></script>
        <script src="assets/js/plugins/jquery.nice-select.js"></script>
        <script src="assets/js/plugins/countdown.js"></script>
        <script src="assets/js/plugins/elevateZoom.js"></script>
        <script src="assets/js/plugins/jquery-ui.min.js"></script>
        <script src="assets/js/plugins/slick.js"></script>
        <script src="assets/js/plugins/scrollup.js"></script>
        <script src="assets/js/plugins/range-script.js"></script> -->

        <!--====== Use the minified version files listed below for better performance and remove the files listed above ======-->

        <script src="{{ asset('assets/js/plugins.min.js') }}"></script>

        <!-- Main Activation JS -->
        <script src="{{ asset('assets/js/main.js') }}"></script>
    </body>

<!-- Mirrored from live.hasthemes.com/html/5/ecolife-preview/ecolife/index-8.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Aug 2020 14:00:50 GMT -->
</html>
