<!DOCTYPE html>
@if(\App\Language::where('code', Session::get('locale', Config::get('app.locale')))->first()->rtl == 1)
<html dir="rtl" lang="en">
@else
<html lang="en">
@endif
<head>

@php
    $seosetting = \App\SeoSetting::first();
@endphp

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="index, follow">
<title>@yield('meta_title', config('app.name', 'Laravel'))</title>
<meta name="description" content="@yield('meta_description', $seosetting->description)" />
<meta name="keywords" content="@yield('meta_keywords', $seosetting->keyword)">
<meta name="author" content="{{ $seosetting->author }}">
<meta name="sitemap_link" content="{{ $seosetting->sitemap_link }}">

@yield('meta')

@if(!isset($detailedProduct))
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="{{ config('app.name', 'Laravel') }}">
    <meta itemprop="description" content="{{ $seosetting->description }}">
    <meta itemprop="image" content="{{ asset(\App\GeneralSetting::first()->logo) }}">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="product">
    <meta name="twitter:site" content="@publisher_handle">
    <meta name="twitter:title" content="{{ config('app.name', 'Laravel') }}">
    <meta name="twitter:description" content="{{ $seosetting->description }}">
    <meta name="twitter:creator" content="@author_handle">
    <meta name="twitter:image" content="{{ asset(\App\GeneralSetting::first()->logo) }}">

    <!-- Open Graph data -->
    <meta property="og:title" content="{{ config('app.name', 'Laravel') }}" />
    <meta property="og:type" content="Ecommerce Site" />
    <meta property="og:url" content="{{ route('home') }}" />
    <meta property="og:image" content="{{ asset(\App\GeneralSetting::first()->logo) }}" />
    <meta property="og:description" content="{{ $seosetting->description }}" />
    <meta property="og:site_name" content="{{ env('APP_NAME') }}" />
@endif

<!-- Favicon -->
<link type="image/x-icon" href="{{ asset(\App\GeneralSetting::first()->favicon) }}" rel="shortcut icon" />

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet" media="none" onload="if(media!='all')media='all'">

<!-- Bootstrap -->
<link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}" type="text/css" media="all">

<!-- Icons -->
<link rel="stylesheet" href="{{ asset('frontend/css/font-awesome.min.css') }}" type="text/css" media="none" onload="if(media!='all')media='all'">
<link rel="stylesheet" href="{{ asset('frontend/css/line-awesome.min.css') }}" type="text/css" media="none" onload="if(media!='all')media='all'">

<link type="text/css" href="{{ asset('frontend/css/bootstrap-tagsinput.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('frontend/css/jodit.min.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('frontend/css/sweetalert2.min.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('frontend/css/slick.css') }}" rel="stylesheet" media="all">
<link type="text/css" href="{{ asset('frontend/css/xzoom.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('frontend/css/jssocials.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('frontend/css/jssocials-theme-flat.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('frontend/css/intlTelInput.min.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('css/spectrum.css')}}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">

<!-- Global style (main) -->
<link type="text/css" href="{{ asset('frontend/css/active-shop.css') }}" rel="stylesheet" media="all">


<link type="text/css" href="{{ asset('frontend/css/main.css') }}" rel="stylesheet" media="all">

@if(\App\Language::where('code', Session::get('locale', Config::get('app.locale')))->first()->rtl == 1)
     <!-- RTL -->
    <link type="text/css" href="{{ asset('frontend/css/active.rtl.css') }}" rel="stylesheet" media="all">
@endif

<!-- color theme -->
<link href="{{ asset('frontend/css/colors/'.\App\GeneralSetting::first()->frontend_color.'.css')}}" rel="stylesheet" media="all">

<!-- Custom style -->
<link type="text/css" href="{{ asset('frontend/css/custom-style.css') }}" rel="stylesheet" media="all">
<link type="text/css" href="{{ asset('css/jquery-confirm.min.css') }}" rel="stylesheet" media="all">
<!-- jQuery -->
<script src="{{ asset('frontend/js/vendor/jquery.min.js') }}"></script>


@if (\App\BusinessSetting::where('type', 'google_analytics')->first()->value == 1)
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ env('TRACKING_ID') }}"></script>

    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', '{{ env('TRACKING_ID') }}');
    </script>
@endif

@if (\App\BusinessSetting::where('type', 'facebook_pixel')->first()->value == 1)
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', {{ env('FACEBOOK_PIXEL_ID') }});
  fbq('track', 'PageView');
</script>
<noscript>
  <img height="1" width="1" style="display:none"
       src="https://www.facebook.com/tr?id={{ env('FACEBOOK_PIXEL_ID') }}/&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
@endif
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/logo/logo-cosmatics.png" />
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800&amp;display=swap" rel="stylesheet" />

        <!-- All CSS Flies   -->
        <!--===== Vendor CSS (Bootstrap & Icon Font) =====-->
        <!-- <link rel="stylesheet" href="assets/css/plugins/bootstrap.min.css" />
        <link rel="stylesheet" href="assets/css/plugins/font-awesome.min.css" />
        <link rel="stylesheet" href="assets/css/plugins/ionicons.min.css" /> -->
        <!--===== Plugins CSS (All Plugins Files) =====-->
        <!-- <link rel="stylesheet" href="assets/css/plugins/jquery-ui.min.css" />
        <link rel="stylesheet" href="assets/css/plugins/meanmenu.css" />
        <link rel="stylesheet" href="assets/css/plugins/nice-select.css" />
        <link rel="stylesheet" href="assets/css/plugins/owl-carousel.css" />
        <link rel="stylesheet" href="assets/css/plugins/slick.css" /> -->
        <!--===== Main Css Files =====-->
        <!-- <link rel="stylesheet" href="assets/css/style.css" /> -->
        <!-- ===== Responsive Css Files ===== -->
        <!-- <link rel="stylesheet" href="assets/css/responsive.css" /> -->

        <!--====== Use the minified version files listed below for better performance and remove the files listed above ======-->

        <link rel="stylesheet" href="{{ asset('assets/css/vendor/plugins.min.css')}}">
        <link rel="stylesheet" href="{{ asset('assets/css/style.min.css')}}">
        <link rel="stylesheet" href="{{ asset('assets/css/responsive.min.css')}}">
		        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Development -->
<script src="https://unpkg.com/@popperjs/core@2/dist/umd/popper.min.js"></script>
<script src="https://unpkg.com/tippy.js@6/dist/tippy-bundle.umd.js"></script>

<!-- Production -->
<script src="https://unpkg.com/@popperjs/core@2"></script>
<script src="https://unpkg.com/tippy.js@6"></script>
<script src="{{ asset('js/jquery-confirm.min.js')}}"></script>
        <style>
            .button.one{
    border:1px solid #fc743a;
}

.button.one {
    background-repeat: no-repeat !important;
    background-position: -120px -120px, 0 0 !important;
    
    background-image: -webkit-linear-gradient(
      top left,
      rgba(255, 255, 255, 0.2) 0%,
      rgba(255, 255, 255, 0.2) 37%,
      rgba(255, 255, 255, 0.8) 45%,
      rgba(255, 255, 255, 0.0) 50%
    ) !important;
    background-image: -moz-linear-gradient(
      0 0,
      rgba(255, 255, 255, 0.2) 0%,
      rgba(255, 255, 255, 0.2) 37%,
      rgba(255, 255, 255, 0.8) 45%,
      rgba(255, 255, 255, 0.0) 50%
    ) !important;    
    background-image: -o-linear-gradient(
      0 0,
      rgba(255, 255, 255, 0.2) 0%,
      rgba(255, 255, 255, 0.2) 37%,
      rgba(255, 255, 255, 0.8) 45%,
      rgba(255, 255, 255, 0.0) 50%
    ) !important;
    background-image: linear-gradient(
      0 0,
      rgba(255, 255, 255, 0.2) 0%,
      rgba(255, 255, 255, 0.2) 37%,
      rgba(255, 255, 255, 0.8) 45%,
      rgba(255, 255, 255, 0.0) 50%
    ) !important;
    
    -moz-background-size: 250% 250%, 100% 100% !important;
         background-size: 250% 250%, 100% 100% !important;
    
    -webkit-transition: background-position 0s ease !important;
       -moz-transition: background-position 0s ease !important;       
         -o-transition: background-position 0s ease !important;
            transition: background-position 0s ease !important;
  }
  .hoverContent {
    display: none;
    opacity: 1;
    padding-top: 5px;
    -webkit-transition: all .2s ease-in-out;
    -moz-transition: all .2s ease-in-out;
    -ms-transition: all .2s ease-in-out;
    -o-transition: all .2s ease-in-out;
    top: 130%;
    position: absolute;
    right: 0;
    z-index: 9999;
    margin-top: -17px;
}
.sc_actLinks {
    width: 238px;
    background: #fff;
    -webkit-box-shadow: 0px 6px 14px 0px rgba(0,0,0,0.2);
    -moz-box-shadow: 0px 6px 14px 0px rgba(0,0,0,0.2);
    box-shadow: 0px 6px 14px 0px rgba(0,0,0,0.2);
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
    -moz-background-clip: padding;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
}
.sc_actLinks .actLink_heading {
    width: 100%;
    background: linear-gradient(45deg, #ff5959a8, #583ab3);
    min-height: 60px;
    color: #fff;
    position: relative;
    height: 60px;
    float: left;
    -webkit-border-radius: 4px 4px 0 0;
    -moz-border-radius: 4px 4px 0 0;
    border-radius: 4px 4px 0 0;
    -moz-background-clip: padding;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
}
.sc_actLinks .actLink_heading a {
    font-size: 18px;
    color: #fff;
    font-weight: 600;
    display: inline-block;
    width: 100%;
    padding-right: 20px;
    line-height: 39px;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
    margin: 7px 0px 0px 55px;
}
.sc_actLinks .actLink_heading.vip_on a.vip_nuser_btn {
    position: absolute;
    top: 34px;
    left: 71px;
    width: auto;
    padding: 3px 11px;
    line-height: normal;
    font-size: 12px;
    font-weight: normal;
    background-color: #222533;
}
.sc_actLinks ul {
    display: inline-block;
    width: 100%;
    padding: 0;
}
.sc_actLinks ul li {
    list-style-type: none;
    display: inline-block;
    width: 100%;
    position: relative;
    text-align: left;
    border-bottom: 1px solid #f7a05826;
}
.sc_actLinks ul li a {
    display: inline-block;
    text-indent: 0px;
    font-size: 15px;
    font-weight: 400;
    color: #222533;
    padding: 12px;
}
.sc_actLinks ul li a i{
    margin-right: 10px;
}
.sc_header .sc_actLinks ul li .usr_ordr {
    background-position: -249px -48px;
}

.log_reg:hover + .hoverContent {
    display: block !important;
}
.cart_val{
    background: #ff7200;
    color: white;
    padding: 0px 0px 4px 6px;
    border-radius: 100%;
    position: absolute;
    margin: 11px 0px 0px -20px;
    width: 22px;
    height: 23px;
}
.login_top > a:hover{
    color:#ff7200 !important;
}
.log_addon{
    border-radius: 19px 0px 0px 19px;
    border-right: none !important;
    background: white;
    box-shadow: 6px 6px 12px 0px #3236ff99;
    color: #3d49dc !important;
}
.log_input{
    border-radius: 0px 19px 19px 0px;
    box-shadow: 6px 6px 12px 0px #3236ff99;
    background-color: white !important;
    border-left: none !important;
}
.log-bg{
    background: url('{{asset("assets/images/Login_BG.png")}}');
}
.log-bg1{
    background: url('{{asset("assets/images/shapes.png")}}');
    padding-top: 10%;
}
.text-md1{
    color: white;
    font-size: 24px !important;
    font-weight: 900;
    line-height: 45px;
}
.text-md2{
    color: white;
    font-size: 20px !important;
}
.need1{
    margin-top: 8%;
}
.reg_but{
    margin-top: 3%;
}

.pad-0{
    box-shadow: 1px 1px 14px 5px #888290;
    padding-left: 0px !important;
    padding-right: 0px !important;
}
.btn_reg{
    padding: 4%;
    
    color: white;
    border-radius: 6px;
    box-shadow: 5px 6px 7px 2px #504848;
    background-color: #ff7200 !important;
    color: white;
    background-repeat: no-repeat !important;
    background-position: -120px -120px, 0 0 !important;
    
    background-image: -webkit-linear-gradient(
      top left,
      rgba(255, 255, 255, 0.2) 0%,
      rgba(255, 255, 255, 0.2) 37%,
      rgba(255, 255, 255, 0.8) 45%,
      rgba(255, 255, 255, 0.0) 50%
    ) !important;
    background-image: -moz-linear-gradient(
      0 0,
      rgba(255, 255, 255, 0.2) 0%,
      rgba(255, 255, 255, 0.2) 37%,
      rgba(255, 255, 255, 0.8) 45%,
      rgba(255, 255, 255, 0.0) 50%
    ) !important;    
    background-image: -o-linear-gradient(
      0 0,
      rgba(255, 255, 255, 0.2) 0%,
      rgba(255, 255, 255, 0.2) 37%,
      rgba(255, 255, 255, 0.8) 45%,
      rgba(255, 255, 255, 0.0) 50%
    ) !important;
    background-image: linear-gradient(
      0 0,
      rgba(255, 255, 255, 0.2) 0%,
      rgba(255, 255, 255, 0.2) 37%,
      rgba(255, 255, 255, 0.8) 45%,
      rgba(255, 255, 255, 0.0) 50%
    ) !important;
    
    -moz-background-size: 250% 250%, 100% 100% !important;
         background-size: 250% 250%, 100% 100% !important;
    
    -webkit-transition: background-position 0s ease !important;
       -moz-transition: background-position 0s ease !important;       
         -o-transition: background-position 0s ease !important;
            transition: background-position 0s ease !important;
}
.btn_reg:hover {
    padding: 4%;
    
    color: white;
    border-radius: 6px;
    box-shadow: 5px 6px 7px 2px #504848;
    background-position: 0 0, 0 0 !important;
    color: white !important;
    -webkit-transition-duration: 0.5s !important;
       -moz-transition-duration: 0.5s !important;
            transition-duration: 0.5s !important;
  }
  .reg-bg{
    background: url('{{asset("assets/images/Vendor_BG.png")}}') no-repeat !important;
  }
  .seller_reg{
    background: url('{{asset("assets/images/Regis_BG.png")}}') no-repeat !important;   
  }
  .breadcrumb-area {
    padding: 5px 0 !important;
    border-bottom: 1px solid #e9e9e9;
    background-color: #fafafa;
}
.breadcrumb-area {
    text-align: center;
    position: relative;
    padding: 70px 0;
    background: none no-repeat !important;
    background-size: cover;
}
.breadcrumb {
    
    margin-bottom: 0rem !important;
}
.aiz-rounded-check:after {
    background: #ff7200 !important;
}
.sc_actLinks .actLink_heading {
    width: 100%;
    /* background: linear-gradient(236deg, #f0456c, #faad3b); */
    min-height: 60px;
    color: #fff;
    position: relative;
    height: 60px;
    float: left;
    background: linear-gradient(450deg, #ff7200, #ff7200);
    -webkit-border-radius: 4px 4px 0 0;
    -moz-border-radius: 4px 4px 0 0;
    border-radius: 4px 4px 0 0;
    -moz-background-clip: padding;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
}
/* .middle{
     transition: .5s ease; 
    transition: .5s cubic-bezier(0, 0.81, 0.57, 1.45);
    opacity: 1;
    position: absolute;
    top: 50%;
    left: 50%;
    background: #9e6e1e70;
    width: 100%;
    height: 100%;
    transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    text-align: center;
} */
/* .sc_actLinks .actLink_heading:hover  .middle{
    opacity:1;
    width:0;
} */
.jconfirm-box{
    width: 30%;
}
.swal2-popup .swal2-select {
    display: none;
}
.swal-button {
  padding: 7px 19px;
  border-radius: 2px;
  background-color: #4962B3;
  font-size: 12px;
  border: 1px solid #3e549a;
  text-shadow: 0px -1px 0px rgba(0, 0, 0, 0.3);
}
.cart_icon_home{
    font-size: 40px;
    margin-left: 25px;
    color: #362b33;
}
.nav-box-number{
    position: absolute;
    top: 0;
    left: 53px;
    min-width: 20px;
    height: 20px;
    line-height: 20px;
    text-align: center;
    border-radius: 18px;
    color: #fff;
    font-size: 12px;
    background-color: #ff2a00;
}
.dropdown-menu {
    border: 1px solid #eceff1;
    border-radius: 1px;
    line-height: 1;
    min-width: 12rem;
    box-shadow: 0 5px 25px 0 rgba(123, 123, 123, 0.15);
}
.dropdown-cart {
    min-width: 360px;
    padding: 0 1rem;
}
.dropdown-cart .dc-header {
    padding: 5px;
    text-align: center;
}
.dropdown-cart .dc-header {
    padding: 1rem 1.5rem;
    border-bottom: 1px solid rgba(0, 0, 0, 0.05);
}
.dropdown-cart .dc-header .heading {
    margin: 0;
    color: rgba(0, 0, 0, 0.7);
}
.dropdown-menu.show {
    display: block;
    z-index: 9999;
}
        </style>
    </head>

    <body class="home-5 home-6 home-8 home-cosmatics">
        <!-- main layout start from here -->
        <!--====== PRELOADER PART START ======-->

        <!-- <div id="preloader">
        <div class="preloader">
            <span></span>
            <span></span>
        </div>
    </div> -->

        <!--====== PRELOADER PART ENDS ======-->
        <div id="main">
            <!-- Header Start -->
           

    @yield('content')
        </div>
       <!-- SCRIPTS -->
<!-- <a href="#" class="back-to-top btn-back-to-top"></a> -->

<!-- Core -->
<script src="{{ asset('frontend/js/vendor/popper.min.js') }}"></script>
<script src="{{ asset('frontend/js/vendor/bootstrap.min.js') }}"></script>

<!-- Plugins: Sorted A-Z -->
<script src="{{ asset('frontend/js/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('frontend/js/select2.min.js') }}"></script>
<script src="{{ asset('frontend/js/nouislider.min.js') }}"></script>
<script src="{{ asset('frontend/js/sweetalert2.min.js') }}"></script>
<script src="{{ asset('frontend/js/slick.min.js') }}"></script>
<script src="{{ asset('frontend/js/jssocials.min.js') }}"></script>
<script src="{{ asset('frontend/js/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('frontend/js/jodit.min.js') }}"></script>
<script src="{{ asset('frontend/js/xzoom.min.js') }}"></script>
<script src="{{ asset('frontend/js/fb-script.js') }}"></script>
<script src="{{ asset('frontend/js/lazysizes.min.js') }}"></script>
<script src="{{ asset('frontend/js/intlTelInput.min.js') }}"></script>

<!-- App JS -->
<script src="{{ asset('frontend/js/active-shop.js') }}"></script>
<script src="{{ asset('frontend/js/main.js') }}"></script>
<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

<script>

    $(".log_reg").hover(function(){
    $('.hoverContent').show();
},function(){
    $('.hoverContent').hide();
    $(".hoverContent").hover(function(){
    $('.hoverContent').show();
},function(){
    $('.hoverContent').hide();
});
  
});
    function showFrontendAlert(type, message){
        if(type == 'danger'){
            type = 'error';
        }

       
        swal({
            position: 'center',
            type: type,
            title: message,
            showCancelButton: false,
            confirmButtonColor: '#ff7200', // There won't be any cancel button
            showConfirmButton: true ,
            timer: 10000
        });
    }

function getReferLink(){

            var token = '{{csrf_token()}}';
   
           var form_data=new FormData();
       
           form_data.append('_token',token);
           $.ajax({
               type: "post",
               url: "{{ route('refer_show') }}",
               cache: false,
               processData: false,
               contentType: false,
               data: form_data,
               dataType:"json",
               success: function(response) {
                $.confirm({
                    boxWidth: '30%',
                        type: 'green',
                        icon:'fa fa-check',
                        title: 'Referral Code',
                        content: response.html,
                       
                        useBootstrap: false,
                        smoothContent:true,
                        buttons: {
                        ok: function () {
                            
                        },

                        }
                        });
                   },
                           error: function (jqXHR, textStatus, errorThrown) {
                             
                               var msg = "";
                               if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                                   msg += "<strong>Something Went Wrong,Please Reload and Try Again</strong>";
                               } else {
                                   if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                       msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                   } else {
                                       msg += "Error(s):<strong><ul>";
                                       $.each(jqXHR.responseJSON['errors'], function (key, value) {
                                           msg += "<li>" + value + "</li>";
                                       });
                                       msg += "</ul></strong>";
                                   }
                               }
                               isValid=0;
                               showFrontendAlert("error", msg);
                           }
           });
}
    function myFunction() {
  /* Get the text field */
  var copyText = document.getElementById("myInput");

  /* Select the text field */
  copyText.select();
  copyText.setSelectionRange(0, 99999); /*For mobile devices*/

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  //alert("Copied the text: " + copyText.value);
}
</script>

@foreach (session('flash_notification', collect())->toArray() as $message)
    <script>
        showFrontendAlert('{{ $message['level'] }}', '{{ $message['message'] }}');
    </script>
@endforeach
<script>

    $(document).ready(function() {
        $('.successErrorMessage').delay(8000).slideUp(300);
        $('.category-nav-element').each(function(i, el) {
            $(el).on('mouseover', function(){
                if(!$(el).find('.sub-cat-menu').hasClass('loaded')){
                    $.post('{{ route('category.elements') }}', {_token: '{{ csrf_token()}}', id:$(el).data('id')}, function(data){
                        $(el).find('.sub-cat-menu').addClass('loaded').html(data);
                    });
                }
            });
        });
        if ($('#lang-change').length > 0) {
            $('#lang-change .dropdown-item a').each(function() {
                $(this).on('click', function(e){
                    e.preventDefault();
                    var $this = $(this);
                    var locale = $this.data('flag');
                    $.post('{{ route('language.change') }}',{_token:'{{ csrf_token() }}', locale:locale}, function(data){
                        location.reload();
                    });

                });
            });
        }

        if ($('#currency-change').length > 0) {
            $('#currency-change .dropdown-item a').each(function() {
                $(this).on('click', function(e){
                    e.preventDefault();
                    var $this = $(this);
                    var currency_code = $this.data('currency');
                    $.post('{{ route('currency.change') }}',{_token:'{{ csrf_token() }}', currency_code:currency_code}, function(data){
                        location.reload();
                    });

                });
            });
        }
    });

    $('#search').on('keyup', function(){
        search();
    });

    $('#search').on('focus', function(){
        search();
    });

    function search(){
        var search = $('#search').val();
        if(search.length > 0){
            $('body').addClass("typed-search-box-shown");

            $('.typed-search-box').removeClass('d-none');
            $('.search-preloader').removeClass('d-none');
            $.post('{{ route('search.ajax') }}', { _token: '{{ @csrf_token() }}', search:search}, function(data){
                if(data == '0'){
                    // $('.typed-search-box').addClass('d-none');
                    $('#search-content').html(null);
                    $('.typed-search-box .search-nothing').removeClass('d-none').html('Sorry, nothing found for <strong>"'+search+'"</strong>');
                    $('.search-preloader').addClass('d-none');

                }
                else{
                    $('.typed-search-box .search-nothing').addClass('d-none').html(null);
                    $('#search-content').html(data);
                    $('.search-preloader').addClass('d-none');
                }
            });
        }
        else {
            $('.typed-search-box').addClass('d-none');
            $('body').removeClass("typed-search-box-shown");
        }
    }

    function updateNavCart(){
        $.post('{{ route('cart.nav_cart') }}', {_token:'{{ csrf_token() }}'}, function(data){
            $('#cart_items').html(data);
        });
    }

    function removeFromCart(key){
        $.post('{{ route('cart.removeFromCart') }}', {_token:'{{ csrf_token() }}', key:key}, function(data){
            updateNavCart();
            $('#cart-summary').html(data);
            showFrontendAlert('success', 'Item has been removed from cart');
            $('#cart_items_sidenav').html(parseInt($('#cart_items_sidenav').html())-1);
        });
    }
    function removeFromCompare(key){
        $.post('{{ route('compare.removeFromCompare') }}', {_token:'{{ csrf_token() }}', id:key}, function(data){
            
           showFrontendAlert('success', 'Item has been removed from Compare List');
            location.reload().setTimeOut(500);
           
        });
    }

    function addToCompare(id){
        $.post('{{ route('compare.addToCompare') }}', {_token:'{{ csrf_token() }}', id:id}, function(data){
            
            if(data != 0){

                $('#compare').html(data);
                showFrontendAlert('success', 'Item has been added to compare list');
                $('#compare_items_sidenav').html(parseInt($('#compare_items_sidenav').html())+1);
                }
                else{
                    showFrontendAlert('warning', 'You can only compare 3 products at the same time. Please remove any product if you wish to add this product.');
                }
            
           
        });
    }

    function addToWishList(id){
        @if (Auth::check() && (Auth::user()->user_type == 'customer' || Auth::user()->user_type == 'seller'))
            $.post('{{ route('wishlists.store') }}', {_token:'{{ csrf_token() }}', id:id}, function(data){
                if(data != 0){
                    $('#wishlist').html(data);
                    showFrontendAlert('success', 'Item has been added to wishlist');
                }
                else{
                    showFrontendAlert('warning', 'Please login first');
                }
            });
        @else
            showFrontendAlert('warning', 'Please login first');
        @endif
    }

    function showAddToCartModal(id){
        if(!$('#modal-size').hasClass('modal-lg')){
            $('#modal-size').addClass('modal-lg');
        }
        $('#addToCart-modal-body').html(null);
        $('#addToCart').modal();
        $('.c-preloader').show();
        $.post('{{ route('cart.showCartModal') }}', {_token:'{{ csrf_token() }}', id:id}, function(data){
            $('.c-preloader').hide();
            $('#addToCart-modal-body').html(data);
            $('.xzoom, .xzoom-gallery').xzoom({
                Xoffset: 20,
                bg: true,
                tint: '#000',
                defaultScale: -1
            });
            getVariantPrice();
        });
    }

    $('#option-choice-form input').on('change', function(){
        getVariantPrice();
    });

    function getVariantPrice(){
        if($('#option-choice-form input[name=quantity]').val() > 0 && checkAddToCartValidity()){
            $.ajax({
               type:"POST",
               url: '{{ route('products.variant_price') }}',
               data: $('#option-choice-form').serializeArray(),
               success: function(data){
                   $('#option-choice-form #chosen_price_div').removeClass('d-none');
                   $('#option-choice-form #chosen_price_div #chosen_price').html(data.price);
                   $('#available-quantity').html(data.quantity);
                   $('.input-number').prop('max', data.quantity);
                   //console.log(data.quantity);
                   if(parseInt(data.quantity) < 1 && data.digital  != 1){
                       $('.buy-now').hide();
                       $('.add-to-cart').hide();
                   }
                   else{
                       $('.buy-now').show();
                       $('.add-to-cart').show();
                   }
               }
           });
        }
    }

    function checkAddToCartValidity(){
        var names = {};
        $('#option-choice-form input:radio').each(function() { // find unique names
              names[$(this).attr('name')] = true;
        });
        var count = 0;
        $.each(names, function() { // then count them
              count++;
        });

        if($('#option-choice-form input:radio:checked').length == count){
            return true;
        }

        return false;
    }

    function addToCart(){
        if(checkAddToCartValidity()) {
            $('#addToCart').modal();
            $('.c-preloader').show();
            $.ajax({
               type:"POST",
               url: '{{ route('cart.addToCart') }}',
               data: $('#option-choice-form').serializeArray(),
               success: function(data){
                   $('#addToCart-modal-body').html(null);
                   $('.c-preloader').hide();
                   $('#modal-size').removeClass('modal-lg');
                   $('#addToCart-modal-body').html(data);
                   updateNavCart();
                   $('#cart_items_sidenav').html(parseInt($('#cart_items_sidenav').html())+1);
               }
           });
        }
        else{
            showFrontendAlert('warning', 'Please choose all the options');
        }
    }

    function buyNow(){
        if(checkAddToCartValidity()) {
            $('#addToCart').modal();
            $('.c-preloader').show();
            $.ajax({
               type:"POST",
               url: '{{ route('cart.addToCart') }}',
               data: $('#option-choice-form').serializeArray(),
               success: function(data){
                   //$('#addToCart-modal-body').html(null);
                   //$('.c-preloader').hide();
                   //$('#modal-size').removeClass('modal-lg');
                   //$('#addToCart-modal-body').html(data);
                   updateNavCart();
                   $('#cart_items_sidenav').html(parseInt($('#cart_items_sidenav').html())+1);
                   window.location.replace("{{ route('cart') }}");
               }
           });
        }
        else{
            showFrontendAlert('warning', 'Please choose all the options');
        }
    }

    function show_purchase_history_details(order_id)
    {
        $('#order-details-modal-body').html(null);

        if(!$('#modal-size').hasClass('modal-lg')){
            $('#modal-size').addClass('modal-lg');
        }

        $.post('{{ route('purchase_history.details') }}', { _token : '{{ @csrf_token() }}', order_id : order_id}, function(data){
            $('#order-details-modal-body').html(data);
            $('#order_details').modal();
            $('.c-preloader').hide();
        });
    }

    function show_order_details(order_id)
    {
        $('#order-details-modal-body').html(null);

        if(!$('#modal-size').hasClass('modal-lg')){
            $('#modal-size').addClass('modal-lg');
        }

        $.post('{{ route('orders.details') }}', { _token : '{{ @csrf_token() }}', order_id : order_id}, function(data){
            $('#order-details-modal-body').html(data);
            $('#order_details').modal();
            $('.c-preloader').hide();
        });
    }

    function cartQuantityInitialize(){
        $('.btn-number').click(function(e) {
            e.preventDefault();

            fieldName = $(this).attr('data-field');
            type = $(this).attr('data-type');
            var input = $("input[name='" + fieldName + "']");
            var currentVal = parseInt(input.val());

            if (!isNaN(currentVal)) {
                if (type == 'minus') {

                    if (currentVal > input.attr('min')) {
                        input.val(currentVal - 1).change();
                    }
                    if (parseInt(input.val()) == input.attr('min')) {
                        $(this).attr('disabled', true);
                    }

                } else if (type == 'plus') {

                    if (currentVal < input.attr('max')) {
                        input.val(currentVal + 1).change();
                    }
                    if (parseInt(input.val()) == input.attr('max')) {
                        $(this).attr('disabled', true);
                    }

                }
            } else {
                input.val(0);
            }
        });

        $('.input-number').focusin(function() {
            $(this).data('oldValue', $(this).val());
        });

        $('.input-number').change(function() {

            minValue = parseInt($(this).attr('min'));
            maxValue = parseInt($(this).attr('max'));
            valueCurrent = parseInt($(this).val());

            name = $(this).attr('name');
            if (valueCurrent >= minValue) {
                $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
            } else {
                alert('Sorry, the minimum value was reached');
                $(this).val($(this).data('oldValue'));
            }
            if (valueCurrent <= maxValue) {
                $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
            } else {
                alert('Sorry, the maximum value was reached');
                $(this).val($(this).data('oldValue'));
            }


        });
        $(".input-number").keydown(function(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    }

     function imageInputInitialize(){
         $('.custom-input-file').each(function() {
             var $input = $(this),
                 $label = $input.next('label'),
                 labelVal = $label.html();

             $input.on('change', function(e) {
                 var fileName = '';

                 if (this.files && this.files.length > 1)
                     fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
                 else if (e.target.value)
                     fileName = e.target.value.split('\\').pop();

                 if (fileName)
                     $label.find('span').html(fileName);
                 else
                     $label.html(labelVal);
             });

             // Firefox bug fix
             $input
                 .on('focus', function() {
                     $input.addClass('has-focus');
                 })
                 .on('blur', function() {
                     $input.removeClass('has-focus');
                 });
         });
     }

</script>
       @yield('script')
  
        <!--====== Vendors js ======-->
        <!-- <script src="{{ asset('assets/js/vendor/jquery-3.5.1.min.js') }}"></script> -->
        <script src="{{ asset('assets/js/vendor/modernizr-3.7.1.min.js') }}"></script>
        <script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
        <script>
$('#iconified').on('keyup', function() {
    var input = $(this);
    if(input.val().length === 0) {
        input.addClass('empty');
    } else {
        input.removeClass('empty');
    }
});
        </script>
            <script>
      // With the above scripts loaded, you can call `tippy()` with a CSS
      // selector and a `content` prop:
      tippy('.favart_add', {
        content: 'Add To Wishlist',
        placement: 'bottom',
      });
      tippy('.compare_add', {
        content: 'Compare',
        placement: 'bottom',
      });
    </script>

        <!--====== Plugins js ======-->
        <!-- <script src="assets/js/plugins/bootstrap.min.js"></script>
        <script src="assets/js/plugins/popper.min.js"></script>
        <script src="assets/js/plugins/meanmenu.js"></script>
        <script src="assets/js/plugins/owl-carousel.js"></script>
        <script src="assets/js/plugins/jquery.nice-select.js"></script>
        <script src="assets/js/plugins/countdown.js"></script>
        <script src="assets/js/plugins/elevateZoom.js"></script>
        <script src="assets/js/plugins/jquery-ui.min.js"></script>
        <script src="assets/js/plugins/slick.js"></script>
        <script src="assets/js/plugins/scrollup.js"></script>
        <script src="assets/js/plugins/range-script.js"></script> -->

        <!--====== Use the minified version files listed below for better performance and remove the files listed above ======-->

        <script src="{{ asset('assets/js/plugins.min.js') }}"></script>

        <!-- Main Activation JS -->
        <script src="{{ asset('assets/js/main.js') }}"></script>
    </body>

<!-- Mirrored from live.hasthemes.com/html/5/ecolife-preview/ecolife/index-8.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Aug 2020 14:00:50 GMT -->
</html>
