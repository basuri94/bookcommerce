<!DOCTYPE html>
@if(\App\Language::where('code', Session::get('locale', Config::get('app.locale')))->first()->rtl == 1)
<html dir="rtl" lang="en">
@else
<html lang="en">
@endif
<head>

@php
    $seosetting = \App\SeoSetting::first();
@endphp

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="index, follow">
<title>@yield('meta_title', config('app.name', 'Laravel'))</title>
<meta name="description" content="@yield('meta_description', $seosetting->description)" />
<meta name="keywords" content="@yield('meta_keywords', $seosetting->keyword)">
<meta name="author" content="{{ $seosetting->author }}">
<meta name="sitemap_link" content="{{ $seosetting->sitemap_link }}">

@yield('meta')

@if(!isset($detailedProduct))
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="{{ config('app.name', 'Laravel') }}">
    <meta itemprop="description" content="{{ $seosetting->description }}">
    <meta itemprop="image" content="{{ asset(\App\GeneralSetting::first()->logo) }}">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="product">
    <meta name="twitter:site" content="@publisher_handle">
    <meta name="twitter:title" content="{{ config('app.name', 'Laravel') }}">
    <meta name="twitter:description" content="{{ $seosetting->description }}">
    <meta name="twitter:creator" content="@author_handle">
    <meta name="twitter:image" content="{{ asset(\App\GeneralSetting::first()->logo) }}">

    <!-- Open Graph data -->
    <meta property="og:title" content="{{ config('app.name', 'Laravel') }}" />
    <meta property="og:type" content="Ecommerce Site" />
    <meta property="og:url" content="{{ route('home') }}" />
    <meta property="og:image" content="{{ asset(\App\GeneralSetting::first()->logo) }}" />
    <meta property="og:description" content="{{ $seosetting->description }}" />
    <meta property="og:site_name" content="{{ env('APP_NAME') }}" />
@endif

<!-- Favicon -->
<link type="image/x-icon" href="{{ asset(\App\GeneralSetting::first()->favicon) }}" rel="shortcut icon" />

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet" media="none" onload="if(media!='all')media='all'">

<!-- Bootstrap -->
<link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}" type="text/css" media="all">

<!-- Icons -->
<link rel="stylesheet" href="{{ asset('frontend/css/font-awesome.min.css') }}" type="text/css" media="none" onload="if(media!='all')media='all'">
<link rel="stylesheet" href="{{ asset('frontend/css/line-awesome.min.css') }}" type="text/css" media="none" onload="if(media!='all')media='all'">

<link type="text/css" href="{{ asset('frontend/css/bootstrap-tagsinput.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('frontend/css/jodit.min.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('frontend/css/sweetalert2.min.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('frontend/css/slick.css') }}" rel="stylesheet" media="all">
<link type="text/css" href="{{ asset('frontend/css/xzoom.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('frontend/css/jssocials.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('frontend/css/jssocials-theme-flat.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('frontend/css/intlTelInput.min.css') }}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">
<link type="text/css" href="{{ asset('css/spectrum.css')}}" rel="stylesheet" media="none" onload="if(media!='all')media='all'">

<!-- Global style (main) -->
<link type="text/css" href="{{ asset('frontend/css/active-shop.css') }}" rel="stylesheet" media="all">


<link type="text/css" href="{{ asset('frontend/css/main.css') }}" rel="stylesheet" media="all">

@if(\App\Language::where('code', Session::get('locale', Config::get('app.locale')))->first()->rtl == 1)
     <!-- RTL -->
    <link type="text/css" href="{{ asset('frontend/css/active.rtl.css') }}" rel="stylesheet" media="all">
@endif

<!-- color theme -->
<link href="{{ asset('frontend/css/colors/'.\App\GeneralSetting::first()->frontend_color.'.css')}}" rel="stylesheet" media="all">

<!-- Custom style -->
<link type="text/css" href="{{ asset('frontend/css/custom-style.css') }}" rel="stylesheet" media="all">

<!-- jQuery -->
<script src="{{ asset('frontend/js/vendor/jquery.min.js') }}"></script>


@if (\App\BusinessSetting::where('type', 'google_analytics')->first()->value == 1)
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ env('TRACKING_ID') }}"></script>

    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', '{{ env('TRACKING_ID') }}');
    </script>
@endif

@if (\App\BusinessSetting::where('type', 'facebook_pixel')->first()->value == 1)
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', {{ env('FACEBOOK_PIXEL_ID') }});
  fbq('track', 'PageView');
</script>
<noscript>
  <img height="1" width="1" style="display:none"
       src="https://www.facebook.com/tr?id={{ env('FACEBOOK_PIXEL_ID') }}/&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
@endif
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/logo/logo-cosmatics.png" />
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800&amp;display=swap" rel="stylesheet" />

        <!-- All CSS Flies   -->
        <!--===== Vendor CSS (Bootstrap & Icon Font) =====-->
        <!-- <link rel="stylesheet" href="assets/css/plugins/bootstrap.min.css" />
        <link rel="stylesheet" href="assets/css/plugins/font-awesome.min.css" />
        <link rel="stylesheet" href="assets/css/plugins/ionicons.min.css" /> -->
        <!--===== Plugins CSS (All Plugins Files) =====-->
        <!-- <link rel="stylesheet" href="assets/css/plugins/jquery-ui.min.css" />
        <link rel="stylesheet" href="assets/css/plugins/meanmenu.css" />
        <link rel="stylesheet" href="assets/css/plugins/nice-select.css" />
        <link rel="stylesheet" href="assets/css/plugins/owl-carousel.css" />
        <link rel="stylesheet" href="assets/css/plugins/slick.css" /> -->
        <!--===== Main Css Files =====-->
        <!-- <link rel="stylesheet" href="assets/css/style.css" /> -->
        <!-- ===== Responsive Css Files ===== -->
        <!-- <link rel="stylesheet" href="assets/css/responsive.css" /> -->

        <!--====== Use the minified version files listed below for better performance and remove the files listed above ======-->

        <link rel="stylesheet" href="{{ asset('assets/css/vendor/plugins.min.css')}}">
        <link rel="stylesheet" href="{{ asset('assets/css/style.min.css')}}">
        <link rel="stylesheet" href="{{ asset('assets/css/responsive.min.css')}}">
		        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Development -->
<script src="https://unpkg.com/@popperjs/core@2/dist/umd/popper.min.js"></script>
<script src="https://unpkg.com/tippy.js@6/dist/tippy-bundle.umd.js"></script>

<!-- Production -->
<script src="https://unpkg.com/@popperjs/core@2"></script>
<script src="https://unpkg.com/tippy.js@6"></script>
        <style>
            .button.one{
    border:1px solid #fc743a;
}

.button.one {
    background-repeat: no-repeat !important;
    background-position: -120px -120px, 0 0 !important;
    
    background-image: -webkit-linear-gradient(
      top left,
      rgba(255, 255, 255, 0.2) 0%,
      rgba(255, 255, 255, 0.2) 37%,
      rgba(255, 255, 255, 0.8) 45%,
      rgba(255, 255, 255, 0.0) 50%
    ) !important;
    background-image: -moz-linear-gradient(
      0 0,
      rgba(255, 255, 255, 0.2) 0%,
      rgba(255, 255, 255, 0.2) 37%,
      rgba(255, 255, 255, 0.8) 45%,
      rgba(255, 255, 255, 0.0) 50%
    ) !important;    
    background-image: -o-linear-gradient(
      0 0,
      rgba(255, 255, 255, 0.2) 0%,
      rgba(255, 255, 255, 0.2) 37%,
      rgba(255, 255, 255, 0.8) 45%,
      rgba(255, 255, 255, 0.0) 50%
    ) !important;
    background-image: linear-gradient(
      0 0,
      rgba(255, 255, 255, 0.2) 0%,
      rgba(255, 255, 255, 0.2) 37%,
      rgba(255, 255, 255, 0.8) 45%,
      rgba(255, 255, 255, 0.0) 50%
    ) !important;
    
    -moz-background-size: 250% 250%, 100% 100% !important;
         background-size: 250% 250%, 100% 100% !important;
    
    -webkit-transition: background-position 0s ease !important;
       -moz-transition: background-position 0s ease !important;       
         -o-transition: background-position 0s ease !important;
            transition: background-position 0s ease !important;
  }
  .hoverContent {
    display: none;
    opacity: 1;
    padding-top: 5px;
    -webkit-transition: all .2s ease-in-out;
    -moz-transition: all .2s ease-in-out;
    -ms-transition: all .2s ease-in-out;
    -o-transition: all .2s ease-in-out;
    top: 130%;
    position: absolute;
    right: 0;
    z-index: 9;
    margin-top: -17px;
}
.sc_actLinks {
    width: 238px;
    background: #fff;
    -webkit-box-shadow: 0px 6px 14px 0px rgba(0,0,0,0.2);
    -moz-box-shadow: 0px 6px 14px 0px rgba(0,0,0,0.2);
    box-shadow: 0px 6px 14px 0px rgba(0,0,0,0.2);
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
    -moz-background-clip: padding;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
}
.sc_actLinks .actLink_heading {
    width: 100%;
    background: linear-gradient(45deg, #ff5959a8, #583ab3);
    min-height: 60px;
    color: #fff;
    position: relative;
    height: 60px;
    float: left;
    -webkit-border-radius: 4px 4px 0 0;
    -moz-border-radius: 4px 4px 0 0;
    border-radius: 4px 4px 0 0;
    -moz-background-clip: padding;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
}
.sc_actLinks .actLink_heading a {
    font-size: 18px;
    color: #fff;
    font-weight: 600;
    display: inline-block;
    width: 100%;
    padding-right: 20px;
    line-height: 39px;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
    margin: 7px 0px 0px 55px;
}
.sc_actLinks .actLink_heading.vip_on a.vip_nuser_btn {
    position: absolute;
    top: 34px;
    left: 71px;
    width: auto;
    padding: 3px 11px;
    line-height: normal;
    font-size: 12px;
    font-weight: normal;
    background-color: #222533;
}
.sc_actLinks ul {
    display: inline-block;
    width: 100%;
    padding: 0;
}
.sc_actLinks ul li {
    list-style-type: none;
    display: inline-block;
    width: 100%;
    position: relative;
    text-align: left;
    border-bottom: 1px solid #f7a05826;
}
.sc_actLinks ul li a {
    display: inline-block;
    text-indent: 0px;
    font-size: 15px;
    font-weight: 400;
    color: #222533;
    padding: 12px;
}
.sc_actLinks ul li a i{
    margin-right: 10px;
}
.sc_header .sc_actLinks ul li .usr_ordr {
    background-position: -249px -48px;
}

.log_reg:hover + .hoverContent {
    display: block !important;
}
.cart_val{
    background: #ff7200;
    color: white;
    padding: 0px 0px 4px 6px;
    border-radius: 100%;
    position: absolute;
    margin: 11px 0px 0px -20px;
    width: 22px;
    height: 23px;
}
.login_top > a:hover{
    color:#ff7200 !important;
}
.log_addon{
    border-radius: 19px 0px 0px 19px;
    box-shadow: 1px 1px 4px 0px #2d2929;
}
.log_input{
    height: 38px;
    border-radius: 0px 19px 19px 0px;
    box-shadow: 1px 1px 12px 0px #3236ff99;
    background-color: white !important;
}
        </style>
    </head>

    <body class="home-5 home-6 home-8 home-cosmatics">
        <!-- main layout start from here -->
        <!--====== PRELOADER PART START ======-->

        <!-- <div id="preloader">
        <div class="preloader">
            <span></span>
            <span></span>
        </div>
    </div> -->

        <!--====== PRELOADER PART ENDS ======-->
        <div id="main">
            <!-- Header Start -->
            <header class="main-header">
                <!-- Header Top Start -->
                <div class="header-top-nav">
                    <div class="container">
                        <div class="row">
                            <!--Left Start-->
                            <div class="col-lg-4 col-md-4">
                                <div class="left-text">
                                    <p>Made In India <img src="{{asset('assets/images/icons/2.jpg')}}"></p>
                                </div>
                            </div>
                            <!--Left End-->
                            <!--Right Start-->
                            <div class="col-lg-8 col-md-8 text-right">
                                <div class="header-right-nav">
                                    <ul class="res-xs-flex">
                                        <li class="after-n" style="color:white;cursor: default;">
                                            Sell with local2vocal &nbsp;&nbsp;<i class="fa fa-chevron-right"></i>
                                        </li>
                                        <li class="login_top">
                                            <a href="/users/login"><i class="fa fa-user"></i>Login / Register</a>
                                        </li>
                                        
                                    </ul>
                               
                                    <div class="dropdown-navs">
                                        <ul>
                                            
                                            <!-- Currency End -->
                                            <!-- Language Start -->
                                            <li class="top-10px mr-15px">
                                                <select>
                                                    <option value="1">English</option>
                                                    <option value="2">Hindi</option>
                                                </select>
                                            </li>
                                            <!-- Language End -->
                                        </ul>
                                    </div>
                                </div>
                                

                            </div>
                            <!--Right End-->
                        </div>
                    </div>
                </div>
                <!-- Header Top End -->
                <!-- Header Buttom Start -->
                <div class="header-navigation d-lg-block d-none">
                    <div class="container">
                        <div class="row">
                            <!-- Logo Start -->
                            <div class="col-md-2 col-sm-2">
                                <div class="logo">
                                    <a href="{{ route('home') }}"><img src="{{ asset('assets/images/logo/logo-cosmatics.png') }}" alt="" style="width:100%;"/></a>
                                </div>
                            </div>
                            <!-- Logo End -->
                            <div class="col-md-10 col-sm-10">
                                <!--Header Bottom Account Start -->
                                <div class="header_account_area">
                                    <!--Seach Area Start -->
                                    <div class="header_account_list search_list">
                                        <a href="javascript:void(0)"><i class="ion-ios-search-strong"></i></a>
                                        <div class="dropdown_search">
                                            <form action="{{ route('search') }}" method="GET">
                                            <i class="fa fa-search icon"></i> 
                                                <input style="font-family: FontAwesome,sans-serif;" aria-label="Search" id="search" name="q"  autocomplete="off" placeholder="Search Now..." type="text"  class="search_front"/>

                                                <button type="submit" class="button one">SEARCH</button>
                                                <div class="typed-search-box d-none">
                                            <div class="search-preloader">
                                                <div class="loader"><div></div><div></div><div></div></div>
                                            </div>
                                            <div class="search-nothing d-none">

                                            </div>
                                            <div id="search-content">

                                            </div>
                                        </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!--Seach Area End -->
                                    <!--Contact info Start -->
                                    <div class="contact-link-wrap">
                               
                                        <i class="fa fa-user-o log_reg" style="font-size: 33px;color: #222533;"></i>
                                        <a href="{{ route('wishlists.index') }}" style="color: #222533;"><i class="fa fa-heart-o" style="font-size: 33px;width: 10%;"></i></a>
                                        <div class="hoverContent">
                          <div class="sc_actLinks">
                            <div class="actLink_heading vip_on" id="vip_on">
                                                              <a href="/users/login" id="show_loginpop" >Login/Register</a>
                               
                               
                            </div>
                            <ul> <li><a href="/dashboard" ><i class="fa fa-dashboard"></i>My Dashboard</a></li>
                                                              <li><a href="javascript:void(0);" ><i class="fa fa-list"></i>My Orders</a></li>
                               

                                                              <li><a href="javascript:void(0);" ><i class="fa fa-envelope"></i>My Returns</a></li>
                                
                                  

                                                              <li><a href="javascript:void(0);" "><i class="fa fa-heart"></i>Wishlist</a></li>
                               

                                                              <li><a href="javascript:void(0);"><i class="fa fa-user"></i>My Profile</a></li>
                                   </ul>
                          </div>
                        </div>
                                        <!--Contact info End -->
                                        <!--Cart info Start -->
                                        <div class="cart-info d-flex">
                                            <div class="mini-cart-warp">
                                            @if(Session::has('cart'))
                                                        @if(count($cart = Session::get('cart')) > 0)
                                                <a href="#" class="count-cart"><span class="cart_val nav-box-number">{{ count(Session::get('cart')) }}</span></a>
                                                @else
                                                <a href="#" class="count-cart"><span class="cart_val nav-box-number">0</span></a>
                                                @endif
                                                @else
                                                <a href="#" class="count-cart"><span class="cart_val nav-box-number">0</span></a>
                                                @endif
                                                <div class="mini-cart-content">
                                                @if(Session::has('cart'))
                                                        @if(count($cart = Session::get('cart')) > 0)
                                                    <ul>
                                                    @php
                                                                    $total = 0;
                                                                @endphp
                                                                @foreach($cart as $key => $cartItem)
                                                                    @php
                                                                        $product = \App\Product::find($cartItem['id']);
                                                                        $total = $total + $cartItem['price']*$cartItem['quantity'];
                                                                    @endphp
                                                        <li class="single-shopping-cart">
                                                            <div class="shopping-cart-img">
                                                                <a href="{{ route('product', $product->slug) }}"><img alt="" src="{{ asset($product->thumbnail_img) }}" /></a>
                                                                <span class="product-quantity">x{{ $cartItem['quantity'] }}</span>
                                                            </div>
                                                            <div class="shopping-cart-title">
                                                                <h4><a href="{{ route('product', $product->slug) }}"> {{ __($product->name) }}</a></h4>
                                                                <span>{{ single_price($cartItem['price']*$cartItem['quantity']) }}</span>
                                                                <div class="shopping-cart-delete">
                                                                    <a href="javascript:" onclick="removeFromCart({{ $key }})"><i class="ion-android-cancel"></i></a>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        @endforeach
                                                        
                                                    </ul>
                                                    <div class="shopping-cart-total">
                                                        <h4>{{translate('Subtotal')}} : <span>{{ single_price($total) }}</span></h4>
                                                        
                                                    </div>
                                                    
                                                    <div class="shopping-cart-btn text-center">
                                                        <a class="default-btn" href="{{ route('cart') }}">checkout</a>
                                                    </div>
                                                    @else
                                                            <div class="dc-header">
                                                                <h3 class="heading heading-6 strong-700">{{translate('Your Cart is empty')}}</h3>
                                                            </div>
                                                        @endif
                                                        @else
                                                        <div class="dc-header">
                                                            <h3 class="heading heading-6 strong-700">{{translate('Your Cart is empty')}}</h3>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <!--Cart info End -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Header Bottom Account End -->
                
                @include('frontend.inc.nav')
                @include('frontend.inc.head_mob_nav')
				
				@include('frontend.inc.category_menu')
				
                
            </header>
            <!-- Header End -->
            
    

            <section class="gry-bg py-4 profile">
                <div class="container">
                    <div class="row cols-xs-space cols-sm-space cols-md-space">
                        <div class="col-lg-3 d-none d-lg-block">
                            @if(Auth::user()->user_type == 'seller')
                                @include('frontend.inc.seller_side_nav')
                            @elseif(Auth::user()->user_type == 'customer')
                                @include('frontend.inc.customer_side_nav')
                            @endif
                        </div>
        
                        <div class="col-lg-9">
                            <div class="main-content">
                                <!-- Page title -->
                                <div class="page-title">
                                    <div class="row align-items-center">
                                        <div class="col-md-6">
                                            <h2 class="heading heading-6 text-capitalize strong-600 mb-0 d-inline-block">
                                                {{ translate('Conversations') }}
                                            </h2>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="float-md-right">
                                                <ul class="breadcrumb">
                                                    <li><a href="{{ route('home') }}">{{ translate('Home') }}</a></li>
                                                    <li><a href="{{ route('dashboard') }}">{{ translate('Dashboard') }}</a></li>
                                                    <li><a href="{{ route('conversations.index') }}">{{ translate('Conversations') }}</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
        
                                <div class="card no-border mt-4 p-3">
                                    <div class="py-4">
                                        @foreach ($conversations as $key => $conversation)
                                            <div class="block block-comment border-bottom">
                                                <div class="row">
                                                    <div class="col-1">
                                                        <div class="block-image">
                                                            @if (Auth::user()->id == $conversation->sender_id)
                                                                <img @if ($conversation->receiver->avatar_original == null) src="{{ asset('frontend/images/user.png') }}" @else src="{{ asset($conversation->receiver->avatar_original) }}" @endif class="rounded-circle">
                                                            @else
                                                                <img @if ($conversation->sender->avatar_original == null) src="{{ asset('frontend/images/user.png') }}" @else src="{{ asset($conversation->sender->avatar_original) }}" @endif class="rounded-circle">
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-2">
                                                        <p>
                                                            @if (Auth::user()->id == $conversation->sender_id)
                                                                <a href="javascript:;">{{ $conversation->receiver->name }}</a>
                                                            @else
                                                                <a href="javascript:;">{{ $conversation->sender->name }}</a>
                                                            @endif
                                                            <br>
                                                            <span class="comment-date">
                                                                {{ date('h:i:m d-m-Y', strtotime($conversation->messages->last()->created_at)) }}
                                                            </span>
                                                        </p>
                                                    </div>
                                                    <div class="col-9">
                                                        <div class="block-body">
                                                            <div class="block-body-inner pb-3">
                                                                <div class="row no-gutters">
                                                                    <div class="col">
                                                                        <h4 class="heading heading-6">
                                                                            <a href="{{ route('conversations.show', encrypt($conversation->id)) }}">
                                                                                {{ $conversation->title }}
                                                                            </a>
                                                                            @if ((Auth::user()->id == $conversation->sender_id && $conversation->sender_viewed == 0) || (Auth::user()->id == $conversation->receiver_id && $conversation->receiver_viewed == 0))
                                                                                <span class="badge badge-pill badge-danger">{{ translate('New') }}</span>
                                                                            @endif
                                                                        </h4>
                                                                    </div>
                                                                </div>
                                                                <p class="comment-text mt-0">
                                                                    {{ $conversation->messages->last()->message }}
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="pagination-wrapper py-4">
                                    <ul class="pagination justify-content-end">
                                        {{ $conversations->links() }}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
	
    @include('frontend.inc.footer')
    
            
        </div>

       @include('frontend.partials.modal')
       <div class="modal fade" id="addToCart">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="c-preloader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
                <button type="button" class="close absolute-close-btn" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div id="addToCart-modal-body">

                </div>
            </div>
        </div>
    </div>
       <!-- SCRIPTS -->
<!-- <a href="#" class="back-to-top btn-back-to-top"></a> -->

<!-- Core -->
<script src="{{ asset('frontend/js/vendor/popper.min.js') }}"></script>
<script src="{{ asset('frontend/js/vendor/bootstrap.min.js') }}"></script>

<!-- Plugins: Sorted A-Z -->
<script src="{{ asset('frontend/js/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('frontend/js/select2.min.js') }}"></script>
<script src="{{ asset('frontend/js/nouislider.min.js') }}"></script>
<script src="{{ asset('frontend/js/sweetalert2.min.js') }}"></script>
<script src="{{ asset('frontend/js/slick.min.js') }}"></script>
<script src="{{ asset('frontend/js/jssocials.min.js') }}"></script>
<script src="{{ asset('frontend/js/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('frontend/js/jodit.min.js') }}"></script>
<script src="{{ asset('frontend/js/xzoom.min.js') }}"></script>
<script src="{{ asset('frontend/js/fb-script.js') }}"></script>
<script src="{{ asset('frontend/js/lazysizes.min.js') }}"></script>
<script src="{{ asset('frontend/js/intlTelInput.min.js') }}"></script>

<!-- App JS -->
<script src="{{ asset('frontend/js/active-shop.js') }}"></script>
<script src="{{ asset('frontend/js/main.js') }}"></script>


<script>
    $(".log_reg").hover(function(){
    $('.hoverContent').show();
},function(){
    $('.hoverContent').hide();
    $(".hoverContent").hover(function(){
    $('.hoverContent').show();
},function(){
    $('.hoverContent').hide();
});
  
});
    function showFrontendAlert(type, message){
        if(type == 'danger'){
            type = 'error';
        }
        swal({
            position: 'center',
            type: type,
            title: message,
            showCancelButton: false,
            showConfirmButton: false,
            timer: 3000
        });
    }
</script>

@foreach (session('flash_notification', collect())->toArray() as $message)
    <script>
        showFrontendAlert('{{ $message['level'] }}', '{{ $message['message'] }}');
    </script>
@endforeach
<script>

    $(document).ready(function() {
        $('.category-nav-element').each(function(i, el) {
            $(el).on('mouseover', function(){
                if(!$(el).find('.sub-cat-menu').hasClass('loaded')){
                    $.post('{{ route('category.elements') }}', {_token: '{{ csrf_token()}}', id:$(el).data('id')}, function(data){
                        $(el).find('.sub-cat-menu').addClass('loaded').html(data);
                    });
                }
            });
        });
        if ($('#lang-change').length > 0) {
            $('#lang-change .dropdown-item a').each(function() {
                $(this).on('click', function(e){
                    e.preventDefault();
                    var $this = $(this);
                    var locale = $this.data('flag');
                    $.post('{{ route('language.change') }}',{_token:'{{ csrf_token() }}', locale:locale}, function(data){
                        location.reload();
                    });

                });
            });
        }

        if ($('#currency-change').length > 0) {
            $('#currency-change .dropdown-item a').each(function() {
                $(this).on('click', function(e){
                    e.preventDefault();
                    var $this = $(this);
                    var currency_code = $this.data('currency');
                    $.post('{{ route('currency.change') }}',{_token:'{{ csrf_token() }}', currency_code:currency_code}, function(data){
                        location.reload();
                    });

                });
            });
        }
    });

    $('#search').on('keyup', function(){
        search();
    });

    $('#search').on('focus', function(){
        search();
    });

    function search(){
        var search = $('#search').val();
        if(search.length > 0){
            $('body').addClass("typed-search-box-shown");

            $('.typed-search-box').removeClass('d-none');
            $('.search-preloader').removeClass('d-none');
            $.post('{{ route('search.ajax') }}', { _token: '{{ @csrf_token() }}', search:search}, function(data){
                if(data == '0'){
                    // $('.typed-search-box').addClass('d-none');
                    $('#search-content').html(null);
                    $('.typed-search-box .search-nothing').removeClass('d-none').html('Sorry, nothing found for <strong>"'+search+'"</strong>');
                    $('.search-preloader').addClass('d-none');

                }
                else{
                    $('.typed-search-box .search-nothing').addClass('d-none').html(null);
                    $('#search-content').html(data);
                    $('.search-preloader').addClass('d-none');
                }
            });
        }
        else {
            $('.typed-search-box').addClass('d-none');
            $('body').removeClass("typed-search-box-shown");
        }
    }

    function updateNavCart(){
        $.post('{{ route('cart.nav_cart') }}', {_token:'{{ csrf_token() }}'}, function(data){
            $('#cart_items').html(data);
        });
    }

    function removeFromCart(key){
        $.post('{{ route('cart.removeFromCart') }}', {_token:'{{ csrf_token() }}', key:key}, function(data){
            updateNavCart();
            $('#cart-summary').html(data);
            showFrontendAlert('success', 'Item has been removed from cart');
            $('#cart_items_sidenav').html(parseInt($('#cart_items_sidenav').html())-1);
        });
    }

    function addToCompare(id){
        $.post('{{ route('compare.addToCompare') }}', {_token:'{{ csrf_token() }}', id:id}, function(data){
            $('#compare').html(data);
            @php 
            $compare = session()->get('compare', collect([]));
            @endphp
            @if(count($compare) == 3) 
            showFrontendAlert('error', 'You Cannot Compare More than 3 Product');
            $('#compare_items_sidenav').html(parseInt($('#compare_items_sidenav').html())+1);
                 @else 
                showFrontendAlert('success', 'Item has been added to compare list');
            $('#compare_items_sidenav').html(parseInt($('#compare_items_sidenav').html())+1);
                
                   @endif  
    }

    function addToWishList(id){
        @if (Auth::check() && (Auth::user()->user_type == 'customer' || Auth::user()->user_type == 'seller'))
            $.post('{{ route('wishlists.store') }}', {_token:'{{ csrf_token() }}', id:id}, function(data){
                if(data != 0){
                    $('#wishlist').html(data);
                    showFrontendAlert('success', 'Item has been added to wishlist');
                }
                else{
                    showFrontendAlert('warning', 'Please login first');
                }
            });
        @else
            showFrontendAlert('warning', 'Please login first');
        @endif
    }

    function showAddToCartModal(id){
        if(!$('#modal-size').hasClass('modal-lg')){
            $('#modal-size').addClass('modal-lg');
        }
        $('#addToCart-modal-body').html(null);
        $('#addToCart').modal();
        $('.c-preloader').show();
        $.post('{{ route('cart.showCartModal') }}', {_token:'{{ csrf_token() }}', id:id}, function(data){
            $('.c-preloader').hide();
            $('#addToCart-modal-body').html(data);
            $('.xzoom, .xzoom-gallery').xzoom({
                Xoffset: 20,
                bg: true,
                tint: '#000',
                defaultScale: -1
            });
            getVariantPrice();
        });
    }

    $('#option-choice-form input').on('change', function(){
        getVariantPrice();
    });

    function getVariantPrice(){
        if($('#option-choice-form input[name=quantity]').val() > 0 && checkAddToCartValidity()){
            $.ajax({
               type:"POST",
               url: '{{ route('products.variant_price') }}',
               data: $('#option-choice-form').serializeArray(),
               success: function(data){
                   $('#option-choice-form #chosen_price_div').removeClass('d-none');
                   $('#option-choice-form #chosen_price_div #chosen_price').html(data.price);
                   $('#available-quantity').html(data.quantity);
                   $('.input-number').prop('max', data.quantity);
                   //console.log(data.quantity);
                   if(parseInt(data.quantity) < 1 && data.digital  != 1){
                       $('.buy-now').hide();
                       $('.add-to-cart').hide();
                   }
                   else{
                       $('.buy-now').show();
                       $('.add-to-cart').show();
                   }
               }
           });
        }
    }

    function checkAddToCartValidity(){
        var names = {};
        $('#option-choice-form input:radio').each(function() { // find unique names
              names[$(this).attr('name')] = true;
        });
        var count = 0;
        $.each(names, function() { // then count them
              count++;
        });

        if($('#option-choice-form input:radio:checked').length == count){
            return true;
        }

        return false;
    }

    function addToCart(){
        if(checkAddToCartValidity()) {
            $('#addToCart').modal();
            $('.c-preloader').show();
            $.ajax({
               type:"POST",
               url: '{{ route('cart.addToCart') }}',
               data: $('#option-choice-form').serializeArray(),
               success: function(data){
                   $('#addToCart-modal-body').html(null);
                   $('.c-preloader').hide();
                   $('#modal-size').removeClass('modal-lg');
                   $('#addToCart-modal-body').html(data);
                   updateNavCart();
                   $('#cart_items_sidenav').html(parseInt($('#cart_items_sidenav').html())+1);
               }
           });
        }
        else{
            showFrontendAlert('warning', 'Please choose all the options');
        }
    }

    function buyNow(){
        if(checkAddToCartValidity()) {
            $('#addToCart').modal();
            $('.c-preloader').show();
            $.ajax({
               type:"POST",
               url: '{{ route('cart.addToCart') }}',
               data: $('#option-choice-form').serializeArray(),
               success: function(data){
                   //$('#addToCart-modal-body').html(null);
                   //$('.c-preloader').hide();
                   //$('#modal-size').removeClass('modal-lg');
                   //$('#addToCart-modal-body').html(data);
                   updateNavCart();
                   $('#cart_items_sidenav').html(parseInt($('#cart_items_sidenav').html())+1);
                   window.location.replace("{{ route('cart') }}");
               }
           });
        }
        else{
            showFrontendAlert('warning', 'Please choose all the options');
        }
    }

    function show_purchase_history_details(order_id)
    {
        $('#order-details-modal-body').html(null);

        if(!$('#modal-size').hasClass('modal-lg')){
            $('#modal-size').addClass('modal-lg');
        }

        $.post('{{ route('purchase_history.details') }}', { _token : '{{ @csrf_token() }}', order_id : order_id}, function(data){
            $('#order-details-modal-body').html(data);
            $('#order_details').modal();
            $('.c-preloader').hide();
        });
    }

    function show_order_details(order_id)
    {
        $('#order-details-modal-body').html(null);

        if(!$('#modal-size').hasClass('modal-lg')){
            $('#modal-size').addClass('modal-lg');
        }

        $.post('{{ route('orders.details') }}', { _token : '{{ @csrf_token() }}', order_id : order_id}, function(data){
            $('#order-details-modal-body').html(data);
            $('#order_details').modal();
            $('.c-preloader').hide();
        });
    }

    function cartQuantityInitialize(){
        $('.btn-number').click(function(e) {
            e.preventDefault();

            fieldName = $(this).attr('data-field');
            type = $(this).attr('data-type');
            var input = $("input[name='" + fieldName + "']");
            var currentVal = parseInt(input.val());

            if (!isNaN(currentVal)) {
                if (type == 'minus') {

                    if (currentVal > input.attr('min')) {
                        input.val(currentVal - 1).change();
                    }
                    if (parseInt(input.val()) == input.attr('min')) {
                        $(this).attr('disabled', true);
                    }

                } else if (type == 'plus') {

                    if (currentVal < input.attr('max')) {
                        input.val(currentVal + 1).change();
                    }
                    if (parseInt(input.val()) == input.attr('max')) {
                        $(this).attr('disabled', true);
                    }

                }
            } else {
                input.val(0);
            }
        });

        $('.input-number').focusin(function() {
            $(this).data('oldValue', $(this).val());
        });

        $('.input-number').change(function() {

            minValue = parseInt($(this).attr('min'));
            maxValue = parseInt($(this).attr('max'));
            valueCurrent = parseInt($(this).val());

            name = $(this).attr('name');
            if (valueCurrent >= minValue) {
                $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
            } else {
                alert('Sorry, the minimum value was reached');
                $(this).val($(this).data('oldValue'));
            }
            if (valueCurrent <= maxValue) {
                $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
            } else {
                alert('Sorry, the maximum value was reached');
                $(this).val($(this).data('oldValue'));
            }


        });
        $(".input-number").keydown(function(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    }

     function imageInputInitialize(){
         $('.custom-input-file').each(function() {
             var $input = $(this),
                 $label = $input.next('label'),
                 labelVal = $label.html();

             $input.on('change', function(e) {
                 var fileName = '';

                 if (this.files && this.files.length > 1)
                     fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
                 else if (e.target.value)
                     fileName = e.target.value.split('\\').pop();

                 if (fileName)
                     $label.find('span').html(fileName);
                 else
                     $label.html(labelVal);
             });

             // Firefox bug fix
             $input
                 .on('focus', function() {
                     $input.addClass('has-focus');
                 })
                 .on('blur', function() {
                     $input.removeClass('has-focus');
                 });
         });
     }

</script>
       @yield('script')
  
        <!--====== Vendors js ======-->
        <!-- <script src="{{ asset('assets/js/vendor/jquery-3.5.1.min.js') }}"></script> -->
        <script src="{{ asset('assets/js/vendor/modernizr-3.7.1.min.js') }}"></script>
        <script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
        <script>
$('#iconified').on('keyup', function() {
    var input = $(this);
    if(input.val().length === 0) {
        input.addClass('empty');
    } else {
        input.removeClass('empty');
    }
});
        </script>
            <script>
      // With the above scripts loaded, you can call `tippy()` with a CSS
      // selector and a `content` prop:
      tippy('.favart_add', {
        content: 'Add To Favorite',
        placement: 'bottom',
      });
      tippy('.compare_add', {
        content: 'Compare',
        placement: 'bottom',
      });
    </script>

        <!--====== Plugins js ======-->
        <!-- <script src="assets/js/plugins/bootstrap.min.js"></script>
        <script src="assets/js/plugins/popper.min.js"></script>
        <script src="assets/js/plugins/meanmenu.js"></script>
        <script src="assets/js/plugins/owl-carousel.js"></script>
        <script src="assets/js/plugins/jquery.nice-select.js"></script>
        <script src="assets/js/plugins/countdown.js"></script>
        <script src="assets/js/plugins/elevateZoom.js"></script>
        <script src="assets/js/plugins/jquery-ui.min.js"></script>
        <script src="assets/js/plugins/slick.js"></script>
        <script src="assets/js/plugins/scrollup.js"></script>
        <script src="assets/js/plugins/range-script.js"></script> -->

        <!--====== Use the minified version files listed below for better performance and remove the files listed above ======-->

        <script src="{{ asset('assets/js/plugins.min.js') }}"></script>

        <!-- Main Activation JS -->
        <script src="{{ asset('assets/js/main.js') }}"></script>
    </body>

<!-- Mirrored from live.hasthemes.com/html/5/ecolife-preview/ecolife/index-8.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Aug 2020 14:00:50 GMT -->
</html>
