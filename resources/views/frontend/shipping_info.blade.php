@extends('frontend.layouts.app')

@section('content')
<style>
.error {
  color: #F00;
  background-color: #FFF;
}

    .required:after {
        content: " *";
        color: red;
    }
</style>
    <div id="page-content">
    <section class="slice-xs sct-color-2 border-bottom">
        <div class="container container-sm">
            <div class="row cols-delimited justify-content-center">
                <div class="col">
                    <div class="icon-block icon-block--style-1-v5 text-center">
                        <div class="block-icon c-gray-light mb-0">
                            <i class="la la-shopping-cart"></i>
                        </div>
                        <div class="block-content d-none d-md-block">
                            <h3 class="heading heading-sm strong-300 c-gray-light text-capitalize">{{ translate('1. My Cart')}}</h3>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="icon-block icon-block--style-1-v5 text-center active">
                        <div class="block-icon mb-0">
                            <i class="la la-map-o"></i>
                        </div>
                        <div class="block-content d-none d-md-block">
                            <h3 class="heading heading-sm strong-300 c-gray-light text-capitalize">{{ translate('2. Shipping info')}}</h3>
                        </div>
                    </div>
                </div>

                <!-- <div class="col">
                    <div class="icon-block icon-block--style-1-v5 text-center">
                        <div class="block-icon mb-0 c-gray-light">
                            <i class="la la-truck"></i>
                        </div>
                        <div class="block-content d-none d-md-block">
                            <h3 class="heading heading-sm strong-300 c-gray-light text-capitalize">{{ translate('3. Delivery info')}}</h3>
                        </div>
                    </div>
                </div> -->

                <div class="col">
                    <div class="icon-block icon-block--style-1-v5 text-center">
                        <div class="block-icon c-gray-light mb-0">
                            <i class="la la-credit-card"></i>
                        </div>
                        <div class="block-content d-none d-md-block">
                            <h3 class="heading heading-sm strong-300 c-gray-light text-capitalize">{{ translate('3. Payment')}}</h3>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="icon-block icon-block--style-1-v5 text-center">
                        <div class="block-icon c-gray-light mb-0">
                            <i class="la la-check-circle"></i>
                        </div>
                        <div class="block-content d-none d-md-block">
                            <h3 class="heading heading-sm strong-300 c-gray-light text-capitalize">{{ translate('4. Confirmation')}}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

        <section class="py-4 gry-bg">
            <div class="container">
                <div class="row cols-xs-space cols-sm-space cols-md-space">
                    <div class="col-lg-8">
                        <form class="form-default" data-toggle="validator" action="{{ route('checkout.store_shipping_infostore') }}" role="form" method="POST">
                            @csrf
                                @if(Auth::check())
                                    <div class="row gutters-5">
                                        @foreach (Auth::user()->addresses as $key => $address)
                                            <div class="col-md-6">
                                                <label class="aiz-megabox d-block bg-white">
                                                    <input type="radio" name="address_id" value="{{ $address->id }}" @if ($address->set_default)
                                                        checked
                                                    @endif required>
                                                    <span class="d-flex p-3 aiz-megabox-elem">
                                                        <span class="aiz-rounded-check flex-shrink-0 mt-1"></span>
                                                        <span class="flex-grow-1 pl-3">
                                                        <div>
                                                <span class="alpha-6">{{ translate('Address') }}:</span>
                                                <span class="strong-600 ml-2">{{ $address->address }}</span>
                                            </div>
                                            <div>
                                                <span class="alpha-6">{{ translate('District') }}:</span>
                                                <span class="strong-600 ml-2">{{ $address->district }}</span>
                                            </div>
                                            <div>
                                                <span class="alpha-6">{{ translate('State') }}:</span>
                                                <span class="strong-600 ml-2">{{ $address->state }}</span>
                                            </div>
                                            <div>
                                                <span class="alpha-6">{{ translate('City') }}:</span>
                                                <span class="strong-600 ml-2">{{ $address->city }}</span>
                                            </div>
                                            <div>
                                                <span class="alpha-6">{{ translate('postal Code') }}:</span>
                                                <span class="strong-600 ml-2">{{ $address->postal_code }}</span>
                                            </div>
                                            <div>
                                                <span class="alpha-6">{{ translate('Region') }}:</span>
                                                <span class="strong-600 ml-2">{{ $address->region }}</span>
                                            </div>
                                            <div>
                                                <span class="alpha-6">{{ translate('Landmark') }}:</span>
                                                <span class="strong-600 ml-2">{{ $address->landmark }}</span>
                                            </div>

                                            <div>
                                                <span class="alpha-6">{{ translate('Phone') }}:</span>
                                                <span class="strong-600 ml-2">{{ $address->phone }}</span>
                                            </div>
                                                        </span>
                                                    </span>
                                                </label>
                                            </div>
                                        @endforeach
                                        <input type="hidden" name="checkout_type" value="logged">
                                        <div class="col-md-6 mx-auto" onclick="add_new_address()">
                                            <div class="border p-3 rounded mb-3 c-pointer text-center bg-white">
                                                <i class="la la-plus la-2x"></i>
                                                <div class="alpha-7">{{ translate('Add New Address') }}</div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">{{ translate('Name')}}</label>
                                                    <input type="text" class="form-control" name="name" placeholder="{{ translate('Name')}}" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">{{ translate('Email')}}</label>
                                                    <input type="text" class="form-control" name="email" placeholder="{{ translate('Email')}}" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">{{ translate('Address')}}</label>
                                                    <input type="text" class="form-control" name="address" placeholder="{{ translate('Address')}}" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            
                                            <div class="col-md-6">
                                                <div class="form-group has-feedback">
                                                    <label class="control-label">{{ translate('City')}}</label>
                                                    <input type="text" class="form-control" placeholder="{{ translate('City')}}" name="city" required>
                                                </div>
                                            </div>
                                       

                                        
                                            <div class="col-md-6">
                                                <div class="form-group has-feedback">
                                                    <label class="control-label">{{ translate('Postal code')}}</label>
                                                    <input type="text"  class="form-control" placeholder="{{ translate('Postal code')}}" maxlength="6" name="postal_code" required>
                                                </div>
                                            </div>
                                            </div>
                                            <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group has-feedback">
                                                    <label class="control-label">{{ translate('Phone')}}</label>
                                                    <input type="text"  class="form-control" pattern="[+[0-9]]{13}" placeholder="{{ translate('Phone')}}" name="phone" maxlength="13" required>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="checkout_type" value="guest">
                                    </div>
                                    </div>
                                @endif
                            <div class="row align-items-center pt-4">
                                <div class="col-md-6">
                                    <a href="{{ route('home') }}" class="link link--style-3">
                                        {{-- <i class="ion-android-arrow-back"></i> --}}
                                        {{ translate('Return to shop')}}
                                    </a>
                                </div>
                                <div class="col-md-6 text-right">
                                    <button type="submit" class="btn btn-styled btn-base-1">{{ translate('Continue to Payment Info')}}</a>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-lg-4 ml-lg-auto">
                        @include('frontend.partials.cart_summary')
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="modal fade" id="new-address-modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-zoom" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="exampleModalLabel">{{ translate('New Address')}}</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form-default" role="form" action="{{ route('addresses.store') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="p-3">
                        <div class="row">
                            <div class="col-md-2">
                                <label class="required">{{ translate('Address') }}</label>
                            </div>
                            <div class="col-md-10">
                                <textarea class="form-control textarea-autogrow mb-3"
                                    placeholder="{{ translate('Your Address') }}" rows="1" name="address"
                                    required></textarea>
                            </div>
                        </div>
                        <!-- <div class="row">
                                <div class="col-md-2">
                                    <label>{{ translate('Country') }}</label>
                                </div>
                                <div class="col-md-10">
                                    <div class="mb-3">
                                        <select class="form-control mb-3 selectpicker" data-placeholder="{{translate('Select your country')}}" name="country" required>
                                            @foreach (\App\Country::where('status', 1)->get() as $key => $country)
                                                <option value="{{ $country->name }}">{{ $country->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div> -->

                        <div class="row">
                            <div class="col-md-2">
                                <label class="required">{{ translate('Postal code')}}</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" pattern="[0-9]{6}" onchange="userAction();" class="form-control mb-3"
                                    placeholder="{{ translate('Enter Postal Code')}}" name="postal_code"
                                    id="postal_code" maxlength="6" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="required">{{ translate('District')}}</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" class="form-control mb-3"
                                    placeholder="{{ translate('Enter District')}}" name="district_id" id="district_id"
                                    maxlength="30" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="required">{{ translate('State')}}</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" class="form-control mb-3" placeholder="{{ translate('Enter State')}}"
                                    name="state_id" id="state_id" maxlength="30" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="required">{{ translate('Region')}}</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" class="form-control mb-3"
                                    placeholder="{{ translate('Enter Region')}}" name="region_id" id="region_id"
                                    maxlength="30" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="required">{{ translate('Landmark')}}</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" class="form-control mb-3"
                                    placeholder="{{ translate('Enter Landmark')}}" name="landmark_id" id="landmark_id"
                                    maxlength="50" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="required">{{ translate('City')}}</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" class="form-control mb-3" placeholder="{{ translate('Enter City')}}"
                                    name="city" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="required">{{ translate('Phone')}}</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" class="form-control mb-3" placeholder="{{ translate('+91')}}"
                                    name="phone" maxlength="13" required pattern="[+0-9]{13}">
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-base-1">{{  translate('Save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js" integrity="sha512-UdIMMlVx0HEynClOIFSyOrPggomfhBKJE28LKl8yR3ghkgugPnG6iLfRfHwushZl1MOPSY6TsuBDGPK2X4zYKg==" crossorigin="anonymous"></script>
<script type="text/javascript">

$(function(){
    $.validator.addMethod("regx", function(value, element, regexpr) {          
    return regexpr.test(value);
}, "Please enter a valid phone number.");
    $("#address_form").validate({
  
    rules: {
        address:{
            required: true ,
        },
        city:{
            required: true ,
        },
        postal_code:{
            required: true ,
            maxlength:6,
            digits: true
        },
      phone:{
       
            required: true ,
            //change regexp to suit your needs
            regx: /^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$/,
          
            maxlength:13,
        
      },
      
    },
  
    messages: {
        address:{
            required : "Please enter address",
        },
        city:{
            required : "Please enter city",
        },
        postal_code:{
            required : "Please enter postal code",
            digits:"Postal code should be in integer",
            maxlength:"Invalid postal code"
        },
        phone: {
      required : "Please enter phone number",
      regx:"Invalid phone number",
      maxlength:'Phone number contains 10 digit only'
    },
    },
    errorElement: 'span',
//   errorPlacement: function(error, element) {
//     error.addClass('invalid-feedback');
//     element.closest('.form-group').append(error);
//   },
  highlight: function(element, errorClass, validClass) {
    $(element).addClass('is-invalid');
  },
  unhighlight: function(element, errorClass, validClass) {
    $(element).removeClass('is-invalid');
  },
    submitHandler: function(form) {
      form.submit();
    }
  });
    
});
    function add_new_address(){
        $('#new-address-modal').modal('show');
    }
    function userAction(val){
let postal_code=$('#postal_code').val();
    $.ajax({
        url:'https://api.postalpincode.in/pincode/'+postal_code,
        dataType:'json',
        type: 'GET',
      
        success:function(response){
            //console.log(response[0].PostOffice[0])
            $('#district_id').val(response[0].PostOffice[0].District);
            $('#state_id').val(response[0].PostOffice[0].State);
            $('#region_id').val(response[0].PostOffice[0].Region);
          
             
        }
    })
  }
    $("input[name=phone]").focus(function(){
        if($("input[name=phone]").val()=="")
  $("input[name=phone]").val("+91");

});

  $("input[name=phone]").keyup(function(){
    // var prefix = "+91"

    // if(this.value.indexOf(prefix) !== 0 ){
    //     this.value = prefix + this.value;
    // }
    if($(this).val().indexOf('+91') == 0) {
        $(this).val($(this).val());
    }else{
      $phone_val=$(this).val();
      if($phone_val!=""){
        $(this).val("");
      }
      $(this).val("+91" + $(this).val());
    }
});
</script>
@endsection
