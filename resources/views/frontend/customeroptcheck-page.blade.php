<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link name="favicon" type="image/x-icon" href="{{asset('img/favicon.png')}}" rel="shortcut icon" />

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="apple-touch-icon" href="{{ asset('front/app-assets/images/ico/apple-icon-120.html')}}">
    <link type="image/x-icon" href="{{ asset(\App\GeneralSetting::first()->favicon) }}" rel="shortcut icon" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/vendors/css/vendors.min.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/bootstrap-extended.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/colors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/components.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/themes/dark-layout.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/themes/semi-dark-layout.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/vendors/css/extensions/sweetalert2.min.css')}}">
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css"
        href="{{ asset('front/app-assets/css/core/menu/menu-types/vertical-menu.min.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('front/app-assets/css/core/colors/palette-gradient.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/app-assets/css/pages/authentication.css')}}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('front/assets/css/style.css')}}">
    <!-- END: Custom CSS-->
<style>
    .form-label-group {
    position: relative;
    margin-bottom: 1.5rem;
    width: 250px;
}
</style>
</head>
<!-- END: Head-->
@php
$generalsetting = \App\GeneralSetting::first();
@endphp
<!-- BEGIN: Body-->

<body
    class="vertical-layout vertical-menu-modern 1-column  navbar-floating footer-static bg-full-screen-image  blank-page blank-page"
    data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="row flexbox-container">
                    <div class="col-xl-12 col-12 d-flex " style="margin-left: 70%;">
                        <div class="card bg-authentication rounded-0 mb-0">
                            <div class="row m-0">

                                <div class="col-lg-12col-12 p-0">
                                    <div class="card rounded-0 mb-0 px-2">
                                        <div class="card-header pb-1">
                                            <div class="card-title">
                                                <h4 class="mb-0">OTP Verification</h4>
                                            </div>
                                        </div>
                                        <p class="px-2">Please enter otp.</p>
                                        <div class="card-content">
                                            <div class="card-body pt-1">
                                               
                                                <form method="" action="#">
                                                    @csrf
                                                    <input type="hidden" id="phone" name="phone" value={{decrypt($phone)}}>
                                                    <fieldset
                                                        class="form-label-group form-group position-relative has-icon-left">
                                                        <input id="otp" type="text"
                                                            class="form-control @error('otp') is-invalid @enderror"
                                                            name="otp" value="{{ old('otp') }}" required
                                                            autocomplete="off" autofocus placeholder="OTP">


                                                        <div class="form-control-position">
                                                            <i class="fa fa-mobile"></i>
                                                        </div>
                                                        <label for="user-name">{{ __('Enter Otp') }}</label>
                                                    </fieldset>
                                                    <fieldset
                                                        class="form-label-group form-group position-relative has-icon-left">
                                                        <input id="new_passowrd" type="password"
                                                            class="form-control @error('new_passowrd') is-invalid @enderror"
                                                            name="new_passowrd" value="{{ old('new_passowrd') }}"
                                                            required autocomplete="off" autofocus placeholder="New Password">

                                                        @error('email')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                        <div class="form-control-position">
                                                            <i class="fa fa-key"></i>
                                                        </div>
                                                        <label for="user-name">{{ __('Password') }}</label>
                                                    </fieldset>
                                                    <fieldset
                                                        class="form-label-group form-group position-relative has-icon-left">
                                                        <input id="confirm_password" type="password"
                                                            class="form-control @error('confirm_password') is-invalid @enderror"
                                                            name="confirm_password"
                                                            value="{{ old('confirm_password') }}" required
                                                            autocomplete="off" autofocus placeholder="Confirm Passowrd">


                                                        <div class="form-control-position">
                                                            <i class="fa fa-key"></i>
                                                        </div>
                                                        <label for="user-name">{{ __('Confirm Password') }}</label>
                                                    </fieldset>
                                                    <button type="button" onclick="otpCheck();" class="btn btn-primary">
                                                        {{ __('Submit') }}
                                                    </button>
                                                    <button type="button" onclick="window.location.href='{{route('login')}}'" class="btn btn-primary">
                                                        {{ __('Back to login') }}
                                                    </button>
                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('front/app-assets/vendors/js/vendors.min.js')}}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('front/app-assets/js/core/app-menu.min.js')}}"></script>
    <script src="{{ asset('front/app-assets/js/core/app.min.js')}}"></script>
    <script src="{{ asset('front/app-assets/js/scripts/components.min.js')}}"></script>
    <script src="{{ asset('frontend/js/sweetalert2.min.js') }}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <!-- END: Page JS-->
    <script>
        $(function(){
        $('.successErrorMessage').delay(8000).slideUp(300);
  

$('input[type="password"]').bind('keyup', function() {
      $('.label-important').remove(); 
      var strongRegex =/^((?!.*[\s])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#\$%\^&\*\s])(?=.*\d).{8,15})/;
      // var strongRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*\s])(?=.{8,})";
    if(this.value.match(strongRegex)) 
        { 
        //alert('Correct, try another...')
        return true;
        }
        else
        { 
          $('.label-important').remove(); 
          $(this).after("<span class='label label-important' style='margin-left: 5%;color: #e63b05;font-weight: 600;font-size: 12px;'>Your password must contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character, but cannot contain whitespace.</span>");
          return false;  
        }
    });
       });

function otpCheck(){
             var otp=$("#otp").val();
            var new_passowrd=$("#new_passowrd").val();
            var confirm_password=$("#confirm_password").val();
            var phone=$('#phone').val();
            var token = $("input[name='_token']").val();
   
           var form_data=new FormData();
           form_data.append('new_passowrd',new_passowrd);
           form_data.append('confirm_password',confirm_password);
           form_data.append('otp',otp);
           form_data.append('phone',phone);
           form_data.append('_token',token);
           $.ajax({
               type: "post",
               url: "{{ route('customerpostOtpVerify') }}",
               cache: false,
               processData: false,
               contentType: false,
               data: form_data,
               dataType:"json",
               success: function(response) {
                   if(response.status==1){
                    
                    Swal({
            position: 'center',
            type: 'success',
            title: response.message,
            showCancelButton: false,
            confirmButtonColor: '#ff7200', // There won't be any cancel button
            confirmButtonText: "Please Login!",
            timer: 10000
        }).then(function (e) {
            console.log(e)
            if (e.value === true) {
              location.href="{{route('user.login')}}";

            } else {
                e.dismiss;
            }

        }, function (dismiss) {
            return false;
        });
  

               }else{
                  
                   showFrontendAlert("error", response.message);
                  //wizardfunc(next,currentActiveStep)
               }
                   },
                           error: function (jqXHR, textStatus, errorThrown) {
                               //$(".se-pre-con").fadeOut("slow");
                               var msg = "";
                               if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                                   msg += "<strong>Something Went Wrong,Please Reload and Try Again</strong>";
                               } else {
                                   if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                       msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                   } else {
                                       msg += "Error(s):<strong><ul>";
                                       $.each(jqXHR.responseJSON['errors'], function (key, value) {
                                           msg += "<li>" + value + "</li>";
                                       });
                                       msg += "</ul></strong>";
                                   }
                               }
                               isValid=0;
                               showFrontendAlert("error", msg);
                           }
           });
}
function showFrontendAlert(type, message){
        if(type == 'danger'){
            type = 'error';
        }

       
        Swal({
            position: 'center',
            type: type,
            title: message,
            showCancelButton: false,
            confirmButtonColor: '#ff7200', // There won't be any cancel button
            showConfirmButton: true ,
            timer: 10000
        });
    }
    </script>
</body>
<!-- END: Body-->

<!-- Mirrored from pixinvent.com/demo/vuexy-html-bootstrap-admin-template/html/ltr/vertical-menu-template/auth-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Aug 2020 19:44:13 GMT -->

</html>