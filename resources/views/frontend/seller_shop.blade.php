@extends('frontend.layouts.app')

@section('meta_title'){{ $shop->meta_title }}@stop

@section('meta_description'){{ $shop->meta_description }}@stop

@section('meta')
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="{{ $shop->meta_title }}">
    <meta itemprop="description" content="{{ $shop->meta_description }}">
    <meta itemprop="image" content="{{ asset($shop->logo) }}">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="product">
    <meta name="twitter:site" content="@publisher_handle">
    <meta name="twitter:title" content="{{ $shop->meta_title }}">
    <meta name="twitter:description" content="{{ $shop->meta_description }}">
    <meta name="twitter:creator" content="@author_handle">
    <meta name="twitter:image" content="{{ asset($shop->meta_img) }}">

    <!-- Open Graph data -->
    <meta property="og:title" content="{{ $shop->meta_title }}" />
    <meta property="og:type" content="Shop" />
    <meta property="og:url" content="{{ route('shop.visit', $shop->slug) }}" />
    <meta property="og:image" content="{{ asset($shop->logo) }}" />
    <meta property="og:description" content="{{ $shop->meta_description }}" />
    <meta property="og:site_name" content="{{ $shop->name }}" />
@endsection
<style>
.alert {
    margin-top:10px;
  padding: 20px;
  background-color:#ff7200;
  color: white;
}

.closebtn {
  margin-left: 15px;
  color: white;
  font-weight: bold;
  float: right;
  font-size: 22px;
  line-height: 20px;
  cursor: pointer;
  transition: 0.3s;
  display: none;
}

.closebtn:hover {
  color: black;
}
</style>
<style>

.breadcrumb-area {
    text-align: center;
    position: relative;
    padding: 70px 0;
    background: none no-repeat !important;
    background-size: cover;
}

.breadcrumb-area {
    padding: 5px 0;
    border-bottom: 1px solid #e9e9e9;
    background-color: #fafafa;
}
.breadcrumb {
    margin-bottom: 0rem !important;
}
.add-to-link ul li {
    display: inline-flex;
    justify-content: center;
    float: none;
    width: 32%;
}
.home-cosmatics .new1 {
    background-color: #3d5a73;
}
.home-cosmatics .new1 {
    background-color: #ff7200;
}
.new1 {
    position: absolute;
    top: 10px!important;
    right: 10px;
    display: inline-block;
    color: #fff;
    padding: 0 5px;
    text-align: center;
    line-height: 20px;
    height: 20px;
    font-size: 12px;
    font-weight: 700;
    border-radius: 5px;
    text-transform: capitalize;
    text-align: center;
    z-index: 1;
    background-color: #ff7200;
}

.add-to-link ul li a {
   
    padding:13px !important;
}
.add-to-link ul li {
    height: 40px;
}
a.quick_view {
  
  padding: 12px !important;
}
.dropdown_search{
    height: 48px;
}
.slick-track{
        min-height: 425px !important;
}
.caorusel-box{
     min-height: 425px !important;
}
</style>
@section('content')
    <!-- <section>
        <img loading="lazy"  src="https://via.placeholder.com/2000x300.jpg" alt="" class="img-fluid">
    </section> -->

    @php
        $total = 0;
        $rating = 0;
        foreach ($shop->user->products as $key => $seller_product) {
            $total += $seller_product->reviews->count();
            $rating += $seller_product->reviews->sum('rating');
        }
    @endphp

    <section class="gry-bg pt-4 ">
        <div class="container">
            <div class="row align-items-baseline">
                <div class="col-md-6">
                    <div class="d-flex">
                        <img
                            height="70"
                            class="lazyload"
                            src="{{ asset('frontend/images/placeholder.jpg') }}"
                            data-src="@if ($shop->logo !== null) {{ asset($shop->logo) }} @else {{ asset('frontend/images/placeholder.jpg') }} @endif"
                            alt="{{ $shop->name }}"
                        >
                        <div class="pl-4">
                            <h3 class="strong-700 heading-4 mb-0">{{ $shop->name }}
                                @if ($shop->user->seller->verification_status == 1)
                                    <span class="ml-2"><i class="fa fa-check-circle" style="color:green"></i></span>
                                @else
                                    <span class="ml-2"><i class="fa fa-times-circle" style="color:red"></i></span>
                                @endif
                            </h3>
                            <div class="star-rating star-rating-sm mb-1">
                                @if ($total > 0)
                                    {{ renderStarRating($rating/$total) }}
                                @else
                                    {{ renderStarRating(0) }}
                                @endif
                            </div>
                            <div class="location alpha-6">{{ $shop->address }}</div>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-6">
                    <ul class="text-md-right mt-4 mt-md-0 social-nav model-2">
                        @if ($shop->facebook != null)
                            <li>
                                <a href="{{ $shop->facebook }}" class="facebook social_a" target="_blank" data-toggle="tooltip" data-original-title="Facebook">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                        @endif
                        @if ($shop->twitter != null)
                            <li>
                                <a href="{{ $shop->twitter }}" class="twitter social_a" target="_blank" data-toggle="tooltip" data-original-title="Twitter">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                        @endif
                        @if ($shop->google != null)
                            <li>
                                <a href="{{ $shop->google }}" class="google-plus social_a" target="_blank" data-toggle="tooltip" data-original-title="Google">
                                    <i class="fa fa-google-plus"></i>
                                </a>
                            </li>
                        @endif
                        @if ($shop->youtube != null)
                            <li>
                                <a href="{{ $shop->youtube }}" class="youtube social_a" target="_blank" data-toggle="tooltip" data-original-title="Youtube">
                                    <i class="fa fa-youtube"></i>
                                </a>
                            </li>
                        @endif
                    </ul>
                </div> --}}
            </div>
        </div>
    </section>
    <section class="bg-white">
        <div class="container">
            <div class="row sticky-top mt-4">
                <div class="col">
                    <div class="seller-shop-menu">
                        <ul class="inline-links">
                            <li @if(!isset($type)) class="active" @endif><a href="{{ route('shop.visit', $shop->slug) }}">{{ translate('Store Home')}}</a></li>
                            <li @if(isset($type) && $type == 'top_selling') class="active" @endif><a href="{{ route('shop.visit.type', ['slug'=>$shop->slug, 'type'=>'top_selling']) }}">{{ translate('Top Selling')}}</a></li>
                            <li @if(isset($type) && $type == 'all_products') class="active" @endif><a href="{{ route('shop.visit.type', ['slug'=>$shop->slug, 'type'=>'all_products']) }}">{{ translate('All Products')}}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @if (!isset($type))
        <section class="py-4">
            <div class="container">
                <div class="home-slide">
                    <div class="slick-carousel" data-slick-arrows="true" data-slick-dots="true">
                        @if ($shop->sliders != null)
                            @foreach (json_decode($shop->sliders) as $key => $slide)
                                <div class="">
                                    <img class="d-block w-100 lazyload" src="{{ asset('frontend/images/placeholder-rect.jpg') }}" data-src="{{ asset($slide) }}" alt="{{ $key }} slide" style="max-height:300px;">
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </section>
        <section class="sct-color-1 pt-5 pb-4">
            <div class="container">
                <div class="section-title section-title--style-1 text-center mb-4">
                    <h3 class="section-title-inner heading-3 strong-600">
                        {{ translate('Featured Products')}}
                    </h3>
                </div>
                <div class="row">
                    <div class="col">
                        @if($shop->user->products->where('is_approve', 1)->where('published', 1)->where('featured', 1)->count() >0)
                        <div class="caorusel-box arrow-round gutters-15">
                            <div class="slick-carousel center-mode" data-slick-items="5" data-slick-lg-items="3"  data-slick-md-items="3" data-slick-sm-items="1" data-slick-xs-items="1">
                                @foreach ($shop->user->products->where('is_approve', 1)->where('published', 1)->where('featured', 1) as $key => $product)
                                    <div class="caorusel-card my-5">
                                    <article class="list-product mb-30px">
                                <div class="img-block">
                                    <a href="{{ route('product', $product->slug) }}" class="thumbnail">
                                        <img class="first-img" src="{{ asset($product->thumbnail_img) }}" alt="{{ __($product->name) }} " style="width:100%"/>
                                        <img class="second-img" src="{{ asset($product->thumbnail_img) }}" alt="{{ __($product->name) }}" style="width:100%"/>
                                    </a>
                                    <div class="quick-view">
                                        <a class="quick_view" href="{{ route('product', $product->slug) }}" data-link-action="quickview" title="Quick view">
                                            <i class="ion-ios-search-strong"></i>
                                        </a>
                                    </div>
                                </div>
                                <ul class="product-flag">
                                @if($product->discount!=0 && $product->discount!='' && $product->discount!=null)                
                                @if ($product->discount_type=="percent")
                                @php
                                    $show_type="%";
                                @endphp
                            <li class="new">{{ $product->discount.$show_type }} off</li>
                                @else 
                                @php
                                    $show_type="₹";
                                @endphp
                                <li class="new">{{ $show_type.$product->discount }} off</li>
                                @endif
                                @endif
                                </ul>
                                <ul class="product-flag" style="float: right;">
                                  
                                    <li class="new1">New</li>
                                </ul>
                                <div class="product-decs1">
                                    <div class="product-decs2">
                                    <div class="star-rating star-rating-sm mt-1">
                                                    {{ renderStarRating($product->rating) }}
                                                </div>
                                        <div class="pricing-meta"  style="float:right;">
                                            <ul>
                                            @if(home_base_price($product->id) != home_discounted_base_price($product->id))
                                    <li class="current-price">{{ home_discounted_base_price($product->id) }}</li>
                                    <li class="old-price">{{ home_base_price($product->id) }}</li>

                                    @else 

                                    <li class="current-price">{{ home_discounted_base_price($product->id) }}</li>
                                    @endif
                                            </ul>
                                        </div>
                                    </div>
                                    

                                    <h2 ><a href="{{ route('product', $product->slug) }}" class="product-link">{{ __($product->name) }}</a></h2>
                                </div>
                                <div class="add-to-link">
                                    <ul>
                                    <li  class="compare_add cart">
                                            <a href="#" onclick="addToCompare({{ $product->id }})"><span class="iconify" data-icon="ion:git-compare" data-inline="false"></span></a>
                                        </li>
                                        <li style="float: none;" class="cart_li"><a class="cart-btn" href="#" onclick="showAddToCartModal({{ $product->id }})">ADD TO CART </a></li>
                                      <li class="favart_add">
                                            <a href="#" onclick="addToWishList({{ $product->id }});"><i class="ion-android-favorite-outline"></i></a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </article>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        @else
                        <div class="alert">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
  <strong>It seems we can't find what you're looking for.</strong> 
</div>
                        @endif
                    </div>
                </div>
            </div>
        </section>
    @endif


    <section class="@if (!isset($type)) gry-bg @endif pt-5">
        <div class="container">
            <h4 class="heading-5 strong-600 border-bottom pb-3 mb-4">
                @if (!isset($type))
                    {{ translate('New Arrival Products')}}
                @elseif ($type == 'top_selling')
                    {{ translate('Top Selling')}}
                @elseif ($type == 'all_products')
                    {{ translate('All Products')}}
                @endif
            </h4>
            <div class="product-list row gutters-5 sm-no-gutters">
                @php
                    if (!isset($type)){
                        $products = \App\Product::where('user_id', $shop->user->id)->where('is_approve', 1)->where('published', 1)->orderBy('created_at', 'desc')->paginate(24);
                    }
                    elseif ($type == 'top_selling'){
                        $products = \App\Product::where('user_id', $shop->user->id)->where('is_approve', 1)->where('published', 1)->orderBy('num_of_sale', 'desc')->paginate(24);
                    }
                    elseif ($type == 'all_products'){
                        $products = \App\Product::where('user_id', $shop->user->id)->where('is_approve', 1)->where('published', 1)->paginate(24);
                    }
                @endphp
                @if($products->count() > 0)
                @foreach ($products as $key => $product)
                <div class="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-6">
                                    <article class="list-product mb-30px">
                                <div class="img-block">
                                    <a href="{{ route('product', $product->slug) }}" class="thumbnail">
                                        <img class="first-img" src="{{ asset($product->thumbnail_img) }}" alt="{{ __($product->name) }} " style="width:100%"/>
                                        <img class="second-img" src="{{ asset($product->thumbnail_img) }}" alt="{{ __($product->name) }}" style="width:100%"/>
                                    </a>
                                    <div class="quick-view">
                                        <a class="quick_view" href="{{ route('product', $product->slug) }}" data-link-action="quickview" title="Quick view">
                                            <i class="ion-ios-search-strong"></i>
                                        </a>
                                    </div>
                                </div>
                                <ul class="product-flag">
                                @if($product->discount!=0 && $product->discount!='' && $product->discount!=null)                
                                @if ($product->discount_type=="percent")
                                @php
                                    $show_type="%";
                                @endphp
                            <li class="new">{{ $product->discount.$show_type }} off</li>
                                @else 
                                @php
                                    $show_type="₹";
                                @endphp
                                <li class="new">{{ $show_type.$product->discount }} off</li>
                                @endif
                                @endif
                                </ul>
                                <ul class="product-flag" style="float: right;">
                                  
                                    <li class="new1">New</li>
                                </ul>
                                <div class="product-decs1">
                                    <div class="product-decs2">
                                    <div class="star-rating star-rating-sm mt-1">
                                                    {{ renderStarRating($product->rating) }}
                                                </div>
                                        <div class="pricing-meta"  style="float:right;">
                                            <ul>
                                            @if(home_base_price($product->id) != home_discounted_base_price($product->id))
                                    <li class="current-price">{{ home_discounted_base_price($product->id) }}</li>
                                    <li class="old-price">{{ home_base_price($product->id) }}</li>

                                    @else 

                                    <li class="current-price">{{ home_discounted_base_price($product->id) }}</li>
                                    @endif
                                            </ul>
                                        </div>
                                    </div>
                                    

                                    <h2 ><a href="{{ route('product', $product->slug) }}" class="product-link">{{ __($product->name) }}</a></h2>
                                </div>
                                <div class="add-to-link">
                                    <ul>
                                    <li  class="compare_add cart">
                                            <a href="#" onclick="addToCompare({{ $product->id }})"><span class="iconify" data-icon="ion:git-compare" data-inline="false"></span></a>
                                        </li>
                                        <li style="float: none;" class="cart_li"><a class="cart-btn" href="#" onclick="showAddToCartModal({{ $product->id }})">ADD TO CART </a></li>
                                      <li class="favart_add">
                                            <a href="#" onclick="addToWishList({{ $product->id }});"><i class="ion-android-favorite-outline"></i></a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </article>
                                    </div>
                @endforeach
                @else
                <div class="alert">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
  <strong>It seems we can't find what you're looking for.</strong> 
</div>
                @endif
            </div>
            <div class="row">
                <div class="col">
                    <div class="products-pagination my-5">
                        <nav aria-label="Center aligned pagination">
                            <ul class="pagination justify-content-center">
                                {{ $products->links() }}
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
