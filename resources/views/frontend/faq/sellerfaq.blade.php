@extends('frontend.layouts.app')

@section('content')

<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css"> -->
    <style type="text/css">
        /* html,
        body {
            height: 100%;
            width: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            font-family: 'Quicksand';
        }
body { background: #fafafa; } */
      .faq {
            width: 100%;
    margin: auto;
        }

        .faqitem .header {
            padding: 15px 18px;
    background: #e2e2e2;
    color: #222533;
    display: flex;
    justify-content: space-between;
    align-items: center;
    cursor: pointer;
    border-radius: 5px;
    border-bottom: 0px solid white;
        }
    .faqitem {
        margin-top: 20px;
    }
    .p-4.bg-white h2 {
            border-bottom: 1px solid #ff7200;
            margin-bottom: 35px;
            padding-bottom: 15px;
        }

        .faqitem .header h4 {
            margin: 0;
            font-size: 17px;
        }

        .faqitem .header .fa.fa-minus {
            display: none;
        }

        .faqitem.jquery-accordion-active .fa.fa-minus {
            display: block;
        }

        .faqitem.jquery-accordion-active .fa.fa-plus {
            display: none;
        }

        .faqitem .content {
            padding: 15px;
    display: none;
    border: 1px solid #dedede;
    border-radius: 5px;
    background: #fdfbfb;
    border-top: 0;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
        }
        .title_faq{
            text-align: center;
    margin-bottom: 20px;
    font-size: 20px;
    font-weight: 800;
    border-bottom: 1px solid #ff7200;
    padding-bottom: 10px;
    color: #ff7200;
        }
	</style>
    <section class="gry-bg py-4">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="p-4 bg-white">
                        <?php
                        use App\tbl_faq;
                            $sellerfaq=tbl_faq::where('type', 'Seller');
                            if(request()->search_faq!=''){
                                $sellerfaq=$sellerfaq->where('question','like','%'.request()->search_faq.'%');    
                            }
                            $sellerfaq=$sellerfaq->get();
                        ?>
                       <h2>{{translate('Sellers FAQ')}}<input type="text" class="search_faq" id="search_faq" name="search_faq" value="{{request()->search_faq}}" placeholder="Search for a solution" style="float: right;font-size: 16px;background-color: #fff;background-clip: padding-box;border: 1px solid #ced4da;padding: .375rem .75rem;"></h2>
                        <div class="faq">
                        @if($sellerfaq->count() > 0)
                        @foreach($sellerfaq as $sellerfaq_dt)
                            <div class="faqitem">
                                <div class="header">
                                <h4>{{$sellerfaq_dt->question}}</h4>
                                <i class="fa fa-plus"></i>
                                <i class="fa fa-minus"></i>
                                </div>
                                <div class="content"><?php echo nl2br($sellerfaq_dt->answer); ?></div>
                            </div>
                        @endforeach
                        @else
                        <div class="faqitem">
                                <div class="header">
                                <h4>No Record Found</h4>
                                </div>
                            </div>
                        @endif
    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="{{ asset('assets/js/jquery-accordion.js')}}"></script>
    <script>
		$(".faq").accordion({
            questionClass: '.header',
            answerClass: '.content',
            itemClass: '.faqitem'
		});
	</script>
	 <script type="text/javascript">
$("#search_faq").keyup(function (e) {
    if (e.keyCode == 13) {
        // AJAX Call
        var search_val=$(this).val();
        window.location = '/sellerfaq?search_faq=' + search_val;
    }
});
</script>
@endsection
