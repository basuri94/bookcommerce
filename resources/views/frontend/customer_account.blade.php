@extends('frontend.layouts.app')

@section('content')
            <!-- Breadcrumb Area start -->
            <section class="breadcrumb-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="breadcrumb-content">
                                <h1 class="breadcrumb-hrading">Account Page</h1>
                                <ul class="breadcrumb-links">
                                    <li><a href="index.html">Home</a></li>
                                    <li>My Account</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Breadcrumb Area End -->
            <!-- account area start -->
            <div class="checkout-area mtb-60px">
            <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
              <div class="list-group">
                <a href="#" class="list-group-item active myaccount_li">
                <i class="fa fa-list" aria-hidden="true"></i>&nbsp;My Order
                </a>
                <a href="#" class="list-group-item myaccount_li">
                <i class="fa fa-user" aria-hidden="true"></i>&nbsp;My Profile
                </a>
                <a href="#" class="list-group-item myaccount_li">
                <i class="fa fa-star" aria-hidden="true"></i>&nbsp;Rate Your Purchase
                </a>
                
                <a href="#" class="list-group-item myaccount_li">
                <i class="fa fa-heart" aria-hidden="true"></i>&nbsp;My Wishlist
                </a>
                <a href="#" class="list-group-item  myaccount_li">
                <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;Help & Support
                </a>
                
              </div>
            </div>
            
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
            <div class="bhoechie-tab-content active">
            
                <!-- Regions -->
                <center>
				  
	            </center>
                </div>
                <div class="bhoechie-tab-content">
				<!-- Sectors -->
                <center>
				                    
				</center>
                </div>
    
                
                
                <!-- Finances -->
                <div class="bhoechie-tab-content">
					<center>
					
                </center>
                </div>
               
                
                <div class="bhoechie-tab-content">
                <!-- Resources -->
                <center>
				             
                </center>
                </div>
    
                <div class="bhoechie-tab-content">
                <!-- Resources -->
                <center>
				
                </center>
                </div>
                
                <!-- Training -->                            
                <div class="bhoechie-tab-content">
                    <center>
					
                    </center>
                </div>
                <!-- CRM -->                            
                <div class="bhoechie-tab-content">
                    <center>
					
                    </center>
                </div>
    
                <!-- hotel search -->
                <div class="bhoechie-tab-content">
                    <center>
                      <h1 class="glyphicon glyphicon-home" style="font-size:12em;color:#55518a"></h1>
                      <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                      <h3 style="margin-top: 0;color:#55518a">Hotel Directory</h3>
                    </center>
                </div>
               
                
            </div>
        </div>
  </div>
</div>
            </div>
            <!-- account area end -->
            <script>
                $(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});

$(function () {
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
});

$(document).ready(function() {
          $("#slider").slider({
              animate: true,
              value:1,
              min: 0,
              max: 100,
              step: 1,
              slide: function(event, ui) {
                  update(1,ui.value); //changed
              }
          });

          $("#slider2").slider({
              animate: true,
              value:0,
              min: 0,
              max: 100,
              step: 1,
              slide: function(event, ui) {
                  update(2,ui.value); //changed
              }
          });
          
          $("#slider3").slider({
              animate: true,
              value:0,
              min: 0,
              max: 100,
              step: 1,
              slide: function(event, ui) {
                  update(3,ui.value); //changed
              }
          });
          
          $("#slider4").slider({
              animate: true,
              value:0,
              min: 0,
              max: 100,
              step: 1,
              slide: function(event, ui) {
                  update(4,ui.value); //changed
              }
          });
          
    	  $("#slider5").slider({
              animate: true,
              value:0,
              min: 0,
              max: 100,
              step: 1,
              slide: function(event, ui) {
                  update(5,ui.value); //changed
              }
          });
          
		  $("#slider6").slider({
              animate: true,
              value:0,
              min: 0,
              max: 100,
              step: 1,
              slide: function(event, ui) {
                  update(6,ui.value); //changed
              }
          });

		  $("#slider7").slider({
              animate: true,
              value:0,
              min: 0,
              max: 100,
              step: 1,
              slide: function(event, ui) {
                  update(7,ui.value); //changed
              }
          });

		  $("#slider8").slider({
              animate: true,
              value:0,
              min: 0,
              max: 100,
              step: 1,
              slide: function(event, ui) {
                  update(8,ui.value); //changed
              }
          });

		  $("#slider9").slider({
              animate: true,
              value:0,
              min: 0,
              max: 100,
              step: 1,
              slide: function(event, ui) {
                  update(9,ui.value); //changed
              }
          });

		  $("#slider10").slider({
              animate: true,
              value:0,
              min: 0,
              max: 100,
              step: 1,
              slide: function(event, ui) {
                  update(10,ui.value); //changed
              }
          });

		  $("#slider11").slider({
              animate: true,
              value:0,
              min: 0,
              max: 100,
              step: 1,
              slide: function(event, ui) {
                  update(11,ui.value); //changed
              }
          });
          
          //Added, set initial value.
          $("#amount").val(0);
          $("#amount4").val(0);
          $("#amount2").val(0);
          $("#amount3").val(0);
          $("#amount5").val(0);
          $("#amount6").val(0);
          $("#amount7").val(0);
          $("#amount8").val(0);
          $("#amount9").val(0);
          $("#amount10").val(0);
          $("#amount11").val(0);
          $("#amount-label").text(0);
          $("#amount2-label").text(0)
          $("#amount3-label").text(0);
          $("#amount4-label").text(0);
          $("#amount5-label").text(0);
          $("#amount6-label").text(0);
          $("#amount7-label").text(0);
          $("#amount8-label").text(0);
          $("#amount9-label").text(0);
          $("#amount10-label").text(0);
          $("#amount11-label").text(0);



          
          update();
      });

      //changed. now with parameter
      function update(slider,val) {
        //changed. Now, directly take value from ui.value. if not set (initial, will use current value.)
        var $amount = slider == 1?val:$("#amount").val();
        var $amount2 = slider == 3?val:$("#amount2").val();
        var $amount3 = slider == 4?val:$("#amount3").val();
        var $amount4 = slider == 2?val:$("#amount4").val();
        var $amount5 = slider == 5?val:$("#amount5").val();
        var $amount6 = slider == 6?val:$("#amount6").val();
        var $amount7 = slider == 7?val:$("#amount7").val();
        var $amount8 = slider == 8?val:$("#amount8").val();
        var $amount9 = slider == 9?val:$("#amount9").val();
        var $amount10 = slider == 10?val:$("#amount10").val();
        var $amount11 = slider == 11?val:$("#amount11").val();


        /* commented
        $amount = $( "#slider" ).slider( "value" );
        $duration = $( "#slider2" ).slider( "value" );
         */

         $( "#amount" ).val($amount);
         $( "#amount-label" ).text($amount);
         $( "#amount2" ).val($amount2);
         $( "#amount2-label" ).text($amount2);
         $( "#amount3" ).val($amount3);
         $( "#amount3-label" ).text($amount3);
         $( "#amount4" ).val($amount4);
         $( "#amount4-label" ).text($amount4);
         $( "#amount5" ).val($amount5);
         $( "#amount5-label" ).text($amount5);
         $( "#amount6" ).val($amount6);
         $( "#amount6-label" ).text($amount6);
         $( "#amount7" ).val($amount7);
         $( "#amount7-label" ).text($amount7);
         $( "#amount8" ).val($amount8);
         $( "#amount8-label" ).text($amount8);
         $( "#amount9" ).val($amount9);
         $( "#amount9-label" ).text($amount9);
         $( "#amount10" ).val($amount10);
         $( "#amount10-label" ).text($amount10);
         $( "#amount11" ).val($amount11);
         $( "#amount11-label" ).text($amount11);





         $('#slider a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
         $('#slider2 a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount4+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
         $('#slider3 a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount2+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
         $('#slider4 a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount3+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
         $('#slider5 a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount5+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
         $('#slider6 a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount6+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
         $('#slider7 a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount7+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
         $('#slider8 a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount8+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
         $('#slider9 a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount9+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
         $('#slider10 a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount10+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
         $('#slider11 a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount11+' <span class="glyphicon glyphicon-chevron-right"></span></label>');




      }

            </script>
           @stop