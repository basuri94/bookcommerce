@extends('frontend.layouts.app')
@section('robots'){{  translate('index') }}@stop
@section('content')

<section class="gry-bg py-4 profile">
    <div class="container">
        <div class="row cols-xs-space cols-sm-space cols-md-space">
            <div class="col-lg-3 d-none d-lg-block">
                @if(Auth::user()->user_type == 'seller')
                @include('frontend.inc.seller_side_nav')
                @elseif(Auth::user()->user_type == 'customer')
                @include('frontend.inc.customer_side_nav')
                @endif
            </div>

            <div class="col-lg-9">
                <div class="main-content">
                    <!-- Page title -->
                    <div class="page-title">
                        <div class="row align-items-center">
                            <div class="col-md-6 col-12">
                                <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                                    {{ translate('Your Digital Product')}}
                                </h2>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="float-md-right">
                                    <ul class="breadcrumb">
                                        <li><a href="{{ route('home') }}">{{ translate('Home')}}</a></li>
                                        <li><a href="{{ route('dashboard') }}">{{ translate('Dashboard')}}</a></li>
                                        <li class="active"><a
                                                href="{{ route('digital_purchase_history.index') }}">{{ translate('Digital Product')}}</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card no-border mt-4">
                        <div>
                            <table class="table table-sm table-hover table-responsive-md">
                                <thead>
                                    <tr>
                                        <th>{{ translate('Sl#')}}</th>
                                        <th>{{ translate('Product')}}</th>
                                        <th>{{translate('View license')}}</th>
                                        @if (\App\BusinessSetting::where('type', 'conversation_system')->first()->value == 1)
                                        <th>{{translate('Message')}}</th>
                                         @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $count=0;
                                    @endphp
                                    @foreach ($orders as $key => $order_id)
                                    @php
                                    $count++;
                                    $order = \App\OrderDetail::find($order_id->id);
                                    @endphp
                                    <tr>
                                        <td>{{  $count }}</td>
                                        <td><a
                                                href="{{ route('product', $order->product->slug) }}">{{ $order->product->name }}</a>
                                        </td>
                                        {{-- <td><a class="btn btn-styled btn-base-1" href="{{route('digitalproducts.download', encrypt($order->product->id))}}">{{ translate('Download')}}</a>
                                        </td> --}}
                                        @if(!empty($order->license_key))
                                        <td><button type="button" class="btn btn-primary btn-sm" title="Click to view liscense key"
                                                onclick="showlicense('{{$order->id}}');"><i class="fa fa-eye">
                                                </i></button></td>
                                        @else

                                        <td style="color:red">Pending..</td>
                                        @endif
                                        @if (\App\BusinessSetting::where('type', 'conversation_system')->first()->value == 1)

                                        <td><button class="btn" style=" background: -webkit-linear-gradient(0deg,#ff5e62 0%,#ff934b 100%);color:white;" onclick="show_chat_modal({{$order->product->id}})">{{ translate('Send Message')}}</button></td>
                                        
                                    @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="pagination-wrapper py-4">
                        <ul class="pagination justify-content-end">
                            {{ $orders->links() }}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="chat_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
        <div class="modal-content position-relative">
            <div class="modal-header">
                <h5 class="modal-title strong-600 heading-5">{{ translate('Any query about this product')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="" action="{{ route('conversations.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" id="product_id" name="product_id" value="">
                <div class="modal-body gry-bg px-3 pt-3">
                    <div class="form-group">
                        <input type="text" class="form-control mb-3" name="title" readonly id="product_name" value="" placeholder="{{ translate('Product Name') }}" required>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" rows="8" name="message" id="message" required placeholder="{{ translate('Your Question') }}"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">{{ translate('Cancel')}}</button>
                    <button type="submit" class="btn btn-base-1 btn-styled">{{ translate('Send')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>



@endsection


@section('script')

<script>
    function showlicense(orderdetailsid){

             
               

                $.ajax({
                type: 'post',
                url: '{{ route("purchase_history.showlicense") }}',
                data: {_token:'{{ @csrf_token() }}',orderdetailsid:orderdetailsid},

                dataType: 'json',
                success: function (response) {
                  
                    $.confirm({
                    boxWidth: '50%',
                    boxHeight: '50%',
                        type: 'green',
                        icon:'fa fa-check',
                        title: 'License Key',
                        content: response.html,
                       
                        useBootstrap: false,
                        smoothContent:true,
                        buttons: {
                        ok: function () {
                            
                        },

                        }
                        });
                

                },
                error: function (jqXHR, textStatus, errorThrown) {

                var msg = "<strong>Failed to Load data.</strong><br/>";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                } else {
                msg += "Error(s):<strong><ul>";
                $.each(jqXHR.responseJSON['errors'], function (key, value) {
                msg += "<li>" + value + "</li>";
                });
                msg += "</ul></strong>";
                }
                }
                Swal.fire({
                title: 'Error',
                icon: 'error',
                html:msg,

                showCloseButton: true,
                showCancelButton: true,
                focusConfirm: false,


                });
                // showFrontendAlert('error', msg);

                }
                });
}

function viewlicense(){
    var x = document.getElementById("licenseInput");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
$('#btnView').hide();
}

function show_chat_modal(productid){
    $.post('{{ route('digitalproducts.conversation') }}', {_token:'{{ csrf_token() }}', productid:productid}, function(data){
                if(data.status == 1){
                    
                    $('#chat_modal').modal('show');
                 
                     $('#product_name').val(data.product.name)
                     $('#product_id').val(data.product.id)
                    // $('#message').val()
                }
                else{
                    showFrontendAlert('danger', 'Something went wrong');
                }
            });



            @if (Auth::check())
                $('#chat_modal').modal('show');
                $('#chat_modal').modal('show');
            @else
                $('#login_modal').modal('show');
            @endif
        }
</script>



<script type="text/javascript">



</script>
@endsection