@extends('frontend.layouts.app')

@section('content')
<style>
  img.first-img {
      min-height: 280px;
    height: 100% !important;
    width: 100%;
}
img.second-img {
    min-height: 280px;
    height: 100% !important;
    width: 100%;
}
    .banner-wrapper img {
    margin-bottom: 3%;
    }
    .feature-slider-item .img-block {
    float: left;
    width: 120px;
    margin-right: 10px;
}
.blog-post-content {
    padding-left: 30px;
    background: #efefefa8;
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    right: 0;
    -webkit-transition: all 300ms linear;
    -moz-transition: all 300ms linear;
    -ms-transition: all 300ms linear;
    -o-transition: all 300ms linear;
    transition: all 300ms linear;
}
.sc_trust li:last-child {
    
    border-right:none;
}
.feature-slider-item .img-block {
    float: left;
    width: 50% !important;
    margin-right: 10px;
}
.feature-slider-item .img-block a img{
    float: left;
    min-height: 100% !important;
    margin-right: 10px;
}
</style>
<!-- Slider Arae Start -->
<div class="slider-area">
    <div class="slider-active-3 owl-carousel slider-hm8 owl-dot-style">
        @foreach (\App\Slider::where('published', 1)->get() as $key => $slider)
        <!-- Slider Single Item Start -->
        <a href="{{ $slider->link }}" target="_blank">
            <div class="slider-height-6 d-flex align-items-start justify-content-start bg-img"
            style="background-image: url('')">
           <img src="{{ asset($slider->photo) }}">
           <!--<div class="container">-->
           <!--                 <div class="slider-content-5 slider-animated-1 text-right">-->
           <!--                     <span class="animated">FRESH FRUIT-NATURAL</span>-->
           <!--                     <h1 class="animated">-->
           <!--                         Pro Skin Whitening <br />-->
           <!--                         Face Creams-->
           <!--                     </h1>-->
           <!--                     <a href="javascript:" class="shop-btn animated">SHOP NOW</a>-->
           <!--                 </div>-->
           <!--             </div> -->
        </div>
</a>
        @endforeach

    </div>
</div>
<!-- Slider Arae End -->

<br><br>

<!-- Category Area Start -->
<section class="categorie-area">
    <div class="container">
        <div class="row">
            @foreach (\App\Category::where('top', 1)->get()->take(9) as $key => $category)
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 category-list ">
                <div class="category-thumb">
                    <a href="{{ route('products.category', $category->slug) }}">
                        <img src="{{ asset($category->banner) }}" alt="{{ __($category->name) }}" />
                    </a>
                </div>
                <div class="desc-listcategoreis">
                    <div class="name_categories">
                        <h4>{{ __($category->name) }}</h4>
                    </div>
                    <span class="number_product" style="text-transform:none;">{{$category->home_text}}</span>
                    <a href="{{ route('products.category', $category->slug) }}"> Shop Now <i
                            class="fa fa-chevron-right"></i></a>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</section>
<!-- Category Area End  -->
<br><br>
<!-- Best Sells Area Start -->

<section class="best-sells-area">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Section Title -->
                <div class="section-title">
                    <h2>Trending Product</h2>
                    <!--<p>Add products to weekly line up</p>-->
                    <a href="{{ route('products.trending') }}" class="view_all">View All</a>
                </div>

                <!-- Section Title -->
            </div>
        </div>
        <!-- Best Sell Slider Carousel Start -->
        <div class="best-sell-slider  owl-carousel owl-nav-style owl-nav-style-2">
            <!-- Single Item -->

            <?php
                    //$det=filter_products(\App\Product::where('published', 1)->where('featured', '1'))->limit(12)->get(); 
                    //echo json_encode($det); 
                    $productlist=filter_products(\App\Product::where('is_approve', 1)->where('published', 1)->where('featured', '1'))->limit(12)->get() ;
                    ?>
                   @if(count($productlist)>0)
            @foreach ($productlist as
            $key => $product)
            <div class="list-product-2">

                <article class="list-product mb-30px">
                    <div class="img-block">
                        <a href="{{ route('product', $product->slug) }}" class="thumbnail">
                            <img class="first-img" src="{{ asset($product->thumbnail_img) }}"
                                alt="{{ __($product->name) }}" />
                            <img class="second-img" src="{{ asset($product->thumbnail_img) }}"
                                alt="{{ __($product->name) }}" />
                        </a>
                        <div class="quick-view">
                            <a class="quick_view" href="{{ route('product', $product->slug) }}"
                                data-link-action="quickview" title="Quick view">
                                <i class="ion-ios-search-strong"></i>
                            </a>
                        </div>
                    </div>
                    <ul class="product-flag">
                     
                    @if($product->discount!=0 && $product->discount!='' && $product->discount!=null)                
    @if ($product->discount_type=="percent")
    @php
        $show_type="%";
    @endphp
<li class="new">{{ $product->discount.$show_type }} off</li>
    @else 
    @php
        $show_type="₹";
    @endphp
    <li class="new">{{ $show_type.$product->discount }} off</li>
    @endif
    @endif



                        
                    </ul>
                    <div class="product-decs1">
                        <div class="product-decs2">
                        <div class="star-rating star-rating-sm mt-1">
                                                    {{ renderStarRating($product->rating) }}
                                                </div>
                            <div class="pricing-meta" style="float:right;">
                                <ul>
                                    @if(home_base_price($product->id) != home_discounted_base_price($product->id))
                                    <li class="current-price">{{ home_discounted_base_price($product->id) }}</li>
                                    <li class="old-price">{{ home_base_price($product->id) }}</li>

                                    @else 

                                    <li class="current-price">{{ home_discounted_base_price($product->id) }}</li>
                                    @endif
                                   

                                </ul>
                            </div>
                        </div>


                        <h2><a href="{{ route('product', $product->slug) }}"
                                class="product-link">{{ __($product->name) }}</a></h2>
                    </div>
                    <div class="add-to-link">
                        <ul>

                            <li class="favart_add">
                                <a href="javascript:" onclick="addToWishList({{ $product->id }});"><i
                                        class="ion-android-favorite-outline"></i></a>
                            </li>
                            <li style="float: none; margin-left:6%;" class="cart_li"><a class="cart-btn"
                                    href="javascript:" onclick="showAddToCartModal({{ $product->id }})">ADD TO CART </a>
                            </li>
                            <li class="compare_add cart">
                                <a href="javascript:" onclick="addToCompare({{ $product->id }})"><span class="iconify"
                                        data-icon="ion:git-compare" data-inline="false"></span></a>
                            </li>
                        </ul>
                    </div>
                </article>


            </div>
            @endforeach
@endif


            <!-- Best Sell Slider Carousel End -->
        </div>
</section>
<!-- Best Sell Area End -->
<!-- Banner Area Start -->
<div class="banner-3-area1">
    <div class="container">
        <div class="row">
            @php
            $image1=\App\Banner::where('position', 2)->where('published', 1)->where('type', 1)->first();
            $image2=\App\Banner::where('position', 2)->where('published', 1)->where('type', 2)->first();
            $image3=\App\Banner::where('position', 2)->where('published', 1)->where('type', 3)->first();
            $image4=\App\Banner::where('position', 2)->where('published', 1)->where('type', 4)->first();

            @endphp
            @if(!empty($image1))
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mb-res-xs-30">
                <div class="banner-wrapper zoom mb-30px">
                    <a href="javascript:"><img src="{{asset($image1->photo)}}" alt="" style="height: 660px;" /></a>
                </div>

            </div>
            @endif
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

                <div class="row">
                    @if(!empty($image2))
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="banner-wrapper zoom mb-30px">
                            <a href="javascript:"><img src="{{asset($image2->photo)}}" alt="" /></a>
                        </div>
                    </div>
                    @endif
                    @if(!empty($image3))
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="banner-wrapper zoom mb-30px">
                            <a href="javascript:"><img src="{{asset($image3->photo)}}" alt="" /></a>
                        </div>
                    </div>
                    @endif
                </div>


                @if(!empty($image4))
                <div class="banner-wrapper zoom">
                    <a href="javascript:"><img src="{{asset($image4->photo)}}" alt="" /></a>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<!-- Banner Area End -->
<!-- Banner Area Start -->
@foreach (\App\Banner::where('position', 1)->where('published', 1)->get() as $key => $banner)
<div class="banner-3-area1">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-res-xs-30">
                <div class="banner-wrapper zoom mb-30px">

                    <a href="{{ $banner->url }}"><img src="{{ asset($banner->photo) }}"
                            alt="{{ env('APP_NAME') }} promo" /></a>

                </div>

            </div>

        </div>
    </div>
</div>
@endforeach
<!-- Banner Area End -->
<!-- Feature Area Start -->
<section class="feature-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Section Title -->
                <div class="section-title">
                    <h2>Top Categories</h2>
                    <!--<p>Add products to weekly line up</p>-->
                </div>
                <!-- Section Title -->
            </div>
        </div>
        <!-- Feature Slider Start -->
        <div class="feature-slider owl-carousel owl-nav-style">
            <!-- Single Item -->
            @foreach (\App\HomeCategory::where('status',1)->get() as $category)
            <div class="feature-slider-item">
                <article class="list-product">
                    <div class="img-block">
                        <a href="{{ route('products.category', $category->category->slug) }}"
                            class="{{ __($category->name) }}">
                            <img class="first-img" style="height: 145px;" src="{{ asset($category->category->icon) }}"
                                alt="{{ __($category->name) }}" />
                            <img class="second-img" style="height: 145px; src="{{ asset($category->category->icon) }}" alt="" />
                            <!-- <img class="first-img" src="{{ asset($category->category->banner) }}" alt="{{ __($category->name) }}" />
                                        <img class="second-img" src="{{ asset($category->banner) }}" alt="{{ __($category->name) }}" /> -->
                        </a>

                    </div>
                    <div class="product-decs">

                        <h2><a href="{{ route('products.category', $category->category->slug) }}" class="product-link"
                                style="color:#373737;font-weight: 600;font-size: 15px;">{{ __($category->name) }}</a>
                        </h2>
                        @if(count($category->category->subcategories)>0)
                        <?php $sub=1; ?>
                        @foreach ($category->category->subcategories as $key=> $subcategory)
                        @if ($key==4)
                          @php  break; @endphp
                        @endif
                        <?php $sub=1; ?>
                        <div class="rating-product">
                            <?php if($sub < 4){ ?>
                            <a href="{{ route('products.subcategory', $subcategory->slug) }}" style="color: #373737;">
                            {{ substr($subcategory->name, 0, 15)}}.. </a>
                        </div>
                        <?php } $sub++; ?>
                        @endforeach
                        @endif
                        {{-- <div class="rating-product">
                                        Nokia
                                    </div>
                                    <div class="rating-product">
                                        Samsung
                                    </div> --}}


                        <div class="pricing-meta">
                            <ul>
                                <li class="old-price not-cut" style="color:#ff7200;border-bottom:1px solid #ff7200;"> <a
                                        style="color: #ff7200;"
                                        href="{{ route('products.category', $category->category->slug) }}">View More
                                        ..</a></li>
                            </ul>
                        </div>
                    </div>
                </article>

            </div>
            @endforeach


        </div>
        <!-- Feature Slider End -->
    </div>
</section>
<!-- Feature Area End -->
<!-- Banner Area Start -->
@php
 $flash_deal = \App\FlashDeal::where('status', 1)->where('featured', 1)->first();
 
//echo strtotime(date('2020-09-15 14:15:30')); die; echo "<br>";
// echo $flash_deal->start_date;die;
//echo date('Y/m/d', $flash_deal->start_date);die;
// echo strtotime(date('Y-m-d H:i'));echo ",";
// echo $flash_deal->start_date;die;
@endphp
@if((!empty($flash_deal)) && strtotime(date('Y-m-d H:i:s')) <= $flash_deal->start_date )

    <div class="banner-3-area1">
        <div class="container">
            @foreach ($flash_deal->flash_deal_products as $key => $flash_deal_product)
            @php
            $product = \App\Product::find($flash_deal_product->product_id);
            @endphp
            @if ($product != null && $product->published != 0)
     
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-res-xs-30">
                    <div class="banner-wrapper">
                        <a href="javascript:"><img src="{{asset($flash_deal->banner)}}" alt="" /></a>
                        <div class="container" style="    margin-top: -25%;margin-bottom: 12%;margin-left: 12%;">
                            <div class="slider-content-5 slider-animated-1 text-left">

                                <h1 class="animated">
                                    {{$flash_deal->title}}<br />
                                    <div class="clockdiv">

                                        <div data-countdown="{{  date('Y/m/d H:i:s', $flash_deal->start_date)}}"><span
                                                class="cdown day">187 <p>Days</p></span> <span class="cdown hour">10 <p>
                                                    Hours</p></span> <span class="cdown minutes">25 <p>Mins</p></span>
                                            <span class="cdown second">13 <p>Sec</p></span></div>
                                    </div>
                                </h1>
                               
                                    <a href="{{ route('product', $product->slug) }}" class="shop-btn animated"
                                        style="background-color:#ff7200;">SHOP NOW</a>
                                 
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            @endif
            @endforeach
        </div>
    </div>


    @endif
    {{-- <div class="container my-4">

      
    
        <div id="carouselExample1" class="carousel slide z-depth-1-half" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(45).jpg" alt="First slide">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(46).jpg" alt="Second slide">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(47).jpg" alt="Third slide">
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExample1" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExample1" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
    
       
    
      </div> --}}
           @php 

$productreview=\App\Review::where('status',1)->get() ;
@endphp
@if(count($productreview)>0)
    <section class="blog-area mb-30px mt-30">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Section title -->
                    <div class="section-title">
                        <h2>Testimonial</h2>
                        <p>Present posts in a best way to highlight interesting moments of your blog.</p>
                    </div>
                    <!-- Section title -->
                </div>
            </div>
            <!-- Blog Slider Start -->
            <div class="blog-slider-active owl-carousel owl-nav-style">
                <!-- single item -->
             
                @foreach ($productreview as $review)
                <article class="blog-post">
                    <div class="blog-post-top">
                        <div class="blog-img">
                            <img src="{{asset($review->product->thumbnail_img)}}" width="407px" height="250px"
                                alt="Product Image" />
                        </div>
                    </div>
                    <div class="blog-post-content d-flex">
                        <div class="blog-post-content-cell">
                            <a href="{{ route('products.category',$review->product->category->slug) }}"
                                class="blog-meta">{{$review->product->category->name}}</a>
                            <h4 class="blog-post-heading"><a href="{{ route('products.category',$review->product->category->slug) }}">
                                    <div class="rating-product" style="float:left;">
                                        @for ($i = 0; $i <$review->rating; $i++)
                                            <i class="ion-android-star"></i>
                                            @endfor

                                    </div>


                                </a></h4>
                            <p class="blog-text">
                                {{$review->comment}}
                            </p>
                            {{-- <a class="read-more-btn" href="blog-single-left-sidebar.html"> Read More <i class="ion-android-arrow-dropright-circle"></i></a> --}}
                        </div>
                    </div>
                </article>
                @endforeach

            </div>
            <!-- Blog Slider Start -->
        </div>
    </section>
    @endif
    <!-- Blog Area End -->
    <!-- Brand area start -->
    <div class="brand-area">
        <div class="container">
            <div class="brand-slider owl-carousel owl-nav-style owl-nav-style-2">
                @foreach (\App\Brand::where('top', 1)->get() as $brand)
                <div class="brand-slider-item">
                    <a href="{{ route('products.brand', $brand->slug) }}"><img src="{{ asset($brand->logo) }}"
                            alt="{{ __($brand->name) }}" /></a>
                </div>
                @endforeach
                {{-- <div class="brand-slider-item">
                            <a href="javascript:"><img src="assets/images/brand-logo/2.jpg" alt="" /></a>
                        </div>
                        <div class="brand-slider-item">
                            <a href="javascript:"><img src="assets/images/brand-logo/3.jpg" alt="" /></a>
                        </div>
                        <div class="brand-slider-item">
                            <a href="javascript:"><img src="assets/images/brand-logo/4.jpg" alt="" /></a>
                        </div>
                        <div class="brand-slider-item">
                            <a href="javascript:"><img src="assets/images/brand-logo/5.jpg" alt="" /></a>
                        </div>
                        <div class="brand-slider-item">
                            <a href="javascript:"><img src="assets/images/brand-logo/1.jpg" alt="" /></a>
                        </div>
                        <div class="brand-slider-item">
                            <a href="javascript:"><img src="assets/images/brand-logo/2.jpg" alt="" /></a>
                        </div> --}}
            </div>
        </div>
    </div>
    <!-- Brand area end -->
    <div class="sc_trust">
        <div class="wrapper">
            <h2 style="color:black;margin-bottom:20px;"><i></i>TheLocal2Vocal Assurance<i></i></h2>
            <ul>
            @foreach (\App\band_of_trust::get() as $band)
            <li><img src="{{ asset($band->image_name) }}"><span>{{ $band->title }}</span></li>
                @endforeach
          
            {{-- <li><img src="{{asset('frontend/img/dkt_sprite_v3.png')}}"><span>Fast doorstep delivery </span></li>
                <li><img src="{{asset('frontend/img/Artboard 3-100.jpg')}}"><span> Easy Return</span></li>
                <li><img src="{{asset('frontend/img/Artboard 4-100.jpg')}}"><span>Authentic products</span></li>
                <li class="sc_speedy_delivery"><img src="{{asset('frontend/img/Artboard 2-100.jpg')}}"><span>Verified
                        Sellers</span></li> --}}
            </ul>
        </div>
    </div>
    <form class="form-inline" method="POST" action="{{ route('subscribers.store') }}">
        @csrf
        <div class="banner-3-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-res-xs-30 newsletter_div">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 news_icon">
                            <i class="fa fa-envelope-open-o news_icon_icon" aria-hidden="true"></i>
                            <p class="news_icon_text">{{ translate('SIGN UP FOR NEWSLETTER') }}</p>
                            <!-- <img src="/frontend/img/newsletter.png"> -->
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 nwes_desc">
                            <p style="padding-top: 6%;color:#8f9396;">
                                {{ translate('Subscribe to our newsletter for all the latest updates.') }}
                            </p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 newsletter_input_div">
                            <input class="newsletter_input" style="font-family: FontAwesome,sans-serif;"
                                placeholder="{{ translate('Your Email...') }}" name="email" type="email"
                                class="search_front" required />
                            <input type="submit" class="newsletter_button" value="Subscribe">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    @stop