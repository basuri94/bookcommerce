@extends('frontend.layouts.app')

@section('content')
<section class="gry-bg py-4 profile">
    <div class="container">
        <div class="row cols-xs-space cols-sm-space cols-md-space">
            <div class="col-lg-3 d-none d-lg-block">
                @if(Auth::user()->user_type == 'seller')
                        @include('frontend.inc.seller_side_nav')
                    @elseif(Auth::user()->user_type == 'customer')
                        @include('frontend.inc.customer_side_nav')
                    @endif
            </div>

            <div class="col-lg-9">
                <div class="main-content">
                    <!-- Page title -->
                    <div class="page-title">
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                                    {{ translate('Add Your Product')}}
                                </h2>
                            </div>
                            <div class="col-md-6">
                                <div class="float-md-right">
                                    <ul class="breadcrumb">
                                        <li><a href="{{ route('home') }}">{{ translate('Home')}}</a></li>
                                        <li><a href="{{ route('dashboard') }}">{{ translate('Dashboard')}}</a></li>
                                        <li><a href="{{ route('seller.products') }}">{{ translate('Products')}}</a></li>
                                        <li class="active"><a href="{{ route('seller.products.upload') }}">{{ translate('Add New Product')}}</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form class="" action="{{route('refund.store')}}" method="POST" enctype="multipart/form-data" id="choice_form">
                        @csrf
                    <input type="hidden" name="product_id" value="{{$getOrderDet->product_id}}">
                    <input type="hidden" name="amount" value="{{$getOrderDet->price+$getOrderDet->tax+$getOrderDet->shipping_cost}}">
                    <input type="hidden" name="order_id" value="{{$getOrderDet->id}}">
                    {{-- <input type="hidden" name="type_select" value="{{$decrypttype}}"/> --}}
                        <div class="form-box bg-white mt-4">
                            <div class="form-box-title px-3 py-2">
                                {{ translate('General')}}
                            </div>
                            <div class="form-box-content p-3">
                           
                         <div class="row">
                                <div class="col-md-2">
                                    <label>{{ translate('Type')}} <span class="required-star">*</span></label>
                                </div>
                                <div class="col-md-10">
                                    <select name="type_select" id="type_select" class="form-control mb-3">
                                        <option value="Return">Return/Refund</option>
                                        <option value="Replacement">Replacement</option>
                                    </select>
                                    </div>
                            </div> 
                            @if($getOrderDet->payment_type=="online")
                            <div class="row type_refund" style="display: none;">
                                <div class="col-md-2">
                                    <label>{{ translate('Refund Via')}} <span class="required-star">*</span></label>
                                </div>
                                <div class="col-md-10">
                                    <select name="type_refund" id="type_refund" class="form-control mb-3">
                                    <option value="">Select</option>
                                    <option value="Wallet">Wallet</option>
                                        <option value="Bank">Bank</option>
                                    </select>
                                    </div>
                            </div> 
                            @endif
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>{{ translate('Product Name')}} <span class="required-star">*</span></label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control mb-3" disabled value="{{$getOrderDet->name}}" name="product" placeholder="{{ translate('Product Name')}}" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>{{ translate('Order Code')}} <span class="required-star">*</span></label>
                                    </div>
                                    <div class="col-md-10">
                                    <input type="text" class="form-control mb-3" disabled name="order" value="{{$getOrderDet->code}}" placeholder="{{ translate('Order Code')}}" required>
                                    </div>
                                </div>
                             
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>{{ translate('Amount')}} <span class="required-star">*</span></label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control mb-3" readonly value="{{$getOrderDet->price+$getOrderDet->tax+$getOrderDet->shipping_cost}}" name="amount" placeholder="{{ translate('Amount') }}" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>{{ translate('Reason')}} <span class="required-star">*</span></label>
                                    </div>
                                    <div class="col-md-10">
                                        <textarea type="text" class="form-control mb-3" name="reason"  required></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-md-2">
                                                <label>{{ translate('Attach Image')}} <span class="required-star">*</span></label>
                                            </div>
                                            <div class="col-md-10">
                                                <input type="file" name="photos" id="photos"  required/>
                                                
                                            </div>
                                </div>

                               
                            </div>
                        </div>
                       
                      <div class="form-box mt-4 text-right">
                            <button type="submit" class="btn btn-styled btn-base-1">{{  translate('Save') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('script')
    <script type="text/javascript">
    var type_select1=$("#type_select").val();
          if(type_select1=="Return"){
$(".type_refund").show();
$("#type_refund").val('');
          }else{
            $(".type_refund").hide();  
            $("#type_refund").val('');       
          }
      $("#type_select").change(function(){
          var type_select=$(this).val();
          if(type_select=="Return"){
$(".type_refund").show();
$("#type_refund").val('');
          }else{
            $(".type_refund").hide();  
            $("#type_refund").val('');       
          }
      });
    </script>

@endsection

