@extends('frontend.layouts.app')

@section('content')

<section class="gry-bg py-4 profile">
    <div class="container">
        <div class="row cols-xs-space cols-sm-space cols-md-space">
            <div class="col-lg-3 d-none d-lg-block">
                @if(Auth::user()->user_type == 'seller')
                @include('frontend.inc.seller_side_nav')
                @elseif(Auth::user()->user_type == 'customer')
                @include('frontend.inc.customer_side_nav')
                @endif
            </div>

            <div class="col-lg-9">
                <div class="main-content">
                    <!-- Page title -->
                    <div class="page-title">
                        <div class="row align-items-center">
                            <div class="col-md-6 col-12">
                                <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                                    {{ translate('Sent Return/Refund Request')}}
                                </h2>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="float-md-right">
                                    <ul class="breadcrumb">
                                        <li><a href="{{ route('home') }}">{{ translate('Home')}}</a></li>
                                        <li><a href="{{ route('dashboard') }}">{{ translate('Dashboard')}}</a></li>
                                        <li class="active"><a
                                                href="{{ route('refund.index') }}">{{ translate('Sent Return/Refund Request')}}</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                
                    <!-- Order history table -->
                    <div class="card no-border mt-4">
                        {{-- <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" href="#return" role="tab" data-toggle="tab" aria-selected="true">Return</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#refund" role="tab" data-toggle="tab">Refund</a>
                            </li>

                        </ul> --}}
                        <table class="table table-sm table-hover table-responsive-md">
                            <thead>
                                <tr>
                                    <th>{{ translate('#')}}</th>
                                    <th>{{ translate('Date')}}</th>
                                    <th>{{ translate('Order Id')}}</th>
                                    <th>{{ translate('Product')}}</th>
                                    <th>{{ translate('Amount')}}</th>
<th>{{ translate('Request Type')}}</th>
                                    <th>{{ translate('Status')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($refund) > 0)
                                @foreach ($refund as $key => $refund_li)

                                <tr>
                                    <td>{{ $refund->firstItem() + $key}}</td>
                                    <td>
                                        {{ date('d-m-Y', strtotime($refund_li->created_at))}}
                                    </td>
                                    <td>{{  $refund_li->code }}</td>
                                    <td>
                                        {{ $refund_li->name }}
                                    </td>
                                    <td>
                                        {{ $refund_li->amount }}
                                    </td>
                                    <td>
                                        {{ $refund_li->type }}
                                    </td>
                                    <td>
                                        @php
                                        //echo $refund_li->status;die;
                                        @endphp

                                        
                                        <span style="color:green"> {{ $refund_li->status }}</span>
                                      

                                    </td>

                                </tr>

                                @endforeach
                                @else 
                                <tr><td rowspan="6">No Record Found</td></tr>

                                @endif
                            
                            </tbody>
                        </table>

                        {{-- <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="return">
                             



                                <table class="table table-sm table-hover table-responsive-md">
                                    <thead>
                                        <tr>
                                            <th>{{ translate('#')}}</th>
                                            <th>{{ translate('Date')}}</th>
                                            <th>{{ translate('Order Id')}}</th>
                                            <th>{{ translate('Product')}}</th>
                                            <th>{{ translate('Amount')}}</th>
                                            
                                            <th>{{ translate('Status')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($return) > 0)
                                        @foreach ($return as $key => $return_li)

                                        <tr>
                                            <td>{{ $return->firstItem() + $key}}</td>
                                            <td>
                                                {{ date('d-m-Y', strtotime($return_li->created_at))}}
                                            </td>
                                            <td>{{  $return_li->code }}</td>
                                            <td>
                                                {{ $return_li->name }}
                                            </td>
                                            <td>
                                                {{ $return_li->amount }}
                                            </td>
                                            <td>
                                                @php
                                                //echo $refund_li->status;die;
                                                @endphp

                                                @if ($return_li->status === "Pending")
                                                <span style="color:red">{{ translate('Pending')}}</span>
                                                @elseif($return_li->status === "Approved")
                                                <span style="color:green"> {{ translate('Approved')}}</span>
                                                @else
                                                <span style="color:green"> {{ translate('Rejected')}}</span>
                                                @endif

                                            </td>

                                        </tr>

                                        @endforeach
                                        @else 
                                        <tr><td rowspan="6">No Record Found</td></tr>

                                    
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="refund">
                               


                            </div>

                        </div> --}}
                        <div>

                        </div>
                    </div>
                   

                    <div class="pagination-wrapper py-4">
                        <ul class="pagination justify-content-end">
                            {{ $refund->links() }}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



@endsection
@section('script')
<script type="text/javascript">

</script>

@endsection