@extends('admin.layout.admin_template')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body">
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{translate('Sub-Sub-categories')}}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">
                    <div class="row">
                            <div class="col-sm-12">
                            <a href="{{ route('subsubcategories.create')}}" class="btn btn-rounded btn-info pull-right">{{translate('Add New Sub Subcategory')}}</a>
                           </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{translate('Sub Subcategory')}}</th>
                                        <th>{{translate('Subcategory')}}</th>
                                        <th>{{translate('Category')}}</th>
                                        {{-- <th>{{translate('Attributes')}}</th> --}}
                                        <th width="10%">{{translate('Options')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($subsubcategories as $key => $subsubcategory)
                                @if ($subsubcategory->subcategory != null && $subsubcategory->subcategory->category != null)
                                    <tr>
                                        <td>{{ ($key+1) + ($subsubcategories->currentPage() - 1)*$subsubcategories->perPage() }}</td>
                                        <td>{{__($subsubcategory->name)}}</td>
                                        <td>{{$subsubcategory->subcategory->name}}</td>
                                        <td>{{$subsubcategory->subcategory->category->name}}</td>
                                        <td>
                                            <div class="btn-group dropdown">
                                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                    {{translate('Actions')}} <i class="dropdown-caret"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="{{route('subsubcategories.edit', encrypt($subsubcategory->id))}}">{{translate('Edit')}}</a></li>
                                                    <li><a onclick="confirm_modal('{{route('subsubcategories.destroy', $subsubcategory->id)}}');">{{translate('Delete')}}</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                                @endforeach
                                </tbody>
                              <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>{{translate('Sub Subcategory')}}</th>
                                        <th>{{translate('Subcategory')}}</th>
                                        <th>{{translate('Category')}}</th>
                                        {{-- <th>{{translate('Attributes')}}</th> --}}
                                        <th width="10%">{{translate('Options')}}</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="clearfix">
                                <div class="pull-right">
                                {{ $subsubcategories->appends(request()->input())->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
</div>
</div>

@endsection
