<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{translate('Slider Information')}}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <form class="form-horizontal" action="{{ route('sliders.store') }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf

                            <div class="form-body">
                                <div class="row">

                                    <div class="col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span class="required">{{translate('URL')}}</span>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" id="url" name="url" placeholder="http://example.com/"
                                                    class="form-control" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <span class="required"> {{translate('Slider Images')}}</span>
                                                <strong>(850px*315px)</strong>
                                            </div>
                                            <div class="col-sm-8" style="background-color:#fff;">
                                                <div id="photos">

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                <div class="col-md-8 offset-md-4">
                                    <button type="submit"
                                        class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                    <button type="reset" onclick="document.location.reload()"
                                        class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                    <a href="{{route('home_settings.index')}}"
                                        class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>

                                </div>
                            </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    </div>
</section>



{{-- <br><br><div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{translate('Slider Information')}}</h3>
</div>

<!--Horizontal Form-->
<!--===================================================-->
<form class="form-horizontal" action="{{ route('sliders.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="panel-body">
        <div class="form-group row">
            <label class="col-sm-3" for="url">{{translate('URL')}}</label>
            <div class="col-sm-9">
                <input type="text" id="url" name="url" placeholder="http://example.com/" class="form-control" required>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-3">
                <label class="control-label">{{translate('Slider Images')}}</label>
                <strong>(850px*315px)</strong>
            </div>
            <div class="col-sm-9" style="background-color:#fff;">
                <div id="photos">

                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer text-center">
        <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
        <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
        <a href="{{route('home_settings.index')}}" class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>
    </div>
</form>
<!--===================================================-->
<!--End Horizontal Form-->

</div> --}}

<script type="text/javascript">
    $(document).ready(function(){
        $("#photos").spartanMultiImagePicker({
            fieldName:        'photos[]',
            maxCount:         10,
            rowHeight:        '200px',
            groupClassName:   'col-md-4 col-sm-9 col-xs-6',
            maxFileSize:      '',
            dropFileLabel : "Drop Here",
            onExtensionErr : function(index, file){
                console.log(index, file,  'extension err');
                alert('Please only input png or jpg type file')
            },
            onSizeErr : function(index, file){
                console.log(index, file,  'file size too big');
                alert('File size too big');
            }
        });
    });

</script>