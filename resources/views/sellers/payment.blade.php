@extends('admin.layout.admin_template')
@section('content')

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body"><!-- Dashboard Ecommerce Starts -->
<!-- Zero configuration table -->
<section id="basic-datatable">
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Zero configuration</h4>
            </div>
            <div class="card-content">
                <div class="card-body card-dashboard">
                    <h3 class="panel-title">{{ \App\Seller::find($payments->first()->seller_id)->user->name }} ({{ \App\Seller::find($payments->first()->seller_id)->user->shop->name }})</h3>
                    <div class="table-responsive">
                        <table class="table zero-configuration">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{translate('Date')}}</th>
                                    <th>{{translate('Amount')}}</th>
                                    <th>{{ translate('Payment Method') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($payments as $key => $payment)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ date('d/m/Y h:m A', strtotime(trim(str_replace('/', '-', $payment->created_at)))) }}</td>
                        <td>
                            {{ single_price($payment->amount) }}
                        </td>
                        <td>{{ ucfirst(str_replace('_', ' ', $payment->payment_method)) }} @if ($payment->txn_code != null) (TRX ID : {{ $payment->txn_code }}) @endif</td>
                    </tr>
                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>{{translate('Date')}}</th>
                                    <th>{{translate('Amount')}}</th>
                                    <th>{{ translate('Payment Method') }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
</div>
</div>
</div>
<!-- END: Content-->


@endsection
