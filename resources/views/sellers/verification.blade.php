@extends('admin.layout.admin_template')
@section('content')

 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body"><!-- page users view start -->
<section class="page-users-view">
<div class="row">
  
  <!-- information start -->
  <div class="col-md-6 col-12 ">
    <div class="card">
      <div class="card-header">
        <div class="card-title mb-2">{{translate('User Info')}}</div>
      </div>
      <div class="card-body">
        <table>
          <tr>
            <td class="font-weight-bold">{{translate('Name : ')}} </td>
            <td>{{ $sellerdata->user->name }}
            </td>
          </tr>
          <tr>
            <td class="font-weight-bold">{{translate('Email : ')}}</td>
            <td>{{ $sellerdata->user->email }}</td>
          </tr>
          {{-- <tr>
            <td class="font-weight-bold">{{translate('Address : ')}}</td>
            <td>{{ $sellerdata->user->address }}
            </td>
          </tr> --}}
          <tr>
            <td class="font-weight-bold">{{translate('Phone : ')}}</td>
            <td>{{ $sellerdata->user->phone_no }}
            </td>
          </tr>
        
          
        </table>
      </div>
    </div>
  </div>
  <!-- information start -->
  <!-- social links end -->
  <div class="col-md-6 col-12 ">
    <div class="card">
      <div class="card-header">
        <div class="card-title mb-2">{{translate('Shop Info')}}</div>
      </div>
      <div class="card-body">
        <table>
            <tr>
                <td class="font-weight-bold">{{translate('Shop Name : ')}}</td>
                <td>{{ $sellerdata->user->shop->name }}
                </td>
              </tr>
              <tr>
                <td class="font-weight-bold">{{translate('Address : ')}}</td>
                <td>{{ $sellerdata->user->shop->address }}
                </td>
              </tr>
         
        </table>
      </div>
    </div>
  </div>
  <!-- social links end -->
  <!-- permissions start -->
  <div class="col-12">
    <div class="card">
      <div class="card-header border-bottom mx-2 px-0">
        <h6 class="border-bottom py-1 mb-0 font-medium-2"><i class="feather icon-lock mr-50 "></i>{{translate('Verification Info')}}
        </h6>
      </div>
      <div class="card-body px-75">
        <div class="table-responsive users-view-permission">
        
          <form class="form-horizontal" enctype="multipart/form-data">

            @csrf
            <div class="form-body">
                <div class="row">
                    @php
                    $exp=explode(" ",$sellerdata->user->name);
                  
                    $fname=$exp[0];
                    if( count($exp) > 1){
                    $lname=$exp[1];
                    }else{
                    $lname='';
                   }
                    @endphp
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('First Name')}}</span>
                            </div>
                            <div class="col-md-8">
                                <input type="text" readonly placeholder="{{translate('First Name')}}"
                                    id="fname" name="fname" class="form-control" required
                                    value="{{ $fname}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span
                                    span>{{translate('Last Name')}}</span></div>
                            <div class="col-md-8">
                                <input type="text" readonly placeholder="{{translate('Last Name')}}"
                                    id="lname" name="lname" class="form-control" required
                                    value="{{ $lname}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span
                                    span>{{translate('Email')}}</span></div>
                            <div class="col-md-8">
                                <input type="text" readonly placeholder="{{translate('Email')}}"
                                    id="email" readonly name="email" class="form-control" required
                                    value="{{ $sellerdata->user->email}}">
                            </div>
                        </div>
                    </div>




                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Phone No')}}</span>
                            </div>
                            <div class="col-md-8">
                                <input type="text" readonly class="form-control" name="phone_no"
                                    value="{{ $sellerdata->user->phone_no }}"
                                    placeholder="{{translate('Phone No')}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <span>{{translate('Nature Of Business')}}</span>
                            </div>
                            <?php $nature=$sellerdata->business_nature; ?>
                            <div class="col-md-8">
                                <select class="form-control" disabled name="business_nature"
                                    id="business_nature">
                                    <option value="">--Select--</option>
                                    @foreach(Config::get('constants.BUSINESS_NATURE') as
                                    $key=>$val)
                                         
                                    <option value="{{$key}}" <?php  if($key===$nature) echo"selected"?> >{{$val}}</option>

                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('HSN Code')}}</span>
                            </div>
                            <div class="col-md-8">
                                <input type="text" readonly placeholder="{{translate('HSN Code')}}"
                                    id="hsn_code" name="hsn_code"
                                    value="{{ $sellerdata->hsn_code }}"
                                    class="form-control">
                            </div>
                        </div>
                    </div> -->
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <span>{{translate('Category Product')}}</span>
                            </div>
                            <div class="col-md-8">
                                @if($sellerdata->product_category==0)
                                <input type="text" readonly value="{{$sellerdata->other_category}}" name="product_category"
                                id="product_category"  class="form-control">
                                @else 
                                    <select class="form-control" disabled name="product_category"
                                    id="product_category">
                                   
                        
                                   @foreach($category as $key=>$val)

                                    <option value="{{$val['id']}}" @if($key==$sellerdata->product_category) selected @endif>{{$val['name']}}
                                    </option>

                                    @endforeach

                                @endif
                                 
                                </select>
                            </div>
                        </div>
                    </div>
                   
                    @if(!empty($sellerdata->product_invoice))
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Product Invoice ')}}</span>
                            </div>
                            <div class="col-md-8">
                                {{-- <input type="file" class="form-control" name="file_pan"> --}}
                                <a href="{{asset('uploads/verification_form/'.$sellerdata->product_invoice)}}"
                                    target="_blank"><span> View Product Invoice</span></a>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(!empty($sellerdata->product_licence))
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Product License/Invoice')}}</span>
                            </div>
                            <div class="col-md-8">
                                {{-- <input type="file" class="form-control" name="file_pan"> --}}
                                <a href="{{asset('uploads/verification_form/'.$sellerdata->product_licence)}}"
                                    target="_blank"><span> View Product License/Invoice</span></a>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <span>{{translate('Business Address')}}</span>
                            </div>
                            <div class="col-md-8">
                                <textarea type="text" class="form-control" readonly
                                    name="address_business"
                                    
                                    placeholder="{{translate('Business Address')}}">{{ $shopdata->address }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <span>{{translate('City')}}</span>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" readonly
                                    name="city"
                                    value="{{ $shopdata->city }}"
                                    placeholder="{{translate('City')}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <span>{{translate('Pincode')}}</span>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" readonly
                                    name="postal_code"
                                    value="{{ $shopdata->postal_code }}"
                                    placeholder="{{translate('Pincode')}}">
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Pan Card')}}</span>
                            </div>
                            <div class="col-md-8">
                                <input type="text" readonly class="form-control" name="pan_card"
                                    value="{{ $sellerdata->pan_card }}"
                                    placeholder="{{translate('Pan Card')}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('Pancard File')}}</span>
                            </div>
                            <div class="col-md-8">
                                {{-- <input type="file" class="form-control" name="file_pan"> --}}
                                <a href="{{asset('uploads/verification_form/'.$sellerdata->file_pan)}}"
                                    target="_blank"><span> View Pancard</span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span>{{translate('GST')}}</span>
                            </div>
                            <div class="col-md-8">
                                <input type="text" readonly class="form-control" name="gst"
                                    value="{{ $sellerdata->gst }}"
                                    placeholder="{{translate('GST')}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span  span>{{translate('GST File')}}</span>
                            </div>
                            <div class="col-md-8">
                                {{-- <input type="file" class="form-control" name="file_gst"> --}}
                                <a href="{{asset('uploads/verification_form/'.$sellerdata->file_gst)}}"
                                    target="_blank"><span> View GST </span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <span  span>{{translate('Benificiary Name')}}</span>
                            </div>
                            <div class="col-md-8">
                                <input type="text" readonly class="form-control" name="bank_acc_no"
                                    value="{{ $sellerdata->benificiary_name }}"
                                    placeholder="{{translate('Benificiary Name')}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <span  span>{{translate('Bank Account number')}}</span>
                            </div>
                            <div class="col-md-8">
                                <input type="text" readonly class="form-control" name="bank_acc_no"
                                    value="{{ $sellerdata->bank_acc_no }}"
                                    placeholder="{{translate('Bank Account number')}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <span  span>{{translate('IFSC Code')}}</span>
                            </div>
                            <div class="col-md-8">
                                <input type="text" readonly class="form-control" name="ifsc_code"
                                    value="{{ $sellerdata->ifsc_code }}"
                                    placeholder="{{translate('IFSC Code')}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4"><span  span>{{translate('Branch')}}</span>
                            </div>
                            <div class="col-md-8">
                                <input type="text" readonly class="form-control" name="bname"
                                    value="{{ $sellerdata->bname }}"
                                    placeholder="{{translate('Branch')}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <span  span>{{translate('Trademark Logo')}}</span>
                            </div>
                            <div class="col-md-8">
                                {{-- <input type="file" class="form-control"
                                    name="trademark_icon"> --}}
                                <a href="{{asset('uploads/verification_form/'.$sellerdata->trademark_icon)}}"
                                    target="_blank"><span> View Trademark</span></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <span  span>{{translate('Cancelled Cheque Upload')}}</span>
                            </div>
                            <div class="col-md-8">
                                {{-- <input type="file" class="form-control"
                                    name="cancelled_cheque"> --}}
                                <a href="{{asset('uploads/verification_form/'.$sellerdata->cancelled_cheque)}}"
                                    target="_blank"><span> View Cancelled Cheque</span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
									<div class="form-group row">
										<div class="col-md-4"><span>{{translate('Additional  Document')}}</span></div>
									<div class="col-md-8">
									<input type="file" class="form-control" name="file" required>
                                    @if ($sellerdata->additonal_doc!='')
                                    <a href="{{asset('uploads/verification_form/'.$sellerdata->additonal_doc)}}"
                                    target="_blank"><span> View Document</span></a>
                                    @endif
									</div>
									</div>
								</div>

                                <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-4">
                                <span  span>{{translate('Review')}}</span>
                            </div>
                            <div class="col-md-8">
                            <textarea placeholder="Review" id="remarks" name="remarks" class="form-control" required></textarea>
                            </div>
                        </div>
                    </div>
                  

                </div>
            </div>
        </form>

          
      
      <div class="text-center">
                <a href="" onclick="this.href='/admin/sellers/reject/{{$sellerdata->id}}?remarks='+document.getElementById('remarks').value" class="btn btn-danger d-innline-block">{{translate('Reject')}}</a></li>
                <a href="{{ route('sellers.approve', $sellerdata->id) }}" class="btn btn-primary d-innline-block">{{translate('Accept')}}</a>
            </div>
            <br>
        </div>
      </div>
    </div>
  </div>
  <!-- permissions end -->
</div>
</section>
<!-- page users view end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->

@endsection
