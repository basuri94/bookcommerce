@extends('admin.layout.admin_template')
@section('content')
   

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h2 class="content-header-title float-left mb-0">{{translate('Sellers')}}</h2>
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">{{translate('Home')}}</a>
                  </li>
                  <li class="breadcrumb-item"><a href="{{route('sellers.index')}}">{{translate('Sellers')}}</a>
                  </li>
                  <li class="breadcrumb-item active"><a href="#">Add New Seller</a>
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--<div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
          <div class="form-group breadcrum-right">
            <div class="dropdown">
              <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>
              <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a></div>
            </div>
          </div>
        </div>-->
      </div>
      <div class="content-body"><!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
  <div class="row match-height">
      <div class="col-md-12 col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Seller Form</h4>
              </div>
              <div class="card-content">
                  <div class="card-body">
                  <form class="form-horizontal" action="{{ route('sellers.store') }}" method="POST" enctype="multipart/form-data">
        	            @csrf
                          <div class="form-body">
                              <div class="row">
                                  <div class="col-12">
                                      <div class="form-group row">
                                          <div class="col-md-4">
                      <span class="required">{{translate('Name')}}</span>
                    </div>
                                          <div class="col-md-8">
                                            <input type="text" placeholder="{{translate('Name')}}" id="name" name="name" class="form-control" required>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-12">
                                      <div class="form-group row">
                                          <div class="col-md-4">
                      <span class="required">{{translate('Email Address')}}</span>
                    </div>
                                          <div class="col-md-8">
                                            <input type="text" placeholder="{{translate('Email Address')}}" id="email" name="email" class="form-control" required>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-12">
                                      <div class="form-group row">
                                          <div class="col-md-4">
                      <span class="required">{{translate('Password')}}</span>
                    </div>
                                          <div class="col-md-8">
                                            <input type="password" placeholder="{{translate('Password')}}" id="password" name="password" class="form-control" required>
                                          </div>
                                      </div>
                                  </div>
                    
                <div class="col-md-8 offset-md-4">
                                      <button type="submit" class="btn btn-primary mr-1 mb-1">{{translate('Save')}}</button>
                                      <button type="reset" onclick="document.location.reload()" class="btn btn-outline-warning mr-1 mb-1">{{translate('Reset')}}</button>
                                      <a href="{{route('sellers.index')}}" class="btn btn-outline-success mr-1 mb-1">{{translate('Cancel')}}</a>

                                  </div>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
      
  </div>
</section>
<!-- // Basic Horizontal form layout section end -->


<!-- // Basic Floating Label Form section end -->

      </div>
    </div>
  </div>
  <!-- END: Content-->

@endsection
