@extends('admin.layout.admin_template')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body"><!-- Dashboard Ecommerce Starts -->
<!-- Zero configuration table -->
<section id="basic-datatable">
<!-- Striped rows start -->
<div class="row" id="table-striped">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{translate('Seller Payments')}}</h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                  
                </div>
                <div class="table-responsive">
                    <table class="table table-striped mb-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>{{translate('Date')}}</th>
                                <th>{{translate('Seller')}}</th>
                                <th>{{translate('Amount')}}</th>
                                <th>{{ translate('Payment Method') }}</th>
                            </tr>
                      </thead>
                      <tbody>
                        @foreach($payments as $key => $payment)
                        @if (\App\Seller::find($payment->seller_id) != null && \App\Seller::find($payment->seller_id)->user != null)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ date('d/m/Y h:m A', strtotime(trim(str_replace('/', '-', $payment->created_at))))  }}</td>
                                <td>
                                    @if (\App\Seller::find($payment->seller_id) != null)
                                        {{ \App\Seller::find($payment->seller_id)->user->name }} ({{ \App\Seller::find($payment->seller_id)->user->shop->name }})
                                    @endif
                                </td>
                                <td>
                                    {{ single_price($payment->amount) }}
                                </td>
                                <td>{{ ucfirst(str_replace('_', ' ', $payment->payment_method)) }} @if ($payment->txn_code != null) (TRX ID : {{ $payment->txn_code }}) @endif</td>
                            </tr>
                        @endif
                    @endforeach
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

</section>
</div>
</div>
</div>


@endsection
