    
@extends('admin.layout.admin_template')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body"><!-- Dashboard Ecommerce Starts -->
<!-- Zero configuration table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{translate('Sellers')}}</h4>
                    <div class="row">
                        <div class="col-sm-12">
                            <a href="{{ route('sellers.create')}}" class="btn btn-rounded btn-info pull-right">{{translate('Add New Seller')}}</a>
                        </div>
                    </div>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">
                        <div class="pull-right clearfix">
                            <form class="" id="sort_sellers" action="" method="GET">
                                <div class="box-inline pad-rgt pull-left">
                                    <div class="select" style="min-width: 300px;">
                                        <select class="form-control demo-select2" name="approved_status" id="approved_status" onchange="sort_sellers()">
                                            <option value="">{{translate('Filter by Approval')}}</option>
                                            <option value="1"  @isset($approved) @if($approved == 'paid') selected @endif @endisset>{{translate('Approved')}}</option>
                                            <option value="0"  @isset($approved) @if($approved == 'unpaid') selected @endif @endisset>{{translate('Non-Approved')}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="box-inline pad-rgt pull-left">
                                    <div class="" style="min-width: 200px;">
                                        <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="{{ translate('Type name or email & Enter') }}">
                                    </div>
                                </div>

                                <div class="box-inline pad-rgt pull-left">
                                    <div class="" style="min-width: 200px;margin-left:10px;">
                                    <button class="btn btn-primary mr-1 mb-1" type="submit">{{ translate('Filter') }}</button>
                                    </div>
                                </div>
                                <div class="box-inline pad-rgt pull-left">
                        @csrf
                                    <div class="" style="min-width: 200px;margin-left:10px;">
                                       <button class="btn btn-primary mr-1 mb-1" type="button" onclick="excelReport();">{{ translate('Export Excel') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{translate('Name')}}</th>
                                        <th>{{translate('Phone')}}</th>
                                        <th>{{translate('Email Address')}}</th>
                                        <th>{{translate('Verification Info')}}</th>
                                        <th>{{translate('Approval')}}</th>
                                        <th>{{ translate('Num. of Products') }}</th>
                                        <th>{{ translate('Due to seller') }}</th>
                                        <th width="10%">{{translate('Options')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($sellers as $key => $seller)
                                    @if($seller->user != null)
                                        <tr>
                                            <td>{{ ($key+1) + ($sellers->currentPage() - 1)*$sellers->perPage() }}</td>
                                            <td>{{$seller->user->name}}</td>
                                            <td>{{$seller->user->phone_no}}</td>
                                            <td>{{$seller->user->email}}</td>
                                            <td>
                                              
                                                    <a href="{{ route('sellers.show_verification_request', $seller->id) }}">
                                                        <div class="label label-table label-info">
                                                            {{translate('Show')}}
                                                        </div>
                                                    </a>
                                               
                                            </td>
                                            <!-- @if($seller->verification_status == 0)
                                            <td><a href="#"><button
                                                onclick="update_approved('{{ $seller->id }}',1 );"
                                                  class="btn btn-danger btn-sm" title="Click to active"><i class="fa fa-toggle-off"
                                                    aria-hidden="true"></i></button></a></td>
                                            @else -->
                                            <!-- <td><a href="#"><button
                                                onclick="update_approved('{{ $seller->id }}',0);"
                                                  class="btn btn-primary btn-sm" title="Click to inactive"><i class="fa fa-toggle-on"
                                                    aria-hidden="true"></i></button></a></td> -->
                                            <!-- @endif -->
                                            <td>
                                <label class="switch">
                                    <input onchange="update_approved(this)" value="{{ $seller->id }}" type="checkbox" <?php if($seller->verification_status == 1) echo "checked";?> >
                                    <span class="slider round"></span>
                                </label>
                            </td>
                                          
                                            <td>{{ \App\Product::where('user_id', $seller->user->id)->count() }}</td>
                                            <td>
                                                @if ($seller->admin_to_pay >= 0)
                                                    {{ single_price($seller->admin_to_pay) }}
                                                @else
                                                    {{ single_price(abs($seller->admin_to_pay)) }} (Due to Admin)
                                                @endif
                                            </td>
                                            <td>
                                                <div class="btn-group dropdown">
                                                    <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                        {{translate('Actions')}} <i class="dropdown-caret"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li style="background-color: purple; color:white" ><a onclick="show_seller_profile('{{$seller->id}}');">{{translate('Profile')}}</a></li>
                                                        <!-- <li style="background-color: purple; color:white" ><a href="{{route('sellers.login_test', $seller->id)}}">{{translate('Login as Seller')}}</a></li> -->
                                                        <li style="background-color: purple; color:white" ><a onclick="show_seller_payment_modal('{{$seller->id}}');">{{translate('Pay Now')}}</a></li>
                                                        <li style="background-color: purple; color:white" ><a  style="background-color: purple; color:white" href="{{route('sellers.payment_history', encrypt($seller->id))}}">{{translate('Payment History')}}</a></li>
                                                        <li style="background-color: purple; color:white"><a  style="background-color: purple; color:white" href="{{route('sellers.edit', encrypt($seller->id))}}">{{translate('Edit')}}</a></li>
                                                        <li style="background-color: purple; color:white"><a onclick="confirm_modal('{{route('sellers.destroy', $seller->id)}}');">{{translate('Delete')}}</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>{{translate('Name')}}</th>
                                        <th>{{translate('Phone')}}</th>
                                        <th>{{translate('Email Address')}}</th>
                                        <th>{{translate('Verification Info')}}</th>
                                        <th>{{translate('Approval')}}</th>
                                        <th>{{ translate('Num. of Products') }}</th>
                                        <th>{{ translate('Due to seller') }}</th>
                                        <th width="10%">{{translate('Options')}}</th>
                                    </tr>
                                </tfoot>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
</div>
</div>
  <!-- Modal -->
  <div class="modal fade text-left" id="payment_modal" tabindex="-1" role="dialog"
  aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content" >
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">{{translate('Pay to seller')}}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-content">
       
      </div>
    
    </div>
  </div>
</div>

 <!-- Modal -->
 <div class="modal fade text-left" id="profile_modal" tabindex="-1" role="dialog"
 aria-labelledby="myModalLabel17" aria-hidden="true">
 <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
   <div class="modal-content" >
    <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">{{translate('Seller Profile')}}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-content">
       
      </div>
    
   </div>
 </div>
</div>





<!-- END: Content-->




@endsection

@section('script')
    <script type="text/javascript">
      function excelReport(){
                var from_date=$('#from_date').val();
                var to_date=$('#to_date').val();
                var token = $("input[name='_token']").val();
        // var fd = new FormData();
        // fd.append('_token', token);
        // fd.append('from_date', from_date);
        // fd.append('to_date', to_date);
        var data={from_date:from_date,to_date:to_date,_token:token};
        redirectPost("{{route('seller_list.export')}}",data); 

               
            }
            var redirectPost = function (url, data = null, method = 'post') {
                var form = document.createElement('form');
                form.method = method;
                form.action = url;
                for (var name in data) {
                    var input = document.createElement('input');
                    input.type = 'hidden';
                    input.name = name;
                    input.value = data[name];
                    form.appendChild(input);
                }
                $('body').append(form);
                form.submit();
            };
        function show_seller_payment_modal(id){
            $.post('{{ route('sellers.payment_modal') }}',{_token:'{{ @csrf_token() }}', id:id}, function(data){
                $('#payment_modal #modal-content').html(data);
                $('#payment_modal').modal('show', {backdrop: 'static'});
                $('.demo-select2-placeholder').select2();
            });
        }

        function show_seller_profile(id){
            $.post('{{ route('sellers.profile_modal') }}',{_token:'{{ @csrf_token() }}', id:id}, function(data){
                $('#profile_modal #modal-content').html(data);
                $('#profile_modal').modal('show', {backdrop: 'static'});
            });
        }

        function update_approved(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('sellers.approved') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: "Approved sellers updated successfully",
                    showConfirmButton: false,
                    timer: 1500
                    });
                   // showAlert('success', 'Approved sellers updated successfully');
                }
                else{
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: "Something went wrong",
                        showConfirmButton: false,
                        timer: 1500
                        });
                  //  showAlert('danger', 'Something went wrong');
                }
                location.reload();
               
            });
        }

        function sort_sellers(el){
            $('#sort_sellers').submit();
        }
    </script>
@stop