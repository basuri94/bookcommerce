 <!-- BEGIN: Content-->
 

      <div class="col-xl-12 col-md-12 col-sm-12 profile-card-2">
        <div class="card">
          <div class="card-header mx-auto pb-0">
            <div class="row m-0">
              <div class="col-sm-12 text-center">
                <h4>{{ $seller->user->name }}</h4>
              </div>
              <div class="col-sm-12 text-center">
                <p class="">{{ $seller->user->shop->name }}</p>
              </div>
            </div>
          </div>
           <!--<div class="card-content">
            <div class="card-body text-center mx-auto">
              <div class="avatar avatar-xl">
              @if($seller->user->avatar_original!='')
                <img class="img-fluid" src="{{ asset($seller->user->avatar_original) }}" alt="img placeholder">
                @endif
              </div>
              <div class="d-flex justify-content-between mt-2">
                <div class="uploads">
                  <p class="font-weight-bold font-medium-2 mb-0">
                    <a href="{{ $seller->user->shop->facebook }}"  data-original-title="Facebook" data-container="body"><i class="fa fa-facebook"></i></a>
                  </p>
                  <span class="">Facebook</span>
                </div>
                <div class="followers">
                  <p class="font-weight-bold font-medium-2 mb-0">
                    <a href="{{ $seller->user->shop->twitter }}" data-original-title="Twitter" data-container="body"><i class="fa fa-twitter"></i></a>
                  </p>
                  <span class="">Twitter</span>
                </div>
                <div class="following">
                  <p class="font-weight-bold font-medium-2 mb-0">
                    <a href="{{ $seller->user->shop->google }}"  data-original-title="Google+" data-container="body"><i class="fa fa-google"></i></a>
                  </p>
                  <span class="">Google</span>
                </div>
              </div> 
             
            </div>
          </div>-->
        </div>
      </div>

      <!-- Profile Cards Ends -->

       <!-- information start -->
       <div class="row">
    <div class="col-md-6 col-6 ">
      <div class="card">
        <div class="card-header">
          <div class="card-title mb-2">Information</div>
        </div>
        <div class="card-body">
          <table>
            <tr>
              <td class="font-weight-bold">{{translate('Name : ')}}  </td>
              <td>{{ $seller->user->name }}
              </td>
            </tr>
            <tr>
              <td class="font-weight-bold">{{translate('Shop Address : ')}}</td>
              <td>{{ $seller->user->shop->address }}</td>
            </tr>
            <tr>
              <td class="font-weight-bold">{{translate('Shop Name : ')}}</td>
              <td><a href="{{ route('shop.visit', $seller->user->shop->slug) }}" class="btn-link"><i class="fa fa-location-arrow"></i>{{ $seller->user->shop->name }}</a>
              </td>
            </tr>
            <tr>
              <td class="font-weight-bold">{{translate('Phone : ')}}</td>
              <td>{{ $seller->user->phone_no }}
              </td>
            </tr>
            
          </table>
        </div>
      </div>
    </div>
    <!-- information start -->
    <!-- social links end -->
    <div class="col-md-6 col-6 ">
      <div class="card">
        <div class="card-header">
          <div class="card-title mb-2">Payment Info</div>
        </div>
        <div class="card-body">
          <table>
            <!-- <tr>
              <td class="font-weight-bold">{{translate('Bank Name : ')}}</td>
              <td>{{ $seller->bank_name }}
              </td>
            </tr>
            <tr>
              <td class="font-weight-bold">{{translate('Bank Acc Name :')}}</td>
              <td>{{ $seller->bank_acc_name }}
              </td>
            </tr>
            <tr>
              <td class="font-weight-bold">{{translate('Bank Acc Number :')}}</td>
              <td>{{ $seller->bank_acc_no }}
              </td>
            </tr>
            <tr>
              <td class="font-weight-bold">{{translate('Bank IFSC Code : ')}}</td>
              <td>{{ $seller->bank_routing_no }}
              </td>
            </tr> -->
            <tr>
              <td class="font-weight-bold">{{translate('Benificiary Name : ')}}</td>
              <td>{{ $seller->benificiary_name }}
              </td>
            </tr>
            <tr>
              <td class="font-weight-bold">{{translate('Bank Account number : ')}}</td>
              <td>{{ $seller->bank_acc_no }}
              </td>
            </tr>
            <tr>
              <td class="font-weight-bold">{{translate('IFSC Code : ')}}</td>
              <td>{{ $seller->ifsc_code }}
              </td>
            </tr>
            <tr>
              <td class="font-weight-bold">{{translate('Branch : ')}}</td>
              <td>{{ $seller->bname }}
              </td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>

    <!-- Striped rows start -->
<div class="row" id="table-striped">
  <div class="col-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title">Product Details</h4>
          </div>
          <div class="card-content">
              <div class="card-body">
               
              </div>
              <div class="table-responsive">
                  <table class="table table-striped mb-0">
            
                    <tbody>
                      <tr>
                        <td>{{ translate('Total Products') }}</td>
                        <td>{{ App\Product::where('user_id', $seller->user->id)->get()->count() }}</td>
                    </tr>
                    <tr>
                        <td>{{ translate('Total Orders') }}</td>
                        <td>{{ App\OrderDetail::where('seller_id', $seller->user->id)->get()->count() }}</td>
                    </tr>
                    <tr>
                        <td>{{ translate('Total Sold Amount') }}</td>
                        @php
                            $orderDetails = \App\OrderDetail::where('seller_id', $seller->user->id)->get();
                            $total = 0;
                            foreach ($orderDetails as $key => $orderDetail) {
                                if($orderDetail->order->payment_status == 'paid'){
                                    $total += $orderDetail->price;
                                }
                            }
                        @endphp
                        <td>{{ single_price($total) }}</td>
                    </tr>
                    <!-- <tr>
                        <td>{{ translate('Wallet Balance') }}</td>
                        <td>{{ single_price($seller->user->balance) }}</td>
                    </tr> -->
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>
<div class="row" id="table-striped">
  <div class="col-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title">Shyplite Address ID </h4>
          </div>
          <div class="card-content">
              <div class="card-body">
               
              </div>
              <div class="table-responsive">
                  <table class="table table-striped mb-0">
            
                    <tbody>
                      <tr>
                        <td>{{ translate('Address ID') }}</td>
                        <td><input type="text" class="form-control" name="addressid" id="addressid" required  value="{{$seller->user->address_id}}"></td>
                    </tr>
                    <tr>
                        <td colspan="2"><a href="" onclick="this.href='/admin/sellers/addressid/{{$seller->user->id}}?addressid='+document.getElementById('addressid').value" class="btn btn-success d-innline-block">{{translate('Update')}}</a></td>
                    </tr>
                    
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>
<!-- Striped rows end -->
    <!-- social links end -->
    </div>

    <div class="modal-footer">
      <div class="panel-footer text-right">
        
          <button class="btn btn-danger" data-dismiss="modal">{{translate('Close')}}</button>
      </div>
  </div>
 

