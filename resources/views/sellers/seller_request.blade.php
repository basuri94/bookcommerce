@extends('admin.layout.admin_template')
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body">
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{ translate('Category Requests') }}</h4>
                   
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">
                        <div class="pull-right clearfix">
                               <form class="" id="sort_sellers" action="" method="GET">
                                    
                                        <div class="box-inline pad-rgt pull-left">
                                            <div class="select" style="min-width: 200px;">
                                                <select class="form-control demo-select2" id="user_id" name="user_id" onchange="sort_products()">
                                                    <option value="">All Sellers</option>
                                                    @foreach (App\Seller::all() as $key => $seller)
                                                        @if ($seller->user != null && $seller->user->shop != null)
                                                            <option value="{{ $seller->user->id }}" @if ($seller->user->id == $seller_id) selected @endif>{{ $seller->user->shop->name }} ({{ $seller->user->name }})</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="box-inline pad-rgt pull-left">
                                    <div class="select" style="min-width: 300px;">
                                        <select class="form-control demo-select2" name="approved_status" id="approved_status" onchange="sort_sellers()">
                                            <option value="">{{translate('Filter by Approval')}}</option>
                                            <option value="1"  @isset($approved) @if($approved == '1') selected @endif @endisset>{{translate('Approved')}}</option>
                                            <option value="0"  @isset($approved) @if($approved == '0') selected @endif @endisset>{{translate('Non-Approved')}}</option>
                                        </select>
                                    </div>
                                </div>
                                   
                                    
                                </form>

                        </div>
                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{ translate('Seller')}}</th>
                                            <th>{{ translate('Category')}}</th>
                                            <th>{{ translate('Sub category')}}</th>
                                            <th>{{ translate('Sub Subcategory')}}</th>
                                            <th>{{ translate('HSN')}}</th>
                                            <th>{{ translate('GST(%)')}}</th>
                                            <th>{{ translate('Options')}}</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                    @foreach ($sellerRequest as $key => $sellerRequests)
                                            <tr>
                                                <td>{{ ($key+1) + ($sellerRequest->currentPage() - 1)*$sellerRequest->perPage() }}</td>
                                                <td>{{ $sellerRequests->user->name }}</td>
                                                <td>{{ $sellerRequests->category }}</td>
                                                 <td>{{ $sellerRequests->subcategory }}</td>
                                                <td>
                                                    @if ($sellerRequests->subsubcategory != null)
                                                        {{ $sellerRequests->subsubcategory}}
                                                    @endif
                                                </td>
                                                <td>{{ $sellerRequests->hsn }}</td>
                                                <td>{{ $sellerRequests->gst }}%</td>
                                                
                                                <td>
                                                <label class="switch">
                                                <input onchange="update_featured(this)" value="{{ $sellerRequests->id }}" type="checkbox" <?php if($sellerRequests->status == 1) echo "checked";?> >
                                                <span class="slider round"></span></label></td>
                                                
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            <div class="clearfix">
                            <div class="pull-right">
                                {{ $sellerRequest->appends(request()->input())->links() }}
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
</div>
</div>

@endsection


@section('script')
    <script type="text/javascript">

        $(document).ready(function(){
            //$('#container').removeClass('mainnav-lg').addClass('mainnav-sm');
        });

        function update_featured(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('request.approval') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){ 
                    showAlert('success', 'Featured products updated successfully');
                    
                }
                else{
                    showAlert('danger', 'Something went wrong');
                   
                }
            });
        }

        function sort_products(el){
            $('#sort_sellers').submit();
        }
        function sort_sellers(el){
            $('#sort_sellers').submit();
        }

    </script>
@endsection
